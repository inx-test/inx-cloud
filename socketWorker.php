<?php
require_once 'config.php';
require_once 'vendor/autoload.php';

use UDPProcess\UDPQueueClient;
use Pheanstalk\Pheanstalk;
use Pheanstalk\PheanstalkInterface;
//Reduce errors
error_reporting(~E_WARNING);


echo "Socket Start \n";
//die("2");


//Create a UDP socket
if(!($sock = socket_create(AF_INET, SOCK_DGRAM, 0)))
{
    $errorcode = socket_last_error();
    $errormsg = socket_strerror($errorcode);
     
    die("Couldn't create socket: [$errorcode] $errormsg \n");
}
 
echo "Socket created \n";
 
// Bind the source address
if( !socket_bind($sock, "0.0.0.0" , 9999) )
{
    $errorcode = socket_last_error();
    $errormsg = socket_strerror($errorcode);
     
    die("Could not bind socket : [$errorcode] $errormsg \n");
}
 
echo "Socket bind OK \n";
 
//Do some communication, this loop can handle multiple clients
while(1)
{
    echo "Waiting for data ... \n";
     
    //Receive some data
    $r = socket_recvfrom($sock, $buf, 512, 0, $remote_ip, $remote_port);
    echo "$remote_ip : $remote_port -- " . $buf;
    
    //Send back the data to the client
    $response = "OK " . $buf; 
    $notQue = new UDPQueueClient(NOTIF_QUEUE_SERVER,SYSTEMLOG_QUEUE_NAME);
    $notQue->queueNotificationRequest(SOCKET_SEND, array($response,$remote_ip));
    
   
   // $responseLength = strlen($response);
    //socket_sendto($sock, $response , $responseLength , 0 , $remote_ip , $remote_port);
    //$qps = $this->notQue->queueNotificationRequest('DataPolling',array(test => "test" ));
    //echo "sent data... ";
}

echo "Stop Process ... \n";
socket_close($sock);