<?php
   include_once("config.php");
   include_once("_classes/curl.php");
   include_once("harmonics.php");
    
    class parameterController extends ControllerBase 
    {	
        # Constructor Method 
        function __constructor(){
        }
        
        function getHarmonics(){
            if(!isset($_SESSION["activeDeviceID"]) || $_SESSION["activeDeviceID"] == "" || $_SESSION["activeDeviceID"] == "0"){
                    $result['status'] = 0; //no device
                    $result['message'] = "Please select device.";
                    die(json_encode($result)); 
                }
                
                $data = array(
                    "action" => "GetScopeData",
                    "source" => 3,
                    "serialNo" => $_SESSION["activeDeviceSerialNo"]
                );
                
                $result = invokeWebMethod(getWebURL($_SESSION["activeDeviceIPAddress"], $data));
                
                $resultArr = json_decode($result,TRUE);
                //echo count($resultArr);
                ///die(">>>".$result);
                if($result == "" || $result = NULL){
                    $result['status'] = 0; 
                    $result['message'] = "Failed to return data.";
                    die(json_encode($result)); 
                }
                else{
                    if(count($resultArr) > 0){
                        $fft1 = new LomontFFT();
                        $rtnVal["status"] = $resultArr["status"];
                        $rtnVal["source"] = $resultArr["source"];
                        $rtnVal["time"] = $resultArr["responseData"]["time"];
                        $rtnVal["scopeRecord"] = $resultArr["responseData"]["scopeRecord"];
                        $rtnVal["harmonicRecord"]["hvPh1"] = $fft1->calc_harmanics($resultArr["responseData"]["scopeRecord"]["ActualPh1V"]);
                        $rtnVal["harmonicRecord"]["hvPh2"] = $fft1->calc_harmanics($resultArr["responseData"]["scopeRecord"]["ActualPh2V"]);
                        $rtnVal["harmonicRecord"]["hvPh3"] = $fft1->calc_harmanics($resultArr["responseData"]["scopeRecord"]["ActualPh3V"]);
                        $rtnVal["harmonicRecord"]["haPh1"] = $fft1->calc_harmanics($resultArr["responseData"]["scopeRecord"]["ActualPh1A"]);
                        $rtnVal["harmonicRecord"]["haPh2"] = $fft1->calc_harmanics($resultArr["responseData"]["scopeRecord"]["ActualPh2A"]);
                        $rtnVal["harmonicRecord"]["haPh3"] = $fft1->calc_harmanics($resultArr["responseData"]["scopeRecord"]["ActualPh3A"]);
                        die(json_encode($rtnVal));
                    }
                else { 
                    $result['status'] = 0; 
                    $result['message'] = "Failed to return data.";
                    die(json_encode($result)); }
                    //die(">>>>".$resultArr);
                }
                
        }
    }
    function test(){
        return 1;
    }


?>