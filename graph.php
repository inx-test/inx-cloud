<?php
include "config.php";
//include_once("tpl/header.tpl.php");
include_once("tpl/dashboard-top.tpl.php"); 
//echo date('Y-m-d H:i:s');
//echo date_default_timezone_get();
?>
<style>
	.even-port-num{
		border-bottom:2px solid yellow;
	padding-bottom:6px;
margin-top:5px;
}

	.odd-port-num{
		border-bottom:2px solid yellow;
	padding-bottom:6px;
	-moz-transform: scaleY(-1);
        -o-transform: scaleY(-1);
        -webkit-transform: scaleY(-1);
        transform: scaleY(-1);
        filter: FlipV;
        -ms-filter: "FlipV";
}

.switch-wrapper .port img{
	height:auto;
}

.port-n-1,.port-n-13,.port-n-25 {
	border-left:2px solid yellow;
	padding-left:6px;
	padding-top: 0px !important;
	border-bottom: 2px solid yellow;
	padding-bottom: 6px !important;
	border-top: 0px;
margin-top:0px;
-moz-transform: scaleY(-1);
        -o-transform: scaleY(-1);
        -webkit-transform: scaleY(-1);
        transform: scaleY(-1);
        filter: FlipV;
        -ms-filter: "FlipV";


}
.yellow-time-line{
	background-image:url("img/yellow-bar.png");
	background-repeat:repeat-x;
	height:8px;
	overflow:hidden;
}

.port-n-2,.port-n-14 {
	border-left:2px solid yellow;
	padding-left:6px;
}

.port-n-11,.port-n-23,.port-n-12,.port-n-24{
	border-right:2px solid yellow;
	padding-right:6px;
}
.switch-wrapper{
border-radius:5px !important;
//background: #7d7d7d;	
background: #463a3a;
background-image:url("img/switch-bg.png");
background-position:right top; 
background-repeat:no-repeat;
border-color:#008a69;
//border-color:#ffffff;
border-width:8px;
}
.port-number, .sw-log-time, .sw-name{
	color:white;
}

.sw-log-power-total{
background: #fefcea; /* Old browsers */
background: -moz-linear-gradient(-45deg, #fefcea 0%, #f1da36 100%); /* FF3.6-15 */
background: -webkit-linear-gradient(-45deg, #fefcea 0%,#f1da36 100%); /* Chrome10-25,Safari5.1-6 */
background: linear-gradient(135deg, #fefcea 0%,#f1da36 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#fefcea', endColorstr='#f1da36',GradientType=1 ); /* IE6-9 fallback on horizontal gradient */
}

</style>
<div class="content clearfix p-b-25">
    
    <!-- START PANEL -->
    
        <div class="panel panel-transparent">
            <div class="panel-heading ">
                <div class="panel-title  bold fs-30"> Power Setting</div>
            </div>
            <div class="panel-body">
                <!--div class="row m-b-10">
                    <div class="col-md-12">
                        <div class="pull-right text-right" style="width: 110px;">
                            <input id="fileupload" type="file" name="files[]" class="inputfile inputfile-1">
                            <label for="fileupload">Upload.</label>
                        </div>
                        <div class="pull-right m-l-15">
                            <h5 class="fs-16 bold hint-text m-t-5">Import Pull Schedule :</h5>
                        </div>
                    </div>
                </div-->
                <div class="row m-b-20">
                    <div class="col-md-12">
                        <div class="btn-toolbar">
                            <div class="btn-group btn-group-sm" id="devfilterContainer">
                            </div>
                        </div>
                    </div>
                </div>
                <div id="resultContainer" class="m-t-15">

                </div>
		<div id="resultInfo" class="m-t-5 m-b-20 text-center">
			<p class="text-center">Move the pointer over the ports for more information.</p>
		</div>
                <div class="row">
                    <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12 col-xl-12" id="divPullScheduleDetails">
                    </div>
                </div>

            </div>
        </div>
    
</div>

<!-- Start file conformation -->
<div class="modal fade stick-up in" id="uploadConfirmation" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header clearfix text-left">
                <h5 class="panel-title">Excel Upload</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                <i class="pg-close fs-14"></i></button>
            </div>
            <div class="modal-body">
                <div class="upload-progress">
                    <h5 class="fs-15" style="color:green;">Please wait while uploading file... <span class="up-pre-span">0</span>%</h5>
                </div>
                <div class="update-conf hide">
                    <h5 class="fs-15 ">Are you sure do you wish to proceed with import.<br> File Name : <span class="up-filename bold"></span> .</h5>
                    <input type="hidden" id="hdnUploadFileName" name="hdnUploadFileName" value="">
                    <div class="row">
                        <input type="button" class="btn btn-danger  btn-sm pull-right m-l-10" name="close_excel" value="Close" id="close_excel_upload" onclick="$GraphEvn.hideUploadConf();" />
                        <input type="button" class="btn btn-success btn-sm pull-right " name="save_excel" value="Save" id="save_excel_upload" onclick="return $GraphEvn.saveExcelDetails()"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End file conformation -->


<?php include_once("tpl/footer.tpl.php"); ?>

<script type="text/javascript">
    var loggedUserId = <?php echo $loggedInUserId; ?>;
    var loggedUserRole = <?php echo $loggedUserRoleId; ?>;

    $(document).ready(function () {
	
        $GraphEvn.init(1);
        $GraphEvn.fileUpload();
        setInterval(function(){
            var $filter = $("#devfilterContainer .active").attr("data-type");
               $GraphEvn.init(2);
            if(typeof $filter != "undefined")
            {
               $GraphEvn.getDeviceData($filter,2);
            }
        }, 65000);//60000);

    });

    var $GraphEvn = {
				blinkCount : 0,
        init : function($type){
			//alert(111);
            var $data = {
                c: 'Settings',
                a: 'getSwitchSort'
            };
            $.ajax({
                type: 'POST',
                url: 'ajax.php',
                data: $data,
                dataType: 'json',
                success: function (res) {
                    if(res.status == 1){
                        var $data = res.result;
                        if($type == 1){
                        for (var $i = 0; $i < $data.length; $i++) {
                            var $item = $data[$i];
                                $("#devfilterContainer").append('<button class="btn btn-default filter-btn" id="btn_filter_'+$item["ID"]+'" data-type="'+$item["ID"]+'" data-toggle="tooltip" data-placement="bottom" data-title="'+$item["Name"]+'" data-container="body" >'+$item["DevNo"]+'</button>');
                        }
                        $(".filter-btn").tooltip();
                            $("#devfilterContainer button").live("click", function () {
                            $("#devfilterContainer button").removeClass("active");
                            $(this).addClass("active");
                            var $str = $(this).attr("data-type");
                            $GraphEvn.getDeviceData($str,1);
                        });
                       $('[data-type="'+$data[0]["ID"]+'"]').click();
                    }
                        else {
							
                            var $cpData = $data;
                            $("#devfilterContainer button").each(function(){
                               var $btnIDStr = $(this).attr("id");
                               var $btnID = $btnIDStr.split("_")[2];
                               var $status = false;
                               //console.log($btnID);
                               for(var $i=0;$i < $data.length; $i++){
                                   var $item = $data[$i];
                                   //console.log($item);
								   //console.log(">"+$item.ID+"< == "+$btnID+"<");
                                   if($item.ID == $btnID){
                                        $data[$i]["status"] = 1;
                                        $status = true;
									}
                               }
                               if(! $status){
								   //console.log("heree");
                                   if(! $(this).hasClass("active")){
                                       $(this).remove();
                                   }
								   else location.reload();
                               }
                           
							});
                            //console.log($data);
                            if($data.length > 0){
                                for(var $i=0;$i < $data.length; $i++){
                                     var $item = $cpData[$i];
                                     if(typeof $item.status == "undefined"){
                                        if($("#devfilterContainer button").length > 0)
                                            $("#devfilterContainer").append('<button class="btn btn-default filter-btn" id="btn_filter_'+$item["ID"]+'" data-type="'+$item["ID"]+'" data-toggle="tooltip" data-placement="bottom" data-title="'+$item["Name"]+'" data-container="body" >'+$item["DevNo"]+'</button>');
                                        else location.reload();
                                     }
                                }
                                $(".filter-btn").tooltip();
                            }
                        }
                        
                    }
                    else{
                       if($type == 1) $("#resultContainer").html("<h5 class='text-center bold m-t-20 hint-text'> No switch available.</h5>");
                       else {
                           if($("#devfilterContainer button").length > 0){
                                //$("#cnCntent").html("Do you wish to delete this alert?");
                                //$("#modalCnfirmation").modal("show");
                                location.reload();
                           }
                       }
                }
            }
            });
        },
        search: function () {

        },
        getDeviceData : function($filter,$type){//$type 1 = get device;2 = update status
            var $data = {
                    c: 'Settings',
                    a: 'getSwitchGraph',
                    filter: $filter
                };
                $.ajax({
                    type: 'POST',
                    url: 'ajax.php',
                    data: $data,
                    dataType: 'json',
                    beforeSend: function () {
                       if($type == 1) {
                            $("#lblPullScheduleDetails").removeClass("hide");
                            __showLoadingAnimation();
                       }
                       if($(".sw-log-time").length > 0){
                           $(".sw-log-time").html('<div class="loading"><div class="loading-bar"></div><div class="loading-bar"></div><div class="loading-bar"></div><div class="loading-bar"></div></div>');

                       }
                    },
                    success: function (res) {
                        if (res.status == 1) {
                            var $resultData = res.result;
                            var $logData = res.logResult;
                            var $scheduleData = res.scheduleResult;
                            var $item = [];
                            var $devID = 0;
                            var $devName = "";
                            var $operPowerT = "";
                            var $time = "";
                            var $patchPanel = "";
                            var $pwStatus = 0;
                            var $devStatus = 0;
                            if($type == 1){
                                $("#resultContainer").html("");
                                if($resultData.length > 0){
                                    $devID = $resultData[0]["DevID"];
                                    $devStatus = $resultData[0]["status"];
                                    $devName =  decodeURIComponent(($resultData[0]["device_name"]).replace(/\+/g, " "));
                                    if($resultData[0]["oper_power_total"] != null) $operPowerT =  $resultData[0]["oper_power_total"]; 
                                    //console.log($devStatus);
                                    
                                }
                                
                                if($logData.length > 0){
                                    for(var $i=0; $logData.length > $i; $i++){
                                        var $port = $logData[$i]["Port"];
                                        
                                        if($i == 0){
                                            if($logData[$i]["LogDate"] != null){
                                                var $dt = new Date($logData[$i]["LogDate"]);
                                                $time = ("0" + ($dt.getMonth()+1)).slice(-2)+"/"+("0" + $dt.getDate()).slice(-2)+"/"+("0" + $dt.getFullYear()).slice(-2)+" "+("0" + $dt.getHours()).slice(-2)+":"+("0" + $dt.getMinutes()).slice(-2);
                                            }
                                        }
                                        
                                        if($port != null){
                                            var $operState = "default"; // NAN
                                            if($logData[$i]["OperState"] == 1) $operState = "ok";   // ON
                                            else if($logData[$i]["OperState"] == 0) $operState = "error"; // OFF

                                            $item[$port]= $operState;

                                            var $operPower  =  $logData[$i]["OperPower"];

                                            if($operPower != null) $item[$port+"_cable"] = $operPower+"w";
                                            else  $item[$port+"_cable"] = "";
                                        }
                                        
                                    }
                                    $pwStatus = 1
                                }
                                if($scheduleData.length > 0){
                                    for(var $i=0; $scheduleData.length > $i; $i++){
                                        $scheduleData[$i]["Name"] = (($scheduleData[$i]["Name"] != null) ? decodeURIComponent(($scheduleData[$i]["Name"]).replace(/\+/g, " ")) : "");
                                        $scheduleData[$i]["CableID"] = (($scheduleData[$i]["CableID"] != null) ? decodeURIComponent(($scheduleData[$i]["CableID"]).replace(/\+/g, " ")) : "");
                                        $scheduleData[$i]["PatchPanel"] = $patchPanel = (($scheduleData[$i]["PatchPanel"] != null) ? decodeURIComponent(($scheduleData[$i]["PatchPanel"]).replace(/\+/g, " ")) : "");
                                        $scheduleData[$i]["Power"] = (($scheduleData[$i]["Power"] != null) ? decodeURIComponent(($scheduleData[$i]["Power"]).replace(/\+/g, " ")) : "");
                                        $scheduleData[$i]["EnginePort"] = (($scheduleData[$i]["EnginePort"] != null) ? decodeURIComponent(($scheduleData[$i]["EnginePort"]).replace(/\+/g, " ")) : "");
                                        $scheduleData[$i]["CableType"] = (($scheduleData[$i]["CableType"] != null) ? decodeURIComponent(($scheduleData[$i]["CableType"]).replace(/\+/g, " ")) : "");
                                        $scheduleData[$i]["Color"] = (($scheduleData[$i]["Color"] != null) ? decodeURIComponent(($scheduleData[$i]["Color"]).replace(/\+/g, " ")) : "");
                                        $scheduleData[$i]["NodeSerialNum"] = (($scheduleData[$i]["NodeSerialNum"] != null) ? decodeURIComponent(($scheduleData[$i]["NodeSerialNum"]).replace(/\+/g, " ")) : "");
                                        $scheduleData[$i]["Vendor"] = (($scheduleData[$i]["Vendor"] != null) ? decodeURIComponent(($scheduleData[$i]["Vendor"]).replace(/\+/g, " ")) : "");
                                        $scheduleData[$i]["Model"] = (($scheduleData[$i]["Model"] != null) ? decodeURIComponent(($scheduleData[$i]["Model"]).replace(/\+/g, " ")) : "");
                                        $scheduleData[$i]["Type"] = (($scheduleData[$i]["Type"] != null) ? decodeURIComponent(($scheduleData[$i]["Type"]).replace(/\+/g, " ")) : "");
                                        $scheduleData[$i]["HWVer"] = (($scheduleData[$i]["HWVer"] != null) ? decodeURIComponent(($scheduleData[$i]["HWVer"]).replace(/\+/g, " ")) : "");
                                        $scheduleData[$i]["SWVer"] = (($scheduleData[$i]["SWVer"] != null) ? decodeURIComponent(($scheduleData[$i]["SWVer"]).replace(/\+/g, " ")) : "");
                                        $scheduleData[$i]["PS_Status"] = (($scheduleData[$i]["PS_Status"] != null) ? decodeURIComponent(($scheduleData[$i]["PS_Status"]).replace(/\+/g, " ")) : "");
                                        $scheduleData[$i]["TalkerPort"] = (($scheduleData[$i]["TalkerPort"] != null) ? decodeURIComponent(($scheduleData[$i]["TalkerPort"]).replace(/\+/g, " ")) : "");
                                        $scheduleData[$i]["NonTalkerPort"] = (($scheduleData[$i]["NonTalkerPort"] != null) ? decodeURIComponent(($scheduleData[$i]["NonTalkerPort"]).replace(/\+/g, " ")) : "");
                                        $scheduleData[$i]["LocationName"] = (($scheduleData[$i]["LocationName"] != null) ? decodeURIComponent(($scheduleData[$i]["LocationName"]).replace(/\+/g, " ")) : "");
                                        $scheduleData[$i]["LocationPath"] = (($scheduleData[$i]["LocationPath"] != null) ? decodeURIComponent(($scheduleData[$i]["LocationPath"]).replace(/\+/g, " ")) : "");
                                        $scheduleData[$i]["TooManyDiscoveries"] = (($scheduleData[$i]["TooManyDiscoveries"] != null) ? decodeURIComponent(($scheduleData[$i]["TooManyDiscoveries"]).replace(/\+/g, " ")) : "");
                                        $scheduleData[$i]["PodID"] = (($scheduleData[$i]["PodID"] != null) ? decodeURIComponent(($scheduleData[$i]["PodID"]).replace(/\+/g, " ")) : "");
                                        $scheduleData[$i]["PodStatus"] = (($scheduleData[$i]["PodStatus"] != null) ? decodeURIComponent(($scheduleData[$i]["PodStatus"]).replace(/\+/g, " ")) : "");
                                        $scheduleData[$i]["device_name"] = $devName;
                                        
                                        //ProtName
                                        var $port = $scheduleData[$i]["Port"];
                                        var $cableID    =  $scheduleData[$i]["CableID"];
                                        if($cableID != null && $cableID != ""){
                                            $cableID = decodeURIComponent($cableID.replace(/\+/g, " "));
                                            var cableIDSplit = $cableID.split("/");
                                            if(typeof $item[$port+"_cable"] != "undefined"){
                                                $item[$port+"_cable"] = cableIDSplit[0]+ " / "+ ($item[$port+"_cable"] != null ? $item[$port+"_cable"] : "");
                                            }
                                            else $item[$port+"_cable"] = cableIDSplit[0];
                                            
                                        }
                                    }
                                }
                                var $switchStr = tmpl("tmpl-graph", {devId:$devID,name:$devName,operPower:$operPowerT,time:$time,patchPanel:$patchPanel,pwStatus : $pwStatus,data:$item});
                                //console.log($switchStr);
                                $("#resultContainer").append($switchStr);
                                
                                
                                if($scheduleData.length > 0){
                                    $("#lblPullScheduleDetails").removeClass("hide");
                                    var $schedule = tmpl("tmpl-schedule", {data:$scheduleData});
                                    $("#divPullScheduleDetails").html($schedule);
                                }
                                else $("#divPullScheduleDetails").html(""); 

                                $(".port_tooltip").tooltip({html:true});
                                if($devStatus == 0) $(".cisco_status").html("<span class=\"notify-bubble bg-green bg-green-ball\"></span>  Online"); 
                                else  $(".cisco_status").html("<span class=\"notify-bubble bg-grey bg-grey-ball\"></span>  Offline"); 
                            }
                            else {
                                var $operPowerStr = "";
                                if($resultData.length > 0){
                                    $devID = $resultData[0]["DevID"];
                                    $devStatus = $resultData[0]["status"];
                                    $devName =  decodeURIComponent(($resultData[0]["device_name"]).replace(/\+/g, " "));
                                    
                                    if($( "#sw_name_"+$devID ).length > 0){
                                        //$( "#sw_name_"+$id).attr("data-title",$patchPanel);
                                        $( "#sw_name_"+$devID+" .sw-name").html($devName);
                                        if($logData.length > 0){ 
                                            if($resultData[0]["oper_power_total"] != null){
                                                $operPowerT =  $resultData[0]["oper_power_total"];
                                                $( "#swPowerTotal_"+$devID).removeClass("hide");
                                                $( "#swPowerTotal_"+$devID).html($resultData[0]["oper_power_total"]+"w");
                                            } 
                                            else $( "#swPowerTotal_"+$devID).addClass("hide");
                                            
                                            if($logData[0]["LogDate"] != null){
                                                var $dt = new Date($logData[0]["LogDate"]);
                                                $("#swLogTime_"+$devID).html(("0" + ($dt.getMonth()+1)).slice(-2)+"/"+("0" + $dt.getDate()).slice(-2)+"/"+("0" + $dt.getFullYear()).slice(-2)+" "+("0" + $dt.getHours()).slice(-2)+":"+("0" + $dt.getMinutes()).slice(-2));
                                            }
                                            else $("#swLogTime_"+$devID).html("");
                                            $( "#swLogTime_"+$devID).removeClass("hide");
                                        }
                                        else{
                                            $( "#swPowerTotal_"+$devID).addClass("hide");
                                            $( "#swPowerTotal_"+$devID).html("");
                                            $( "#swLogTime_"+$devID).addClass("hide");
                                            $("#swLogTime_"+$devID).html("");
                                        }
                                    }
                                    if($devStatus == 0) $(".cisco_status").html("<span class=\"notify-bubble bg-green bg-green-ball\"></span>  Active"); 
                                    else  $(".cisco_status").html("<span class=\"notify-bubble bg-grey bg-grey-ball\"></span>  Offline"); 
                                    
                                }
                                if($scheduleData.length > 0){
                                    for(var $i=0; $scheduleData.length > $i; $i++){
                                        var $operPwStr = "";
                                        $patchPanel = (($scheduleData[$i]["PatchPanel"] != null) ? decodeURIComponent(($scheduleData[$i]["PatchPanel"]).replace(/\+/g, " ")) : "");
                                        
                                        //ProtName
                                        var $port = $scheduleData[$i]["Port"];
                                        var $cableID    =  $scheduleData[$i]["CableID"];
                                        $( "#sw_name_"+$devID).attr('title',$patchPanel).tooltip('fixTitle');
                                        $("#sw_name_"+$devID).attr("data-title",$patchPanel);
                                        if($cableID != null && $cableID != ""){
                                            $cableID = decodeURIComponent($cableID.replace(/\+/g, " "));
                                            var cableIDSplit = $cableID.split("/");
                                            $item[$port] = cableIDSplit[0];
                                        }
                                    }
                                }
                                if($logData.length > 0){ 
                                    for(var $i=0; $logData.length > $i; $i++){
                                        var $port = $logData[$i]["Port"];
                                        if($port != null){
                                            var $operState = "default.gif";
                                            if($logData[$i]["OperState"] == 1){
                                                $operState = "ok.gif";
                                                $( "#cisco_img_"+$devID+"_"+$port ).addClass("port-st-ok");
                                                $( "#cisco_img_"+$devID+"_"+$port ).removeClass("port-st-error");
                                                $( "#cisco_img_"+$devID+"_"+$port ).removeClass("port-st-default");
                                            }
                                            else if($logData[$i]["OperState"] == 0){
                                                $operState = "error.gif";
                                                $( "#cisco_img_"+$devID+"_"+$port ).addClass("port-st-error");
                                                $( "#cisco_img_"+$devID+"_"+$port ).removeClass("port-st-ok");
                                                $( "#cisco_img_"+$devID+"_"+$port ).removeClass("port-st-default");
                                            }
                                            else{
                                                $( "#cisco_img_"+$devID+"_"+$port ).addClass("port-st-default");
                                                $( "#cisco_img_"+$devID+"_"+$port ).removeClass("port-st-ok");
                                                $( "#cisco_img_"+$devID+"_"+$port ).removeClass("port-st-error");
                                            }
                                            if($( "#cisco_img_"+$devID+"_"+$port ).length > 0){
                                                $( "#cisco_img_"+$devID+"_"+$port ).attr("src","img/port-"+$operState);

                                                //power
                                                var $operPower  =  $logData[$i]["OperPower"];
                                                
                                                $( "#port_"+$devID+"_"+$port ).removeAttr("title");
                                                $( "#port_"+$devID+"_"+$port ).removeAttr("data-original-title");
                                                $( "#port_"+$devID+"_"+$port ).removeAttr("data-title");
                                                //console.log($item[$port]);
                                                if(typeof $item[$port] != "undefined") {
                                                    $( "#port_"+$devID+"_"+$port ).attr('title',$item[$port]+"/"+$operPower+"w").tooltip('fixTitle');
                                                    $( "#port_"+$devID+"_"+$port ).attr("data-title",$item[$port]+"/"+$operPower+"w"); 
                                                }
                                                else { 
                                                    $( "#port_"+$devID+"_"+$port ).attr('title',$operPower+"w").tooltip('fixTitle');
                                                    $( "#port_"+$devID+"_"+$port ).attr("data-title",$operPower+"w"); 
                                                }
                                            }
                                        }
                                    }
                                }
                                else{
                                    
                                    $( ".port-st-ok").attr("src","img/port-default.gif");
                                    $( ".port-st-error").attr("src","img/port-default.gif");
                                    $( ".port img").removeClass("port-st-ok");
                                    $( ".port img").removeClass("port-st-error");
                                    $( ".port img").removeClass("port-st-default");
                                    var $pCount = 24;
                                    for(var $i = 1;$pCount>= $i;$i++){
                                        //console.log($("#port_"+$devID+"_"+$i).html());
                                        if(typeof $item[$i] != "undefined") {
                                            $( "#port_"+$devID+"_"+$i ).attr('title',$item[$i]).tooltip('fixTitle');
                                            $( "#port_"+$devID+"_"+$i ).attr("data-title",$item[$i]); 
                                        }
                                        else {
                                            var $portTitle = $("#port_"+$devID+"_"+$i).attr("data-title");
                                            if(typeof $portTitle != "undefined"){
                                                $( "#port_"+$devID+"_"+$i ).attr('title',"").tooltip('fixTitle');
                                                $( "#port_"+$devID+"_"+$i ).tooltip("destroy");
                                                $( "#port_"+$devID+"_"+$i ).attr("data-title",""); 
                                            }
                                        }
                                        /*var $portTitle = $("#port_"+$devID+"_"+$i).attr("data-title");
                                        console.log($portTitle);
                                        if(typeof $portTitle != "undefined"){
                                            var $splitTitle = $portTitle.split("/");
                                            console.log($splitTitle);
                                            $("#port_"+$devID+"_"+$i).attr("data-title",$splitTitle[0]);
                                            $( "#port_"+$devID+"_"+$i ).attr('title',$splitTitle[0]).tooltip('fixTitle');
                                        }*/
                                    }
                                }
                                //$(".port_tooltip").data('tooltip', false);
                                //$(".port_tooltip").tooltip({html:true});
                            }
                            $("#resultInfo").show();
                        }
                        else {
                            $("#resultContainer").html("<h5 class='text-center bold m-t-20 hint-text'> No switch available.</h5>");
                            $("#divPullScheduleDetails").html("");
                            $("#resultInfo").hide();
                        }

                        $GraphEvn.blinkOffPorts(true);
                    },
                    complete: function () {
                       if($type == 1) __hideLoadingAnimation();
                       
                    },
                    error: function (r) {
                       if($type == 1) __hideLoadingAnimation();
                        console.log(r);
                    }
                });
        },
        fileUpload : function(){
            // Call the fileupload widget and set some parameters
            $('#fileupload').fileupload({
                url: 'fileUpload.php',
                singleFileUploads: true,
                dataType: 'json',
                acceptFileTypes: /(\.|\/)(xls|xlsx)$/i,
                done: function (e, data) {
                    $.each(data.result.files, function (index, file) {
                        var imageName = file.name;

                        if(file.error){
                            $createAlert({status : "fail",title : "Failed",text :file.error});
                            $('#uploadConfirmation').modal("hide");
                        }
                        else{
                            $('#uploadConfirmation .up-filename').html(imageName);
                            $("#uploadConfirmation .upload-progress").addClass("hide");
                            $("#uploadConfirmation .update-conf").removeClass("hide");
                            $("#hdnUploadFileName").val(imageName);
                        }
                    });
                },
                progressall: function (e, data) {
                    // Update the progress bar while files are being uploaded
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $('#uploadConfirmation .up-pre-span').html(progress);
                }
            }).bind('fileuploadadd', function (e, data) {
                var acceptFileTypes = /(\.|\/)(xls|xlsx)$/i;
				var fileName = data.originalFiles[0]['name'];
				var splitName = fileName.split(".");
				var ext = splitName[splitName.length - 1];
				ext = ext.toLowerCase();
                if( ext != 'xls' && ext !='xlsx' ) {
					console.log("test");
                    $createAlert({status : "fail",title : "Failed",text :"Not an accepted file type"});
                    return false;
                }
                else{
                    $('#uploadConfirmation').modal("show");
                    $("#uploadConfirmation .update-conf").addClass("hide");
                    $("#uploadConfirmation .upload-progress").removeClass("hide");

                }
            });
        },
        saveExcelDetails : function(){
            var $fileName = $("#hdnUploadFileName").val();
            if($fileName.trim() == ""){
                $createAlert({status : "fail",title : "Failed",text :"Please select switch schedule excel sheet. "});
                return false;
            }
            var $data = {
                    c: 'Settings',
                    a: 'updateSwitchSchedule',
                    fileName: encodeURI($fileName)
                };
                $.ajax({
                    type: 'POST',
                    url: 'ajax.php',
                    data: $data,
                    dataType: 'json',
                    beforeSend: function () {
                       __showLoadingAnimation();
                    },
                    success: function (res) {
                        if(res.Status == 1){
                            $('#uploadConfirmation').modal("hide");
                            $createAlert({status : "success",title : "Success",text :"successfully updated records"});

                            //Refresh switch
                             var $filter = $("#devfilterContainer .active").attr("data-type");
                            $GraphEvn.getDeviceData($filter,1);
                        }
                        else{
                            $('#uploadConfirmation').modal("hide");
                            $createAlert({status : "fail",title : "Failed",text :"Failed to updated records"});
                        }
                    },
                    complete: function () {
                        __hideLoadingAnimation();
                    },
                    error: function (r) {
                       __hideLoadingAnimation();
                        $createAlert({status : "fail",title : "Failed",text :r});
                    }
                });
        },
        hideUploadConf : function(){
            $('#uploadConfirmation').modal("hide");
            var $fileName = $("#hdnUploadFileName").val();
            if($fileName.trim() == ""){ return false; }
            var $data = {
                c: 'Settings',
                a: 'removeExistFile',
                fileName: encodeURI($fileName)
            };
            $.ajax({
                type: 'POST',
                url: 'ajax.php',
                data: $data,
                dataType: 'json',
                success: function (res) {

                },
                error: function (r) {
                    console.log(r);
                }
            });
        },
        editSwitchName:function($id){
            var $name = $("#sw_name_"+$id+" .sw-name").html();
            $("#sw_name_"+$id).addClass("hide");
            $("#sw_edit_"+$id).removeClass("hide");
            $("#sw_edit_"+$id).find(".sw-edit-input").val($name);
        },
        saveName:function($id){
             var $name = $("#sw_edit_"+$id).find(".sw-edit-input").val();
             if($name.trim() == ""){
                $createAlert({status : "fail",title : "Failed",text :"Please enter name. "});
                return false;
            }
            var $data = {
                c: 'Settings',
                a: 'saveSwitchName',
                name: $name.trim(),
                deviceID:$id
            };
            $.ajax({
                type: 'POST',
                url: 'ajax.php',
                data: $data,
                dataType: 'json',
                beforeSend: function () {
                   __showLoadingAnimation();
                },
                success: function (res) {
                    if(res.Status){
                        $createAlert({status : "success",title : "Success",text :"successfully updated name"});
                        $("#sw_name_"+$id+" .sw-name").html($name)
                        //$("#sw_name_"+$id).html($name + ' <span onclick="$GraphEvn.editSwitchName('+$id+');"><i class="fa fa-pencil hint-text text-master" aria-hidden="true"></i></span>');
                        $GraphEvn.cancelEdit($id);
                    }
                    else{
                        $createAlert({status : "fail",title : "Failed",text :"Failed to updated name"});
                    }
                },
                complete: function () {
                    __hideLoadingAnimation();
                },
                error: function (r) {
                   __hideLoadingAnimation();
                    $createAlert({status : "fail",title : "Failed",text :r});
                }
            });
        },
        cancelEdit:function($id){
            $("#sw_edit_"+$id).addClass("hide");
            $("#sw_name_"+$id).removeClass("hide");
        },
        blinkOffPorts: function(isInit){
                        if( isInit  == true ) $GraphEvn.blinkCount = 0;
                        $(".port-st-error").each(function(){
                                        var currentURL = $(this).attr("src");
//console.log(currentURL )
                                        if( currentURL == "img/port-error.gif" ){
                                                        $(this).attr("src","img/port-error-blink.gif");
                                        }
                                        else {
                                                        $(this).attr("src","img/port-error.gif");
                                        }
                        });

                        $GraphEvn.blinkCount++;

                        if( $GraphEvn.blinkCount < 10 ){
setTimeout(function(){
$GraphEvn.blinkOffPorts(false);
}, 1000);	

}
        }
    };
</script>

<script type="text/x-tmpl" id="tmpl-graph">
    {% var ports = []; %}
    {% for ( var i = 1; i < 49; i++) { %}
         {% var portStatus = "default"; %}
         {% var PortName = ""; %}
         {% if (i in o.data) { %}
            {% portStatus = o.data[i]; %}
        {% } %}
        {% if ((i+"_cable") in o.data) { %}
            {% PortName = o.data[(i+"_cable")]; %}
        {% } %}
        {% ports[i] = portStatus; %}
        {% ports[i+"_Name"] = PortName; %}
    {% } %}
    <div class="switch-wrapper m-b-15" id="sw_{%=o.devId %}">
        <h5 class="m-b-0 m-t-0 bold fs-16 port_tooltip" id="sw_name_{%=o.devId %}" data-toggle="tooltip" data-placement="top" data-title="{%=o.patchPanel %}"><span class="sw-name">{%=o.name %}</span>
            <span style="display:none;" onclick="$GraphEvn.editSwitchName({%=o.devId %});"><i class="fa fa-pencil hint-text text-master" aria-hidden="true"></i></span>
             <span class="cisco_status text-white fs-12 "></span>         
        </h5>
        <div class="row hide" id="sw_edit_{%=o.devId %}">
            <div class="col-sm-10 col-xs-12 col-md-4 col-lg-4 col-xl-4">
                <input type="text" class="full-width sw-edit-input form-control" value="">
            </div>
            <div class="col-sm-2 col-xs-2 col-md-2 col-lg-2 col-xl-2 p-l-0 m-t-5">
                <input type="button" class="btn btn-default btn-xs pull-left m-r-10" value="Save" onclick="return $GraphEvn.saveName({%=o.devId %})"/>
                <input type="button" class="btn btn-default  btn-xs pull-left" value="Close" onclick="return $GraphEvn.cancelEdit({%=o.devId %})"/>
            </div>
        </div>
        <!--div class="sw-log-time port_tooltip" id="swLogTime_{%=o.devId %}"  data-toggle="tooltip" data-placement="left" data-title="Last updated on">
            <span>{%=o.time %}</span>
        </div-->
        <span style="margin-left:-50px" class="sw-log-power-total port_tooltip {% if(o.operPower == '' || o.pwStatus == 0) { %} hide {% } %}" id="swPowerTotal_{%=o.devId %}"  data-toggle="tooltip" data-placement="bottom" data-title="Oper power total">{%=o.operPower %}w</span>

        <table class="m-t-10" style="width:100%">
            <tr>
                {% var rowInc = 1; %}
                {% var portNumber = 0; %}

                {% for (var i = 0; ( i < 2 && rowInc <= 2); i++) { %}
                    <td>
                        <table class="m-r-10">
                            {% for (var k = 0; ( k < 2); k++) { %}
                                <tr>
                                    {% var portCounter = 0; %}

                                    {% portCounter = portNumber;  %}
                                    {% for (var j = 0; ( j < 6); j++) {  %}
					{% var borderClass = "even-port-num"; %}
					{% if (portCounter  % 2 != 0) { %}
						{% borderClass = "odd-port-num";%}
					{% } %}
                                        {% if (k == 0){  %}
                                            {% portCounter = j == 0 ? portCounter + 1 : portCounter + 2;  %}
                                        {% } else { %}
                                            {% portCounter = portCounter + 2;  } %}
                                        <td>

                                            <div class="port port-row-{%=k %} port_tooltip" id="port_{%=o.devId %}_{%=portCounter%}" data-toggle="tooltip" data-placement="top" data-title="{%=ports[(portCounter+'_Name')]%}">
                                                {% if (portCounter  % 2 != 0) { %}<div class="port-number port-number-{%=ports[portCounter] %}>">{%=portCounter %}</div> {% } %}
                                                <img class="{%=borderClass%} port-n-{%=portCounter%} port-st-{%=ports[portCounter] %}" src="img/port-{%=ports[portCounter] %}.gif"  id="cisco_img_{%=o.devId %}_{%=portCounter%}">
						{% if(k == 1) { %}<div class="yellow-time-line"></div>{% } %}
						{% if (portCounter  % 2 == 0) { %}<div class="port-number port-number-{%=ports[portCounter] %}>">{%=portCounter %}</div> {% } %}

                                            </div>
                                        </td>
                                    {% } %}
                                </tr>
                                {% if(k == 1) { %}
                                    {% portNumber = portCounter; %}
                                {% } %}
                            {% } %}
                        </table>
                    </td>
                {% } %}
            </tr>
        </table> 
    </div>
</script>
<script type="text/x-tmpl" id="tmpl-schedule">
    {% if ( o.data[0]["Item"] != null ) { %}

    <h5 id="lblPullScheduleDetails" class="panel-title  bold fs-16 upper">Pull schedule</h5>
    <div class="table-responsive table-bordered">
        <table class="table m-b-0">
            <thead>
                <tr>
                    <th>Item</th>
                    <th>Floor</th>
                    <th>Room</th>
                    <th>Rack</th>
                    <th>Patch Panel</th>
                    <th>Power</th>
                    <th>Engine Name</th>
                    <th>Engine Port</th>
                    <th>Cable ID</th>
                    <th>Cable Type/Color</th>
                    <th>Node Serial Num</th>
                    <th>Name</th>
                    <th>Vendor</th>
                    <th>Model</th>
                    <th>Type</th>
                    <th>HW Ver</th>
                    <th>SW Ver</th>
                    <th>Status</th>
                    <th>Talk Port</th>
                    <th>Non-Talk Port</th>
                    <th>Location ID</th>
                    <th>Location Name</th>
                    <th>Location Path</th>
                    <th>Discovery Count</th>
                    <th>Too Many Discoveries</th>
                    <th>Pod ID</th>
                    <th>Pod Status</th>
                </tr>
            </thead>
            <tbody>
                {% var portNumber = o.data; %}
                {% for (var k = 0; ( k <o.data.length); k++) { %}
                    {% if ( o.data[k]["Item"] != null ) { %}
                    <tr>
                        <td>{%=o.data[k]["Item"]%}</td>
                        <td>{%=o.data[k]["Floor"]%}</td>
                        <td>{%=o.data[k]["Room"]%}</td>
                        <td>{%=o.data[k]["Rack"]%}</td>
                        <td>{%=o.data[k]["PatchPanel"]%}</td>
                        <td>{%=o.data[k]["Power"]%}</td>
                        <td>{%=o.data[k]["device_name"]%}</td>
                        <td>{%=o.data[k]["EnginePort"]%}</td>
                        <td>{%=o.data[k]["CableID"]%}</td>
                        <td>{%=o.data[k]["CableType"]%}/{%=o.data[k]["Color"]%}</td>
                        <td>{%=o.data[k]["NodeSerialNum"]%}</td>
                        <td>{%=o.data[k]["Name"]%}</td>
                        <td>{%=o.data[k]["Vendor"]%}</td>
                        <td>{%=o.data[k]["Model"]%}</td>
                        <td>{%=o.data[k]["Type"]%}</td>
                        <td>{%=o.data[k]["HWVer"]%}</td>
                        <td>{%=o.data[k]["SWVer"]%}</td>
                        <td>{%=o.data[k]["PS_Status"]%}</td>
                        <td>{%=o.data[k]["TalkerPort"]%}</td>
                        <td>{%=o.data[k]["NonTalkerPort"]%}</td>
                        <td>{%=o.data[k]["LocationID"]%}</td>
                        <td>{%=o.data[k]["LocationName"]%}</td>
                        <td>{%=o.data[k]["LocationPath"]%}</td>
                        <td>{% if(o.data[k]["DiscoveryCount"] != 0) { %}{%=o.data[k]["DiscoveryCount"]%} {% } %}</td>
                        <td>{%=o.data[k]["TooManyDiscoveries"]%}</td>
                        <td>{%=o.data[k]["PodID"]%}</td>
                        <td>{%=o.data[k]["PodStatus"]%}</td>
                    </tr>
                    {% } %}
                {% } %}
            </tbody>
        </table>
    </div>
{% } %}
</script>
