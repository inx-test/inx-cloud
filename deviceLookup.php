<?php
   include_once("config.php");
   include_once("_classes/curl.php");
      
    $action = $_REQUEST["action"]; // Action 
    $source = 3;//$_REQUEST["source"]; //Source Type //1 = device;// 2 = galileo;// = supervisor
       
    
    if($action == "PingDevice"){
        $deviceIP = $_REQUEST["DeviceIP"];
        $serialNo = $_REQUEST["serialNo"]; // Device Serial Number
        //die($_REQUEST["data"]);
        $result = invokeWebMethod(getWebURL($deviceIP, $_REQUEST));
        $db = new DBC();
        if($result == NULL || $result == ""){
            $sql = "UPDATE pmi_device SET status = 0 WHERE id =".$_REQUEST["id"];
            $db->query($sql);
        }
        else{
            try {
                $resultArr = json_decode($result,TRUE);
                //print_r($resultArr);
                if($resultArr["responseData"]["PingStatus"] == 1){
                    $sql = "UPDATE pmi_device SET status = 1 WHERE id =".$_REQUEST["id"];
                    $db->query($sql);
                }
                else{
                    $sql = "UPDATE pmi_device SET status = 0 WHERE id =".$_REQUEST["id"];
                    $db->query($sql);
                }
            } catch (Exception $ex) {
                $sql = "UPDATE pmi_device SET status = 0 WHERE id =".$_REQUEST["id"];
                $db->query($sql);
            }
            
        }
        die($result);
    }
    else{
        if(!isset($_SESSION["activeDeviceID"]) || $_SESSION["activeDeviceID"] == "" || $_SESSION["activeDeviceID"] == "0"){
            $result["status"] = 0;
            die(json_encode($result)); 
        }
        else if($action == "GetPanelParameters" ){
            $_REQUEST["serialNo"] = $_SESSION["activeDeviceSerialNo"];
            die(invokeWebMethod(getWebURL($_SESSION["activeDeviceIPAddress"], $_REQUEST)));
        }
        else if($action == "SavePanelParams"){
            $_REQUEST["serialNo"] = $_SESSION["activeDeviceSerialNo"];
            $_REQUEST["DEVICEID"] =  $_SESSION["activeDeviceID"];
            die(invokeWebMethod(getWebURL($_SESSION["activeDeviceIPAddress"], $_REQUEST)));
        }            
        else if($action == "SetBanksStatus"){
            $_REQUEST["serialNo"] = $_SESSION["activeDeviceSerialNo"];
            die(invokeWebMethod(getWebURL($_SESSION["activeDeviceIPAddress"], $_REQUEST)));
        }
        else if($action == "Get_HB"){
            die(invokeWebMethod(getWebURL($_SESSION["activeDeviceIPAddress"], $_REQUEST)));
        }
        else if($action == "LockPanelParams"){            
            die(invokeWebMethod(getWebURL($_SESSION["activeDeviceIPAddress"], $_REQUEST)));
        }
    }
        
?>

