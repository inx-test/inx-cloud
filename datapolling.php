<?php
require_once 'config.php';
require_once "Infrastructure/DataPollingProcess/DataPollingWorker.php";
require_once 'Infrastructure/DataPollingProcess/DeviceDataFetch.php';

echo DB_HOST;

print_r($argv);
$commands=array();
for($i=1;$i < sizeof($argv); $i++)
{
	$cmd=$argv[$i];
	$commands[$cmd]=1;
        $commands["delete_span"] = LOG_DATA_DELETE_SPAN;
}

$dev = new DeviceDataFetch();
$dev->getData();
$dc = $dev->getDeviceCount();
echo "Num devices=".$dc;

if ( $dc == 0 ) exit;

$workerCount = ( $dc > DATAWORKERCOUNT ) ? DATAWORKERCOUNT : $dc;

// Worker pool
$workers = array();
echo "dwc=".$workerCount."\n";
// Initialize and start the workers
for ($i=0; $i<$workerCount; $i++) {
		$workers[$i] = new DataPoolingWorker($i+1,$dev,$commands);
		//$workers[$i]->setCommand(GET_HB);
		//$workers[$i]->setCommand(GET_LOG);
		//$workers[$i]->start();
		$workers[$i]->start();
}

// wait for workers to complete

for ($i=0; $i < $workerCount;$i++) {
	$workers[$i]->join();
}
?>
