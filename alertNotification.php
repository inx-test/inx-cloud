<?php
require_once 'Infrastructure/EmailProcess/NotificationWorker.php';
require_once 'Infrastructure/EmailProcess/AlertDataFetch.php';
require_once 'config.php';


$alert = new AlertDataFetch();
$alertsCt=$alert->loadAlerts();
$SMTP = $alert->smtpInfo[0];
print_r($SMTP);

echo "$alertsCt alerts to process\n";

// Worker pool
$workers = array();

if($alertsCt > 0 && count($SMTP)>0 && $SMTP['alert_notify'] == 1){
    
    $nworkers = ( $alertsCt > NOTIFICATIONWORKERCOUNT ) ? NOTIFICATIONWORKERCOUNT : $alertsCt;
    echo "Starting $nworkers worker(s) to process alert emails\n";
    // Initialize and start the workers
    for ($i=0; $i < $nworkers; $i++) {
        $workers[$i] = new NotificationWorker($i+1,$alert,$SMTP);
        //print_r($workers[$i]);
        $workers[$i]->run();
        //print_r($workers[$i]);
    }

	// wait for workers to complete
	for ($i=0; $i < $nworkers;$i++) {
		$workers[$i]->join();
	}    
}


?>
