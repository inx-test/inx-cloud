var oldCharXaxisType = "%a"
var searchCriteria = [];
var somePlot = null;
var $modelArr = [];
var guageSettingsList = [];
var isChartPage = false;
var formattedGuageArray = [];
var timeFormat = 'HH:mm:ss';
var _HB_VTHD = 0, _HB_ATHD = 0, HB_VTHD_C = 0, _HB_ATHD_C = 0, _HB_PEAKV_HARM = 0, _HB_PEAKV_HARM_C = 0, _HB_PEAKA_HARM = 0, _HB_PEAKA_HARM_C = 0, _1st_HRM_C = 0;
var $legendsArr = ["Driver_A_KW","Driver_B_KW","Temperature","Motion"];
var $legendsIdentityArr = [{"Driver_A_KW":"KW","Driver_B_KW":"KW","Temp":"Temperature","Motion":"Motion"}];
var chartIdentityColor = {
    'DriverAKW':'rgba(222, 42, 3,0.9)',
    'DriverBKW':'rgba(84, 138, 255,0.9)',
    'Temperature':'rgba(47, 182, 180,0.9)',
    'Motion':'rgba(243, 32, 169,0.9)'
};
var chartIdentityLineColor = {
    'DriverAKW':'rgba(222, 42, 3,0.5)',
    'DriverBKW':'rgba(84, 138, 255,0.5)',
    'Temperature':'rgba(47, 182, 180,0.5)',
    'Motion':'rgba(243, 32, 169,0.5)'
};
var chartAreaIdentityColor = {
    'DriverAKW':'rgba(222, 42, 3,0.5)',
    'DriverBKW':'rgba(84, 138, 255,0.5)',
    'Temperature':'rgba(47, 182, 180,0.5)',
    'Motion':'rgba(243, 32, 169,0.5)'
};
var GLOBAL_TIME_ZONES = ['GMT', 'CET', 'EET', 'AST', 'GST', 'MVT',
    'BST', 'CXT', 'HKT', 'JST', 'AET', 'NCT',
    'NZST', 'EGT', 'FNT', 'GFT', 'FKT', 'EST',
    'CST', 'MST', 'PST', 'AKST', 'HAST', 'NUT', 'AoE'
];
$('#portlet-advance').portlet({
    onRefresh: function () {
        setTimeout(function () {
// Throw any error you encounter while refreshing
            $('#portlet-advance').portlet({
                error: "Something went terribly wrong. Just keep calm and carry on!"
            });
        }, 2000);
    }
});
// Toggle Popover 
$(function () {
    $('[data-toggle="popover"]').popover({
        html: true,
        content: function () {
            return $(this).find(".popovercontent").html();
        }
    });
});
var __updateDeviceName = function () {
    var dn = $("#__inpDeviceName").val();
    $("#device-name").html(dn);
    $('[data-toggle="popover"]').popover('hide');
}

function __getBaseUrl() {
    var re = new RegExp(/^.*\//);
    return re.exec(window.location.href);
}

var initAreaChart = function ($rangeType) {
// Load chart data
//console.log(1121212121);
    if ($rangeType == 1) {
        searchCriteria = [];
        $(".back-to-chart").addClass("hide");
    }
    else if ($rangeType == 2) {
        $(".back-to-chart").removeClass("hide");
    }
    else if ($rangeType == 3) {
//console.log(searchCriteria.length);
        searchCriteria.splice(searchCriteria.length - 1, 1);
        if (searchCriteria.length == 0) {
            $(".back-to-chart").addClass("hide");
            $rangeType = 1;
        }
    }
    var data = __getChartJSON();
    //console.log(data.nvd3.chartData);
    //if(data.nvd3.chartData[0].values.length == 0) $("#nvd3-area svg").html("");
    (function () {
        var $margin = {left: 50, bottom: 50}
        if (charXaxisType == "%m/%d/%Y %H:%M") {
            $margin = {left: 50, bottom: 65};
        }
        // Renders a stacked area chart
        nv.addGraph(function () {
            var chart = nv.models.stackedAreaChart()
                    .margin($margin)
                    .color([
                        $.Pages.getColor('success'),
                        $.Pages.getColor('danger'),
                        $.Pages.getColor('primary'),
                        $.Pages.getColor('complete'),
                    ])
                    .x(function (d) {
                        return d[0]
                    }) //We can modify the data accessor functions...
                    .y(function (d) {
                        console.log(d[1]);
                        return d[1];
                    }) //...in case your data is formatted differently.
                    .useInteractiveGuideline(true) //Tooltips which show all data points. Very nice!
                    .rightAlignYAxis(false) //Let's move the y-axis to the right side.
                    .transitionDuration(500)
                    .showControls(false) //Allow user to choose 'Stacked', 'Stream', 'Expanded' mode.
                    .clipEdge(true);
            chart.xAxis
                    .tickFormat(function (d) {
                        return d3.time.format(charXaxisType)(new Date(d));
                    });
            chart.yAxis
                    .tickFormat(d3.format(',.2f'));
            chart.legend.margin({
                top: 30
            });
            d3.select('#nvd3-area svg')
                    //.datum(data)
                    .datum(data.nvd3.chartData)
                    .call(chart);
            nv.utils.windowResize(function () {
                chart.update();
            });
            $('#nvd3-area').data('chart', chart);
            return chart;
        }, function () {
            d3.selectAll(".nv-x text").attr("transform", function (d) {
                return "translate(" + this.getBBox().height * -2 + "," + this.getBBox().height + ")rotate(-45)";
            });
            /*d3.selectAll(".nv-point").on('click',function(e){
             if(charXaxisType != "%H:%M."){
             var startDate = e[0];
             var endDate = "";
             var unitsTime = parseInt($("#timeRange").val());
             if(searchCriteria.length == 0 && unitsTime != 1){
             var hrs = new Date(startDate).getHours()+ parseInt(unitsTime);
             hrs = hrs <= 23 ? hrs : 23;
             endDate = new Date(startDate).setHours(hrs);
             unitsTime = 1;
             }
             else {
             endDate = new Date(startDate);
             unitsTime = 0;
             }
             searchCriteria.push({startDate : startDate,endDate : endDate,time : unitsTime});
             initAreaChart(2); 
             }
             });*/
        });
    })();
}

var initLineChart = function ($rangeType) {
    if ($rangeType == 1) {
        searchCriteria = [];
        $(".back-to-chart").addClass("hide");
    }
    else if ($rangeType == 2) {
        $(".back-to-chart").removeClass("hide");
    }
    else if ($rangeType == 3) {
        searchCriteria.splice(searchCriteria.length - 1, 1);
        if (searchCriteria.length == 0) {
            $(".back-to-chart").addClass("hide");
            $rangeType = 1;
        }
    }
    var data = __getChartJSON();
    if (data.nvd3.chartData[0].values.length == 0)
        $("#nvd3-line svg").html("");
    (function () {

        var $margin = {bottom: 50}
        if (charXaxisType == "%m/%d/%Y %H:%M") {
            $margin = {bottom: 65};
        }
        nv.addGraph(function () {
            var chart = nv.models.lineChart()
                    .x(function (d) {
                        return d[0]
                    })
                    .y(function (d) {
                        return d[1];
                    })
                    .margin($margin)
                    .color([
                        $.Pages.getColor('success'),
                        $.Pages.getColor('danger'),
                        $.Pages.getColor('primary'),
                        $.Pages.getColor('complete'),
                    ])
                    .useInteractiveGuideline(true);
            chart.xAxis
                    .tickFormat(function (d) {
                        return d3.time.format(charXaxisType)(new Date(d));
                    });
            /*.tickValues(data.customTicksX)
             .tickFormat(function(d) { 
             return d3.time.format(charXaxisType)(new Date(d));
             });*/

            chart.yAxis.tickFormat(d3.format(',.2f'));
            d3.select('#nvd3-line svg')
                    .datum(data.nvd3.chartData)
                    .transition().duration(500)
                    .call(chart);
            nv.utils.windowResize(chart.update);
            $('#nvd3-line').data('chart', chart);
            return chart;
        }, function () {
            /*d3.selectAll(".nv-x text").attr("transform", function(d) {
             return "translate(" + this.getBBox().height * -2 + "," + this.getBBox().height + ")rotate(-45)";
             });*/
            /*d3.selectAll(".nv-point").on('click',function(e){
             if(charXaxisType != "%H:%M."){
             var startDate = e[0];
             var endDate = "";
             var unitsTime = parseInt($("#timeRange").val());
             if(searchCriteria.length == 0 && unitsTime != 1){
             var hrs = new Date(startDate).getHours()+ parseInt(unitsTime);
             hrs = hrs <= 23 ? hrs : 23;
             endDate = new Date(startDate).setHours(hrs);
             unitsTime = 1;
             }
             else {
             endDate = new Date(startDate);
             unitsTime = 0;
             }
             searchCriteria.push({startDate : startDate,endDate : endDate,time : unitsTime});
             initLineChart(2); 
             }
             });*/
        });
    })();
}

var initScopeChart = function () {
    var dataset = [];
    var colorArr = [];
    // var test = $scopeRecord.IdealPh1V;
    var Ph1Max = typeof $scopeRecord.ActualPh1A != "undefined" ? Math.max.apply(Math, $scopeRecord.ActualPh1V.map(function (i) {
        return i[1];
    })) : 208;
    //console.log(Ph1Max);

    if (Ph1Max <= 240) {
        //console.log("1111");
        colorArr["ph1VA"] = "rgb(0, 0, 0)";
        colorArr["ph2VA"] = "rgb(255, 0, 0)";
        colorArr["ph3VA"] = "rgb(46, 46, 254)";
    }
    else {
        //console.log("2222");
        colorArr["ph1VA"] = "rgb(139,69,19)";
        colorArr["ph2VA"] = "rgb(255,165,0)";
        colorArr["ph3VA"] = "rgb(255,255,0)";
    }

    colorArr["ph1VI"] = "rgb(180, 0, 255)";
    colorArr["ph2VI"] = "rgb(0, 211, 255)";
    colorArr["ph3VI"] = "rgb(3, 153, 158)";
    colorArr["ph1AA"] = "rgb(165, 223, 0)";
    colorArr["ph2AA"] = "rgb(254, 46, 247)";
    colorArr["ph3AA"] = "rgb(255, 128, 0)";
    colorArr["ph1AI"] = "rgb(181, 0, 8)";
    colorArr["ph2AI"] = "rgb(46, 254, 126)";
    colorArr["ph3AI"] = "rgb(0, 255, 240)";
    colorArr["ph1P"] = "rgb(88, 211, 247)";
    colorArr["ph2P"] = "rgb(250, 88, 244)";
    colorArr["ph3P"] = "rgb(247, 129, 129)";
    ///console.log(colorArr);
    var vMinArray = [0, 0, 0, 0, 0, 0];
    var vMaxArray = [0, 0, 0, 0, 0, 0];
    var iMinArray = [0, 0, 0, 0, 0, 0];
    var iMaxArray = [0, 0, 0, 0, 0, 0];
    /*if($("#checkShowIdeal").is(':checked')){
     $("#li-Actual").removeClass("hide");
     $("#li-KVA").addClass("hide");
     if(typeof $scopeRecord.IdealPh1A != "undefined"){
     if($("#checkShowph1").is(':checked')){
     dataset.push({
     label: "Ideal Ph1 A",
     data: FormatedArray($scopeRecord.IdealPh1A,128) ,
     yaxis: 2,
     color: colorArr.ph1AI,
     lines: {show: true}
     },
     {
     label: "Ideal Ph1 V",
     data: FormatedArray($scopeRecord.IdealPh1V,128),
     yaxis: 1,
     color: colorArr.ph1VI,
     lines: {show: true}
     });
     
     $("#spIdealph1V").css("background",colorArr.ph1VI);
     $("#spIdealph1A").css("background",colorArr.ph1AI);
     //console.log(colorArr.ph1AI);
     
     $("#lblPh1V").removeClass("hide");
     $("#lblPh1A").removeClass("hide");
     $("#spIdealph1V").removeClass("hide");
     $("#spIdealph1A").removeClass("hide");
     }
     else{
     $("#lblPh1V").addClass("hide");
     $("#lblPh1A").addClass("hide");
     $("#spIdealph1V").addClass("hide");
     $("#spIdealph1A").addClass("hide");
     }
     if($("#checkShowph2").is(':checked')){
     dataset.push({
     label: "Ideal Ph2 A",
     data: FormatedArray($scopeRecord.IdealPh2A,128),
     yaxis: 2,
     color: colorArr.ph2AI,
     lines: {show: true}
     },
     {
     label: "Ideal Ph1 V",
     data: FormatedArray($scopeRecord.IdealPh2V,128),
     yaxis: 1,
     color: colorArr.ph2VI,
     lines: {show: true}
     });
     
     $("#spIdealph2V").css("background",colorArr.ph2VI);
     $("#spIdealph2A").css("background",colorArr.ph2AI);
     
     $("#lblPh2V").removeClass("hide");
     $("#lblPh2A").removeClass("hide");
     $("#spIdealph2V").removeClass("hide");
     $("#spIdealph2A").removeClass("hide");
     }
     else{
     $("#lblPh2V").addClass("hide");
     $("#lblPh2A").addClass("hide");
     $("#spIdealph2V").addClass("hide");
     $("#spIdealph2A").addClass("hide");
     }
     if($("#checkShowph3").is(':checked')){
     dataset.push({
     label: "Ideal Ph3 A",
     data: FormatedArray($scopeRecord.IdealPh3A,128),
     yaxis: 2,
     color: colorArr.ph3AI,
     lines: {show: true}
     },
     {
     label: "Ideal Ph3 V",
     data: FormatedArray($scopeRecord.IdealPh3V,128),
     yaxis: 1,
     color: colorArr.ph3VI,
     lines: {show: true}
     });
     
     $("#spIdealph3V").css("background",colorArr.ph3VI);
     $("#spIdealph3A").css("background",colorArr.ph3AI);
     
     $("#lblPh3V").removeClass("hide");
     $("#lblPh3A").removeClass("hide");
     $("#spIdealph3V").removeClass("hide");
     $("#spIdealph3A").removeClass("hide");
     }
     else{
     $("#lblPh3V").addClass("hide");
     $("#lblPh3A").addClass("hide");
     $("#spIdealph3V").addClass("hide");
     $("#spIdealph3A").addClass("hide");
     }
     }
     }
     else{
     $("#spIdealph1V").addClass("hide");
     $("#spIdealph1A").addClass("hide");
     $("#spIdealph2V").addClass("hide");
     $("#spIdealph2A").addClass("hide");
     $("#spIdealph3V").addClass("hide");
     $("#spIdealph3A").addClass("hide");
     }*/

    if ($("#checkShowActual").is(':checked')) {
        $("#li-Actual").removeClass("hide");
        $("#li-KVA").addClass("hide");
        if (typeof $scopeRecord.ActualPh1A != "undefined") {
            if ($("#checkShowph1").is(':checked')) {

                vMinArray[0] = $scopeRecord.ActualPh1V.min();
                vMaxArray[0] = $scopeRecord.ActualPh1V.max();
                iMinArray[0] = $scopeRecord.ActualPh1A.min();
                iMaxArray[0] = $scopeRecord.ActualPh1A.max();
                //console.log(colorArr.ph1VA);
                dataset.push({
                    label: "Actual Ph1 A",
                    data: FormatedArray($scopeRecord.ActualPh1A, 128),
                    yaxis: 2,
                    color: colorArr.ph1AA,
                    lines: {show: true}
                },
                {
                    label: "Actual Ph1 V",
                    data: FormatedArray($scopeRecord.ActualPh1V, 128),
                    yaxis: 1,
                    color: colorArr.ph1VA,
                    lines: {show: true}
                });
                $("#spActph1V").css("background", colorArr.ph1VA);
                $("#spActph1A").css("background", colorArr.ph1AA);
                $("#lblPh1V").removeClass("hide");
                $("#lblPh1A").removeClass("hide");
                $("#spActph1V").removeClass("hide");
                $("#spActph1A").removeClass("hide");
            }
            else {
                $("#lblPh1V").addClass("hide");
                $("#lblPh1A").addClass("hide");
                $("#spActph1V").addClass("hide");
                $("#spActph1A").addClass("hide");
            }
            if ($("#checkShowph2").is(':checked')) {

                vMinArray[1] = $scopeRecord.ActualPh2V.min();
                vMaxArray[1] = $scopeRecord.ActualPh2V.max();
                iMinArray[1] = $scopeRecord.ActualPh2A.min();
                iMaxArray[1] = $scopeRecord.ActualPh2A.max();
                dataset.push({
                    label: "Actual Ph2 A",
                    data: FormatedArray($scopeRecord.ActualPh2A, 128),
                    yaxis: 2,
                    color: colorArr.ph2AA,
                    lines: {show: true}
                },
                {
                    label: "Actual Ph1 V",
                    data: FormatedArray($scopeRecord.ActualPh2V, 128),
                    yaxis: 1,
                    color: colorArr.ph2VA,
                    lines: {show: true}
                });
                $("#spActph2V").css("background", colorArr.ph2VA);
                $("#spActph2A").css("background", colorArr.ph2AA);
                $("#lblPh2V").removeClass("hide");
                $("#lblPh2A").removeClass("hide");
                $("#spActph2V").removeClass("hide");
                $("#spActph2A").removeClass("hide");
            }
            else {
                $("#lblPh2V").addClass("hide");
                $("#lblPh2A").addClass("hide");
                $("#spActph2V").addClass("hide");
                $("#spActph2A").addClass("hide");
            }
            if ($("#checkShowph3").is(':checked')) {
                vMinArray[2] = $scopeRecord.ActualPh3V.min();
                vMaxArray[2] = $scopeRecord.ActualPh3V.max();
                iMinArray[2] = $scopeRecord.ActualPh3A.min();
                iMaxArray[2] = $scopeRecord.ActualPh3A.max();
                dataset.push({
                    label: "Actual Ph3 A",
                    data: FormatedArray($scopeRecord.ActualPh3A, 128),
                    yaxis: 2,
                    color: colorArr.ph3AA,
                    lines: {show: true}
                },
                {
                    label: "Actual Ph3 V",
                    data: FormatedArray($scopeRecord.ActualPh3V, 128),
                    yaxis: 1,
                    color: colorArr.ph3VA,
                    lines: {show: true}
                });
                $("#spActph3V").css("background-color", colorArr.ph3VA);
                $("#spActph3A").css("background-color", colorArr.ph3AA);
                $("#lblPh3V").removeClass("hide");
                $("#lblPh3A").removeClass("hide");
                $("#spActph3V").removeClass("hide");
                $("#spActph3A").removeClass("hide");
            }
            else {
                $("#lblPh3V").addClass("hide");
                $("#lblPh3A").addClass("hide");
                $("#spActph3V").addClass("hide");
                $("#spActph3A").addClass("hide");
            }
        }
    }
    else {
        $("#spActph1V").addClass("hide");
        $("#spActph1A").addClass("hide");
        $("#spActph2V").addClass("hide");
        $("#spActph2A").addClass("hide");
        $("#spActph3V").addClass("hide");
        $("#spActph3A").addClass("hide");
    }

    if ($("#checkShowKVA").is(':checked')) {
        $("#li-KVA").removeClass("hide");
        $("#li-Actual").addClass("hide");
        if (typeof $scopeRecord.KVAPh1 != "undefined") {
            if ($("#checkShowph1").is(':checked')) {

                vMinArray[0] = $scopeRecord.KVAPh1.min();
                vMaxArray[0] = $scopeRecord.KVAPh1.max();
                dataset.push({
                    label: "KVA Ph1 P",
                    data: FormatedArray($scopeRecord.KVAPh1, 128),
                    yaxis: 1,
                    color: colorArr.ph1P,
                    lines: {show: true}
                });
                $("#spKVAph1V").css("background-color", colorArr.ph1P);
                $("#lblPh1P").removeClass("hide");
                $("#spKVAph1V").removeClass("hide");
            }
            else {
                $("#lblPh1P").addClass("hide");
                $("#spKVAph1V").addClass("hide");
            }
            if ($("#checkShowph2").is(':checked')) {

                vMinArray[1] = $scopeRecord.KVAPh2.min();
                vMaxArray[1] = $scopeRecord.KVAPh2.max();
                dataset.push({
                    label: "KVA Ph2 P",
                    data: FormatedArray($scopeRecord.KVAPh2, 128),
                    yaxis: 1,
                    color: colorArr.ph2P,
                    lines: {show: true}
                });
                $("#spKVAph2V").css("background-color", colorArr.ph2P);
                $("#spKVAph2V").removeClass("hide");
                $("#lblPh2P").removeClass("hide");
            }
            else {
                $("#lblPh2P").addClass("hide");
                $("#spKVAph2V").addClass("hide");
            }
            if ($("#checkShowph3").is(':checked')) {

                vMinArray[2] = $scopeRecord.KVAPh3.min();
                vMaxArray[2] = $scopeRecord.KVAPh3.max();
                dataset.push({
                    label: "KVA Ph3 P",
                    data: FormatedArray($scopeRecord.KVAPh3, 128),
                    yaxis: 1,
                    color: colorArr.ph3P,
                    lines: {show: true}
                });
                $("#spKVAph3V").css("background-color", colorArr.ph3P.toString());
                ///console.log(colorArr.ph3P);
                $("#lblPh3P").removeClass("hide");
                $("#spKVAph3V").removeClass("hide");
            }
            else {
                $("#lblPh3P").addClass("hide");
                $("#spKVAph3V").addClass("hide");
            }
        }
    }
    else {
        $("#lblPh1P").addClass("hide");
        $("#spKVAph1V").addClass("hide");
        $("#lblPh2P").addClass("hide");
        $("#spKVAph2V").addClass("hide");
        $("#lblPh3P").addClass("hide");
        $("#spKVAph3V").addClass("hide");
    }

    /*if(!$("#checkShowIdeal").is(':checked') && !$("#checkShowActual").is(':checked')){
     
     $("#lblPh1V").addClass("hide");
     $("#lblPh1A").addClass("hide");
     $("#lblPh2V").addClass("hide");
     $("#lblPh2A").addClass("hide");
     $("#lblPh3V").addClass("hide");
     $("#lblPh3A").addClass("hide");
     }*/
    if (dataset.length == 0) {

        if (typeof $scopeRecord.IdealPh1A == "undefined")
            $(".scope-nodata strong").html("No Data Available.");
        else
            $(".scope-nodata strong").html("Please select any criteria.");
        $(".scope-nodata").removeClass("hide");
        $(".scale-time").addClass("hide");
    }
    else {
        $(".scope-nodata").addClass("hide");
        $(".scale-time").addClass("hide");
        //$(".scale-time").removeClass("hide");
    }
    var minV = vMinArray.min();
    var maxV = vMaxArray.max();
    var minI = iMinArray.min();
    var maxI = iMaxArray.max();
    var ymaxV = Math.abs(minV) > Math.abs(maxV) ? Math.abs(minV) : Math.abs(maxV);
    var ymaxI = Math.abs(minI) > Math.abs(maxI) ? Math.abs(minI) : Math.abs(maxI);
    ymaxV = AutoRange(ymaxV);
    ymaxI = AutoRange(ymaxI);
    var options = {
        xaxis: {
            ticks: []
        },
        yaxes: [{
                position: "left",
                color: "black",
                axisLabel: "VOLTAGE",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 3,
                tickColor: "#C3C5C7",
                min: -1 * ymaxV,
                max: ymaxV
            }, {
                position: "right",
                color: "black",
                axisLabel: "CURRENT",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 3,
                tickColor: "#C3C5C7",
                min: -1 * ymaxI,
                max: ymaxI
            }],
        legend: {
            noColumns: 0,
            labelFormatter: function (label, series) {
                return "";
                // return '<a href="#" onClick="togglePlot('+series.idx+',this); return false;" class="text-black fs-12 bold">'+label+'</a>';
            },
            backgroundColor: "transparent",
            backgroundOpacity: 0.9,
            labelBoxBorderColor: "#000000",
            position: "ne"
        },
        grid: {
            hoverable: true,
            borderWidth: 1,
            mouseActiveRadius: 50,
            borderColor: {top: "#C3C5C7", left: "#C3C5C7"},
            backgroundColor: {
                colors: ["transparent", "transparent"]
            },
            markings: function (axes) {
                var markings = [];
                var xaxis = axes.xaxis;
                for (var i = 1; i < 18; i++) {
                    var $mainTick = i * 7.12;
                    var $minTickStart = $mainTick - 7.12;
                    markings.push({xaxis: {from: $mainTick, to: $mainTick}, color: "rgba(98, 98, 98, 0.3)"});
                    for (var j = 1; j < 4; j++) {
                        markings.push({xaxis: {from: $minTickStart + j * 1.778, to: $minTickStart + j * 1.778}, color: "rgba(98, 98, 98, 0.1)"});
                        if (i == 17) {
                            markings.push({xaxis: {from: $mainTick + j * 1.778, to: $mainTick + j * 1.778}, color: "rgba(98, 98, 98, 0.1)"});
                        }
                    }
                }
                return markings;
            },
            axisMargin: 20
        }
    };
    isChartPage = false;
    somePlot = $.plot($("#flot-Scope"), dataset, options);
    $("#flot-Scope").UseTooltip();
}

function togglePlot(seriesIdx, obj) {
    var someData = somePlot.getData();
    someData[seriesIdx].lines.show = !someData[seriesIdx].lines.show;
    somePlot.setData(someData);
    someData[seriesIdx].points.show = !someData[seriesIdx].points.show;
    somePlot.setData(someData);
    somePlot.draw();
    if ($(obj).hasClass("text-black-lighter")) {
        $(obj).addClass("text-black");
        $(obj).removeClass("text-black-lighter");
    }
    else {
        $(obj).addClass("text-black-lighter");
        $(obj).removeClass("text-black");
    }
}
var previousPoint = null,
        previousLabel = null;
$.fn.UseTooltip = function () {
    $(this).bind("plothover", function (event, pos, item) {
        if (item) {
            if ((previousLabel != item.series.label) || (previousPoint != item.dataIndex)) {
                previousPoint = item.dataIndex;
                previousLabel = item.series.label;
                $("#tooltip").remove();
                /*var x = item.datapoint[0];
                 var y = item.datapoint[1];
                 
                 var color = item.series.color;*/
                var x = item.series.data[previousPoint][0];
                var y = item.datapoint[1];
                var color = item.series.color;
                var appendString = "";
                if (isChartPage) {
                    appendString = " for <strong>" + $.plot.formatDate(new Date(x), oldCharXaxisType) + "</strong>";
                }


                showTooltip(item.pageX, item.pageY, color, "<strong>" + item.series.label + "</strong>" + " : <strong>" + y + "</strong> " + appendString);
            }
        } else {
            $("#tooltip").remove();
            previousPoint = null;
        }
    });
};
function showTooltip(x, y, color, contents) {
    $('<div id="tooltip">' + contents + '</div>').css({
        position: 'absolute',
        display: 'none',
        top: y - 40,
        left: x - 120,
        border: '2px solid ' + color,
        padding: '3px',
        'font-size': '9px',
        'border-radius': '5px',
        'background-color': '#fff',
        'font-family': 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
        opacity: 0.9
    }).appendTo("body").fadeIn(200);
}

AutoRangeData =
        [
            1, 2, 5, 10, 20,
            50, 100, 200, 500, 1000,
            1500, 2000, 2500, 3000, 3500,
            4000, 4500, 5000, 6000, 7000,
            8000, 9000, 10000, 15000, 20000,
            25000, 30000, 35000, 40000, 45000,
            50000, 60000, 70000, 80000, 90000,
            100000, 200000, 5000000
        ];
// given a value find the Auto Range Value given by Above Table
function AutoRange(v)
{
    if (v > 5000000)
        return v;
    var limit = 1;
    var i = 0;
    while (AutoRangeData[i] < v)
    {
        i++;
        if (i > 37)
            break;
    }

    limit = AutoRangeData[i];
    return limit;
}
Array.prototype.max = function () {
    return Math.max.apply(null, this);
};
Array.prototype.min = function () {
    return Math.min.apply(null, this);
};
var initBarChart = function ($rangeType) {
    var data = __getChartJSON();
    //console.log(data);
    // Renders a line chart
    if (data.nvd3.chartData[0].values.length == 0)
        $("#nvd3-Bar svg").html("");
    var $margin = {left: 50, bottom: 50};
    if (charXaxisType == "%m/%d/%Y %H:%M") {
        $margin = {right: 50, bottom: 65};
    }
    (function () {
        //__showLoadingAnimation();
        nv.addGraph(function () {
            var chart = nv.models.multiBarChart()
                    .x(function (d) {
                        return d[0]
                    })
                    .y(function (d) {
                        return d[1]
                    })
                    .color([
                        $.Pages.getColor('success', .7),
                        $.Pages.getColor('info'),
                        $.Pages.getColor('primary', .87),
                        $.Pages.getColor('warning'),
                        $.Pages.getColor('complete', .67),
                        $.Pages.getColor('success-dark'),
                        $.Pages.getColor('menu', .2)
                    ])
                    .groupSpacing(0.1)
                    .tooltips(true)
                    .tooltipContent(function (key, from, legVal, graph) {
                        //console.log(graph);
                        //var $ = parseInt(graph.point[1]);
                        var $arrVal = graph.series["values"];
                        var $pointIndex = graph.pointIndex;
                        var $toTimeSta = "";
                        var $toData = new Date();
                        //console.log($arrVal.length +">"+ $pointIndex);
                        if ($arrVal.length > ($pointIndex + 1)) {
                            $toTimeSta = graph.series["values"][parseInt(graph.pointIndex) + 1][0];
                            $toData = new Date($toTimeSta);
                        }
                        else {
                            $toTimeSta = graph.series["values"][parseInt(graph.pointIndex)][0];
                            $toData = new Date($toTimeSta);
                            //$toData.setHours(24,0,0,0);
                            //console.log($toData);
                        }
                        var $to = "";
                        //console.log(from);
                        if (charXaxisType == "%m/%d/%Y %H:%M") {
                            $to = ("0" + ($toData.getMonth() + 1)).slice(-2) + "/" + ("0" + $toData.getDate()).slice(-2) + "/" + $toData.getFullYear() + " " + ("0" + $toData.getHours()).slice(-2) + ":" + ("0" + $toData.getMinutes()).slice(-2);
                        }
                        else if (charXaxisType == "%m/%d/%Y") {
                            $to = ("0" + ($toData.getMonth() + 1)).slice(-2) + "/" + ("0" + $toData.getDate()).slice(-2) + "/" + $toData.getFullYear();
                        }
                        else if (charXaxisType == "%H:%M:%S") {
                            $to = ("0" + $toData.getHours()).slice(-2) + ":" + ("0" + $toData.getMinutes()).slice(-2) + ":" + ("0" + $toData.getSeconds()).slice(-2);
                        }
                        else if (charXaxisType == "%H:%M") {
                            $to = ("0" + $toData.getHours()).slice(-2) + ":" + ("0" + $toData.getMinutes()).slice(-2);
                        }
                        else {
                            $to = ("0" + $toData.getHours()).slice(-2) + ":" + ("0" + $toData.getMinutes()).slice(-2) + ":" + ("0" + $toData.getSeconds()).slice(-2);
                        }
                        return '<h3>' + key + '</h3>' +
                                '<p> <span class="legtext">' + legVal + '</span>' +
                                '</br> <span class="legbw">between </span></br><span class="legrange">' + from + ' - ' + $to + '<span></p>';
                    });
            chart.xAxis
                    .tickFormat(function (d) {
                        return d3.time.format(charXaxisType)(new Date(d));
                    });
            chart.margin($margin);
            chart.showControls(false);
            chart.yAxis
                    .tickFormat(d3.format(',.2f'));
            d3.select('#nvd3-Bar svg')
                    .datum(data.nvd3.chartData)
                    .transition().duration(500)
                    .call(chart);
            nv.utils.windowResize(function () {
                chart.update();
            });
            $('#nvd3-Bar').data('chart', chart);
            return chart;
        }, function () {

            /* setTimeout(function(){
             d3.selectAll('.nv-bar').attr('width', function(d){
             var width = this.getBBox().width;
             var newWidth = width - 2;
             console.log("width ==> " + width + "; newWidth ==> " + newWidth);
             this.width = newWidth;
             return  newWidth;
             
             });
             }, 1000);*/
            d3.selectAll(".nv-x text").attr("transform", function (d) {
                return "translate(" + this.getBBox().height * -2 + "," + this.getBBox().height + ")rotate(-45)";
            });
            /*d3.selectAll(".nv-bar").on('click',function(e){
             if(charXaxisType != "%H:%M."){
             var startDate = e[0];
             var endDate = "";
             var unitsTime = parseInt($("#timeRange").val());
             if(searchCriteria.length == 0 && unitsTime != 1){
             var hrs = new Date(startDate).getHours()+ parseInt(unitsTime);
             //hrs = hrs <= 23 ? hrs : 23;
             endDate = new Date(startDate).setHours(hrs);
             unitsTime = 1;
             }
             else {
             endDate = new Date(startDate);
             unitsTime = 0;
             }
             searchCriteria.push({startDate : startDate,endDate : endDate,time : unitsTime});
             initBarChart(2); 
             }
             });*/

        });
    })();
}

var initHarmonicChart = function (data) {
    //console.log(data);
    /*if (data != undefined && data != null && data.length != 0 ){
     if(!$("#checkShow1st").is(':checked')){
     //data[0].values[0][1] = "0";
     delete data[0].values[0];
     var datasetVal = data[0].values;
     for(var i = 0; i < datasetVal.length; i++){
     data[0].values[i] = data[0].values[i+1];
     }
     }
     }*/


    var maxVal = Math.max.apply(Math, data[0].values.map(function (i) {
        return i[1];
    }));
    var minVal = Math.min.apply(Math, data[0].values.map(function (i) {
        return i[1];
    }));
    var tickStep = CalculateChartRange(minVal, maxVal);
    var ticks = [];
    for (var $k = tickStep[0]; $k <= tickStep[1]; $k = $k + tickStep[2]) {
        ticks.push($k);
    }


    if ((data[0].values).length > 0) {
        var $peakID = data[0].PID == "1" ? "1<sup>st</sup>" : data[0].PID == "2" ? "2<sup>nd</sup>" : data[0].PID == "3" ? "3<sup>rd</sup>" : data[0].PID == "0" ? "0" : data[0].PID + "<sup>th</sup>";
        var $THD = data[0].THDval;
        var $THDVal = RoundOneDecimal($THD, 1);
        $("#txtTHDVal").html("THD : " + $THDVal + "%, ");
        $("#txtPeakVal").html("Peak : " + RoundOneDecimal(data[0].PVal, 1) + "% @ " + $peakID);
        $("#txtHTime").html(data[0].ID + " Harmanic @ " + data[0].Htime);
        $(".harmonic-notify").removeClass("hide");
    }
    else
        $(".harmonic-notify").addClass("hide");
    //console.log(data);
    (function () {
        nv.addGraph(function () {
            var chart = nv.models.multiBarChart()
                    .x(function (d) {
                        return d[0]
                    })
                    .y(function (d) {
                        return d[1]
                    })
                    .color([
                        $.Pages.getColor('danger', .7),
                    ])
                    .tooltips(true)
                    .tooltipContent(function (key, y, e, graph) {

                        if (y % 2 == 0)
                            return '';
                        return  '<h3>' + key + '</h3>' + '<p> <span class="legtext">' + e + ' on ' + y + '</span></p>';
                    })
                    .showLegend(false)
                    .showControls(false)
                    .yDomain([tickStep[0], tickStep[1]]);
            chart.xAxis
                    .tickFormat(d3.format('d'));
            chart.yAxis
                    .tickFormat(d3.format('.02s'));
            chart.margin({left: 60, bottom: 20, right: 0});
            d3.select('#nvd3-HarmonicBar svg')
                    .datum(data)
                    .transition()
                    .duration(500)
                    .call(chart);
            nv.utils.windowResize(function () {
                chart.update();
            });
            $('#nvd3-HarmonicBar').data('chart', chart);
            return chart;
        }, function () {
            $("nv-legendWrap").addClass("hide");
            /* $("nvd3 nv-legend").addClass("hide");*/
        });
    })();
}

Date.prototype.addDays = function (days) {
    var dat = new Date(this.valueOf())
    dat.setDate(dat.getDate() + days);
    return dat;
}

function getDates(startDate, stopDate) {
    var dateArray = new Array();
    var currentDate = startDate;
    while (currentDate <= stopDate) {
        dateArray.push(currentDate)
        currentDate = currentDate.addDays(1);
    }
    return dateArray;
}

Date.prototype.addHours = function (h) {
    this.setHours(this.getHours() + h);
    return this;
}

var __getChartJSON = function () {
    var $cType = $("#selChartType").val();
    var legendMasterList = [];
    var selectDevices = $('#chartSelDevice').select2("val");
    var selectedlegends = $('#chartParamSelection').select2("val");
    var startDate = new Date(new Date($('#dateRange').data('date')));
    var endDate = new Date(new Date($('#dateRange').data('date')));
    var timeRange = $("#selDuration").select2("val");
    var XaxisLabel = "Time";
    var charXaxisType = "%a";
    var inct = 1;
    var yAxisData = [];
    isChartPage = true;
    //console.log(selectDevices);
    //console.log("$cType == "+$cType+" selectDevices =="+selectDevices+" selectedlegends =="+selectedlegends+" timeRange == "+timeRange)
    if (!selectValidation('chartSelDevice', 'device', 'divDevice') ||
            !multiSelValidation('chartParamSelection', 'parameter', 'divParameter'))
        return false;
    if (timeRange == "1m") {
        inct = 1;
        startDate.setMinutes(endDate.getMinutes() - 15);
        charXaxisType = "%H:%M:%S";
        XaxisLabel = "Time";
    }
    else if (timeRange == "5m") {
        inct = 5;
        endDate.setMinutes(endDate.getMinutes() + 5);
        charXaxisType = "%H:%M:%S";
        XaxisLabel = "Time";
    }
    else if (timeRange == "30m") {
        inct = 1;
        endDate.setMinutes(endDate.getMinutes() + 30);
        charXaxisType = "%H:%M";
        XaxisLabel = "Time";
    }
    else if (timeRange == "1h") {
        inct = 1;
        endDate.setHours(endDate.getHours() + 1);
        charXaxisType = "%H:%M";
        XaxisLabel = "Time";
    }
    else if (timeRange == "12h") {
        inct = 5;
        endDate.setHours(endDate.getHours() + 12);
        charXaxisType = "%H:%M";
        XaxisLabel = "Date & Time";
    }
    else if (timeRange == "24h") {
        inct = 1;
        endDate.setHours(endDate.getHours() + 24);
        charXaxisType = "%H:%M";
        XaxisLabel = "Date & Time";
    }
    else if (timeRange == "7d") {
        inct = 1;
        endDate.setDate(endDate.getDate() + 7);
        charXaxisType = "%m/%d/%Y %H:%M";
        XaxisLabel = "Date & Time";
    }
    else if (timeRange == "30d") {
        inct = 1;
        endDate.setDate(endDate.getDate() + 30);
        charXaxisType = "%m/%d/%Y %H:%M";
        XaxisLabel = "Date & Time";
    }
    else if (timeRange == "1y") {
        inct = 1;
        endDate.setDate(endDate.getDate() + 365);
        charXaxisType = "%m/%d/%Y";
        XaxisLabel = "Date";
    }
    oldCharXaxisType = charXaxisType;
    /*if(searchCriteria.length == 0 ){
     timeRange = parseInt($("#timeRange").val());
     inct = timeRange;
     }
     else{
     var item = searchCriteria[searchCriteria.length-1];
     startDate = new Date(item.startDate);
     endDate = new Date(item.endDate);
     timeRange = parseInt(item.time);
     inct = timeRange;
     }*/
    $("#div-chart-wrapper").html(""); // clean chart area

    var chartData = GetDeviceChartValue(selectDevices, startDate, endDate, timeRange);
    //console.log("chart data");
    //console.log(chartData);
    if (chartData.length == 0) {
        $("#div-chart-wrapper").html('<div class="chart-nodata"><strong>No Data Available.</strong></div>');
        return false;
    }
    //console.log(selectDevices);
    var singleMode = false;
    if (selectedlegends.length > 0) {
        var yaxisCount = 1;
        var dataIndex = 0;
        var unitChart = "";
        var unitChartDivisor = 1;
        for (var i = 0; i < selectedlegends.length; i++) {
            var displayLegend = $('#chartParamSelection option[value="' + selectedlegends[i] + '"]').html();
            var identity = displayLegend.replace(/ /g, '');
            //if( selectDevices.length > 0){
            var minVal = 0;
            var maxVal = 0;
            var chartCol = "";
            //for (var j = 0; j < selectDevices.length; j++) {
            var device = selectDevices;
            var deviceName = $('#chartSelDevice option[value="' + device + '"]').html();
            var legendValues = [];
            //console.log(device);
            legendValues = BindChartData(chartData, identity, charXaxisType, timeRange, device);
            //console.log(legendValues);
            //var chartCol = Please.make_scheme(chartIdentityColor[identity],{ format: 'rgb-string'});
            //console.log("identity === "+identity);
            chartCol = chartIdentityColor[identity]; //Please.make_color({base_color: identity });
            //console.log(chartIdentityColor);
            //console.log("chartCol === "+chartCol);
            var chartFillCol = chartCol;
            if ($cType == 2)
                chartFillCol = chartAreaIdentityColor[identity]; //convertHex(chartCol,50);// Area chart
            //console.log(chartFillCol);

            if (legendValues.length > 0) {

                //Tickes
                var tempMinVal = legendValues.min();
                var tempMaxVal = legendValues.max();
                if (minVal == 0 || minVal > tempMinVal)
                    minVal = tempMinVal;
                if (maxVal == 0 || maxVal < tempMaxVal)
                    maxVal = tempMaxVal;
                var cgData = formattedGuageArray[identity];
                unitChart = cgData.unit;
                unitChartDivisor = 1;
                //var isChartAutoK =  ? true : false;
                console.log("unitChartDivisor === "+unitChartDivisor);
                if (cgData.auto_k == 1) {

                    var maxChPoint = Math.abs(minVal) > Math.abs(maxVal) ? Math.abs(minVal) : Math.abs(maxVal);
                    var newChartUnitPrefix = "";
                    var iChartCnt = 1;
                    while (maxChPoint > 1000) {
                        if (iChartCnt > 2)
                            break;
                        unitChartDivisor *= 1000;
                        maxChPoint /= 1000;
                        if (iChartCnt == 1)
                            newChartUnitPrefix = "K"
                        else
                            newChartUnitPrefix = "M";
                        iChartCnt++;
                    }

                    unitChart = newChartUnitPrefix + unitChart;
                }
                var dataObj = {
                    yAxisID: identity,
                    label: deviceName + " " + selectedlegends[i],
                    backgroundColor: chartFillCol,
                    data: legendValues,
                    unitChartDivisor: unitChartDivisor,
                    unitChart: unitChart,
                    borderColor: chartCol
                };
                if ($cType == 3)
                    dataObj["fill"] = false;
                legendMasterList.push(dataObj);
                //dataIndex++;
            }
            //}
            var tickStep = CalculateChartRange(minVal, maxVal);
            var yaxisObj = {
                type: "linear", // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
                display: true,
                position: "left",
                id: identity,
                color: chartCol,
                scaleLabel: {
                    display: true,
                    labelString: displayLegend + " (" + unitChart + ")",
                    fontColor: chartCol
                },
                ticks: {
                    fontColor: chartCol,
                    min: tickStep[0],
                    max: tickStep[1],
                    stepSize: tickStep[2],
                    unitChartDivisor: unitChartDivisor,
                    callback: function (dataLabel, index) {
                        //return dataLabel/this.unitChartDivisor;
                        /*console.log("unitChartDivisor <<<<=== "+unitChartDivisor);
                        console.log(this);
                        console.log("dataLabel === "+dataLabel);
                        console.log(RoundOneDecimal(dataLabel / this.unitChartDivisor, 1));*/
                        return RoundOneDecimal(dataLabel / unitChartDivisor, 1);
                    }
                }
            };
            if (i != 0) {
                yaxisObj["gridLines"] = {drawOnChartArea: false}
            }

            yAxisData.push(yaxisObj);
            //yaxisCount++;
            //}
        }
        /*var retJSON = {
         nvd3:{
         "chartData" :legendMasterList
         },
         singleDate:singleMode, 
         customTicksX:BindChartDataPeriod(chartData,charXaxisType,timeRange)
         };*/
    }


    var xLabelString = "time"
    if (timeRange == "1m" || timeRange == "5m") {
        timeFormat = 'HH:MM:ss';
        xLabelString = 'Time';
        timeUnit = 'second';
    }
    else if (timeRange == "30m" || timeRange == "1h") {
        timeFormat = 'HH:MM';
        xLabelString = 'Time';
        timeUnit = 'minute';
    }
    else if (timeRange == "1y") {
        timeFormat = 'mm/dd/yyyy';
        xLabelString = 'Date';
        timeUnit = 'day';
    }
    else {
        timeFormat = 'mm/dd/yyyy HH:MM';
        xLabelString = 'Date & Time';
        timeUnit = 'hour';
    }

    //Chart data binding
    var barChartData = {
        labels: BindChartDataPeriod(chartData, timeRange),
        datasets: legendMasterList
    };
    //console.log(barChartData);
    //xaxis configuration
    var xAxisData = [{
            /*type: "time",*/
            time: {
                format: timeFormat,
                tooltipFormat: timeFormat/*,
                 unit: timeUnit*/
            },
            scaleLabel: {
                display: true,
                labelString: xLabelString
            },
            ticks: {
                callback: function (dataLabel, index) {
                    return dataLabel.format(timeFormat);
                    /*console.log(multiplyFactor);
                     return index % multiplyFactor === 0 ? dataLabel : '';*/
                }
            }/*,
             //parser: timeFormat*/


        }];
    //Chart Options
    var chartOptions = {
        data: barChartData,
        options: {
            responsive: true,
            hoverMode: 'label',
            hoverAnimationDuration: 400,
            stacked: false,
            title: {
                display: false
            },
            tooltips: {
                mode: 'single',
                callbacks: {
                    label: function (tooltipItems, data) {
                        var cDispUnit = (data.datasets[tooltipItems.datasetIndex].unitChart);
                        var cDispVal = tooltipItems.yLabel / data.datasets[tooltipItems.datasetIndex].unitChartDivisor;
                        var chartDispParam = data.datasets[tooltipItems.datasetIndex].label;
                        return chartDispParam + ': ' + RoundOneDecimal(cDispVal, 1) + ' ' + cDispUnit;
                    }
                }
            },
            scales: {
                xAxes: xAxisData,
                yAxes: yAxisData
            }
        }
    };
    if ($cType == 1)
        chartOptions["type"] = 'bar';
    else
        chartOptions["type"] = 'line';
    $("#div-chart-wrapper").html("");
    $("#div-chart-wrapper").html('<canvas id="canvas-chart" style="width: 995px; height: 497px;" width="995" height="497"></canvas>');
    var ctx = document.getElementById("canvas-chart").getContext("2d");
    //console.log(chartOptions);

    window.myBar = new Chart(ctx, chartOptions);
}

//Get chart values
function GetDeviceChartValue(devices, startDate, endDate, timeRange) {
    var gaugeValues = [];
    var date1 = new Date(startDate);
    var date2 = new Date(endDate);
    startDate = date1.getFullYear() + '-' + (date1.getMonth() + 1) + '-' + date1.getDate() + ' ' + date1.getHours() + ':' + date1.getMinutes() + ':' + date1.getSeconds();
    endDate = date2.getFullYear() + '-' + (date2.getMonth() + 1) + '-' + date2.getDate() + ' ' + date2.getHours() + ':' + date2.getMinutes() + ':' + date2.getSeconds();
    $.ajax({
        type: 'POST',
        url: 'ajax.php',
        data: {'c': 'Chart', 'a': 'GetDeviceChartValue', 'timeRange': timeRange, 'dateBegin': startDate, 'dateEnd': endDate, 'devices': devices},
        dataType: 'json',
        async: false,
        beforeSend: function () {
            __showLoadingAnimation();
        },
        success: function (res) {
            //console.log( res.result);
            gaugeValues = res.result;
        },
        complete: function () {
            setTimeout(function () {
                __hideLoadingAnimation();
            }, 300);
        },
        error: function (r) {
            setTimeout(function () {
                __hideLoadingAnimation();
            }, 300);
            console.log(r);
        }
    });
    //console.log(gaugeValues);
    return gaugeValues;
}

function BindChartData(chartData, identity, charXaxisType, timeRange, device) {

    var legendValues = [];
    //console.log( identity);
    for (gCount = 0; gCount < chartData.length; gCount++) {

        var valueG = chartData[gCount];
        var dateKey;
        //console.log("charXaxisType=="+charXaxisType+"==timeRange=="+timeRange);
        if (valueG['deviceID'] == device) {
            /*if(timeRange == "5m" || timeRange == "1m")dateKey = new Date(valueG["year"],valueG["month"]-1,valueG["day"],valueG["hour"],valueG["minute"],valueG["second"]);
             else if(timeRange == "30m" || timeRange == "1h")dateKey = new Date(valueG["year"],valueG["month"]-1,valueG["day"],valueG["hour"],valueG["minute"]);
             else if(timeRange == "12h" || timeRange == "24h"){
             var min = (parseInt(valueG["minute"])) * 5;
             dateKey = new Date(valueG["year"],valueG["month"]-1,valueG["day"],valueG["hour"],min);
             }
             else if(timeRange == "7d"){ dateKey = new Date(valueG["year"],valueG["month"]-1,valueG["day"],valueG["hour"]);console.log(dateKey);}
             else if(timeRange == "30d"){
             var hrs = (parseInt(valueG["hour"])) * 4;
             dateKey = new Date(valueG["year"],valueG["month"]-1,valueG["day"],hrs);
             }
             else dateKey = new Date(valueG["year"],valueG["month"]-1,valueG["day"]);*/

            if (timeRange == "1m")
                dateKey = new Date(valueG["year"], valueG["month"] - 1, valueG["day"], valueG["hour"], valueG["minute"], valueG["second"]);
            else if (timeRange == "5m")
                dateKey = new Date(valueG["year"], valueG["month"] - 1, valueG["day"], valueG["hour"], valueG["minute"], valueG["second"]);
            else if (timeRange == "30m" || timeRange == "1h")
                dateKey = new Date(valueG["year"], valueG["month"] - 1, valueG["day"], valueG["hour"], valueG["minute"]);
            else if (timeRange == "12h" || timeRange == "24h")
                dateKey = new Date(valueG["year"], valueG["month"] - 1, valueG["day"], valueG["hour"], valueG["minute"]);
            else if (timeRange == "7d")
                dateKey = new Date(valueG["year"], valueG["month"] - 1, valueG["day"], valueG["hour"]);
            else if (timeRange == "30d")
                dateKey = new Date(valueG["year"], valueG["month"] - 1, valueG["day"], valueG["hour"]);
            else
                dateKey = new Date(valueG["year"], valueG["month"] - 1, valueG["day"]);
            //var legendData = {x:dateKey.getTime(),y:parseFloat(GetGaugeValueByIdentity(identity, valueG))};
            //console.log("legend==="+identity);    
            //legendValues.push(legendData);
            legendValues.push(parseFloat(GetGaugeValueByIdentity(identity, valueG)));
        }
    }

    return legendValues;
}

function BindChartDataPeriod(chartData, timeRange) {
    var xTickMarkers = [];
    var devID = 0;
    for (gCount = 0; gCount < chartData.length; gCount++) {

        var valueG = chartData[gCount];
        //console.log(valueG);
        var dateKey;
        if (devID == 0 || devID == valueG["deviceID"]) {
            if (timeRange == "1m")
                dateKey = new Date(valueG["year"], valueG["month"] - 1, valueG["day"], valueG["hour"], valueG["minute"], valueG["second"]);
            else if (timeRange == "5m")
                dateKey = new Date(valueG["year"], valueG["month"] - 1, valueG["day"], valueG["hour"], valueG["minute"], valueG["second"]);
            else if (timeRange == "30m" || timeRange == "1h")
                dateKey = new Date(valueG["year"], valueG["month"] - 1, valueG["day"], valueG["hour"], valueG["minute"]);
            else if (timeRange == "12h" || timeRange == "24h")
                dateKey = new Date(valueG["year"], valueG["month"] - 1, valueG["day"], valueG["hour"], valueG["minute"]);
            else if (timeRange == "7d")
                dateKey = new Date(valueG["year"], valueG["month"] - 1, valueG["day"], valueG["hour"]);
            else if (timeRange == "30d")
                dateKey = new Date(valueG["year"], valueG["month"] - 1, valueG["day"], valueG["hour"]);
            else
                dateKey = new Date(valueG["year"], valueG["month"] - 1, valueG["day"]);
            /*if((parseInt(timeRange)>= 2 && parseInt(timeRange)<= 23)){
             var hrs = (parseInt(valueG["hour"])) * parseInt(timeRange);
             dateKey = new Date(valueG["year"],valueG["month"]-1,valueG["day"],hrs);
             }
             else if(charXaxisType == "%H:%M."){
             dateKey = new Date(valueG["year"],valueG["month"]-1,valueG["day"],valueG["hour"],(parseInt(valueG["minute"]))*15);
             }
             else if(charXaxisType == "%m/%d/%Y"){
             dateKey = new Date(valueG["year"],valueG["month"]-1,valueG["day"]);
             } 
             else{
             dateKey = new Date(valueG["year"],valueG["month"]-1,valueG["day"],valueG["hour"]);
             }*/
            xTickMarkers.push(dateKey);
            devID = valueG["deviceID"];
        }
    }
    return xTickMarkers;
}

var minMaxArray = [1, 2, 5, 10, 20, 50, 100, 200, 500, 1000,
    2000, 5000, 10000, 20000, 50000,
    100000, 200000, 500000, 1000000];
//Step json
var stepWidthArray = {
    1: 0.2,
    2: 0.5,
    5: 1,
    10: 2,
    20: 5,
    50: 10,
    100: 20,
    200: 50,
    500: 100,
    1000: 200,
    2000: 500,
    5000: 1000,
    10000: 2000,
    20000: 5000,
    50000: 10000,
    100000: 20000,
    200000: 50000,
    500000: 100000,
    1000000: 200000
};
//Step width finder
function CalculateStepWidth(val) {
    return stepWidthArray[val];
}

//auto range finder
function AutoRangeChartValue(val) {

    var rangedVal = 10;
    for (i = 0; i < 19; i++) {
        if (val < minMaxArray[i]) {
            rangedVal = minMaxArray[i];
            break;
        }
    }

    return rangedVal;
}

//Calulate max tick position;
function MaxTickCalculator(maxVal, steps) {
    var ret = steps;
    while (maxVal > ret) {
        ret += steps;
    }

    return ret;
}

//Calculate range
function CalculateChartRange(minVal, maxVal) {
    var correctedValue = maxVal;
    var steps = 1;
    if (maxVal < 0) {
        correctedValue = AutoRangeChartValue(Math.abs(minVal));
        return [-1 * correctedValue, 0, CalculateStepWidth(correctedValue)];
    }
    else if (Math.abs(minVal) > maxVal) {
        correctedValue = AutoRangeChartValue(Math.abs(minVal));
        var steps = CalculateStepWidth(correctedValue);
        return [-1 * correctedValue, MaxTickCalculator(maxVal, steps), steps];
    }
    else {
        correctedValue = AutoRangeChartValue(maxVal);
        if (minVal < 0) {
            var steps = CalculateStepWidth(correctedValue);
            return [-1 * MaxTickCalculator(Math.abs(minVal), steps), correctedValue, steps];
        }
        else
            return [0, correctedValue, CalculateStepWidth(correctedValue)];
    }


}
function hashCode(str) { // java String#hashCode
    var hash = 0;
    for (var i = 0; i < str.length; i++) {
        hash = str.charCodeAt(i) + ((hash << 5) - hash);
    }
    return hash;
}

function intToRGB(str) {
    var i = hashCode(str);
    var c = (i & 0x00FFFFFF)
            .toString(16)
            .toUpperCase();
    return "#" + "00000".substring(0, 6 - c.length) + c;
}

var $chartPlot = null;
var togglePlotChart = function (seriesIdx, chartType, $this) {
    var someData = $chartPlot.getData();
    var showChart;
    if (chartType == 1) {
        someData[seriesIdx].bars.show = !someData[seriesIdx].bars.show;
        showChart = someData[seriesIdx].bars.show;
    }
    else {
        someData[seriesIdx].lines.show = !someData[seriesIdx].lines.show;
        showChart = someData[seriesIdx].lines.show;
    }


    $chartPlot.setData(someData);
    $chartPlot.draw();
    var pn = $($this).closest('td').prev('td');
    var fDiv = pn.find('div:first')

    if (showChart)
        fDiv.find('div:first').css('border-width', '5px');
    else
        fDiv.find('div').css('border-width', '1px');
}


function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

var __initSlidersVoltage = function (fieldId) {
    $(fieldId).ionRangeSlider({
        min: 0,
        max: 250,
        type: 'double',
        prefix: "",
        from: 0,
        to: 0,
        maxPostfix: "+",
        prettify_enabled: false,
        grid: false
    });
}

var __initSlidersAmperage = function (fieldId) {
    $(fieldId).ionRangeSlider({
        min: 0,
        max: 250,
        type: 'double',
        from: 0,
        to: 0,
        prefix: "",
        maxPostfix: "+",
        prettify_enabled: false,
        grid: false
    });
}

var __initSlidersPF = function (fieldId) {
    $(fieldId).ionRangeSlider({
        min: 0,
        max: 100,
        type: 'double',
        prefix: "",
        from: $(fieldId).attr("from"),
        to: $(fieldId).attr("to"),
        maxPostfix: "+",
        prettify_enabled: false,
        grid: false
    });
}

var __displayGaugeParam = function (selObj) {

    if (typeof (selObj.value) !== 'undefined' && selObj.value != "" && selObj.value > 0)
    {
        option = $('#selGauge option[value="' + selObj.value + '"]')

        var parameters = {
            'selGauge': selObj.value,
            'gaugeName': $(option).html().trim(),
            'min': option.data('min'),
            'max': option.data('max'),
            'major': option.data('major-ticks'),
            'g_minval': option.data('g-minval'),
            'cus': option.data('cus'),
            'color': option.data('color'),
            'visible': option.data('visible'),
            'auto-k': option.data('auto-k'),
            'is-avg': option.data('is-avg'),
            'unit': option.data('unit')
        }

        $('#div-box-gague-settings').load('GuageSettings.php', parameters);
        $('#div-box-gague-settings').show();
    }
    else {
        //$(selObj).select2("val", "");
        $('#div-box-gague-settings').hide();
        $('#div-box-gague-settings').html('');
    }
}

$(document).ready(function () {
    
    var $pageName = getPageName();
	
    if($pageName != "instanceManagement" && !$(".dev-sel-main").hasClass("dev-selected")){
        _deviceSel.open();
    }
    else{$(".page-content-wrapper").removeClass("hide");}
    setInterval($gauges.setLogData, 5000); 
	setInterval($gauges.alertMessages, 5000); 
    //$gauges.getAllGauge();
    
    /*$('input[name="daterange"]').daterangepicker({
     ranges: {
     'Today': [moment(), moment()],
     'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
     'Last 7 Days': [moment().subtract(6, 'days'), moment()],
     'Last 30 Days': [moment().subtract(29, 'days'), moment()],
     'This Month': [moment().startOf('month'), moment().endOf('month')],
     'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
     },
     "opens": "left"
     }, 
     function(start, end, label) {
     if( start.format('YYYY-MM-DD') != end.format('YYYY-MM-DD')){
     $("#timeRange").val(24).trigger('change');
     }
     else{
     $("#timeRange").val(1).trigger('change');
     }
     
     });*/
    /*Set System Time*/
    /* setInterval(function(){
     $dt = new Date();
     $(".sys-time").html(("0" + $dt.getHours()).slice(-2)+":"+("0" + $dt.getMinutes()).slice(-2));
     }, 1000); */
    //setLogData();
    //setInterval(setLogData, 2000); 
    /*var tt =0;
     setInterval(function(){
     if(tt<6) $createAlert({status : "success",title : "Success",text :"text"+tt});
     tt++;
     },3000);*/
    //setInterval(function(){hideAlert($('.nn-alert-container  .nn-alert:last-child'));},5000);

});
var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;
    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};
function openListview() {
    $("#device-list").removeClass("hide");
}

function closeListview() {
    $("#device-list").addClass("hide");
}

var $draggable = function ($element, $handle) {
    $($element).draggabilly(
            {
                containment: '#mapContainer',
                handle: $handle
                        // position:{ x: 20, y: -30 }
            });
};
var __initTimeRange = function ($fieldId) {
    $($fieldId).ionRangeSlider({
        min: 0,
        max: 24,
        type: 'single',
        prefix: "",
        from: 1,
        //to: 1,
        maxPostfix: "+",
        prettify_enabled: true,
        grid: false,
        prefix: "Today last ",
                postfix: " hrs"
    });
}

var __intknobBar = function ($fieldId, $val, $color) {
    $($fieldId).knob({
        'min': 0,
        'max': 180,
        'readOnly': true,
        'thickness': .1,
        'bgColor': '#A9A9A9',
        'fgColor': $color,
        'dynamicDraw': true,
        'width': 156,
        'height': 156
    });
    $($fieldId).addClass("tem-text");
    __setKnobVal($fieldId, $val, $color, 180);
    //$($fieldId).val($val).trigger('change');

}

var __intknobBarCaps = function ($fieldId, $val, $color) {
    $($fieldId).knob({
        'min': 0,
        'max': 1000,
        'readOnly': true,
        'thickness': .1,
        'bgColor': '#A9A9A9',
        'fgColor': $color,
        'dynamicDraw': true,
        'width': 156,
        'height': 156
    });
    $($fieldId).addClass("tem-text");
    __setKnobVal($fieldId, $val, $color, 1000);
}

var __setKnobVal = function ($fieldId, $val, $color, maxVal) {

    if ($val > maxVal)
        maxVal = $val;
    $($fieldId).trigger('configure', {
        "fgColor": $color,
        'max': maxVal
    });
    $({value: $($fieldId).val()}).animate({value: $val}, {
        duration: 1000,
        easing: 'swing',
        progress: function () {
            $($fieldId).val(Math.ceil(this.value)).trigger('change');
        }
    });
}

var __intTimeknobBar = function ($fieldId, $val) {
    $($fieldId).knob({
        'min': 1,
        'max': 24,
        'thickness': .1,
        'bgColor': '#FFFFFF',
        'fgColor': 'rgb(253, 85, 19)',
        'width': 90,
        'height': 90,
        'format': function (value) {
            return value + ' h';
        }
    });
    $($fieldId).val($val).trigger('change');
}

//Adjust color zone range;
function CorrectingColorZone(highlights, min, max) {
    var newColorZone = JSON.parse(JSON.stringify(highlights));
    for (i = 0; i < newColorZone.length; i++) {
        if (newColorZone[i].from < min)
            newColorZone[i].from = min;
        if (newColorZone[i].to < min)
            newColorZone[i].to = min;
        if (newColorZone[i].from > max)
            newColorZone[i].from = max;
        if (newColorZone[i].to > max)
            newColorZone[i].to = max;
    }

    return newColorZone;
}

var GuageComponent = function () {
    //console.log('instance created');
    this.setValue = function (valNew, maxValNew) {
        try {

            this.options.value = valNew;
            if (typeof maxValNew != "undefined")
                this.options.maxValue = maxValNew;
            
            var $elemID = this.options.elem;
            var $splitElemID = $elemID.split("_");
            //console.log("ste gauge value");
            //Auto K Convertion
            if (parseInt(this.options.autok) == 1 && parseFloat(this.options.value) > 1000) {
                var autoKValueA = parseFloat(this.options.value);
                var incrLimiterA = 2;
                while (autoKValueA > 1000) {

                    if (incrLimiterA == 0)
                        break;
                    incrLimiterA--;
                    autoKValueA = autoKValueA / 1000;
                    valNew = valNew / 1000;
                }

            }

            var minV = 0, maxV = 0, mTicks = 0;
            minV = parseInt(this.options.minValue);
            maxV = parseInt(this.options.maxValue);
            mTicks = parseInt(this.options.minorTicks);
            //console.log(">>>>> minV => " + minV + " ; maxV ==> " + maxV + " ; +mTicks ==> " + mTicks );
            // Auto Range if needed
            if (parseFloat(this.options.value) > parseFloat(this.options.maxValue) || parseInt(this.options.value) < parseInt(this.options.minValue)) {
                //console.log("auto range");
                while (parseFloat(this.options.value) >= maxV) {
                    maxV = maxV + mTicks;
                }

                while (parseFloat(this.options.value) <= minV) {
                    minV = minV - mTicks;
                }
                //if((this.options.units).trim() == '%' && maxV > 100) maxV = 100;

                var divisions = (maxV - minV) / mTicks;
                //console.log(divisions);
                if (divisions > 5) {
                    // Now scale the ticks to match the new max value
                    if (parseFloat(this.options.maxValue) < maxV) {
                        //mTicks = parseFloat(mTicks * maxV/parseFloat(this.options.maxValue));
                        //mTicks = mTicks * maxV/parseFloat(this.options.maxValue);
                        mTicks = parseInt(maxV / 4);
                        // if((this.options.units).trim() == '%') mTicks = 10;
                    }
                }
            }

            var divX = (maxV - minV) / mTicks;
            if (divX > 6) {
                mTicks = parseFloat((maxV - minV) / 4);
                //if((this.options.units).trim() == '%') mTicks = 10;
            }
            //console.log(mTicks);

            // Auto k if needed
            var unitPrefix = "";
            //if(parseInt(this.options.autok) == 1 && parseFloat(this.options.value) > 1000){
            if ((parseInt(this.options.autok) == 1 || this.options.autok == true) && parseFloat(this.options.value) > 1000) {

                var autoKValue = parseFloat(this.options.value);
                var incrLimiter = 2;
                //console.log(incrLimiter);
                //console.log("mTicks 1 == "+mTicks);
                while (autoKValue > 1000) {
                    //console.log("autoKValue == "+autoKValue+" inc == "+incrLimiter);
                    if (incrLimiter == 2)
                        unitPrefix = "K";
                    else if (incrLimiter == 1)
                        unitPrefix = "M";
                    else
                        break;
                    minV = minV / 1000;
                    maxV = maxV / 1000;
                    mTicks = mTicks / 1000;
                    //console.log("mTicks == "+mTicks);
                    incrLimiter--;
                    autoKValue = autoKValue / 1000;
                }
                //console.log("exist");
            }
            //return false;
            //console.log("max="+maxV+",min="+minV+",mTicks="+mTicks);
            // Now calculate the major ticks and unit
            var majorTicks = [];
            var incr = parseInt(minV);
            mTicks = (mTicks < 1) ? 1 : mTicks;
            maxV = (maxV < 1) ? 1 : maxV;
            //console.log("mTicks === "+mTicks);
            var $whileCount = 0;
            while (incr <= maxV) {
                majorTicks.push(incr);
                incr += parseInt(mTicks);
                //incr += mTicks;
                //console.log("incr == "+incr+" maxV == "+maxV+"");
            }
            var nHigh = maxV + parseInt(mTicks);
            //console.log("valNew => " + valNew + " ; maxV => " + maxV + " ; incr ==> " + incr + " ; +mti ==> " + nHigh );
            if (incr != nHigh) {
                majorTicks.push(incr);
                maxV = incr;
            }
            if ($splitElemID[2] == 'Motion') {
                majorTicks = ['0', '1'];
                minV = 0;
                maxV = 1;
            }
            /*if (this.options.elem == '__Powerfactor') {
                majorTicks = ['0', '10', '20', '30', '40', '50', '60', '70', '80', '90', '100', '-90', '-80'];
                minV = 0;
                maxV = 120;
            }*/

            //items not to do auto scale to :
            //VTHD , ATHD , VIMB, Cureent IMB . Neutural Amps, Power factor, 
            /*if (!(this.options.elem == '__Powerfactor' || this.options.elem == '__VoltageTHD' || this.options.elem == '__CurrentTHD' || this.options.elem == '__VoltageIMB' || this.options.elem == '__CurrentIMB' || this.options.elem == '__NeutralAmps')) {
                var gMinTick;
                if (this.options.elem == '__Current' && valNew < 50) {
                    maxV = 50;
                    gMinTick = 10;
                }
                else if (this.options.elem == '__Voltage' && valNew < 100) {
                    maxV = 100;
                    gMinTick = 20;
                }
                else {
                    maxV = AutoRangeChartValue(Math.abs(valNew));
                    console.log("maxV == "+maxV);
                    gMinTick = CalculateStepWidth(maxV);
                    console.log(gMinTick);
                }
                if (valNew < 0) {
                    minV = -1 * maxV;
                    maxV = 0;
                    if (this.options.elem == '__Current' && valNew > -50) {
                        minV = -50;
                        gMinTick = 10;
                    }
                    else if (this.options.elem == '__Voltage' && valNew > -100) {
                        minV = -100;
                        gMinTick = 20;
                    }
                }
                console.log("gMinTick");
                console.log(gMinTick);


                var gCurrentTick = minV;
                majorTicks = [];
                while (gCurrentTick <= maxV) {
                    majorTicks.push(RoundOneDecimal(gCurrentTick, 1));
                    gCurrentTick += gMinTick;
                }
                console.log("gCurrentTick");
                console.log(gCurrentTick);

            }*/

            var colorZone = CorrectingColorZone(this.options.highlights, minV, maxV);
            //console.log(majorTicks);
            var splitDec = valNew.toString().split(".");
            var units = unitPrefix + this.options.units;
            var strDispLength = splitDec[0].toString().length;
            if (valNew < 0)
                strDispLength--;
            this.gauge.updateConfig({
                minValue: minV,
                maxValue: maxV,
                majorTicks: majorTicks,
                units: units,
                highlights: colorZone,
                valueFormat: {int: strDispLength, dec: 1}
            });
            Gauge.Collection.get(this.options.elem).setValue(valNew);
            //console.log(splitDec);
        }
        catch (ex) {
            console.log(ex);
        }
    };
    this.changeHiligter = function (highlights) {
        if (typeof (opt.highlights) == 'array')
        {
            this.options.highlights = opt.highlights;
            this.initialize;
        }
    };
    this.options = {};
    this.initialize = function (opt) {
        var renderToElem = opt.elem;
        var units = '';
        var title = '';
        var minValue = 0;
        var maxValue = 100;
        var minorTicks = 10;
        var width = typeof ($("#" + renderToElem).prop("width")) == 'undefined' ? 250 : $("#" + renderToElem).prop("width");
        var height = typeof ($("#" + renderToElem).prop("height")) == 'undefined' ? 250 : $("#" + renderToElem).prop("height");
        if (typeof (opt.units) == 'undefined')
            opt.units = units;
        else
            units = opt.units;
        if (typeof (opt.title) == 'undefined')
            opt.title = title;
        else
            title = opt.title;
        if (typeof (opt.minValue) == 'undefined')
            opt.minValue = minValue;
        else
            minValue = opt.minValue;
        if (typeof (opt.maxValue) == 'undefined')
            opt.maxValue = maxValue;
        else
            maxValue = opt.maxValue;
        if (typeof (opt.minorTicks) == 'undefined')
            opt.minorTicks = minorTicks;
        else
            minorTicks = opt.minorTicks;
        if (typeof (opt.value) == 'undefined')
            opt.value = minValue;
        var majorTicks = [];
        //var incr = parseInt(minValue);
        var incr = parseInt(minValue);
        /*console.log("minValue=="+minValue);
         console.log("maxValue=="+maxValue);
         console.log("minorTicks=="+minorTicks);
         console.log("incr=="+incr);*/

        while (incr <= parseInt(maxValue)) {
            //console.log("incr==="+incr);
            majorTicks.push(incr);
            incr = incr + parseInt(minorTicks);
        }
        var $elemID = opt.elem;
        var $splitElemID = $elemID.split("_");
        //console.log($splitElemID);
        
        if ($splitElemID[2] == 'Motion') {
            majorTicks = ['0', '1'];
            minValue = 0;
            maxValue = 1;
        }
        
        /*if (opt.elem == '__Powerfactor') {
            majorTicks = ['0', '10', '20', '30', '40', '50', '60', '70', '80', '90', '100', '-90', '-80'];
            minValue = 0;
            maxValue = 120;
        }*/

        /* if(Object.prototype.toString.call( opt.majorTicks ) === '[object Array]') { 
         majorTicks = opt.majorTicks;
         }
         else
         {
         majorTickInt = maxValue / 5;
         majorTicks = [ 0, majorTickInt, 2*majorTickInt, 3*majorTickInt, 4*majorTickInt, maxValue];
         opt.majorTicks = majorTicks;
         }*/

        highlights = [{
                from: minValue,
                to: maxValue,
                color: '#fff'
        }];
        if (Object.prototype.toString.call(opt.highlights) === '[object Array]')
        {
            highlights = opt.highlights;
            /*if (opt.elem == '__Powerfactor') {
                var pfLower = {
                    color: "#F0E68C",
                    from: 100,
                    to: 120
                }

                highlights.push(pfLower);
            }*/
        }
        else {
            opt.highlights = highlights;
        }
        this.options = opt;
        //console.log(this.options.elem);
        //console.log(Gauge.Collection);
        //if (typeof Gauge.Collection.get(this.options.elem) !== 'undefined') {
          //  this.remove();
        //}
        //console.log(this.options.elem);
        var $gaugeOpt = {
            renderTo: renderToElem,
            width: width,
            height: height,
            glow: true,
            units: units,
            title: title,
            minValue: minValue,
            maxValue: maxValue,
            majorTicks: majorTicks,
            minorTicks: 10,
            highlights: highlights,
            //valueFormat :{ int : maxValue.toString().length, dec : 1},
            animation: {
                delay: 10,
                duration: 500,
                fn: 'linear'
            }
        }
        //console.log($gaugeOpt);
        var gauge = new Gauge($gaugeOpt);
        gauge.draw();
        this.gauge = gauge;
        this.setValue(0);
        $('[data-toggle="tooltip"]').tooltip();
    };
    this.remove = function () {
        if (typeof Gauge.Collection.get(this.options.elem) !== 'undefined') {
            var currentElement = this.options.elem;
            Gauge.Collection.forEach(function (result, index) {
                if (result.config.renderTo == currentElement) {
                    //Remove from array
                    Gauge.Collection.splice(index, 1);
                }

            });
        }
    };
};
//initialize ionSlider
var __initSlider = function (elem, min, max) {


    var from = $(elem).data("from");
    var to = $(elem).data("to")

    if ($(elem).data("from") < min)
        from = min;
    else if ($(elem).data("from") > max)
        from = max;
    if ($(elem).data("to") < min)
        to = min;
    else if ($(elem).data("to") > max)
        to = max;
    $(elem).ionRangeSlider({
        min: min,
        max: max,
        type: 'double',
        prefix: "",
        from: from,
        to: to,
        maxPostfix: "+",
        prettify_enabled: false,
        grid: false,
        onFinish: function (data) {
            var $id = $(elem).attr("id");
            var $slid0 = $("#inp-color-0").prop("value").split(";");
            var $slid1 = $("#inp-color-1").prop("value").split(";");
            var $slid2 = $("#inp-color-2").prop("value").split(";");
            if ($id == "inp-color-0") {
                var s1f = data.to;
                $slid1[0] = s1f;
                var s1t = $slid1[1] > data.to ? $slid1[1] : data.to;
                $slid1[1] = s1t;
                var s2f = $slid2[0] > s1t ? $slid2[0] : s1t;
                $slid2[0] = s2f;
                var s2t = $slid2[1] > s1t ? $slid2[1] : s1t;
                $slid2[1] = s2t;
                $("#inp-color-1").data("ionRangeSlider").update({from: s1f, to: s1t});
                $("#inp-color-2").data("ionRangeSlider").update({from: s2f, to: s2t});
                $("#inp-color-1").prop("value", s1f + ";" + s1t);
                $("#inp-color-2").prop("value", s2f + ";" + s2t);
            }
            else if ($id == "inp-color-1") {
                var s0f = $slid0[0] < data.from ? $slid0[0] : data.from;
                $slid0[0] = s0f;
                var s0t = data.from;
                $slid0[1] = s0t;
                var s2f = data.to;
                $slid2[0] = s2f;
                var s2t = $slid2[1] > s2f ? $slid2[1] : s2f;
                $slid2[1] > s2t

                $("#inp-color-0").data("ionRangeSlider").update({from: s0f, to: s0t});
                $("#inp-color-2").data("ionRangeSlider").update({from: s2f, to: s2t});
                $("#inp-color-0").prop("value", s0f + ";" + s0t);
                $("#inp-color-2").prop("value", s2f + ";" + s2t);
            }
            else {

                var s1f = $slid1[0] <= data.from ? $slid1[0] : data.from;
                $slid1[0] = s1f;
                var s1t = data.from;
                $slid1[1] = s1t;
                var s0f = $slid0[0] <= s1f ? $slid0[0] : s1f;
                $slid0[0] = s0f;
                var s0t = s1f;
                $slid0[1] = s0t;
                $("#inp-color-1").data("ionRangeSlider").update({from: s1f, to: s1t});
                $("#inp-color-0").data("ionRangeSlider").update({from: s0f, to: s0t});
                $("#inp-color-1").prop("value", s1f + ";" + s1t);
                $("#inp-color-0").prop("value", s0f + ";" + s0t);
            }

            SetColorChanger();
            $(".inp-color-range-values").each(function () {
                var values = $(this).val().split(';');
                var cnt = $(this).prop("id").split('-')[2];
                $("#hdn-color-" + cnt).data('from', values[0]);
                $("#hdn-color-" + cnt).data('to', values[1]);
            });
        }
    });
};
//Cancel gauge settings
function CancelGaugeSettings() {

    $("#selGauge").select2("val", "");
    $('#div-box-gague-settings').hide();
    $('#div-box-gague-settings').html('');
}

//Save gauge settings
function ApplyGaugeSettings() {
    var checkedStatus = 0;
    var guageID = $("#selGauge").val();
    var guageLabel = $("#selGauge option:selected").text();
    url = 'AjaxCaller.jsp';
    var color = "";
    var colorOption = "";
    var colorNotInRange = false;
    if ($(".inp-color-range-values").length > 0) {

        color = "";
        colorOption = "";
        $(".inp-color-range-values").each(function () {
            var cnt = $(this).prop("id").split('-')[2];
            var values = [$("#hdn-color-" + cnt).data("from"), $("#hdn-color-" + cnt).data("to")];
            var selectedColor = $(this).data('color');
            if (selectedColor == "red")
                selectedColor = "#FFA07A";
            else if (selectedColor == "yellow")
                selectedColor = "#F0E68C";
            else if (selectedColor == "green")
                selectedColor = "#98F596";
            color += (color == "") ? "{\"from\" : " + values[0] + ",\"to\" : " + values[1] + " , \"color\" : \"" + selectedColor + "\" }" : ",{\"from\" : " + values[0] + ",\"to\" : " + values[1] + " , \"color\" : \"" + selectedColor + "\" }";
            colorOption += (colorOption == "") ? "{\"from\" : " + values[0] + ",\"to\" : " + values[1] + " , \"color\" : \"" + selectedColor + "\" }" : ",{\"from\" : " + values[0] + ",\"to\" : " + values[1] + " , \"color\" : \"" + selectedColor + "\" }";
            var minRangeVal = parseFloat($("#txt-range-start").val());
            var maxRangeVal = parseFloat($("#txt-range-end").val());
            if (parseFloat(values[0]) < minRangeVal || parseFloat(values[0]) > maxRangeVal)
                colorNotInRange = true;
            if (parseFloat(values[1]) < minRangeVal || parseFloat(values[1]) > maxRangeVal)
                colorNotInRange = true;
        });
        color = "[" + color + "]";
        colorOption = "[" + colorOption + "]";
    }

    if (colorNotInRange) {
        //alert("Guage color zone(s) overlaped.\nPlease correct color zone in range.");
        $createAlert({status: "fail", title: "Error!", text: "Guage color zone(s) overlaped.<br/>Please correct color zone in range."});
        return false;
    }


    //console.log(colorOption);
    var dataArr = {'c': 'Dashboard',
        'a': 'SaveGaugeSettings',
        'guageID': guageID,
        'min': $("#txt-range-start").val(),
        'max': $("#txt-range-end").val(),
        'majorticks': $("#txt-major-ticks").val(),
        'color': escape(color),
        'label': guageLabel};
    //console.log(color);
    //return false;

    $.ajax({
        type: 'POST',
        url: 'ajax.php',
        data: dataArr,
        dataType: 'json',
        beforeSend: function () {
            __showLoadingAnimation();
        },
        success: function (res) {
            if (res.component != '') {
                option = $('#selGauge option[value="' + guageID + '"]');
                option.data('min', $("#txt-range-start").val());
                option.data('max', $("#txt-range-end").val());
                option.data('major-ticks', $("#txt-major-ticks").val());
                option.data('color', colorOption);
                if (option.data('visible') == 1) {
                    if (typeof res.result[0] != "undefined") {
                        var identity = option.html().replace(/ /g, '');
                        //dentity = (identity == "Voltage480" || identity == "Voltage370" || identity == "Voltage208") ? "Voltage" : identity;
                        //console.log(identity);
                        
                        if ($gauges.oldGaugeCavas.length > 0) {
                            $gauges.oldGaugeCavas.forEach(function (renderElem, gid) {
                                var $slitIdentity = renderElem.split("_");
                                //console.log($slitIdentity);
                                if($slitIdentity[2] == identity){
                                    Gauge.Collection.forEach(function (result, index) {
                                        //console.log(result.config.renderTo + " = " + renderElem);
                                        if (result.config.renderTo == renderElem) {
                                            //Remove from collection
                                            Gauge.Collection.splice(index, 1);
                                        }
                                    });
                                    var $gidentity = $slitIdentity[2] + "_" + $slitIdentity[3];
                                    pageGuages[$gidentity].remove();
                                    delete pageGuages[$gidentity];
                                    $("#div_" + identity + "_" + $slitIdentity[3]).html("");
                                    //var guageValues = res.getValue;
                                    $gauges.ShowGuage(res.result[0], {"ID":$slitIdentity[3]}, "div_" + identity + "_" + $slitIdentity[3]);
                                    //if (guageValues.length > 0)
                                    //valueG = guageValues[0];
                                }
                            });
                        }
                       
                        
                        
                        
                    }
                    $createAlert({status: "success", title: "Successfully Queued", text: "Successfully request queued to update gauge settings"});
                }

                CancelGaugeSettings();
            }
        },
        complete: function () {
            __hideLoadingAnimation();
            $gauges.setLogData();
        },
        error: function (r) {
            __hideLoadingAnimation();
            console.log(r);
        }
    });
}

//Convert dataBase date to local date formate
function localDateFromate($date, $type) {
    /*$type = 1 //Full Date $type = 2 //Date $type = 3 Time*/
    var $splitDate = $date.split("-");
    var $date = $splitDate[2].split(" ");
    var $hrs = $date[1].split(":");
    var $rtval = "";
    if ($type == 1)
        $rtval = $splitDate[1] + "/" + $date[0] + "/" + $splitDate[0] + " " + $hrs[0] + ":" + $hrs[1] + ":" + $hrs[2];
    else if ($type == 2)
        $rtval = $splitDate[1] + "/" + $date[0] + "/" + $splitDate[0];
    else if ($type == 2)
        $rtval = $hrs[0] + ":" + $hrs[1] + ":" + $hrs[2];
    return $rtval
}

function getPageName() {
    var url = window.location.href;
    //console.log(url);
    var index = url.lastIndexOf("/") + 1;
    var filenameWithExtension = url.substr(index);
    var filename = filenameWithExtension.split(".")[0]; // <-- added this line
    return filename; // <-- added this line
}

function setLogData() {
    var pageName = getPageName();
    $.ajax({
        type: 'POST',
        url: 'ajax.php',
        data: {'c': 'Dashboard', 'a': 'GetGuageValue', 'pagename': pageName},
        dataType: 'json',
        success: function (res) {
            var $alertData = res.alerts;
            var $allDevices = res.allDevices;
            var $priorityAlerts = res.priorityAlert;
            //console.log(res);
            if (res.status == 1) {
                var $HB = res.reuslt;
                //console.log($HB);
                var $activeDev = res.deviceData;
                var minmaxvalues = res.minmaxValue;
                //var panelDetailData = res.panelDetailData;
                var deviceStatus = res.allDevices;
                var AVGMinMaxData = res.AVGMaxMinData[0];
                if (pageName == "dashboard") {
                    if ($HB.length > 0) {
                        ResetGaugesValues($HB[0]); //console.log("111");
                        showTemGuages($HB[0]);
                        if (typeof ($HB[0].logdate) != "undefined") {
                            var $logDate = $HB[0].logdate;
                            $dt = new Date($logDate);
                            $(".log-time").html(("0" + $dt.getHours()).slice(-2) + ":" + ("0" + $dt.getMinutes()).slice(-2));
                        }
                        else
                            $(".log-time").html("00:00");
                    }
                    else {
                        SetGaugesValuesMin(); //console.log("111");
                        $(".log-time").html("00:00");
                    }
                    showAlert($alertData);
                    showAVGMinMax(AVGMinMaxData);
                }
                else if (pageName == "panelDetails") {
                    if (typeof ($HB[0]) != "undefined" && typeof ($activeDev[0]) != "undefined") {
                        setParameterValues($HB[0], minmaxvalues[0], $activeDev[0]);
                        showCapacitanceGraph($HB[0]);
                        showTemGuages($HB[0]);
                        setBanksStatus($HB[0]);
                    }
                    else {

                        $("#panel-details .param-value").html("Nil");
                        __setKnobVal("#__capPh1", 0, "#4259D2", 1000);
                        __setKnobVal("#__capPh2", 0, "#4259D2", 1000);
                        __setKnobVal("#__capPh3", 0, "#4259D2", 1000);
                        //__setKnobVal("#__temMax",0,"#F7A044");
                        __setKnobVal("#__temint", 0, "#F7A044", 180);
                        __setKnobVal("#__temAmb", 0, "#F7A044", 180);
                        for (var i = 0; i <= 6; i++) {
                            var bankTitle = (i == 0 ? "Cap Bank 150" : i == 1 ? "Cap Bank 151" : i == 2 ? "Cap Bank 152" : i == 3 ? "Cap Bank 153" : i == 4 ? "Cap Bank 154" : i == 5 ? "Cap Bank 155" : "Cap Bank 156");
                            $("#bank" + i).html("<lable class='banks-lbl'>" + bankTitle + "</lable><div class='banks-status-lbl'> N/A</div>");
                            //$("#bank"+i).html(bankTitle +"<span class=''> N/A</span>");
                        }

                    }
                }
                else if (pageName == "parameters") {
                    //console.log($HB[0]);

                    var HBValue = $HB[0];
                    if ($HB.length > 0) {
                        //console.log(HBValue["athd"]);
                        _HB_ATHD = HBValue["athd"];
                        _HB_VTHD = HBValue["vthd"];
                        _HB_PEAKV_HARM = HBValue["v_harm"];
                        _HB_PEAKA_HARM = HBValue["a_harm"];
                    }
                    else {
                        _HB_ATHD = 0;
                        _HB_VTHD = 0;
                    }
                    if (typeof $activeDev[0] != "undefined") {

                        if (needParameterSet)
                            resetParamValue($HB[0], $activeDev[0]);
                        if (typeof HBValue != "undefined" && HBValue.hasOwnProperty("banks_status")) {
                            var banks = false;
                            if (parseInt(HBValue.banks_status) == 1)
                                banks = true;
                            /*var modelBased = 7;
                             
                             if( parseInt(HBValue.model_number) == 3 || parseInt(HBValue.model_number) == 4 || parseInt(HBValue.model_number) == 7 || parseInt(HBValue.model_number) == 8 || parseInt(HBValue.model_number) == 9 || parseInt(HBValue.model_number) == 10 || parseInt(HBValue.model_number) == 11)
                             modelBased = 6;
                             else if(parseInt(HBValue.model_number) == 1 || parseInt(HBValue.model_number) == 2 || parseInt(HBValue.model_number) == 5 || parseInt(HBValue.model_number) == 6)
                             modelBased = 4;
                             
                             for(i = 0; i < modelBased; i++){
                             if(parseInt(HBValue["bank" + i]) >= 128){
                             banks = true;
                             break;
                             }
                             }*/

                            if (banks) {
                                //console.log("active");
                                $(".switch-red").removeClass("hide");
                                $(".switch-green").addClass("hide");
                                $("#txtBankStatus").html("Banks are on.");
                                $("#txtBankStatus").removeClass("text-danger");
                                $("#txtBankStatus").addClass("text-success-dark");
                            }
                            else {
                                //console.log("inactive");
                                $(".switch-red").addClass("hide");
                                $(".switch-green").removeClass("hide");
                                $("#txtBankStatus").html("Banks are off.");
                                $("#txtBankStatus").removeClass("text-success-dark");
                                $("#txtBankStatus").addClass("text-danger");
                            }

                            $("#btnRecord").prop('disabled', false);
                        }
                        else {
                            $(".switch-red").removeClass("active disable");
                            $(".switch-green").removeClass("active disable");
                            $("#btnRecord").prop('disabled', true);
                        }
                    }
                }
                setDeviceStuts(deviceStatus, pageName);
                if (typeof ($activeDev[0]) != "undefined")
                    setActiveDevice($activeDev);
            }
            else {
                if (pageName == "dashboard") {
                    var $guages = res.reuslt;
                    ResetGaugesValues($guages[0]);
                }
            }

            if ($alertData.length > 0) {
                $("#__notify span").html($alertData.length);
                $("#__notify").removeClass("hide");
            }
            else
                $("#__notify").addClass("hide");
            if (pageName == "deviceManagement") {
                showDeviceAlert($priorityAlerts, $allDevices);
            }
        },
        error: function (r) {
            console.log(r);
        }
    });
}

/*START DASHBOARD*/

var pageGuages;
var orderJSON = {
    'DriverAKW': 1,
    'DriverBKW': 2,
    'Temperature': 3,
    'Motion': 4
};
function displayGuages() {
    $.ajax({
        type: 'POST',
        url: 'ajax.php',
        data: {'c': 'Dashboard', 'a': 'GetGuages'},
        dataType: 'json',
        success: function (res) {
            //console.log(res);
            if (res.status == 0) {
                var $allGuages = res.allGuage;
                var $guagesVal = res.getValue;
                var $guages = res.reuslt;
                var $alertData = res.alerts;
                var AVGMinMaxData = res.AVGMaxMinData[0];
                showTemGuages($guagesVal[0]);
                GuageDisplaySetting($allGuages);
                showAlert($alertData);
                guageSettingsList = $allGuages;
                setGuageSettings(res.modelNo);
                pageGuages = {};
                for (gCount = 0; gCount < $guages.length; gCount++) {
                    var guage = $guages[gCount];
                    //console.log($guagesVal);
                    ShowGuage(guage, $guagesVal[0]);
                }
                //console.log($guagesVal);
                if (typeof ($guagesVal[0]) != "undefined") {
                    if (typeof ($guagesVal[0].logdate) != "undefined") {
                        var $logDate = $guagesVal[0].logdate;
                        $dt = new Date($logDate);
                        $(".log-time").html(("0" + $dt.getHours()).slice(-2) + ":" + ("0" + $dt.getMinutes()).slice(-2));
                    }
                    else
                        $(".log-time").html("00:00");
                }
                else
                    $(".log-time").html("00:00");
                showAVGMinMax(AVGMinMaxData);
            }
        },
        error: function (r) {
            console.log(r);
        }
    });
}



function getVoltageMaxVal(model_number) {
    var maxVal = 480;
    switch (model_number) {
        case "SP1000A" :
            maxVal = 208;
            break;
        case "SP1000A2":
            maxVal = 208;
            break;
        case "SP1000B" :
            maxVal = 208;
            break;
        case "SP1000B2":
            maxVal = 208;
            break;
        case "SP1000C" :
            maxVal = 370;
            break;
        case "SP1000C2":
            maxVal = 370;
            break;
        case "SP1000D" :
            maxVal = 370;
            break;
        case "SP1000D2":
            maxVal = 370;
            break;
        case "SP1000E" :
            maxVal = 480;
            break;
        case "SP1000E2":
            maxVal = 480;
            break;
        case "SP1000E3":
            maxVal = 480;
            break;
        case "SP1000E7":
            maxVal = 480;
            break;
        case "SP1000E8":
            maxVal = 480;
            break;
        case "SP1000E9":
            maxVal = 480;
            break;
    }
    return maxVal;
}

//Get gauge value by identity
function GetGaugeValueByIdentity(identity, valueG) {
    if (identity == 'DriverAKW') {
        if (typeof (valueG) == 'undefined')
            return 0;
        else{
            var DriverAKW = (parseFloat($.isEmptyObject(valueG.VoltageMaster) ? 0 : valueG.VoltageMaster) *
                parseFloat($.isEmptyObject(valueG.VoltageMaster) ? 0 : valueG.DriverCurrent));
            return RoundOneDecimal(DriverAKW, 1);
        }
    }
    else if (identity == 'DriverBKW') {
        if (typeof (valueG) == 'undefined')
            return 0;
        else{
            var DriverBKW = (parseFloat($.isEmptyObject(valueG.SubVoltage) ? 0 : valueG.SubVoltage) *
                parseFloat($.isEmptyObject(valueG.DriverSubCurrent) ? 0 : valueG.DriverSubCurrent));
            return RoundOneDecimal(DriverBKW, 1);
        }
    }
    else if (identity == 'Temperature') {
        if (typeof (valueG) == 'undefined')
            return 0;
        else {
           // var temp = (parseFloat($.isEmptyObject(valueG.NodeTemp) ? 0 : valueG.NodeTemp) +
            //    parseFloat($.isEmptyObject(valueG.OCTemp) ? 0 : valueG.OCTemp) +
            //    parseFloat($.isEmptyObject(valueG.DriverTemp) ? 0 : valueG.DriverTemp))/3;
            return RoundOneDecimal(valueG.Temp, 1);
        }
    }
    
    else if (identity == 'Motion') {
        if (typeof (valueG) == 'undefined')
            return 0;
        else
            return  Math.floor(Math.random() * (0 - 1 + 1)) + 1;//$.isEmptyObject(valueG.OCStatus) ? 0 :valueG.OCStatus;
    }
    else
        return 0;
}

function GuageDisplaySetting(allGuages) {

    var popoverContent = '<ul class="guage-list" style="width:200px;">';
    //console.log(allGuages.length);
    for (gCount = 0; gCount < allGuages.length; gCount++) {
        var guage = allGuages[gCount];
        var identity = (guage.label).replace(/ /g, '');
        //console.log(identity);
        if (gCount == 0) {
            popoverContent += "<li class='p-l-0'>";
            popoverContent += '<div class="checkbox check-primary m-t-0 m-b-0">';
            popoverContent += '<input type="checkbox" data-guage-id="all" id="Sel-all" value="All" onclick="javascript:ShowAllGuages(this);" >';
            popoverContent += '<label for="Sel-all">Select All</label>';
            popoverContent += '</div>';
            popoverContent += '</li>';
        }
        
        popoverContent += "<li class='p-l-0'>";
        popoverContent += '<div class="checkbox check-primary m-t-0 m-b-0">';
        popoverContent += '<input type="checkbox" data-guage-id="' + guage.ID + '" class="gauge_list_check" id="Sel-' + identity + '" value="' + identity + '" onclick="javascript:ShowHideGuages(this);" >';
        popoverContent += '<label for="Sel-' + identity + '">' + guage.label + '</label>';
        popoverContent += '</div>';
        popoverContent += '</li>';
    }

    popoverContent += '</ul>';
    $("#div-all-guage-popover-content").html(popoverContent);
    $('#btnSelguage').popover({
        "html": true, template: '<div class="popover guage-popover"><div class="arrow"></div><h5 class="popover-title bold bg-white"></h5><div class="popover-content clearfix"></div></div>',
        "content": $("#div-all-guage-popover-content").html(),
        "container": "#portlet-advance",
        "placement": "bottom"/*,
         "trigger":"focus"*/
    })
            .on('shown.bs.popover', function () {
                var as = 0;
                var ts = $("#div-all-guage-popover-content li").length - 1;
                /*$("#div-all-guage-popover-content").find("input[type=checkbox]:checked").each(function(){
                 $(this).prop('checked',false);
                 ts++;
                 });*/
                $("#guageContainer").children().not('#div-guage-setting-block').each(function () {
                    var idSplit = this.id.split('_');
                    //console.log(idSplit[1]);
                    $("#Sel-" + idSplit[1]).prop("checked", true);
                    as++;
                });
                //console.log( ts+"=="+ as);
                if (ts == as)
                    $("#Sel-all").prop("checked", true);
            })
            .on('hidden.bs.popover', function ()
            {
                /*$("#div-all-guage-popover-content").find("input[type=checkbox]:checked").each(function(){
                 $(this).prop('checked',false);
                 });*/
            }).click(function (e) {
        e.preventDefault();
        // Exibe o popover.
        $('.popover').css('left', '0px');
    });
}

function ShowAllGuages(obj) {
    var checkedStatus = 0;
    if ($(obj).is(':checked'))
        checkedStatus = 1;
    $.ajax({
        type: 'POST',
        url: 'ajax.php',
        data: {'c': 'Dashboard', 'a': 'SetAllGuagesVisibility', 'checkedStatus': checkedStatus},
        dataType: 'json',
        beforeSend: function () {
            __showLoadingAnimation();
        },
        success: function (res) {
            if (checkedStatus == 1) {
                var ts = $("#div-all-guage-popover-content li").length - 1;
                //console.log(res.reuslt.length);
                if (res.reuslt.length > 0) {
                    for (var i = 0; i < res.reuslt.length; i++) {
                        valueG = res.getValue[i];
                        ShowGuage(res.reuslt[i], valueG);
                        if (res.reuslt[i].label == "Voltage")
                            $('input[data-guage-id="0"]').prop("checked", true);
                        else
                            $('input[data-guage-id="' + res.reuslt[i].ID + '"]').prop("checked", true);
                    }
                    if (res.getValue[0] > 0)
                        ResetGaugesValues(res.getValue[0]);
                }
            }
            else {
                try {
                    for (var i = 0; i < res.reuslt.length; i++) {
                        var identity = "";
                        var label = res.reuslt[i].label;
                        if (label != "KWH" && label != "KVA" && label != "KVAR") {
                            //console.log(res.reuslt[i].label);
                            if (label == "Voltage") {
                                // console.log("str"+$('input[data-guage-id="0"]').val());
                                identity = $('input[data-guage-id="0"]').val();
                                $('input[data-guage-id="0"]').prop("checked", false);
                            }
                            else {
                                identity = $('input[data-guage-id="' + res.reuslt[i].ID + '"]').val();
                                $('input[data-guage-id="' + res.reuslt[i].ID + '"]').prop("checked", false);
                            }

                            //console.log(identity);
                            pageGuages[identity].remove();
                            delete pageGuages[identity];
                            $("#div_" + identity).html("");
                            $("#div_" + identity).remove();
                        }
                    }
                }
                catch (ex) {
                    //alert(JSON.stringify(ex));
                }
            }
        },
        complete: function () {
            __hideLoadingAnimation();
        },
        error: function (r) {
            __hideLoadingAnimation();
            console.log(r);
        }
    });
}

//Show or hide guages
function ShowHideGuages(obj) {

    var checkedStatus = 0;
    var guageID = $(obj).data("guage-id");
    var identity = $(obj).val();
    if ($(obj).is(':checked')) {
        checkedStatus = 1;
        var actualSel = $(".popover-content").find('.gauge_list_check:checked').length;
        var totalSel = $("#div-all-guage-popover-content li").length - 1
        //console.log(actualSel+"==="+totalSel);
        if (actualSel == totalSel)
            $('input[data-guage-id = "all"]').prop("checked", true);
    }
    else
        $('input[data-guage-id = "all"]').prop("checked", false);
    $.ajax({
        type: 'POST',
        url: 'ajax.php',
        data: {'c': 'Dashboard', 'a': 'SetGuageVisibility', 'guageID': guageID, 'checkedStatus': checkedStatus},
        dataType: 'json',
        beforeSend: function () {
            __showLoadingAnimation();
        },
        success: function (res) {
            //console.log(res);
            if (checkedStatus == 1) {
                //console.log(res.reuslt.length);
                if (res.reuslt.length > 0) {
                    ShowGuage(res.reuslt[0]);
                    if (res.getValue > 0)
                        ResetGaugesValues(res.getValue[0]);
                }
            }
            else {
                try {
                    //console.log(identity);
                    pageGuages[identity].remove();
                    //console.log(222);
                    delete pageGuages[identity];
                    //console.log(333);
                    $("#div_" + identity).html("");
                    $("#div_" + identity).remove();
                }
                catch (ex) {
                    alert(JSON.stringify(ex));
                }
            }
        },
        complete: function () {
            __hideLoadingAnimation();
        },
        error: function (r) {
            __hideLoadingAnimation();
            console.log(r);
        }
    });
}

function setGuageSettings(model) {
    var guages = guageSettingsList;
    var sel = $('#selGauge');
    sel.empty();
    sel.append('<option value="">Select a gauge to edit settings</option>');
    //console.log("Gauge Len === "+guages.length);
    for (var i = 0; i < guages.length; i++) {
        //if (model == 0) {
            sel.append('<option value="' + guages[i].ID +
                    '" data-unit="' + guages[i].unit +
                    '" data-auto-k="' + guages[i].auto_k +
                    '" data-is-avg="' + guages[i].auto_range +
                    '" data-min="' + guages[i].min_val +
                    '" data-max="' + guages[i].max_val +
                    '" data-major-ticks = "' + guages[i].markers +
                    '" data-cus= "' + guages[i].custom +
                    '" data-g-minval= "' + guages[i].min_mark_val +
                    '" data-g-model-no= "' + guages[i].model_no +
                    '" data-color=\'' + escape(guages[i].color) +
                    '\' data-visible="' + guages[i].display + '" >' +  guages[i].label + '</option>');
       /* }
        else {
            if (guages[i].model_no == model || guages[i].model_no == 0) {
                sel.append('<option value="' + guages[i].ID +
                        '" data-unit="' + guages[i].unit +
                        '" data-auto-k="' + guages[i].auto_k +
                        '" data-is-avg="' + guages[i].auto_range +
                        '" data-min="' + guages[i].min_val +
                        '" data-max="' + guages[i].max_val +
                        '" data-major-ticks = "' + guages[i].markers +
                        '" data-cus= "' + guages[i].custom +
                        '" data-g-minval= "' + guages[i].min_mark_val +
                        '" data-g-model-no= "' + guages[i].model_no +
                        '" data-color=\'' + escape(guages[i].color) +
                        '\' data-visible="' + guages[i].display + '" >' +
                        guages[i].label + " " + guages[i].model + '</option>');
            }
        }*/

    }
    $("#selGauge").select2({});
}

function showTemGuages(valueG) {

    var $max = 0, $int = 0, $amb = 0;
    var $maxColor = "", $intColor = "", $ambColor = "";
    //console.log(valueG);
    if (typeof (valueG) != 'undefined') {
        if (typeof (valueG.tint) != 'undefined')
            $int = parseInt(valueG.tint);
        if (typeof (valueG.tamb) != 'undefined')
            $amb = parseInt(valueG.tamb);
        if ($int > $amb)
            $max = $int;
        else
            $max = $amb;
    }
    $maxColor = ($max < 130) ? "#99F969" : ($max > 130 && $max < 160) ? "#F7A044" : "#F97E7A";
    $intColor = $int < 110 ? "#99F969" : $int > 110 && $int < 140 ? "#F7A044" : "#F97E7A";
    $ambColor = $amb < 100 ? "#99F969" : $amb > 100 && $amb < 120 ? "#F7A044" : "#F97E7A";
    //__setKnobVal("#__temMax",$max,$maxColor);
    __setKnobVal("#__temint", $int, $intColor, 180);
    __setKnobVal("#__temAmb", $amb, $ambColor, 180);
}

function showCapacitanceGraph(valueG) {

    var $cap1 = 0, $cap2 = 0, $cap3 = 0;
    var $cap1Color = "", $cap2Color = "", $cap3Color = "";
    //console.log(valueG);
    if (typeof (valueG) != 'undefined') {
        if (typeof (valueG.caps1) != 'undefined')
            $cap1 = parseInt(valueG.caps1);
        if (typeof (valueG.caps2) != 'undefined')
            $cap2 = parseInt(valueG.caps2);
        if (typeof (valueG.caps3) != 'undefined')
            $cap3 = parseInt(valueG.caps3);
    }
    $cap1Color = "#4259D2";
    $cap2Color = "#4259D2";
    $cap3Color = "#4259D2";
    __setKnobVal("#__capPh1", $cap1, $cap1Color, 1000);
    __setKnobVal("#__capPh2", $cap2, $cap2Color, 1000);
    __setKnobVal("#__capPh3", $cap3, $cap3Color, 1000);
    /*if($cap1 <= 1000) __setKnobVal("#__capPh1",$cap1,$cap1Color);
     else __intknobBarCaps("#__capPh1",5000,$cap1Color);
     
     if($cap2 <= 1000) __setKnobVal("#__capPh2",$cap2,$cap2Color);
     else __intknobBarCaps("#__capPh2",5000,$cap2Color);
     
     if($cap3 <= 1000) __setKnobVal("#__capPh3",$cap3,$cap3Color);
     else __intknobBarCaps("#__capPh3",5000,$cap3Color);*/

}


//Initialize or Reset guage values
function SetGaugesValuesMin() {

    $(".canvas-guage-pmi").each(function () {
        var identity = $(this).data('vaiable');
        var elemID = $(this).prop('id');
        if (identity != "KWH" && identity != "KVA" && identity != "KVAR") {
            //console.log(identity+">>"+pageGuages[identity].options.minValue);
            pageGuages[identity].setValue(0);
        }
    });
}
//END DASHBOARD

//Change Active Device Status
function setActiveDevice($activeDev) {
    var $result = $activeDev[0];
    var $modelName = ($modelArr.hasOwnProperty($result.model_number)) ? " (<span class='fs-12 bold'>" + $modelArr[$result.model_number].ModelNo + "</span>)" : " (<span class='fs-12 bold'>Unknown</span>)";
    $("#_popDeviceList").find(".deviceName").html($result.device_name + $modelName);
    //console.log($activeDev);
    if ($result.status == 0) {
        $("#_popDeviceList").find(".deviceStatus").removeClass("bg-green bg-green-ball blink");
        $("#_popDeviceList").find(".deviceStatus").addClass("bg-red bg-red-ball");
        $('#_popDeviceList').find(".device").prop('title', 'Device inactive');
    }
    else if ($result.status == 1) {
        $("#_popDeviceList").find(".deviceStatus").removeClass("bg-red bg-red-ball");
        $("#_popDeviceList").find(".deviceStatus").addClass("bg-green bg-green-ball blink");
        $('#_popDeviceList').find(".device").prop('title', 'Device active');
    }
    $('#_popDeviceList').find(".device").tooltip();
}

/*BANKS STATUS*/
function setBanksStatus(HBValue) {

    for (var i = 0; i <= 6; i++) {
        var status = (i == 0 ? HBValue.bank0 : i == 1 ? HBValue.bank1 : i == 2 ? HBValue.bank2 : i == 3 ? HBValue.bank3 : i == 4 ? HBValue.bank4 : i == 5 ? HBValue.bank5 : HBValue.bank6);
        var bankTitle = (i == 0 ? "Cap Bank 150" : i == 1 ? "Cap Bank 151" : i == 2 ? "Cap Bank 152" : i == 3 ? "Cap Bank 153" : i == 4 ? "Cap Bank 154" : i == 5 ? "Cap Bank 155" : "Cap Bank 156");
        if (parseInt(status) >= 128) {
            $("#bank" + i).html("<lable class='banks-lbl'>" + bankTitle + "</lable><div class='banks-status-lbl banks-on'> OK</div>");
        }
        else {
            $("#bank" + i).html("<lable class='banks-lbl'>" + bankTitle + "</lable><div class='banks-status-lbl banks-off'> OFF</div>");
        }
    }

    /*if(parseInt(HBValue.model_number) == 12 || parseInt(HBValue.model_number) == 13)
     $("#bank6").html("<lable class='banks-lbl'>Cap Bank 156</lable><div class='banks-status-lbl'> N/A</div>");*/

    if (parseInt(HBValue.model_number) == 3 || parseInt(HBValue.model_number) == 4 || parseInt(HBValue.model_number) == 7 || parseInt(HBValue.model_number) == 8 || parseInt(HBValue.model_number) == 9 || parseInt(HBValue.model_number) == 10 || parseInt(HBValue.model_number) == 11)
        $("#bank6").html("<lable class='banks-lbl'>Cap Bank 156</lable><div class='banks-status-lbl'> N/A</div>");
    else if (parseInt(HBValue.model_number) == 1 || parseInt(HBValue.model_number) == 2 || parseInt(HBValue.model_number) == 5 || parseInt(HBValue.model_number) == 6) {
        $("#bank6").html("<lable class='banks-lbl'>Cap Bank 156</lable><div class='banks-status-lbl'> N/A</div>");
        $("#bank5").html("<lable class='banks-lbl'>Cap Bank 155</lable><div class='banks-status-lbl'> N/A</div>");
        $("#bank4").html("<lable class='banks-lbl'>Cap Bank 154</lable><div class='banks-status-lbl'> N/A</div>");
    }

}

/*PANEL DETAILS PARAMETER VALUE SET*/
function setParameterValues(parameterValue, minmax, activeDevice) {
    // alert("hiiii");

    var $datetime = parameterValue['logdate'];
    var $date = new Date($datetime);
    //var date = $datetime.split(' ')[0];
    var time = $datetime.split(' ')[1];
    var $volt3Ph = RoundOneDecimal(((parseFloat(parameterValue['volts_ph1']) + parseFloat(parameterValue['volts_ph2']) + parseFloat(parameterValue['volts_ph3'])) / Math.sqrt(3)), 1);
    var $amp3Ph = RoundOneDecimal(((parseFloat(parameterValue['amps1']) + parseFloat(parameterValue['amps2']) + parseFloat(parameterValue['amps3'])) / Math.sqrt(3)), 1);
    //var $avr3PhPF = ((parseFloat(parameterValue['pf1']) + parseFloat(parameterValue['pf2']) + parseFloat(parameterValue['pf3']))/3).toFixed(2);
    var $avr3PhPF = 0.0;
    if ((parseFloat(parameterValue['apar_pwr1']) + parseFloat(parameterValue['apar_pwr2']) + parseFloat(parameterValue['apar_pwr3'])) != 0) {
        $avr3PhPF = (parseFloat(parameterValue['pf1']) * parseFloat(parameterValue['apar_pwr1']) +
                parseFloat(parameterValue['pf2']) * parseFloat(parameterValue['apar_pwr2']) +
                parseFloat(parameterValue['pf3']) * parseFloat(parameterValue['apar_pwr3'])) /
                (parseFloat(parameterValue['apar_pwr1']) +
                        parseFloat(parameterValue['apar_pwr2']) +
                        parseFloat(parameterValue['apar_pwr3']));
    }

    $avr3PhPF = RoundOneDecimal($avr3PhPF, 1);
    /*var $apprnt3PhPwr = (parseFloat($pwrPh1) + parseFloat($pwrPh2) + parseFloat($pwrPh3)).toFixed(3);
     var $realPh1Pwr = ((parameterValue['volts_ph1'] * parameterValue['amps1'] * parameterValue['pf1'])/1000).toFixed(3);  //kW
     var $realPh2Pwr = ((parameterValue['volts_ph2'] * parameterValue['amps2'] * parameterValue['pf2'])/1000).toFixed(3);
     var $realPh3Pwr = ((parameterValue['volts_ph3'] * parameterValue['amps3'] * parameterValue['pf3'])/1000).toFixed(3);
     var $real3PhPwr = (parseFloat($realPh1Pwr) + parseFloat($realPh2Pwr) + parseFloat($realPh3Pwr)).toFixed(3);
     var $reactPh1Pwr = ((Math.sqrt(($pwrPh1*$pwrPh1)-($realPh1Pwr*$realPh1Pwr)))/1000).toFixed(3);  //kVAR
     var $reactPh2Pwr = ((Math.sqrt(($pwrPh2*$pwrPh2)-($realPh2Pwr*$realPh2Pwr)))/1000).toFixed(3);
     var $reactPh3Pwr = ((Math.sqrt(($pwrPh3*$pwrPh3)-($realPh3Pwr*$realPh3Pwr)))/1000).toFixed(3);
     var $react3PhPwr = (Math.sqrt(($apprnt3PhPwr * $apprnt3PhPwr)-($real3PhPwr * $real3PhPwr))).toFixed(3); */

    var $pwrPh1 = RoundOneDecimal((parameterValue['volts_ph1'] * parameterValue['amps1']), 1);
    var $pwrPh2 = RoundOneDecimal((parameterValue['volts_ph2'] * parameterValue['amps2']), 1);
    var $pwrPh3 = RoundOneDecimal((parameterValue['volts_ph3'] * parameterValue['amps3']), 1);
    var $real1 = RoundOneDecimal((parameterValue['volts_ph1'] * parameterValue['amps1'] * (parameterValue['pf1'] / 100)), 1);
    var $real2 = RoundOneDecimal((parameterValue['volts_ph2'] * parameterValue['amps2'] * (parameterValue['pf2'] / 100)), 1);
    var $real3 = RoundOneDecimal((parameterValue['volts_ph3'] * parameterValue['amps3'] * (parameterValue['pf3'] / 100)), 1);
    var $apprPwrPh1 = RoundOneDecimal(((parameterValue['apar_pwr1']) / 1000), 1); //kVA
    var $apprPwrPh2 = RoundOneDecimal(((parameterValue['apar_pwr2']) / 1000), 1);
    var $apprPwrPh3 = RoundOneDecimal(((parameterValue['apar_pwr3']) / 1000), 1);
    var $apprnt3PhPwr = RoundOneDecimal(((parseFloat($pwrPh1) + parseFloat($pwrPh2) + parseFloat($pwrPh3)) / 1000), 1);
    var $realPh1Pwr = RoundOneDecimal(((parameterValue['real_pwr1']) / 1000), 1); //kW
    var $realPh2Pwr = RoundOneDecimal(((parameterValue['real_pwr2']) / 1000), 1);
    var $realPh3Pwr = RoundOneDecimal(((parameterValue['real_pwr3']) / 1000), 1);
    var $real3PhPwr = RoundOneDecimal(((parseFloat($real1) + parseFloat($real2) + parseFloat($real3)) / 1000), 1);
    var $reactPh1Pwr = RoundOneDecimal(((parameterValue['react_pwr1']) / 1000), 1); //kVAR
    var $reactPh2Pwr = RoundOneDecimal(((parameterValue['react_pwr2']) / 1000), 1);
    var $reactPh3Pwr = RoundOneDecimal(((parameterValue['react_pwr3']) / 1000), 1);
    var $react3PhPwr = RoundOneDecimal((Math.sqrt(($apprnt3PhPwr * $apprnt3PhPwr) - ($real3PhPwr * $real3PhPwr))), 1);
    /*var $pwrImb = 0;
     if((parseFloat($pwrPh1) + parseFloat($pwrPh2) + parseFloat($pwrPh3)) != 0){
     $pwrImb = Math.max($pwrPh1,$pwrPh2,$pwrPh3)/((parseFloat($pwrPh1) + parseFloat($pwrPh2) + parseFloat($pwrPh3))/3);
     }
     $pwrImb = RoundOneDecimal($pwrImb,1);*/
    $(".param-value").html("");
    if (parameterValue.length == 0) {
        $("#panel-details .param-value").html("Nil");
    }
    else {
        $("#time").html(("0" + $date.getHours()).slice(-2) + ":" + ("0" + $date.getMinutes()).slice(-2));
        $("#date").html(localDateFromate($datetime, 2));
        $("#serial").html(parameterValue['serial_no']);
        $("#mainRev").html(RoundOneDecimal(parameterValue['main_rev'], 1));
        $("#interRev").html(RoundOneDecimal(parameterValue['inter_rev'], 1));
        $("#meterRev").html(RoundOneDecimal(parameterValue['meter_rev'], 1));
        $("#3PhVolt").html($volt3Ph);
        $("#VoltPh1").html(RoundOneDecimal(parameterValue['volts_ph1'], 1));
        $("#VoltPh2").html(RoundOneDecimal(parameterValue['volts_ph2'], 1));
        $("#VoltPh3").html(RoundOneDecimal(parameterValue['volts_ph3'], 1));
        $("#Min3PhVolt").html(RoundOneDecimal(minmax['minvolt'], 1));
        $("#Max3PhVolt").html(RoundOneDecimal(minmax['maxvolt'], 1));
        $("#3PhAmps").html($amp3Ph);
        $("#AmpsPh1").html(RoundOneDecimal(parameterValue['amps1'], 1));
        $("#AmpsPh2").html(RoundOneDecimal(parameterValue['amps2'], 1));
        $("#AmpsPh3").html(RoundOneDecimal(parameterValue['amps3'], 1));
        $("#Min3PhAmps").html(RoundOneDecimal(minmax['mincurrent'], 1));
        $("#Max3PhAmps").html(RoundOneDecimal(minmax['maxcurrent'], 1));
        $("#Avr3PhPF").html($avr3PhPF + " %");
        $("#AvrPh1PF").html(RoundOneDecimal(parameterValue['pf1'], 1) + " %");
        $("#AvrPh2PF").html(RoundOneDecimal(parameterValue['pf2'], 1) + " %");
        $("#AvrPh3PF").html(RoundOneDecimal(parameterValue['pf3'], 1) + " %");
        $("#MinAvr3PhPF").html(RoundOneDecimal(minmax['minpower'], 1) + " %");
        $("#MaxAvr3PhPF").html(RoundOneDecimal(minmax['maxpower'], 1) + " %");
        $("#Appar3PhPwr").html($apprnt3PhPwr.toString() + "<span class='fs-10'> KVA</span>");
        $("#ApparPh1Pwr").html($apprPwrPh1.toString() + "<span class='fs-10'> KVA</span>");
        $("#ApparPh2Pwr").html($apprPwrPh2.toString() + "<span class='fs-10'> KVA</span>");
        $("#ApparPh3Pwr").html($apprPwrPh3.toString() + "<span class='fs-10'> KVA</span>");
        $("#Real3PhPwr").html($real3PhPwr.toString() + "<span class='fs-10'> KW</span>");
        $("#RealPh1Pwr").html($realPh1Pwr.toString() + "<span class='fs-10'> KW</span>");
        $("#RealPh2Pwr").html($realPh2Pwr.toString() + "<span class='fs-10'> KW</span>");
        $("#RealPh3Pwr").html($realPh3Pwr.toString() + "<span class='fs-10'> KW</span>");
        $("#React3PhPwr").html($react3PhPwr.toString() + "<span class='fs-10'> KVAR</span>");
        $("#ReactPh1Pwr").html($reactPh1Pwr.toString() + "<span class='fs-10'> KVAR</span>");
        $("#ReactPh2Pwr").html($reactPh2Pwr.toString() + "<span class='fs-10'> KVAR</span>");
        $("#ReactPh3Pwr").html($reactPh3Pwr.toString() + "<span class='fs-10'> KVAR</span>");
        $("#VoltageImb").html(RoundOneDecimal(parameterValue['volts_imb'], 1) + " %");
        $("#CurrentImb").html(RoundOneDecimal(parameterValue['amps_imb'], 1) + " %");
        $("#LineFrq").html(RoundOneDecimal(parameterValue['line_frq'], 1));
        $("#Max24hVimb").html(RoundOneDecimal(minmax['maxvoltimb'], 1));
        $("#Max24hAimb").html(RoundOneDecimal(minmax['maxcurrentimb'], 1));
        $("#VoltsTHD").html(RoundOneDecimal(parameterValue['vthd'], 1) + " %");
        $("#AmpsTHD").html(RoundOneDecimal(parameterValue['athd'], 1) + " %");
        $("#FirstHarm").html(RoundOneDecimal(parameterValue['h1'], 1) + " %");
        $("#ThirdHarm").html(RoundOneDecimal(parameterValue['h3'], 1) + " %");
        $("#FifthHarm").html(RoundOneDecimal(parameterValue['h5'], 1) + " %");
        $("#SeventhHarm").html(RoundOneDecimal(parameterValue['h7'], 1) + " %");
        $("#NinthHarm").html(RoundOneDecimal(parameterValue['h9'], 1) + " %");
        $("#EleventhHarm").html(RoundOneDecimal(parameterValue['h11'], 1) + " %");
        $("#PeakVHarm").html(RoundOneDecimal(parameterValue['v_harm'], 1) + " %");
        $("#PeakVHFrq").html(RoundOneDecimal(parameterValue['vh_freq'], 1));
        $("#PeakAHarm").html(RoundOneDecimal(parameterValue['a_harm'], 1) + " %");
        $("#PeakAHFrq").html(RoundOneDecimal(parameterValue['ah_freq'], 1));
        $("#V1CalSlope").html(RoundOneDecimal(parameterValue['v1_slope'], 5));
        $("#V2CalSlope").html(RoundOneDecimal(parameterValue['v2_slope'], 5));
        $("#V3CalSlope").html(RoundOneDecimal(parameterValue['v3_slope'], 5));
        $("#A1CalSlope").html(RoundOneDecimal(parameterValue['a1_slope'], 5));
        $("#A2CalSlope").html(RoundOneDecimal(parameterValue['a2_slope'], 5));
        $("#A3CalSlope").html(RoundOneDecimal(parameterValue['a3_slope'], 5));
        $("#V1CalInter").html(RoundOneDecimal(parameterValue['v1_inter'], 5));
        $("#V2CalInter").html(RoundOneDecimal(parameterValue['v2_inter'], 5));
        $("#V3CalInter").html(RoundOneDecimal(parameterValue['v3_inter'], 5));
        $("#A1CalInter").html(RoundOneDecimal(parameterValue['a1_inter'], 5));
        $("#A2CalInter").html(RoundOneDecimal(parameterValue['a2_inter'], 5));
        $("#A3CalInter").html(RoundOneDecimal(parameterValue['a3_inter'], 5));
        $("#neutralAmps").html(RoundOneDecimal(parameterValue['neutral_amps'], 1));
        $("#multiplier").html(RoundOneDecimal(parameterValue['amps_multiplier'], 1));
        $("#sysStatus").html(parameterValue['status']);
        $("#softLock").html(parameterValue['softLock']);
    }

}

/*round desimal pont*/
function RoundOneDecimal(number, desPoint) {
    //number = parseFloat(number);
    if (typeof desPoint == "undefined")
        desPoint = 0;
    number = (number == null || number == NaN) ? 0 : number;
    try {
        number = parseFloat(number);
    }
    catch (e) {
        number = 0;
    }

    nval = number;
    var nval = Math.floor(Math.abs(number) * Math.pow(10, desPoint)) / Math.pow(10, desPoint);
    nval = (number < 0) ? -1 * nval : nval;
    //if(number != 0) console.log(number + "====>"+nval);
    return nval;
}

//Show Device Alert
function showDeviceAlert(alertData, allDevices) {
    //alert(alertData)
    var deviceID = '';
    var alertDataArr = new Array();
    var severityArr = new Array();
    for (var i = 0; i < alertData.length; i++) {
        if (alertDataArr.indexOf(alertData[i]['DeviceID']) == -1) {
            alertDataArr.push(alertData[i]['DeviceID']);
            severityArr.push(alertData[i]['severity']);
        }
    }
    //alert(alertDataArr);
    //alert(severityArr);
    for (var j = 0; j < allDevices.length; j++)
    {
        deviceID = allDevices[j]['id'];
        if (alertDataArr.indexOf(deviceID) != -1) {
            var index = alertDataArr.indexOf(deviceID);
            if (severityArr[index] == "Caution") {
                $("#deviceBox_" + deviceID).find("#deviceAlert").attr("class", "notify-bubble bg-yellow bg-yellow-ball m-r-10");
                $("#deviceBox_" + deviceID).find("#deviceAlert").attr("data-original-title", "Caution");
            }
            else {
                $("#deviceBox_" + deviceID).find("#deviceAlert").attr("class", "notify-bubble bg-red flash bg-red-ball m-r-10");
                $("#deviceBox_" + deviceID).find("#deviceAlert").attr("data-original-title", "Critical");
            }

        }
        else {
            $("#deviceBox_" + deviceID).find("#deviceAlert").attr("class", "notify-bubble bg-grey bg-grey-ball m-r-10");
            $("#deviceBox_" + deviceID).find("#deviceAlert").attr("data-original-title", "No Alerts");
        }
    }

}

function isNumber(value) {
    //alert(value);
    if (!/^[-+]?[0-9]+$/.test(value)) {
        return false;
    }
    return true;
}

function isDecimal(value) {
    if (!/^[-+]?\d{0,10}(\.\d{0,2})?$/i.test(value)) {
        return false;
    }
    return true;
}

function ValidateIPaddress(inputText) {
    var ipformat = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
    if (inputText.match(ipformat)) {
        return true;
    }
    else {
        return false;
    }
}

function CheckDecimal(inputtxt) {
    var decimal = /^[-+]?[0-9]+\.[0-9]+$/;
    if (inputtxt.match(decimal)) {
        return true;
    }
    else {
        return false;
    }
}

function selectValidation($id, $name, $parentDiv, $errorContainerDiv) {
    var $elemVal = $("#" + $id).select2("val");
    //console.log($errorContainerDiv );
    if ($elemVal == "-1") {
        $("#" + $parentDiv).addClass("has-error");
        if (typeof $errorContainerDiv == "undefined") {
            $("#" + $parentDiv + " .sp_error").html("Please Select a " + $name);
            $("#" + $parentDiv + " .sp_error").removeClass("hide");
        }
        else {
            $("#" + $errorContainerDiv + " .sp_error").html("Please Select a " + $name);
            $("#" + $errorContainerDiv + " .sp_error").removeClass("hide");
        }
        $("#" + $id).focus();
        return false;
    }
    else {
        $("#" + $parentDiv).removeClass("has-error");
        if (typeof $errorContainerDiv == "undefined") {
            $("#" + $parentDiv + " .sp_error").html("");
            $("#" + $parentDiv + " .sp_error").addClass("hide");
        }
        else {
            $("#" + $errorContainerDiv + " .sp_error").html("");
            $("#" + $errorContainerDiv + " .sp_error").addClass("hide");
        }
        return true;
    }
}

function multiSelValidation($id, $name, $parentDiv) {
    var $elemVal = $("#" + $id).select2("val");
    //console.log($elemVal);
    if ($elemVal == null || $elemVal.length == 0) {
        $("#" + $parentDiv).addClass("has-error");
        $("#" + $parentDiv + " .sp_error").html("Please select at least one " + $name);
        $("#" + $parentDiv + " .sp_error").removeClass("hide");
        $("#" + $id).focus();
        //console.log("$elemVal");
        return false;
    }
    else {
        $("#" + $parentDiv).removeClass("has-error");
        $("#" + $parentDiv + " .sp_error").html("");
        $("#" + $parentDiv + " .sp_error").addClass("hide");
        return true;
    }
}

/*Notification Alert*/
var $createAlert = function createAlert(opts) {
    var status, title, text, alertTemplate, color, icon;
    if (opts === undefined) {
        opts = {};
    }

    status = opts.status || '';
    title = opts.title || '';
    text = opts.text || '';
    //color = opts.color || '';

    if (status != "") {
        if (status == "success") {
            icon = "check";
            color = "text-success";
        }
        else if (status == "fail") {
            icon = "exclamation-triangle";
            color = "text-danger";
        }
        else if (status == "info") {
            icon = "info-circle";
        }
    }
    if ($(".nn-alert-container").length == 0) {
        //console.log("container Created");
        $("body").append("<div class='nn-alert-container'></div>");
        var $nnAlert = $('.nn-alert-container');
        $nnAlert.on('click', '.nn-alert', function () {
            hideAlert($(this));
        });
    }
    var $id = new Date().getTime();
    alertTemplate = '<div class="nn-alert cursor nn-alert-custom animated pgn-flip " id="__' + $id + '" ><i class="fa fa-' + icon + ' ' + color + '"></i><strong>' + title + '</strong><p style="padding-left:43px;">' + text + '</p></div>';
    $(alertTemplate).prependTo('.nn-alert-container');
    setTimeout(function () {
        hideAlert($("#__" + $id));
    }, 5000); //slideInDown

}

function hideAlert($alert) {
    $alert.removeClass('pgn-flip');
    $alert.addClass('fadeOut');
    setInterval(function () {
        $alert.remove();
    }, 5000);
    //console.log($('.nn-alert-container  .nn-alert:last-child').length);
}

/*GET SELECTED DEVICE */
var activeDeviceArr = [];
function $getSelDevice($successFn) {
    $.ajax({
        type: 'POST',
        url: 'ajax.php',
        data: {'c': 'Device', 'a': 'getSelectedDevice'},
        dataType: 'json',
        async: false,
        success: function (res) {
            if (res.deviceID != '' && res.deviceID != null) {
                activeDeviceArr = [];
                activeDeviceArr.push({'deviceID': res.deviceID,
                    'deviceName': res.deviceName,
                    'serialNo': res.serialNo,
                    'IDAddress': res.IDAddress,
                    'location': res.location});
                ///$successFn();
            }
        },
        complete: function () {

        },
        error: function (r) {
            console.log(r);
            $("#waitingTr").addClass("hide");
            $("#completeTr").removeClass("hide");
        }
    });
}

function setDeviceStuts($data, $page) {
    if ($page == "deviceManagement") {
        //console.log($data);
        for (var i = 0; i < $data.length; i++) {
            var $dev = $data[i];
            //console.log($dev);
            if ($dev.status == 1) {
                $("#deviceBox_" + $dev.id + " #deviceActive").attr("class", "");
                $("#deviceBox_" + $dev.id + " #deviceActive").addClass("notify-bubble blink bg-green bg-green-ball");
                $("#deviceBox_" + $dev.id + " #deviceActive").attr("data-original-title", "Device is Active");
            }
            else {
                $("#deviceBox_" + $dev.id + " #deviceActive").attr("class", "");
                $("#deviceBox_" + $dev.id + " #deviceActive").addClass("notify-bubble bg-red bg-red-ball");
                $("#deviceBox_" + $dev.id + " #deviceActive").attr("data-original-title", "Device is Inactive");
            }
        }
    }
    if ($("#pop_device_list").length > 0) {
        for (var i = 0; i < $data.length; i++) {
            var $dev = $data[i];
            if ($dev.status == 1) {
                $("#device-trigger_" + $dev.id + " .device-count div").attr("class", "");
                $("#device-trigger_" + $dev.id + " .device-count div").addClass("bg-green bg-green-ball blink");
            }
            else {
                $("#device-trigger_" + $dev.id + " .device-count div").attr("class", "");
                $("#device-trigger_" + $dev.id + " .device-count div").addClass("bg-red bg-red-ball");
            }
        }
    }
}

function setTimer() {
    var $timer = setInterval(function () {
        if ($(".timer").length > 0) {
            var n = $(".timer").html();
            if (n != "00")
                $(".timer").html(("0" + (parseInt(n) - 1)).slice(-2));
            else
                clearInterval($timer);
        }
        else
            clearInterval($timer);
    }, 1000);
}

function convertTimeFrame(timeinsec) {
    //alert(timeinsec);
    String.prototype.toHHMMSS = function () {
        var sec_num = parseInt(this, 10); // don't forget the second parm
        var hours = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds = sec_num - (hours * 3600) - (minutes * 60);
        if (hours < 10) {
            hours = "0" + hours;
        }
        if (minutes < 10) {
            minutes = "0" + minutes;
        }
        if (seconds < 10) {
            seconds = "0" + seconds;
        }
        var time = hours + ':' + minutes + ':' + seconds;
        return time;
    }

    var count = (parseInt(timeinsec) - 1).toString();
    var counter = setInterval(timer, 1000);
    function timer() {

        if (parseInt(count) <= 0) {
            clearInterval(counter);
            $('#recordTimer .timer').html("00:00:00");
            return;
        }
        var temp = count.toHHMMSS();
        //alert(temp);
        count = (parseInt(count) - 1).toString();
        $('#recordTimer .timer').html(temp);
    }

}

function showAVGMinMax(AVGMinMax) {
    //console.log(AVGMinMax);
    if (typeof ("#div_KWH") !== 'undefined') {
        if ($("#div_KWH .avg-range").length == 0) {
            $("#div_KWH").append('<div class="avg-range full-width">' +
                    // data-placement="left" data-toggle="tooltip" data-trigger="hover" title="" data-original-title="Device alerts"
                    '<span class="font-montserrat pull-left hide" id="KWHMin"><i class="fa fa-caret-down"></i></span>' +
                    '<span class="font-montserrat pull-right hide" id="KWHMax"><i class="fa fa-caret-up"></i></span>' +
                    '</div>');
        }
    }
    if (typeof ("#div_KVA") !== 'undefined') {
        if ($("#div_KVA .avg-range").length == 0) {
            //console.log(111);
            $("#div_KVA").append('<div class="avg-range full-width">' +
                    '<span class="font-montserrat pull-left hide" id="KVAMin"><i class="fa fa-caret-down"></i></span>' +
                    '<span class="font-montserrat pull-right hide" id="KVAMax"><i class="fa fa-caret-up"></i></span>' +
                    '</div>');
        }
    }
    if (typeof ("#div_KVAR") !== 'undefined') {
        if ($("#div_KVAR .avg-range").length == 0) {
            //console.log(2222);
            $("#div_KVAR").append('<div class="avg-range full-width">' +
                    '<span class="font-montserrat pull-left hide" id="KVARMin"><i class="fa fa-caret-down"></i></span>' +
                    '<span class="font-montserrat pull-right hide" id="KVARMax"><i class="fa fa-caret-up"></i></span>' +
                    '</div>');
        }
    }
    //KWH
    if (AVGMinMax.MIN_KWH !== 'undefined' && AVGMinMax.MIN_KWH !== null) {
        $("#KWHMin").removeClass("hide");
        $("#KWHMin").html('<i class="fa fa-caret-down"></i> ' + AVGMinMax.MIN_KWH);
    }
    else
        $("#KWHMax").addClass("hide");
    if (AVGMinMax.MAX_KWH !== 'undefined' && AVGMinMax.MAX_KWH !== null) {

        $("#KWHMax").html('<i class="fa fa-caret-up"></i> ' + AVGMinMax.MAX_KWH);
        $("#KWHMax").removeClass("hide");
    }
    else
        $("#KWHMin").addClass("hide");
    //KVA
    if (AVGMinMax.MIN_KVA !== 'undefined' && AVGMinMax.MIN_KVA !== null) {
        $("#KVAMin").removeClass("hide");
        $("#KVAMin").html('<i class="fa fa-caret-down"></i> ' + AVGMinMax.MIN_KVA);
    }
    else
        $("#KVAMin").addClass("hide");
    if (AVGMinMax.MAX_KVA !== 'undefined' && AVGMinMax.MAX_KVA !== null) {

        $("#KVAMax").html('<i class="fa fa-caret-up"></i> ' + AVGMinMax.MAX_KVA);
        $("#KVAMax").removeClass("hide");
    }
    else
        $("#KVAMax").addClass("hide");
    //KVAR
    if (AVGMinMax.MIN_KVAR !== 'undefined' && AVGMinMax.MIN_KVAR !== null) {
        $("#KVARMin").removeClass("hide");
        $("#KVARMin").html('<i class="fa fa-caret-down"></i> ' + AVGMinMax.MIN_KVAR);
    }
    else
        $("#KVARMin").addClass("hide");
    if (AVGMinMax.MAX_KVAR !== 'undefined' && AVGMinMax.MAX_KVAR !== null) {

        $("#KVARMax").html('<i class="fa fa-caret-up"></i> ' + AVGMinMax.MAX_KVAR);
        $("#KVARMax").removeClass("hide");
    }
    else
        $("#KVARMax").addClass("hide");
}

//Convert to formatted array for output
function FormatedArray(data, range)
{
    var obj = [];
    for (i = 0; i < range; i++) {
        obj[i] = [i, data[i]];
    }

    return obj;
}
//Convert to formatted array for output
function FormatedHarmonicArray(data, range, inc, hPeak)
{
// 
    //console.log(data);
    var obj = [];
    var thdVal = 0;
    var pVal = 0;
    var $type = $('input[name="radioHarmonic"]:checked').val();
    if ($type == "voltPh1" || $type == "voltPh2" || $type == "voltPh3") {
        thdVal = _HB_VTHD_C;
        pVal = _HB_PEAKV_HARM_C;
    }
    else if ($type == "ampsPh1" || $type == "ampsPh2" || $type == "ampsPh3") {
        thdVal = _HB_ATHD_C;
        pVal = _HB_PEAKA_HARM_C;
    }

    var highHarm = 0;
    for (i = 3; i <= range + 3; i++) {
        if (i > 3) {
            var modVal = i % 2;
            if (modVal != 0) {
                var absHarm = Math.abs(data[i]);
                if (absHarm > highHarm)
                    highHarm = absHarm;
            }
        }


    }


    for (i = 3; i <= range + 3; i++) {
        if (!$("#checkShow1st").is(':checked') && i == 3)
            obj[0] = [1, 0];
        else {
            if (i == 3)
                obj[0] = [1, (100 - thdVal)];
            else {
                var modVal = i % 2;
                if (modVal != 0) {
                    var absHarm = Math.abs(data[i]);
                    var hd = (absHarm * parseFloat(pVal)) / parseFloat(highHarm);
                    //console.log("Index: " + i + ", Chart Value: " + absHarm + ", Harmonics Peak From HB: " + pVal + ", Current Peak: " + highHarm + ", Calculated Peak: " + hd);
                    obj[i - 3] = [i - 2, hd];
                }
                else
                    obj[i - 3] = [i - 2, 0];
            }
        }
    }
    return obj;
}

function displayVoltageGuages() {
    $.ajax({
        type: 'POST',
        url: 'ajax.php',
        data: {'c': 'Dashboard', 'a': 'getVoltageGuage'},
        dataType: 'json',
        beforeSend: function () {
            __showLoadingAnimation();
        },
        success: function (res) {
            //console.log(typeof res.reuslt[0]);
            if (res.status == 1 && (typeof res.reuslt[0] != "undefined")) {
                var identity = res.reuslt[0].label;
                var modelNo = res.reuslt[0].model_no;
                pageGuages[identity].remove();
                delete pageGuages[identity];
                $("#div_" + identity).html("");
                $("#div_" + identity).remove();
                ShowGuage(res.reuslt[0]);
                setGuageSettings(modelNo);
            }
        },
        complete: function () {
            __hideLoadingAnimation();
        },
        error: function (r) {
            __hideLoadingAnimation();
            console.log(r);
        }
    });
}

//Change on min or max value of gauge
function RangeChanged() {

    if ($(".inp-color-range-values").length > 0) {
        //console.log(2);
        var minVal = $("#txt-range-start").val();
        var maxVal = $("#txt-range-end").val();
        $(".inp-color-range-values").each(function () {

            DestroySlider(this.id);
            CreateSlider(this.id)
        });
    }
}
/*
 function encodeStr($str){
 return escape(encodeURIComponent($str));
 }
 function decodeStr($str){
 return unescape(decodeURIComponent($str));
 }*/
/* CONVERT HEX TO RGBA*/
function convertHex(hex, opacity) {
    hex = hex.replace('#', '');
    r = parseInt(hex.substring(0, 2), 16);
    g = parseInt(hex.substring(2, 4), 16);
    b = parseInt(hex.substring(4, 6), 16);
    result = 'rgba(' + r + ',' + g + ',' + b + ',' + opacity / 100 + ')';
    return result;
}
// Menu
(function ($) {
    $.fn.superfish = function (op) {

        var sf = $.fn.superfish,
                c = sf.c,
                $arrow = $(['<span class="', c.arrowClass, '"> &#187;</span>'].join('')),
                over = function () {
                    var $$ = $(this), menu = getMenu($$);
                    clearTimeout(menu.sfTimer);
                    $$.showSuperfishUl().siblings().hideSuperfishUl();
                },
                out = function () {
                    var $$ = $(this), menu = getMenu($$), o = sf.op;
                    clearTimeout(menu.sfTimer);
                    menu.sfTimer = setTimeout(function () {
                        o.retainPath = ($.inArray($$[0], o.$path) > -1);
                        $$.hideSuperfishUl();
                        if (o.$path.length && $$.parents(['li.', o.hoverClass].join('')).length < 1) {
                            over.call(o.$path);
                        }
                    }, o.delay);
                },
                getMenu = function ($menu) {
                    var menu = $menu.parents(['ul.', c.menuClass, ':first'].join(''))[0];
                    sf.op = sf.o[menu.serial];
                    return menu;
                },
                addArrow = function ($a) {
                    $a.addClass(c.anchorClass).append($arrow.clone());
                };
        return this.each(function () {
            var s = this.serial = sf.o.length;
            var o = $.extend({}, sf.defaults, op);
            o.$path = $('li.' + o.pathClass, this).slice(0, o.pathLevels).each(function () {
                $(this).addClass([o.hoverClass, c.bcClass].join(' '))
                        .filter('li:has(ul)').removeClass(o.pathClass);
            });
            sf.o[s] = sf.op = o;
            $('li:has(ul)', this)[($.fn.hoverIntent && !o.disableHI) ? 'hoverIntent' : 'hover'](over, out).each(function () {
                if (o.autoArrows)
                    addArrow($('>a:first-child', this));
            })
                    .not('.' + c.bcClass)
                    .hideSuperfishUl();
            var $a = $('a', this);
            $a.each(function (i) {
                var $li = $a.eq(i).parents('li');
            });
            o.onInit.call(this);
        }).each(function () {
            var menuClasses = [c.menuClass];
            if (sf.op.dropShadows && !($.browser.msie && $.browser.version < 7))
                menuClasses.push(c.shadowClass);
            $(this).addClass(menuClasses.join(' '));
        });
    };
    var sf = $.fn.superfish;
    sf.o = [];
    sf.op = {};
    sf.IE7fix = function () {
        var o = sf.op;
        if ($.browser.msie && $.browser.version > 6 && o.dropShadows && o.animation.opacity != undefined)
            this.toggleClass(sf.c.shadowClass + '-off');
    };
    sf.c = {
        bcClass: 'sf-breadcrumb',
        menuClass: 'sf-js-enabled',
        anchorClass: 'sf-with-ul',
        arrowClass: 'sf-sub-indicator',
        shadowClass: 'sf-shadow'
    };
    sf.defaults = {
        hoverClass: 'sfHover',
        pathClass: 'overideThisToUse',
        pathLevels: 2,
        delay: 500,
        animation: {height: 'show'},
        speed: 'normal',
        autoArrows: false,
        dropShadows: false,
        disableHI: false, // true disables hoverIntent detection
        onInit: function () {
        }, // callback functions
        onBeforeShow: function () {
        },
        onShow: function () {
        },
        onHide: function () {
        }
    };
    $.fn.extend({
        hideSuperfishUl: function () {
            var o = sf.op,
                    not = (o.retainPath === true) ? o.$path : '';
            o.retainPath = false;
            var $ul = $(['li.', o.hoverClass].join(''), this).add(this).not(not).removeClass(o.hoverClass)
                    .find('>ul').hide();
            o.onHide.call($ul);
            return this;
        },
        showSuperfishUl: function () {
            var o = sf.op,
                    sh = sf.c.shadowClass + '-off',
                    $ul = this.not('.accorChild').addClass(o.hoverClass)
                    .find('>ul:hidden');
            sf.IE7fix.call($ul);
            o.onBeforeShow.call($ul);
            $ul.animate(o.animation, o.speed, function () {
                sf.IE7fix.call($ul);
                o.onShow.call($ul);
            });
            return this;
        }
    });
})(jQuery);
/*---------------------*/
$(function () {
    $('.sf-menu').superfish()
})
var $treeView = ["#leftTreePanel", "#rightTreePanel"];
var $rightTreeViewStatus = 0;
var $nodeAddToStatus = 0;
var $mergePolicyStatus = 0;
var $locationEditorStatus = 0;
var $policyStatus = 0;
var $overrideStatus = 0;
var $showGaugesStatus = 0;
var $showTerminalStatus = 0;
var $ClusterTree = {
    /****************************************
     * jqtree view installation
     * @param {type} $type = 0 main cluster tree view;1= left side cluster tree view
     * @returns {object}
     ****************************************/
    init: function ($type,$status) {
        $.ajax({
            type: 'POST',
            url: 'ajax.php',
            data: {'c': 'LocationSettings', 'a': 'GetNodeTreeList', type: $type},
            dataType: 'json',
            async: false,
            success: function ($data) {

                if ($data.status == 0) {
                    //console.log($data.result);
                    var $tree = $($treeView[$type]);
                    if(typeof $status != "undefined" && $status == 1) $tree.dynatree("destroy");
                        
                    var $dataArr = $data.result;
                    if ($type == 1)
                        $rightTreeViewStatus = 1;
                    $tree.dynatree({
                        selectMode: 2,
                        clickFolderMode: 1,
                        debugLevel: 0,
                        children: $dataArr,
                        onKeydown: function (node, event) {
                            if (event.which == 32) {
                                node.toggleSelect();
                                return false;
                            }
                        },
                        // The following options are only required, if we have more than one tree on one page:
                        cookieId: "dynatree-Cb2",
                        idPrefix: "dynatree-Cb2-",
                        onClick: function (node, event) {
                            $ClusterTree.closeTreepopover();
                            //console.log(node);
                            var $key = node.data.key,
                                    $title = node.data.tname,
                                    $btnID = event.target.id,
                                    $nodeID = node.data.id,
                                    $clusterID = node.data.clusterID,
                                    $nodeType = node.data.nodeType;
                            if ($nodeAddToStatus == 1 && $nodeType == 1 && $type == 1) {
                                $("#hdnClusterID").val($nodeID);
                                $("#lblClusterName").html($title);
                            }

                            $("#hdnTreeViewActiveKey").val($key);

                            if ($(event.target).hasClass('tv-edit')) {
                                //menuItemEdit(node);
                                var $htmlTmp = '<div style="width: 250px;">';
                                $htmlTmp += '<div class="">';
                                $htmlTmp += '<div class="form-group m-b-5">';
                                $htmlTmp += '<input type="text" class="form-control" placeholder="Menu Name" required="" id="txtNodeEdit" value="' + $.trim($title) + '">';
                                $htmlTmp += '</div>';
                                $htmlTmp += '</div>';
                                $htmlTmp += '<div class="clearfix">';
                                $htmlTmp += '<button class="btn btn-success btn-xs pull-right m-l-10" onclick="return $ClusterTree.saveTreeNode();">Save</button>';
                                $htmlTmp += '<button class="btn btn-default btn-xs pull-right m-l-10" onclick="return $ClusterTree.closeTreepopover();">Cancel</button>';
                                $htmlTmp += '</div>';
                                $htmlTmp += '</div>';
                                //console.log($btnID);
                                $("#" + $btnID).popover({
                                    title: '',
                                    html: 'true',
                                    trigger: 'click',
                                    content: $htmlTmp,
                                    container: 'body',
                                    placement: "right"
                                })
                                .on("show.bs.popover", function (e) {
                                    // hide all other popovers
                                    $("[rel=popover]").not(e.target).popover("destroy");
                                    $(".popover").remove();
                                });

                                $("#" + $btnID).popover("show");
                            }
                            else if ($(event.target).hasClass('tv-delete')) {
                                var $htmlTmp = '<div style="width: 206px;">';
                                $htmlTmp += '<div class="">';
                                $htmlTmp += '<div class="form-group m-b-0">';
                                $htmlTmp += '<span class="fs-14">Are you sure you want to delete</span>';
                                $htmlTmp += '</div>';
                                $htmlTmp += '</div>';
                                $htmlTmp += '<div class="clearfix m-t-5">';
                                $htmlTmp += '<button class="btn btn-danger btn-xs pull-right m-l-10" onclick="return $ClusterTree.deleteTreeNode(' + $type + ');">Yes</button>';
                                $htmlTmp += '<button class="btn btn-default btn-xs pull-right m-l-10" onclick="return $ClusterTree.closeTreepopover();">No</button>';
                                $htmlTmp += '</div>';
                                $htmlTmp += '</div>';
                                $("#" + $btnID).popover({
                                    title: '',
                                    html: 'true',
                                    trigger: 'click',
                                    content: $htmlTmp,
                                    container: 'body',
                                    placement: $type == 0 ? "right" : "bottom"
                                }).on("show.bs.popover", function (e) {
                                    // hide all other popovers
                                    $("[rel=popover]").not(e.target).popover("destroy");
                                    $(".popover").remove();
                                });
                                $("#" + $btnID).popover("show");
                            }
                            else if ($(event.target).hasClass('tv-move')) {

                                if ($("#clusterShelf").find("#widget_" + $nodeID).length == 0) {

                                    if ($("#hdnClusterID").val() != "") {
                                        var $clusterNode = $($treeView[1]).dynatree("getTree").activateKey("C_" + $("#hdnClusterID").val());
                                        var $childnode = $clusterNode.childList;
                                        //console.log($childnode);
                                        if ($childnode != null) {

                                            for (var i = 0; i < $childnode.length; i++) {
                                                if ($childnode[0]["data"]["id"] == $nodeID) {
                                                    $createAlert({status: "info", title: "Warring", text: "Already exists in same cluster"});
                                                    return false;
                                                }
                                            }
                                            
                                        }
                                    }

                                    var $nodeTitle = $(node.data.title);
                                    var $count = $nodeTitle.find(".t-count-num").html();
                                    var $htmlStr = '<div class="nodeWidget" id="widget_' + $nodeID + '" data-toggle="tooltip" data-placement="top" title="'+node.data.tname+'">';
                                    $htmlStr += '<div class="nodeWidget-container hide">';
                                    $htmlStr += '<div class="nodeWidget-body">';
                                    $htmlStr += '<a href="javascript:void(0);" onclick="return $addToCluster.remove(' + $nodeID + ');"><i class="fa fa-times"></i></a>';
                                    $htmlStr += node.data.tname;
                                    $htmlStr += '</div>';

                                    $htmlStr += '<div class="nodeWidget-footer">';
                                    $htmlStr += 'Tag (' + $count + ')';
                                    $htmlStr += '</div>';
                                    $htmlStr += '</div>';
                                    $htmlStr += '</div>';
                                    $("#clusterShelf").append($htmlStr).promise().done(function (arg1) {
                                        //console.log(arg1);
                                        var $widgetelem = $("#widget_" + $nodeID);
                                        $(".animate-path").html($widgetelem.html()).promise().done(function (arg1) {
                                            $(".animate-path").removeClass("hide");
                                            $(".animate-path .nodeWidget-container").removeClass("hide");
                                            var offset = $widgetelem.offset();
                                            var posY = offset.top - $(window).scrollTop();
                                            var posX = offset.left - $(window).scrollLeft();

                                            var bezier_params = {
                                                start: {
                                                    x: event.pageX,
                                                    y: event.pageY,
                                                    angle: 10,
                                                    length: 0.433
                                                },
                                                end: {
                                                    x: posX,
                                                    y: posY,
                                                    angle: -10,
                                                    length: 0.25
                                                }
                                            };

                                            $(".animate-path").animate({path: new $.path.bezier(bezier_params)}, 500, function () {
                                                $(".nodeWidget-container").removeClass("hide");
                                                $(".animate-path").html("");
                                                $(".animate-path").addClass("hide");
                                            });
                                        });

                                    });
                                    
                                    $('[data-toggle="tooltip"]').tooltip();
                                }
                                else
                                    $createAlert({status: "info", title: "Warring", text: "Already exists in cluster shelf"});
                            }

                            if ($mergePolicyStatus == 1 && $nodeType == 1 && $clusterID != 0) {
                                //$().select2("val", $clusterID);
                                $("#selCluster").val($clusterID).trigger("change")
                            }
                            if ($locationEditorStatus == 1 && $nodeType == 1 && $clusterID != 0) {
                               $locationEditor.getActiveNode($clusterID);
                               $("#spClusterName").html(" - "+$title);
                               $("#divUploadBtn").removeClass("hide");
                            }
                            if($policyStatus == 1 && $nodeType == 1 && $clusterID != 0){
                                $Policy.getAll($clusterID);
                                $("#lblCluster").html($title);
                               $("#rightPanelTitle").html("Policy in cluster : "+$title);
                            }
                            if($overrideStatus == 1 && $nodeType == 1 && $clusterID != 0){
                                
                                $overridePolicy.getAll($clusterID);
                                $("#lblCluster").html($title);
                            }
                            if($showGaugesStatus == 1 && $nodeType == 1 && typeof $clusterID != "undefined" && $clusterID != 0 ){
                                $gauges.changeType(2);
                                $("#selGaugesCluster").select2("val",$clusterID);
                                $gauges.displayGuages();
                            }
                            if($showGaugesStatus == 1 && $nodeType == 2 && typeof $nodeID != "undefined" && $nodeID != 0 ){
                                $gauges.changeType(1);
                                $("#selGaugesNode").select2("val",$nodeID);
                                $gauges.displayGuages();
                            }
                            if($showTerminalStatus == 1 && $nodeType == 2 && typeof $nodeID != "undefined" && $nodeID != 0 ){
                                $terminalEle.init($nodeID);
                            }
                            //console.log("$nodeType === "+$nodeType+" $clusterID === "+$clusterID+" $nodeID ===== "+$nodeID);

                        },
                        onCreate: function (node, nodeSpan) {
                            //console.log("$nodeAddToStatus == " + $nodeAddToStatus + " node.tree.$tree.context.id == " + node.tree.$tree.context.id);
                            if ($nodeAddToStatus == 1 && node.tree.$tree.context.id == "leftTreePanel") {//"

                                if (node.data.nodeType == 2 && node.data.clusterID != 0) {

                                    var $htmlstr = $("<div>" + node.data.title + "</div>");
                                    $htmlstr.find(".tv-btn-control").append("<i class='fa fa-arrow-right p-l-10 tv-move' id='menu_move_" + new Date().getTime() + "' aria-hidden='true'></i>");
                                    node.data.title = $htmlstr.html();
                                    node.render();
                                }
                                toggleBtControl(0);
                            }
                            else
                                toggleBtControl(1);
                        },
                        dnd: {
                            onDragStart: function (node) {
                                /** This function MUST be defined to enable dragging for the tree.
                                 *  Return false to cancel dragging of node.
                                 */
                                var $key = node.data.key,
                                        $nodeType = node.data.nodeType;
                                if ($type == 1)
                                    return false;
                                else if ($key == "C_0")
                                    return false;
                                else if ($nodeType == 3)
                                    return false;

                                //logMsg("tree.onDragStart(%o)", node);
                                return true;
                            },
                            onDragStop: function (node) {
                                // This function is optional.
                                // logMsg("tree.onDragStop(%o)", node);
                            },
                            autoExpandMS: 1000,
                            preventVoidMoves: true, // Prevent dropping nodes 'before self', etc.
                            onDragEnter: function (node, sourceNode) {
                                /** sourceNode may be null for non-dynatree droppables.
                                 *  Return false to disallow dropping on node. In this case
                                 *  onDragOver and onDragLeave are not called.
                                 *  Return 'over', 'before, or 'after' to force a hitMode.
                                 *  Return ['before', 'after'] to restrict available hitModes.
                                 *  Any other return value will calc the hitMode from the cursor position.
                                 */
                                //logMsg("tree.onDragEnter(%o, %o)", node, sourceNode);
                                return true;
                            },
                            onDragOver: function (node, sourceNode, hitMode) {
                                /** Return false to disallow dropping this node.
                                 *
                                 */
                                //logMsg("tree.onDragOver(%o, %o, %o)", node, sourceNode, hitMode);

                                var $parentNodeType = node.data.nodeType.toString(),
                                        $draggNodeType = sourceNode.data.nodeType.toString();
                                //console.log("$parentNodeType ==="+$parentNodeType+"    $draggNodeType   ===="+$draggNodeType)
                                if (($draggNodeType == 1 && (($parentNodeType == 1 && hitMode != "before" && hitMode != "after") || $parentNodeType == 2 || $parentNodeType == 3)) ||
                                        ($draggNodeType == 2 && (($parentNodeType == 1 && hitMode == "after") || ($parentNodeType == 2 && hitMode != "before") || $parentNodeType == 3)))
                                    return false;
                                // Prevent dropping a parent below it's own child
                                if (node.isDescendantOf(sourceNode)) {
                                    return false;
                                }
                                // Prohibit creating childs in non-folders (only sorting allowed)
                                if (!node.data.isFolder && hitMode === "over") {
                                    return "after";
                                }
                            },
                            onDrop: function (node, sourceNode, hitMode, ui, draggable) {
                                /** This function MUST be defined to enable dropping of items on
                                 * the tree.
                                 */
                                //logMsg("tree.onDrop(%o, %o, %s)", node, sourceNode, hitMode);

                                // expand the drop target
                                //sourceNode.expand(true);
                                if (sourceNode.data.nodeType == 1)
                                    sourceNode.move(node, hitMode);
                                //save from database
                                var status = $ClusterTree.changeOrder(node, sourceNode, hitMode);
                                //if(sourceNode.data.nodeType == 2 && status) sourceNode.move(node, hitMode);
                            },
                            onDragLeave: function (node, sourceNode) {
                                /** Always called if onDragEnter was called.
                                 */
                                //logMsg("tree.onDragLeave(%o, %o)", node, sourceNode);
                            }
                        }

                    });
                    toggleBtControl($type);

                    //event trigger for save new cluster
                    if ($type == 0) {
                        $("#txtNewCluster").keyup(function (event) {
                            if (event.keyCode == 13) {
                                // addnewCluster();
                                $ClusterTree.addnewCluster(0);
                            }
                        });
                    }

                }
                else {
                    $($treeView[$type]).html('<h5 class="text-center bold hint-text"><i class="fa fa-exclamation-triangle fa-2x" aria-hidden="true"></i> <br>Cluster not available</h>');
                    //event trigger for save new cluster
                    if ($type == 0) {
                        $("#txtNewCluster").keyup(function (event) {
                            if (event.keyCode == 13) {
                                // addnewCluster();
                                $ClusterTree.addnewCluster(1);
                            }
                        });
                    }
                }
                
                
            
            },
            complete: function () {
               if($nodeAddToStatus == 1 && $type == 0){
                   //console.log(">><<<>><<<>>>");
                   if(typeof $status != "undefined" && $status == 1) $ClusterTree.init(1,1);
                   else $ClusterTree.init(1);
               }
            },
            error: function (r) {
                console.log(r);
                $($treeView[$type]).html('<h5 class="text-center bold hint-text"><i class="fa fa-exclamation-triangle fa-2x" aria-hidden="true"></i> <br>Cluster not available</h>');
            }
        });
    },
    //popover close 
    closeTreepopover: function () {
        $('.popover').popover('hide');
    },
    //add new cluster
    saveTreeNode: function () {
        var $name = $("#txtNodeEdit").val();
        var $key = $("#hdnTreeViewActiveKey").val();


        $("#leftTreePanel").dynatree("getTree").selectKey($key);
        var $node = $("#leftTreePanel").dynatree("getActiveNode");
        var $type = $node.data.nodeType, //1:Cluster,2:node,3:tag
                $id = $node.data.id,
                $title = $node.data.title,
                $tname = $node.data.tname;

        $.ajax({
            type: 'POST',
            url: 'ajax.php',
            data: {'c': 'LocationSettings', a: 'saveTreeNodeName', id: $id, nodeName: $name, type: $type},
            dataType: 'json',
            beforeSend: function () {
                __showLoadingAnimation();
            },
            success: function (res) {
                //console.log(res);
                if (res.component != '') {
                    var $htmlstr = $("<div>" + $title + "</div>");
                    $htmlstr.find(".t-title").html($name);

                    var node = $("#leftTreePanel").dynatree("getActiveNode");
                    node.data.title = $htmlstr.html();
                    node.data.tname = $name;
                    node.render();
                    toggleBtControl();

                    if ($rightTreeViewStatus == 1) {
                        $($treeView[1]).dynatree("destroy");
                        $ClusterTree.init(1);
                    }
                    $('.popover').popover('hide');
                }
                else {
                    $createAlert({status: "fail", title: "Failed Queue", text: "Failed to request queue to save cluster"});
                }

            },
            complete: function () {
                __hideLoadingAnimation();
            },
            error: function (r) {
                __hideLoadingAnimation();
                console.log(r);
            }
        });

    },
    //Delete tree view node (cluster/node/tag)
    deleteTreeNode: function ($type) {
        var $key = $("#hdnTreeViewActiveKey").val();

        var $node = $($treeView[$type]).dynatree("getTree").selectKey($key);
        var $nodeType = $node.data.nodeType, //1:Cluster,2:node,3:tag
                $id = $node.data.id,
                $clusterID = $node.data.clusterID,
                $child = $node.data.children != null ? 1 : 0;
        //console.log($node);
        $.ajax({
            type: 'POST',
            url: 'ajax.php',
            data: {'c': 'LocationSettings', a: 'deleteTreeNode', id: $id, clusterId: $clusterID, type: $nodeType, child: $child},
            dataType: 'json',
            beforeSend: function () {
                __showLoadingAnimation();
            },
            success: function (res) {
                //console.log(res);
                if (res.component != '') {
                    var $unassigned = res.unassignedNode;
                    // remove node from tree view
                    var $node = $($treeView[$type]).dynatree("getActiveNode");
                    var $revoceNodeCID = $node.data.clusterID;
                    var $revoceNodeType = $node.data.nodeType;
                    $node.remove();
                   /* if ($unassigned.length > 0) {
                        //console.log($unassigned);
                        $node = $("#leftTreePanel").dynatree("getTree").activateKey("C_0");
                        $node.addChild($unassigned);
                        $ClusterTree.changeCount($node, $unassigned.length);
                    }*/
                    if ($revoceNodeType == 2) {
                        $node = $($treeView[$type]).dynatree("getTree").activateKey("C_" + $revoceNodeCID);
                        $ClusterTree.changeCount($node, -1);
                    }
                    if ($rightTreeViewStatus == 1) {
                        $($treeView[$type == 1 ? 0 : 1]).dynatree("destroy");
                        $ClusterTree.init($type == 1 ? 0 : 1);
                    }
                }
                else {
                    $createAlert({status: "fail", title: "Failed Queue", text: "Failed to request queue to  delete node"});
                }
                $('.popover').popover('hide');
            },
            complete: function () {
                __hideLoadingAnimation();
            },
            error: function (r) {
                __hideLoadingAnimation();
                console.log(r);
            }
        });
    },
    changeOrder: function ($node, $sourceNode, $hitMode) {
        //return false;
        var $tree = $node.tree.toDict(),
                $nodeID = $node.data.id,
                $nodeType = $node.data.nodeType,
                $nodeKey = $node.data.key,
                $nodeClusterID = $node.data.clusterID,
                $sourceNodeID = $sourceNode.data.id,
                $sourceNodeType = $sourceNode.data.nodeType,
                $sourceNodeClusterID = $sourceNode.data.clusterID,
                $sourceNodeKey = $sourceNode.data.key;

        //validate already exists;
        var $exists = 0;
        if ($sourceNodeType == 2) {
            var $activeClusterNode = $("#leftTreePanel").dynatree("getTree").activateKey($nodeKey);
            var $child = $activeClusterNode.childList;
            if ($child != null) {
                for (var i = 0; i < $child.length; i++) {
                    if ($child[i]["data"]["id"] == $sourceNodeID)
                        $exists = 1;
                    break;
                }
            }
        }
        //console.log("$exists == "+ $exists);
        if ($exists == 1)
            return false;

        //ajax function
        var $data = {c: 'LocationSettings',
            a: 'treeViewChangeOrder',
            tree: $tree,
            nodeID: $nodeID,
            nodeType: $nodeType,
            nodeClusterID: $nodeClusterID,
            sourceNodeID: $sourceNodeID,
            sourceNodeType: $sourceNodeType,
            sourceNodeClusterID: $sourceNodeClusterID
        };

        $.ajax({
            type: 'POST',
            url: 'ajax.php',
            data: $data,
            dataType: 'json',
            beforeSend: function () {
                // __showLoadingAnimation();
            },
            success: function (res) {
                //console.log(res);
                if (res.status == 1) {
                    if ($sourceNodeType == 2) {//Node
                        var $htmlstr = $("<div>" + $sourceNode.data.title + "</div>");
                        if ($sourceNode.data.clusterID == 0) {
                            var $btnHtml = "<i class='fa fa-trash-o p-l-5 tv-delete' id='menu_del_" + new Date().getTime() + "' aria-hidden='true'></i>";
                            if ($nodeAddToStatus == 1)
                                $btnHtml += "<i class='fa fa-arrow-right p-l-5 tv-move' id='menu_move_" + new Date().getTime() + "' aria-hidden='true'></i>";
                            $htmlstr.find(".tv-btn-control").append($btnHtml);
                        }
                        $sourceNode.data.title = $htmlstr.html();
                        $sourceNode.data.clusterID = $nodeClusterID;
                        $sourceNode.data.nodeType = 2;
                        $sourceNode.move($node, $hitMode);

                        var $clusterNode = $("#leftTreePanel").dynatree("getTree").activateKey("C_" + $sourceNodeClusterID);
                        $ClusterTree.changeCount($clusterNode, -1);
                        var $clusterNode = $("#leftTreePanel").dynatree("getTree").activateKey("C_" + $nodeClusterID);
                        $ClusterTree.changeCount($clusterNode, 1);
                    }
                    if ($rightTreeViewStatus == 1) {
                        $($treeView[1]).dynatree("destroy");
                        $ClusterTree.init(1);
                    }
                }
                else {
                    return false;
                }

            },
            complete: function () {
                toggleBtControl();
                // __hideLoadingAnimation();
                return true;
            },
            error: function (r) {
                // __hideLoadingAnimation();
                console.log(r);
                return false;
            }
        });
        return true;
    },
    addnewCluster: function ($addStatus) {
        var $name = $("#txtNewCluster").val();
        var $type = 1;
        if ($name.trim() == "")
            return false;

        $.ajax({
            type: 'POST',
            url: 'ajax.php',
            data: {'c': 'LocationSettings', a: 'saveTreeNodeName', id: 0, nodeName: $name, type: $type},
            dataType: 'json',
            beforeSend: function () {
                __showLoadingAnimation();
            },
            success: function (res) {
                //console.log(res);
                if (res.component != '') {
                    if(typeof $addStatus != "undefined" && $addStatus == 1){
                        //$($treeView[0]).dynatree("destroy");
                        $ClusterTree.init(0);
                        if ($rightTreeViewStatus == 1) {
                          //  $($treeView[1]).dynatree("destroy");
                            $ClusterTree.init(1);
                        }
                    }
                    else{
                        /*var clusterID = res.clusterID;
                        var htmlContent = res.htmlContent;
                        var obj = [{
                                key: "C_" + clusterID,
                                title: htmlContent,
                                tname: $name,
                                id: clusterID,
                                isFolder: true,
                                nodeType: 1
                            }];
                        $("#leftTreePanel").dynatree("getRoot").addChild(obj);
                        toggleBtControl();
                        $("#txtNewCluster").val("");
                        if ($rightTreeViewStatus == 1) {
                            $($treeView[1]).dynatree("destroy");
                            $ClusterTree.init(1);
                        }*/
                    }
                    
                    $createAlert({status: "success", title: "Successfully Queued", text: "Successfully request queued to save new cluster"});
                }
                else {
                    $createAlert({status: "fail", title: "Failed Queue", text: "Failed to request queue to save cluster"});
                }
            },
            complete: function () {
                __hideLoadingAnimation();
            },
            error: function (r) {
                __hideLoadingAnimation();
                console.log(r);
            }
        });

    },
    changeCount: function ($node, $inc) {
        var $htmlstr = $("<div>" + $node.data.title + "</div>");
        var $count = $htmlstr.find(".t-count-num").html();
        $htmlstr.find(".t-count-num").html(parseInt($count) + $inc);
        $node.data.title = $htmlstr.html();
        $node.render();
        toggleBtControl();

    },
    getAllCluster: function ($elm,$selID) {
        var $data = {
            'c': 'LocationSettings',
            a: 'getAllCluster'
        };
        $.ajax({
            type: 'POST',
            url: 'ajax.php',
            data: $data,
            dataType: 'json',
            beforeSend: function () {
            },
            success: function (res) {
                var $optionHtml = "<option value='-1'>Select a cluster</option>";
                if (res.status == 1) {
                    var $resultData = res.data;
                    var $len = $resultData.length;
                    if ($len > 0) {
                        for (var i = 0; i < $len; i++) {
                            var $selStr = "";
                            if($selID != "undefined" && $selID == $resultData[i]["ID"]) $selStr = "selected"
                            $optionHtml += '<option value="' + $resultData[i]["ID"] + '" '+$selStr+'>' + $resultData[i]["Name"] + '</option>';
                        }
                    }
                }
                
                $($elm).html($optionHtml);
                $($elm).select2();
            },
            complete: function () {
            },
            error: function (r) {
                console.log(r);
            }
        });
    },
    getAllNode:function($elm,$type,$selID){
        var $data = {
            'c': 'LocationSettings',
            a: 'getAllNodes'
        };
        $.ajax({
            type: 'POST',
            url: 'ajax.php',
            data: $data,
            dataType: 'json',
            beforeSend: function () {
            },
            success: function (res) {
                var $optionHtml = "";
                if(typeof $type == "undefined") $optionHtml = "<option value='-1'>Select a node</option>";
                if (res.status == 1) {
                    var $resultData = res.data;
                    var $len = $resultData.length;
                    if ($len > 0) {
                        for (var i = 0; i < $len; i++) {
                            var selStr = "";
                            if($selID != "undefined" && $resultData[i]["ID"] == $selID) selStr="selected";
                            $optionHtml += '<option value="' + $resultData[i]["ID"] + '" '+selStr+'>' + $resultData[i]["Name"] + '</option>';
                        }
                    }
                }
                
                $($elm).html($optionHtml);
                $($elm).select2();
            },
            complete: function () {
            },
            error: function (r) {
                console.log(r);
            }
        });
    },
    getAllTag:function($elm){
        var $data = {
            'c': 'LocationSettings',
            a: 'getAllTag'
        };
        $.ajax({
            type: 'POST',
            url: 'ajax.php',
            data: $data,
            dataType: 'json',
            beforeSend: function () {
            },
            success: function (res) {
                var $optionHtml = "";
                if(typeof $type == "undefined") $optionHtml = "<option value='-1'>Select a tag</option>";
                if (res.status == 1) {
                    var $resultData = res.result;
                    var $len = $resultData.length;
                    if ($len > 0) {
                        for (var i = 0; i < $len; i++) {
                            $optionHtml += '<option value="' + $resultData[i]["ID"] + '" data-type="'+$resultData[i]["TagTypeID"]+'">' + $resultData[i]["Name"] + '</option>';
    }
                    }
                }

                $($elm).html($optionHtml);
                $($elm).select2();
            },
            complete: function () {
            },
            error: function (r) {
                console.log(r);
            }
        });
    },    
    getAllTagGroup:function($elm){
        var $data = {
            'c': 'LocationSettings',
            a: 'getAllTag'
};
        $.ajax({
            type: 'POST',
            url: 'ajax.php',
            data: $data,
            dataType: 'json',
            beforeSend: function () {
            },
            success: function (res) {
                var $optionHtml = "";
                if(typeof $type == "undefined") $optionHtml = "<option value='-1'>Select a tag</option>";
                if (res.status == 1) {
                    var $resultData = res.result;
                    var $len = $resultData.length;
                    if ($len > 0) {
                        for (var i = 0; i < $len; i++) {
                            var $tagType = $resultData[i]["TagTypeID"];
                            var $cls = "";
                            if($tagType == 1) $cls = "bold";
                            else $cls = "m-l-15";
                                $optionHtml += '<option value="' + $resultData[i]["ID"] + '" data-type="'+$resultData[i]["TagTypeID"]+'" class="'+$cls+'">' + $resultData[i]["Name"] + '</option>';
                        }
                    }
                }

                $($elm).html($optionHtml);
                $($elm).select2();
            },
            complete: function () {
            },
            error: function (r) {
                console.log(r);
            }
        });
    }

};

//buttons hide and show will be focus node
function toggleBtControl ($type) {
    $(".dynatree-node").hover(function () {
        //console.log($(this).find(".tv-btn-control").length)
        $(".tv-btn-control").addClass('hide');
        $(this).find(".tv-btn-control").removeClass('hide');
    });
}

var $addToCluster = {
    save: function () {
        var $clusterID = $("#hdnClusterID").val();
        var $nodeIDs = "";

        if ($clusterID == "") {
            $createAlert({status: "fail", title: "Failed", text: "Please select a cluster."});
            return false;
        }

        $("#clusterShelf .nodeWidget").each(function () {
            var $ID = $(this).attr("id");
            var $splitID = $ID.split("_");
            $nodeIDs += ($nodeIDs != "" ? "," : "") + $splitID[1];
        });
        if ($nodeIDs == "") {
            $createAlert({status: "fail", title: "Failed", text: "Please select Nodes."});
            return false;
        }
        var $status = 0;
        $.ajax({
            type: 'POST',
            url: 'ajax.php',
            data: {'c': 'LocationSettings', a: 'insertNodeToCluster', clusterID: $clusterID, nodeIDs: decodeURI($nodeIDs)},
            dataType: 'json',
            beforeSend: function () {
                __showLoadingAnimation();
            },
            success: function (res) {
                //console.log(res);
                $component = res.component;
                if ($component == 1) {

                    __hideLoadingAnimation();
                    $createAlert({status: "success", title: "Successfully Queued", text: "Successfully request queued to copy new cluster"});

                    $("#clusterShelf .nodeWidget").each(function () {
                        var $ID = $(this).attr("id");
                        $(".animate-path").html($(this).html()).promise().done(function (arg1) {

                            $(".animate-path").removeClass("hide");

                            var offset = $("#" + $ID).offset();
                            var posY = offset.top - $(window).scrollTop();
                            var posX = offset.left - $(window).scrollLeft();
                            var pOffset = $("#rightTreePanel .dynatree-active").offset();
                            var pPosY = pOffset.top - $(window).scrollTop();
                            var pPosX = pOffset.left - $(window).scrollLeft();

                            $("#" + $ID).remove();

                            var bezier_params = {
                                start: {
                                    x: posX,
                                    y: posY,
                                    angle: 10,
                                    length: 0.433
                                },
                                end: {
                                    x: pPosX,
                                    y: pPosY,
                                    angle: -10,
                                    length: 0.25
                                }
                            };
                            $(".animate-path").animate({path: new $.path.bezier(bezier_params)}, 500, function () {
                                $(".animate-path").html("");
                                $(".animate-path").addClass("hide");
                            });
                        });
                    });

                    $("#hdnClusterID").val("");
                    $("#lblClusterName").html('<span class="semi-bold text-warning">Please select a cluster on cluster panel</span>');

                    var $leftNodes = res.leftSideNodes;
                    var $rightNodes = res.rightSideNodes;
                    if ($leftNodes != null) {
                        var $count = $leftNodes.length;
                        var $clusterNode = $($treeView[0]).dynatree("getTree").activateKey("C_" + $clusterID);
                        $clusterNode.addChild($leftNodes);
                        $ClusterTree.changeCount($clusterNode, $count);
                        /*for (var i = 0; i < $count; i++) {
                         var $htmlstr = $("<div>" + $leftNodes[i]["title"] + "</div>");
                         $htmlstr.find(".tv-btn-control").append("<i class='fa fa-arrow-right p-l-5 tv-move' id='menu_move_"+ new Date().getTime() + "' aria-hidden='true'></i>");
                         $leftNodes[i]["title"] = $htmlstr.html();
                         }*/

                        var $count = $rightNodes.length,
                                $clusterNode = $($treeView[1]).dynatree("getTree").activateKey("C_" + $clusterID);
                        $clusterNode.addChild($rightNodes);
                        $ClusterTree.changeCount($clusterNode, $count);
                    }



                }
                else {
                    __hideLoadingAnimation();
                    $createAlert({status: "fail", title: "Failed Queue", text: "Failed to request queue to copy nodes."});
                }
            },
            complete: function () {

                if ($status == 1) {

                }
            },
            error: function (r) {
                __hideLoadingAnimation();
                $createAlert({status: "fail", title: "Failed", text: r});
            }
        });

    },
    remove: function ($id) {
        $("#widget_" + $id).remove();
    }
};

function removeError(id) {
    $("#" + id).removeClass("has-error");
    $("#" + id + " span.sp_error").html("");
    $("#" + id + " span.sp_error").addClass("hide");
}

var $Policy = {
    int: function () {
        $("#dim_slider").ionRangeSlider({
            type: "single",
            min: 0,
            max: 100,
            from: 10,
            postfix: "%"/*,
             keyboard: true, onChange: function (data) {
             console.log("ssss====" + data.from);
             }*/
        });
        $('#txtTime,#txtSatartTime,#txtEndTime').datetimepicker({
            format: 'LT'
        });

        $("#txtStartDate").datepicker(
                {
                    format: "mm/dd/yyyy",
                    //endDate: "1d",
                    autoclose: true
                }
        ).on('changeDate', function (ev) {
            $('#txtEndDate').datepicker("remove");
            var startDate = new Date($("#txtStartDate").val());
            $("#txtEndDate").datepicker({
                format: "mm/dd/yyyy",
                //endDate: "1d",
                autoclose: true,
                startDate: startDate
            });
        });
        $("#txtEndDate").datepicker(
                {
                    format: "mm/dd/yyyy",
                    //endDate: "1d",
                    autoclose: true//,
                    //startDate :startDate
                }
        );
        $("#txtLightDefault").keypress(function (e) {
            //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                //display error message
                //$("#errmsg").html("Digits Only").show().fadeOut("slow");
                return false;
           }
           
         
        });
        
        //$("#txtDuration").mask("99-99-99-999");
    },
    getAll: function ($clusterID) {
        
        var $data = {
            c : 'LocationSettings',
            a : 'GetAllPolicy'
        };
        if (typeof $clusterID != "undefined"){
            $data["clusterID"] = $clusterID;
            $("#policyFilterContainer").removeClass("hide");
            
        }
        else {
            $("#policyFilterContainer").addClass("hide");
            $("#rightPanelTitle").html("Policy");
            //$Policy.clear();
        }
            

        $.ajax({
            type: 'POST',
            url: 'ajax.php',
            data: $data,
            dataType: 'json',
            beforeSend: function () {
                __showLoadingAnimation();
            },
            success: function (res) {
                //console.log(res);
                if (res.status == 1) {

                    var $resultData = res.data;
                    var $len = $resultData.length;
                    //console.log($len);
                    if ($len > 0) {
                        var $htmlStr = "";
                        for (var i = 0; i < $len; i++) {
                            var $eventHtml = typeof $clusterID == "undefined" ? 'onclick="return $Policy.policyEdit(' + $resultData[i]["ID"] + ')"' : "";
                            $htmlStr += '<a href="javascript:void(0);" id="policy_widget_' + $resultData[i]["ID"] + '" '+$eventHtml+' >';
                            $htmlStr += '    <div class="policy-list">';
                            $htmlStr += '        <span>' + $resultData[i]["Name"] + '</span>';
                            $htmlStr += '    </div>';
                            $htmlStr += '</a>';
                        }
                        $("#rightTreePanel").html($htmlStr);
                    }

                }
                else{
                    $("#rightTreePanel").html("<h5 class='text-center fs-14' >Policy not available.<h5>");
                }
            },
            complete: function () {
                __hideLoadingAnimation();
            },
            error: function (r) {
                __hideLoadingAnimation();
                console.log(r);
            }
        });
    },
    changeFormate: function ($value) {
        if ($value == 1) {
            $("#lblTime").html("Time");
            $("#singleTimeContainer").addClass("hide");
            $("#dateTimeContainer").removeClass("hide");
            $("#repeatContainer").removeClass("hide");
            $("#startDateContainer").addClass("hide");
            $("#endDateContainer").addClass("hide");
            $("#divTimeFormate").addClass("hide");
            $("#startDateContainer").parent().removeClass("row");
            $(".div-strat-time").addClass("p-l-0");
            $("#divMotion").removeClass("hide");
            $("#divMotion .check-success").removeClass("hide");
            $("#divduration").addClass("hide");
            $("#divduration label").html("DURATION :");
            $("#divduration label").addClass("col-sm-2");
            $("#divduration label").removeClass("col-sm-3");
            $("#chMotionLock").prop("checked", "checked");
            
        }
        else if ($value == 2) {
            var $timeFormate = $("#seleTimeFormate").val();
            $("#dateTimeContainer").removeClass("hide");
            $("#singleTimeContainer").addClass("hide");
            $("#divTimeFormate").removeClass("hide");
            $("#startDateContainer").parent().addClass("row");
            $(".div-strat-time").removeClass("p-l-0");
            $("#endDateContainer").removeClass("hide");
            $("#startDateContainer").removeClass("hide");
            if ($timeFormate == 0)
                $("#repeatContainer").addClass("hide");
            else
                $("#repeatContainer").removeClass("hide");
            $("#divMotion").removeClass("hide");
            $("#divMotion .check-success").removeClass("hide");
            $("#divduration").addClass("hide");
            $("#divduration label").html("DURATION :");
            $("#divduration label").addClass("col-sm-2");
            $("#divduration label").removeClass("col-sm-3");
            $("#chMotionLock").prop("checked", "checked");
            
        }
        else if ($value == 3) {
            $("#lblTime").html("For next time");
            $("#singleTimeContainer").addClass("hide");
            $("#divTimeFormate").removeClass("hide");
            $("#repeatContainer").addClass("hide");
            $("#dateTimeContainer").addClass("hide");
            $("#startDateContainer").parent().addClass("row");
            $(".div-strat-time").removeClass("p-l-0");
            $("#divMotion").removeClass("hide");
            $("#divMotion .check-success").addClass("hide");
            $("#divduration").removeClass("hide");
             $("#divduration label").removeClass("col-sm-2");
             $("#divduration label").addClass("col-sm-3");
             $("#divduration label").html("For next time : ");
             $("#chMotionLock").prop("checked", "checked");
             
            
        }
        
        $(".checkDay").prop("checked", "");
        $("#checkSelectAll").prop("checked", "");
    },
    changeTimeFormate: function ($value) {
        if ($value == 1) {
            $("#startDateContainer").addClass("hide");
            $("#endDateContainer").addClass("hide");
            $("#startDateContainer").parent().removeClass("row");
            $(".div-strat-time").addClass("p-l-0");
        }
        else {
            
            $("#repeatContainer").addClass("hide");
            $("#startDateContainer").removeClass("hide");
            $("#endDateContainer").removeClass("hide");
            $("#startDateContainer").parent().addClass("row");
            $(".div-strat-time").removeClass("p-l-0");
        }
        if ($value == 0 || $value == 2 || $value == 3){
            $("#repeatContainer").addClass("hide");
        }
        else {
            $("#repeatContainer").removeClass("hide");}
        
        $(".checkDay").prop("checked", "");
        $("#checkSelectAll").prop("checked", "");
    },
    changeRepeatStatus: function ($obj) {
        var $value = $($obj).val();
        var $checked = $($obj).is(':checked')
        if ($value == "ALL") {
            if ($checked)
                $(".checkDay").prop("checked", "checked");
            else
                $(".checkDay").prop("checked", "");
        }
        else {
            var $count = 0;
            $('.checkDay').each(function () {
                if (this.checked)
                    $count++
            });
            if ($count == 7)
                $("#checkSelectAll").prop("checked", "checked");
            else
                $("#checkSelectAll").prop("checked", "");
        }
    },
    changeMotionStatus: function ($obj) {
        var $checked = $($obj).is(':checked')
        $("#txtDurationH").val("");
        $("#txtDurationM").val("");
        if ($checked){
            $("label[for='chMotionLock']").html("Motion Locked");
            $("#divduration").addClass("hide");
        }
        else{
            $("label[for='chMotionLock']").html("Motion Unlocked");
            $("#divduration").removeClass("hide");
        }
       
    },
    save: function () {
        var $id = $("#hdnPolicyID").val();
        var $name = $("#txtName").val();
        var $defaultVal = $("#txtLightDefault").val();
        var $formate = $("#selFormate").val();
        var $action = $("#selAction").val();
        var $fadeIn = $("#selFadeIn").val();
        var $dim = $("#dim_slider").val();
        var $time = $("#txtTime").val();
        var $timeFormate = $("#seleTimeFormate").val();
        var $startDate = $("#txtStartDate").val();
        var $startTime = $("#txtSatartTime").val();
        var $endDate = $("#txtEndDate").val();
        var $endTime = $("#txtEndTime").val();
        var $endDate = $("#txtEndDate").val();
        var $motionStatus = $("#chMotionLock").is(':checked')
        var $durationHour = $("#txtDurationH").val();
        var $durationMinute = $("#txtDurationM").val();
                
        if ($name == "") {
            $("#divName").addClass("has-error");
            $("#divName .sp_error").html("Please enter policy name");
            $("#divName .sp_error").removeClass("hide");
            $("#txtName").focus();
            return false;
        }
        if($defaultVal == "" || parseInt($defaultVal) > 100){
            $("#divLightDefault").addClass("has-error");
            $("#divLightDefault .sp_error").html("Default value should be between 0 and 100");
            $("#divLightDefault .sp_error").removeClass("hide");
            $("#txtLightDefault").focus();
            return false;
        }
        
        if((! $motionStatus || $formate == 3) && $durationHour == "" &&  $durationMinute == ""){
            $("#divduration").addClass("has-error");
            $("#divduration .sp_error").html("Please enter duration hours or minute.");
            $("#divduration .sp_error").removeClass("hide");
            $("#divduration").focus();
            return false;
        }
        var $duration =(+( $durationHour == ""? 0 : $durationHour ) ) * 60 * 60 + (+( $durationMinute == ""? 0 : $durationMinute )) * 60 ;
        if(($formate == 3 || ! $motionStatus) && $duration > 14400 ){
            $("#divduration").addClass("has-error");
            $("#divduration .sp_error").html("Duration between 1 minute and 4 hour.");
            $("#divduration .sp_error").removeClass("hide");
            $("#divduration").focus();
            return false;
        }
        if ($formate == 2 && $timeFormate != 1 && $startDate == "") {
            $("#divStartDate").addClass("has-error");
            $("#divStartDate .sp_error").html("Please select start date");
            $("#divStartDate .sp_error").removeClass("hide");
            $("#txtStartDate").focus();
            return false;
        }

        if (($formate == 1 || $formate == 2) && $startTime == "") {
            $("#divStartTime").addClass("has-error");
            $createAlert({status: "info", title: "Warring", text: "Please Select start time"});
            return false;
        }

        if ($formate == 2 && $timeFormate != 1 && $endDate == "") {
            $("#divEndDate").addClass("has-error");
            $createAlert({status: "info", title: "Warring", text: "Please select end date"});
            return false;
        }

        if (($formate == 1 || $formate == 2) && $endTime == "") {
            $("#divEndTime").addClass("has-error");
            $createAlert({status: "info", title: "Warring", text: "Please Select end time"});
            return false;
        }

        var $sCal = 0;
        var $eCal = 0;
        if ($formate != 3){
            var $Sh = parseInt($startTime.split(":")[0]);
            var $Sm = parseInt(($startTime.split(":")[1]).split(" ")[0]);
            var $St = $startTime.split(" ")[1];
            var $Eh = parseInt($endTime.split(":")[0]);
            var $Em = parseInt(($endTime.split(":")[1]).split(" ")[0]);
            var $Et = $endTime.split(" ")[1];
            
            if($St == "PM" && $Sh != 12 ) $sCal = ($Sh + 12)*60;
            else if($St == "AM" && $Sh == 12) $sCal = 0;
            else  $sCal = $Sh * 60;
            $sCal += $Sm;
            
            //console.log("$Et ==== "+$Et);
            if($Et == "PM" && $Eh != 12 ) $eCal = ($Eh + 12)*60;
            else if($Et == "AM" && $Eh == 12) $eCal = 0;
            else  $eCal = $Eh * 60;
            $eCal += $Em;
            
            //console.log("$eCal === "+$eCal+"   $sCal ==== "+$sCal);
            if(($formate == 1) && ($eCal < $sCal)){
                $("#divEndTime").addClass("has-error");
                $createAlert({status: "info", title: "Warring", text: "Please select proper time."});
                return false;
            }
            
            if(($formate == 2 || $timeFormate != 1) && ($eCal < $sCal) && (new Date($startDate).getTime() == new Date($endDate).getTime())){ 
                $("#divEndTime").addClass("has-error");
                $createAlert({status: "info", title: "Warring", text: "Please select proper time."});
                return false;
            }
        }

        /*if ($formate == 3 && $time == "") {
            $("#DivTime").addClass("has-error");
            $createAlert({status: "info", title: "Warring", text: "Please Select time"});
            return false;
        }*/

        var $data = {
            c: 'LocationSettings',
            a: 'insertOrUpdatePolicy',
            name: $name,
            lightDefault : $defaultVal,
            formate: $formate,
            action: $action,
            fadein: $fadeIn,
            dim: $dim
        };
        if($formate == 3){
            $data["startTime"] = $startTime;
            $data["endTime"] = $endTime;
        }
        else if ($formate == 3){
            $data["time"] = $time;
        }
        else {
            $data["timeFormate"] = $timeFormate;
            if ($timeFormate != 1) {
                $data["startDate"] = $startDate;
                $data["endDate"] = $endDate;
            }

            $data["startTime"] = $startTime;
            $data["endTime"] = $endTime;
        }
        if($formate == 3 || ! $motionStatus){
            $data["motionStatus"] = (!$motionStatus ? 1 : 0);
            $data["durationHour"] =  $durationHour == ""? 0 : $durationHour ;
            $data["durationMinute"] =  $durationMinute == ""? 0 : $durationMinute;
        }
        else {
            $data["motionStatus"] = 0;
            $data["durationHour"] =  0;
            $data["durationMinute"] =  0;
        }
        
        if ($id != "")
            $data["id"] = $id;

        if ($formate != 3 || ($formate == 2 && $timeFormate != 0)) {
            var $checkedStatus = false;
            $('.checkDay').each(function () {
                //console.log("start");
                if (this.checked){
                    $data["day" + $(this).val()] = 1;
                    $checkedStatus = true;
                }
                else
                    $data["day" + $(this).val()] = 0;
            });
            if(($formate == 1 || ($formate == 2 && $timeFormate == 1)) && !$checkedStatus){
                $createAlert({status: "info", title: "Warring", text: "Please select a day of week."});
                return false;
            }
        }
        $.ajax({
            type: 'POST',
            url: 'ajax.php',
            data: $data,
            dataType: 'json',
            beforeSend: function () {
                __showLoadingAnimation();
            },
            success: function (res) {
                //var $status = res.status;
				var $component = res.component;
                if ($component != '') {
                    $createAlert({status: "success", title: "Success", text: 'Successfully Queued'});
                    $Policy.clear();
                   //if($("#modalPolicy").length == 0 )$Policy.getAll();
                }
                else {
                    $createAlert({status: "fail", title: "Failed", text: 'Failed queue request'});
                }
            },
            complete: function () {
                __hideLoadingAnimation();
            },
            error: function (r) {
                __hideLoadingAnimation();
                $createAlert({status: "fail", title: "Failed", text: r});
            }
        });
    },
    clear: function () {
        $("#hdnPolicyID").val("");
        $("#txtName").val("");
        $("#selFormate").select2("val", 1);
        $("#selAction").select2("val", 1);
        $("#selFadeIn").select2("val", 1);
        $("#dim_slider").data("ionRangeSlider").update({from: 10});
        $("#txtTime").val("");
        $("#txtLightDefault").val("");
        $("#seleTimeFormate").select2("val", 0);
        $("#txtStartDate").val("");
        $("#txtSatartTime").val("");
        $("#txtEndDate").val("");
        $("#txtEndTime").val("");

        $("#lblTime").val("Time");

        $(".checkDay").prop("checked", "");
        $("#checkSelectAll").prop("checked", "");
        
        $("#divduration").addClass("hide");
        $("#chMotionLock").prop("checked", "checked");
        $("#txtDurationH").val("");
        $("#txtDurationM").val("");

        
        $("#btnPolicyDel").removeAttr("disabled");
        $("#removeErrorMSG").addClass("hide");

        //$("#singleTimeContainer").removeClass("hide");
        //$("#dateTimeContainer").addClass("hide");
        $Policy.changeFormate(1);
    },
    policyEdit: function ($id) {
        var $data = {
            c: 'LocationSettings',
            a: 'GetPolicyDetails',
            id: $id
        };
        $.ajax({
            type: 'POST',
            url: 'ajax.php',
            data: $data,
            dataType: 'json',
            beforeSend: function () {
                __showLoadingAnimation();
            },
            success: function (res) {
                //console.log(res);
                if (res.status == 1) {
                    //console.log(res.data);
                    $Policy.clear();
                    var $resultData = res.data[0];
                    var $id = $resultData.ID;
                    var $name = $resultData.Name;
                    var $lightDefault = $resultData.LightDefault;
                    var $formate = $resultData.Formate;
                    var $action = $resultData.Action;
                    var $dim = $resultData.Dim;
                    var $fadeIn = $resultData.FadeIn;
                    var $timeFormate = $resultData.TimeFormate;
                    var $startDate = $resultData.StartDate;
                    var $endDate = $resultData.EndDate;
                    var $startTime = $resultData.StartTime;
                    var $endTime = $resultData.EndTime;
                    var $motionStatus = $resultData.MotionStatus;
                    var $durationHour = $resultData.DurationHour;
                    var $durationMinute = $resultData.DurationMinute;
                    var $day1 = $resultData.Day1;
                    var $day2 = $resultData.Day2;
                    var $day3 = $resultData.Day3;
                    var $day4 = $resultData.Day4;
                    var $day5 = $resultData.Day5;
                    var $day6 = $resultData.Day6;
                    var $day7 = $resultData.Day7;
                    var $existNodeCount = res.existsNode;

                    $("#hdnPolicyID").val($id);
                    $("#hdnPEditStat").val($existNodeCount);
                    $("#txtName").val($name);
                    $("#txtLightDefault").val($lightDefault);
                    $("#selFormate").select2("val", $formate);
                    $("#selAction").select2("val", $action);
                    $("#selFadeIn").select2("val", $fadeIn);
                    $("#dim_slider").val($dim);
                    
                    $Policy.changeFormate($formate);
                    
                    if($motionStatus == 0 && $formate != 3){
                        $("#chMotionLock").prop("checked", "checked");
                        $("#txtDurationH").val("");
                        $("#txtDurationM").val("");
                    }
                    else{
                        $("#divduration").removeClass("hide");
                        $("#chMotionLock").prop("checked", "");
                        $("#txtDurationH").val($durationHour);
                        $("#txtDurationM").val($durationMinute);
                    }
                    if ($formate == 1) {
                        var $spliDate = $startTime.split(":");
                        var $date = new Date();
                        $date.setHours($spliDate[0]);
                        $date.setMinutes($spliDate[1]);
                        $('#txtSatartTime').data("DateTimePicker").date($date);
                        if($endTime != null && $endTime != ""){
                            var $spliDate = $endTime.split(":");
                            var $date = new Date();
                            $date.setHours($spliDate[0]);
                            $date.setMinutes($spliDate[1]);
                            $('#txtEndTime').data("DateTimePicker").date($date);
                        }
                        
                    }
                    $("#dim_slider").data("ionRangeSlider").update({from: $dim});
                    /*if ($formate == 3) {
                        var $spliDate = $startTime.split(":");
                        var $date = new Date();
                        $date.setHours($spliDate[0]);
                        $date.setMinutes($spliDate[1]);
                        $('#txtTime').data("DateTimePicker").date($date);
                    }
                    else*/
                    if ($formate == 2) {
                        $("#seleTimeFormate").select2("val", $timeFormate);

                        //Select value and trigger change event
                        $("#seleTimeFormate").val($timeFormate).trigger("change");

                        //$Policy.changeRepeatStatus($("#seleTimeFormate"));
                        if ($timeFormate != 1) {
                            $('#txtStartDate').datepicker('setDate', new Date($startDate));
                            $('#txtStartDate').datepicker('update');

                            $('#txtEndDate').datepicker('setDate', new Date($endDate));
                            $('#txtEndDate').datepicker('update');
                        }
                        var $spliDate = $startTime.split(":");
                        var $date = new Date();
                        $date.setHours($spliDate[0]);
                        $date.setMinutes($spliDate[1]);
                        $('#txtSatartTime').data("DateTimePicker").date($date);

                        var $spliDate = $endTime.split(":");
                        var $date = new Date();
                        $date.setHours($spliDate[0]);
                        $date.setMinutes($spliDate[1]);
                        $('#txtEndTime').data("DateTimePicker").date($date);
                    }
                    if ($formate != 3 || ($formate == 2 && $timeFormate != 0)) {
                        if ($day1 == 1 && $day2 == 1 && $day3 == 1 && $day4 == 1 && $day5 == 1 && $day6 == 1 && $day7 == 1)
                            $("#checkSelectAll").prop("checked", "checked");
                        if ($day1 == 1)
                            $("#checkMon").prop("checked", "checked");
                        if ($day2 == 1)
                            $("#checkTue").prop("checked", "checked");
                        if ($day3 == 1)
                            $("#checkWed").prop("checked", "checked");
                        if ($day4 == 1)
                            $("#checkThu").prop("checked", "checked");
                        if ($day5 == 1)
                            $("#checkFri").prop("checked", "checked");
                        if ($day6 == 1)
                            $("#checkSat").prop("checked", "checked");
                        if ($day7 == 1)
                            $("#checkSun").prop("checked", "checked");
                    }
                    
                    if($existNodeCount > 0){
                        $("#btnPolicyDel").prop("disabled",true);
                        $("#removeErrorMSG").removeClass("hide");
                }
                    else {
                        $("#btnPolicyDel").removeAttr("disabled");
                        $("#removeErrorMSG").addClass("hide");
                    }
                }
            },
            complete: function () {
                __hideLoadingAnimation();
            },
            error: function (r) {
                __hideLoadingAnimation();
                console.log(r);
            }
        });
    },
    removePolicy: function () {
        var $id = $("#hdnPolicyID").val();
        var $delFlag = $("#hdnPEditStat").val();
        if($delFlag > 0){
            $createAlert({status: "info", title: "Warring", text: "Sorry, You can�t removed this since policy merged."});
            return false;
        }
        if ($id != "") {
            var $data = {
                c: 'LocationSettings',
                a: 'removePolicy',
                id: $id
            };

            $.ajax({
                type: 'POST',
                url: 'ajax.php',
                data: $data,
                dataType: 'json',
                beforeSend: function () {
                    __showLoadingAnimation();
                },
                success: function (res) {
                    var $status = res.status;
                    if ($status == 1) {

                        $createAlert({status: "success", title: "Successfully", text: res.msg});
                        $Policy.clear();
                        $("#policy_widget_" + $id).remove();
                    }
                    else {
                        $createAlert({status: "fail", title: "Failed", text: res.msg});
                    }
                },
                complete: function () {
                    __hideLoadingAnimation();
                },
                error: function (r) {
                    __hideLoadingAnimation();
                    $createAlert({status: "fail", title: "Failed", text: r});
                }
            });
        }
        else {
            $createAlert({status: "info", title: "Warring", text: "Select policy on policy panel"});
        }

    }
};

var $mergePolicy = {
    getAll: function () {
        var $data = {
            'c': 'LocationSettings',
            a: 'GetAllPolicy',
            policyType : "1,2"
        };
        $.ajax({
            type: 'POST',
            url: 'ajax.php',
            data: $data,
            dataType: 'json',
            beforeSend: function () {
                __showLoadingAnimation();
            },
            success: function (res) {
                //console.log(res);
                if (res.status == 1) {

                    var $resultData = res.data;
                    var $len = $resultData.length;
                    //console.log($len);
                    if ($len > 0) {
                        var $htmlStr = "";
                        var $optionHtml = "";
                        for (var i = 0; i < $len; i++) {
                            $htmlStr += '<a href="javascript:void(0);" id="policy_widget_' + $resultData[i]["ID"] + '" data-id="' + $resultData[i]["ID"] + '" onclick="return $mergePolicy.select(this)" >';
                            $htmlStr += '    <div class="policy-list">';
                            $htmlStr += '        <span>' + $resultData[i]["Name"] + '</span>';
                            $htmlStr += '    </div>';
                            $htmlStr += '</a>';
                            $optionHtml += '<option value="' + $resultData[i]["ID"] + '">' + $resultData[i]["Name"] + '</option>';
                        }

                        $("#rightTreePanel").html($htmlStr);
                        $("#selPolicy").html($optionHtml);
                        $("#selPolicy").select2();
                    }

                }
            },
            complete: function () {
                __hideLoadingAnimation();
            },
            error: function (r) {
                __hideLoadingAnimation();
                console.log(r);
            }
        });
    },
    select: function ($obj) {
        var $id = $($obj).attr("data-id");
        var $selected = $("#selPolicy").val();

        if ($selected == null)
            $selected = $id;
        else if ($.inArray($id, $selected) == -1)
            $selected.push($id);

        $("#selPolicy").select2("val", $selected);
    },
    merge: function () {
        var $clusterID = $("#selCluster").val();
        var $policy = $("#selPolicy").val();
        //console.log($policy);
        if ($clusterID == 0 || $clusterID == -1 || $clusterID == "") {
            $("#divCluster").addClass("has-error");
            $("#divCluster .sp_error").html("Please select a cluster");
            $("#divCluster .sp_error").removeClass("hide");
            $("#divCluster").focus();
            return false;
        }
        if ($policy == null || $clusterID == "") {
            $("#divPolicy").addClass("has-error");
            $("#divPolicy .sp_error").html("Please select a policy");
            $("#divPolicy .sp_error").removeClass("hide");
            $("#divPolicy").focus();
            return false;
        }

        var $data = {
            c: 'LocationSettings',
            a: 'mergepolicy',
            clusterID: $clusterID,
            policy: $policy
        };
        $.ajax({
            type: 'POST',
            url: 'ajax.php',
            data: $data,
            dataType: 'json',
            beforeSend: function () {
                __showLoadingAnimation();
            },
            success: function (res) {
                //console.log(res);
                if (res.status == 1) {
                    $createAlert({status: "success", title: "Successfully", text: res.msg});
                    var $resultData = res.data;
                    var $len = $resultData.length;
                    //console.log($len);
                    if ($len > 0) {
                        for (var i = 0; i < $len; i++) {
                            var $item = $resultData[i];
                            var $htmlStr = '<div class="nodeWidget" id="widget_' + $item.ID + '" data-toggle="tooltip" data-placement="top" title="'+$item.Name+'">';
                            $htmlStr += '<div class="nodeWidget-container hide">';
                            $htmlStr += '<div class="nodeWidget-body">';
                            $htmlStr += '<a href="javascript:void(0);" onclick="return $mergePolicy.removeConfirm(' + $item.ID + ');"><i class="fa fa-times"></i></a>';
                            $htmlStr += $item.Name;
                            $htmlStr += '</div>';

                            $htmlStr += '<div class="nodeWidget-footer">';
                            if ($item.Formate == 1)
                                $htmlStr += 'Weekly';
                            else if ($item.Formate == 2)
                                $htmlStr += 'Single';
                            else if ($item.Formate == 3)
                                $htmlStr += 'Overrider';
                            $htmlStr += '</div>';
                            $htmlStr += '</div>';
                            $htmlStr += '</div>';
                            $("#clusterShelf").append($htmlStr).promise().done(function (arg1) {
                                //console.log(arg1);
                                var $widgetelem = $("#widget_" + $item.ID);
                                $(".animate-path").html($widgetelem.html()).promise().done(function (arg1) {
                                    $(".animate-path").removeClass("hide");
                                    $(".animate-path .nodeWidget-container").removeClass("hide");
                                    var offset = $widgetelem.offset();
                                    var posY = offset.top - $(window).scrollTop();
                                    var posX = offset.left - $(window).scrollLeft();
                                    var pOffset = $("#selPolicy").offset();
                                    var pPosY = pOffset.top - $(window).scrollTop();
                                    var pPosX = pOffset.left - $(window).scrollLeft();

                                    var bezier_params = {
                                        start: {
                                            x: pPosX,
                                            y: pPosY,
                                            angle: 10,
                                            length: 0.433
                                        },
                                        end: {
                                            x: posX,
                                            y: posY,
                                            angle: -10,
                                            length: 0.25
                                        }
                                    };

                                    $(".animate-path").animate({path: new $.path.bezier(bezier_params)}, 500, function () {
                                        $(".nodeWidget-container").removeClass("hide");
                                        $(".animate-path").html("");
                                        $(".animate-path").addClass("hide");
                                    });
                                });

                            });
                            
                            $('[data-toggle="tooltip"]').tooltip();
                        }
                    }
                    
                    $("#selPolicy").select2("val","");

                }
                else $createAlert({status: "fail", title: "Failed", text: res.msg});
                
            },
            complete: function () {
                __hideLoadingAnimation();
            },
            error: function (r) {
                __hideLoadingAnimation();
                $createAlert({status: "fail", title: "Failed", text: r});
                console.log(r);
            }
        });
    },
    getallByClusterID:function($clusterID){
         var $data = {
            'c': 'LocationSettings',
            a: 'GetAllPolicy'
        };
        var $result ;
        if (typeof $clusterID != "undefined")
            $data["clusterID"] = $clusterID;

        $.ajax({
            type: 'POST',
            url: 'ajax.php',
            data: $data,
            dataType: 'json',
            async:false,
            beforeSend: function () {
               // __showLoadingAnimation();
            },
            success: function (res) {
                //console.log(res);
                if (res.status == 1) {
                    $result = res.data;
                }
            },
            complete: function () {
               // __hideLoadingAnimation();
               
            },
            error: function (r) {
               // __hideLoadingAnimation();
                console.log(r);
            }
        });
        return $result;
    },
    clusterIDChange:function($val){
        $("#clusterShelf").html("");
        selectValidation('selCluster','cluster','divCluster');
        var $polisyList = $mergePolicy.getallByClusterID($val);
        
        if($polisyList == null ) return false;
        var $len = $polisyList.length;
        
        if ($len > 0) {
            for (var i = 0; i < $len; i++) {
                var $item = $polisyList[i];
                var $htmlStr = '<div class="nodeWidget" id="widget_' + $item.ID + '" data-toggle="tooltip" data-placement="top" title="'+$item.Name+'">';
                $htmlStr += '<div class="nodeWidget-container hide">';
                $htmlStr += '<div class="nodeWidget-body">';
                $htmlStr += '<a href="javascript:void(0);" onclick="return $mergePolicy.removeConfirm(' + $item.ID + ');"><i class="fa fa-times"></i></a>';
                $htmlStr += $item.Name;
                $htmlStr += '</div>';

                $htmlStr += '<div class="nodeWidget-footer">';
                if ($item.Formate == 1)
                    $htmlStr += 'Weekly';
                else if ($item.Formate == 2)
                    $htmlStr += 'Single';
                else if ($item.Formate == 3)
                    $htmlStr += 'Overrider';
                $htmlStr += '</div>';
                $htmlStr += '</div>';
                $htmlStr += '</div>';
                $("#clusterShelf").append($htmlStr).promise().done(function (arg1) {
                    //console.log(arg1);
                    var $widgetelem = $("#widget_" + $item.ID);
                    $(".animate-path").html($widgetelem.html()).promise().done(function (arg1) {
                        $(".animate-path").removeClass("hide");
                        $(".animate-path .nodeWidget-container").removeClass("hide");
                        var offset = $widgetelem.offset();
                        var posY = offset.top - $(window).scrollTop();
                        var posX = offset.left - $(window).scrollLeft();
                        var pOffset = $("#selCluster").offset();
                        var pPosY = pOffset.top - $(window).scrollTop();
                        var pPosX = pOffset.left - $(window).scrollLeft();

                        var bezier_params = {
                            start: {
                                x: pPosX,
                                y: pPosY,
                                angle: 10,
                                length: 0.433
                            },
                            end: {
                                x: posX,
                                y: posY,
                                angle: -10,
                                length: 0.25
                            }
                        };

                        $(".animate-path").animate({path: new $.path.bezier(bezier_params)}, 500, function () {
                            $(".nodeWidget-container").removeClass("hide");
                            $(".animate-path").html("");
                            $(".animate-path").addClass("hide");
                        });
                    });

                });
                $('[data-toggle="tooltip"]').tooltip();
            }
        }
    },
    remove:function(){
        var $policyID = $("#delPolicyID").val();
         var $id = $("#selCluster").val();
        if ($id != 0) {
            var $data = {
                c: 'LocationSettings',
                a: 'removeMergePolicy',
                clusterID: $id,
                policyID:$policyID
            };

            $.ajax({
                type: 'POST',
                url: 'ajax.php',
                data: $data,
                dataType: 'json',
                beforeSend: function () {
                    __showLoadingAnimation();
                },
                success: function (res) {
                    var $status = res.status;
                    if ($status == 1) {

                        $createAlert({status: "success", title: "Successfully", text: res.msg});
                        if(res.id != null) $Policy.clear();
                        $("#widget_" + $policyID).remove();
                    }
                    else if ($status == 2){
                        //$createAlert({status: "fail", title: "Can't remove policy while its running !"});
                        $createAlert({status: "fail", title: "Failed", text: res.msg});
                    }
                    else if ($status == 0){
                        $createAlert({status: "fail", title: "Failed", text: res.msg});
                    }
                },
                complete: function () {
                    __hideLoadingAnimation();
                },
                error: function (r) {
                    __hideLoadingAnimation();
                    $createAlert({status: "fail", title: "Failed", text: r});
                }
            });
        }
        else {
            $createAlert({status: "info", title: "Warring", text: "Select policy on policy panel"});
        }
		 $("#modalDelCnfirmation").modal("hide");
    },
    removeConfirm :function($policyID){
        $("#btnCnfmYes").addClass("btn-success");
        $("#modalDelCnfirmation").modal("show");
        $("#delPolicyID").val($policyID);
    }


};    
var $timelineObj;
var $timeLineData = [];  
var $timeLineNextID = 0;
var $timeLinePervID = 0;
var $animateTimeout;
var $animateFinal;
var $timeLineRangeChage = 0;
var $timeLineIsRangeChaged = 0;
var $timelineTimer = null;
var $timelineLivemode = true;
var $TimeLine = {
    init :function ($elem,$startDate,$endDate){
        $timelineLivemode =false;
        if($timelineTimer != null) clearInterval($timelineTimer);
        // specify options
        var options = {
            width       :  '100%',
            //height   : '200px',
            minHeight   : '200',
            style       : 'box',
            box       : {align:'left'},
            showNavigation : true,
            animate     :true,
            animateZoom :true,
            start       : $startDate,
            end         : $endDate,
            //cluster     : true,
            moveable    : false,
            zoomable    : false,
            //stackEvents : false
        };
        
        // Instantiate our timeline object.
        $timelineObj = new links.Timeline(document.getElementById($elem), options);
        links.events.addListener($timelineObj, 'rangechange',$TimeLine.rangechange);
        links.events.addListener($timelineObj, 'select', $TimeLine.__filterAlertsByType);
        //$timelineObj.setVisibleChartRange($startDate,$endDate);
        
        // Draw our timeline with the created data and options
        $timelineObj.draw($timeLineData);
        $(".timeline-navigation-zoom-in").tooltip({container:"body"});
        $(".timeline-navigation-zoom-out").tooltip({container:"body"});
        $(".timeline-navigation-move-left").tooltip({container:"body"});
        $(".timeline-navigation-move-right").tooltip({container:"body"});
        
        $timelineObj.setVisibleChartRange($startDate, $endDate);
        
        //time line live tracking 
        var $current_time = new moment().format("H");
                    
        var $retryStatus = false;

        var $rangeID = $("#seldateRange").select2("val");
        var $timeRange = $("#selTimeRange").select2("val");
        
        if($rangeID == 0 && (($timeRange==$current_time) || ($timelineLivemode && ($timeRange == ( $current_time - 1))))) $retryStatus = true;
        else if($rangeID == 1 && 
            (new Date($endDate).getTime() == new Date($startDate).getTime()) && 
            (new Date($endDate).getDate() == new Date().getDate()) && (new Date($endDate).getMonth() == new Date().getMonth()))  $retryStatus = true;
        if($retryStatus){
            $timelineTimer = setInterval(function () {
                $timelineLivemode = true;
                var $rangeID = $("#seldateRange").select2("val");
                var $timeRange = $("#selTimeRange").select2("val");
                var current_time = new moment().format("HH");
                if($rangeID == 0 && ($timeRange == ( $current_time - 1))) $('#selTimeRange').select2("val",parseInt(current_time));
                $TimeLine.getEventData(2,$elem);
            }, __DashboardRunTime);
        }
        else{
            $timelineLivemode = false;
            if($timelineTimer != null) clearInterval($timelineTimer);
            $timelineTimer = null;
        }
    },
    getEventData :function($state,$elem){
        var $startDate =$crStartDate;
        var $endDate =$crEndDate;
        var $timelineSDate = new Date($startDate.getFullYear() + "-" + ($startDate.getMonth() + 1) + "-" + $startDate.getDate());
        var $timelineEDate = new Date($endDate.getFullYear() + "-" + ($endDate.getMonth() + 1) + "-" + $endDate.getDate());
		
        var $selType = $("#selDeviceType").select2("val");
        var $devID = $("#selDev").select2("val");
        var $rangeID = $("#seldateRange").select2("val");
        var $timeRange = $("#selTimeRange").select2("val");
        
        /*var $devID = $("#selDev").select2("val");
        var $paramID = $("#selParam").select2("val");
        var $paramText = $("#selParam").select2('data').text;
        var $rangeID = $("#seldateRange").select2("val");
        var $rangeText = $("#seldateRange").select2('data').text;
        var $timeRange = $("#selTimeRange").select2("val");
        
        //type 0=>events, 1=>savings, 2 =>Policy, 3=>peak demand
        var $contentArr = [
            {"msg" : "E_258","cls":"timeline-event-tag","type": 0},
            {"msg" : "S_12","cls":"timeline-savings-tag","type": 1},
            {"msg" : "P_245","cls":"timeline-policy-tag","type": 2},
            {"msg" : "E_23","cls":"timeline-event-tag","type": 0},
            {"msg" : "S_8","cls":"timeline-savings-tag","type": 1},
            {"msg" : "P_12","cls":"timeline-policy-tag","type": 2},
            {"msg" : "PEAK DEMAND","cls":"timeline-peak-tag","type": 3},
            {"msg" : "E_200","cls":"timeline-event-tag","type": 0},
            {"msg" : "E_29","cls":"timeline-event-tag","type": 0},
            {"msg" : "E_45","cls":"timeline-event-tag","type": 0}
        ];
        
        var $currentdate = new Date();
        var $eDate = $endDate;
        if($eDate > $currentdate) $eDate = new Date();
        else $eDate = $endDate;
        $timeLineData = [];
        for(var $i=0; $i < 10; $i++){
            
            var $content =  $contentArr[$i]["msg"];
            var $className = $contentArr[$i]["cls"];
            var $type = $contentArr[$i]["type"];
           
            if($rangeID == 0) {
                var $d = new Date(new Date().setMinutes(Math.floor((Math.random() * (new moment().format("m"))) + 1)));
                $timeLineData.push({
                    start       : $d,
                    content     : $content,
                    className   : $className,
                    t        : $type
                });
            }
            else if($rangeID == 1) {
                var $d = new Date($startDate.getTime() + Math.random() * ($endDate.getTime() - $startDate.getTime()));
                if($d.getHours() > $currentdate.getHours()) $d = new Date($d.setHours(Math.floor((Math.random() * (new moment().format("H"))) + 1)));
                $timeLineData.push({
                    start       : $d,
                    content     : $content,
                    className   : $className,
                    t        : $type
                });
            }
            else{
                var $d = new Date($startDate.getTime() + Math.random() * ($endDate.getTime() - $startDate.getTime()));
                if($d.getDate() > $currentdate.getDate()) $d = new Date($d.setDate((Math.floor((Math.random() * (new moment().format("D"))) + 1))));
                $timeLineData.push({
                    start       : $d,
                    content     : $content,
                    className   : $className,
                    t        : $type
                });
            }
        }

        $TimeLine.init($elem,$startDate,$endDate);*/
        if($rangeID == 0){
            $timelineSDate.setHours($timeRange);
            $timelineSDate.setMinutes(0);
            $timelineSDate.setSeconds(0);

            $timelineEDate.setHours($timeRange);
            $timelineEDate.setMinutes(59);
            $timelineEDate.setSeconds(59);
        }
        else if($rangeID == 1 && (new Date($endDate).getTime() == new Date($startDate).getTime())){
            $timelineSDate.setHours(0);
            $timelineSDate.setMinutes(0);
            $timelineSDate.setSeconds(0);
            $timelineEDate.setHours(23);
            $timelineEDate.setMinutes(59);
            $timelineEDate.setSeconds(59);
        }
        else {
            $timelineSDate.setHours(0);
            $timelineSDate.setMinutes(0);
            $timelineSDate.setSeconds(0);
            $timelineEDate.setHours(23);
            $timelineEDate.setMinutes(59);
            $timelineEDate.setSeconds(59);
        }
        
        if($state == 1){
            $timeLineData = [];
        }
        $clientReport.__generateRandEvents(2,-1,$timelineSDate,$timelineEDate,$selType,$devID,$rangeID,0);
        
        var $data = {c:'Dashboard',
            a:'getEventData',
            sDate:$timelineSDate.getFullYear() + "-" + ($timelineSDate.getMonth() + 1) + "-" + $timelineSDate.getDate()+" "+$timelineSDate.getHours()+":"+$timelineSDate.getMinutes()+":"+$timelineSDate.getSeconds(),
            eDate:$timelineEDate.getFullYear() + "-" + ($timelineEDate.getMonth() + 1) + "-" + $timelineEDate.getDate()+" "+$timelineEDate.getHours()+":"+$timelineEDate.getMinutes()+":"+$timelineEDate.getSeconds(),
            selectType:$selType,
            devID : $devID,
            state : $state,
            range :$rangeID
        }
        
        $.ajax({
                type: 'POST',
                url: 'ajax.php',
                data: $data,
                dataType: 'json',
                beforeSend: function () {
                    //__showLoadingAnimation();
                },
                success: function (res) {
                    //$timeLineData.
                    var $eventCount = res.eventCount;
					if($state == 1)$timeLineData = [];
                    if ($eventCount.length > 0) {
                        for(var $i = 0;$i < $eventCount.length;$i++){
                           $timeLineData.push({
                                start           : new Date($eventCount[$i]["start"]),
                                content         : $eventCount[$i]["content"],
                                className       : $eventCount[$i]["className"],
                                t               : $eventCount[$i]["t"],
                                filterType      : $eventCount[$i]["filterType"]
                            });
                        }
                        if($state == 2) $TimeLine.updateTimeLine($elem,$eventCount.length);//Update
                    }
                    
                    
                },
                complete: function () {
                    if($state == 1) $TimeLine.init($elem,$timelineSDate,$timelineEDate);//initialize
                    //if($prevent == 1) $timeLineRangeChage = 0;
                },
                error: function (r) {
                    console.log(r);
                    if($state == 1) $TimeLine.init($elem,$timelineSDate,$timelineEDate);//initialize
                }
        });
    },
    
    updateTimeLine :function ($elem,$len){
        if($len > 0) $timelineObj.setData($timeLineData);
        //if($timeLineIsRangeChaged == 0) $TimeLine.animateTo();
    },
    rangechange:function($data){
        if($timeLineRangeChage == 0){
            $timeLineRangeChage = 1;
            $timeLineIsRangeChaged = 1;
            $(".live-timeline").removeClass("live");
            $(".live-timeline").addClass("offline");
            $(".live-timeline span").html("Offline");
            $TimeLine.animateCancel();
            $TimeLine.getEventData(2,"#timeLineContainer",0,$timeLinePervID,1);
        }
       
    },
    animateTo:function(){
        var $date = new Date();//.getFullYear()+","+(new Date().getMonth()+1)+","+new Date().getDate()+","+new Date().getHours()+","+new Date().;
        // get the new final date
        $animateFinal = $date.valueOf();
        $timelineObj.setCustomTime($date);
        
        // cancel any running animation
        $TimeLine.animateCancel();
        
        //animate towards the final date
        var animate = function () {
            var range = $timelineObj.getVisibleChartRange();
            var current = (range.start.getTime() + range.end.getTime())/ 2;
            var width = (range.end.getTime() - range.start.getTime());
            var minDiff = Math.max(width / 1000, 1);
            var diff = ($animateFinal - current);
            if (Math.abs(diff) > minDiff) {
                // move towards the final date
                var start = new Date(range.start.getTime() + diff / 4);
                var end = new Date(range.end.getTime() + diff / 4);
                $timelineObj.setVisibleChartRange(start, end);
                // start next timer
                $animateTimeout = setTimeout(animate, 50);
            }
        };
        animate();
    },
    animateCancel :function(){
        if ($animateTimeout) {
            clearTimeout($animateTimeout);
            $animateTimeout = undefined;
        }
    },
    triggerLive:function(){
        $timeLineRangeChage = 0;
        $timeLineIsRangeChaged = 0;
        $(".live-timeline").removeClass("offline");
        $(".live-timeline").addClass("live");
        $(".live-timeline span").html("Live");
        $TimeLine.animateTo();
    },
    __filterAlertsByType : function (){
        
        var $selType = $("#selDeviceType").select2("val");
        var $devID = $("#selDev").select2("val");
        var $rangeID = $("#seldateRange").select2("val");
        var $timeRange = $("#selTimeRange").select2("val");
        var sel = $timelineObj.getSelection();
        var data = $timelineObj.getData();
        
        if (sel.length) {
          if (sel[0].row != undefined) {
            var row = sel[0].row;
            var $Type = data[row]['t']; 
            var $selStart = new Date(data[row]['start']); 
            var $filterType = data[row]['filterType']; 
            $clientReport.__generateRandEvents(1,$Type,$selStart,$selStart,$selType,$devID,$rangeID,$filterType);
            $('.db-alerts, #btnClearAlertFilter').removeClass('hidden'); 
            $clientReport.__globalAlertsFilterApplied = false;
            $('#alertsTitle').html('Viewing '+ data[row]['content']);
          }
        }     
    },
    __clearAlertFilter :function(){
        $('.dashboardEvents').html("");
        $('#alertsTitle').html("");
        $clientReport.__globalAlertsFilterApplied = true;
    }
};

function formatAMPM(date,$isSplit) {
  var hours = date.getHours();
  var minutes = date.getMinutes();
  var ampm = hours >= 12 ? 'pm' : 'am';
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? '0'+minutes : minutes;
  var strTime = hours + ':' + minutes + ' ' + ampm;
  if(typeof $isSplit == "undefined" ) return strTime;
  else return [hours,minutes,ampm];
  
}
var $liveChartObj;
var $liveChartIntCallObj;
var $liveBarChartData = [];
var $liveChartStatus = 0;
var $liveChart = {
    init:function(){
        $ClusterTree.getAllCluster("#selLiveCluster");
        $ClusterTree.getAllNode("#selLiveNode");
        $("#selLiveUnit").select2();
        //$liveChart.getJsonData(1);
    },
    changeType:function($type){
        /*if($type == 1){
            $("#chLiveChartAll").prop("checked","checked");
            $("#chLiveChartDev").removeAttr("checked");
            $("#chLiveChartCluster").removeAttr("checked");

            $("#selLiveNode").select2("enable",false);
            $("#selLiveCluster").select2("enable",false);
            $("#selLiveNode").select2("val",-1);
            $("#selLiveCluster").select2("val",-1);
        }*/
        if($type == 1){
            $("#chLiveChartDev").prop("checked","checked");
            $("#chLiveChartCluster").removeAttr("checked");

            $("#selLiveNode").select2("enable",true);
            $("#selLiveCluster").select2("enable",false);
            
            $("#selLiveCluster").select2("val",-1);
        }
        else if($type == 2){
            $("#chLiveChartCluster").prop("checked","checked");
            //$("#chLiveChartAll").removeAttr("checked");
            $("#chLiveChartDev").removeAttr("checked");

            $("#selLiveNode").select2("enable",false);
            $("#selLiveCluster").select2("enable",true);
            $("#selLiveNode").select2("val",-1);
        }
        
        
    },
    getJsonData:function($status){
        var $type = 1;
        var $nodeID = 0;
        var $clusterID = 0;
        var $retnVal = [];
        //var $barChartData = [];
        var $legendMasterList = [];
        var $unit =  $("#selLiveUnit").select2("val");
        var $currentLegend = [];
        if($unit == 1)$currentLegend = [0,1];
        if($unit == 2)$currentLegend = [2];
        if($unit == 3)$currentLegend = [3];
        if($('#chLiveChartDev').is(':checked')){
            $type = 2;
            $nodeID = $("#selLiveNode").select2("val");
            $nodeID = ($nodeID != -1 ? $nodeID : 0);
            $clusterID = 0;
        }
        else if($('#chLiveChartCluster').is(':checked')){
            $type = 3;
            $clusterID = $("#selLiveCluster").select2("val");
            $clusterID = ($clusterID != -1 ? $clusterID : 0);
            $nodeID = 0;
        }
        if($status == 1){
            $liveBarChartData = [];
            $liveChartStatus = 0;
            if($liveChartIntCallObj != null) clearInterval($liveChartIntCallObj);
        }
        else $liveChartStatus=1;
        
        var $dateKey = new Date();
        var $legendMasterList = [];
        $.ajax({
            type: 'POST',
            url: 'ajax.php',
            data: {'c': 'Chart', 'a': 'getTagLiveStatus', 'type': $type, 'nodeID': $nodeID, 'clusterID': $clusterID,'unit':$unit},
            dataType: 'json',
            async: false,
            beforeSend: function () {
                //__showLoadingAnimation();
            },
            success: function (res) {
                //console.log( res.result);
                //gaugeValues = res.result;
                var $nodeArr = res.nodeResult;
                if(res.status == 1){
                    var $chartArr = res.result;
                    if($chartArr.length > 0){
                        var $legCount = $chartArr.length;
                        if($unit == 3) $legCount = $nodeArr.length;
                        
                        for (var i = 0; i < $legCount; ++i) {
                            for (var j = 0; j < $currentLegend.length; ++j) {
                                //console.log($liveBarChartData.length);
                                var $identity = $legendsArr[$currentLegend[j]];
                                ///console.log($identity);
                                var $chatVal = $chartArr[i][$identity];
                                if($chatVal == null) $chatVal = 0;
                                
                                if($liveChartStatus == 0){
                                    var $chartFillCol = stringToColour($identity); //chartIdentityLineColor[];
                                    var $labelStr = $identity;
                                    
                                    if($nodeArr.length > 0 && $unit == 3){
                                        $labelStr = $labelStr+" "+$nodeArr[i]["TagName"];
                                        $chartFillCol = stringToColour($nodeArr[i]["TagName"]); //chartMotionColor[i];
                                    }
                                    //console.log($labelStr);
                                    var $dataObj = {
                                        yAxisID:$legendsIdentityArr[0][$identity],
                                        label: $labelStr,
                                        backgroundColor: $chartFillCol,
                                        data: [$chatVal],
                                        borderColor: $chartFillCol,
                                        fill:false
                                    };
                                    $legendMasterList.push($dataObj);
                                }
                                else{
                                   var $legData = $liveChartObj.config.data.datasets;;
                                   
                                    for(var x=0; x < $legData.length; x++){
                                        var $yID = $legData[x]["yAxisID"];
                                        var $xLabel = $legData[x]["label"];
                                        var $labelStr = $identity;
                                        if($nodeArr.length > 0 && $unit == 3) $labelStr = $labelStr+" "+$nodeArr[i]["TagName"];
                                        //console.log($yID + " ::::: "+$legendsIdentityArr[0][$identity]+" ====== "+$xLabel+" >>>>>> "+$labelStr);
                                        if($yID == $legendsIdentityArr[0][$identity] && $xLabel == $labelStr){
                                            var $legDataset = $legData[x]["data"];
                                            
                                            if($legDataset.length == 15){
                                                $legDataset.shift();
                                                //$liveChartObj.update();
                                            }
                                            
                                            $legDataset.push($chatVal);
                                            $liveChartObj.update();
                                            //console.log($legDataset.length);
                                            //$liveBarChartData["datasets"][x]["data"] = $legDataset;
                                        }
                                    }
                                    //console.log($legData);
                                }
                            }
                        }
                    }
                }
                else{
                    //console.log($nodeArr);
                    var $legCount = 1;
                    if($unit == 3) $legCount = $nodeArr.length;
                    for (var i = 0; i < $legCount; ++i) {
                     for (var j = 0; j < $currentLegend.length; ++j) {
                            if($liveBarChartData.length == 0){
                               var $identity = $legendsArr[$currentLegend[j]];
                               ///console.log($identity);
                               var $chatVal = 0;
                               var $chartFillCol = stringToColour($identity); //chartIdentityLineColor[$identity];
                               var $labelStr = $identity;
                               if($unit == 3  && $nodeArr.length > 0){
                                   $chartFillCol = stringToColour($nodeArr[i]["TagName"]);
                                   $labelStr = $identity+" "+$nodeArr[i]["TagName"];
                               }
                               //console.log($labelStr);

                               var $dataObj = {
                                   yAxisID:$legendsIdentityArr[0][$identity],
                                   label: $labelStr,
                                   backgroundColor: $chartFillCol,
                                   data: [$chatVal],
                                   borderColor: $chartFillCol,
                                   fill:false
                               };
                               $legendMasterList.push($dataObj);

                            }
                            else{
                                var $chatVal = 0;
                                var $identity = $legendsArr[$currentLegend[j]];
                                var $legData = $liveChartObj.config.data.datasets;
                                for(var x=0; x < $legData.length; x++){
                                    var $yID = $legData[x]["yAxisID"];
                                    if($yID == $legendsIdentityArr[0][$identity]){
                                        var $legDataset = $legData[x]["data"];
                                        if($legDataset.length == 15)$legDataset.shift();
                                        $legDataset.push($chatVal);
                                        $liveChartObj.update();
                                        //$liveBarChartData["datasets"][x]["data"] = $legDataset;
                                    }
                                }
                            }
                        }
                    }
                }
               
               if($liveChartStatus == 0){
                    $liveBarChartData = {'labels':[$dateKey],'datasets':$legendMasterList};
                }
                else{
                    var $labelArr = $liveChartObj.config.data.labels;
                    $labelArr.push($dateKey);
                    //console.log($labelArr.length);
                    if($labelArr.length > 15) $labelArr.shift();
                    
                    $liveChartObj.update();
                    //console.log($labelArr.length);
                    /*$liveBarChartData["datasets"][x]["data"] = $legData;
                    console.log($liveBarChartData["labels"]);*/
                }
                //console.log($liveChartObj);
            },
            complete: function () {
                if($status == 1){
                    $liveChart.initBarChart($status,$currentLegend);
                }
                /*else if($status == 2){
                    $liveChart.initBarChart(2,$currentLegend);
                }*/
            },
            error: function (r) {
                console.log(r);
            }
        });
        
        //return $liveBarChartData;
    },
    initBarChart:function($type,$currentLegend){
        //console.log("here.....1");
        
        if ($type == 1) {
            var yAxisData = [];
            for (var j = 0; j < $currentLegend.length; ++j) {

                var $identity = $legendsArr[$currentLegend[j]];
                var yaxisObj = {
                    type: "linear", // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
                    display: true,
                    position: "left",
                    id: $legendsIdentityArr[0][$identity],
                    //color:chartIdentityColor[$legendsIdentityArr[0][$identity]],
                    scaleLabel: {
                        display: true,
                        labelString: $legendsIdentityArr[0][$identity]//,
                                //fontColor: chartIdentityColor[$legendsIdentityArr[0][$identity]]
                    },
                    ticks: {
                        //fontColor: chartIdentityColor[$legendsIdentityArr[0][$identity]]
                    }
                };
                yAxisData.push(yaxisObj);
            }
            //console.log("here.....2");
            var xAxisData = [{
                    time: {
                        format: "HH:MM:ss",
                        tooltipFormat: "HH:MM:ss"
                    },
                    scaleLabel: {
                        display: true,
                        labelString: "Time"
                    },
                    ticks: {
                        callback: function (dataLabel, index) {
                            return dataLabel.format("HH:MM:ss");
                        }
                    }
                }];
            //console.log("here.....6");
            //Chart Options
            var chartOptions = {
                type: "line",
                data: $liveBarChartData,
                options: {
                    responsive: true,
                    hoverMode: 'label',
                    hoverAnimationDuration: 400,
                    stacked: false,
                    borderWidth: 10,
                    title: {
                        display: false
                    },
                    tooltips: {
                        //enabled:false,
                        mode: 'single',
                        callbacks: {
                            label: function (tooltipItems, data) {
                                //var cDispUnit = (data.datasets[tooltipItems.datasetIndex].unitChart);
                                var cDispVal = tooltipItems.yLabel;/// data.datasets[tooltipItems.datasetIndex].unitChartDivisor;
                                var chartDispParam = data.datasets[tooltipItems.datasetIndex].label;
                                return chartDispParam + ': ' + RoundOneDecimal(cDispVal, 1);
                            }
                        }
                    },
                    scales: {
                        xAxes: xAxisData,
                        yAxes: yAxisData
                    }
                }
            };
            $("#div-chart-wrapper").html("");
            $("#div-chart-wrapper").html('<canvas id="canvas-chart" style="width: 995px; height: 497px;" width="995" height="300"></canvas>');
            var ctx = document.getElementById("canvas-chart").getContext("2d");
            $liveChartObj = new Chart(ctx, chartOptions);
            
            $liveChartIntCallObj = setInterval(function () {
                $liveChart.getJsonData(2)
            }, 5000);
            //console.log($liveChartObj);
        }
        else {
            //$liveChartObj.config.data = $liveBarChartData;
            //$liveChartObj.update();
        }

        

        //console.log("here.....7");
        ///console.log(JSON.stringify(chartOptions));
       
        
        if($type == 1){
           
        } 
    }
};

//Color generated function 
var stringToColour = function(str) {
    var hash = 0;
    for (var i = 0; i < str.length; i++) {
        hash = str.charCodeAt(i) + ((hash << 5) - hash);
    }
    var colour = '#';
    for (var i = 0; i < 3; i++) {
        var value = (hash >> (i * 8)) & 0xFF;
        colour += ('00' + value.toString(16)).substr(-2);
    }
    return colour;
}

var $gauges = {
    init : function(){
        $ClusterTree.getAllCluster("#selGaugesCluster");
        $ClusterTree.getAllNode("#selGaugesNode");
        $gauges.getAllGauge();
    },
    changeType:function($type){
        //console.log("Test....");
        /*if($type == 1){
            $("#chLiveChartAll").prop("checked","checked");
            $("#chLiveChartDev").removeAttr("checked");
            $("#chLiveChartCluster").removeAttr("checked");

            $("#selLiveNode").select2("enable",false);
            $("#selLiveCluster").select2("enable",false);
            $("#selLiveNode").select2("val",-1);
            $("#selLiveCluster").select2("val",-1);
        }*/
        if($type == 1){
            $("#chGaugesDev").prop("checked","checked");
            $("#chGaugesCluster").removeAttr("checked");

            $("#selGaugesNode").select2("enable",true);
            $("#selGaugesCluster").select2("enable",false);
            
            $("#selGaugesCluster").select2("val",-1);
        }
        else if($type == 2){
            $("#chGaugesCluster").prop("checked","checked");
            //$("#chLiveChartAll").removeAttr("checked");
            $("#chGaugesDev").removeAttr("checked");

            $("#selGaugesNode").select2("enable",false);
            $("#selGaugesCluster").select2("enable",true);
            $("#selGaugesNode").select2("val",-1);
        }       
        
    },
    oldGaugeCavas : [], 
    displayGuages:function(){
        var $nodeID = 0;
        var $clusterID = 0;
        var $type = 0;
        if($('#chGaugesDev').is(':checked')){
            $type = 1;
            $nodeID = $("#selGaugesNode").select2("val");
            $nodeID = ($nodeID != -1 ? $nodeID : 0);
            $clusterID = 0;
        }
        else if($('#chGaugesCluster').is(':checked')){
            $type = 2;
            $clusterID = $("#selGaugesCluster").select2("val");
            $clusterID = ($clusterID != -1 ? $clusterID : 0);
            $nodeID = 0;
        }
        $.ajax({
            type: 'POST',
            url: 'ajax.php',
            data: {'c': 'Dashboard', 'a': 'GetGuages','type': $type, 'nodeID': $nodeID, 'clusterID': $clusterID},
            dataType: 'json',
            beforeSend: function() {__showLoadingAnimation();},
            success: function (res) {
                //console.log(res);
                if (res.status == 1) {
                    var $allGuages = res.allGuage;
                    var $guagesVal = res.getValue;
                    var $guages = res.reuslt;
                    //var $alertData = res.alerts;
                    //var AVGMinMaxData = res.AVGMaxMinData[0];
                    //showTemGuages($guagesVal[0]);
                    //GuageDisplaySetting($allGuages);
                    //showAlert($alertData);
                    //guageSettingsList = $allGuages;
                    setGuageSettings(0);
                    pageGuages = {};
                    
                    // Remove Collections If any 
                     var neededGauges = [];
                     if( Gauge.Collection.length >  0 ){
                        if( $gauges.oldGaugeCavas.length >  0 ){
                            $gauges.oldGaugeCavas.forEach( function(renderElem, gid) {
                                Gauge.Collection.forEach(function (result, index) {
                                    if (result.config.renderTo == renderElem) {
                                        //Remove from collection
                                        Gauge.Collection.splice(index, 1);
                                    }
                               });
                            });
                        }
                    }
                    
                    $("#guageContainer").html("").promise().done(function() {
                        if($guagesVal.length > 0){
                            for(var $nCount = 0; $nCount < $guagesVal.length; $nCount++){
                                if($guagesVal[$nCount]["ID"] != null && $guagesVal[$nCount]["ID"] != ""){
                                    for (gCount = 0; gCount < $guages.length; gCount++) {
                                        var guage = $guages[gCount];
                                        $gauges.ShowGuage(guage, $guagesVal[$nCount]);
                                    }
                                }
                            }
                        }
                        else $("#guageContainer").html("<h5 class=\"text-center text-l-help\">No nodes available.</h5>")
                    });
                    //console.log($guagesVal);
                    /*if (typeof ($guagesVal[0]) != "undefined") {
                        if (typeof ($guagesVal[0].logdate) != "undefined") {
                            var $logDate = $guagesVal[0].logdate;
                            $dt = new Date($logDate);
                            $(".log-time").html(("0" + $dt.getHours()).slice(-2) + ":" + ("0" + $dt.getMinutes()).slice(-2));
                        }
                        else
                            $(".log-time").html("00:00");
                    }
                    else
                        $(".log-time").html("00:00");*/
                }
            },
            complete:function (){__hideLoadingAnimation();},
            error: function (r) {
                __hideLoadingAnimation();
                console.log(r);
            }
            
        });
    },
    setLogData: function () {
        var $nodeID = 0;
        var $clusterID = 0;
        var $type = 0;
        var pageName = getPageName();
        if(pageName == "gauges"){
            if($('#chGaugesDev').is(':checked')){
                $type = 1;
                $nodeID = $("#selGaugesNode").select2("val");
                $nodeID = ($nodeID != -1 ? $nodeID : 0);
                $clusterID = 0;
            }
            else if($('#chGaugesCluster').is(':checked')){
                $type = 2;
                $clusterID = $("#selGaugesCluster").select2("val");
                $clusterID = ($clusterID != -1 ? $clusterID : 0);
                $nodeID = 0;
            }
        }
        $.ajax({
            type: 'POST',
            url: 'ajax.php',
            data: {'c': 'Dashboard', 'a': 'GetGuageValue', 'pagename': pageName,'type': $type, 'nodeID': $nodeID, 'clusterID': $clusterID},
            dataType: 'json',
            success: function (res) {
                var $alertData = res.alerts;
                var $alertCount = res.alertsCount
                if (res.status == 1) {
                    var $getStatus = res.reuslt;
                    var $allGauges = res.allGauges;
                    if(pageName == "gauges" && $getStatus.length > 0 ){
                        for(var $i=0;$i < $getStatus.length; $i++){
                            var $item = $getStatus[$i];
                            $nodeID = $item.ID;
                            for (var $j = 0; $j < $allGauges.length; $j++) {
                                var $identity =$allGauges[$j].Identity;
                                pageGuages[$identity+ "_" + $nodeID].setValue(GetGaugeValueByIdentity($identity, $item));
                            }
                        }
                    }
                    
                    if($alertCount > 0){
                        $("#__notify span").html($alertCount);
                         $("#__notify .notification-bubble").removeClass("hide");
                    }
                    /*if (pageName == "dashboard") {
                        if ($HB.length > 0) {
                            ResetGaugesValues($HB[0]); //console.log("111");
                            showTemGuages($HB[0]);
                            if (typeof ($HB[0].logdate) != "undefined") {
                                var $logDate = $HB[0].logdate;
                                $dt = new Date($logDate);
                                $(".log-time").html(("0" + $dt.getHours()).slice(-2) + ":" + ("0" + $dt.getMinutes()).slice(-2));
                            }
                            else
                                $(".log-time").html("00:00");
                        }
                        else {
                            SetGaugesValuesMin(); //console.log("111");
                            $(".log-time").html("00:00");
                        }
                        showAlert($alertData);
                        showAVGMinMax(AVGMinMaxData);
                    }*/
                    if(pageName == "alertList" && $alertData.length > 0 ){
                        //console.log($alertData);
                        var $count = 0;
                        for(var $i=($alertData.length - 1); $i >= 0; $i--){
                            var $item = $alertData[$i];
                            var $id = $item.ID;
                            if($("#alert_content_"+$id).length == 0){
                                if($count == 0) $(".notify-buble-lg").removeClass("flash");
                                $(".panel-heading").removeClass("hide");
                                $("#alertContainer .text-l-help").html("");
                                $("#alertContainer").prepend(getAlertContent($item));
                                $count++;
                            }
                            
                        }
                    }
                    else{
                        //console.log("start alert section");
                        //console.log($alertData);
                        for(var $i=($alertData.length - 1); $i >= 0; $i--){
                            var $item = $alertData[$i];
                            var $type = $item.AlertType;
                            var $severity = $item.Severity;
                            var $title = $item.Title;
                            var $msg = $item.Description;
                            //console.log("$type ==== "+$type+"  $severity ==== "+$severity);
                            if($type == 2 || $type == 3 || $type == 4){
                                if($severity == 1){//Normal
                                    $createAlert({status: "success", title: $title, text: $msg});
                                    if(pageName == "maintenance"){
                                        location.reload();
                                    }
                                }
                                else if($severity == 7){//critical
                                    $createAlert({status: "fail", title: $title, text: $msg});
                                }
                            }
                        }
                    }
                }
                else{
                    $("#__notify .notification-bubble").addClass("hide");
                }

                /*if ($alertData.length > 0) {
                    $("#__notify span").html($alertData.length);
                    $("#__notify").removeClass("hide");
                }
                else
                    $("#__notify").addClass("hide");*/
            },
            error: function (r) {
                console.log(r);
            }
        });
    },
    
    //function show guage on DOM
    ShowGuage: function (guage, valueG, elem) {
        try {

            var identity = (guage.label).replace(/ /g, '');
            var emptyElement = true;
            var displayHTML = ""
            var $nodeID = valueG.ID;
            var $nodeContainer = "#nodeGaugeContainer_" + $nodeID;
            var orderElem = guage.display_order//orderJSON[identity];

            //If already exists gauge
            if (typeof pageGuages != 'undefined') {
                if (typeof pageGuages[identity + "_" + $nodeID] != 'undefined') {
                    return false;
                }
            }
            if (typeof (elem) !== 'undefined')
                emptyElement = false;
            if (emptyElement) {
                //console.log(identity);
                /*if (identity == "KWH" || identity == "KVA" || identity == "KVAR") {
                 if (identity == "KVAR")
                 displayHTML += '<div class="col-sm-4 text-center" id="div_' + identity + '">'
                 else
                 displayHTML += '<div class="col-sm-4 text-center b-r-1" id="div_' + identity + '">'
                 displayHTML += '</div>';
                 $("#AVGguageContainer").append(displayHTML);
                 }
                 else {*/
                var $borderClass = "b-r-1";
                if(identity == "Motion") $borderClass = "";
                displayHTML += '<div class="col-sm-3 text-center div-gauge-data gauge-' + identity + ' ' + $borderClass + '" data-order="' + orderElem + '" id="div_' + identity + '_' + $nodeID + '">';
                displayHTML += '</div>';

                if ($("#guageContainer " + $nodeContainer).length == 0) {
                    var $containerHtml = "<div class=\"panel panel-default m-b-10 panel-savings m-b-15\">";
                    $containerHtml += "<div class=\"panel-heading clearfix p-b-0 p-t-10\" style=\"min-height:0px\">";
                    $containerHtml += "    <div class=\"panel-title title-metal\">";
                    $containerHtml += valueG.Name;
                    $containerHtml += "    </div>";
                    $containerHtml += "</div>";
                    $containerHtml += "<div class=\"panel-body p-b-10\">";
                    $containerHtml += "    <div class=\"row\" id=\"nodeGaugeContainer_" + $nodeID + "\">";

                    $containerHtml += "    </div>"
                    $containerHtml += "</div>"
                    $containerHtml += "</div>"
                    $("#guageContainer").append($containerHtml)
                }
                //console.log("test");
                //$("#guageContainer").append(displayHTML);
                
                var firstOrder = $($nodeContainer + " div.div-gauge-data:first").data("order");
                var lastOrder = $($nodeContainer + " div.div-gauge-data:last").data("order");

                if (typeof firstOrder == 'undefined') {
                    $($nodeContainer).append(displayHTML);
                    //console.log(1);
                }
                else if (parseInt(firstOrder) > parseInt(orderElem)) {
                    $($nodeContainer).find("[data-order='" + firstOrder + "']").before(displayHTML);
                    ///console.log(2);
                }
                else if (parseInt(lastOrder) < parseInt(orderElem)) {
                    $($nodeContainer).append(displayHTML);
                    //console.log(3);
                }
                else {
                    var afterElement = 0;
                    $(".div-gauge-data").each(function () {
                        if (parseInt($(this).data("order")) < parseInt(orderElem)) {
                            afterElement = $(this).data("order");
                        }
                    });
                    $($nodeContainer).find("[data-order='" + afterElement + "']").after(displayHTML);
                    //console.log("data-tt == "+afterElement);
                }
                //}
            }

            //Create canvas element
            var canvdiv = document.getElementById('div_' + identity + '_' + $nodeID);
            var canvGauge = document.createElement('canvas');
            canvGauge.setAttribute('id', '__' + identity + '_' + $nodeID);
            canvGauge.setAttribute('data-vaiable', identity);
            
            this.oldGaugeCavas.push('__' + identity + '_' + $nodeID);
            
            //console.log('div_' + identity + '_' + $nodeID);
            //console.log(canvdiv);
            /*if (identity == "KWH" || identity == "KVA" || identity == "KVAR") {
             canvGauge.setAttribute('class', 'canvas-guage-pmi');
             canvGauge.setAttribute('width', '184');
             canvGauge.setAttribute('height', '184');
             }
             else {*/
            canvGauge.setAttribute('class', 'canvas-guage-pmi cursor can-' + identity);
            //canvGauge.setAttribute('data-toggle', 'tooltip');
            //canvGauge.setAttribute('data-placement', 'left');
            //canvGauge.setAttribute('title', 'Open chart');
            canvGauge.setAttribute('width', '200');
            canvGauge.setAttribute('height', '200');
            //canvGauge.setAttribute('onclick', "gotoChart(\'" + identity + "\');");
            //}
            var maxVal = guage.max_val;

            /*if(identity == "Voltage" && typeof valueG != "undefined"){
             if(typeof valueG.model_number != "undefined"){
             maxVal = getVoltageMaxVal(model[valueG.model_number]);
             //console.log(maxVal);
             }
             }*/
            canvdiv.appendChild(canvGauge);
            //Canvas options
            //console.log(guage.label + " " + guage.model);
            var guageOptions = {
                elem: canvGauge.id,
                title: guage.label,
                units: guage.unit,
                minValue: guage.min_val,
                maxValue: maxVal,
                minorTicks: guage.min_mark_val,
                majorTicks: (guage.markers).split(','),
                id: guage.ID + "_" + $nodeID,
                value: 0,
                autok: guage.auto_k
            }
            //console.log(JSON.stringify(guageOptions));
            if (guage.custom) {
                if (guage.color != '') {
                    c = eval("(" + guage.color + ")");
                    guageOptions['highlights'] = c;
                }
            }

            if (typeof pageGuages === 'undefined') {
                //if($("#div-guage-list div.col-sm-8").length > 0) $("#div-guage-list div.col-sm-8").remove();
                pageGuages = {};
            }

            //Initialize gauge canvas
            if (typeof pageGuages[identity + "_" + $nodeID] === 'undefined') {
                pageGuages[identity + "_" + $nodeID] = new GuageComponent();
                pageGuages[identity + "_" + $nodeID].initialize(guageOptions);
            }
            //Initialize gauge canvas
            //pageGuages[identity] = new GuageComponent();
            //pageGuages[identity].initialize(guageOptions);
        }
        catch (ex) {
            console.log(JSON.stringify(ex));
        }

    },
    //Get All Gauge
    getAllGauge : function(){
        $.ajax({
            type: 'POST',
            url: 'ajax.php',
            data: {'c': 'Dashboard', 'a': 'GetAllGauge'},
            dataType: 'json',
            success: function (res) {
                if (res.status == 1) {
                    var $allGuages = res.result;
                    guageSettingsList = $allGuages;
                    setGuageSettings(0);
                }
            },
            error: function (r) {
                console.log(r);
            }
        });
    }
        
};



/*********************
 *  Terminal Emulator
 *********************/
var $terminalEle = {
    init : function($nodeID){
        $.ajax({
            type: 'POST',
            url: 'ajax.php',
            data: {'c': 'Terminal', 'a': 'createTerminal','nodeID' : $nodeID},
            dataType: 'json',
            success: function (res) {
                //console.log(res);
                if (res.status == 1) {
                    $(".text-l-help").addClass("hide");
                    $("#termal-container").removeClass("hide");
                    
                    $activeNode = $nodeID;
                    var $name = res.name;
                    var $ipAddress = res.ipAddress;
                    $("#termal-container .title").html($name+"  "+$ipAddress);
                    if($('.term-inputbox').hasClass("terminal")){
                        $('.term-inputbox').terminal().destroy();
                    }
                    $('.term-inputbox').terminal(function(command, term) {
                        if (command !== '') {
                            try {
                                term.echo("[[i;#FFEB3B;<tr-waiting>]Command sent. Waiting for response...]");
                                $terminalEle.exeCommand(command,term);

                            } catch(e) {
                                term.error(new String(e));
                            }
                        } else {
                           term.echo("[[i;#ff3300;]Invalid command]");
                        }
                    }, {
                        greetings: '',
                        name: 'js_demo',
                        height: 500,
                        prompt: $name + '> '
                    });
                }
                else {
                    $(".text-l-help").removeClass("hide");
                    $("#termal-container").addClass("hide");
                }
            },
            error: function (r) {
                console.log(r);
            }
        });
    },
    exeCommand : function (command,term){
        //console.log("command == "+command);
        if($activeNode != 0 && command.trim() != "" ){
            $.ajax({
                type: 'POST',
                url: 'ajax.php',
                async : true,
                data: {'c': 'Terminal', 'a': 'sentToNode','nodeID' : $activeNode,'cmd':command},
                dataType: 'json',
                success: function (res) {
					//console.log(res);
                    if (res.status == 1) {
						$resCMD = "";
						//console.log(res.result);
                        $.each(res.result, function (key, value) {
							//console.log(value);
							$resCMD += String.fromCharCode(value);
						});
						term.echo($resCMD);
					}
                    else term.echo("[[i;#ff3300;]Failed to execution]");
                },
                error: function (r) {
                    if(typeof r == "string") term.echo("[[i;#FFEB3B;]"+r+"]");
                    else term.echo("[[i;#ff3300;]"+r.statusText+"]");
                   //term.echo("[[i;#FFEB3B;<tr-waiting>]"+r+"]");
                }
            });
        }
        else if($activeNode == 0) term.echo("[[i;#ff3300;]Session expired, please select node]");
        else  term.echo("[[i;#ff3300;]Invalid command]");
        
      // term.echo('success.');
       //console.log($(".term-inputbox").find(".tr-waiting").parent());
       //$(".term-inputbox").find(".tr-waiting").parent().remove();
        
    }
}

function asyncPostToServer(url,postData,successCB,errorCB,completeCB,asyncVal,loader)
        {
	if(typeof(loader)!='undefined') __showLoadingAnimation();
	var d = new Date();
	var n = d.getTime();
	try{
            $.ajax({
                url: url,
                type: 'post',
                data: postData,
                dataType: 'json',
                async:  asyncVal,
                success: function(data) {
                    if(typeof(loader)!='undefined') __hideLoadingAnimation();
                    if ( typeof(successCB)!='undefined') successCB(data);
                },
                error:function(){
                    if ( typeof(errorCB)!='undefined')	errorCB();
                    if(typeof(loader)!='undefined') __hideLoadingAnimation();
                },
                complete:function(){
                    if ( typeof(completeCB)!='undefined') completeCB();
                    if(typeof(loader)!='undefined') __hideLoadingAnimation();
                }
            });
	}
	catch(err){
		if ( typeof(errorCB)!='undefined') errorCB();
		if(typeof(loader)!='undefined') __hideLoadingAnimation();
	}
}
//Icontains
jQuery.expr[":"].icontains = jQuery.expr.createPseudo(function (arg) {
    return function (elem) {
        return jQuery(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
    };
});
var $_maintenance = {
    QASearch : function (obj){
        var name = $(obj).val();
        //$(obj).removeClass("inpFilterProject-error");
        if( name != '' ) {
            //console.log( $( "div.qaFilter:icontains('"+name+"')"));
            $( ".qaFilter" ).hide();
            $( "div.qaFilter:icontains('"+name+"')").show();

            $(".qa-filter-empty").addClass("hide"); 
            if( $( "div.qaFilter:icontains('"+name+"')").length == 0  ) $(".qa-filter-empty").removeClass("hide"); 
         }
         else{
             $( ".qaFilter" ).show();
             $(".qa-filter-empty").addClass("hide"); 
         }
    }  
};

var $GraphEvn = {
    blinkCount: 0,
    init: function ($type) {
        var $data = {
            c: 'Settings',
            a: 'getSwitchSort'
        };
        $.ajax({
            type: 'POST',
            url: 'ajax.php',
            data: $data,
            dataType: 'json',
            success: function (res) {
                if (res.status == 1) {
                    var $data = res.result;
                    if ($type == 1) {
                        for (var $i = 0; $i < $data.length; $i++) {
                            var $item = $data[$i];
                            $("#devfilterContainer").append('<button class="btn btn-default filter-btn" id="btn_filter_' + $item["ID"] + '" data-type="' + $item["ID"] + '" data-toggle="tooltip" data-placement="bottom" data-title="' + $item["Name"] + '" data-container="body" >' + $item["DevNo"] + '</button>');
                        }
                        $(".filter-btn").tooltip();
                        $("#devfilterContainer button").live("click", function () {
                            $("#devfilterContainer button").removeClass("active");
                            $(this).addClass("active");
                            var $str = $(this).attr("data-type");
                            $GraphEvn.getDeviceData($str, 1);
                        });
                        $('[data-type="' + $data[0]["ID"] + '"]').click();
                    }
                    else {
                        var $cpData = $data;
                        $("#devfilterContainer button").each(function () {
                            var $btnIDStr = $(this).attr("id");
                            var $btnID = $btnIDStr.split("_")[2];
                            var $status = false;
                            //console.log($btnID);
                            for (var $i = 0; $i < $data.length; $i++) {
                                var $item = $data[$i];
                                //console.log($item);
                                if ($item.ID == $btnID) {
                                    $data[$i]["status"] = 1;
                                    $status = true;
                                }
                            }
                            if (!$status) {
                                if (!$(this).hasClass("active")) {
                                    $(this).remove();
                                }
                                else
                                    location.reload();
                            }

                        });
                        //console.log($data);
                        if ($data.length > 0) {
                            for (var $i = 0; $i < $data.length; $i++) {
                                var $item = $cpData[$i];
                                if (typeof $item.status == "undefined") {
                                    if ($("#devfilterContainer button").length > 0)
                                        $("#devfilterContainer").append('<button class="btn btn-default filter-btn" id="btn_filter_' + $item["ID"] + '" data-type="' + $item["ID"] + '" data-toggle="tooltip" data-placement="bottom" data-title="' + $item["Name"] + '" data-container="body" >' + $item["DevNo"] + '</button>');
                                    else
                                        location.reload();
                                }
                            }
                            $(".filter-btn").tooltip();
                        }
                    }

                }
                else {
                    if ($type == 1)
                        $("#resultContainer").html("<h5 class='text-center bold m-t-20 hint-text'> No switch available.</h5>");
                    else {
                        if ($("#devfilterContainer button").length > 0) {
                            //$("#cnCntent").html("Do you wish to delete this alert?");
                            //$("#modalCnfirmation").modal("show");
                            location.reload();
                        }
                    }
                }
            }
        });
    },
    search: function () {

    },
    getDeviceData: function ($filter, $type) {//$type 1 = get device;2 = update status
        var $data = {
            c: 'Settings',
            a: 'getSwitchGraph',
            filter: $filter
        };
        $.ajax({
            type: 'POST',
            url: 'ajax.php',
            data: $data,
            dataType: 'json',
            beforeSend: function () {
                if ($type == 1) {
                    $("#lblPullScheduleDetails").removeClass("hide");
                    __showLoadingAnimation();
                }
                if ($(".sw-log-time").length > 0) {
                    $(".sw-log-time").html('<div class="loading"><div class="loading-bar"></div><div class="loading-bar"></div><div class="loading-bar"></div><div class="loading-bar"></div></div>');

                }
            },
            success: function (res) {
                if (res.status == 1) {
                    var $resultData = res.result;
                    var $logData = res.logResult;
                    var $scheduleData = res.scheduleResult;
                    var $item = [];
                    var $devID = 0;
                    var $devName = "";
                    var $operPowerT = "";
                    var $time = "";
                    var $patchPanel = "";
                    var $pwStatus = 0;
                    var $devStatus = 0;
                    if ($type == 1) {
                        $("#resultContainer").html("");
                        if ($resultData.length > 0) {
                            $devID = $resultData[0]["DevID"];
                            $devStatus = $resultData[0]["status"];
                            $devName = decodeURIComponent(($resultData[0]["device_name"]).replace(/\+/g, " "));
                            if ($resultData[0]["oper_power_total"] != null)
                                $operPowerT = $resultData[0]["oper_power_total"];
                            //console.log($devStatus);

                        }

                        if ($logData.length > 0) {
                            for (var $i = 0; $logData.length > $i; $i++) {
                                var $port = $logData[$i]["Port"];

                                if ($i == 0) {
                                    if ($logData[$i]["LogDate"] != null) {
                                        var $dt = new Date($logData[$i]["LogDate"]);
                                        $time = ("0" + ($dt.getMonth() + 1)).slice(-2) + "/" + ("0" + $dt.getDate()).slice(-2) + "/" + ("0" + $dt.getFullYear()).slice(-2) + " " + ("0" + $dt.getHours()).slice(-2) + ":" + ("0" + $dt.getMinutes()).slice(-2);
                                    }
                                }

                                if ($port != null) {
                                    var $operState = "default"; // NAN
                                    if ($logData[$i]["OperState"] == 1)
                                        $operState = "ok";   // ON
                                    else if ($logData[$i]["OperState"] == 0)
                                        $operState = "error"; // OFF

                                    $item[$port] = $operState;

                                    var $operPower = $logData[$i]["OperPower"];

                                    if ($operPower != null)
                                        $item[$port + "_cable"] = $operPower + "w";
                                    else
                                        $item[$port + "_cable"] = "";
                                }

                            }
                            $pwStatus = 1
                        }
                        if ($scheduleData.length > 0) {
                            for (var $i = 0; $scheduleData.length > $i; $i++) {
                                $scheduleData[$i]["Name"] = (($scheduleData[$i]["Name"] != null) ? decodeURIComponent(($scheduleData[$i]["Name"]).replace(/\+/g, " ")) : "");
                                $scheduleData[$i]["CableID"] = (($scheduleData[$i]["CableID"] != null) ? decodeURIComponent(($scheduleData[$i]["CableID"]).replace(/\+/g, " ")) : "");
                                $scheduleData[$i]["PatchPanel"] = $patchPanel = (($scheduleData[$i]["PatchPanel"] != null) ? decodeURIComponent(($scheduleData[$i]["PatchPanel"]).replace(/\+/g, " ")) : "");
                                $scheduleData[$i]["Power"] = (($scheduleData[$i]["Power"] != null) ? decodeURIComponent(($scheduleData[$i]["Power"]).replace(/\+/g, " ")) : "");
                                $scheduleData[$i]["EnginePort"] = (($scheduleData[$i]["EnginePort"] != null) ? decodeURIComponent(($scheduleData[$i]["EnginePort"]).replace(/\+/g, " ")) : "");
                                $scheduleData[$i]["CableType"] = (($scheduleData[$i]["CableType"] != null) ? decodeURIComponent(($scheduleData[$i]["CableType"]).replace(/\+/g, " ")) : "");
                                $scheduleData[$i]["Color"] = (($scheduleData[$i]["Color"] != null) ? decodeURIComponent(($scheduleData[$i]["Color"]).replace(/\+/g, " ")) : "");
                                $scheduleData[$i]["NodeSerialNum"] = (($scheduleData[$i]["NodeSerialNum"] != null) ? decodeURIComponent(($scheduleData[$i]["NodeSerialNum"]).replace(/\+/g, " ")) : "");
                                $scheduleData[$i]["Vendor"] = (($scheduleData[$i]["Vendor"] != null) ? decodeURIComponent(($scheduleData[$i]["Vendor"]).replace(/\+/g, " ")) : "");
                                $scheduleData[$i]["Model"] = (($scheduleData[$i]["Model"] != null) ? decodeURIComponent(($scheduleData[$i]["Model"]).replace(/\+/g, " ")) : "");
                                $scheduleData[$i]["Type"] = (($scheduleData[$i]["Type"] != null) ? decodeURIComponent(($scheduleData[$i]["Type"]).replace(/\+/g, " ")) : "");
                                $scheduleData[$i]["HWVer"] = (($scheduleData[$i]["HWVer"] != null) ? decodeURIComponent(($scheduleData[$i]["HWVer"]).replace(/\+/g, " ")) : "");
                                $scheduleData[$i]["SWVer"] = (($scheduleData[$i]["SWVer"] != null) ? decodeURIComponent(($scheduleData[$i]["SWVer"]).replace(/\+/g, " ")) : "");
                                $scheduleData[$i]["PS_Status"] = (($scheduleData[$i]["PS_Status"] != null) ? decodeURIComponent(($scheduleData[$i]["PS_Status"]).replace(/\+/g, " ")) : "");
                                $scheduleData[$i]["TalkerPort"] = (($scheduleData[$i]["TalkerPort"] != null) ? decodeURIComponent(($scheduleData[$i]["TalkerPort"]).replace(/\+/g, " ")) : "");
                                $scheduleData[$i]["NonTalkerPort"] = (($scheduleData[$i]["NonTalkerPort"] != null) ? decodeURIComponent(($scheduleData[$i]["NonTalkerPort"]).replace(/\+/g, " ")) : "");
                                $scheduleData[$i]["LocationName"] = (($scheduleData[$i]["LocationName"] != null) ? decodeURIComponent(($scheduleData[$i]["LocationName"]).replace(/\+/g, " ")) : "");
                                $scheduleData[$i]["LocationPath"] = (($scheduleData[$i]["LocationPath"] != null) ? decodeURIComponent(($scheduleData[$i]["LocationPath"]).replace(/\+/g, " ")) : "");
                                $scheduleData[$i]["TooManyDiscoveries"] = (($scheduleData[$i]["TooManyDiscoveries"] != null) ? decodeURIComponent(($scheduleData[$i]["TooManyDiscoveries"]).replace(/\+/g, " ")) : "");
                                $scheduleData[$i]["PodID"] = (($scheduleData[$i]["PodID"] != null) ? decodeURIComponent(($scheduleData[$i]["PodID"]).replace(/\+/g, " ")) : "");
                                $scheduleData[$i]["PodStatus"] = (($scheduleData[$i]["PodStatus"] != null) ? decodeURIComponent(($scheduleData[$i]["PodStatus"]).replace(/\+/g, " ")) : "");
                                $scheduleData[$i]["device_name"] = $devName;

                                //ProtName
                                var $port = $scheduleData[$i]["Port"];
                                var $cableID = $scheduleData[$i]["CableID"];
                                if ($cableID != null && $cableID != "") {
                                    $cableID = decodeURIComponent($cableID.replace(/\+/g, " "));
                                    var cableIDSplit = $cableID.split("/");
                                    if (typeof $item[$port + "_cable"] != "undefined") {
                                        $item[$port + "_cable"] = cableIDSplit[0] + " / " + ($item[$port + "_cable"] != null ? $item[$port + "_cable"] : "");
                                    }
                                    else
                                        $item[$port + "_cable"] = cableIDSplit[0];

                                }
                            }
                        }
                        var $switchStr = tmpl("tmpl-graph", {devId: $devID, name: $devName, operPower: $operPowerT, time: $time, patchPanel: $patchPanel, pwStatus: $pwStatus, data: $item});
                        //console.log($switchStr);
                        $("#resultContainer").append($switchStr);


                        if ($scheduleData.length > 0) {
                            $("#lblPullScheduleDetails").removeClass("hide");
                            var $schedule = tmpl("tmpl-schedule", {data: $scheduleData});
                            $("#divPullScheduleDetails").html($schedule);
                        }
                        else
                            $("#divPullScheduleDetails").html("");

                        $(".port_tooltip").tooltip({html: true});
                        if ($devStatus == 0)
                            $(".cisco_status").html("<span class=\"notify-bubble bg-green bg-green-ball\"></span>  Online");
                        else
                            $(".cisco_status").html("<span class=\"notify-bubble bg-grey bg-grey-ball\"></span>  Offline");
                    }
                    else {
                        var $operPowerStr = "";
                        if ($resultData.length > 0) {
                            $devID = $resultData[0]["DevID"];
                            $devStatus = $resultData[0]["status"];
                            $devName = decodeURIComponent(($resultData[0]["device_name"]).replace(/\+/g, " "));

                            if ($("#sw_name_" + $devID).length > 0) {
                                //$( "#sw_name_"+$id).attr("data-title",$patchPanel);
                                $("#sw_name_" + $devID + " .sw-name").html($devName);
                                if ($logData.length > 0) {
                                    if ($resultData[0]["oper_power_total"] != null) {
                                        $operPowerT = $resultData[0]["oper_power_total"];
                                        $("#swPowerTotal_" + $devID).removeClass("hide");
                                        $("#swPowerTotal_" + $devID).html($resultData[0]["oper_power_total"] + "w");
                                    }
                                    else
                                        $("#swPowerTotal_" + $devID).addClass("hide");

                                    if ($logData[0]["LogDate"] != null) {
                                        var $dt = new Date($logData[0]["LogDate"]);
                                        $("#swLogTime_" + $devID).html(("0" + ($dt.getMonth() + 1)).slice(-2) + "/" + ("0" + $dt.getDate()).slice(-2) + "/" + ("0" + $dt.getFullYear()).slice(-2) + " " + ("0" + $dt.getHours()).slice(-2) + ":" + ("0" + $dt.getMinutes()).slice(-2));
                                    }
                                    else
                                        $("#swLogTime_" + $devID).html("");
                                    $("#swLogTime_" + $devID).removeClass("hide");
                                }
                                else {
                                    $("#swPowerTotal_" + $devID).addClass("hide");
                                    $("#swPowerTotal_" + $devID).html("");
                                    $("#swLogTime_" + $devID).addClass("hide");
                                    $("#swLogTime_" + $devID).html("");
                                }
                            }
                            if ($devStatus == 0)
                                $(".cisco_status").html("<span class=\"notify-bubble bg-green bg-green-ball\"></span>  Online");
                            else
                                $(".cisco_status").html("<span class=\"notify-bubble bg-grey bg-grey-ball\"></span>  Offline");

                        }
                        if ($scheduleData.length > 0) {
                            for (var $i = 0; $scheduleData.length > $i; $i++) {
                                var $operPwStr = "";
                                $patchPanel = (($scheduleData[$i]["PatchPanel"] != null) ? decodeURIComponent(($scheduleData[$i]["PatchPanel"]).replace(/\+/g, " ")) : "");

                                //ProtName
                                var $port = $scheduleData[$i]["Port"];
                                var $cableID = $scheduleData[$i]["CableID"];
                                $("#sw_name_" + $devID).attr('title', $patchPanel).tooltip('fixTitle');
                                $("#sw_name_" + $devID).attr("data-title", $patchPanel);
                                if ($cableID != null && $cableID != "") {
                                    $cableID = decodeURIComponent($cableID.replace(/\+/g, " "));
                                    var cableIDSplit = $cableID.split("/");
                                    $item[$port] = cableIDSplit[0];
                                }
                            }
                        }
                        if ($logData.length > 0) {
                            for (var $i = 0; $logData.length > $i; $i++) {
                                var $port = $logData[$i]["Port"];
                                if ($port != null) {
                                    var $operState = "default.gif";
                                    if ($logData[$i]["OperState"] == 1) {
                                        $operState = "ok.gif";
                                        $("#cisco_img_" + $devID + "_" + $port).addClass("port-st-ok");
                                        $("#cisco_img_" + $devID + "_" + $port).removeClass("port-st-error");
                                        $("#cisco_img_" + $devID + "_" + $port).removeClass("port-st-default");
                                    }
                                    else if ($logData[$i]["OperState"] == 0) {
                                        $operState = "error.gif";
                                        $("#cisco_img_" + $devID + "_" + $port).addClass("port-st-error");
                                        $("#cisco_img_" + $devID + "_" + $port).removeClass("port-st-ok");
                                        $("#cisco_img_" + $devID + "_" + $port).removeClass("port-st-default");
                                    }
                                    else {
                                        $("#cisco_img_" + $devID + "_" + $port).addClass("port-st-default");
                                        $("#cisco_img_" + $devID + "_" + $port).removeClass("port-st-ok");
                                        $("#cisco_img_" + $devID + "_" + $port).removeClass("port-st-error");
                                    }
                                    if ($("#cisco_img_" + $devID + "_" + $port).length > 0) {
                                        $("#cisco_img_" + $devID + "_" + $port).attr("src", "img/port-" + $operState);

                                        //power
                                        var $operPower = $logData[$i]["OperPower"];

                                        $("#port_" + $devID + "_" + $port).removeAttr("title");
                                        $("#port_" + $devID + "_" + $port).removeAttr("data-original-title");
                                        $("#port_" + $devID + "_" + $port).removeAttr("data-title");
                                        //console.log($item[$port]);
                                        if (typeof $item[$port] != "undefined") {
                                            $("#port_" + $devID + "_" + $port).attr('title', $item[$port] + "/" + $operPower + "w").tooltip('fixTitle');
                                            $("#port_" + $devID + "_" + $port).attr("data-title", $item[$port] + "/" + $operPower + "w");
                                        }
                                        else {
                                            $("#port_" + $devID + "_" + $port).attr('title', $operPower + "w").tooltip('fixTitle');
                                            $("#port_" + $devID + "_" + $port).attr("data-title", $operPower + "w");
                                        }
                                    }
                                }
                            }
                        }
                        else {
                            $(".port-st-ok").attr("src", "img/port-default.gif");
                            $(".port-st-error").attr("src", "img/port-default.gif");
                            $(".port img").removeClass("port-st-ok");
                            $(".port img").removeClass("port-st-error");
                            $(".port img").removeClass("port-st-default");
                            var $pCount = 48;
                            for (var $i = 1; $pCount >= $i; $i++) {
                                //console.log($("#port_"+$devID+"_"+$i).html());
                                if (typeof $item[$i] != "undefined") {
                                    $("#port_" + $devID + "_" + $i).attr('title', $item[$i]).tooltip('fixTitle');
                                    $("#port_" + $devID + "_" + $i).attr("data-title", $item[$i]);
                                }
                                else {
                                    var $portTitle = $("#port_" + $devID + "_" + $i).attr("data-title");
                                    if (typeof $portTitle != "undefined") {
                                        $("#port_" + $devID + "_" + $i).attr('title', "").tooltip('fixTitle');
                                        $("#port_" + $devID + "_" + $i).tooltip("destroy");
                                        $("#port_" + $devID + "_" + $i).attr("data-title", "");
                                    }
                                }
                                /*var $portTitle = $("#port_"+$devID+"_"+$i).attr("data-title");
                                 console.log($portTitle);
                                 if(typeof $portTitle != "undefined"){
                                 var $splitTitle = $portTitle.split("/");
                                 console.log($splitTitle);
                                 $("#port_"+$devID+"_"+$i).attr("data-title",$splitTitle[0]);
                                 $( "#port_"+$devID+"_"+$i ).attr('title',$splitTitle[0]).tooltip('fixTitle');
                                 }*/
                            }
                        }
                        //$(".port_tooltip").data('tooltip', false);
                        //$(".port_tooltip").tooltip({html:true});
                    }
                    $("#resultInfo").show();
                }
                else {
                    console.log("1212");
                    $("#resultContainer").html("<h5 class='text-center bold m-t-20 hint-text'> No switch available.</h5>");
                    $("#divPullScheduleDetails").html("");

                    $("#resultInfo").hide();
                }

                $GraphEvn.blinkOffPorts(true);
            },
            complete: function () {
                if ($type == 1)
                    __hideLoadingAnimation();

            },
            error: function (r) {
                if ($type == 1)
                    __hideLoadingAnimation();
                console.log(r);
            }
        });
    },
    fileUpload: function () {
        // Call the fileupload widget and set some parameters
        $('#fileupload').fileupload({
            url: 'fileUpload.php',
            singleFileUploads: true,
            dataType: 'json',
            acceptFileTypes: /(\.|\/)(xls|xlsx)$/i,
            done: function (e, data) {
                $.each(data.result.files, function (index, file) {
                    var imageName = file.name;

                    if (file.error) {
                        $createAlert({status: "fail", title: "Failed", text: file.error});
                        $('#uploadConfirmation').modal("hide");
                    }
                    else {
                        $('#uploadConfirmation .up-filename').html(imageName);
                        $("#uploadConfirmation .upload-progress").addClass("hide");
                        $("#uploadConfirmation .update-conf").removeClass("hide");
                        $("#hdnUploadFileName").val(imageName);
                    }
                });
            },
            progressall: function (e, data) {
                // Update the progress bar while files are being uploaded
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('#uploadConfirmation .up-pre-span').html(progress);
            }
        }).bind('fileuploadadd', function (e, data) {
            var acceptFileTypes = /(\.|\/)(xls|xlsx)$/i;
            var fileName = data.originalFiles[0]['name'];
            var splitName = fileName.split(".");
            var ext = splitName[splitName.length - 1];
            ext = ext.toLowerCase();
            if (ext != 'xls' && ext != 'xlsx') {
                console.log("test");
                $createAlert({status: "fail", title: "Failed", text: "Not an accepted file type"});
                return false;
            }
            else {
                $('#uploadConfirmation').modal("show");
                $("#uploadConfirmation .update-conf").addClass("hide");
                $("#uploadConfirmation .upload-progress").removeClass("hide");

            }
        });
    },
    saveExcelDetails: function () {
        var $fileName = $("#hdnUploadFileName").val();
        if ($fileName.trim() == "") {
            $createAlert({status: "fail", title: "Failed", text: "Please select switch schedule excel sheet. "});
            return false;
        }
        var $data = {
            c: 'Settings',
            a: 'updateSwitchSchedule',
            fileName: encodeURI($fileName)
        };
        $.ajax({
            type: 'POST',
            url: 'ajax.php',
            data: $data,
            dataType: 'json',
            beforeSend: function () {
                __showLoadingAnimation();
            },
            success: function (res) {
                if (res.Status == 1) {
                    $('#uploadConfirmation').modal("hide");
                    $createAlert({status: "success", title: "Success", text: "successfully updated records"});

                    //Refresh switch
                    var $filter = $("#devfilterContainer .active").attr("data-type");
                    $GraphEvn.getDeviceData($filter, 1);
                }
                else {
                    $('#uploadConfirmation').modal("hide");
                    $createAlert({status: "fail", title: "Failed", text: "Failed to updated records"});
                }
            },
            complete: function () {
                __hideLoadingAnimation();
            },
            error: function (r) {
                __hideLoadingAnimation();
                $createAlert({status: "fail", title: "Failed", text: r});
            }
        });
    },
    hideUploadConf: function () {
        $('#uploadConfirmation').modal("hide");
        var $fileName = $("#hdnUploadFileName").val();
        if ($fileName.trim() == "") {
            return false;
        }
        var $data = {
            c: 'Settings',
            a: 'removeExistFile',
            fileName: encodeURI($fileName)
        };
        $.ajax({
            type: 'POST',
            url: 'ajax.php',
            data: $data,
            dataType: 'json',
            success: function (res) {

            },
            error: function (r) {
                console.log(r);
            }
        });
    },
    editSwitchName: function ($id) {
        var $name = $("#sw_name_" + $id + " .sw-name").html();
        $("#sw_name_" + $id).addClass("hide");
        $("#sw_edit_" + $id).removeClass("hide");
        $("#sw_edit_" + $id).find(".sw-edit-input").val($name);
    },
    saveName: function ($id) {
        var $name = $("#sw_edit_" + $id).find(".sw-edit-input").val();
        if ($name.trim() == "") {
            $createAlert({status: "fail", title: "Failed", text: "Please enter name. "});
            return false;
        }
        var $data = {
            c: 'Settings',
            a: 'saveSwitchName',
            name: $name.trim(),
            deviceID: $id
        };
        $.ajax({
            type: 'POST',
            url: 'ajax.php',
            data: $data,
            dataType: 'json',
            beforeSend: function () {
                __showLoadingAnimation();
            },
            success: function (res) {
                if (res.Status) {
                    $createAlert({status: "success", title: "Success", text: "successfully updated name"});
                    $("#sw_name_" + $id + " .sw-name").html($name)
                    //$("#sw_name_"+$id).html($name + ' <span onclick="$GraphEvn.editSwitchName('+$id+');"><i class="fa fa-pencil hint-text text-master" aria-hidden="true"></i></span>');
                    $GraphEvn.cancelEdit($id);
                }
                else {
                    $createAlert({status: "fail", title: "Failed", text: "Failed to updated name"});
                }
            },
            complete: function () {
                __hideLoadingAnimation();
            },
            error: function (r) {
                __hideLoadingAnimation();
                $createAlert({status: "fail", title: "Failed", text: r});
            }
        });
    },
    cancelEdit: function ($id) {
        $("#sw_edit_" + $id).addClass("hide");
        $("#sw_name_" + $id).removeClass("hide");
    },
    blinkOffPorts: function (isInit) {
        if (isInit == true)
            $GraphEvn.blinkCount = 0;
        $(".port-st-error").each(function () {
            var currentURL = $(this).attr("src");
//console.log(currentURL )
            if (currentURL == "img/port-error.gif") {
                $(this).attr("src", "img/port-error-blink.gif");
            }
            else {
                $(this).attr("src", "img/port-error.gif");
            }
        });

        $GraphEvn.blinkCount++;

        if ($GraphEvn.blinkCount < 10) {
            setTimeout(function () {
                $GraphEvn.blinkOffPorts(false);
            }, 1000);
        }
    }
};

var __ClearTag = {
    removeConfirm : function(){
        $("#btnCnfmYes").addClass("btn-success");
        $("#modalClearCnfirmation").modal("show");
    },
    clear_tag : function(){
        if($__loggedUserRole == $__adRole){
            var $url = 'ajax.php';
            var $postdata = {
                'c'           : 'Settings', 
                'a'          : 'clearTag'
            };
            //Success call back function for ajax
            var $successCB = function (res)
            {
                if(res.status == 1){
                    $createAlert({status: "success", title: "Success", text: "successfully executed clear tag."});
                    $ClusterTree.init(0);
                }
                else{
                    $createAlert({status: "info", title: "Warring", text: "Failed to execute clear tag."});
                }
            }
            //Error call back function for ajax
            var $errorCB = function () {
            };

            //Complete callback function for ajax
            var $completeCB = function () {
            };

            //ajax call
            asyncPostToServer($url, $postdata, $successCB, $errorCB, $completeCB, true,true);
        }
        else {
            $createAlert({status: "info", title: "Warring", text: "Administrator has only permission to clear the tag."});
        }
        $("#modalClearCnfirmation").modal("hide");
    }
};

var __SetPower = {
    removeConfirm : function(){
        $("#btnCnfmYes").addClass("btn-success");
        $("#modalClearpowerCnfirmation").modal("show");
    },
    Set_Power : function(){
        if($__loggedUserRole == $__adRole){
            var $url = 'ajax.php';
            var $postdata = {
                'c'           : 'Settings', 
                'a'          : 'setPower'
            };
            //Success call back function for ajax
            var $successCB = function (res)
            {
				
                if(res.status == 1){
                    $createAlert({status: "success", title: "Success", text: "successfully Set Power to active switche(s)"});
              
                }
                else{
                    $createAlert({status: "info", title: "Warring", text: "Failed to set power to switche(s)"});
                }
            }
            //Error call back function for ajax
            var $errorCB = function () {
            };

            //Complete callback function for ajax
            var $completeCB = function () {
            };

            //ajax call
            asyncPostToServer($url, $postdata, $successCB, $errorCB, $completeCB, true,true);
        }
        else {
            $createAlert({status: "info", title: "Warring", text: "Administrator has only permission to Set Power."});
        }
        $("#modalClearpowerCnfirmation").modal("hide");
    }
};

var _deviceSel = {
    open : function(){
        $("#device-sel-container").modal("show");
        $("#device-sel-area").html("<h5 class='text-center bold'>Loading please wait ...</h5>");
        var $url = 'ajax.php';
        var $postdata = {
            'c'          : 'Device', 
            'a'          : 'getAllinstance'
        };
        //Success call back function for ajax
        var $successCB = function (res)
        {
             if(res.status == 1){
                $("#device-sel-area").html("");
                var $data = res.result;
                var $activeDevID = res.activeDevID;
                for(var $i=0; $i<$data.length; $i++){
                    var $item = $data[$i];
                    var $activeClass = "";
                    if($activeDevID == $item["id"] )$activeClass = "active";
                        
                    var $html = "<div class='col-sm-4 p-l-0 p-r-5 p-b-5' onclick=\"return _deviceSel.selInstance("+$item["id"]+"); \">";
                    $html += "<div class='device-sel-item "+$activeClass+"'>";
                    $html += $item["device_name"];
                    $html += "<div style='font-size: 11px;color: #6b6b6b;'>";
                    if($item["Status"] == 1) $html += "Online"
                    else $html += "Offline"
                    $html += "</div>";
                    $html += "</div>";
                    $html += "</div>";
                    $("#device-sel-area").append($html);
                }
             }   
             else{
                $("#device-sel-area").html("<h5 class='text-center bold '>Instance not available. </h5>");
             }

        }
        //Error call back function for ajax
        var $errorCB = function () {
        };

        //Complete callback function for ajax
        var $completeCB = function () {
        };

        //ajax call
        asyncPostToServer($url, $postdata, $successCB, $errorCB, $completeCB, true,true);
    },
    selInstance : function($selID){
        var $pageName = getPageName();
        var $url = 'ajax.php';
        var $postdata = {
            'c'          : 'Device', 
            'a'          : 'selInstance',
            'selIns'     : $selID    
        };
        //Success call back function for ajax
        var $successCB = function (res)
        {
            if(res.status == 1){
				var time = (new Date).getTime();
                var $url =__getBaseUrl()[0]+$pageName+".php?t="+time;
                window.location.href = $url;
				location.reload(true);
            } 
            else if(res.instancestatus == 2){
                $("#device-sel-container").modal("hide");
            }

        }
        //Error call back function for ajax
        var $errorCB = function () {
        };

        //Complete callback function for ajax
        var $completeCB = function () {
        };

        //ajax call
        asyncPostToServer($url, $postdata, $successCB, $errorCB, $completeCB, true,true);
    }
}


/************** Loader ***************/

 function __showLoadingAnimation(){
    $.blockUI({ 
		message: '<div class="loading">'+
                            '<div class="loading-bar"></div>'+
                            '<div class="loading-bar"></div>'+
                            '<div class="loading-bar"></div>'+
                            '<div class="loading-bar"></div>'+
                        '</div>',
		overlayCSS: { 
			opacity: .5,
			backgroundColor: '#e4e7ea' ,
			basez:	10000	
		},
		css: { 
            border: 'none', 
            width:'250px', 
            height:'80px',
            top:'40%',
            padding:'30px 15px 15px 15px',
            left:'40%',
            backgroundColor: 'rgba(255, 255, 255, 0.33)',
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: 1,
            color: '#fff',
        } 
	}); 
 }
 
 function __hideLoadingAnimation(){
     $.unblockUI();
 }



