<?php //include_once("tpl/header.tpl.php");
include_once("tpl/dashboard-top.tpl.php");

 ?>
 <style>
.slidecontainer {
    width: 100%;
}

.slider {
    -webkit-appearance: none;
    width: 100%;
    height: 15px;
    border-radius: 5px;
    background: #d3d3d3;
    outline: none;
    opacity: 0.7;
    -webkit-transition: .2s;
    transition: opacity .2s;
}

.slider:hover {
    opacity: 1;
}

.slider::-webkit-slider-thumb {
    -webkit-appearance: none;
    appearance: none;
    width: 25px;
    height: 25px;
    border-radius: 50%;
    background: #4CAF50;
    cursor: pointer;
}

.slider::-moz-range-thumb {
    width: 25px;
    height: 25px;
    border-radius: 50%;
    background: #4CAF50;
    cursor: pointer;
}
 </style>
<!-- START PAGE CONTENT -->
<div class="content clearfix p-b-0">


        <div class="panel panel-transparent">
            <div class="panel-heading ">
                <div class="panel-title bold fs-16">
                    Send Commands to Cluster
                </div>
            </div>
            <div class="panel-body">
			<div class="col-md-12 text-center">
			    <ul>
                                        <!-- Drop down starts for device --->
					<li>
						<select  data-init-plugin="select2" id="selDeviceType" style="width: 100px;" onchange="$clientReport.__setDevice();">
							<option value="1">All</option>
							<!--option value="2">Fixture</option-->
							<option value="3">Cluster</option>
						</select>
					</li>
					<!-- Drop down ends --->

					<!-- Drop down starts for device --->
					<li>
						<select data-init-plugin="select2" id="selDev" style="width: 200px;"  disabled>
							<option value="-1">All selected</option>
						</select>
					</li>

                </ul>
				</div>
			    <div class="row">
                    <div class="col-md-12 text-center">
                        <button class="btn btn-complete btn-cons bold btn-cons" onclick="return $clientReport.__send(210);">Light ON </button>
                    </div>
                </div>
				<br>
			    <div class="row">
                    <div class="col-md-12 text-center">
                        <button class="btn btn-complete btn-cons bold btn-cons" onclick="return $clientReport.__send(211,0);">Light OFF</button>
                    </div>
                </div>
				<br>
				<div class="row">

					Select Dim Level

                   <div class="""slidecontainer">
					<input type="range" min="1" max="100" value="50" class="slider" id="myRange" onchange="$clientReport.__send(111,this.value)">
					<p> <span id="demo"></span></p>
					</div>
                    <!--button class="btn btn-complete btn-cons bold btn-cons" onclick="return $clientReport.__send(111);">Light Dim </button-->
                </div>
				<br>

				<div class="row">
                    <div class="col-md-12 text-center">
                        <button class="btn btn-complete btn-cons bold btn-cons" onclick="return $clientReport.__send(1,0);">Scene 1</button>
                    </div>
                </div>
				<br>
				<div class="row">
                    <div class="col-md-12 text-center">
                        <button class="btn btn-complete btn-cons bold btn-cons" onclick="return $clientReport.__send(2,0);">Scene 2</button>
                    </div>
                </div>
				<br>
				<div class="row">
                    <div class="col-md-12 text-center">
                        <button class="btn btn-complete btn-cons bold btn-cons" onclick="return $clientReport.__send(3,0);">Scene 3</button>
                    </div>
                </div>
				<div class="row">

					Select Color Level

                   <div class="""slidecontainer">
					<input type="range" min="3000" max="5000" value="3500" class="slider" id="myRangeAT" onchange="$clientReport.__send(186,this.value)">
					<p> <span id="AT"></span></p>
					</div>
                    <!--button class="btn btn-complete btn-cons bold btn-cons" onclick="return $clientReport.__send(111);">Light Dim </button-->
                </div>
            </div>
        </div>
    <input type="hidden" id="hdnClusterID" name="hdnClusterID" value="">

</div>
<!-- END PAGE CONTENT -->
<?php include_once("tpl/footer.tpl.php"); ?>
<script src="js/jquery.path.js" type="text/javascript"></script>
<script type="text/javascript">
    $mergePolicyStatus =1;
    $(document).ready(function () {
        $ClusterTree.init(0);
    });

var slider = document.getElementById("myRange");
var output = document.getElementById("demo");
output.innerHTML = slider.value;

slider.oninput = function() {
  output.innerHTML = this.value;
}

var sliderAT = document.getElementById("myRangeAT");
var outputAT = document.getElementById("AT");
outputAT.innerHTML = sliderAT.value;

sliderAT.oninput = function() {
  outputAT.innerHTML = this.value;
}

//-------------
	var $clientReport = {
		__setDevice :function (){

			var selVal = $("#selDeviceType").select2("val");

			var $options = '';
			$('#selDev').html('');
			$('#selDev').select2('data', null);
			if( selVal == 1 ){
				$('#selDev').append('<option value="-1">All selected</option>');
				$('#selDev').select2('val', "-1");
				$("#selDev").prop("disabled", true);
			}
			else{
				$options ="";
				$('#selDev').select2('data', null);
				$("#selDev").prop("disabled", false);
				if( selVal == 2) $ClusterTree.getAllTagGroup("#selDev");
				else if( selVal == 3) $ClusterTree.getAllCluster("#selDev");
				$('#selDev').select2('val', "1");
			}


		},
		__send: function ($control,$dim) {
			var $type = $("#selDeviceType").val();
			var $clusterID = $("#selDev").val();

			//var $dim = $("#dimV").val();

                var $data = {
					'a': 'sendControl',
					'c': 'LocationSettings',
					t:$type,
					cluster: $clusterID,
					p1: $dim,
					command:$control
                };

                $.ajax({
                    type: 'POST',
                    url: 'ajax.php',
                    data: $data,
                    dataType: 'json',
                    beforeSend: function () {
                        __showLoadingAnimation();
                    },
                    success: function (res) {
                        var $status = res.status;
                        if ($status == 1) {

                            $createAlert({status: "success", title: "Successfully", text: " Sent Command to Cluster ! "});
							__hideLoadingAnimation();
                        }
                        else {
                            $createAlert({status: "fail", title: "Failed", text: " To Send Command to Cluster! "});
							__hideLoadingAnimation();
                        }
                    },
                    complete: function () {
                        __hideLoadingAnimation();
                    },
                    error: function (r) {
                        __hideLoadingAnimation();
                        //$createAlert({status: "fail", title: "Failed", text: r});
						$createAlert({status: "success", title: "Successfully", text: " Sent Command to Cluster ! "});
                    }
                });

        }
	}
</script>
