<?php
include "config.php";
//include_once("tpl/header.tpl.php");
include_once("tpl/dashboard-top.tpl.php");
?>
   <div class="content p-b-0">
      <!-- START PANEL -->
       <div class="panel panel-transparent m-b-0">
         <div class="panel-heading ">
            <div class="panel-title full-width">
                <h5 class="semi-bold bold m-l-25 m-b-0 m-t-0">
                    Alert Definition
                </h5>
            </div>

         </div>
         <div class="panel-body">
             <div class="col-sm-12">
                    <div class="row">
                        <div id="portlet-advance " class="panel panel-default  panel-savings">
                            <div class="panel-heading p-t-15">
                                <!--<div class="panel-title title-metal pull-left p-t-10">
                                    Alert Definition
                                </div>-->
                                <?php if(  $loggedUserRoleId == ADMIN_ROLE || $loggedUserRoleId == ASSISTANT_ROLE || $loggedUserRoleId == SUPER_ADMIN_ROLE){?>
                                <button type="button" id="__btnAlertTem" onclick="createAlert();" class="btn btn-tag btn-complete btn-tag-rounded pull-left">Create New Alert</button>
                                <button type="button" id="btnSmtpConfig" onclick="openSmtpConfig();" class="btn btn-tag btn-default btn-tag-rounded pull-left m-l-20"><i class="fa fa-cog p-r-5"></i>SMTP Configure</button>
                                <?php }?>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <input type="hidden" id="activeDevice" name="activeDevice" value="<?php echo $activeDevice;?>" />
                                </div>
                                <table id="alertTable" class="table table-striped" cellspacing="0" width="100%">
                                    <thead class="bg-white">
                                        <tr>
                                            <th>Node / Cluster / System</th>
                                            <th class="p-l-20">Parameter</th>
                                            <th class="p-l-20">Condition</th>
                                            <th class="p-l-20">Severity</th>
                                            <th class="p-l-20">Description</th>
                                            <?php if(  $loggedUserRoleId == ADMIN_ROLE || $loggedUserRoleId == ASSISTANT_ROLE || $loggedUserRoleId == SUPER_ADMIN_ROLE){?>
                                            <th id="roleBasedTh"></th>
                                            <?php }?> 
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- END PANEL -->

    <!-- MODAL CONFIRMATION ALERT -->
    <div class="modal fade slide-up disable-scroll" data-backdrop="static" id="modalCnfirmation" tabindex="-1" role="dialog" aria-hidden="false">
      <div class="modal-dialog" style="width: 300px;">
        <div class="modal-content-wrapper">
          <div class="modal-content mdal-custom">
            <div class="modal-body p-b-5">
                <p class="bold fs-20 p-t-20" id="cnCntent">
                    Do you want to bank off ?
                </p>
            </div>
            <div class="modal-footer text-right">
                <button type="button" class="btn btn-sm btn-danger pull-right" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-sm btn-tag-rounded pull-right m-r-10" onclick="actionConfirm();">OK</button>
            </div>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
    </div>
    <!-- END CONFIRMATION ALERT -->

    <!-- MODAL ALERT TEMPLATE-->
    <div class="modal fade slide-up disable-scroll" data-backdrop="static" id="modalAlertTem" tabindex="-1" role="dialog" aria-hidden="false">
      <div class="modal-dialog modal-lg">
        <div class="modal-content-wrapper">
          <div class="modal-content mdal-custom">
            <div class="modal-header clearfix text-left bold fs-16">
                Alert Definition
                <button type="button" class="close" onclick="closeAlert();">&times;</button>
            </div>
                <div class="modal-body p-b-5 b-b b-t b-grey">
                    <div class="hide save-alert" id="alertBox">
                        <p class="alert-msg"></p>
                    </div>
                    <div class="row p-t-5">
                        <div class="col-sm-4 p-r-5">
                            <div class="form-group form-group-default form-group-default-select2" id="divType">
                                <label>
                                    Type
                                </label>
                                <select class=" full-width" data-init-plugin="select2" id="selType" required onchange="return selectValidation('selType','Type','divType')">
                                    <option value="0" selected>System</option>
                                    <option value="1" selected>Node</option>
                                    <option value="2" selected>Cluster</option>
                                </select>
                                <span class="text-danger sp_error hide"></span>
                            </div>
                        </div>
                        <div class="col-sm-3 p-l-5 p-r-5">
                            <div class="form-group form-group-default form-group-default-select2" id="divServerity">
                                <label>Severity</label>
                                <select class=" full-width" data-init-plugin="select2" id="selSeverity" required onchange="selectValidation('selSeverity','severity','divServerity')">
                                    <option value="-1" selected>Select severity</option>
                                    <option value="1"> Normal</option>
                                    <option value="2">Acceptable</option>
                                    <option value="3">Above Normal</option>
                                    <option value="4">Severe</option>
                                    <option value="5">Ambient</option>
                                    <option value="6">Above Ambient</option>
                                    <option value="7">Critical</option>
                                </select>
                                <span class="text-danger sp_error hide"></span>
                            </div>
                        </div>

                        <div class="col-sm-3 p-l-5 p-r-5">
                            <div class="form-group form-group-default form-group-default-select2" id="divParamType">
                                <label>Parameter</label>
                                <select class=" full-width" data-init-plugin="select2" id="selParameterType" required onchange="return selectValidation('selParameterType','parameter type','divParamType');" >
                                    <option value="-1" selected>Select parameter type</option>
                                    <option value="DriverAKW">Driver A KW</option>
                                    <option value="DriverBKW">Driver B KW</option>
                                    <option value="Temperature">Temperature</option>
                                    <option value="Motion">Motion</option>
                                </select>
                                <span class="text-danger sp_error hide"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-sm-4 p-r-5">
                            <div class="form-group form-group-default form-group-default-select2" id="divDevice">

                                <label id="lblDevType">
                                    Node
                                </label>
                                <select class=" full-width" data-init-plugin="select2" id="selDeviceName" required>

                                </select>
                                <span class="text-danger sp_error hide"></span>
                            </div>
                        </div>
                        <div class="col-sm-3 p-l-5 p-r-5">
                            <div class="form-group form-group-default form-group-default-select2" id="divCondition">
                                <label>Condition</label>
                                <select class=" full-width" data-init-plugin="select2" id="selCondition"  required onchange="return changeCondition(this.value);">
                                    <option value="-1">Select condition</option>
                                    <option value=">">Greater-than</option>
                                    <option value="<">Less-than</option>
                                    <option value="<=">less or equal to</option>
                                    <option value=">=">Greater or equal to</option>
                                    <option value="=">Equal to</option>
                                    <option value="!=">Not equal to</option>
                                    <option value="between">between</option>
                                </select>
                                <span class="text-danger sp_error hide"></span>
                            </div>
                        </div>
                        <div class="col-sm-3 p-l-5 p-r-5">
                            <div class="form-group form-group-default m-b-0" id="divParamFromvalue">
                                <label>Value</label>
                                <input type="text" value="" class="form-control" vlaue="0" id="parameterFromVal" name="parameterFromVal" onblur="return checkParamValue(1);" onkeyup="removeErrorAlert('divParamFromvalue');"/>
                                <span class="text-danger hide"></span>
                            </div>
                        </div>
                        <div class="col-sm-2 p-l-5 p-r-15 hide" id="div_toVal">
                            <div class="form-group form-group-default m-b-0 " id="divParamTovalue">
                                <label>TO</label>
                                <input type="text" value="" class="form-control" vlaue="0" id="parameterToVal" name="parameterToVal" onblur="return checkParamValue(2);" onkeyup="removeErrorAlert('divParamTovalue');" />
                                <span class="text-danger hide"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group  form-group-default" id="divDescription">
                        <label>Description</label>
                        <textarea cols="10" rows="20" class="form-control full-width" style="height: 175px;" id="alertDescription" name="alertDescription" onkeyup="removeErrorAlert('divDescription');"></textarea><span class="text-danger hide"></span>
                    </div>

                    <div class="form-group form-group-default form-group-default-select2">
                        <label>Send email Notification to</label>
                        <select class="full-width" data-init-plugin="select2" multiple id="selUsers" required placeholder="select user">

                        </select>
                        <span class="text-danger hide"></span>
                    </div>
                </div>
                <input type="hidden" id="editAlertId" name="editAlertId" value="" />
                <input type="hidden" id="delAlertId" name="delAlertId" value="" />
                <div class="modal-footer text-right">
                    <button type="button" class="btn btn-danger pull-right" onclick="closeAlert();">Close</button>
                    <button type="button" class="btn btn-success pull-right m-r-10 hide" id="btnSaveAlert" onclick="return saveNewAlert();">Save</button>
                    <button type="button" class="btn btn-success pull-right m-r-10 hide" id="btnSaveChange" onclick="return saveAlertChanges();">Save Changes</button>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
      </div>
    </div>
    <!-- END  ALERT TEMPLATE-->

    <!-- START SMTP CONFIGURE TEMPLATE-->
    <div class="modal fade slide-up disable-scroll" data-backdrop="static" id="modalSmtpconfig" tabindex="-1" role="dialog" aria-hidden="false">
        <div class="modal-dialog">
            <div class="modal-content-wrapper">
                <div class="modal-content mdal-custom">
                    <div class="modal-header clearfix text-left bold fs-16">
                        <i class="fa fa-cog p-r-10"></i>SMTP Configuration
                        <button type="button" class="close" onclick="closeConfig();">&times;</button>
                    </div>
                    <div class="modal-body p-b-5 b-b b-t b-grey">
                        <div class="row">
                            <div class="p-l-20 font-montserrat text-uppercase">Server Information</div>
                            <hr class="m-b-0 m-t-5 m-l-15 m-r-15b-dark-grey b-dashed">
                            <div class="col-sm-12">
                                <div class="form-group form-group-default m-b-0 m-t-10" id="divServer">
                                    <label>SMTP Server</label>
                                    <input type="text" class="form-control" vlaue="" id="txtServer" name="txtServer" onkeyup="removeErrorAlert('divServer');"/>
                                    <span class="text-danger hide"></span>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group form-group-default m-b-0 m-t-10" id="divPort">
                                    <label>SMTP Server Port</label>
                                    <input type="text" class="form-control" vlaue="25" id="txtserverPort" name="txtserverPort" onblur="return checkPort();" onkeyup="removeErrorAlert('divPort');"/>
                                    <span class="text-danger hide"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="p-l-20 p-t-10 font-montserrat text-uppercase">Authentication Information</div>
                            <hr class="m-b-0 m-t-5 m-l-15 m-r-15 b-dark-grey b-dashed">
                            <div class="col-sm-12">
                                <div class="form-group form-group-default m-b-0 m-t-10" id="divFromEmail">
                                    <label>From Email Address</label>
                                    <input type="text" class="form-control" vlaue="" id="txtFromEmail" name="txtFromEmail" onkeyup="removeErrorAlert('divFromEmail');" autocomplete="off"/>
                                    <span class="text-danger hide"></span>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group form-group-default m-b-0 m-t-10" id="divPassword">
                                    <label>Password</label>
                                    <input type="password" class="form-control" vlaue="" id="txtPassword" name="txtPassword" onkeyup="removeErrorAlert('divPassword');" autocomplete="off"/>
                                    <span class="text-danger hide"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row p-t-5">
                            <div class="col-sm-6 m-b-10">
                                <div class="checkbox check-primary m-t-10 m-r-15 m-b-0">
                                    <input type="checkbox" id="emailAlert" value="1" >
                                    <label for="emailAlert" style="font-size:14px;">Email Alert Notification</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer text-right">
                        <button type="button" class="btn btn-danger pull-right" onclick="closeConfig();">Close</button>
                        <button type="button" class="btn btn-success pull-right m-r-10" id="btnSaveAlert" onclick="return configureSMTP();">Test Connection</button>
                    </div>
                    </form>
                </div>
            </div>
        <!-- /.modal-content -->
        </div>
    </div>
    <!-- END SMTP CONFIGURE TEMPLATE-->

    <?php include_once("tpl/footer.tpl.php"); ?>

<script type="text/javascript">
    var $G_OFFSET = 0;
     var $G_ROWS_PER_PAGE = 10;
     var $G_LIMIT = 10;
     var loggedUserId = <?php echo $loggedInUserId; ?>;
     var loggedUserRole = <?php echo $loggedUserRoleId; ?>;

     $(document).ready(function(){
        $(".content").css("min-height", ($(window).height())-126);

        //Initialise DataTable
        loadAlertDataTableRec("fetchAllAlertInfo","{}");
        $('[data-toggle="tooltip"]').tooltip();

    });

    function loadAlertDataTableRec(action,data)
    {
        var $requestURL = 'ajax.php?c=Device&a='+action;
		var $aoColumnsDefs = [{
                 "aTargets": [ 5 ],
                 "bVisible": true		//"bVisible": false
                 }];
        if($('#roleBasedTh').is(':visible') !== true){  //if(loggedUserRole == 3){
             $aoColumnsDefs = [
                 {
                 "aTargets": [ 5 ],
                 "bVisible": false
                 }
                 ];
       }

        $("#alertTable").dataTable({
                "bProcessing": true,
                "bServerSide": true,
                "sServerMethod": 'POST',
                "sAjaxSource": $requestURL,
                "bFilter": false,
                "bSort": false,
                //"aaSorting" : [[0, 'desc']],
                //"pageLength": 10, // set the initial value,
                "bLengthChange":false,
                "aLengthMenu": [[10, 25, 50, 100], [ 10, 25, 50, 100 ]],
                "fnServerParams": function(aoData) {
                        /* pass custom variables to server */
                        aoData.push({ "name": "offset", "value": $G_OFFSET });
                       aoData.push({ "name": "limit", "value": $G_LIMIT });
                },

                aoColumns: [
                    { "mDataProp": "DeviceName" ,
                         "mRender": function (data, type, full)
                         {
                             var $str = '';
                             if(full["DeviceName"] != null) $str += decodeURIComponent((full["DeviceName"]).replace(/\+/g, " "));
                             else $str += "System";
                             if(full["severity"]=="1"){//Normal
                                 $str += '<span class="notify-bubble bg-green b-r-2"></span>';
                             }
                             else if(full["severity"]=="2"){//Acceptable
                                 $str += '<span class="notify-bubble bg-blue b-r-2"></span>';
                             }

                             else if(full["severity"]=="3"){//Above Normal
                                 $str += '<span class="notify-bubble bg-dark-blue b-r-2"></span>';
                             }

                             else if(full["severity"]=="4"){//Severe
                                 $str += '<span class="notify-bubble bg-yellow b-r-2"></span>';
                             }
                             else if(full["severity"]=="5"){//Ambiant
                                $str += '<span class="notify-bubble bg-dark-blue b-r-2"></span>';
                             }
                             else if(full["severity"]=="6"){//Above ambiant
                                $str += '<span class="notify-bubble bg-green b-r-2"></span>';
                             }
                             else if(full["severity"]=="7"){//Critical
                                 $str += '<span class="notify-bubble bg-red b-r-2"></span>';
                             }


                             return $str;
                         }
					},
                    { "mDataProp": "parameter" },
                    { "mDataProp": "id"  ,
                         "mRender": function (data, type, full)
                         {
                            var $str = '';
                            var condition = full["parameter_condition"];
                            if(condition == "between"){
                                $str += full["parameter_from_value"] +" - "+ full["parameter_to_value"];
                            }
                            else{
                              $str += full["parameter_condition"] +""+ full["parameter_from_value"];
                            }

                             return $str;
                         }},
                    { "mDataProp": "severity",
                    "mRender": function (data, type, full)
                        {
                            var $str = "";
                             if(data=="1")$str += 'Normal';
                             else if(data=="2")$str += 'Acceptable';
                             else if(data=="3")$str += 'Above Normal';
                             else if(data=="4")$str += 'Severe';
                             else if(data=="5")$str += "Ambient";
                             else if(data=="6")$str += "Above Ambient";
                             else if(data=="7")$str += "Critical";

                             return $str;
                        }
                    },
                    { "mDataProp": "description"},
                    { "mDataProp": "id" ,
                        "mRender": function (data, type, full)
                        {
                            var $str = '';
                            //if(loggedUser == 1 || loggedUser == 2){
                                $str += '<a onclick="deleteAlert('+full["id"]+')" delid="'+full["id"]+'" class="text-danger removeAlert cursor" data-placement="left" data-toggle="tooltip" title="Delete" style="float:right;"><i class="fa fa-trash"></i></a>';
                                $str += '<a onclick="editAlert('+full["id"]+');" class="p-r-10 cursor" data-placement="left" data-toggle="tooltip" title="Edit" style="float:right;"><i class="fa fa-pencil"></i></a>';
                            //}
                            return $str;
                        }
                    }

               ],

               "aoColumnDefs" : $aoColumnsDefs,
               "fnInitComplete": function(oSettings, json) {
                //console.log(111111);
                    //DeleteAlertConfirmation();
                },
                 "fnDrawCallback": function (oSettings) {

                 },
               "sDom": "<'row'<'col-md-6 col-sm-6'l><'col-md-6 col-sm-6'f>r>t<'row'<'col-md-6 col-sm-6'i><'col-md-6  col-sm-6'p>>",
                "language": {
                        "lengthMenu": "Display _MENU_ records per page",
                        "zeroRecords": "No Alerts Created",
                        "info": "Showing page _PAGE_ of _PAGES_"+",total records _TOTAL_  ",
                        "infoFiltered": "filtered from _MAX_ total records"
                }
            });

    }

    /*function getActiveDevice()
    {
        $("#selDeviceName").html("");
         $("#selDeviceName").append('<option value="-1" select>Select device</option>');
        $.ajax ({
            type: 'POST',
            url: 'ajax.php',
            data: { 'c':'Dashboard', 'a': 'getAllDevice' },
            dataType: 'json',
            success: function(response){
               var activeDevice = response.deviceData;
               var deviceHTML = '';
               if(activeDevice.length>0)
               {
                    for(var i=0; i<activeDevice.length; i++)
                    {
                        deviceHTML += '<option value="'+activeDevice[i]['id']+'">'+ decodeURIComponent((activeDevice[i]['device_name']).replace(/\+/g, " ")) +'</option>';
                    }

                    $("#selDeviceName").append(deviceHTML);
                    if(response.activeDeviceID == 0){
                        $("#selDeviceName").select2().select2('val',activeDevice[0]['id']);
                    }
                    else{
                        $("#selDeviceName").select2().select2('val',response.activeDeviceID);
                    }
               }
            },
            error: function(r){
                console.log(r);
            }
        });
    }*/

    function getUser(){
        $("#selUsers").html("");

        $.ajax ({
            type: 'POST',
            url: 'ajax.php',
            data: { 'c':'User', 'a': 'fetchAllUser', 'loggedUser':loggedUserId },
            dataType: 'json',
            success: function(response){
               var user = response.userInfo;
               var userHTML = '';

               if(typeof(user) != 'undefined' && user.length>0)
               {
                   for(var i=0; i<user.length; i++){
                      userHTML += '<option value="'+user[i]['id']+'">'+user[i]['firstname']+' '+user[i]['lastname']+'</option>';
                   }
               }
               $("#selUsers").append(userHTML);
            },
            error: function(r){
                console.log(r);
            }
        });
    }

    function deleteAlert(id){
        $("#btnCnfmYes").addClass("btn-success");
        $("#cnCntent").html("Do you wish to delete this alert?");
        $("#modalCnfirmation").modal("show");
        $("#delAlertId").val(id);
    }

    function actionConfirm()
    {
        var table = $('#alertTable').DataTable();
        var deleteId = $("#delAlertId").val();
        //alert(deleteId);
        $.ajax ({
            type: 'POST',
            url:'ajax.php',
            data: { 'c':'Device', 'a':'deleteAlertInfo', 'delId':deleteId },
            dataType: 'json',
            beforeSend: function() {__showLoadingAnimation();},
            success: function(response){

                if(response.component != ''){
                    $("#modalCnfirmation").modal("hide");
                    $("#delAlertId").val("");
                    $createAlert({status :"info",title : "Waiting",text : 'Successfully Deleted Alert Request Queued'});
                    //table.destroy();
                    //loadAlertDataTableRec("fetchAllAlertInfo","{}");
                }
                else if(response.status == 0){
                    $createAlert({status :"fail",title : "Waiting",text : response.message});
                    $("#modalCnfirmation").modal("hide");
                    $("#delAlertId").val("");
                }
            },
            complete:function (){__hideLoadingAnimation();},
            error:function(r){__hideLoadingAnimation();}
        })
    }

    function createAlert(){
        //var selectedDevice = $("#activeDevice").val();
        $("#modalAlertTem").modal("show");
        $("#btnSaveChange").addClass("hide");
        $("#btnSaveAlert").removeClass("hide");
        clearData();
    }

    function clearData(){
        //var selectedDevice = $("#activeDevice").val();
        $("#selDeviceName").select2().select2('val','-1');
        $("#selClusterName").select2().select2('val','-1');
        $("#selParameterType").select2().select2('val','-1');
        $("#selCondition").select2().select2('val','-1');
        $("#parameterFromVal").val("");
        $("#parameterToVal").val("");
        $("#selSeverity").select2().select2('val','-1');
        $("#alertDescription").val("");
        $("#selUsers").select2().select2('val','');
        $ClusterTree.getAllCluster("#selClusterName");
        $ClusterTree.getAllNode("#selDeviceName");
        //getActiveDevice();
        getUser();
        $("#modalAlertTem .form-group").removeClass("has-error");
        $("#modalAlertTem .text-danger").addClass("hide");
        $("#div_toVal").addClass("hide");
        $("#alertBox p").html("");
        $("#alertBox").addClass("hide");
    }

    function editAlert(id){
        $("#modalAlertTem").modal("show");
        $("#btnSaveAlert").addClass("hide");
        $("#btnSaveChange").removeClass("hide");
        $("#modalAlertTem .form-group").removeClass("has-error");
        $("#modalAlertTem .text-danger").addClass("hide");
        $("#alertBox").addClass("hide");
        $("#editAlertId").val(id);
        $ClusterTree.getAllNode("#selDeviceName");
        $ClusterTree.getAllCluster("#selClusterName");
        //getActiveDevice();
        getUser();

        $.ajax ({
            type:'POST',
                url:'ajax.php',
                data: { 'c':'Device', 'a':'fetchAlertData', 'alertId':id },
                dataType:'json',
                success: function(response){
                    if(response.status == 1){
                        if(response.toValue !=0 && response.condtn == "between"){
                            $("#div_toVal").removeClass("hide");
                            $("#parameterToVal").val(response.toValue);
                        }
                        console.log(response);
                        if(response.type == 1) {
                            alertChangeType(1);
                            $("#selClusterName").select2().select2('val',response.device);
                        }
                        else if(response.type == 2){
                            alertChangeType(2);
                           setTimeout(function(){ $("#selDeviceName").select2().select2('val',response.device); }, 100);
                        }
                        $("#selParameterType").select2().select2('val',response.parameter);
                        $("#selCondition").select2().select2('val',response.condtn);
                        $("#parameterFromVal").val(response.fromValue);
                        $("#divParamTovalue").val(response.toValue);
                        $("#selSeverity").select2().select2('val',response.severity);
                        $("#alertDescription").val(response.description);
                        $("#selUsers").select2().select2('val',response.sendmail);
                    }
                    else{
                        //alert(response.status);
                    }
                },
                error: function(r){
                    console.log(r);
                }
        });

    }

    function saveNewAlert()
    {
        var deviceId        = "";
        var parameter       = $("#selParameterType").val();
        var condition       = $("#selCondition").val();
        var paramFromValue  = $("#parameterFromVal").val();
        var paramToValue    = $("#parameterToVal").val();
        var severity        = $("#selSeverity").val();
        var description     = $("#alertDescription").val();
        var userNotified    = $("#selUsers").select2().val();
        var table           = $('#alertTable').DataTable();
        var type            = 0;

        var selType = $("#selType").select2().val();
         if( selType == 2 ){
             deviceId       = $("#selClusterName").select2().val(); // $("#selClusterName").val();
             type           = 1;
         }
         else if( selType == 1 ){
             deviceId       = $("#selDeviceName").select2().val(); // $("#selDeviceName").val();
             type           = 2;
         }




        if(deviceId == "-1"){
            $("#divDevice").addClass("has-error");
            $("#divDevice .sp_error").html("Please Select a device");
            $("#divDevice .sp_error").removeClass("hide");
            $("#selDeviceName").focus();
            return false;
        }
        if(severity == "-1"){
            $("#divServerity").addClass("has-error");
            $("#divServerity .sp_error").html("Please Select a severity");
            $("#divServerity .sp_error").removeClass("hide");
            $("#selSeverity").focus();
            return false;
        }
        if(parameter == "-1"){
            $("#divParamType").addClass("has-error");
            $("#divParamType .sp_error").html("Please Select a Parameter");
            $("#divParamType .sp_error").removeClass("hide");
            $("#selParameterType").focus();
            return false;
        }
        if(condition == "-1"){
            $("#divCondition").addClass("has-error");
            $("#divCondition .sp_error").html("Please Select a Condition");
            $("#divCondition .sp_error").removeClass("hide");
            $("#selCondition").focus();
            return false;
        }
        if(paramFromValue == ""){
            $("#divParamFromvalue").addClass("has-error");
            $("#divParamFromvalue span").html("Please Enter Parameter Value.");
            $("#divParamFromvalue span").removeClass("hide");
            setTimeout(function() {
                $("#parameterFromVal").focus();
            }, 200);
            return false;
        }
        if(condition == "between" && paramToValue == ""){
            $("#divParamTovalue").addClass("has-error");
            $("#divParamTovalue span").html("Please Enter Parameter Value.");
            $("#divParamTovalue span").removeClass("hide");
            setTimeout(function() {
                $("#parameterToVal").focus();
            }, 200);
            return false;
        }
        else  paramToValue == "0";

        if(paramToValue != "" && paramToValue <= paramFromValue){
            $("#divParamTovalue").addClass("has-error");
            $("#divParamTovalue span").html("value must be greater than from value.");
            $("#divParamTovalue span").removeClass("hide");
            setTimeout(function() {
                $("#parameterToVal").focus();
            }, 200);
            return false;
        }
        if(description == ""){
            $("#divDescription").addClass("has-error");
            $("#divDescription span").html("Please enter a Description.");
            $("#divDescription span").removeClass("hide");
            setTimeout(function() {
                $("#alertDescription").focus();
            }, 200);
            return false;
        }

        if(userNotified == null){
            var notify = 0;
        }
        else{
           notify =  userNotified;
        }
//alert(deviceId);
        $.ajax ({
            type: 'POST',
            url: 'ajax.php',
            data: { 'c': 'Device', 'a':'addDeviceAlert', 'type':type,'device':deviceId, 'parameter':parameter, 'condition':condition, 'fromValue':paramFromValue,'toValue':paramToValue, 'severity':severity, 'description':description, 'userNotified':notify },
            dataType: 'json',
            beforeSend: function() {__showLoadingAnimation();},
            success: function(response){
                if( response.component != '' ){
                    $createAlert({status :"info",title : "Successfully Queued",text : 'Successfully added Device Alert Request Queued'});
                   //$("#modalAlertTem").modal("hide");
                   clearData();
                  // table.destroy();
                  // loadAlertDataTableRec("fetchAllAlertInfo","{}");
                }
                else if( response.status == 0 ){
                    $createAlert({status :"fail",title : "Failed",text : "Failed to add Device Alert Request Queued" });
                }
                //alert(response.status);
            },
            complete:function (){__hideLoadingAnimation();},
            error: function(r){
                __hideLoadingAnimation();
                console.log(r);
            }
        });
    }

    function saveAlertChanges()
    {
        var id              = $("#editAlertId").val();
        var deviceId        = "";
        var parameter       = $("#selParameterType").val();
        var condition       = $("#selCondition").val();
        var paramFromValue  = $("#parameterFromVal").val();
        var paramToValue    = $("#parameterToVal").val();
        var severity        = $("#selSeverity").val();
        var description     = $("#alertDescription").val();
        var userNotified    = $("#selUsers").select2().val();
        var alertTable      = $('#alertTable').dataTable();
        var table           = $('#alertTable').DataTable();
        var type            = 0;

        var selType = $("#selType").select2().val();
        /* if($('#chCluster').is(':checked')){
             deviceId       = $("#selClusterName").val();
             type           = 1;
         }
         else if($('#chNode').is(':checked')){
             deviceId       = $("#selDeviceName").val();
             type           = 2;
         }*/

         if( selType == 2 ){
             deviceId       = $("#selClusterName").select2().val(); // $("#selClusterName").val();
             type           = 1;
         }
         else if( selType == 1 ){
             deviceId       = $("#selDeviceName").select2().val(); // $("#selDeviceName").val();
             type           = 2;
         }



        if(deviceId == "-1"){
            $("#divDevice").addClass("has-error");
            $("#divDevice .sp_error").html("Please Select a device");
            $("#divDevice .sp_error").removeClass("hide");
            $("#selDeviceName").focus();
            return false;
        }
        if(severity == "-1"){
            $("#divServerity").addClass("has-error");
            $("#divServerity .sp_error").html("Please Select a severity");
            $("#divServerity .sp_error").removeClass("hide");
            $("#selSeverity").focus();
            return false;
        }
        if(parameter == "-1"){
            $("#divParamType").addClass("has-error");
            $("#divParamType .sp_error").html("Please Select a Parameter");
            $("#divParamType .sp_error").removeClass("hide");
            $("#selParameterType").focus();
            return false;
        }
        if(condition == "-1"){
            $("#divCondition").addClass("has-error");
            $("#divCondition .sp_error").html("Please Select a Condition");
            $("#divCondition .sp_error").removeClass("hide");
            $("#selCondition").focus();
            return false;
        }
        if(paramFromValue == ""){
            $("#divParamFromvalue").addClass("has-error");
            $("#divParamFromvalue span").html("Please Enter Parameter Value.");
            $("#divParamFromvalue span").removeClass("hide");
            setTimeout(function() {
                $("#parameterFromVal").focus();
            }, 200);
            return false;
        }
        if(condition == "between" && paramToValue == ""){
            $("#divParamTovalue").addClass("has-error");
            $("#divParamTovalue span").html("Please Enter Parameter Value.");
            $("#divParamTovalue span").removeClass("hide");
            setTimeout(function() {
                $("#parameterToVal").focus();
            }, 200);
            return false;
        }
        else paramToValue == "0";

        if(paramToValue != "" && parseFloat(paramToValue) <= parseFloat(paramFromValue)){
            $("#divParamTovalue").addClass("has-error");
            $("#divParamTovalue span").html("value must be greater than from value.");
            $("#divParamTovalue span").removeClass("hide");
            setTimeout(function() {
                $("#parameterToVal").focus();
            }, 200);
            return false;
        }
        if(description == ""){
            $("#divDescription").addClass("has-error");
            $("#divDescription span").html("Please enter a Description.");
            $("#divDescription span").removeClass("hide");
            setTimeout(function() {
                $("#alertDescription").focus();
            }, 200);
            return false;
        }
        if(userNotified == null){
            var notify = 0;
        }
        else{
           notify =  userNotified;
        }

        $.ajax ({
            type: 'POST',
            url: 'ajax.php',
            data: { 'c': 'Device','a':'updateAlert', 'type':type , 'device':deviceId, 'parameter':parameter, 'condition':condition, 'fromValue':paramFromValue,'toValue':paramToValue, 'severity':severity, 'description':description, 'userNotified':notify, 'alertId':id },
            dataType: 'json',
            beforeSend: function() {__showLoadingAnimation();},
            success: function(response){
                if( response.component != '' ){
                    $createAlert({status :"info",title : "Successfully Queued",text : 'Successfully updated device alert request queued'});
                    clearData();
                   /* $("#modalAlertTem").modal("hide");
                    $("#editAlertId").val("");
                    table.destroy();
                    loadAlertDataTableRec("fetchAllAlertInfo","{}");*/
                  }
                else if( response.status == 0 ){
                    $createAlert({status :"fail",title : "Failed Queue",text : 'Failed to update device alert request queued'});
                }
                //alert(response.status);
            },
            complete:function (){__hideLoadingAnimation();},
            error: function(r){
                __hideLoadingAnimation();
                console.log(r);
            }
        });
    }

    function closeAlert()
    {
        //var selectedDevice = $("#activeDevice").val();
        $("#modalAlertTem").modal("hide");
        $("#selDeviceName").select2().select2('val','-1');
        $("#selClusterName").select2().select2('val','-1');
        $("#selParameterType").select2().select2('val','-1');
        $("#selCondition").select2().select2('val','>');
        $("#parameterVal").val("");
        $("#selSeverity").select2().select2('val','Critical');
        $("#alertDescription").val("");
        $("#selUsers").select2().select2('val','');
        $("#alertBox p").html("");
        $("#alertBox").addClass("hide");
        //Parameter To Value
        $("#parameterToVal").val("");
        $("#div_toVal").addClass("hide");
        $("#divParamTovalue").addClass("has-error");
        $("#divParamTovalue span").removeClass("hide");
    }

    function checkParamValue($type){
        var paramValue  = $type == 1 ? $("#parameterFromVal").val() : $("#parameterToVal").val();

        if(paramValue!=""){
            if(!isNumber(paramValue) && !CheckDecimal(paramValue)){
                if($type == 1){
                    $("#divParamFromvalue").addClass("has-error");
                    $("#divParamFromvalue span").html("Enter numbers or decimals");
                    $("#divParamFromvalue span").removeClass("hide");
                    setTimeout(function() {  $("#parameterFromVal").focus(); return false;}, 200);
                }
                else{
                    $("#divParamTovalue").addClass("has-error");
                    $("#divParamTovalue span").html("Enter numbers or decimals");
                    $("#divParamTovalue span").removeClass("hide");
                    setTimeout(function() {  $("#parameterToVal").focus(); return false;}, 200);
                }
            }
            else{
                return true;
            }
        }
    }

    function removeErrorAlert(id){
        $("#"+id).removeClass("has-error");
        $("#"+id+" span").html("");
        $("#"+id+" span").addClass("hide");
    }

    function changeCondition($val){
       if($val == "between"){
            $("#parameterToVal").val("");
            $("#div_toVal").removeClass("hide");
            $("#divParamTovalue").removeClass("has-error");
            $("#divParamTovalue span").addClass("hide");
       }
       else{
            $("#parameterToVal").val("");
            $("#div_toVal").addClass("hide");
            $("#divParamTovalue").addClass("has-error");
            $("#divParamTovalue span").removeClass("hide");
       }
       selectValidation('selCondition','condition','divCondition')
    }

    function openSmtpConfig(){
        $("#modalSmtpconfig").modal("show");
        $("#modalSmtpconfig .text-danger").addClass("hide");
        ///__showLoadingAnimation();
        $.ajax({
            type: 'POST',
            url: 'ajax.php',
            data: { 'c': 'SMTP', 'a':'getSmtpConfigInfo' },
            dataType: 'json',
            success: function(response){
                if(response.status == 1){
                    var info = response.smtpInfo;
                    $("#txtServer").val(info[0]['smtp_server']);
                    $("#txtserverPort").val(info[0]['smtp_port']);
                    $("#txtFromEmail").val(info[0]['from_email']);
                    $("#txtPassword").val(info[0]['password']);
                    if(info[0]['alert_notify'] == 1){
                        $("#emailAlert").prop("checked",true);
                    }

                }
                else{
                    $("#txtServer").val("");
                    $("#txtserverPort").val(25);
                    $("#txtFromEmail").val("");
                    $("#txtPassword").val("");
                }
            },
            error: function(r){
                console.log(r);
            }
        });

    }

    function closeConfig()
    {
        $("#modalSmtpconfig").modal("hide");
    }

    function configureSMTP(){
        var alertnotify = 0;

        var server      = $("#txtServer").val();
        var port        = $("#txtserverPort").val();
        var fromemail   = $("#txtFromEmail").val();
        var passwrd     = $("#txtPassword").val();

        var alertnotify = 0;
        $('input:checkbox:checked').map(function(){
           alertnotify = $(this).val();
        });
        //return false;

        if(server == ""){
            $("#divServer").addClass("has-error");
            $("#divServer span").html("Please Enter SMTP Server Name");
            $("#divServer span").removeClass("hide");
            $("#txtServer").focus();
            return false;
        }
        if(port == ""){
            $("#divPort").addClass("has-error");
            $("#divPort span").html("Please Enter Server Port");
            $("#divPort span").removeClass("hide");
            $("#txtserverPort").focus();
            return false;
        }
        else{
            if(!checkPort())  return false;
        }
        if(fromemail == ""){
            $("#divFromEmail").addClass("has-error");
            $("#divFromEmail span").removeClass("hide");
            $("#divFromEmail span").html("Please Enter From Email Address");
            $('#txtFromEmail').focus();
            return false;
        }
        else{
            if(!ValidateEmail(fromemail))
            {
                $("#divFromEmail").addClass("has-error");
                $("#divFromEmail span").removeClass("hide");
                $("#divFromEmail span").html("Invalid From Email Address");
                $('#txtFromEmail').focus();
                return false;
            }
        }
        if(passwrd == ""){
            $("#divPassword").addClass("has-error");
            $("#divPassword span").removeClass("hide");
            $("#divPassword span").html("Please Enter Password");
            $('#txtPassword').focus();
            return false;
        }

        $.ajax({
            type: 'POST',
            url: 'ajax.php',
            data: { 'c': 'SMTP', 'a':'saveSmtpConfig', 'server':server, 'port':port, 'fromemail':fromemail, 'password':passwrd, 'alertnotify':alertnotify },
            dataType: 'json',
            beforeSend: function() {__showLoadingAnimation();},
            success: function(response){
                if(response.component != ''){
                    $("#modalSmtpconfig").modal("hide");
                    $createAlert({status :"success",title : "Success Queued",text : 'Successfully Connection To SMTP Server Request Queued'});
                }
                else{
                    $createAlert({status :"fail",title : "Failed Queue",text : 'SMTP Server Connection Request Failed'});
                }
            },
            complete:function (){__hideLoadingAnimation();},
            error: function(r){__hideLoadingAnimation();}
        })

    }

    function checkPort(){
        var value = $.trim($("#txtserverPort").val());
        if(value!=""){
            if(!isNumber(value)){
                $("#divPort").addClass("has-error");
                $("#divPort span").html("Server Port must be in numbers");
                $("#divPort span").removeClass("hide");
                setTimeout(function() {
                    $("#txtserverPort").focus();
                    return false;
                }, 200);
            }
            else{
                return true;
            }
        }
    }

    function removeErrorAlert(divID){
        $("#"+divID).removeClass("has-error");
        $("#"+divID+" span").addClass("hide");
        $("#"+divID+" span").html("");
    }

    /*var enc = escape(encodeURIComponent(description));
        var dec =decodeURIComponent(unescape(enc));
       console.log(enc);
       console.log(dec);*/

    function alertChangeType($type){
        if($type == 1){
            $("#chCluster").prop("checked","checked");
            $("#chNode").removeAttr("checked");

            $("#selClusterName").select2("enable",true);
            $("#selDeviceName").select2("enable",false);
        }
        else if($type == 2){
            $("#chNode").prop("checked","checked");
            $("#chCluster").removeAttr("checked");

            $("#selClusterName").select2("enable",false);
            $("#selDeviceName").select2("enable",true);
        }
        $("#selClusterName").select2("val",-1);
        $("#selDeviceName").select2("val",-1);

    }
</script>
