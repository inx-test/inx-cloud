<?php
require_once '_classes/DBC.php';
require_once 'phpMailer/PHPMailerAutoload.php';

class NotificationWorker  extends Thread {

    public $tid = 0;
    private $alertData;
    private $alert;
    private $SMTPInfo;
    private $emailTemplate="";
   
    public function __construct($tid,$alertData,$SMTPInfo) {
        $this->tid = $tid;
        $this->alertData = $alertData;
        $this->SMTPInfo = $SMTPInfo;
        $this->emailTemplate = file_get_contents('tpl/emailTemplate.html');
    }

    public function setAlert($alert)
    {
        $this->alert = $alert;
        //print_r($this->dev);
    }
    
    public function run() {
        
        $db = new \DBC();
        echo "after conn\n";
        $db->autocommit(false);
        try 
        {
            //print_r($this->SMTPInfo);
            //return;
            echo "THREAD = ".$this->tid."\n";
            while(true)
            {
                $this->alertData->getNextAlert($this);    //synchronized fn
                if ( $this->alert == null ) break;
                echo $this->alert['AlertDefID']."\n";
                $userIDList = explode(",", $this->alert['user_notified']);
                
                print_r($userIDList);
                
                $toEmailUsers = array();
                $emailSent=0;
                $x=0;
                for($i = 0; $i < count($userIDList);$i++)
                {
                    if($userIDList[$i] != NULL && $userIDList[$i] != "" && $userIDList[$i] != 0){
                        
			//$userIDArr = explode(",", $userIDList[$i]);			
			echo "user Details \n";
                        //print_r($userIDArr);
                        $user = $db->get_array("select firstname,lastname,email from pmi_user where id=".$userIDList[$i]);
                        //echo "select firstname,lastname,email from pmi_user where id=".$userIDList[$i];
                        //print_r($user);
						
                        //select TIMEDIFF(Now(),CreatedOn) from pmi_alert_lookup where AlertDefId=2 and UserID=1 and DeviceID=1 and MINUTE(TIMEDIFF(Now(),CreatedOn)) < 5
                        $sql = "SELECT TIMEDIFF(Now(),CreatedOn) FROM pmi_alert_lookup WHERE UserID = ". $userIDList[$i] ." AND DeviceID = ". $this->alert['DeviceID'].""
                        . " AND AlertDefID = ". $this->alert['AlertDefID']." AND MINUTE(TIMEDIFF(Now(),CreatedOn)) < ".ALERT_DELAY_TIME;
                        echo $sql."\n";
                        $lookUpDetail = $db->get_result($sql);
                         
                        // if query returns records, it means you are in wait time for this user/alert/device combo.
                        if(count($lookUpDetail)==0)	{	// send email alert to this user    
				echo "lookup process \n";                       
                            // also remove the lookup records for this user/device/alerttype combo.
                            $db->update("delete from pmi_alert_lookup where UserID = ". $userIDList[$i] ." AND DeviceID = ". $this->alert['DeviceID']." AND AlertDefID = ". $this->alert['AlertDefID']);
                            $toEmailUsers[$x++] = $user;
                            
                            // insert new record for this email sending
                            $db->query("insert into pmi_alert_lookup(UserID,DeviceID,AlertDefID,CreatedOn) values(".$userIDList[$i].",".$this->alert['DeviceID'].",". $this->alert['AlertDefID'].",NOW())");
                        }
                    }
                }
                
                // send alert email out
                if ( $x > 0 )	{
			echo "mail notification process strat \n";
                    //$messgHtml = str_replace("[[EMAIL_TITLE]]","Alert",$this->emailTemplate;
                    //description
                    $color = $this->alert["severity"] == "Critical" ? "#FE5555" : "#FFB200";
                    $msgSubject = $this->alert["severity"]." alert"; //$this->alert["parameter"]." = ".$this->alert["Value"]." on ".$this->alert["device_name"];

                    $msgText = "<span style=\"color:".$color.";\">".$this->alert["severity"]." alert</span>";
                    
                    $parameterVal = explode(".", $this->alert["Value"]);
                    
                    $msgHtmlContent = "<b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$this->alert["description"]."</b><br><br>";

                    $msgHtmlContent .= "<b>Device</b>: ".$this->alert["Name"]."<br>";
                    //$msgHtmlContent .= "<b>Location</b>: ".$this->alert["location"]."<br>";
                    $msgHtmlContent .= "<b>".$this->alert["parameter"]."</b>: ".$parameterVal[0].".".substr($parameterVal[1], 0, 2)."<br>";
                    $msgHtmlContent .= "<b>When</b>: ".$this->alert["CratedOn"]."<br>";
                    $msgHtmlContent .= "<b>Severity</b>: ".$this->alert["severity"];


                    $messgHtml = str_replace("[[EMAIL_TITLE]]",$msgText,$this->emailTemplate);
                    $messgHtml = str_replace("[[EMAIL_CONTENT]]",$msgHtmlContent,$messgHtml);
                    //echo $messgHtml;

                    $emailSent=$this->sendEmail($toEmailUsers,$msgSubject,$messgHtml,$msgText);
                }
                else
                {
                        echo "No emails to send\n";
                }
				
                // Now update alert data to set email status
                if ( $emailSent > 0 ) $db->update("Update pmi_device_alerts set EmailProcess=$emailSent, Status=1 where ID=".$this->alert["ID"]);
                $db->commit();
            }
            echo "end of notification Thread ".$this->tid."\n";
        }
        catch (\Exception $ex)
        {
            $db->rollback();
            echo "exception is:" . $ex->getMessage() . "\n";
            //die($ex);
        } 
        $db->close();
    }
	
	public function sendEmail($toUsers,$subject, $messageHtml,$messageTxt)
	{
		print_r($toUsers);
		
		$mail = new PHPMailer;
		$mail->SMTPDebug = 0;	// Enable verbose debug output

		$mail->isSMTP();                                      // Set mailer to use SMTP
		$mail->Host = $this->SMTPInfo['smtp_server']; // 'smtp1.example.com;smtp2.example.com';  // Specify main and backup SMTP servers
		$mail->SMTPAuth = true;                              // Enable SMTP authentication
		$mail->Username = $this->SMTPInfo['from_email'];                 // SMTP username
		$mail->Password = $this->SMTPInfo['password'];                           // SMTP password
		$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
		$mail->Port = 587;                                    // TCP port to connect to

		$mail->setFrom($this->SMTPInfo['from_email'], 'PMI Supervisor');
		
		foreach($toUsers as $toUser)
		{
			$mail->addAddress($toUser['email'], $toUser['name']);     // Add a recipient
		}
	
		$mail->isHTML(true);    // Set email format to HTML

		$mail->Subject = $subject;
		$mail->Body    = $messageHtml;
		$mail->AltBody = $messageTxt;

		if(!$mail->send()) {
			echo 'Message could not be sent.';
			echo 'Mailer Error: ' . $mail->ErrorInfo;
			return 0;
		} else {
			echo 'Message has been sent';
		}
		return 1;
	}
}
?>
