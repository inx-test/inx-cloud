<?php
require_once '_classes/DBC.php';

class AlertDataFetch extends \Thread    
{
    private $index = 0;
    private $alerts = array();
    public $smtpInfo = array();
   
    public function __construct() {
    }
    
    public function loadAlerts(){
        $db = new \DBC();
        $sql =<<<EOF
                SELECT A.ID,A.AlertDefID,
                    A.DeviceID,
                    A.Value,
                    T.Name,
                    AD.parameter,
                    AD.parameter_condition,
                    AD.severity,
                    AD.description,
                    AD.user_notified,
                    A.CratedOn
                FROM 
                    pmi_device_alerts A 
                LEFT JOIN 
                    pmi_alert_definition AD ON(AD.id = A.AlertDefID) 
                LEFT JOIN 
                    pmi_tag T ON (T.ID = A.DeviceID)
                WHERE 
                    A.AlertType=1
EOF;
		$this->alerts = $db->get_result($sql);
       print_r($this->alerts);
        
        $sql = "SELECT * FROM pmi_smtp_config WHERE status = 1";
        $this->smtpInfo = $db->get_result($sql);
       // print_r($this->smtpInfo);
        $db->close();
        return count($this->alerts);
	$this->index=0;
    }
    
    public function getNextAlert(&$ct){
        
        $this->synchronized(
            function($thread,&$ct)
            {
                echo "[".$ct->tid."] in sync function ".$thread->index."\n";
		if($this->index == null)$this->index = 0;

                if($thread->index < count($thread->alerts)) {
                    
                    //echo "index1 ===".$thread->index." \n";
                    $ct->setAlert($thread->alerts[$thread->index]);
                    $thread->index++;
                    //echo "index2 ===".$thread->index." \n";

                }
                else {
                    $ct->setAlert(null);
                    echo "[".$ct->tid."] Nothing more to process\n";
                }
            }, $this,$ct
        );
    }
    
    public function run()       
    {
    }
    
    public function mailProcess(){
        echo 'hai i am here';
    }
}
?>
