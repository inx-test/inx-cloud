<?php

namespace InboundProcess;
use Pheanstalk\Pheanstalk;
use Pheanstalk\PheanstalkInterface;

abstract class InboundProcess {

    protected $dao;
    protected $queue;
    protected $smarty;
    protected $mailer = null;
    protected $bgprocesslog; // Log instance for background process
    protected $encMessageTopicID = 0;
    protected $fixtureTypeArr = array();

    public function __construct($dao,$queue) {
      //  echo "\n\n Here...\n\n\n";
        $this->dao = $dao;
        $this->queue = $queue;
        
        
        $this->fixtureTypeArr[FIXTURE_TYPE_NODE]["Namae"]="Node";$this->fixtureTypeArr[FIXTURE_TYPE_NODE]["ShortName"]="ND";
        $this->fixtureTypeArr[FIXTURE_TYPE_DRIVERA]["Namae"]="Driver A";$this->fixtureTypeArr[FIXTURE_TYPE_DRIVERA]["ShortName"]="DR_A";
        $this->fixtureTypeArr[FIXTURE_TYPE_DRIVERB]["Namae"]="Driver B";$this->fixtureTypeArr[FIXTURE_TYPE_DRIVERB]["ShortName"]="DR_B";
        $this->fixtureTypeArr[FIXTURE_TYPE_WALLSWITCH]["Namae"]="Wall switch";$this->fixtureTypeArr[FIXTURE_TYPE_WALLSWITCH]["ShortName"]="WS";
        $this->fixtureTypeArr[FIXTURE_TYPE_OCCUPANCYSENSOR]["Namae"]="Occupency sensor";$this->fixtureTypeArr[FIXTURE_TYPE_OCCUPANCYSENSOR]["ShortName"]="OS";

    }
    
    protected function queueRequest($component,$data,$delay=0,$priority=PheanstalkInterface::DEFAULT_PRIORITY)
    {
        $reJob = array("component" => $component, "data" => $data);
        //print_r(json_encode($reJob));
        $qID = $this->queue->put(json_encode($reJob),$priority,$delay);
        //echo "qID: >>>>>> $qID \n"; 
        
        //echo "Delay>>> ".$delay; 
       // var_dump($test);
    }
    
    abstract public function processRecord($record);
    
    
    protected function getPacketID($dao){
        try{
            $sql="SELECT PacketID FROM pmi_packet_settings FOR UPDATE";
            $rtnVal = $dao->get_single_result($sql);
            $rtnVal = !empty($rtnVal) ? $rtnVal : 5;
            //echo "\n\nPACKET ID === $rtnVal \n\n";
            $updateVal = $rtnVal == 255 ? 5 : $rtnVal+1;
            $dao->update("UPDATE pmi_packet_settings SET PacketID = $updateVal");
            
            //echo "UPDATE pmi_packet_settings SET PacketID = $rtnVal \n\n";
            return $rtnVal;//$rtnVal["PacketID"];
        } catch (Exception $ex) {
            throw $ex;
        }
    }

}

?>