<?php

namespace InboundProcess;
use Pheanstalk\Pheanstalk;
use Pheanstalk\PheanstalkInterface;

class InboundQueueClient {

    private $queue;
    private $status = 0;
    private $errorMsg;

    public function __construct($queueUri, $queueName) {

        try {
            $this->queue = new Pheanstalk($queueUri);
            $this->queue->useTube($queueName);
        } catch (\Exception $ex) {
            $this->status = 1; // Error Connecting Requeue
            $this->errorMsg = $ex->getMessage();
            die($ex->getMessage());
        }

    }

    public function queueNotificationRequest($component, $record,$delay=0,$priority=PheanstalkInterface::DEFAULT_PRIORITY) {
        // Check Pheanstalk connected

        if ($this->status != 0)
            $this->reQueueJob($record);
        else {

            // If yes get connection & satus
            try {
                //echo "Server Not Listening: ".$this->queue->getConnection()->isServiceListening();
                if ($this->queue->getConnection()->isServiceListening()) {
                    $job = array("component" => $component, "data" => $record);
                    // if($component == EVENT_COM) $priority = QUEUE_HIGH_PRIORITY;
                    $priority = QUEUE_HIGH_PRIORITY;
                    //print_r(json_encode($job));
                    $this->queue->put(json_encode($job),$priority,$delay);
					//echo "put>>>>> \n";
                    return true;
                } else {
                    $this->status = 2;
                    $this->reQueueJob($record);
                }
            } catch (\Exception $ex) {
                $this->status = 3;
                $this->reQueueJob($record);
            }
        }

        return false;
    }

    // Requeue JOB
    private function reQueueJob($record) {
        try {
          //echo "Inside Re - Queue - ".$this->status;
           /* $systemLogModel = new \SystemLogModel();
            if (sizeof($record) > 0) {
                $sysLogID = $record["ID"];
                $systemLog["ID"] = $sysLogID;
                $systemLog["Status"] = SYSTEM_LOG_FLAG_REQUEUED;
                $systemLog["ReQueueReason"] = ( $this->status == 1) ? "Pheanstalk connection failed. " : (( $this->status == 2) ? "Failed to insert to Queue - Not listening" : "Error occured while inserting to Queue");
                $systemLogModel->save($systemLog);
            }*/
        } catch (\Exception $ex) {
            // Major error unable to reque notifiy Operations
            $exceptionSerialized = $ex->__toString();
            system("echo '" . $exceptionSerialized . "'| mail -s 'NOTIFICATION RE-QUEUE FAILED - CRITICAL' operations@inspextor.me");
        }
    }

}

?>
