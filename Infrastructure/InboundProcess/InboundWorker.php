<?php
namespace InboundProcess;
require_once 'config.php';
require 'vendor/autoload.php';

use Pheanstalk\Pheanstalk;
use Pheanstalk\PheanstalkInterface;

class InboundWorker extends \Thread
{

    private $processor = array(SYNCHRONIZE => "InboundProcess\SynchronizeProcessor");

    private $queue = null;
    private $queueUri = null;
    private $queueName = null;
    private $tid = 0;

    public function __construct($tid, $queueUri, $queueName) {
	$this->tid = $tid;
        $this->queueUri = $queueUri;
        $this->queueName = $queueName;
    }

    public function run() {
        
        require 'vendor/autoload.php'; // This is required for thread to be able to load classes.

        $queue = new Pheanstalk($this->queueUri);
        $queue->watch($this->queueName);

        echo "Thread " . $this->tid . " joined queue. " . $this->queueUri . "-" . $this->queueName . "\n";
        while ($job = $queue->reserve()) {
            
            // Per item connection 
            $dao = new \DBC();
            $dao->autocommit(false);

            try {
                echo "Thread " . $this->tid . " processing job.\n";
				
                $received = json_decode($job->getData(),true); 
				//print_r($received); die;
                //print_r($job);

                //$queue->delete($job); // remove job from queue

                $component = $received['component'];
                //print_r("Component >> ". $component); die; 

                $ret = $this->processRecord($component, $received['data'], $dao, $queue);

                if (!isset($ret) || $ret <= 0) {
                    $queue->delete($job); // remove job from queue
                    $dao->commit();
                    echo "deleted job\n";
                    //var_dump($job);
                } else {
                   // $queue->release($job, PheanstalkInterface::DEFAULT_PRIORITY, 2); // release with a delay so that the job comes back for processing after delay time.
                    echo "releasing job for " . $ret . "seconds\n";
                }

            } catch (\Exception $ex) {
                echo "exception is:" . $ex->getMessage() . "\n";
                //die();
                //Insert exception details to db
                try {
                    $tempRe = $job->getData();
                    $dbrec = array();
                    $dbrec["JData"] = $tempRe;
                    $dbrec["ExceptionMsg"] = $ex->getMessage();
                    $status = $dao->insert_query($dbrec, "pmi_test_pr");
                    $dao->commit();
                } catch (Exception $exc) {
                    $dao->rollback();
                    echo "exception is:" . $ex->getMessage() . "\n";
                }

                // TBD what to do here wrt logging ?
                $queue->delete($job);
                //$queue->release($job);
            }
            //$queue->release($job);
            echo "end of job\n";

            // Close once done 
            $dao->autocommit(true);
            $dao->close();
        }
       
    }

    protected function processRecord($component, $record, $dao, $queue) {
        //die("component === $component ||||| \n ");
        $componentProcessorClass = "";
        switch ($component) {
            case SYNCHRONIZE:
			case INITIAL_SYNCHRONIZE:
			case ALERTMSG_SYNCHRONIZE:
                $componentProcessorClass = "InboundProcess\SynchronizeProcessor";
                break;
        }
		//die("component === $componentProcessorClass ||||| \n ");
        // $componentProcessorClass = "UDPProcess\CiscoDataProcessor"; //$this->processor[$component];
       echo "component=" . $component . "=>" . $componentProcessorClass . "\n"; 
        $ret = 0;
        print("\n" . $componentProcessorClass . "\n");
       // echo "yes";

        if (isset($componentProcessorClass)) {
            echo ">>>>>>".$componentProcessorClass."\n";
            try {

                $processor = new $componentProcessorClass($dao,$queue);
                $ret = $processor->processRecord($record);
            } catch (Exception $e) {
                echo $e->getMessage();
            }
        }
        return $ret;
    }

}
?>
