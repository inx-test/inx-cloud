<?php

namespace InboundProcess;

include_once("config.php");
require_once("_classes/256.php");
require_once("_classes/DBC.php");

// Load namespace
use phpseclib\Net\SSH2;

class SynchronizeProcessor extends InboundProcess {
	
	private function  atomic_put_contents($filename, $data)
	{
		// Copied largely from http://php.net/manual/en/function.flock.php
		$fp = fopen($filename, "w+");
		if (flock($fp, LOCK_EX)) {
			fwrite($fp, $data);
			flock($fp, LOCK_UN);
		}
		fclose($fp);
	}

    public function processRecord($record) {
        print_r($record);
        echo "\n Synchronize Data Proceess Start... \n";
		//echo PRIVATE_KEY;
        try {
            $encryptStr = $record["p"];
            $instanceName = $record["i"];
            $fileName = $record["f"];
			$action = $record["a"];
            $decryptStr = decrypt( $encryptStr, PRIVATE_KEY );
			echo "\r\ndecrypt ::: $decryptStr ";

			$sql =<<<EOD
					select PID.id,PIDD.DeviceDBName,PID.device_name,PID.inspextorID FROM pmi_inx_device PID
					LEFT JOIN  pmi_inx_device_db  PIDD ON (PIDD.DeviceID = PID.id)
					WHERE PID.inspextorID = '$instanceName' AND PID.`Password` = '$decryptStr'
EOD;
			//echo "here0001 __>". $sql;
			//var_dump( $this->dao );
      //$dao = new \DBC();
			$instanceDetails = $this->dao->get_result($sql,MYSQLI_USE_RESULT);
			print_r($instanceDetails);

			if(sizeof($instanceDetails)>0){
				$dbName = $instanceDetails[0]["DeviceDBName"];
				$dbId = $instanceDetails[0]["id"];

				if($fileName != '')
				{
					echo "\r\n Filename is not null == $fileName";
					$filePath = SERVER_URL.SERVER_IMPORT.DIRECTORY_SEPARATOR.$fileName;
					$extractFolder = str_replace(".zip", "", $filePath );
					echo "file path:::".$filePath;
					if(file_exists($filePath)){
						echo "\r\n File exist == $fileName";
						// Unzip file to same folder where zip uploaded.
						$cmd = 'unzip -o '.$filePath.' -d '.$extractFolder;
						echo $cmd."\n" ;
						if(exec($cmd)){
							echo "\r\n File unzipped == $filePath to $extractFolder";
							
							// Initial sync
							if( $action == 'initialSync' ){
								$structFile = $extractFolder.DIRECTORY_SEPARATOR .'structure.sql';
								$dataFile = $extractFolder.DIRECTORY_SEPARATOR .'data.sql';

								// Check upload is valid or not
								if( file_exists($structFile) && file_exists($dataFile) ){
									// Invalid bundle
									// Create a database with instance name & load sql files to that database

									$dropDBCmd = 'echo "drop database if exists '.$dbName.'" | mysql -u '.DB_USER_ROOT.' -p'.DB_PASSWD_ROOT.' 2>&1';
									echo $dropDBCmd;
                  $dropDBResult = exec($dropDBCmd);

									$createDBCmd = 'echo "create database if not exists '.$dbName.'" | mysql -u '.DB_USER_ROOT.' -p'.DB_PASSWD_ROOT.' 2>&1';
                  echo $createDBCmd;
                  $createDBResult = exec($createDBCmd);

                  echo $createDBResult;

									if( !empty($createDBResult) ){

										$exists = ( strpos($createDBResult, "database exists") !== false ) ? false : true;

										// Import data files
										// Structure
										$importStructureCmd = 'mysql -u '.DB_USER_ROOT.' -p'.DB_PASSWD_ROOT.' '.$dbName.' < '.$structFile .' 2>&1';
										if( exec($importStructureCmd) ) {

											// Data
											$importDataCmd = 'mysql -u '.DB_USER_ROOT.' -p'.DB_PASSWD_ROOT.' '.$dbName.' < '.$dataFile .' 2>&1';
											if( exec($importDataCmd) ) {
												// Success
												echo "Sync success";
												$sqlUpdate = "Update  pmi_inx_device set Status = 1 where id= $dbId";
												echo $sqlUpdate;
												$this->dao->query($sqlUpdate);
												//unlink($filePath);//zip
												//unlink($structFile);//file inside the folder
												//unlink($dataFile);//file inside the folder
												//rmdir($extractFolder);//folder
											}
										}
										// End IF
									}
									// Create database command

								}
								else {
									// No valid files found.
									// Skipping import & sync.
									// Replay back to Local ?
								}
								// End Else
							}
							else if( $action == 'synchronize' )
							{
								$sqlfilename = $record["n"];
								$filePath = SERVER_URL.SERVER_IMPORT.DIRECTORY_SEPARATOR.$fileName;
								$extractFolder = str_replace(".zip", "", $filePath );
								$dataFile = $extractFolder.DIRECTORY_SEPARATOR .$sqlfilename;
								//$date3hourPrior = date("Y-m-d H:i:s", strtotime('-1 hour') );

								//$orderOldFile = SERVER_URL.SERVER_IMPORT.DIRECTORY_SEPARATOR.$instanceName."_".strtotime($date3hourPrior)."_sync_bundle.zip";

								$dir = SERVER_URL.SERVER_IMPORT.DIRECTORY_SEPARATOR;
								//echo "Directory".$dir."\n \n";
								if (is_dir($dir)){
									if ($dh = opendir($dir)){
										while (($file = readdir($dh)) !== false)
										{
											if($file !='' && $file != '.' &&  $file != '..')
											{
												$lastModifiedTime = filemtime($dir."".$file);
												$t = time()-3600 ;
												if($lastModifiedTime <= $t)
												{
			echo "file=".$file."\n";
													$ext = explode(".",$file)[1];
													if($ext == "zip")
													{
														unlink($dir.$file);
													}
													else
													{
														$extractFolder = $dir.$file;

														//echo "Starts Unlink";
														//echo "---\n".$extractFolder."\n";
														$files2 = scandir($extractFolder, 1);
														if(file_exists($extractFolder."/".$files2[0]))
														{
															unlink($extractFolder."/".$files2[0]);//file inside the folder
															rmdir($extractFolder);
														}
														//rmdir($dir.$file);
													}
												}
											}
											//break;
										}
										closedir($dh);
									}
								}
								//$orderLogFile = SERVER_URL.SERVER_IMPORT.DIRECTORY_SEPARATOR.$instanceName."_".date('Y-m-d')."_sync_bundle.zip";
								if( file_exists($dataFile) ){
									echo $dataFile;
										// Data
									$importDataCmd = 'mysql -u '.DB_USER_ROOT.' -p'.DB_PASSWD_ROOT.' '.$dbName.' < '.$dataFile .' 2>&1';

									echo $importDataCmd;
									if( exec($importDataCmd) ) {
										// Success
										echo "Sync success";
										$sqlUpdate = "Update  pmi_inx_device set Status = 1 where id= $dbId";
										$this->dao->query($sqlUpdate);

										//unlink($filePath);//zip
										//unlink($extractFolder."/".$sqlfilename);//file inside the folder
										//rmdir($extractFolder);
									}

								}
							}

						}
					}
				}
				if($action == 'alertMessage')
				{
					print_r($record);
          file_put_contents('request-from-alert.txt', print_r($record,true));
					$resStatus = 0;
					$db = new \DBC(DB_HOST,DB_USER,DB_PASSWD,$dbName);

					/*$idQry = "SELECT `AUTO_INCREMENT` FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = '".$dbName."' AND TABLE_NAME = 'pmi_response_messages'";
					$idRes = $db->get_result($idQry);
					print_r($idRes);
					$autoincreid = $idRes[0]['AUTO_INCREMENT'];
					echo "===";
					echo $autoincreid;
					echo "-------";
					if($autoincreid != 1)
					{
						$val = $autoincreid-1;
						$updateQry = "Update pmi_response_messages set Deleted =1 where ID=".$val;
						echo $updateQry;
						$resStatus = $db->query($updateQry);
					}*/
					if($record['query'] != '')
					{
						echo $record['query'];
						$sql = $record['query'];
						$db->query($sql);
					}
					if($record['type'] == 'clearTag')
					{
						 $truncateSQL = array(
							"pmi_alert_definition",
							"pmi_alert_lookup",
							"pmi_cluster_mapping",
							"pmi_device_alerts",
							"pmi_event",
							"pmi_map_nodes",
							"pmi_merge_request",
							"pmi_node",
							"pmi_node_savings",
							"pmi_node_status",
							"pmi_override",
							"pmi_override_cluster_mapping",
							"pmi_packet_queue",
							"pmi_policy_line_mapping",
							"pmi_policy_mapping",
							"pmi_policy_node_mapping",
							"pmi_tag",
							"pmi_tag_status");
						foreach ($truncateSQL as $value) {
							 $db->query("TRUNCATE TABLE $value");
						}
					}
					else if($record['type'] == 'saveUploadImage' || $record['type'] == 'updateSwitchSchedule')
					{
						$filename = $record['filename'];
						if($record['type'] == 'updateSwitchSchedule')
						{
							$filePath = SERVER_URL.SERVER_IMPORT.DIRECTORY_SEPARATOR.$filename;
							echo $filePath."\n";
							unlink($filePath);
							$filePath = ROOTPATH."files/".urldecode($filename);
							echo $filePath;
							unlink($filePath);
						}
						else{
							$filePath = SERVER_URL.SERVER_LOCATION.DIRECTORY_SEPARATOR.$filename;
							echo $filePath."\n";
							unlink($filePath);
						}
					}
					else if ( $record['type'] == 'terminal' ){
						file_put_contents(ROOTPATH.'terminal-out.txt', $record['result']);
						$obLogId = $record['obLogId'];
						
						if( $obLogId >  0 ) {
							// Prepare Data 
							$dataToUpdate = array();
							$dataToUpdate['Status'] = 3; // Replied 
							$dataToUpdate['Data'] = $record['result']; // Reply msg - overwrite data 

							$dataToUpdateCondition = array();
							$dataToUpdateCondition["ID"]= $obLogId;


							// Save to database
							$db->update_query($dataToUpdate, 'pmi_outbound_log', $dataToUpdateCondition);
						}
						
					}

					$msg = $record['msg'];
					$status = $record["status"];
					$resultData = $record['result'];
					$resultType = $record['type'];
					$sqlQry = "Insert into pmi_response_messages(ResponseMessages,CreatedOn,Status,Result,Type) values('$msg', NOW(),$status,'$resultData','$resultType')";
					echo $sqlQry;
					$db->query($sqlQry);
				}
				$this->dao->commit();
			}
        } catch (Exception $exc) {
            $this->dao->rollback();
			echo $exc->getMessage();
        }
    }

}

?>
