<?php
require_once '_classes/DBC.php';

class DeviceDataFetch extends \Thread    
{
    private $index = 0;
    private $devices = array();
    private $dev;
    
    public function __construct() {
    }
    
    public function getData(){
        $db = new \DBC();
        $sql = "select id,serial_no,ip_address,galileo_id,daily_log_id,hourly_log_id from pmi_device where deleted=0";
        $this->devices = $db->get_result($sql);
        print_r($this->devices);
        $db->close();
	$this->index=0;
    }
    
    public function getNewDeviceData(&$ct){
        
        $this->synchronized(
            function($thread,&$ct)
            {
                echo "[".$ct->tid."] in sync function :".$thread->index.":\n";
                if($thread->index < count($thread->devices)) {
                    $ct->setDevice($thread->devices[$thread->index++]);
                }
                else {
                    $ct->setDevice(null);
                    echo "[".$ct->tid."] Nothing more to process\n";
                }
            }, $this,$ct
        );
    }
    
    public function getDeviceCount()
    {
		return count($this->devices);
	}
	
    public function run()       
    {
    }
    
    public function deviceAlerts()
    {
        echo "Alerts Detected";
    }
}
?>
