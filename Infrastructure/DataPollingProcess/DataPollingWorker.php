<?php
require_once '_classes/DBC.php';
include_once(APL."MCrypt.php");

function exception_error_handler($errno, $errstr, $errfile, $errline) {
    //echo $errno;
    if ($errNo != E_NOTICE && $errNo != E_WARNING) {
        echo "::" . $errno . "-" . $errstr . "-" . $errfile . "-" . $errline . "::\n";
        throw new Exception($errstr . "=" . $errno . "-" . "0" . "-" . $errfile . "-" . $errline);
    }
}

class DataPoolingWorker  extends \Thread {

    public $tid = 0;
    private $devData;
    private $dev;
    private $count=0;
    private $commands=array();
    private $BULK_QUERY_COUNT=10;
    
    public function __construct($tid,&$devData,$commands) {
        $this->tid = $tid;
        $this->devData = $devData;
        $this->commands=$commands;
    }

    public function setDevice($dev)
    {
        $this->dev = $dev;
        //print_r($this->dev);
    }
	
    public function run() {
        //set_error_handler("exception_error_handler");
        $db = new \DBC();
        $db->autocommit(false);
        
        try 
        {
            while(true)
            {
                $this->devData->getNewDeviceData($this);    //synchronized fn
                if ( $this->dev == null ) {
					echo "[".$this->tid."] nothing to process.\n";
					break;
				}
                echo "[".$this->tid."] processing device ".$this->dev['serial_no']."=>".$this->dev['ip_address']."\n"; 
                //sleep(1);
                //continue;
                echo "H1\n";
                print_r($this->commands);
                $devIP = $this->dev['ip_address'];

                //********************  GET_HB Section Starts  ***********************************//
                if ( isset($this->commands[GET_HB]) )	{
                    $dataHB = array(
                                    "action" => "Get_HB", 
                                    "source" => 3
                    );
                    $contentHB = http_build_query($dataHB);
                    $urlHB = $this->getWebURL($devIP,$dataHB);
                    print(">>>>".$urlHB);

                    $resultHB = $this->invokeWebMethod($urlHB);
                    echo "[".$this->tid."] HB= ".$resultHB."\n";

                    $resultArr = json_decode($resultHB,true);
                    $resultHBValues = $resultArr['responseData'];
                    unset($resultHBValues['ID']);
                    //echo "here\n";
                    //print_r($resultHBValues);
                    echo "so=".count($resultHBValues)."\n";
                    $deviceName="";

                    if( count($resultHBValues) > 0 )
                    {
                            echo "HB EXIST \n \n";
                            $deviceId = $this->dev['id'];
                            $existDevSql = "select count(id) from pmi_device where id='$deviceId' and deleted = 0";
                            $existDev = $db->get_single_result($existDevSql);
                            
                            if($existDev != 0){
                                //$resultHBValues["DEVICENAME"]="DEV Name";
                                if ( isset($resultHBValues["DEVICENAME"])	)	{
                                    $deviceName=$resultHBValues["DEVICENAME"];
                                    unset($resultHBValues["DEVICENAME"]);
                                }

                                if ( isset($resultHBValues["SOFTLOCK"])	)	{
                                    unset($resultHBValues["SOFTLOCK"]);
                                }
                                ///$resultHBValues["deleted"]=0;	// undelete HB record if data is coming in.
                                $where = array();
                                $where["deviceID"]  =  $this->dev['id'];


                                $existsql = "select count(*) from pmi_heart_beat where deviceID='$deviceId'";
                                $countHB = $db->get_single_result($existsql);
                                //echo "COUNT = ".$countHB;

                               /******************* Start temporary process *************/

                                if (array_key_exists('RES2', $resultHBValues)) {
                                    //echo "RES2 \n";
                                    $resultHBValues["H1"] = $resultHBValues["RES2"];
                                    unset($resultHBValues["RES2"]);
                                }
                                if (array_key_exists('RES3', $resultHBValues)) {
                                    $resultHBValues["H3"] = $resultHBValues["RES3"];
                                    unset($resultHBValues["RES3"]);
                                }
                                if (array_key_exists('RES4', $resultHBValues)) {
                                    $resultHBValues["H5"] = $resultHBValues["RES4"];
                                    unset($resultHBValues["RES4"]);
                                }
                                if (array_key_exists('RES5', $resultHBValues)) {
                                    $resultHBValues["H7"] = $resultHBValues["RES5"];
                                    unset($resultHBValues["RES5"]);
                                }
                                if (array_key_exists('RES6', $resultHBValues)) {
                                    $resultHBValues["H9"] = $resultHBValues["RES6"];
                                    unset($resultHBValues["RES6"]);
                                }
                                if (array_key_exists('RES7', $resultHBValues)) {
                                    $resultHBValues["H11"] = $resultHBValues["RES7"];
                                    unset($resultHBValues["RES7"]);
                                    /*echo "HB______ \n";
                                    print_r($resultHBValues);
                                    echo "\n";*/
                                }

                                /******************* end temporary process *************/     

                                if($countHB == 0){
                                    //Insert HB data record to database
                                    $resultHBValues["deviceID"] = $this->dev['id'];
                                    $addDeviceHB = $db->insert_query($resultHBValues , 'pmi_heart_beat'); 
                                }
                                else{
                                    //Update HB data Record for this Device in database
                                    $updateDeviceHB = $db->update_query($resultHBValues , 'pmi_heart_beat',$where);
                                    echo "1>>>>".$updateDeviceHB."\n";
                                }

                                $dqry = "update pmi_device set status=1";
                                if ( $deviceName != "" ) $dqry.=",device_name='".$deviceName."'";
                                $statusSql = $dqry . " where id=".$this->dev['id'];

                                echo "$statusSql\n";
                                $devStatus = $db->query($statusSql);
                                
                                //************* ALERT SECTION START***********************//
                                $this->generateAlert($deviceId,$resultHBValues,$db);
                                //************* ALERT SECTION END***********************//
                                
                                /******** KWH,KVA AND KVAR SUM CALCULATION START *******/
                                $this->generateDevSum($deviceId,$resultHBValues,$db);
                                /******** KWH,KVA AND KVAR SUM CALCULATION END *******/
                            }
                            else{
                                //update Last Log, Last Daily log and Last Hourly log IDs
                                echo "DEVICE NOT AVAILABLE \n \n";
                                $updateLastLogIDs = $db->update_query(array("deleted"=>1) , 'pmi_heart_beat',array("deviceID"=>$deviceId));
                                echo "HB DELETED >>>>".$updateDeviceHB."\n";
                            }
                    }
                    else
                    {
                        $deviceId = $this->dev['id'];

                        //update Last Log, Last Daily log and Last Hourly log IDs
                        echo "HB NOT EXIST \n \n";
                        $updateStatus = $db->query("update pmi_device set status=0 where id=".$this->dev['id']);
                        $updateLastLogIDs = $db->update_query(array("deleted"=>1) , 'pmi_heart_beat',array("deviceID"=>$deviceId));
                        echo "2>>>>".$updateDeviceHB."\n";

                    }
                }
                //********************  GET_HB Section Ends  ***********************************//
                              
                if ( isset($this->commands[GET_LOG]) )	{

                  //********************  GET_LOG Section Starts  ********************************//
                    $hasMoreRecords = true;

                    while($hasMoreRecords)
                    {
                                  $dataLOG = array(
                                                  "action" => "GetLogData", 
                                                  "logID" => $this->dev['galileo_id'],
                                                  "hourlyLogID" => $this->dev['hourly_log_id'],
                                                  "dailyLogID" => $this->dev['daily_log_id'],
                                                  "serialNo" => $this->dev['serial_no'],
                                                  "source" => 3,
                                                  "limit" => LOG_DATA_FETCH_LIMIT
                                  );
                                  $contentLOG = http_build_query($dataLOG);

                                  $urlLOG = $this->getWebURL($devIP,$dataLOG);
                                  echo "URL LOG : $urlLOG\n";
                                  $resultLOG = $this->invokeWebMethod($urlLOG,120);
                                  echo "[".$this->tid."] LOG= ".$resultLOG."\n";

                                  $resultLOGArr = json_decode($resultLOG,true);
                                  $logDataArr = $resultLOGArr["responseData"]["LogData"];
                                  $dailyLogArr = $resultLOGArr["responseData"]["LogDaily"];
                                  $hourlyLogArr = $resultLOGArr["responseData"]["LogHourly"];
                                  $maxLogID = $resultLOGArr["responseData"]["MaxLogID"];
                                  print_r($resultLOGArr["responseData"]);
                                  $where = array();
                                  $where["id"]  =  $this->dev['id'];

                                  $lastLogID = array();

                                  if(sizeof($logDataArr) > 0 ) 
                                  {
                                          $logRec = array();
                                          $dailyLogRec = array();
                                          $hourlyLogRec = array();
                                          $i = 0;
                                          //$lastLogID['status'] = 1;
                                          echo "Log data records received=".count($logDataArr)."\n";
                                          if ( count($logDataArr) > 0 )	{
                                                  $logfieldsql = "INSERT INTO pmi_log (deviceID, status, volts_ph1, volts_ph2, volts_ph3, amps1, amps2, amps3, pf1, pf2, pf3, tamb, tint, vthd, athd, caps1, caps2, caps3, volts_imb, amps_imb, apar_pwr, apar_pwr1, apar_pwr2, apar_pwr3, real_pwr, react_pwr, neutral_amps, res0, res1, cs1, cs2, logdate, timez) VALUES ";
                                                  $logvaluesql = "";
                                                  $ind=0;
                                                  foreach($logDataArr as $resArr)
                                                  {
                                                          $resArr["deviceID"] = $this->dev['id'];
                                                          $lastLogID['galileo_id'] = $resArr['ID'];

                                                          if($logvaluesql != "") $logvaluesql .= ",";
                                                          $logvaluesql .= "('".$resArr['deviceID']."','".$resArr['STATUS']."','".$resArr['VOLTS_PH1']."','".$resArr['VOLTS_PH2']."','".$resArr['VOLTS_PH3']."','".$resArr['AMPS1']."','".$resArr['AMPS2']."','".$resArr['AMPS3']."','".$resArr['PF1']."','".$resArr['PF2']."','".$resArr['PF3']."','".$resArr['TAMB']."','".$resArr['TINT']."','".$resArr['VTHD']."','".$resArr['ATHD']."','".$resArr['CAPS1']."','".$resArr['CAPS2']."','".$resArr['CAPS3']."','".$resArr['VOLTS_IMB']."','".$resArr['AMPS_IMB']."','".$resArr['APAR_PWR']."','".$resArr['APAR_PWR1']."','".$resArr['APAR_PWR2']."','".$resArr['APAR_PWR3']."','".$resArr['REAL_PWR']."','".$resArr['REACT_PWR']."','".$resArr['NEUTRAL_AMPS']."','".$resArr['RES0']."','".$resArr['RES1']."','".$resArr['CS1']."','".$resArr['CS2']."','".$resArr['LOGDATE']."','".$resArr['TIMEZ']."')";

                                                          $ind++;
                                                          if ( $ind % $this->BULK_QUERY_COUNT == 0 )	{
                                                                  echo "$ind=>\n";
                                                                  //insert Device Log data
                                                                  $logsql = $logfieldsql.$logvaluesql;
                                                                  //echo $logsql;
                                                                  $addDeviceLOG = $db->query($logsql);
                                                                  $logvaluesql="";	//reset log sql query
                                                          } 

                                                  } 

                                                  if ( $logvaluesql != "" )	{
                                                          echo "inserting remaining log records\n";
                                                          //insert Device Log data
                                                          $logsql = $logfieldsql.$logvaluesql;
                                                          //echo $logsql;
                                                          $addDeviceLOG = $db->query($logsql);
                                                  }
                                                  $this->dev['galileo_id'] = $lastLogID['galileo_id'];
                                          }

                                          echo "Daily Log data records received=".count($dailyLogArr)."\n";
                                          if ( count($dailyLogArr) > 0 )	{
                                                  $dailyfieldsql = "INSERT INTO pmi_daily_log (deviceID, status, volts_ph1, volts_ph2, volts_ph3, amps1, amps2, amps3, pf1, pf2, pf3, vthd, athd, volts_imb, amps_imb, apar_pwr, apar_pwr1, apar_pwr2, apar_pwr3, real_pwr, react_pwr, neutral_amps, logdate, daily_LogID) VALUES ";
                                                  $dailyvaluesql = "";
                                                  $ind=0;
                                                  foreach($dailyLogArr as $dailyArr)
                                                  {
                                                          $dailyArr["deviceID"] = $this->dev['id'];
                                                          $dailyArr["daily_LogID"] = $dailyArr['ID'];
                                                          $lastLogID['daily_log_id'] = $dailyArr['ID'];
                                                          unset($dailyArr["ID"]);
                                                          //print_r($dailyArr); 
                                                          if($dailyArr["daily_LogID"] == $this->dev['daily_log_id']){
                                                                  $dailyWhere = array();
                                                                  $dailyWhere['daily_LogID'] = $dailyArr["daily_LogID"];
                                                                  $dailyWhere['deviceID']    = $this->dev['id'];   

                                                                  $updateDailyLog = $db->update_query($dailyArr , 'pmi_daily_log',$dailyWhere);
                                                          }
                                                          else{
                                                                  if($dailyvaluesql != "") $dailyvaluesql .= ",";
                                                                  $dailyvaluesql .= "('".$dailyArr["deviceID"]."','".$dailyArr['STATUS']."','".$dailyArr['VOLTS_PH1']."','".$dailyArr['VOLTS_PH2']."','".$dailyArr['VOLTS_PH3']."','".$dailyArr['AMPS1']."','".$dailyArr['AMPS2']."','".$dailyArr['AMPS3']."','".$dailyArr['PF1']."','".$dailyArr['PF2']."','".$dailyArr['PF3']."','".$dailyArr['VTHD']."','".$dailyArr['ATHD']."','".$dailyArr['VOLTS_IMB']."','".$dailyArr['AMPS_IMB']."','".$dailyArr['APAR_PWR']."','".$resArr['APAR_PWR1']."','".$resArr['APAR_PWR2']."','".$resArr['APAR_PWR3']."','".$dailyArr['REAL_PWR']."','".$dailyArr['REACT_PWR']."','".$dailyArr['NEUTRAL_AMPS']."','".$dailyArr['LOGDATE']."','".$dailyArr["daily_LogID"]."')";
                                                          }

                                                          $ind++;
                                                          if ( $ind % $this->BULK_QUERY_COUNT == 0 )	{
                                                                  echo "$ind=>\n";
                                                                  $dailysql = $dailyfieldsql.$dailyvaluesql;
                                                                  $addDailyLOG = $db->query($dailysql);
                                                                  $dailyvaluesql="";	//reset dailyvaluesql
                                                          } 
                                                  }
                                                  //insert Daily Log data
                                                  if($dailyvaluesql!=""){
                                                          echo "inserting remaining daily log records\n";
                                                          $dailysql = $dailyfieldsql.$dailyvaluesql;
                                                          $addDailyLOG = $db->query($dailysql);
                                                  }
                                                  $this->dev['daily_log_id'] = $lastLogID['daily_log_id'];

                                          }

                                          echo "Hourly Log data records received=".count($hourlyLogArr)."\n";
                                          if ( count($hourlyLogArr) > 0 )	{
                                                  $hourfieldsql = "INSERT INTO pmi_hourly_log (deviceID, status, volts_ph1, volts_ph2, volts_ph3, amps1, amps2, amps3, pf1, pf2, pf3, vthd, athd, volts_imb, amps_imb, apar_pwr, apar_pwr1, apar_pwr2, apar_pwr3, real_pwr, react_pwr, neutral_amps, logdate, hour_LogID) VALUES ";
                                                  $hourvaluesql = "";
                                                  $ind=0;
                                                  foreach($hourlyLogArr as $hourlyArr)
                                                  {
                                                          $hourlyArr["deviceID"] = $this->dev['id'];
                                                          $hourlyArr["hour_LogID"] = $hourlyArr['ID'];
                                                          $lastLogID['hourly_log_id'] = $hourlyArr['ID'];
                                                          unset($hourlyArr["ID"]);
                                                          //print_r($hourlyArr); 
                                                          if($hourlyArr["hour_LogID"] == $this->dev['hourly_log_id']){
                                                                  $hourlyWhere = array();
                                                                  $hourlyWhere['hour_LogID']  = $hourlyArr["hour_LogID"];
                                                                  $hourlyWhere['deviceID']    = $this->dev['id'];   

                                                                  $updateHourLog = $db->update_query($hourlyArr , 'pmi_hourly_log',$hourlyWhere);
                                                          }
                                                          else{
                                                                  if($hourvaluesql != "") $hourvaluesql .= ",";
                                                                  $hourvaluesql .= "('".$hourlyArr["deviceID"]."','".$hourlyArr['STATUS']."','".$hourlyArr['VOLTS_PH1']."','".$hourlyArr['VOLTS_PH2']."','".$hourlyArr['VOLTS_PH3']."','".$hourlyArr['AMPS1']."','".$hourlyArr['AMPS2']."','".$hourlyArr['AMPS3']."','".$hourlyArr['PF1']."','".$hourlyArr['PF2']."','".$hourlyArr['PF3']."','".$hourlyArr['VTHD']."','".$hourlyArr['ATHD']."','".$hourlyArr['VOLTS_IMB']."','".$hourlyArr['AMPS_IMB']."','".$hourlyArr['APAR_PWR']."','".$resArr['APAR_PWR1']."','".$resArr['APAR_PWR2']."','".$resArr['APAR_PWR3']."','".$hourlyArr['REAL_PWR']."','".$hourlyArr['REACT_PWR']."','".$hourlyArr['NEUTRAL_AMPS']."','".$hourlyArr['LOGDATE']."','".$hourlyArr["hour_LogID"]."')";
                                                          }

                                                          $ind++;
                                                          if ( $ind % $this->BULK_QUERY_COUNT == 0 )	{
                                                                  echo "$ind=>\n";
                                                                  $hourlysql = $hourfieldsql.$hourvaluesql;
                                                                  $addHourlyLOG = $db->query($hourlysql);
                                                                  $hourvaluesql="";	//reset hourvaluesql
                                                          }
                                                  }
                                                  //insert Hourly Log data
                                                  if($hourvaluesql!=""){
                                                          echo "inserting remaining hourly log records\n";
                                                          $hourlysql = $hourfieldsql.$hourvaluesql;
                                                          $addHourlyLOG = $db->query($hourlysql);
                                                  }
                                                  $this->dev['hourly_log_id'] = $lastLogID['hourly_log_id'];
                                          }
                                          print_r($lastLogID);
                                          //update Last Log, Last Daily log and Last Hourly log IDs
                                          $updateLastLogIDs = $db->update_query($lastLogID , 'pmi_device',$where);
                                          echo 'last log ID ===>'.$lastLogID['galileo_id'];
                                          if($maxLogID[0]['MAXLOGID'] - $lastLogID['galileo_id'] < 10);
                                              $hasMoreRecords = false;

                                  }
                                  else{
                                      $hasMoreRecords = false;
                                  }
                                  //********************  GET_LOG Section Ends  ********************************//
                              }

                          }
		
                if ( isset($this->commands[REMOVE_OLD_LOG_DATA]) ){
                    echo "\nRemove old record process starts at " . date('Y-m-d H:i:s.S');
                    echo "\nProcessing......";
                    $delete_span = isset($this->commands["delete_span"])? $this->commands["delete_span"] : 3;

                    $time = strtotime("-" . $delete_span . " year", time());
                    $date = date("Y-m-d", $time);

                    echo "\n\nDelete span is " . $delete_span . " year(s).";
                    echo "\nDelete data before " . $date . ".\n";

                    $logDlelete = "DELETE FROM pmi_log WHERE logdate <= '" . $date . "'";
                    echo "\nDeleting log data...\n";
                    $db->query($logDlelete);

                    $hourDelete = "DELETE FROM pmi_hourly_log WHERE logdate <= '" . $date . "'";
                    echo "\nDeleting hourly log data...\n";
                    $db->query($hourDelete);

                    $dailyDelete = "DELETE FROM pmi_daily_log WHERE logdate <= '" . $date . "'";
                    echo "\nDeleting daily log data...\n";
                    $db->query($dailyDelete);

                    echo "\nRemove old record process completed at " . date('Y-m-d H:i:s.S');
                }          
                echo "\n\nCommiting results\n";
                $db->commit();
         } 
         echo "[".$this->tid."] end of polling Thread ".$this->tid."\n";
        }
        catch (\Exception $ex)
        {
			$db->rollback();
            echo "exception is:" . $ex->getMessage() . "\n";
            //die($ex);
        } 
        $db->close();
    }
    
    //Get web url
    public function getWebURL($IPAddress,$data)
    {
		$mcrypt = new MCrypt();
		$k = uniqid('SP');
		$hk = $mcrypt->encrypt($k);
		$data["k"] = $k;
		$data["hk"] = $hk;
		
        $url = str_replace("[DEVICE_IP]",$IPAddress,LOOKUP_URL);
        //$url = str_replace("[DEVICE_IP]","192.168.2.24",LOOKUP_URL);
        $postURL = $url.http_build_query($data);
        
        $postURL =  preg_replace( "/[\n\r]/", "", $postURL );
        return $postURL;
    }
    
    //curl Function to get response data
    public function invokeWebMethod($url,$to=30)
    {
        try
        {
            //  Initiate curl
            $ch = curl_init();
            // Disable SSL verification
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            // Will return the response, if false it print the response
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); // Uncomment this line if you get no gateway response.
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
			curl_setopt($ch,CURLOPT_TIMEOUT,$to);

            // Set the url
            curl_setopt($ch, CURLOPT_URL,$url);
            // Execute
            $result =curl_exec($ch);
            // Closing
            curl_close($ch);

            return $result;
        } 
        catch (Exception $ex) {
            echo "Exception is:" . $ex->getMessage() . "\n";
        }
    }
    
    private function checkAlerts($condition,$paramFromValue,$paramToValue,$cur)
    {
		$genAlert=false;
		if($condition == '>'){
                        if($cur > $paramFromValue) $genAlert=true;
		}
		else if($condition == '<'){
			if($cur < $paramFromValue) $genAlert=true;
		}
		else if($condition == '<='){
			if($cur <= $paramFromValue) $genAlert=true;
		}
		else if($condition == '>='){
			if($cur >= $paramFromValue) $genAlert=true;
		}
		else if($condition == '='){
			if($cur == $paramFromValue) $genAlert=true;
		}
		else if($condition == '!='){
			if($cur != $paramFromValue) $genAlert=true;
		}
		else if($condition == 'between'){
			if($cur > $paramFromValue && $cur < $paramToValue) $genAlert=true;
		}
		return $genAlert;
	}
	
    //Alert Generation
    public function generateAlert($devId,$resultHBValues,$db){
        echo "Alert Generate \n";
        try 
        {
            $alertsql = "select * from pmi_alert_definition where device_id='$devId' and deleted=0";
            $devAlertDef = $db->get_result($alertsql);
            //print_r($devAlertDef); 

            $updateAlert = array();
            $updateAlert['Status']     = 1;

            $where = array();
            $where['DeviceID']   = $devId;
            $where['Status']   = 0;

            $alertArr = array();
            $i=0;

            //update alert Status
            $updateAlertStatus = $db->update_query($updateAlert , 'pmi_device_alerts',$where);

            //foreach($logDataArr as $resArr)
            if(!empty($resultHBValues))
            {
                //echo "Alert Process \n";
                $amperage = ($resultHBValues['AMPS1'] + $resultHBValues['AMPS2'] + $resultHBValues['AMPS3'])/3;
                $volt3Ph = ($resultHBValues['VOLTS_PH1'] + $resultHBValues['VOLTS_PH2'] + $resultHBValues['VOLTS_PH3'])/sqrt(3);
                $amps3Ph = ($resultHBValues['AMPS1'] + $resultHBValues['AMPS2'] + $resultHBValues['AMPS3'])/sqrt(3);    
                $neutralAmps = $resultHBValues['NEUTRAL_AMPS'];
                //$apparPwr = ($resultHBValues['VOLTS_PH1']*$resultHBValues['AMPS1']) + ($resultHBValues['VOLTS_PH2']*$resultHBValues['AMPS2']) + ($resultHBValues['VOLTS_PH3']*$resultHBValues['AMPS3']);
                //$realPwr = ($resultHBValues['VOLTS_PH1']*$resultHBValues['AMPS1']*$resultHBValues['PF1']) + ($resultHBValues['VOLTS_PH2']*$resultHBValues['AMPS2']*$resultHBValues['PF2']) + ($resultHBValues['VOLTS_PH3']*$resultHBValues['AMPS3']*$resultHBValues['PF3']);
                //$reactPwr = sqrt(($appar3PhPwr*$appar3PhPwr) - ($real3PhPwr*$real3PhPwr));

                $apparPwr = $resultHBValues['APAR_PWR'];
                $realPwr = $resultHBValues['REAL_PWR'];
                $reactPwr = $resultHBValues['REACT_PWR'];
                $powerfactr = (($resultHBValues['PF1'] * $resultHBValues['APAR_PWR1'])+
                        ($resultHBValues['PF1'] * $resultHBValues['APAR_PWR2'])+
                        ($resultHBValues['PF1'] * $resultHBValues['APAR_PWR3']))/
                        ($resultHBValues['APAR_PWR1'] + $resultHBValues['APAR_PWR2'] + $resultHBValues['APAR_PWR3']);
                $vthd = $resultHBValues['VTHD'];
                $athd = $resultHBValues['ATHD'];
                $voltImb = $resultHBValues['VOLTS_IMB'];
                $ampsImb = $resultHBValues['AMPS_IMB'];
                $tint = $resultHBValues['TINT'];
                $tamp = $resultHBValues['TAMB'];
                $logDate = $resultHBValues['LOGDATE'];
                
                $j = 0;

                //echo "Device: ".$this->dev['id']."\n";

                foreach ($devAlertDef as $alertDef)
                {
                    $parameter = $alertDef['parameter'];
                    $condition = $alertDef['parameter_condition'];
                    $paramFromValue = $alertDef['parameter_from_value'];
                    $paramToValue = $alertDef['parameter_to_value'];
                    $genAlert=false;
                    
                    switch ($parameter) {
                        case "Voltage":
                            if ( $this->checkAlerts($condition,$paramFromValue,$paramToValue,$volt3Ph) )	$alertArr[$i][$j] = array("DeviceID" => $devId, "AlertDefID" => $alertDef['id'], "Status" => 0, "CratedOn" => $logDate, "Value"=>$volt3Ph);
                            break;
                            
                        case "Amperage":
                            if ( $this->checkAlerts($condition,$paramFromValue,$paramToValue,$amperage) )	$alertArr[$i][$j] = array("DeviceID" => $devId, "AlertDefID" => $alertDef['id'], "Status" => 0, "CratedOn" => $logDate, "Value"=>$amperage);
                            break;
                            
                        case "Neutral Amps":
                            if ( $this->checkAlerts($condition,$paramFromValue,$paramToValue,$neutralAmps) )	$alertArr[$i][$j] = array("DeviceID" => $devId, "AlertDefID" => $alertDef['id'], "Status" => 0, "CratedOn" => $logDate,"Value"=>$neutralAmps);
                            break;
                            
                        case "Apparent Power":
                            if ( $this->checkAlerts($condition,$paramFromValue,$paramToValue,$apparPwr) )	$alertArr[$i][$j] = array("DeviceID" => $devId, "AlertDefID" => $alertDef['id'], "Status" => 0, "CratedOn" => $logDate,"Value"=>$apparPwr);
                            break;
                            
                        case "Real Power":
                                if ( $this->checkAlerts($condition,$paramFromValue,$paramToValue,$realPwr) )	$alertArr[$i][$j] = array("DeviceID" => $devId, "AlertDefID" => $alertDef['id'], "Status" => 0, "CratedOn" => $logDate,"Value"=>$realPwr);
                            break;
                            
                        case "React Power":
                                if ( $this->checkAlerts($condition,$paramFromValue,$paramToValue,$reactPwr) )	$alertArr[$i][$j] = array("DeviceID" => $devId, "AlertDefID" => $alertDef['id'], "Status" => 0, "CratedOn" => $logDate,"Value"=>$reactPwr);
                            break;
                            
                        case "Power Factor":
                                if ( $this->checkAlerts($condition,$paramFromValue,$paramToValue,$powerfactr) )	$alertArr[$i][$j] = array("DeviceID" => $devId, "AlertDefID" => $alertDef['id'], "Status" => 0, "CratedOn" => $logDate,"Value"=>$powerfactr);
                            break;
                            
                        case "Current":
                                if ( $this->checkAlerts($condition,$paramFromValue,$paramToValue,$amps3Ph) ) $alertArr[$i][$j] = array("DeviceID" => $devId, "AlertDefID" => $alertDef['id'], "Status" => 0, "CratedOn" => $logDate,"Value" => $amps3Ph);
                            break;
                            
                        case "Voltage IMB":
                                if ( $this->checkAlerts($condition,$paramFromValue,$paramToValue,$voltImb) ) $alertArr[$i][$j] = array("DeviceID" => $devId, "AlertDefID" => $alertDef['id'], "Status" => 0, "CratedOn" => $logDate,"Value" => $voltImb);
                            break;
                            
                        case "Current IMB":
                                if ( $this->checkAlerts($condition,$paramFromValue,$paramToValue,$ampsImb) ) $alertArr[$i][$j] = array("DeviceID" => $devId, "AlertDefID" => $alertDef['id'], "Status" => 0, "CratedOn" => $logDate, "Value" => $ampsImb);
                            break;
                            
                        case "Voltage THD":
                                if ( $this->checkAlerts($condition,$paramFromValue,$paramToValue,$vthd) ) $alertArr[$i][$j] = array("DeviceID" => $devId, "AlertDefID" => $alertDef['id'], "Status" => 0, "CratedOn" => $logDate,"Value" => $vthd);
                            break;
                            
                        case "Current THD":
                                if ( $this->checkAlerts($condition,$paramFromValue,$paramToValue,$athd) ) $alertArr[$i][$j] = array("DeviceID" => $devId, "AlertDefID" => $alertDef['id'], "Status" => 0, "CratedOn" => $logDate, "Value" => $athd);
                            break;
                            
                        case "Internal Temperature":
                                if ( $this->checkAlerts($condition,$paramFromValue,$paramToValue,$tint) ) $alertArr[$i][$j] = array("DeviceID" => $devId, "AlertDefID" => $alertDef['id'], "Status" => 0, "CratedOn" => $logDate, "Value" => $tint);
                            break;
                            
                        case "Ambient Temperature":
                                if ( $this->checkAlerts($condition,$paramFromValue,$paramToValue,$tamp) ) $alertArr[$i][$j] = array("DeviceID" => $devId, "AlertDefID" => $alertDef['id'], "Status" => 0, "CratedOn" => $logDate, "Value" => $tamp);
                            break; 
                        default:
                            echo "No Parameter for Device";
                    }
                    $j++;
                }
                $i++;

            }
            print_r($alertArr);

            //echo sizeof($alertArr);
            $alertfieldsql = "INSERT INTO pmi_device_alerts (DeviceID, AlertDefID, Status, CratedOn,Value) VALUES ";
            $alertvaluesql = "";
            foreach($alertArr as $alertVal)
            {
                foreach($alertVal as $alert){
                    if($alertvaluesql != "") $alertvaluesql .= ",";
                    $alertvaluesql .= "('".$alert["DeviceID"]."','".$alert["AlertDefID"]."','".$alert["Status"]."','".$alert["CratedOn"]."','".$alert["Value"]."')";
                }
            }
            //insert alerts generated
            if($alertvaluesql!=""){
                $newalertsql = $alertfieldsql.$alertvaluesql;
                $addNewAlerts = $db->query($newalertsql);
            }
        }
        catch (\Exception $ex)
        {
            echo "exception is:" . $ex->getMessage() . "\n";
        } 
    }
    
    public function generateDevSum($deviceId,$resultHBValues,$db){
        $hour = date("H", strtotime($resultHBValues["LOGDATE"]));
        $where = array();
        $where["LogHour"]  =  $hour;
        $where["DeviceID"] = $deviceId;
        
        $devSumData["KWH"] = $resultHBValues["KWH_24"];
        $devSumData["KVA"] = $resultHBValues["KVA_24"];
        $devSumData["KVAR"] = $resultHBValues["KVAR_24"];
        
        $updateDeviceSum = $db->update_query($devSumData , 'pmi_device_sum',$where);
    }

}


?>
