<?php
include "config.php";
//include_once("tpl/header.tpl.php");
include_once("tpl/dashboard-top.tpl.php"); 
?>
<div class="content clearfix">
    <?php include_once("tpl/leftTreeViewPanel.tpl.php"); ?>
    <!-- START PANEL -->
    <div class="content-center ">

        <div class="panel panel-transparent">
            <div class="panel-heading ">
                <div class="panel-title"></div>
            </div>
            <div class="panel-body">
                <div class="col-sm-3"></div>
                <div class="col-sm-6">
                    <div class="row">
                        <div id="portlet-advance " class="panel panel-default  panel-savings">
                            <div class="panel-heading p-t-15 bold fs-16 upper b-b b-grey">
                                Create Event
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group required " id="divName">
                                            <label>Event Name</label>
                                            <input type="text" class="form-control" id="txtOverrideName" required="" placeholder="Enter Event name"  onkeyup="removeError('divName');">
                                            <span class="text-danger sp_error hide"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">  
                                    <div class=" col-md-12 ">
                                        <div class="form-group required" id="divswitch">
                                            <label class="">Wall Switch</label>
                                            <select class="full-width" id="selswitch" data-placeholder="Select foramte" data-init-plugin="select2" onchange="return selectValidation('selswitch', 'switch', 'divswitch');">
                                                
                                            </select>
                                            <span class="text-danger sp_error hide"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group required" id="divSource">
                                            <label class="">Trigger Source</label>
                                            <select class="full-width" id="selSource" data-placeholder="Select foramte" data-init-plugin="select2" onchange="return selectValidation('selSource', 'source', 'divSource');">
                                                <option value="-1">select a source</option>
                                                <option value="1">Scene 1</option>
                                                <option value="2">Scene 2</option>
                                                <option value="3">Scene 3</option>
                                                <option value="4">ON Button</option>
                                            </select>
                                            <span class="text-danger sp_error hide"></span>
                                        </div>
                                    </div>
                                </div>                                   

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group required" id="divPolicy">
                                            <label class="">Policy</label>
                                            <select class="full-width" id="selPolicy" data-placeholder="Select foramte" data-init-plugin="select2" onchange="return selectValidation('selPolicy', 'policy', 'divPolicy');">

                                            </select>
                                            <span class="text-danger sp_error hide"></span>
                                        </div>
                                    </div>
                                </div>   
                                <div class="row">
                                    <div class="col-md-12 text-right m-b-10">
                                        <a href="javascript:void(0);" class="bold" onclick="$overridePolicy.addPolicy();">add policy ?</a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group required" id="divCluster">
                                            <label class="">Cluster</label>
                                            <select class="full-width" id="selCluster" data-placeholder="Select Cluster" data-init-plugin="select2" multiple onchange="return multiSelValidation('selCluster', 'cluster', 'divCluster');">

                                            </select>
                                            <span class="text-danger sp_error hide"></span>
                                        </div>
                                    </div>
                                </div>   
                            </div>
                            <input  type="hidden" id="hdnOverridePolicyID" name="hdnOverridePolicyID" value="">
                            <div class="row text-center">
                                <div class="col-md-12 m-b-15">
                                    <button type="button" class="btn btn-cons btn-success btn-sm" onclick="return $overridePolicy.save();">Save</button>
                                    <button type="button" class="btn btn-cons btn-danger btn-sm" onclick='return $overridePolicy.remove();'>Remove</button>
                                    <button type="button" class="btn btn-cons btn-default btn-sm " onclick='return $overridePolicy.clear();'>Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row hide" id="policyFilterContainer">
                <div class="col-md-12 text-center">
                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-8 m-t-10 filterTitle" >
                            <h5 class="bold"> Showing override policy for cluster : <span id="lblCluster"></span></h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 m-t-5">
                            <a href="javascript:void(0);" onclick="return $overridePolicy.getAll();" class="bold"> <i class="fa fa-arrow-left" aria-hidden="true"></i> Back to default</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <!-- START POLICY CREATION MODAL -->
    <div class="modal fade slide-up disable-scroll" data-backdrop="static" id="modalPolicy" tabindex="-1" role="dialog" aria-hidden="false">
        <div class="modal-dialog">
            <div class="modal-content-wrapper">
                <div class="modal-content mdal-custom">
                    <div class="modal-header clearfix text-left bold fs-16">
                        <i class="fa fa-cog p-r-10"></i>Policy Creation
                        <button type="button" class="close" onclick="$overridePolicy.policyClose();">&times;</button>
                    </div>
                    <div class="modal-body p-b-5 b-b b-t b-grey">
                        <!-- START POLICY CREATION CONTAINER -->
                        <?php include_once("tpl/policyContent.tpl.php"); ?>
                        <!-- END POLICY CREATION CONTAINER -->
                    </div>
                </div>
            </div>
        <!-- /.modal-content -->
        </div>
    </div>
    <!-- END POLICY CREATION MODAL -->
    
    </div>
    <?php include_once("tpl/rightTreeViewPanel.tpl.php"); ?>
</div>
<!-- END PANEL -->

<?php include_once("tpl/footer.tpl.php"); ?>

<script type="text/javascript">
    var loggedUserId = <?php echo $loggedInUserId; ?>;
    var loggedUserRole = <?php echo $loggedUserRoleId; ?>;
    $overrideStatus = 1;
    $(document).ready(function () {
        $ClusterTree.init(0);
        $('[data-toggle="tooltip"]').tooltip();
        $overridePolicy.init();
    });
    var $overridePolicy = {
        init:function(){
            $overridePolicy.getAllCluster("#selCluster",0);
            //$ClusterTree.getAllCluster("#selCluster");
            $overridePolicy.getAllPolicy();
            $overridePolicy.getTag();
            $overridePolicy.getAll();
        },
        getAllPolicy: function ($clusterID) {

            var $data = {
                c : 'LocationSettings',
                a : 'GetAllPolicy',
                policyType : 3 //Override policy
            };
            if (typeof $clusterID != "undefined") $data["clusterID"] = $clusterID;
            $.ajax({
                type: 'POST',
                url: 'ajax.php',
                data: $data,
                dataType: 'json',
                beforeSend: function () {
                    __showLoadingAnimation();
                },
                success: function (res) {
                //console.log(res);
                if (res.status == 1) {

                    var $resultData = res.data;
                            var $len = $resultData.length;
                            //console.log($len);
                    if ($len > 0) {
                        var $optionHtml = "<option value='-1'>Select a policy</option>";
                        for (var i = 0; i < $len; i++) {
                            $optionHtml += '<option value="' + $resultData[i]["ID"] + '">' + $resultData[i]["Name"] + '</option>';
                        }
                        $("#selPolicy").html($optionHtml);
                        $("#selPolicy").select2();
                    }

                }
                else{
                    $("#selPolicy").html("");
                    $("#selPolicy").select2();
                }
                },
                complete: function () {
                __hideLoadingAnimation();
                },
                error: function (r) {
                __hideLoadingAnimation();
                        console.log(r);
                }
            });
        },
        getTag: function () {

            var $data = {
                c : 'LocationSettings',
                a : 'getTagByTagID',
                type : 4
            };
            $.ajax({
                type: 'POST',
                url: 'ajax.php',
                data: $data,
                dataType: 'json',
                beforeSend: function () {
                    __showLoadingAnimation();
                },
                success: function (res) {
                //console.log(res);
                    if (res.status == 1) {

                        var $resultData = res.data;
                        var $len = $resultData.length;
                                //console.log($len);
                        if ($len > 0) {
                            var $optionHtml = "<option value='-1'>Please select a prepheral Device</option>";
                            for (var i = 0; i < $len; i++) {
                                $optionHtml += '<option value="' + $resultData[i]["ID"] + '">' + $resultData[i]["Name"] + '</option>';
                            }
                            $("#selswitch").html($optionHtml);
                            $("#selswitch").select2().on('change', function (e) {  $overridePolicy.getAllCluster("#selCluster",$("#selswitch").val()); });
                        }

                    }
                    else{
                        $("#selswitch").html("");
                        $("#selswitch").select2().on('change', function (e) {  $overridePolicy.getAllCluster("#selCluster",$("#selswitch").val()); });
                    }
                },
                complete: function () {
                __hideLoadingAnimation();
                },
                error: function (r) {
                __hideLoadingAnimation();
                        console.log(r);
                }
            });
        },
        save:function(){
            var $overridePolicyID = $("#hdnOverridePolicyID").val();
            var $name = $("#txtOverrideName").val();
            var $switchId = $("#selswitch").val();
            var $sourceId = $("#selSource").val();
            var $policyId = $("#selPolicy").val();
            var $clusterId = $("#selCluster").val();
            if($name == ""){
                $("#divName").addClass("has-error");
                $("#divName .sp_error").html("Please enter name.");
                $("#divName .sp_error").removeClass("hide");
                $("#txtOverrideName").focus();
                return false;
            }
            if($switchId == "" || $switchId == "-1"){
                $("#divswitch").addClass("has-error");
                $("#divswitch .sp_error").html("Please select a prepheral Device.");
                $("#divswitch .sp_error").removeClass("hide");
                $("#selswitch").focus();
                return false;
            }
            
            if($sourceId == "" || $sourceId == "-1"){
                $("#divSource").addClass("has-error");
                $("#divSource .sp_error").html("Please select a override source.");
                $("#divSource .sp_error").removeClass("hide");
                $("#selSource").focus();
                return false;
            }
            if($policyId == "" || $policyId == "-1"){
                $("#divPolicy").addClass("has-error");
                $("#divPolicy .sp_error").html("Please select a policy.");
                $("#divPolicy .sp_error").removeClass("hide");
                $("#selPolicy").focus();
                return false;
            }
            if($clusterId == null || $clusterId == "-1"){
                $("#divCluster").addClass("has-error");
                $("#divCluster .sp_error").html("Please select a cluster.");
                $("#divCluster .sp_error").removeClass("hide");
                $("#selCluster").focus();
                return false;
            }
            
            var $data = {
                c          : 'LocationSettings',
                a          : 'insertOrUpdateOverriderPolicy',
                name       : $name,
                switchId   : $switchId,
                sourceId   : $sourceId,
                policyId   : $policyId,
                clusterId  : $clusterId
            };  
            
            if($overridePolicyID != "") $data["id"] = $overridePolicyID;
            
            $.ajax({
            type: 'POST',
            url: 'ajax.php',
            data: $data,
            dataType: 'json',
            beforeSend: function () {
                __showLoadingAnimation();
            },
            success: function (res) {
                var $component = res.component;
                if ($component != '') {
                    $createAlert({status: "success", title: "Successfully Queued", text: "Successfully request queued to save override"});
                    $overridePolicy.clear();
                    $overridePolicy.getAll();
                }
                else {
                    $createAlert({status: "fail", title: "Failed Queue", text: "Failed to save override"});
                }
            },
            complete: function () {
                __hideLoadingAnimation();
            },
            error: function (r) {
                __hideLoadingAnimation();
                $createAlert({status: "fail", title: "Failed", text: r});
            }
        });
            
        },
        getAll: function ($clusterID) {
        
            var $data = {
                c : 'LocationSettings',
                a : 'GetAllOverridePolicy'
            };
            if (typeof $clusterID != "undefined"){
                $data["clusterID"] = $clusterID;
                $("#policyFilterContainer").removeClass("hide");

            }
            else {
                $("#policyFilterContainer").addClass("hide");
                //$overridePolicy.clear();
            }
            

            $.ajax({
                type: 'POST',
                url: 'ajax.php',
                data: $data,
                dataType: 'json',
                beforeSend: function () {
                    __showLoadingAnimation();
                },
                success: function (res) {
                    //console.log(res);
                    if (res.status == 1) {

                        var $resultData = res.data;
                        var $len = $resultData.length;
                        //console.log($len);
                        if ($len > 0) {
                            var $htmlStr = "";
                            for (var i = 0; i < $len; i++) {
                                var $eventHtml = typeof $clusterID == "undefined" ? ' onclick="return $overridePolicy.edit(' + $resultData[i]["ID"] + ')"' : "";
                                $htmlStr += '<a href="javascript:void(0);" id="policy_widget_' + $resultData[i]["ID"] + '" '+$eventHtml+'>';
                                $htmlStr += '    <div class="policy-list">';
                                $htmlStr += '       <div class="full-width clearfix">'
                                $htmlStr += '        <span>' + $resultData[i]["Name"] + '</span>';
                                $htmlStr += '       </div>'
                                $htmlStr += '       <div class="full-width clearfix">'
                                $htmlStr += '        <span class="pull-left fs-10 overrider-cluster-item">C : ' + $resultData[i]["ClusterName"] + '</span>';
                                $htmlStr += '        <span class="pull-right fs-10 overrider-dev-item">D : ' + $resultData[i]["TagName"] + '</span>';
                                $htmlStr += '       </div>'
                                $htmlStr += '    </div>';
                                $htmlStr += '</a>';
                            }
                            $("#rightTreePanel").html($htmlStr);
                        }

                    }
                    else{
                        $("#rightTreePanel").html("<h5 class='text-center fs-14' >Override policy not available.<h5>");
                    }
                },
                complete: function () {
                    __hideLoadingAnimation();
                },
                error: function (r) {
                    __hideLoadingAnimation();
                    console.log(r);
                }
            });
        },
        edit: function ($id) {
            var $data = {
                c: 'LocationSettings',
                a: 'GetAllOverridePolicy',
                id: $id
            };
            $.ajax({
                type: 'POST',
                url: 'ajax.php',
                data: $data,
                dataType: 'json',
                beforeSend: function () {
                    __showLoadingAnimation();
                },
                success: function (res) {
                    //console.log(res);
                    if (res.status == 1) {
                        //console.log(res.data);
                        $overridePolicy.clear();
                        var $resultData = res.data[0];
                        var $id = $resultData.ID;
                        var $name = $resultData.Name;
                        var $switchID = $resultData.SwitchId;
                        var $sourceID = $resultData.SourceType;
                        var $policyID = $resultData.PolicyID;
                        var $clusterID = $resultData.ClusterID;
                        var $clusterArr = $clusterID.split(",");

                        $("#hdnOverridePolicyID").val($id);
                        $("#txtOverrideName").val($name).trigger("change");
                        $("#selswitch").val($switchID).trigger("change");
                        $("#selSource").val($sourceID).trigger("change");
                        $("#selPolicy").val($policyID).trigger("change");
                        $overridePolicy.getAllCluster("#selCluster",$switchID,$clusterArr);
                        //$("#selCluster").val($clusterArr).trigger("change");
                    }
                },
                complete: function () {
                    __hideLoadingAnimation();
                },
                error: function (r) {
                    __hideLoadingAnimation();
                    console.log(r);
                }
            });
        },
        clear: function () {
            $("#hdnOverridePolicyID").val("");
            $("#txtOverrideName").val("");
            $("#selswitch").select2("val", -1);
            $("#selSource").select2("val", -1);
            $("#selPolicy").select2("val", -1);
            $("#selCluster").select2("val", []);
        },
        remove: function () {
            var $id = $("#hdnOverridePolicyID").val();
            if ($id != "") {
                var $data = {
                    c: 'LocationSettings',
                    a: 'removeOverridePolicy',
                    id: $id
                };

                $.ajax({
                    type: 'POST',
                    url: 'ajax.php',
                    data: $data,
                    dataType: 'json',
                    beforeSend: function () {
                        __showLoadingAnimation();
                    },
                    success: function (res) {
                        var $component = res.component;
                        if ($component != '') {

                            $createAlert({status: "success", title: "Successfully Queued", text: "Successfully request queued to remove override policy"});
                            $overridePolicy.clear();
                            $("#policy_widget_" + $id).remove();
                        }
                        else {
                            $createAlert({status: "fail", title: "Failed Queue", text: 'Failed to queue override policy'});
                        }
                    },
                    complete: function () {
                        __hideLoadingAnimation();
                    },
                    error: function (r) {
                        __hideLoadingAnimation();
                        $createAlert({status: "fail", title: "Failed", text: r});
                    }
                });
            }
            else {
                $createAlert({status: "info", title: "Warring", text: "Select policy on policy panel"});
            }

        },
        addPolicy:function(){
            $Policy.int();
            $("#modalPolicy").modal("show");
        },
        policyClose:function(){
            $("#modalPolicy").modal("hide");
            $overridePolicy.getAllPolicy();
        },
        getAllCluster: function ($elm,$selTagID,$clusterIDs) {
            
        if($selTagID == 0){
            var $optionHtml = "<option value='-1'>Select a cluster</option>";
            $($elm).html($optionHtml);
            $($elm).select2();
            return false;
        }
        var $data = {
            'c': 'LocationSettings',
            a: 'getAllClusterByTagID',
            "tagID":$selTagID
        };
        $.ajax({
            type: 'POST',
            url: 'ajax.php',
            data: $data,
            dataType: 'json',
            beforeSend: function () {
            },
            success: function (res) {
                var $optionHtml = "<option value='-1'>Select a cluster</option>";
                if (res.status == 1) {
                    var $resultData = res.data;
                    var $len = $resultData.length;
                    if ($len > 0) {
                        for (var i = 0; i < $len; i++) {
                            var $selStr = "";
                            $optionHtml += '<option value="' + $resultData[i]["ID"] + '" '+$selStr+'>' + $resultData[i]["Name"] + '</option>';
                        }
                    }
                }
        
                $($elm).html($optionHtml);
                $($elm).select2();
                if(typeof $clusterIDs != "undefined"){
                    $($elm).val($clusterIDs).trigger("change");
                }
            },
            complete: function () {
            },
            error: function (r) {
                console.log(r);
            }
        });
    }
        
    };
</script>