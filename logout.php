<?php
	session_start();
	session_destroy();
	clearstatcache();
	session_unset();
	unset($_SESSION); 
	$_SESSION = array();
	header('cache-control: no-cache,no-store,must-revalidate'); // HTTP 1.1. header('pragma: no-cache'); // HTTP 1.0. header('expires: 0'); 
	header('Location: index.php');
?>