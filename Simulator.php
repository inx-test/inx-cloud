<?php
    require_once('config.php');
    require_once('UDP/PMIPacket.php');
    require_once('UDP/UDP.php');

    // UDP Helper Class
    $udp = new UDP();
    $nodeIp = "";
    $broadcastIp = "";
    // Post
    if( isset($_POST["_cmdbroadcast"]) && $_POST["_cmdbroadcast"] !=""  ){

        $cmd = $_POST["_selcmd"];
        $nodeIp = $_POST["_nodeip"];
        $broadcastIp = $_POST["_networkip"];
        $tagId = $_POST["_tagid"];
	$broadcastIp = shell_exec("ifconfig | awk '/broadcast / {print $6}'");
        // Check event, ack or request

        // Tag Requst
        if( strpos($cmd, "-") !== false ){

          // Sent a dummy NODE TAG REQUEST
          $tagReqPacket = new PMIPacket();
          $tagReqPacket->preamble = 255; // Optional
          $tagReqPacket->startByte = 85; // Optional
          $tagReqPacket->packetId = rand(5,255); // Get Incremental Packet ID - Mandatory
          $tagReqPacket->tagIdHighByte = 0; // Optional for this case & SET_TAG
          $tagReqPacket->tagIdLowByte = 0; // Optional for this case & SET_TAG

          //Dummy
          list( $commad, $serialNumber, $fixtureType ) = explode( "-", $cmd );
          list( $ip1, $ip2, $ip3, $ip4 ) = explode( ".", $nodeIp );

          $tagReqPacketBytes = array(
              PT_REQUESTS, // This is a Request
              $commad, // Packet Type
              $ip1,
              $ip2,
              $ip3,
              $ip4,
              0,
              0,
              $udp->getHiByte($serialNumber),
              $udp->getLoByte($serialNumber),
              $fixtureType,
              1
          );
          // End Dummy

          $tagReqPacket->numberOfBytes = sizeof($tagReqPacketBytes); // Bytes to follow Packet Id: 1, Packet Command: 1, 4 IP, 4 Serial, 1 Fixture Type, 1 Number of Tag

          // Set Bytes
          $tagReqPacket->commandParam = $tagReqPacketBytes;


          // Send
          $udp->send( $broadcastIp,INX_LISTENER_PORT,$tagReqPacket );
        }
		// Status
        else if( strpos($cmd, "$") !== false ){
			
		$statusPacket = new PMIPacket();

		$statusPacket->preamble = 255; // Optional
		$statusPacket->startByte = 85; // Optional
		$statusPacket->packetId = rand(5, 255); // Get Incremental Packet ID - Mandatory
		$statusPacket->tagIdHighByte = 255;
		$statusPacket->tagIdLowByte = 255;

		$statusPacketBytes = array(PT_STATUS,GET_STATUS);

		$statusPacket->numberOfBytes = sizeof($statusPacketBytes);

		// Set Bytes
		$statusPacket->commandParam = $statusPacketBytes;

		echo $statusPacket->getDecimal();
		// Send
		$status = $udp->broadcast($broadcastIp, $statusPacket, NODE_LISTENER_PORT);

        }

        // Event
        else if( strpos($cmd, "#") !== false ){
          $eventPacket = new PMIPacket();
          $eventPacket->preamble = 255; // Optional
          $eventPacket->startByte = 85; // Optional
          $eventPacket->packetId = rand(5,255); // Get Incremental Packet ID - Mandatory
          $eventPacket->tagIdHighByte = 255;//$udp->getHiByte($tagId);
          $eventPacket->tagIdLowByte = 255;//$udp->getLoByte($tagId);

          //Dummy
          list( $commad ) = explode( "#", $cmd );
          $parameter1 = 0;
          $parameter2 = 0;
          if( $commad ==  EV_MOTION ){
              $parameter1 = 0;
              $parameter2 = 0;
          }

          $eventPacketBytes = array(
              PT_EVENTS, // This is a Event
              $commad, // Packet Type
              //$parameter1,
              //$parameter2,
          );
          // End Dummy

          $eventPacket->numberOfBytes = sizeof($eventPacketBytes);

          // Set Bytes
          $eventPacket->commandParam = $eventPacketBytes;

          // Send
          //$udp->send( $broadcastIp,INX_LISTENER_PORT,$eventPacket );
		  $udp->broadcast($broadcastIp, $eventPacket, NODE_LISTENER_PORT);
        }

        // Ack
        else {
            // TODO:
            $ackTagPacket = new PMIPacket();
            $ackTagPacket->preamble = 255; // Optional
            $ackTagPacket->startByte = 85; // Optional
            $ackTagPacket->packetId = rand(5,255); // Get Incremental Packet ID - Mandatory
            $ackTagPacket->tagIdHighByte = $udp->getHiByte($tagId);
            $ackTagPacket->tagIdLowByte = $udp->getLoByte($tagId);

            //Dummy
            $commad = $cmd;

            $ackTagPacketBytes = array(
                PT_ACK_SHORT, // This is a Shor ACK ( ? )
                $commad // Packet Type 199
            );
            // End Dummy

            $ackTagPacket->numberOfBytes = sizeof($ackTagPacketBytes);

            // Set Bytes
            $ackTagPacket->commandParam = $ackTagPacketBytes;

            // Send
            $udp->send( $broadcastIp,INX_LISTENER_PORT,$ackTagPacket );
        }
    }


?>
<form method="post">
   <table width="60%" cellpadding="5" cellspacing="5">
     <tr>
         <td colspan="2">Broadcast Addr ( Use Same Node IP for Now ): <input type="text" name="_networkip" maxlenth="20" value="<?php echo $broadcastIp;?>"/></td>
     </tr>
      <tr>
          <td colspan="2">Node IP: <input type="text" name="_nodeip" maxlenth="20" value="<?php echo $nodeIp;?>"/></td>
      </tr>
      <tr>
        <td height="30"></td>
      </tr>
      <tr>
          <td width="50%">
              <p>Select a Tag Request or Event to Broadcast: </p>
              <select name='_selcmd' style="width:90%;">
                  <option value=""  selected="">Select one...</option>
                  <option value="" disabled="disabled">#SELECT TAG REQUEST</option>

                  <!-- PacketConfig.php > NODE_TAG_REQUEST - Dummy Serial Number -->
                  <option value="<?php echo NODE_TAG_REQUEST;?>-12345-<?php echo FIXTURE_TYPE_NODE;?>">Node Tag</option>
                  <option value="<?php echo NODE_TAG_REQUEST;?>-12346-<?php echo FIXTURE_TYPE_DRIVERA;?>">Driver A</option>
                  <option value="<?php echo NODE_TAG_REQUEST;?>-12347-<?php echo FIXTURE_TYPE_DRIVERB;?>">Driver B</option>
                  <option value="<?php echo NODE_TAG_REQUEST;?>-12348-<?php echo FIXTURE_TYPE_WALLSWITCH;?>">Wall Switch</option>
                  <option value="<?php echo NODE_TAG_REQUEST;?>-12349-<?php echo FIXTURE_TYPE_OCCUPANCYSENSOR;?>">Occupancy Sensor</option>
				<!------------------------ACK--------------------------->
                  <option value="" disabled="disabled"></option>
                  <option value="" disabled="disabled">#SELECT ACK</option>
                  <option value="<?php echo PT_ACK_SHORT;?>">Set Tag Ack</option>
				  
					<!------------------------Status--------------------------->
                  <option value="" disabled="disabled"></option>
                  <option value="" disabled="disabled">#STATUS</option>
                  <option value="<?php echo GET_STATUS;?>$">GET_STATUS</option>
				  
				  <!------------------------Event--------------------------->
                  <option value="" disabled="disabled"></option>
                  <option value="" disabled="disabled">#SELECT EVENT</option>
                  <option value="<?php echo MOTION_DETECTED;?>#">OS - MOTION_DETECTED</option>
                  <option value="<?php echo OVERRIDE_1;?>#">WS - OVERRIDE_1</option>
                  <option value="<?php echo OVERRIDE_2;?>#">WS - OVERRIDE_2</option>
                  <option value="<?php echo OVERRIDE_3;?>#">WS - OVERRIDE_3</option>
                  <option value="<?php echo UP_BUTTON;?>#">WS - UP_BUTTON</option>
                  <option value="<?php echo DOWN_BUTTON;?>#">WS - DOWN_BUTTON</option>
                  <option value="<?php echo ON_BUTTON;?>#">WS - ON_BUTTON</option>
                  <option value="<?php echo CANCEL_BUTTON;?>#">WS - CANCEL_BUTTON</option>


              </select>
          </td>

      </tr>
      <tr>
          <td colspan="2">
            <p>Enter database Tag ID here if you use Events or ACK</p>
            <input type="text" name="_tagid" maxlenth="20" value=""/>
          </td>
      </tr>
      <tr>
        <td colspan="2">
            <input type="submit" name="_cmdbroadcast" value=" Broadcast ">
        </td>
      </tr>
   </table>
 </form>
