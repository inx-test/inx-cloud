<?php 
include "config.php";
include_once("tpl/header.tpl.php");  

$deviceId=0;
if(isset($_SESSION["activeDeviceID"])){
    $deviceId = $_SESSION["activeDeviceID"];
    $activeDeviceIP = $_SESSION["activeDeviceIDAddress"];
}
?>
   <div class="content">
      <!-- START PANEL -->
       <div class="panel panel-transparent">
         <div class="panel-heading ">
           <div class="panel-title">Parameters</div>
         </div>
            <div class="panel-body">
                <div class="row">
                <div class='col-sm-4 p-r-5'>
                    <div id="portlet-advance " class="panel panel-default panel-savings m-b-10">
                        <div class="panel-body" style="height: 485px;">
                            <div  class='row'>
                                <div class="col-sm-12">
                                    <div class="form-group col-sm-6 form-group-default m-b-5">
                                        <label class="fs-10">Model Number</label>
                                        <input type="text" class="form-control" readonly value='' id="txtModel" placeholder="Model Number" style="color: rgb(71, 71, 71) !important;" />
                                    </div>
                                    
                                    <div class="form-group col-sm-6  form-group-default form-group-default-select2 m-b-5" id="divInst">
                                        <label class="fs-10">Installation format</label>
                                        <select name="" data-init-plugin="select2" class="full-width" id="txtInstallation" onchange="return selectValidation('txtInstallation','installation format','divInst','divErrorContainer');">
                                            <option value="-1" selected>Select format</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-6 form-group-default form-group-default-select2 m-b-5" id="divType">
                                        <label class="fs-10">Panel cable Size/Type</label>
                                        <select name="" data-init-plugin="select2" class="full-width" id="txtCableSize" onchange="return selectValidation('txtCableSize','cable size/type','divType','divErrorContainer')">
                                                <option value="-1" selected>Select cable type</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-6 form-group-default form-group-default-select2 m-b-5" id="divMaterial">
                                        <label class="fs-10">Cable material</label>
                                        <select name="" data-init-plugin="select2" class="full-width" id="txtCableMaterial" onchange="return selectValidation('txtCableMaterial','cable materia','divMaterial','divErrorContainer')">
                                                <option value="-1" selected>Select material</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-6 form-group-default form-group-default-select2 m-b-5" id="divStartup">
                                        <label class="fs-10">Default Startup</label>
                                        <select name="" data-init-plugin="select2" class="full-width" id="txtStartUp" onchange="return selectValidation('txtStartUp','default Startup','divStartup','divErrorContainer')">
                                                <option value="-1" selected>Select startup</option>
                                                <option value="1">Banks On</option>
                                                <option value="2">Banks Off</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-6 form-group-default form-group-default-select2 m-b-5" id="divCalibration">
                                        <label class="fs-10">Selected Calibration</label>
                                        <select name="" data-init-plugin="select2" class="full-width" id="txtCalibration" onchange="return selectValidation('txtCalibration','calibration','divCalibration','divErrorContainer')">
                                            <option value="-1" selected>Select calibration</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-6 form-group-default form-group-default-select2 m-b-5" id="divTime">
                                        <label class="fs-10">Select Response Period</label>
                                        <select name="" data-init-plugin="select2" class="full-width" id="txtResponsePeriod" onchange="return selectValidation('txtResponsePeriod','period','divTime','divErrorContainer')">
                                                <option value="-1" selected>Select period</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-6 form-group-default form-group-default-select2 m-b-5" id="divUnit">
                                        <label class="fs-10"># of parallel Systems</label>
                                        <select name="" data-init-plugin="select2" class="full-width"  id="txtParallelSys" onchange="return selectValidation('txtParallelSys','unit','divUnit','divErrorContainer')">
                                                <option value="-1" selected>Select unit</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-6 form-group-default m-b-5" id="divMultipler">
                                        <label class="fs-10">Multipler </label>
                                        <input type="text" class="form-control panel-parameter" value='' id="txtMultipler" onblur="return oprationValidation();" onkeyup="return removeErrorAlert();" placeholder="Please enter value"/>
                                    </div>
                                    <div class="form-group col-sm-6 form-group-default form-group-default-select2 m-b-5" id="divCapcitorFormat">
                                        <label class="fs-10">Capacitor Format</label>
                                        <select name="" data-init-plugin="select2" class="full-width" id="sel-moving-avg">
                                            <option value="-1">Select Capacitor format</option>
                                            <option value="1">Average</option> 
                                             <option value="2">Individual</option>
                                        </select>
                                    </div>
                                    <input type="hidden" id="activeDeviceId" value="<?php echo $deviceId; ?>" />
                                    <input type="hidden" id="activeDeviceIP" value="<?php echo $activeDeviceIP; ?>" />
                                    <input type="hidden" id="bankSwitchType" value="" />
                                    <!--<input type="hidden" id="deviceBankStatus" value="0" deviceid=""/>-->
                                    <input type="hidden" id="resetMemory" value="0" />
                                    <div class="blockLoader hide" id="parameterLoader">
                                            <div class="loading">
                                                    <div class="loading-bar"></div>
                                                    <div class="loading-bar"></div>
                                                    <div class="loading-bar"></div>
                                                    <div class="loading-bar"></div>
                                            </div>
                                    </div>
                                    
                                    <div class="blockError hide" id="datanotAvailable">
                                        <span><i class="fa fa-exclamation-circle fa-2x"></i> No data available, </span>
                                        <a href="javascript:void(0);" onclick="return loadPanelParameter();" 
                                           class="">Please try again</a>
                                           
                                    </div>
                                    <?php 
                                    if(  $loggedUserRoleId == ADMIN_ROLE || $loggedUserRoleId == ASSISTANT_ROLE) 
                                    { 
                                    ?>
                                        <div class="pull-left m-t-5  m-r-10">
                                            <div class="checkbox check-complete m-t-0 m-b-0">
                                                <input type="checkbox" id="checklock" onchange="return locakParameter();">
                                                <label for="checklock" class=" bold"> Lock Params</label>
                                            </div>
                                        </div>
                                        <div class="pull-right m-t-5">
                                            <button type="button" class="btn btn-tag btn-success btn-tag-rounded" id="btnApply" onclick="return applyParameters();"> Apply </button>
                                        </div>
                                    <?php
                                            }
                                    ?>
                                    
                                    <div class="clearfix col-sm-12 p-l-0 p-r-0" id="divErrorContainer">
                                        <span class="text-danger sp_error  bold hide"></span>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="clearfix col-sm-12 p-t-10">
                                <span class="t-s-1 bold" id="txtBankStatus"></span>
                            </div>
                            <?php if(  $loggedUserRoleId == ADMIN_ROLE || $loggedUserRoleId == ASSISTANT_ROLE){?>
                                <div class="row">
                                    <div class='switch-buttons m-t-10 text-center m-l-10'>
                                        <a href='javascript:void(0);' class="switch-red pull-left first hide" onclick="bankswitch(0)">
                                            <i class="fa fa-power-off"></i> Turn off banks
                                        </a>
                                        <a class='switch-green pull-left bold first' href='javascript:void(0);' onclick="bankswitch(1)">
                                          <i class="fa fa-power-off"></i> Turn on banks
                                        </a>
                                        
                                        <a href='javascript:void(0);' class="switch-orange pull-left"onclick="setDateTimeConfirm()">
                                            <i class="fa fa-calendar"></i> Set Date and time
                                        </a>
                                        <a href='javascript:void(0);' class="switch-yellow pull-left last" onclick="resetMemory();"> 
                                            <i class="fa fa-refresh"></i> Reset Memory
                                        </a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class='switch-buttons m-t-10 text-center'>
                                    </div>
                                </div>
                            <?php }?>
                        </div>
                    </div>
                </div>
                <div class='col-sm-8 p-l-5'>
                    <div id="portlet-scopeChart " class="panel panel-default panel-savings m-b-10">
                        <div class="panel-body" style="height: 485px;">
                            <div class="col-sm-2 padding-0">
                                <div class='chart-param col-sm-12 btn-indic p-b-0'>
                                    <ul class="scope-chart">
                                        <li class='p-l-0'>
                                            <div class="checkbox check-primary m-t-0 m-b-0">
                                                <input type="checkbox" id="checkShowph1" onclick="return changeScopeCheck(this);">
                                                <label for="checkShowph1">Show Ph1</label>
                                            </div>
                                        </li>
                                        <li class='p-l-0'>
                                            <div class="checkbox check-primary m-t-0 m-b-0">
                                                <input type="checkbox" id="checkShowph2" onclick="return changeScopeCheck(this);" >
                                                <label for="checkShowph2">Show Ph2</label>
                                            </div>
                                        </li>
                                        <li class='p-l-0'>
                                            <div class="checkbox check-primary m-t-0 m-b-0">
                                                <input type="checkbox" id="checkShowph3" onclick="return changeScopeCheck(this);" >
                                                <label for="checkShowph3">Show Ph3</label>
                                            </div>
                                        </li>
                                        <li class="p-t-0 text-center">
                                            <button type="button" class="btn btn-tag  btn-complete btn-tag-rounded" onclick="return captureChartInt();">
                                                Capture
                                            </button>
                                        </li>
                                        <li class='p-l-0'>
                                            <div class="checkbox check-primary m-t-0 m-b-0">
                                                <input type="checkbox" id="checkShowKVA" onclick="return changeScopeCheck(this);">
                                                <label for="checkShowKVA">Show KVA</label>
                                            </div>
                                        </li>
                                        <li class='p-l-0'>
                                            <div class="checkbox check-primary m-t-0 m-b-0">
                                                <input type="checkbox" id="checkShowActual" onclick="return changeScopeCheck(this);">
                                                <label for="checkShowActual">Show Actual</label>
                                            </div>
                                        </li>
                                        <!--li class='p-l-0' id="li-check-ideal">
                                            <div class="checkbox check-primary  m-t-0 m-b-0">
                                                <input type="checkbox" id="checkShowIdeal" onclick="return changeScopeCheck(this);">
                                                <label for="checkShowIdeal">Show Ideal</label>
                                            </div>
                                        </li-->
                                        <li class="p-l-0 text-center clearfix hide" id="li-Actual" style="border-bottom: none !important;">
                                            <ul>
                                                <li class="p-l-0 text-center clearfix hide" id="lblPh1V">
                                                    <span class="notify-bubble pull-right m-l-5 hide" id="spIdealph1V" data-placement="right" data-toggle="tooltip" data-trigger="hover" data-original-title="Ideal" ></span>
                                                    <span class="notify-bubble pull-right hide" id="spActph1V" data-placement="right" data-toggle="tooltip" data-trigger="hover" data-original-title="Actual" ></span>
                                                    <span class="fs-14 semi-bold pull-left"> Ph1 V</span>
                                                </li>
                                                <li class="p-l-0 text-center clearfix hide" id="lblPh1A">
                                                    <span class="notify-bubble pull-right m-l-5 hide" id="spIdealph1A" data-placement="right" data-toggle="tooltip" data-trigger="hover" data-original-title="Ideal" ></span>
                                                    <span class="notify-bubble pull-right hide" id="spActph1A" data-placement="right" data-toggle="tooltip" data-trigger="hover" data-original-title="Actual" ></span>
                                                    <span class="fs-14 semi-bold pull-left"> Ph1 A</span>
                                                </li>
                                                <li class="p-l-0 text-center clearfix hide" id="lblPh2V">
                                                    <span class="notify-bubble pull-right m-l-5 hide" id="spIdealph2V" data-placement="right" data-toggle="tooltip" data-trigger="hover" data-original-title="Ideal" ></span>
                                                    <span class="notify-bubble pull-right hide" id="spActph2V" data-placement="right" data-toggle="tooltip" data-trigger="hover" data-original-title="Actual" ></span>
                                                    <span class="fs-14 semi-bold pull-left"> Ph2 V</span>
                                                </li>
                                                <li class="p-l-0 text-center clearfix hide" id="lblPh2A">
                                                    <span class="notify-bubble pull-right m-l-5 hide" id="spIdealph2A" data-placement="right" data-toggle="tooltip" data-trigger="hover" data-original-title="Ideal" ></span>
                                                    <span class="notify-bubble pull-right hide" id="spActph2A" data-placement="right" data-toggle="tooltip" data-trigger="hover" data-original-title="Actual" ></span>
                                                    <span class="fs-14 semi-bold pull-left"> Ph2 A</span>
                                                </li>
                                                <li class="p-l-0 text-center clearfix hide" id="lblPh3V">
                                                    <span class="notify-bubble pull-right m-l-5 hide" id="spIdealph3V" data-placement="right" data-toggle="tooltip" data-trigger="hover" data-original-title="Ideal" ></span>
                                                    <span class="notify-bubble pull-right hide" id="spActph3V" data-placement="right" data-toggle="tooltip" data-trigger="hover" data-original-title="Actual" ></span>
                                                    <span class="fs-14 semi-bold pull-left"> Ph3 V</span>
                                                </li>
                                                <li class="p-l-0 text-center clearfix hide" style="border:none !important;" id="lblPh3A">
                                                    <span class="notify-bubble pull-right m-l-5 hide"  data-placement="right" data-toggle="tooltip" data-trigger="hover" data-original-title="Ideal" id="spIdealph3A"></span>
                                                    <span class="notify-bubble pull-right hide" id="spActph3A" data-placement="right" data-toggle="tooltip" data-trigger="hover" data-original-title="Actual" ></span>
                                                    <span class="fs-14 semi-bold pull-left"> Ph3 A</span>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="p-l-0 text-center clearfix hide" id="li-KVA" style="border-bottom: none !important;">
                                            <ul>
                                                <li class="p-l-0 text-center clearfix hide" id="lblPh1P">
                                                    <span class="notify-bubble pull-right m-l-5 hide" id="spKVAph1V"></span>
                                                    <span class="fs-14 semi-bold pull-left"> Ph1 P</span>
                                                </li>
                                                <li class="p-l-0 text-center clearfix hide" id="lblPh2P">
                                                    <span class="notify-bubble pull-right m-l-5 hide" id="spKVAph2V"></span>
                                                    <span class="fs-14 semi-bold pull-left"> Ph2 P</span>
                                                </li>
                                                <li class="p-l-0 text-center clearfix hide" id="lblPh3P">
                                                    <span class="notify-bubble pull-right m-l-5 hide" id="spKVAph3V"></span>
                                                    <span class="fs-14 semi-bold pull-left"> Ph3 P</span>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-10 p-l-5 p-r-5">
				<div id="nvd3-Scope" class="line-chart text-center">
                                    <div class="scale-time hide">X Axis Scale : 0.833 ms/div</div>
                                    <div id="flot-Scope" style="width:100%;height:100%;margin:0 auto"></div>
                                    
                                    <!--div class="scale-v">Volts/Div:0.5 V</div-->
                                    
                                    <!--div class="scale-a">Amps/Div:0.5 A</div-->
                                    <div class="scope-nodata">     
                                        <strong>No Data Available.</strong>    
                                    </div>
                                </div>
                                <div class="pull-left">
                                    <div id="waitingTr" class="hide">
                                        <div class="bold pull-left p-r-15">Waiting for trigger</div>
                                        <div class="loading pull-left">
                                            <div class="loading-bar"></div>
                                            <div class="loading-bar"></div>
                                            <div class="loading-bar"></div>
                                            <div class="loading-bar"></div>
                                        </div>
                                    </div>
                                    <div id="completeTr" class="hide">
                                        <div class="bold pull-left p-r-15">3 Phase @ <span id="TrComplete-time">10:10:17</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
                                
                <div class="row">
                    <div class='col-sm-4 p-r-5'>
                        <div id="portlet-advance " class="panel panel-default panel-savings m-b-10">
                            <div class="panel-heading clearfix p-b-0 p-t-10" style="min-height: 35px !important;">
                                <div class="panel-title title-metal">
                                    Export log data
                                </div>
                            </div>
                            <div class="panel-body" style="height: 150px;">
                                <!--Export Data-->
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="row">
                                            <div class="col-sm-9">
                                                    <div class="form-group form-group-default input-group col-sm-12 m-b-5" id="divStart" style="float:left;">
                                                        <label>Start Date</label>
                                                        <input type="text" class="form-control"  name='startDate' id="startDate" onfocus="removeExportAlert('divStart');">
                                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                    </div>
                                                    <div class="form-group form-group-default input-group col-sm-12 m-b-5" id="divEnd" style="float:left;">
                                                        <label>End Date</label>
                                                        <input type="text" class="form-control"  name='endDate' id="endDate" onfocus="removeExportAlert('divEnd');">
                                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                    </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="pull-right">
                                                    <button type="button" class="btn btn-tag btn-success btn-tag-rounded btn-export" id="btnExport" onclick="generateExport();">Export</button>
                                                    <a href="" id="clickHidden" target="_blank" ></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix col-sm-8 p-l-0 p-r-0 m-t-5" id="divExportError">
                                            <span class="text-danger sp_error bold hide"></span>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <div id="portlet-advance " class="panel panel-default panel-savings m-b-10">
                            <div class="panel-heading clearfix p-b-0 p-t-10" style="min-height: 35px !important;">
                                <div class="panel-title title-metal">
                                    Record heartbeat data
                                </div>
                            </div>
                            <div class="panel-body" style="height: 98px;">
                                <!--Export Data-->
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group form-group-default input-group col-sm-6 m-b-5" style="float:left;">
                                            <label>Time Frame (sec)</label>
                                            <input type="text" class="form-control"  name='timeFrame' id="timeFrame" value="0" onblur="return checkTimeFrame(this.value);">
                                            <span class="input-group-addon p-l-0 p-r-0" id="timeChange" style="border-left: 1px solid rgba(0, 0, 0, 0.07);">
                                                <div id="up" class="p-b-5" style="border-bottom: 1px solid rgba(0, 0, 0, 0.07);"><i class="fa fa-arrow-up"></i></div>
                                                <div id="down" class="p-t-5"><i class="fa fa-arrow-down"></i></div>
                                            </span>
                                        </div>
                                        <div class="pull-right m-t-5 col-sm-6">
                                            <button type="button" class="btn btn-tag btn-success btn-play m-t-5" 
                                                     data-placement="top" data-toggle="tooltip" data-trigger="hover" title="" data-original-title="Start Record"
                                                    id="btnRecord" onclick="return startRecordData();"><i class="fa fa-play fa-lg"></i>
                                            </button>
                                            <button type="button" class="btn btn-tag btn-danger btn-stop m-t-5"
                                                     data-placement="top" data-toggle="tooltip" data-trigger="hover" title="" data-original-title="Stop Record"
                                                    id="btnStopRecord" onclick="stopRecordData();" disabled> <i class="fa fa-stop fa-lg"></i>
                                                
                                            </button>
                                            <a id="downloadFile" class="btn btn-tag btn-default btn-download m-t-5 hide"
                                               data-placement="top" data-toggle="tooltip" data-trigger="hover" title="" data-original-title="Download File"
                                               download="record.csv" href="#" onclick="hideLink();">
                                                <i class="fa fa-download fa-lg"></i>
                                            </a>
                                        </div>
                                        <div class="clearfix col-sm-8 p-l-0 p-r-0 m-t-5" id="divRecordError">
                                                <span class="text-danger sp_error bold hide"></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 hide" id="divStopRecord">
                                        <div class="col-sm-6 p-l-0">
                                            <div class="fs-20 p-t-5 pull-left bold" id="recordCounter">Recording Data </div>
                                            <div class="loading pull-left p-l-10 p-t-5"><div class="loading-bar"></div><div class="loading-bar"></div><div class="loading-bar"></div><div class="loading-bar"></div></div>
                                        </div>
                                        <div class="col-sm-6 p-l-0 p-r-0 timer" id="recordTimer">
                                            <span id="Countdown"></span>
                                        </div>
                                    </div>
                                    
                                    <input type="hidden" id="stopRecordFlag" value="0"/>
                                </div>
                            </div>
                        </div>
                    </div>
                                        
                    <div class='col-sm-8 p-l-5'>
                        <div id="portlet-scopeChart " class="panel panel-default panel-savings">
                            <div class="panel-body p-b-10">
                                <div class="col-sm-2 padding-0">
                                    <div class='chart-param col-sm-12 btn-indic p-b-0'>
                                        <ul class="harmonic-chart-list">
                                            <li class='p-l-0'>
                                                <div class="radio radio-primary m-t-0 m-b-0">
                                                    <input type="radio" value="voltPh1" name="radioHarmonic" id="voltPh1" onclick="return changeHarmonicType();">
                                                    <label for="voltPh1">Volt Ph1</label>
                                                </div>
                                            </li>
                                            <li class='p-l-0'>
                                                <div class="radio radio-primary m-t-0 m-b-0">
                                                    <input type="radio" value="voltPh2" name="radioHarmonic" id="voltPh2" onclick="return changeHarmonicType();">
                                                    <label for="voltPh2">Volt Ph2</label>
                                                </div>
                                            </li>
                                            <li class='p-l-0'>
                                                <div class="radio radio-primary m-t-0 m-b-0">
                                                    <input type="radio" value="voltPh3" name="radioHarmonic" id="voltPh3" onclick="return changeHarmonicType();">
                                                    <label for="voltPh3">Volt Ph3</label>
                                                </div>
                                            </li>
                                            <li class='p-l-0'>
                                                <div class="radio radio-primary m-t-0 m-b-0">
                                                    <input type="radio" value="ampsPh1" name="radioHarmonic" id="ampsPh1" onclick="return changeHarmonicType();">
                                                    <label for="ampsPh1">Amps Ph1</label>
                                                </div>
                                            </li>
                                            <li class='p-l-0'>
                                                <div class="radio radio-primary m-t-0 m-b-0">
                                                    <input type="radio" value="ampsPh2" name="radioHarmonic" id="ampsPh2" onclick="return changeHarmonicType();">
                                                    <label for="ampsPh2">Amps Ph2</label>
                                                </div>
                                            </li>
                                            <li class='p-l-0'>
                                                <div class="radio radio-primary m-t-0 m-b-0">
                                                    <input type="radio" value="ampsPh3" name="radioHarmonic" id="ampsPh3" onclick="return changeHarmonicType();">
                                                    <label for="ampsPh3">Amps Ph3</label>
                                                </div>
                                            </li>
                                            <li class="p-t-0 text-center">
                                                <button type="button" class="btn btn-tag btn-complete btn-tag-rounded p-l-10 p-r-10" id="btnHarmonicReset" onclick="return captureChartInt();">
                                                    Capture
                                                </button>
                                            </li>
                                            <li class='p-l-0'>
                                                <div class="checkbox check-primary m-t-0 m-b-0">
                                                    <input type="checkbox" id="checkShow1st" onclick="return changeHarmonicType();">
                                                    <label for="checkShow1st">Show 1st</label>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-sm-10 p-l-10 p-r-5">
                                    <div id="nvd3-HarmonicBar" class="text-center" data-x-grid="false">
                                        <svg></svg>
                                    </div>
                                    <div class="harmonic-nodata text-center">     
                                        <strong>No Data Available.</strong>    
                                    </div>
                                    <div class="pull-left">
                                        <div id="harmonic-loader" class="hide">
                                            <div class="bold pull-left p-r-15">Waiting for trigger</div>
                                            <div class="loading pull-left">
                                                <div class="loading-bar"></div>
                                                <div class="loading-bar"></div>
                                                <div class="loading-bar"></div>
                                                <div class="loading-bar"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="harmonic-notify text-right hide">
                                        <div class="bold fs-12 clearfix">
                                           <span id="txtTHDVal"></span> 
                                           <span id="txtPeakVal"></span>
                                        </div>
                                        <div class="bold fs-12 clearfix">
                                            <span id="txtHTime"></span> 
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <!-- END PANEL -->
   </div>
        
    <!-- MODAL CONFIRMATION ALERT -->
    <div class="modal fade slide-up disable-scroll" data-backdrop="static" id="modalCnfirmation" tabindex="-1" role="dialog" aria-hidden="false">
      <div class="modal-dialog" style="width: 300px;">
        <div class="modal-content-wrapper">
          <div class="modal-content mdal-custom">
            <div class="modal-body p-b-5">
                <p class="bold fs-20 p-t-10" id="cnCntent">
                    Do you want to bank off
                </p>
            </div>
            <div class="modal-footer text-right">
                <button type="button" class="btn btn-danger btn-sm pull-right" id="btnCnfmNo" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-success btn-sm pull-right m-r-10" id="btnCnfmYes" onclick="actionConfirm();">OK</button>
            </div>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
    </div>
    <!-- END CONFIRMATION ALERT -->
    
<?php include_once("tpl/footer.tpl.php"); ?>

<script type="text/javascript">
    var $scopeRecord = [],$harmonicRecord = [],$harmonicDatatime,$ParamsData ;
    
    var needParameterSet = true;
    $(document).ready(function(){
        needParameterSet = true;
        //$(".content").height($(document).height());
        $("#up").on('click',function(){
            if($("#timeFrame").val() < 600){
                $("#timeFrame").val(parseInt($("#timeFrame").val())+5);
                $("#divRecordError span").addClass("hide");
            }
            else{
                $("#divRecordError span").removeClass("hide");
                $("#divRecordError span").html("Maximum Time Frame reached");
                return false; 
            }
        });
        
        $("#down").on('click',function(){
            if($("#timeFrame").val() > 0){
                $("#timeFrame").val(parseInt($("#timeFrame").val())-5);
                $("#divRecordError span").addClass("hide");
            }
            else{
                $("#divRecordError span").removeClass("hide");
                $("#divRecordError span").html("Minimum Time Frame reached");
                return false; 
            }
        });
        
        $('.panel-parameter').keypress(function(e){
          if(e.keyCode==13){
            $('#btnApply').click(); 
            return false;
        }
        });
        loadPanelParameter();
        
        $("#startDate").datepicker(
            {   
                format: "mm/dd/yyyy",
                endDate: "1d",
                autoclose: true
            }
        ).on('changeDate', function (ev) {
            $('#endDate').datepicker("remove");
            var startDate = new Date($("#startDate").val());
            $("#endDate").datepicker(
                {  
                    format: "mm/dd/yyyy",
                    endDate: "1d",
                    autoclose: true,
                    startDate :startDate
                }
            );
        });
        $("#endDate").datepicker(
            {  
                format: "mm/dd/yyyy",
                endDate: "1d",
                autoclose: true//,
               // startDate :startDate
            }
        );
        /*$( "#txtMultipler" ).keypress(function(evt) {
            //console.log(this);
            var theEvent = evt || window.event;
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode( key );
            var regex =  /^(\+|-)?(\d*\.?\d*)$/;
            if( !regex.test(key) ) {
              theEvent.returnValue = false;
              if(theEvent.preventDefault) theEvent.preventDefault();
            }
            
            var text = $(this).val();
            if (text.indexOf('.') != -1 && text.substring(text.indexOf('.')).length > 2) {
                    event.preventDefault();
            } 
        });*/
        
        $( "#txtMultipler" ).keypress(function(evt) {
        	
            var theEvent = evt || window.event;
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode( key );
            keyCode = key;
            var oldValue = $( "#txtMultipler" ).val();
            oldMValue = oldValue;
            
            var regex =  /^(\+|-)?(\d*\.?\d*)$/;
            
            if( !regex.test(key) ) {
            	
            	theEvent.stopPropagation();
            	theEvent.preventDefault();  
            	theEvent.returnValue = false;
            	theEvent.cancelBubble = true;
                return false;
            }
            
            if(key == '.' && oldValue.indexOf('.') > -1){
            	theEvent.stopPropagation();
            	theEvent.preventDefault();  
            	theEvent.returnValue = false;
            	theEvent.cancelBubble = true;
                return false;
            }
            
            if((key == '-' ||  key == '+') && (oldValue.indexOf('-') > -1 || oldValue.indexOf('+') > -1)){
            	theEvent.stopPropagation();
            	theEvent.preventDefault();  
            	theEvent.returnValue = false;
            	theEvent.cancelBubble = true;
                return false;
            }
        });
        
        $( "#txtMultipler" ).keyup(function(evt) {
        	
            var theEvent = evt || window.event;
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode( key );
            key = keyCode;
            var newValue = $( "#txtMultipler" ).val(); 
            
            if(key == '.' && newValue.indexOf('.') == 0){
            	var afterVal = '0'+newValue;
            	$( "#txtMultipler" ).val(afterVal);
                return false;
            }
            
            if((newValue.indexOf('-') > 0 || newValue.indexOf('+') > 0)){
            	
            	if(key == '-') arrSplit = oldMValue.split('-');
            	else arrSplit = oldMValue.split('+');
            	
            	if(typeof arrSplit[1] != 'undefined') $( "#txtMultipler" ).val(arrSplit[0]+arrSplit[1]);
            	else $( "#txtMultipler" ).val(arrSplit[0]);
                return false;
            }
            
            var arrVal = newValue.split('.');
            
            if(arrVal.length > 1){
            	console.log('Splitted')
            	if(arrVal[1].length > 2){
            		var afterVal = arrVal[1].substring(0, 2);
            		$( "#txtMultipler" ).val(arrVal[0]+'.'+afterVal);
                    return false;
            	}
            }
            
        });
    });
    
    function loadPanelParameter(){
        var activeDeviceID  = $("#activeDeviceId").val();
        $("#datanotAvailable").addClass("hide");
        //console.log(activeDeviceArr);
        var $data = {
            'action' : 'GetPanelParameters',
            'source' : '3'
        }
        $.ajax({
            type: 'POST',
            url: "deviceLookup.php",
            data: $data,
            dataType: 'json',
            beforeSend: function() {$("#parameterLoader").removeClass("hide");},
            success: function(response){
                //console.log(response);
                if(response.status == 1){
                    var $allParams = response.responseData.panelAllParams;
                    var $lockedStatus = response.responseData.lockedStatus[0];
                    $ParamsData = [];
                    //var $modelName = ($modelArr.hasOwnProperty($ParamsData.MODEL_NUMBER)) ? $modelArr[$ParamsData.MODEL_NUMBER].ModelNo : "Unknown";
                    //console.log($ParamsData);
                    //console.log($ParamsData.MODEL_NUMBER);
                    //$("#txtModel").val($modelName);
                    setSelectData("txtInstallation",$allParams.installationformat,-1,"Select format");
                    setSelectData("txtCableSize",$allParams.cabletype,-1,"Select cable type");
                    setSelectData("txtCableMaterial",$allParams.cablematerial,-1,"Select material");
                    setSelectData("txtCalibration",$allParams.calibration,-1,"Selected Calibration");
                    setSelectData("txtResponsePeriod",$allParams.responseperiod,-1,"Select Response Period");
                    setSelectData("txtParallelSys",$allParams.parallelsystem,-1,"Select unit");
                    
                    if($lockedStatus.LOCKED) $( "#checklock" ).prop( "checked", true );
                    else $( "#checklock" ).prop( "checked", false );
                    
                    $ParamsData["LOCKED"] = $lockedStatus.LOCKED;
                    //console.log($ParamsData);
                }
                else{
                    $ParamsData=[];
                    setParamValue();
                    $("#datanotAvailable").removeClass("hide");
                }
            },
            timeout: 45000,
            complete:function (){
                //console.log("complete");
                
                $("#parameterLoader").addClass("hide");
            },
            error:function(r){
                if(typeof $ParamsData == "undefined") $("#datanotAvailable").removeClass("hide");
                $("#parameterLoader").addClass("hide");
                console.log(r);
            }
        })
    }
    
    function resetParamValue(hbData, activDev){
        
        if(typeof (hbData) != "undefined" && typeof (activDev) != "undefined"){
            
            needParameterSet = false;
            $lockedStatusTemp = $ParamsData["LOCKED"];
            $ParamsData = hbData;
            $ParamsData["LOCKED"] = $lockedStatusTemp;
            //console.log($ParamsData.calc_format);
            var $modelName = ($modelArr.hasOwnProperty($ParamsData.model_number)) ? $modelArr[$ParamsData.model_number].ModelNo : "Unknown";
            $("#txtModel").val($modelName);
            $("#sel-moving-avg").select2().select2("val",$ParamsData.calc_format);
            $("#txtInstallation").select2().select2('val',$ParamsData.install_format);
            $("#txtCableSize").select2().select2('val',$ParamsData.cable_size);
            $("#txtCableMaterial").select2().select2('val',$ParamsData.cable_material);
            $("#txtStartUp").select2().select2('val',$ParamsData.bank_startup_status);
            $("#txtCalibration").select2().select2('val',$ParamsData.calibartion_set);
            $("#txtResponsePeriod").select2().select2('val',$ParamsData.respns_delay);
            $("#txtParallelSys").select2().select2('val',$ParamsData.no_parallel_units);
            $("#txtMultipler").val(RoundOneDecimal($ParamsData.amps_multiplier,2));
        }            
    }
    
    function setParamValue(){
        $("#txtInstallation").select2().select2('val','-1');
        $("#txtCableSize").select2().select2('val','-1');
        $("#txtCableMaterial").select2().select2('val','-1');
        $("#txtStartUp").select2().select2('val','-1');
        $("#txtCalibration").select2().select2('val','-1');
        $("#txtResponsePeriod").select2().select2('val','-1');
        $("#txtParallelSys").select2().select2('val','-1');
        $("#txtMultipler").val('');
    }
    
    function setSelectData($id,$data,$type,$default){
        var sel = $('#'+$id);
        sel.html('<option value="-1" selected >'+$default+'</option>');
        $.each($data, function( $key, $value ) {
            //console.log($type +"=="+ $key);
           if($type == $key )sel.append('<option value="'+$key+'" selected>'+$value+'</option>');
           else sel.append('<option value="'+$key+'">'+$value+'</option>');
        });
        sel.select2({});
    }
    
    function applyParameters(){
        var model = $("#txtModel").val();
        var installation = $("#txtInstallation").val();
        var cableSize = $("#txtCableSize").val();
        var cableMaterial = $("#txtCableMaterial").val();
        var startUp = $("#txtStartUp").val();
        var calibration = $("#txtCalibration").val();
        var responsePeriod = $("#txtResponsePeriod").val();
        var parallelSys = $("#txtParallelSys").val();
        var multipler = $.trim($("#txtMultipler").val());
        var deviceID  = $("#activeDeviceId").val();
        var capacitoFormat = $("#sel-moving-avg").val();
        var count = 0;
        
        if($("#checklock").is(':checked')) {
            $createAlert({status : "fail",title : "Failed",text :"Device is locked"});
            return false;
        }
        
        if(deviceID == 0){
            $createAlert({status : "info",title : "Info",text :"Please Select a Device"});
            return false;
        }
        if(!selectValidation('txtInstallation','installation format','divInst','divErrorContainer') ||
            !selectValidation('txtCableSize','cable size/type','divType','divErrorContainer') ||
            !selectValidation('txtCableMaterial','cable materia','divMaterial','divErrorContainer') ||
            !selectValidation('txtStartUp','default Startup','divStartup','divErrorContainer') ||
            !selectValidation('txtCalibration','calibration','divCalibration','divErrorContainer') ||
            !selectValidation('txtResponsePeriod','period','divTime','divErrorContainer') ||
            !selectValidation('sel-moving-avg','capacito format','divMaterial','divErrorContainer') || 
            !selectValidation('txtParallelSys','unit','divUnit','divErrorContainer')){return false;}
        
        if(multipler == "" || !isDecimal(multipler))
        {
            $("#divMultipler").addClass("has-error");
            $("#divErrorContainer span").removeClass("hide");
            $("#divErrorContainer span").html("Enter multipler .");
            $('#txtMultipler').focus();
            return false;
        }
        if($ParamsData == null){
            $createAlert({status : "info",title : "Info",text :"Please Select a Device"});
            return false;
        }
        var $data = {
            'action' : 'SavePanelParams',
            //"MODELNUMBER":model,
            "UserID":"<?php echo $_SESSION['LOGGED_USER_ID'] ?>",
            "Role":"<?php echo $_SESSION['LOGGED_USER_DEVICE_ROLE'] ?>",
            'source' : '3',
        }  
        if($ParamsData.no_parallel_units.toString() != parallelSys){$data.PARALLELSYSTEM = parallelSys; count++;}
        if($ParamsData.bank_startup_status.toString() != startUp){$data.DEFAULTSTARTUP = startUp; count++;}
        if($ParamsData.cable_size.toString() != cableSize){$data.CABLETYPE = cableSize; count++;}
        if($ParamsData.install_format.toString() != installation){$data.INSTALLATIONFORMAT = installation; count++;}
        if($ParamsData.calibartion_set.toString() != calibration){$data.CALIBRATION = calibration;count++;}
        if($ParamsData.respns_delay.toString() != responsePeriod){$data.RESPONSEPERIOD = responsePeriod; count++;}
        if($ParamsData.amps_multiplier.toString() != multipler){$data.MULTIPLIER = multipler; count++;}
        if($ParamsData.cable_material.toString() != cableMaterial){$data.CABLEMATERIAL = cableMaterial; count++;}
        if($ParamsData.calc_format.toString() != capacitoFormat){$data.CALC_SPAN = capacitoFormat; count++;}
        
        if(count == 0){
            $createAlert({status : "info",title : "Info",text :"Parameters not changed."});
            return false;
        }
        
        /*$("#btnCnfmYes").addClass("hide");
        $("#btnCnfmNo").addClass("hide");
         $("#cnCntent").html('Please wait to apply parameters'+
            '</br><span class="timer" id="ConfirmCountdown"></span>'+
                '<div class="loading confirmLoader p-l-5"><div class="loading-bar"></div><div class="loading-bar"></div><div class="loading-bar"></div><div class="loading-bar"></div></div>');
            //$("#modalCnfirmation .modal-footer").addClass("hide");
            $("#modalCnfirmation").modal("show");*/
        //setTimer();
        
        $.ajax ({
            type: 'POST',
            url: "deviceLookup.php",
            data: $data,
            dataType: 'json',
            beforeSend: function() {
                __showLoadingAnimation();
                //$('#ConfirmCountdown').countdown({until: +45, compact: true,format: 'S', description: ''});
            },
            success: function(response){
                //$("#modalCnfirmation").modal("hide");
               // $('#ConfirmCountdown').countdown('destroy');
                if(response.status == 1){
                    var errArray = "";
                    
                    var result = response.responseData;
                    
                    $.each($data, function (key, data) {
                        if(key != "Role" && key != "action" && key != "source" && key != "UserID"){
                               if(typeof(result[key]) == 'undefined' || result[key] != 1) errArray += errArray == "" ? key : ", " + key;
                            }
                        });
                    if(errArray != ""){
                        $createAlert({status : "success",title : "Success",text :"Error occured while save parameter(s): " + errArray});
                    }
                    else $createAlert({status : "success",title : "Success",text :"Successfully applied parameters"});
                    
                }
                else{
                    $createAlert({status : "fail",title : "Failed",text :"Failed to apply parameters"});
                    $("#txtModel").val(model);
                    $("#txtInstallation").val(installation);
                    $("#txtCableSize").val(cableSize);
                    $("#txtCableMaterial").val(cableMaterial);
                    $("#txtStartUp").val(startUp);
                    $("#txtCalibration").val(calibration);
                    $("#txtResponsePeriod").val(responsePeriod);
                    $("#txtParallelSys").val(parallelSys);
                    $("#txtMultipler").val(multipler); 
                }
                
                needParameterSet = true;
            },
            timeout: 45000,
            error: function(r){
                __hideLoadingAnimation();
                //$("#modalCnfirmation").modal("hide");
                //$('#ConfirmCountdown').countdown('destroy');
                $createAlert({status : "fail",title : "Failed",text :"Failed to apply parameters"});
            },
            complete:function(){
                __hideLoadingAnimation();
            }
        });
    }
    
    function locakParameter(){
        var deviceID  = $("#activeDeviceId").val();
        var lockParams = 0;
        var lockStatus = "Unlocked";
        var lockErrorStatus = "Unlock";
        if($("#checklock").is(':checked')){
            lockParams = 1;
            lockStatus = "Locked"
            lockErrorStatus = "Lock";
        }
        console.log($ParamsData);
        if( $ParamsData == null){
            $createAlert({status : "info",title : "Info",text :"Please Select a Device"});
            return false;
        }
        else if($ParamsData.length == 0){
            $createAlert({status : "info",title : "Info",text :"Please Select a Device"});
            return false;
        }
        
        var $data = {
            'action' : 'LockPanelParams',
            "UserID":"<?php echo $_SESSION['LOGGED_USER_ID'] ?>",
            "Role":"<?php echo $_SESSION['LOGGED_USER_DEVICE_ROLE'] ?>",
            'source' : '3',
            'Locked' : lockParams
        }  
        $.ajax ({
            type: 'POST',
            url: "deviceLookup.php",
            data: $data,
            dataType: 'json',
            beforeSend: function() {
                __showLoadingAnimation();
            },
            success: function(response){
                if(response.status == 1){
                    $createAlert({status : "success",title : "Success",text :"Successfully "+lockStatus});
                }
                else{
                    $createAlert({status : "fail",title : "Failed",text :response.error});
                    if(lockParams == 1) $("#checklock").prop("checked",false);
                    else  $("#checklock").prop("checked",true);                        
                }
            },
            timeout: 45000,
            complete:function (){__hideLoadingAnimation();},
            error: function(r){
                if(lockParams == 1) $("#checklock").prop("checked",true);
                else  $("#checklock").prop("checked",false);     
                __hideLoadingAnimation();
                $createAlert({status : "fail",title : "Failed",text :"Failed to "+lockErrorStatus});
            }
        });
    }
    
    function bankswitch($type){
        var deviceId = $("#activeDeviceId").val();
        
        if(deviceId == 0){
            $createAlert({status : "info",title : "Info",text :"Please Select a Device"});
            return false;
        }
        $("#btnCnfmYes").attr("onclick","return actionConfirm();");
        $("#btnCnfmYes").removeClass("hide");
        $("#btnCnfmNo").removeClass("hide");
        if($type == 0){
            //$("#btnCnfmYes").removeClass("btn-success");
            //$("#btnCnfmYes").addClass("btn-red");
            $("#cnCntent").html("Do you wish to turn banks OFF?");
        }
        else {
            //$("#btnCnfmYes").removeClass("btn-red");
            //$("#btnCnfmYes").addClass("btn-success");
            $("#cnCntent").html("Do you wish to turn banks ON?");
        }
        
        $("#bankSwitchType").val($type);
        //$("#modalCnfirmation .modal-footer").removeClass("hide");
        $("#modalCnfirmation").modal("show");
    }
    
    function captureChartInt(){
        $("#waitingTr").removeClass("hide");
        $("#harmonic-loader").removeClass("hide");
        $("#completeTr").addClass("hide");    
        
        // Get scope response 
        setTimeout(function(){
           getScopeData(); 
        },0);
    }
    
    function getScopeData(){
            var $data = {
                'c' : 'parameter',
                'a' : 'getHarmonics'
            }
            $.ajax({
                type:'POST',
                url: 'ajax.php',
                data: $data,
                dataType: 'json',
                async:true,
                beforeSend: function() {
                   /// __showLoadingAnimation();
                },
                success: function(res) {
                        if(res.status == 1){
                            $scopeRecord = res.scopeRecord;
                            $harmonicRecord = res.harmonicRecord;
                            _HB_VTHD_C = _HB_VTHD;
                            _HB_ATHD_C = _HB_ATHD;
			    _HB_PEAKV_HARM_C = _HB_PEAKV_HARM;
                            _HB_PEAKA_HARM_C = _HB_PEAKA_HARM;
                            //var $dt = new Date(res.time);
                            var $dt = new Date();
                            $harmonicDatatime = $dt;
                            $("#TrComplete-time").html(("0" + $dt.getHours()).slice(-2)+":"+("0" + $dt.getMinutes()).slice(-2)+":"+("0" + $dt.getSeconds()).slice(-2));
                            
                            
                            if(!($('#checkShowKVA').prop('checked') || /*$('#checkShowIdeal').prop('checked') ||*/ $('#checkShowActual').prop('checked'))){
                                $('#checkShowActual').prop('checked', true);
                            };
                                                        
                            if(!($('#checkShowph1').prop('checked') || $('#checkShowph2').prop('checked') || $('#checkShowph3').prop('checked'))){
                                $('#checkShowph1').prop('checked', true);
                                
                            };
                            initScopeChart();
                            if(!($('#voltPh1').prop('checked') || $('#voltPh2').prop('checked') || $('#voltPh3').prop('checked') ||
                                    $('#ampsPh1').prop('checked') || $('#ampsPh2').prop('checked') || $('#ampsPh3').prop('checked') )){
                                
                                $("#voltPh1").prop("checked",true);
                                
                            };
                            changeHarmonicType();
                            //$("#btnHarmonicReset").html("Capture");
                            $("#voltPh1").click();
                        }
                        else{
                            $createAlert({status :"fail",title : "Failed",text : res.message});
                            $("#waitingTr").addClass("hide");
                            $("#completeTr").removeClass("hide");
                            $("#TrComplete-time").html("Failed");
                            $("#harmonic-loader").addClass("hide");
                        }
                        //console.log($harmonicRecord);
                },
                complete:function(){
                   // __hideLoadingAnimation();
                    $("#waitingTr").addClass("hide");
                    $("#completeTr").removeClass("hide");
                    $("#harmonic-loader").addClass("hide");
                },
                error: function(r) {
                   // __hideLoadingAnimation();
                    $("#waitingTr").addClass("hide");
                    $("#completeTr").removeClass("hide");
                    $("#TrComplete-time").html("Failed");
                    $("#harmonic-loader").addClass("hide");
                }
            });
    }
    
    function changeScopeCheck($obj){
        var $id = $obj.id;
        if($id == "checkShowKVA"){
            
            if(!$("#checkShowKVA").is(':checked')){
                /*$("#li-check-ideal").removeClass("hide");*/
                $('#checkShowKVA').attr('checked', false);
            }
            else{
                /*$('#checkShowIdeal').attr('checked', false);*/
                $('#checkShowActual').attr('checked', false);
                /*$("#li-check-ideal").addClass("hide");*/
            }
            
        }
        else if($id == "checkShowActual"){
            /*$("#li-check-ideal").removeClass("hide");*/
            $('#checkShowKVA').attr('checked', false);
        }
        initScopeChart();
    }
    
    function changeHarmonicType(){
        if ($harmonicRecord != undefined && $harmonicRecord != null && $harmonicRecord.length != 0 ){
            var $data =new Array();
            $('input[name="radioHarmonic"]:checked').each(function() {
                var $type = "";
                $type = $(this).attr("value");
                var $tim = ("0" + $harmonicDatatime.getHours()).slice(-2)+":"+("0" + $harmonicDatatime.getMinutes()).slice(-2)+":"+("0" + $harmonicDatatime.getSeconds()).slice(-2)+":"+("0" + $harmonicDatatime.getMilliseconds()).slice(-2);
                
		if($type == "voltPh1"){
                    var $thdVal = _HB_VTHD_C != 0 ? _HB_VTHD_C : $harmonicRecord.hvPh1.THDval;
		    var _Pval = _HB_PEAKV_HARM_C != 0 ? _HB_PEAKV_HARM_C : $harmonicRecord.hvP13.Pval;
                    
                    $data.push({"key": "voltage Phase 1",
                        "values": JSON.parse(JSON.stringify(FormatedHarmonicArray($harmonicRecord.hvPh1.value,53,1,$harmonicRecord.hvPh1.Pval))),
                        "THDval" : $thdVal,
                        "PID":$harmonicRecord.hvPh1.PID,
                        "PVal":_Pval,
                        "Htime":$tim,"ID":"V1"});
                }
                else if($type == "voltPh2"){
                    var $thdVal = _HB_VTHD_C != 0 ? _HB_VTHD_C : $harmonicRecord.hvPh2.THDval;
		    var _Pval = _HB_PEAKV_HARM_C != 0 ? _HB_PEAKV_HARM_C : $harmonicRecord.hvPh2.Pval;

                    $data.push({"key": "voltage Phase 2",
                        "values":  JSON.parse(JSON.stringify(FormatedHarmonicArray($harmonicRecord.hvPh2.value,53,1,$harmonicRecord.hvPh2.Pval))),
                        "THDval" : $thdVal,
                        "PID":$harmonicRecord.hvPh2.PID,
                        "PVal":_Pval,
                        "Htime":$tim,
                        "ID":"V2"});
                }
                else if($type == "voltPh3") {
                    var $thdVal = _HB_VTHD_C != 0 ? _HB_VTHD_C : $harmonicRecord.hvPh3.THDval;
		    var _Pval = _HB_PEAKV_HARM_C != 0 ? _HB_PEAKV_HARM_C : $harmonicRecord.hvPh3.Pval;

                    $data.push({"key": "voltage Phase 3",
                        "values":  JSON.parse(JSON.stringify(FormatedHarmonicArray($harmonicRecord.hvPh3.value,53,1,$harmonicRecord.hvPh3.Pval))),
                        "THDval" : $thdVal,
                        "PID":$harmonicRecord.hvPh3.PID,
                        "PVal":_Pval,
                        "Htime":$tim,
                        "ID":"V3"});
                }
                else if($type == "ampsPh1"){
                    var $thdVal = _HB_ATHD_C != 0 ? _HB_ATHD_C : $harmonicRecord.haPh1.THDval;
		    var _Pval = _HB_PEAKA_HARM_C != 0 ? _HB_PEAKA_HARM_C : $harmonicRecord.haPh1.Pval;

                    $data.push({"key": "Current Phase 1",
                        "values":  JSON.parse(JSON.stringify(FormatedHarmonicArray($harmonicRecord.haPh1.value,53,1,$harmonicRecord.haPh1.Pval))),
                        "THDval" : $thdVal,
                        "PID":$harmonicRecord.haPh1.PID,
                        "PVal":_Pval,
                        "Htime":$tim,
                        "ID":"A1"});
                }
                else if($type == "ampsPh2"){
                    var $thdVal = _HB_ATHD_C != 0 ? _HB_ATHD_C : $harmonicRecord.haPh2.THDval;
		    var _Pval = _HB_PEAKA_HARM_C != 0 ? _HB_PEAKA_HARM_C : $harmonicRecord.haPh2.Pval;

                    $data.push({"key": "Current Phase 2",
                        "values":  JSON.parse(JSON.stringify(FormatedHarmonicArray($harmonicRecord.haPh2.value,53,1,$harmonicRecord.haPh2.Pval))),
                        "THDval" : $thdVal,
                        "PID":$harmonicRecord.haPh2.PID,
                        "PVal":_Pval,
                        "Htime":$tim,
                        "ID":"A2"});
                }
                else if($type == "ampsPh3"){
                    var $thdVal = _HB_ATHD_C != 0 ? _HB_ATHD_C : $harmonicRecord.haPh3.THDval;
		    var _Pval = _HB_PEAKA_HARM_C != 0 ? _HB_PEAKA_HARM_C : $harmonicRecord.haPh3.Pval;

                    $data.push({"key": "Current Phase 3","values":  JSON.parse(JSON.stringify(FormatedHarmonicArray($harmonicRecord.haPh3.value,53,1,$harmonicRecord.haPh3.Pval))),
                        "THDval" : $thdVal,
                        "PID":$harmonicRecord.haPh3.PID,
                        "PVal":_Pval,
                        "Htime":$tim,
                        "ID":"A3"});
                }
                
                $(".harmonic-nodata").addClass("hide");
                initHarmonicChart($data);
            }); 
        }
        else{
            $(".harmonic-notify").addClass("hide");
            $(".harmonic-nodata").removeClass("hide");
        }
    }
    
    function actionConfirm(){
        var bankSwitchType = $("#bankSwitchType").val();
        var deviceId = $("#activeDeviceId").val();
        var resetmemory = $("#resetMemory").val();  
        
        $("#resetMemory").val("0");      
        $("#bankSwitchType").val("0");
        $("#btnCnfmYes").addClass("hide");
        $("#btnCnfmNo").addClass("hide");
                
        //return false;
        if(resetmemory == 0)
        {       
            var $status = (bankSwitchType == 1 ? 'ON' : 'OFF');
            $("#cnCntent").html('Please wait while turning banks '+ $status +
            '</br><span class="timer" id="ConfirmCountdown"></span>'+
                '<div class="loading confirmLoader p-l-5"><div class="loading-bar"></div><div class="loading-bar"></div><div class="loading-bar"></div><div class="loading-bar"></div></div>');
            //$("#modalCnfirmation .modal-footer").addClass("hide");
            $("#modalCnfirmation").modal("show");
        
            var $data = {
                'action' : 'SetBanksStatus',
                'source' : '3',
                'status' : bankSwitchType
            }
            $.ajax({
                type:'POST',
                url: "deviceLookup.php",
                data: $data,       
                dataType: 'json',
                beforeSend: function() {
                    $('#ConfirmCountdown').countdown({until: +45, compact: true,format: 'S', description: ''});},
                success: function(response){
                    //console.log(response);
                    $("#modalCnfirmation").modal("hide");
                    if(response.status == 1)
                    {
                        if(response.responseData.ChangeBankStatus == 1){
                            if(response.responseData.BanksStatus == 1) {
                                $createAlert({status : "success",title : "Success",text :"Successfully turned banks ON"});
                                //$(".switch-buttons .switch-red").removeClass("active");
                                //$(".switch-buttons .switch-green").addClass("active");
                            }
                            else {
                                $createAlert({status : "success",title : "Success",text :"Successfully turned banks OFF"});
                                //$(".switch-buttons .switch-green").removeClass("active");
                                //$(".switch-buttons .switch-red").addClass("active");
                            }
                        }
                        else $createAlert({status : "fail",title : "Failed",text :"Failed to turn banks "+$status});
                    }
                    else $createAlert({status : "fail",title : "Failed",text :"Failed to turn banks "+$status});
                    $('#ConfirmCountdown').countdown('destroy');
                },
                timeout: 45000,
                error: function(jqXHR, textStatus, errorThrown){
                    //console.log(r);
                    $("#modalCnfirmation").modal("hide");
                    $createAlert({status : "fail",title : "Failed",text :"Failed to turn banks "+$status});
                    $('#ConfirmCountdown').countdown('destroy');
                }
            });
        }
        else{  
            var ynValue = $("#txt-rst-yes-no").val();
            
            if(ynValue.trim().toLowerCase() != "yes" ) {
                $("#modalCnfirmation").modal("hide");
                return false;
            }
                   
            $("#cnCntent").html('Please wait while reseting memory'+
            '</br><span class="timer" id="ConfirmCountdown"></span>'+
                '<div class="loading confirmLoader p-l-5"><div class="loading-bar"></div><div class="loading-bar"></div><div class="loading-bar"></div><div class="loading-bar"></div></div>');
            //$("#modalCnfirmation .modal-footer").addClass("hide");
            $("#modalCnfirmation").modal("show");
            var $data = {
            'c' : 'Device',
            'a' : 'ResetDeviceMemory'
            }
            $.ajax({
                type:'POST',
                url: "ajax.php",
                data: $data,
                dataType: 'json',
                beforeSend: function() {
                    $('#ConfirmCountdown').countdown({until: +60, compact: true,format: 'S', description: ''});},
                success: function(response){
                    //console.log(response);
                    $("#modalCnfirmation").modal("hide");
                    if(response.status == 1){
                        $createAlert({status : "success",title : "Success",text :"Successfully completed device reset."});
                    }
                    else $createAlert({status : "fail",title : "Failed",text :"Failed to reset memory."});
                    $("#resetMemory").val(0);
                    $('#ConfirmCountdown').countdown('destroy');
                },
                timeout: 60000,
                error: function(r){
                    $("#modalCnfirmation").modal("hide");
                    $createAlert({status : "fail",title : "Failed",text :"Failed to reset memory."});
                    $('#ConfirmCountdown').countdown('destroy');
                }
            });
        }
        
    }
    
    function resetMemory(){
        var deviceId = $("#activeDeviceId").val();
        
        if(deviceId == 0){
            
            $createAlert({status : "info",title : "Info",text :"Please Select a Device"});
            return false;
        }
        $("#btnCnfmYes").attr("onclick","return actionConfirm();");
        $("#btnCnfmYes").removeClass("hide");
        $("#btnCnfmNo").removeClass("hide");
        //$("#cnCntent").html("Do you wish to reset memory?");
        $("#cnCntent").html('<div class="form-group"><label class="fs-10">All data will be removed.<br>Do you wish to reset device memory (Yes / No)?</label><input type="text" value="" class="form-control" id="txt-rst-yes-no"></div>');
        //$("#modalCnfirmation .modal-footer").removeClass("hide");
        $("#modalCnfirmation").modal("show");
        $("#resetMemory").val(1);
        
        
    }
    
    function removeErrorAlert(){
        $("#divMultipler").removeClass("has-error");
        $("#divErrorContainer span").addClass("hide");
        $("#divErrorContainer span").html("");
        
    }
    
    function oprationValidation(){
        var $elemVal = $("#txtMultipler").val();
        if($elemVal == ""){
            $("#divMultipler").addClass("has-error");
            $("#divErrorContainer span").removeClass("hide");
            $("#divErrorContainer span").html("Enter multipler.");
            $('#txtMultipler').focus();
            return false;
        }
        else if(!isDecimal($elemVal)){
            $("#divMultipler").addClass("has-error");
            $("#divErrorContainer span").removeClass("hide");
            $("#divErrorContainer span").html("Invalid multipler.");
            $('#txtMultipler').focus();
            return false;
        }
    }
    
    function generateExport(){
        __showLoadingAnimation();
        var startDate   = $("#startDate").val();
        var endDate     = $("#endDate").val();
        var deviceID  = $("#activeDeviceId").val();
        
        if(deviceID == 0){
            $createAlert({status : "info",title : "Info",text :"Please Select a Device"});
            __hideLoadingAnimation();
            return false;
        }
        if(startDate == ""){
            $("#divStart").addClass("has-error");
            $("#divExportError span").removeClass("hide");
            $("#divExportError span").html("Please Select Start Date");
            __hideLoadingAnimation();
            return false;
        }
        
        if(endDate == ""){
            $("#divEnd").addClass("has-error");
            $("#divExportError span").removeClass("hide");
            $("#divExportError span").html("Please Select End Date");
            __hideLoadingAnimation();
            return false;
        }
        
        var hdnAnchor = document.getElementById('clickHidden');
        hdnAnchor.href ='ajax.php?c=Device&a=exportData&StartDate='+startDate+'&EndDate='+endDate;
        hdnAnchor.click();
        __hideLoadingAnimation();
    }
    
    function removeExportAlert(id){
        $("#"+id).removeClass("has-error");
        $("#divExportError span").addClass("hide");
        $("#divExportError span").html("");
    }
    
    function setDateTimeConfirm(){
        var deviceId = $("#activeDeviceId").val();
        
        if(deviceId == 0){
            $createAlert({status : "info",title : "Info",text :"Please Select a Device"});
            return false;
        }
        $("#cnCntent").html("Do you wish to reset date and time?");
        $("#btnCnfmYes").attr("onclick","return setDateTime();");
        $("#btnCnfmYes").removeClass("hide");
        $("#btnCnfmNo").removeClass("hide");
        $("#modalCnfirmation").modal("show");
    }
    
    function setDateTime(){
        
        $("#btnCnfmYes").addClass("hide");
        $("#btnCnfmNo").addClass("hide");
        $("#cnCntent").html('Please wait while reset date and time'+
            '</br><span class="timer" id="ConfirmCountdown"></span>'+
                '<div class="loading confirmLoader p-l-5"><div class="loading-bar"></div><div class="loading-bar"></div><div class="loading-bar"></div><div class="loading-bar"></div></div>');
        $("#modalCnfirmation").modal("show");
        var $date = new Date();
        console.log($date.getFullYear()+"-"+$date.getMonth()+"-"+$date.getDate()+" "+$date.getHours()+":"+$date.getMinutes()+":"+$date.getSeconds());
        //return false;
        var $data = {
            'c' : 'Device',
            'a' : 'setDateTime',
            'date' : escape($date.getFullYear()+"-"+$date.getMonth()+"-"+$date.getDate()+" "+$date.getHours()+":"+$date.getMinutes()+":"+$date.getSeconds())
        };
        $.ajax({
            type:'POST',
            url:'ajax.php',
            data: $data,       
            dataType: 'json',
             beforeSend: function() {
                    $('#ConfirmCountdown').countdown({until: +45, compact: true,format: 'S', description: ''});},
            success: function(response){
                //console.log(response);
                $("#modalCnfirmation").modal("hide");
                $('#ConfirmCountdown').countdown('destroy');
               if(response == null)$createAlert({status : "fail",title : "Failed",text :"Failed to set date and time"});
               if(response.status == 1){
                   var result = response.responseData;
                   if(result.SetTimeStatus == 1 && result.SetDateStatus == 1){
                       var $date = new Date(result.Date);
                       $createAlert({status : "success",title : "Success",text :"Successfully set date and time </br> Date : "+("0" + ($date.getMonth()+1)).slice(-2)+"/"+("0" + $date.getDate()).slice(-2)+"/"+("0" + $date.getYear()).slice(-2) +" "+ result.Time});
                   }
                   else{
                        if(result.SetDateStatus == 1){
                             var $date = new Date(result.Date);
                            $createAlert({status : "success",title : "Success",text :"Successfully set date </br> Date : "+("0" + ($date.getMonth()+1)).slice(-2)+"/"+("0" + $date.getDate()).slice(-2)+"/"+("0" + $date.getYear()).slice(-2)});
                        }
                        else $createAlert({status : "fail",title : "Failed",text :"Failed to set date"});
                        if(result.SetTimeStatus == 1) $createAlert({status : "success",title : "Success",text :"Successfully set time </br> Time : "+ result.Time});
                        else $createAlert({status : "fail",title : "Failed",text :"Failed to set time"});
                   }
               }
               else if(response.status == 2) $createAlert({status : "fail",title : "Failed",text :"Please select a device"});
               else  $createAlert({status : "fail",title : "Failed",text :"Failed to set date and time"});
            },
            timeout: 45000,
            //complete:function (){__hideLoadingAnimation();},
            error: function(jqXHR, textStatus, errorThrown){
                //console.log(r);
                $('#ConfirmCountdown').countdown('destroy');
                $("#modalCnfirmation").modal("hide");
               // __hideLoadingAnimation();
                $createAlert({status : "fail",title : "Failed",text :"Failed to set date and time"});
            }
        });
    }
    
    function checkTimeFrame(time){
        //alert(time);
        if(time != ""){
            var valid = time%5;
            if(time>600){
                $("#divRecordError span").removeClass("hide");
                $("#divRecordError span").html("Maximum Time Frame is 600");
                return false;
            }
            else if(time < 0){
                $("#divRecordError span").removeClass("hide");
                $("#divRecordError span").html("Time Frame must be positive");
                return false;
            }
            else if(valid != 0){
                $("#divRecordError span").removeClass("hide");
                $("#divRecordError span").html("Time Frame must be multiple of 5");
                return false;
            }
            else{
               $("#divRecordError span").addClass("hide");
               $("#divRecordError span").html("");
               return true;
            }
        }
    }
    
    var stopFlag = 0;
    var recordArr = new Array();
    var timeframe = 0;
	var setTimer; 
    
    function startRecordData(){
        var deviceID  = $("#activeDeviceId").val();
        timeframe = $("#timeFrame").val();
        $("#downloadFile").addClass("hide");
        recordArr.length = 0;
        stopFlag = 0;
        
        if(deviceID == 0){
            $createAlert({status : "info",title : "Info",text :"Please Select a Device"});
            return false;
        }
        if(timeframe == "" || timeframe <= 0){
            $("#divRecordError span").removeClass("hide");
            $("#divRecordError span").html("Please provide valid Time Frame");
            return false;
        }
        if(!checkTimeFrame(timeframe)){
            return false;
        }
		
		// Starting fresh countdown
		$("#recordCounter").html("Recording Data ");
        $('#Countdown').countdown({until: +timeframe, compact: true,format: 'HMS', description: '',onExpiry: function(){
			$("#recordCounter").html("Creating CSV File. ");
		}});
        //convertTimeFrame();
		// 
        
        $("#timeFrame").prop("readonly",true);
        $("#timeChange").addClass("hide");
        $("#btnRecord").prop('disabled', true);
        $('#btnRecord').tooltip('hide');
        $("#btnStopRecord").prop('disabled', false);
        $("#divStopRecord").removeClass("hide");
        
        getRecordData();
    }
       
    function getRecordData(){
        if(stopFlag == 1 || timeframe == 0){
            _clearIntvl();
            $("#divStopRecord").addClass("hide");
            $("#btnStopRecord").prop('disabled', true);
            $('#btnStopRecord').tooltip('hide');
            $("#btnRecord").prop('disabled', false);
            $("#timeFrame").prop("readonly",false);
            $("#timeChange").removeClass("hide");
            $("#downloadFile").removeClass("hide");
            $('#Countdown').countdown('destroy');
            //alert(recordArr);
            createDownloadLink("#downloadFile",recordArr,"record.csv");
            return false;
        }
        
        
        var $data = {
            'action' : 'Get_HB',
            'source' : '3'
        } 
        
		

        var i=0;
        setTimer = setInterval(function(){
        
            // If user pressed stop or time s already up, stop!.
            if(stopFlag == 1 || timeframe == 0){
                _clearIntvl();
                return false;
            }
            if(timeframe!=0 && stopFlag == 0){
                $.ajax({
                    type:'POST',
                    url: "deviceLookup.php",
                    data: $data,
                    async: true,
                    dataType: 'json',
                    success: function(response){
						
                        if(response.responseData!=""){
                            recordArr[i] = response.responseData;
                            i++;
                            
                            timeframe = parseInt(timeframe)-5;
                //console.log(timeframe);
                
                            if(timeframe == 0){
                                _clearIntvl();

                                // Recording finished so change message to file creation 
                                $("#recordCounter").html("Creating CSV File. ");

                                $("#divStopRecord").addClass("hide");
                                $("#btnStopRecord").prop('disabled', true);
                                $('#btnStopRecord').tooltip('hide');
                                $("#btnRecord").prop('disabled', false);
                                $("#timeFrame").prop("readonly",false);
                                $("#timeChange").removeClass("hide");
                                $("#downloadFile").removeClass("hide");
                                $('#Countdown').countdown('destroy');
                                createDownloadLink("#downloadFile",recordArr,"record.csv");
                            }

                        }
                        
                    },
                    error: function(r){
                        console.log(r);
                    }
                });
                //alert(timeframe);
                
               
                
                
            }
            /*else{
				_clearIntvl();

                $("#divStopRecord").addClass("hide");
                $("#btnStopRecord").prop('disabled', true);
                $('#btnStopRecord').tooltip('hide');
                $("#btnRecord").prop('disabled', false);
                $("#timeFrame").prop("readonly",false);
                $("#timeChange").removeClass("hide");
                $("#downloadFile").removeClass("hide");
                $('#Countdown').countdown('destroy');
                //alert(recordArr);
                createDownloadLink("#downloadFile",recordArr,"record.csv");
            }*/
      
        }, 5000); // ajax call to get HB data every 5 seconds
        
        
        
        
    }

    function _clearIntvl()
    {
            //alert(setTimer);
            window.clearInterval(setTimer);
    }
    
    var blobObject = null;

    function createDownloadLink(anchorSelector, str, fileName){
    //alert(fileName);
    if(window.navigator.msSaveOrOpenBlob) {
        var fileData = [str];
        blobObject = new Blob(fileData);
        $(anchorSelector).click(function(){
                window.navigator.msSaveOrOpenBlob(blobObject, fileName);
        });
    } 
    else 
    {
        var j=0;
        var limit= str.length;
        var allRecords = "";
        var title="";
        var keys = ["PREAMBLE",
                    "START_BYTE", 
                    "TRANSMITTER_ID", 
                    "SERIAL_NO", 
                    "COMMAND_CODE", 
                    "COMMAND_PARAMETER", 
                    "CS1", 
                    "CS2", 
                    "LOGDATE",
                    "TIMEZONE",
                    "MAC_ID", 
                    "STATUS", 
                    "RESET_DURATION",	    		
                    "VOLTS_PH1", 
                    "VOLTS_PH2", 
                    "VOLTS_PH3", 
                    "AMPS1", 
                    "AMPS2", 
                    "AMPS3", 
                    "PF1", 
                    "PF2", 
                    "PF3", 	    		
                    "APAR_PWR1", "APAR_PWR2", "APAR_PWR3", 
                    "REAL_PWR1",  "REAL_PWR2",  "REAL_PWR3", 
                    "REACT_PWR1",  "REACT_PWR2",  "REACT_PWR3", 	    		
                    "APAR_PWR", 
                    "REAL_PWR", 
                    "REACT_PWR",	    		
                    "KWH_24",	    		
                    "VTHD", 
                    "ATHD", 	    		
                    "CAPS1", 
                    "CAPS2", 
                    "CAPS3", 	    		
                    "TAMB", 
                    "TINT", 	    		
                    "RES0", 
                    "RES1", 	    		  		
                    "V1_SLOPE",  "V1_INTER", 
                    "V2_SLOPE",  "V2_INTER", 
                    "V3_SLOPE",  "V3_INTER", 
                    "A1_SLOPE", "A1_INTER", 
                    "A2_SLOPE",  "A2_INTER", 
                    "A3_SLOPE",  "A3_INTER", 
                    "AMPS_MULTIPLIER", 
                    "NEUTRAL_AMPS", 
                    "LINE_FRQ", 
                    "VOLTS_IMB", 
                    "AMPS_IMB", 
                    "INTER_REV", 
                    "METER_REV", 
                    "MAIN_REV", 
                    "DAUGHTER_REV", 
                    "INSTALL_FORMAT", 
                    "CABLE_SIZE", 
                    "CABLE_MATERIAL", 
                    "CALIBARTION_SET", 
                    "RESPNS_DELAY", 
                    "NO_PARALLEL_UNITS", 
                    "MODEL_NUMBER",  
                    "CALC_FORMAT",  
                    "BANK_STARTUP_STATUS",  
                    "BANK0", 
                    "BANK1", 
                    "BANK2", 
                    "BANK3", 
                    "BANK4", 
                    "BANK5", 
                    "BANK6", 
                    "V_HARM", 
                    "VH_FREQ", 
                    "A_HARM", 
                    "AH_FREQ", 
                    "SURGE_CNT", 
                    "BROWNOUT_CNT", 
                    "IP1", 
                    "IP2", 
                    "IP3", 
                    "IP4", 
                    "MASK_IP1", 
                    "MASK_IP2", 
                    "MASK_IP3", 
                    "MASK_IP4", 
                    "RES2",
                    "RES3",
                    "RES4",
                    "RES5",
                    "RES6",
                    "RES7",
                    "RES8",
                    "RES9",
                    "RES10",
                    "RES11",
                    "RES12",
                    "RES13",
                    "RES14",
                    "RES15",
                    "RES16",
                    "RES17",
                    "RES18",
                    "RES19",
                    "RES20",
                    "RES21",
                    "RES22",
                    "RES23",
                    "CSM", 
                    "FN",
                    "DELETED",
                    "KVA_24",
                    "KVAR_24", 
                    "KVAMIN", 
                    "KVAMAX", 
                    "KVARMIN", 
                    "KVARMAX", 
                    "P3VMIN", 
                    "P3VMAX", 
                    "P3AMIN", 
                    "P3AMAX", 
                    "P3PFMIN", 
                    "P3PFMAX", 
                    "P3VIMIN", 
                    "P3VIMAX"];
        /*for (var key in str[j]) {
           keys.push(key);
        }*/
        //console.log(keys);
        if(limit == 0){
            allRecords = "No Records Available";
        }
        else{
            allRecords += keys+"\r\n";
            for(j=0; j<limit; j++){
                var values = new Array();
                for (var key in keys) {
                   // console.log(str[j][keys[key]] +">>>>"+keys[key]);
                   
                    var keyS = keys[key]
                    if(keyS == 'TIMEZONE'){
                        keyS = 'TIMEZ';
                        var tz = str[j][keyS];
                        
                        if(tz == null || tz == '' || tz < 0) tz = 0;
                        else if(tz >24) tz = 24;
                        else tz = parseInt(tz);
                        
                        values.push(GLOBAL_TIME_ZONES[tz]);
                    }
                    else{
                        values.push(str[j][keyS]);
                    }
                }
               allRecords += values.toString()+"\r\n"; 
            }
        }
        //alert(allRecords);
        
        var url = "data:text/csv;charset=utf-8," + encodeURIComponent(allRecords);
        $(anchorSelector).attr("href", url);
    }
}

    function hideLink(){
    $("#downloadFile").addClass("hide");
}  

    function stopRecordData(){
    stopFlag = 1;
    getRecordData();
    
}

    
</script>

       
