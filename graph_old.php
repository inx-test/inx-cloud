<?php
include "config.php";
include_once("tpl/header.tpl.php");

?>

<div class="content clearfix">
    <?php include_once("tpl/leftTreeViewPanel.tpl.php"); ?>
    <!-- START PANEL -->
    <div class="content-left ">
        <div class="panel panel-transparent">
            <div class="panel-heading ">
                <div class="panel-title  bold fs-16"> Graph</div>
            </div>
            <div class="panel-body">
                <div class="row m-b-10">
                    <div class="col-md-12">
                        <div class="pull-right text-right" style="width: 168px;">
                            <input id="fileupload" type="file" name="files[]" class="inputfile inputfile-1">
                            <label for="fileupload">Upload.</label>
                        </div>
                        <div class="pull-right m-l-15">
                            <h5 class="fs-16 bold hint-text m-t-5">Import Pull Schedule :</h5>
                        </div>
                    </div>
                </div>
                <div class="row m-b-10">
                    <div class="col-md-12">
                        <div class="btn-toolbar">
                            <div class="btn-group btn-group-sm" id="abcPagination">
                                <button class="btn btn-default" data-type="A">A</button>
                                <button class="btn btn-default" data-type="B">B</button>
                                <button class="btn btn-default" data-type="C">C</button>
                                <button class="btn btn-default" data-type="D">D</button>
                                <button class="btn btn-default" data-type="E">E</button>
                                <button class="btn btn-default" data-type="F">F</button>
                                <button class="btn btn-default" data-type="G">G</button>
                                <button class="btn btn-default" data-type="H">H</button>
                                <button class="btn btn-default" data-type="I">I</button>
                                <button class="btn btn-default" data-type="J">J</button>
                                <button class="btn btn-default" data-type="K">K</button>
                                <button class="btn btn-default" data-type="L">L</button>
                                <button class="btn btn-default" data-type="M">M</button>
                                <button class="btn btn-default" data-type="N">N</button>
                                <button class="btn btn-default" data-type="O">O</button>
                                <button class="btn btn-default" data-type="P">P</button>
                                <button class="btn btn-default" data-type="Q">Q</button>
                                <button class="btn btn-default" data-type="R">R</button>
                                <button class="btn btn-default" data-type="S">S</button>
                                <button class="btn btn-default" data-type="T">T</button>
                                <button class="btn btn-default" data-type="U">U</button>
                                <button class="btn btn-default" data-type="V">V</button>
                                <button class="btn btn-default" data-type="W">W</button>
                                <button class="btn btn-default" data-type="X">X</button>
                                <button class="btn btn-default" data-type="Y">Y</button>
                                <button class="btn btn-default" data-type="Z">Z</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="resultContainer" class="m-t-15">

                </div>

            </div>
        </div>
    </div>
</div>

<!-- Start file conformation -->
<div class="modal fade stick-up in" id="uploadConfirmation" data-backdrop="static" data-keyboard="false" role="dialog">
        <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header clearfix text-left">
                        <h5 class="panel-title">Excel Upload</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        <i class="pg-close fs-14"></i></button>
                    </div>
                    <div class="modal-body">
                        <div class="upload-progress">
                            <h5 class="fs-15" style="color:green;">Please wait while uploading file... <span class="up-pre-span">0</span>%</h5>
                        </div>
                        <div class="update-conf hide">
                            <h5 class="fs-15 ">Are you sure do you wish to proceed with import.<br> File Name : <span class="up-filename bold"></span> .</h5>
                            <input type="hidden" id="hdnUploadFileName" name="hdnUploadFileName" value="">
                            <div class="row">
                                <input type="button" class="btn btn-danger  btn-sm pull-right m-l-10" name="close_excel" value="Close" id="close_excel_upload" onclick="$GraphEvn.hideUploadConf();" />
                                <input type="button" class="btn btn-success btn-sm pull-right " name="save_excel" value="Save" id="save_excel_upload" onclick="return $GraphEvn.saveExcelDetails()"/>
                            </div>
                        </div>
                    </div>			
                </div>
        </div>
</div> 
<!-- End file conformation -->


<?php include_once("tpl/footer.tpl.php"); ?>

<script type="text/javascript">
    var loggedUserId = <?php echo $loggedInUserId; ?>;
    var loggedUserRole = <?php echo $loggedUserRoleId; ?>;

    $(document).ready(function () {
        $GraphEvn.init();
        $GraphEvn.search();
        $GraphEvn.fileUpload();
        $('[data-type="A"]').click();
        setInterval(function(){ 
            var $filter = $("#abcPagination .active").attr("data-type");
            if(typeof $filter != "undefined")
            {
               $GraphEvn.getDeviceData($filter,2);
            }
        }, 60000);
    });

    var $GraphEvn = {
        init : function(){
            var $data = {
                c: 'Settings',
                a: 'getfirstSwitchEle'
            };
            $.ajax({
                type: 'POST',
                url: 'ajax.php',
                data: $data,
                dataType: 'json',
                success: function (res) {
                    if(res.length > 0){
                       var devName = decodeURIComponent((res[0]["device_name"]).replace(/\+/g, " "));  
                       var firstChar = (devName.trim().charAt(0));
                       $('[data-type="'+firstChar.toUpperCase()+'"]').click();
                    }
                }
            });
        },
        search: function () {
            $("#abcPagination button").on("click", function () {
                $("#abcPagination button").removeClass("active");
                $(this).addClass("active");
                var $str = $(this).attr("data-type");
                $GraphEvn.getDeviceData($str,1);
            });
        },
        getDeviceData : function($filter,$type){//$type 1 = get device;2 = update status
            var $data = {
                    c: 'Settings',
                    a: 'getSwitchGraph',
                    filter: $filter
                };
                $.ajax({
                    type: 'POST',
                    url: 'ajax.php',
                    data: $data,
                    dataType: 'json',
                    beforeSend: function () {
                       if($type == 1) __showLoadingAnimation();
                       if($(".sw-log-time").length > 0){
                           $(".sw-log-time").html('<div class="loading"><div class="loading-bar"></div><div class="loading-bar"></div><div class="loading-bar"></div><div class="loading-bar"></div></div>');
                         
                       }
                    },
                    success: function (res) {
                        //console.log(res);
                        if (res.status == 1) {

                            var $resultData = res.result;
                            var $ciscoArr = [];
                            var $item = [];
                            var $ciscoID = 0;
                            var $devCount = 0;
                            //console.log($resultData);
                            //console.log($resultData.length);
                            if($type == 1){
                                $("#resultContainer").html("");
                                for(var $i=0; $resultData.length > $i; $i++){

                                    if($resultData[$i]["id"] != $ciscoID){

                                        if($ciscoID != 0 ){
                                            //console.log("$ciscoID =="+$ciscoID);
                                            //console.log($item);
                                            $ciscoArr[$devCount] = $item;
                                            $item = [];
                                            $devCount++; 
                                        }
                                        $item["ID"] = $resultData[$i]["id"];
                                        $item["Name"]  = decodeURIComponent(($resultData[$i]["device_name"]).replace(/\+/g, " ")); 
                                        $item["PatchPanel"]  = (($resultData[$i]["PatchPanel"] != null) ? decodeURIComponent(($resultData[$i]["PatchPanel"]).replace(/\+/g, " ")) : ""); 
                                        if($resultData[$i]["LogDate"] != null){
                                            var $dt = new Date($resultData[$i]["LogDate"]);
                                            $item["TimeLog"] = ("0" + ($dt.getMonth()+1)).slice(-2)+"/"+("0" + $dt.getDate()).slice(-2)+"/"+("0" + $dt.getFullYear()).slice(-2)+" "+("0" + $dt.getHours()).slice(-2)+":"+("0" + $dt.getMinutes()).slice(-2);
                                        }
                                        else $item["TimeLog"] = "";
                                    }

                                    var $port = $resultData[$i]["Port"];
                                    var $cableID =  $resultData[$i]["CableID"];
                                    var $operState = "default"; // NAN
                                    if($resultData[$i]["OperState"] == 1) $operState = "ok";   // ON                             
                                    else if($resultData[$i]["OperState"] == 0) $operState = "error"; // OFF

                                    if($port != null && $port != ""){
                                        //Port status
                                        $item[$port] = $operState;
                                        //ProtName
                                        if($cableID != null && $cableID != ""){
                                            $cableID = decodeURIComponent($cableID.replace(/\+/g, " "));
                                            var cableIDSplit = $cableID.split("/");
                                            $item[$port+"_cable"] = cableIDSplit[0];
                                        }
                                    }     
                                    
                                    $ciscoID = $resultData[$i]["id"];
                                }
                                //console.log("item");
                                if(Object.keys($item).length > 0) $ciscoArr[$devCount] = $item;

                                //console.log($ciscoArr);
                                //console.log("append tmpl start");
                                for (var $j = 0; $j < $ciscoArr.length; $j++) {
                                    //console.log("append tmpl");
                                    var $switchStr = tmpl("tmpl-graph", {data:$ciscoArr[$j]}); 
                                    //console.log($switchStr);
                                    $("#resultContainer").append($switchStr);
                                }
                                $(".port_tooltip").tooltip();
                            }
                            else {
                                if($resultData.length > 0){
                                    
                                    
                                    for(var $i=0; $resultData.length > $i; $i++){
                                        var $port = $resultData[$i]["Port"];
                                        var $id = $resultData[0]["id"];
                                        var $nane = decodeURIComponent(($resultData[0]["device_name"]).replace(/\+/g, " ")); 
                                        var $patchPanel  = (($item["TimeLog"] = null) ? decodeURIComponent(($resultData[0]["PatchPanel"]).replace(/\+/g, " ")) : ""); 

                                        if($( "#sw_name_"+$id ).length > 0){
                                            $( "#sw_name_"+$id).attr("title",$patchPanel);
                                            $( "#sw_name_"+$id+" .sw-name").html($nane);

                                            if($resultData[0]["LogDate"] != null){
                                                var $dt = new Date($resultData[0]["LogDate"]);
                                                $("#swLogTime_"+$id).html(("0" + ($dt.getMonth()+1)).slice(-2)+"/"+("0" + $dt.getDate()).slice(-2)+"/"+("0" + $dt.getFullYear()).slice(-2)+" "+("0" + $dt.getHours()).slice(-2)+":"+("0" + $dt.getMinutes()).slice(-2));
                                            }
                                        }
                                        var $operState = "default.png";
                                        if($resultData[$i]["OperState"] == 1) $operState = "ok.gif";                                
                                        else if($resultData[$i]["OperState"] == 0) $operState = "error.gif";
                                        if($( "#cisco_img_"+$id+"_"+$port ).length > 0){
                                            $( "#cisco_img_"+$id+"_"+$port ).attr("src","img/port-"+$operState);

                                            //ProtName
                                            if($cableID != null && $cableID != ""){
                                                $cableID = decodeURIComponent($cableID.replace(/\+/g, " "));
                                                var cableIDSplit = $cableID.split("/");
                                                 $( "#port_"+$id+"_"+$port ).attr("title",cableIDSplit[0]);
                                            }

                                        }
                                    }
                                }
                            }
                            
                        }
                        else {
                            $("#resultContainer").html("<h5 class='text-center bold m-t-20 hint-text'> No switch available.</h5>")
                        }
                    },
                    complete: function () {
                       if($type == 1) __hideLoadingAnimation();
                    },
                    error: function (r) {
                       if($type == 1) __hideLoadingAnimation();
                        console.log(r);
                    }
                });
        },
        fileUpload : function(){
            // Call the fileupload widget and set some parameters
            $('#fileupload').fileupload({
                url: 'fileUpload.php',
                singleFileUploads: true,
                dataType: 'json',
                acceptFileTypes: /(\.|\/)(xls|xlsx)$/i,
                done: function (e, data) {
                    $.each(data.result.files, function (index, file) {
                        var imageName = file.name;

                        if(file.error){
                            $createAlert({status : "fail",title : "Failed",text :file.error});
                            $('#imageCropModal').modal("hide");
                        }
                        else{
                            $('#uploadConfirmation .up-filename').html(imageName);
                            $("#uploadConfirmation .upload-progress").addClass("hide");
                            $("#uploadConfirmation .update-conf").removeClass("hide");
                            $("#hdnUploadFileName").val(imageName);
                        }
                    });
                },
                progressall: function (e, data) {
                    // Update the progress bar while files are being uploaded
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $('#uploadConfirmation .up-pre-span').html(progress);
                }
            }).bind('fileuploadadd', function (e, data) {
                var acceptFileTypes = /(\.|\/)(xls|xlsx)$/i;
				var fileName = data.originalFiles[0]['name'];
				var splitName = fileName.split(".");
				var ext = splitName[splitName.length - 1];
				ext = ext.toLowerCase();
                if( ext != 'xls' && ext !='xlsx' ) {
					console.log("test");
                    $createAlert({status : "fail",title : "Failed",text :"Not an accepted file type"});
                    return false;
                }
                else{
                    $('#uploadConfirmation').modal("show");
                    $("#uploadConfirmation .update-conf").addClass("hide");
                    $("#uploadConfirmation .upload-progress").removeClass("hide");
                    
                }
            });
        },
        saveExcelDetails : function(){
            var $fileName = $("#hdnUploadFileName").val();
            if($fileName.trim() == ""){
                $createAlert({status : "fail",title : "Failed",text :"Please select switch schedule excel sheet. "});
                return false;
            }
            var $data = {
                    c: 'Settings',
                    a: 'updateSwitchSchedule',
                    fileName: encodeURI($fileName)
                };
                $.ajax({
                    type: 'POST',
                    url: 'ajax.php',
                    data: $data,
                    dataType: 'json',
                    beforeSend: function () {
                       __showLoadingAnimation();
                    },
                    success: function (res) {
                        if(res.Status == 1){
                            $('#uploadConfirmation').modal("hide"); 
                            $createAlert({status : "success",title : "Success",text :"successfully updated records"});
                            
                            //Refresh switch
                             var $filter = $("#abcPagination .active").attr("data-type");
                            $GraphEvn.getDeviceData($filter,1);
                        }
                        else{
                            $('#uploadConfirmation').modal("hide"); 
                            $createAlert({status : "fail",title : "Failed",text :"Failed to updated records"});
                        }
                    },
                    complete: function () {
                        __hideLoadingAnimation();
                    },
                    error: function (r) {
                       __hideLoadingAnimation();
                        $createAlert({status : "fail",title : "Failed",text :r});
                    }
                });
        },
        hideUploadConf : function(){
            $('#uploadConfirmation').modal("hide");
            var $fileName = $("#hdnUploadFileName").val();
            if($fileName.trim() == ""){ return false; }
            var $data = {
                c: 'Settings',
                a: 'removeExistFile',
                fileName: encodeURI($fileName)
            };
            $.ajax({
                type: 'POST',
                url: 'ajax.php',
                data: $data,
                dataType: 'json',
                success: function (res) {
                    
                },
                error: function (r) {
                    console.log(r);
                }
            });
        },
        editSwitchName:function($id){
            var $name = $("#sw_name_"+$id+" .sw-name").html();
            $("#sw_name_"+$id).addClass("hide");
            $("#sw_edit_"+$id).removeClass("hide");
            $("#sw_edit_"+$id).find(".sw-edit-input").val($name);
        },
        saveName:function($id){
             var $name = $("#sw_edit_"+$id).find(".sw-edit-input").val();
             if($name.trim() == ""){
                $createAlert({status : "fail",title : "Failed",text :"Please enter name. "});
                return false;
            }
            var $data = {
                c: 'Settings',
                a: 'saveSwitchName',
                name: $name.trim(),
                deviceID:$id
            };
            $.ajax({
                type: 'POST',
                url: 'ajax.php',
                data: $data,
                dataType: 'json',
                beforeSend: function () {
                   __showLoadingAnimation();
                },
                success: function (res) {
                    if(res.Status){
                        $createAlert({status : "success",title : "Success",text :"successfully updated name"});
                        $("#sw_name_"+$id+" .sw-name").html($name)
                        //$("#sw_name_"+$id).html($name + ' <span onclick="$GraphEvn.editSwitchName('+$id+');"><i class="fa fa-pencil hint-text text-master" aria-hidden="true"></i></span>');
                        $GraphEvn.cancelEdit($id);
                    }
                    else{
                        $createAlert({status : "fail",title : "Failed",text :"Failed to updated name"});
                    }
                },
                complete: function () {
                    __hideLoadingAnimation();
                },
                error: function (r) {
                   __hideLoadingAnimation();
                    $createAlert({status : "fail",title : "Failed",text :r});
                }
            });
        },
        cancelEdit:function($id){
            $("#sw_edit_"+$id).addClass("hide");
            $("#sw_name_"+$id).removeClass("hide");
        }
    };
</script>

<script type="text/x-tmpl" id="tmpl-graph">
    {% var ports = []; %}
    {% for ( var i = 1; i < 49; i++) { %}
         {% var portStatus = "default"; %}
         {% var PortName = ""; %}
         {% if (i in o.data) { %}
            {% portStatus = o.data[i]; %}
        {% } %}
        {% if ((i+"_cable") in o.data) { %}
            {% PortName = o.data[(i+"_cable")]; %}
        {% } %}
        {% ports[i] = portStatus; %}
        {% ports[i+"_Name"] = PortName; %}
    {% } %}
    <div class="switch-wrapper m-b-15" id="sw_{%=o.data['ID'] %}">
        <h5 class="m-b-0 m-t-0 bold fs-16 port_tooltip" id="sw_name_{%=o.data['ID'] %}" data-toggle="tooltip" data-placement="left" title="{%=o.data['PatchPanel'] %}"><span class="sw-name">{%=o.data["Name"] %}</span>
            <span onclick="$GraphEvn.editSwitchName({%=o.data['ID'] %});"><i class="fa fa-pencil hint-text text-master" aria-hidden="true"></i></span>
        </h5>
        <div class="row hide" id="sw_edit_{%=o.data['ID'] %}">
            <div class="col-sm-10 col-xs-12 col-md-4 col-lg-4 col-xl-4">
                <input type="text" class="full-width sw-edit-input form-control" value="">
            </div>
            <div class="col-sm-2 col-xs-2 col-md-2 col-lg-2 col-xl-2 p-l-0 m-t-5">
                <input type="button" class="btn btn-default btn-xs pull-left m-r-10" value="Save" onclick="return $GraphEvn.saveName({%=o.data['ID'] %})"/>
                <input type="button" class="btn btn-default  btn-xs pull-left" value="Close" onclick="return $GraphEvn.cancelEdit({%=o.data['ID'] %})"/>
            </div>
        </div>
        <div class="sw-log-time port_tooltip" id="swLogTime_{%=o.data['ID'] %}"  data-toggle="tooltip" data-placement="left" title="Last updated on">
            <span>{%=o.data['TimeLog'] %}</span>
        </div>
        <table class="m-t-10">
            <tr>
                {% var rowInc = 1; %}
                {% var portNumber = 0; %}
                
                {% for (var i = 0; ( i < 6 && rowInc <= 2); i++) { %}
                    <td>
                        <table class="m-r-5">
                            {% for (var k = 0; ( k < 2); k++) { %}
                                <tr>
                                    {% var portCounter = 0; %}
                                    {% portCounter = portNumber;  %}       
                                    {% for (var j = 0; ( j < 4); j++) {  %}
                                        {% if (k == 0){  %}
                                            {% portCounter = j == 0 ? portCounter + 1 : portCounter + 2;  %}
                                        {% } else { %}
                                            {% portCounter = portCounter + 2;  } %}
                                        <td>
                                            <div class="port port-row-{%=k %} port_tooltip" id="port_{%=o.data['ID'] %}_{%=portCounter%}" data-toggle="tooltip" data-placement="top" title="{%=ports[(portCounter+'_Name')]%}">
                                                <div class="port-number port-number-{%=ports[portCounter] %}>">{%=portCounter %}</div>
                                                <img src="img/port-{%=ports[portCounter] %}.gif" width="100%" id="cisco_img_{%=o.data['ID'] %}_{%=portCounter%}">
                                            </div>
                                        </td>
                                    {% } %}
                                </tr>
                                {% if(k == 1) { %}
                                    {% portNumber = portCounter; %}
                                {% } %}
                            {% } %}
                        </table>
                    </td>
                {% } %}
            </tr>
        </table>
    </div>
</script>

