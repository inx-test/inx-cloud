<?php
//Reduce errors
error_reporting(~E_WARNING);

// Inspextor Listern Thread Count
define('INX_PACKETREC_THREAD_COUNT',50);

// ACK Retry Count
define('INX_ACK_RETRY_COUNT',3);

// ACK Retry Interval in Seconds
define('INX_ACK_RETRY_INTERVAL',1);

//Listen IP
define('INX_LISTENER_IP','0.0.0.0');

// UDP Ports
define('INX_LISTENER_PORT',10025);
//define('NODE_LISTENER_PORT',10026);
define('NODE_LISTENER_PORT',10025);

// Packet Types
define('PT_REQUESTS',21); // Request
define('PT_STATUS',22); // Status
define('PT_COMMANDS',23); // Commands
define('PT_EVENTS',25); // Events
define('PT_POLICY',27); // Policy
define('PT_ACK',29); // ACK
//rx_term 197
//tx_term 199
// Commands
define('LIGHT_ON',110); // Direct command of turn the light ON
define('LIGHT_OFF',111); // Direct Command to turn the light OFF
define('LIGHT_FLASH',112); // num of flash		LIGHT FLASH
define('LIGHT_DIM',113);	// dim level % of max	LIGHT DIM
define('LIGHT_BRIGHTER',114);	// raise level by 1 %
define('LIGHT_DARKER',115);	// raise level by 1 %
define('SET_TAG',101); // Assign Tag to Node
define('NODE_TAG_REQUEST',102); // Node is requesting a tag
define('GET_NODE_STATUS',108); // Get node status __Tem ID
define('PT_ACK_SHORT',199); // ACK SHORT
define('RESTORE_TO_DEFAULT',159); // Restore to default
define('CLEAR_TAG',104); // clear tag
define('TEST_SENSOR',106); // clear tag

define('IP_CHANGE',109);		// AK_TAG added
define('RX_TERM',197); // recive terminal
define('TX_TERM',198); // transiter terminal

define('SET_TIME',120);		// h,m,s      	// Set System Time HH:MM
define('SET_DATE',121);		// y,m,d 		// Set System Date MM:DD:YY
define('GET_TIME',122);		// get_time	// Get System Time HH:MM
define('GET_DATE',123);	// get_date 	// Get System Date MM:DD:YY
define('GET_STATUS',124);		//// GET STATUS REQUEST TO NODE
define('GET_LOGS',125);		//
define('GET_SERIAL_NUM',126);       //

define('GET_SAVING',140);		//
define('SET_SAVING',141);       //
define('SET_TIME_DATE',136);		// h,m,s      	// Set System Time HH:MM		AK_TAG SET_TIME_DATE
define ('SET_FIXTURE_TYPE',160);
define('SET_DEF_DIM',184);
define('SET_COLOR',186);	//autotune AK 
//Events array
define('MOTION_DETECTED',201);
define('SILENCE_DETECTED',202);
define('EV_MOTION',203);
define('USER_EVENT',204);
define('OVERRIDE_1',205);
define('OVERRIDE_2',206);
define('OVERRIDE_3',207);
define('UP_BUTTON',208);
define('DOWN_BUTTON',209);
define('ON_BUTTON',210);
define('CANCEL_BUTTON',211);
define('EV_OVERRIDE',213);
define('START_TIMER',214);
define('EV_CMD_ACTION',98);		// was 99 AK 12/20/17

define('SET_SENSOR_TYPE',147);
//Policy
define('CREATE_POLICY',150);
define('ADD_POLICY_LINE',151);
define('REMOVE_POLICY_LINE',152);
define('REMOVE_AT_POLICY',153);
define('CHANGE_POLICY',154);
define('CHANGE_TIMED_POLICY',155);
define('ADD_AT_POLICY',156);
define('DEFAULT_POLICY',0);

//Remote program 
define('LOAD_PROGRAM',180);
define('ADD_PROGRAM_LINE',181);
define('CREATE_PROGRAM',182);
define('ACK_PROGRAM_LINE',183);
define('HEX_LINE_ERR',196);

// Packet High Value
define('PACKET_ID_MAX',255);
define('PACKET_ID_MIN',50);

// Packet Positions
define("BYTE_POSITION_PREAMBLE",1);
define("BYTE_POSITION_START_BYTE",2);
define("BYTE_POSITION_START_PACKET_ID",3);
define("BYTE_POSITION_START_TAG_HIGH_BYTE",4);
define("BYTE_POSITION_START_TAG_LOW_BYTE",5);
define("BYTE_POSITION_CLUSTER_ID",6);
//define("BYTE_POSITION_BYTES_TO_FOLLOW",6);
define("BYTE_POSITION_BYTES_TO_FOLLOW",16);
define("BYTE_POSITION_PACKET_TYPE",17);
define("BYTE_POSITION_COMMAND_PARAM",18);

// Is it a broadcase or regular packet
define("UDP_NO_BROADCAST_PACKET",0);
define("UDP_BROADCAST_PACKET",0);
define("CHECK_PACKET_CS",false);

// Fixture Types
define('FIXTURE_TYPE_NODE',1);
define('FIXTURE_TYPE_DRIVERA',2);
define('FIXTURE_TYPE_DRIVERB',3);
define('FIXTURE_TYPE_WALLSWITCH',4);
define('FIXTURE_TYPE_OCCUPANCYSENSOR',5);


//$GLOBALS["FX_TYPE_ARR"] = $fixtureTypeArr; 
//echo $fixtureTypeArr;
// UDP Constant Values
define('UDP_RECEIVE',1);
define('UDP_SEND',2);
define('UDP_RETRY_COUNT',5);

//Event message placeholder
define('EVENT_MSG_PH',"[TITLE]-[SN]/[IP] ([FT])");

//Direction Arr
$devDirectionArr = array();
$devDirectionArr[1]="north";
$devDirectionArr[2]="south";
$devDirectionArr[3]="east";
$devDirectionArr[4]="west";
define('DEV_DIRECTION_ARR',  serialize($devDirectionArr));


//Event title arr
$devEventArr = array();
$devEventArr[EV_MOTION]="Motion detected ";
$devEventArr[SILENCE_DETECTED]="Silence detected";
$devEventArr[USER_EVENT]="User event";
$devEventArr[OVERRIDE_1]="Override 1";
$devEventArr[OVERRIDE_2]="Override 2";
$devEventArr[OVERRIDE_3]="Override 3";
$devEventArr[UP_BUTTON]="Up Button";
$devEventArr[DOWN_BUTTON]="Down butoon";
$devEventArr[ON_BUTTON]="On button";
$devEventArr[CANCEL_BUTTON]="Cancel button";
define('DEV_EVENT_MSG',  serialize($devEventArr));

//Packet message 
define('PCK_EVN_ACK_SENT_FAILED_MSG',"Failed to send acknowledge.");
define('PCK_SENT_FAILED',"Packet senting failed.");
define('PCK_EVENT_SENT_FAILED',"Event packet senting failed.");
define('PCK_INVALID_TITLE',"Invalid packet.");
define('PCK_INVALID_MSG',"Invalid packet received.");
define('PCK_INVALID_COMD_MSG',"Unkown packet command received.");


?>
