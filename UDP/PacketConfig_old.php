<?php
//Reduce errors
error_reporting(~E_WARNING);

// Inspextor Listern Thread Count
define('INX_PACKETREC_THREAD_COUNT',10);

// ACK Retry Count
define('INX_ACK_RETRY_COUNT',3);

// ACK Retry Interval in Seconds
define('INX_ACK_RETRY_INTERVAL',1);

//Listen IP
define('INX_LISTENER_IP','0.0.0.0');

// UDP Ports
define('INX_LISTENER_PORT',10025);
define('NODE_LISTENER_PORT',10026);

// Packet Types
define('PT_REQUESTS',21); // Request
define('PT_STATUS',22); // Status
define('PT_COMMANDS',23); // Commands
define('PT_EVENTS',25); // Events
define('PT_POLICY',27); // Policy
define('PT_ACK',29); // ACK
define('PT_ACK_SHORT',199); // ACK SHORT

// Commands
define('LIGHT_ON',210); // Direct command of turn the light ON
define('LIGHT_OFF',211); // Direct Command to turn the light OFF
define('LIGHT_FLASH',112); // num of flash		LIGHT FLASH
define('LIGHT_DIM',113);	// dim level % of max	LIGHT DIM
define('LIGHT_BRIGHTER',208);	// raise level by 1 %
define('LIGHT_DARKER',209);	// raise level by 1 %
define('SET_TAG',101); // Assign Tag to Node
define('NODE_TAG_REQUEST',102); // Node is requesting a tag
define('GET_NODE_STATUS',108); // Get node status __Tem ID//151

define('SET_TIME',120);		// h,m,s      	// Set System Time HH:MM
define('SET_DATE',121);		// y,m,d 		// Set System Date MM:DD:YY
define('GET_TIME',122);		// get_time	// Get System Time HH:MM
define('GET_DATE',123);	// get_date 	// Get System Date MM:DD:YY
define('GET_STATUS',124);		//// GET STATUS REQUEST TO NODE
define('GET_LOGS',125);		//
define('GET_SERIAL_NUM',126);       //

//Events array
define('MOTION_DETECTED',201);
define('SILENCE_DETECTED',202);
define('USER_EVENT',204);
define('OVERRIDE_1',205);
define('OVERRIDE_2',206);
define('OVERRIDE_3',207);
define('UP_BUTTON',208);
define('DOWN_BUTTON',209);
define('ON_BUTTON',210);
define('CANCEL_BUTTON',211);

//Policy
define('CREATE_POLICY',150);
define('ADD_POLICY_LINE',151);
define('REMOVE_POLICY_LINE',152);
define('PILICY_INFO_REQ',153);
define('CHANGE_POLICY',154);
define('CHANGE_TIMED_POLICY',155);
define('MODIFY_POLICY',156);

// Packet High Value
define('PACKET_ID_MAX',255);
define('PACKET_ID_MIN',5);

// Packet Positions
define("BYTE_POSITION_PREAMBLE",1);
define("BYTE_POSITION_START_BYTE",2);
define("BYTE_POSITION_START_PACKET_ID",3);
define("BYTE_POSITION_START_TAG_HIGH_BYTE",4);
define("BYTE_POSITION_START_TAG_LOW_BYTE",5);
define("BYTE_POSITION_BYTES_TO_FOLLOW",6);
define("BYTE_POSITION_PACKET_TYPE",7);
define("BYTE_POSITION_COMMAND_PARAM",8);

// Is it a broadcase or regular packet
define("UDP_NO_BROADCAST_PACKET",0);
define("UDP_BROADCAST_PACKET",0);
define("CHECK_PACKET_CS",false);

// Fixture Types
define('FIXTURE_TYPE_NODE',1);
define('FIXTURE_TYPE_DRIVERA',2);
define('FIXTURE_TYPE_DRIVERB',3);
define('FIXTURE_TYPE_WALLSWITCH',4);
define('FIXTURE_TYPE_OCCUPANCYSENSOR',5);

$fixtureTypeArr[FIXTURE_TYPE_NODE]["Namae"]="Node";$fixtureTypeArr[FIXTURE_TYPE_NODE]["ShortName"]="ND";
$fixtureTypeArr[FIXTURE_TYPE_DRIVERA]["Namae"]="Driver A";$fixtureTypeArr[FIXTURE_TYPE_DRIVERA]["ShortName"]="DR_A";
$fixtureTypeArr[FIXTURE_TYPE_DRIVERB]["Namae"]="Driver B";$fixtureTypeArr[FIXTURE_TYPE_DRIVERB]["ShortName"]="DR_B";
$fixtureTypeArr[FIXTURE_TYPE_WALLSWITCH]["Namae"]="Wall switch";$fixtureTypeArr[FIXTURE_TYPE_WALLSWITCH]["ShortName"]="WS";
$fixtureTypeArr[FIXTURE_TYPE_OCCUPANCYSENSOR]["Namae"]="Occupency sensor";$fixtureTypeArr[FIXTURE_TYPE_OCCUPANCYSENSOR]["ShortName"]="OS";
$GLOBALS["FX_TYPE_ARR"] = $fixtureTypeArr;

// UDP Constant Values
define('UDP_RECEIVE',1);
define('UDP_SEND',2);
define('UDP_RETRY_COUNT',3);

//Event message placeholder
define('EVENT_MSG_PH',"[TITLE]-[SN]/[IP] ([FT])");

//Direction Arr
$devDirectionArr[1]="north";
$devDirectionArr[2]="south";
$devDirectionArr[3]="east";
$devDirectionArr[4]="west";
$GLOBALS["DEV_DIRECTION_ARR"] = $devDirectionArr;


//Event title arr
$devEventArr[MOTION_DETECTED]="Motion detected ";
$devEventArr[SILENCE_DETECTED]="Silence detected";
$devEventArr[USER_EVENT]="User event";
$devEventArr[OVERRIDE_1]="Override 1";
$devEventArr[OVERRIDE_2]="Override 2";
$devEventArr[OVERRIDE_3]="Override 3";
$devEventArr[UP_BUTTON]="Up Button";
$devEventArr[DOWN_BUTTON]="Down butoon";
$devEventArr[ON_BUTTON]="On button";
$devEventArr[CANCEL_BUTTON]="Cancel button";
$GLOBALS["DEV_EVENT"] = $devEventArr;

?>
