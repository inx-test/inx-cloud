<?php
/*--------------------------------------------------------------
* From Packet Format Doc:
*
Packet Format:
1) 255				preamble
2) 0X55			Start Byte
3) packet ID			pkt_id		// this is a rolodex ID 255
4) device Tag ID high	byte	Tg_id_hb
5) device Tag ID low byte 	Tg_id_lb
6) Number of bytes to follow.
  a) Packet Type	 		pkt_type
  b) Packet Parameters	n
  c) Packet Data (specific to command or event)	b1, b2, b3 ...
7) CS: check sum		tx_cs
--------------------------------------------------------------*/

require_once('UDP/UDP.php');

class PMIPacket{

      // Packet fields
      public $preamble = 255; // Default 255
      public $startByte = 85; // Default 85
      public $packetId = PACKET_ID_MIN; // Default 5
      public $tagIdHighByte = 255; // Default 0
      public $tagIdLowByte = 255; // Default 0
      public $numberOfBytes = 0; // Default 0
      public $packetType = 0; //
      public $command = 0; // Default
      public $commandParam = array(); // Default parameters
      public $checksum = 0; // Default 0
      public $checksumMatch = false; // True or False

      // Final Binary Packet
      private $packetBinary = NULL; // Packed binary data

      // Parsed Decimal Array
      private $packetDecimal = array(); // Parsed packet

      // Calculated Checksum
      private $checksumCalculated = 0;
      
      // Check if packed or not 
      private $isDecSeeded = false; 


      // Other Parsed Fields
      public $ipAddress = ""; // IP Address of Node
      public $serialNumber = ""; // Serial Number of NODE, OC, WS, DRVS
      public $fixtureType = ""; // Fixture types
      public $numberOfTags = ""; // Number of tags
      // ACK
	  public $ackPacketId = 0; 
	  
      // Event
      public $eventDirection = ""; // 1-4
      public $eventParamOther = "";
      
        //Status
        public $ST_statusTime=""; 
        public $ST_statusMonth = 0;
        public $ST_statusDate = 0; 
        public $ST_statusYear = 0;
        public $ST_statusHour = 0;
        public $ST_statusMinute = 0;
        public $ST_statusSecond =  0;
        public $ST_DOW=0; 
        public $ST_VoltageMasterHiByte = 0; 
        public $ST_voltageMasterLoByte = 0;
        public $ST_voltageSubHiByte = 0; 
        public $ST_voltageSubLoByte = 0;
        public $ST_nodeTemp = 0; 
        public $ST_policyActive = 0;
        public $ST_nodeStatusBit = 0;
        public $ST_driverCurrentHiByte = 0; 
        public $ST_driverCurrentLoByte = 0; 
        public $ST_driverSubCurrentHiByte = 0;
        public $ST_driverSubCurrentLoByte = 0;
        public $ST_driverTemp = 0; 
        public $ST_driverStatusBit = 0;
        public $ST_WSTemp = 0; 
        public $ST_WSStatusBit = 0;
        public $ST_OC1Tem = 0; 
        public $ST_OC1StatusBit = 0;
      


      // Construct with Binary Data
      function __construct( $binary, $packetArr=array() ) {
          // Check if binary provided
          if( isset($binary) && $binary!= NULL ) {
              // Not parameter
              $this->packetBinary = $binary;

              // Parse Data
              $this->__parse();
          }
          else if ( isset($packetArr) & sizeof($packetArr) > 0 ) {
                //echo "data setting ... \n\n";
                $this->ipAddress = $packetArr["ipAddress"]; // Optional
                $this->serialNumber =  $packetArr["serialNumber"]; // Optional
                $this->fixtureType = $packetArr["fixtureType"]; // Optional
                
                $this->preamble = $packetArr["preamble"]; // Optional
                $this->startByte = $packetArr["startByte"]; // Optional
          }
      }

      // Get As Binary
      public function getBinary(){
        // pack to byte
        $this->__packb();

        // Return binary
        return $this->packetBinary;
      }

      // Get Decimal Representation
      public function getDecimal(){
         if( !$this->isDecSeeded ) $this->__seedDecimal();
         
         if( sizeof($this->packetDecimal) > 0 ){
            return implode( ",", $this->packetDecimal );
         }
      }
   
     public function setPacketType($type)
     {
         $this->commandParam[0] = $type;
     }
      
     public function setCommand($command)
     {
         $this->commandParam[1] = $command;
     }
     
      // Set tag of this packet
      public function setTag($tagId)
      {
          $this->commandParam[2] = UDP::getHiByte($tagId);
          $this->commandParam[3] = UDP::getLoByte($tagId);
      }
	  public function setPeripheralType($tagType){
		 $this->commandParam[4] = $tagType;
	  }
      // Event of this packet
      public function setEvent($parameter1,$parameter2)
      {
          $this->commandParam[2] = $parameter1;
          $this->commandParam[3] = $parameter2;
      }
      //set tag id low and high byte
      public function setLowHightByte($tagId)
      {
          $this->tagIdHighByte = UDP::getHiByte($tagId);
          $this->tagIdLowByte = UDP::getLoByte($tagId);
      }
      
      
      ///////////////////////////////////////////////////////////
      // Private Methods
      private function __packc($dec){
          return pack('C*',$dec);
      }
      
      // Fill Decimal 
     private function __seedDecimal(){
          
          $this->numberOfBytes = sizeof($this->commandParam);
          
          // Combine packet
          $this->packetDecimal =  array();
          array_push( $this->packetDecimal,$this->preamble );
          array_push( $this->packetDecimal,$this->startByte );
          array_push( $this->packetDecimal,$this->packetId );
          array_push( $this->packetDecimal,$this->tagIdHighByte );
          array_push( $this->packetDecimal,$this->tagIdLowByte );
          array_push( $this->packetDecimal,$this->numberOfBytes );

          // Combine parameters
          if( sizeof($this->commandParam) > 0  ){
            foreach( $this->commandParam as $cp => $param ) {
                array_push( $this->packetDecimal,$param );
            }
          }

          // Calculate Checksum & Convert to Binary
          $this->checksum = 0;
          foreach( $this->packetDecimal as $index => $data ){

              // Find checksum
              $packetCharsArray = str_split($data);
              for( $i=0; $i < sizeof($packetCharsArray); $i++ ){
                    $this->checksum += $packetCharsArray[$i];
              }
          }
          
          array_push( $this->packetDecimal,$this->checksum );
          
          // Mark as seeded 
          $this->isDecSeeded = true; 
     }

      // Packet to Byte
      // Get Binary
      private function __packb(){
          if( !$this->isDecSeeded ) $this->__seedDecimal();
          
          // Reset to null 
          $this->packetBinary = NULL; 
          
          // Calculate Checksum & Convert to Binary
          foreach( $this->packetDecimal as $index => $data ){
              // Add to binary
              $this->packetBinary .= $this->__packc($data);
          }       
      }

      // Parse Packet & Set fields
      private function __parse(){

          // Default status
          $result = false;

          try
          {
              if( $this->packetBinary!= NULL ){
                    $this->packetDecimal = unpack( "C*", $this->packetBinary );
                    if( sizeof($this->packetDecimal) > 0 ){
echo "Packet Contents: \n";
					print_r( $this->packetDecimal) ;

                        $this->preamble = $this->packetDecimal[BYTE_POSITION_PREAMBLE];
                        $this->startByte = $this->packetDecimal[BYTE_POSITION_START_BYTE];
                        $this->packetId = $this->packetDecimal[BYTE_POSITION_START_PACKET_ID];
                        $this->tagIdHighByte = $this->packetDecimal[BYTE_POSITION_START_TAG_HIGH_BYTE];
                        $this->tagIdLowByte = $this->packetDecimal[BYTE_POSITION_START_TAG_LOW_BYTE];
                        $this->numberOfBytes = $this->packetDecimal[BYTE_POSITION_BYTES_TO_FOLLOW];

                        // Loop & follow bytes
                        $followedBytes = 0;
                        $followByteIndex = BYTE_POSITION_BYTES_TO_FOLLOW + 1;
                        $this->commandParam = array();

                        while( $followedBytes < $this->numberOfBytes ){
                            // Read Content
                            $packetData = $this->packetDecimal[$followByteIndex];

                            // Packet Type
                            if( $followByteIndex == BYTE_POSITION_PACKET_TYPE ) $this->packetType = $packetData;

                            // Packet Command
                            else if( $followByteIndex == BYTE_POSITION_COMMAND_PARAM ) $this->command = $packetData;

                            // Push to data array
                            else array_push($this->commandParam,$packetData);


                            $followedBytes++;
                            $followByteIndex++;
                        }

                        // Parse Parameters
                        $this->__setPacketFields();

                        // Checksum
                        $this->checksum = $this->packetDecimal[ sizeof($this->packetDecimal)-1 ];


                        // Calculate checksum
                        $this->checksumCalculated = 0;
                        for( $i=1; $i < sizeof($this->packetDecimal)-1;  $i++ ){

                            // Find checksum
                            $packetCharsArray = str_split( $this->packetDecimal[$i] );
                            foreach( $packetCharsArray as $k=>$v ){
                                  $this->checksumCalculated += $v;
                            }
                        }

                        // Check checksum validation
                        if( $this->checksum == $this->checksumCalculated ) $this->checksumMatch = true;

                    }
                    else {
                        throw new Exception("Failed to parse UDP packet. Packet is empty.");
                    }
              }

          }
          catch(Exception $ex){
              throw new Exception("Failed to parse UDP packet. Error: ".$e->getMessage() );
          }
      }


      // Read & Set Parameters based on Packet Type
      private function __setPacketFields(){
            $byteOrder = 0;
            switch($this->command){

				case PT_ACK_SHORT:
					$this->ackPacketId = $this->commandParam[$byteOrder++]; // Packet ACK ID
					break;
                case NODE_TAG_REQUEST:
                    // IP Address
                    $this->ipAddress = implode('.', array_slice($this->commandParam, $byteOrder, 4));
                    $byteOrder = $byteOrder+4;


                    // Serial Number
                    $serialByte1 = $this->commandParam[$byteOrder++]; // Serial Bytes
                    $serialByte2 = $this->commandParam[$byteOrder++]; // Serial Bytes
                    $serialByte3 = $this->commandParam[$byteOrder++]; // Serial Bytes
                    $serialByte4 = $this->commandParam[$byteOrder++]; // Serial Bytes


                    $serialNumber1 = ( $serialByte1 * 256 ) + $serialByte2;
                    $serialNumber2 = ( $serialByte3 * 256 ) + $serialByte4;
                    $this->serialNumber = ( $serialNumber1 * 256) + $serialNumber2;

                    //Fixture Type
                    $this->fixtureType = $this->commandParam[$byteOrder++]; // Fixture Type

                    // Number of Tags
                    $this->numberOfTags = $this->commandParam[$byteOrder++]; // Fixture Type
                    break;
                case MOTION_DETECTED:
                case SILENCE_DETECTED:
                case USER_EVENT:
                case OVERRIDE_1:
                case OVERRIDE_2:
                case OVERRIDE_3:
                case UP_BUTTON:
                case DOWN_BUTTON:
                case ON_BUTTON:
                case CANCEL_BUTTON:
                    // IP Address
                    $this->eventDirection = $this->commandParam[$byteOrder++];
                    $this->eventParamOther = $this->commandParam[$byteOrder++];
                    break;
                case GET_NODE_STATUS;
                    //Status
                    $this->ST_statusYear = $this->commandParam[$byteOrder++];
                    $this->ST_statusMonth = $this->commandParam[$byteOrder++];
                    $this->ST_statusDate = $this->commandParam[$byteOrder++]; 
                    
                    $this->ST_DOW = $this->commandParam[$byteOrder++];
                    
                    $this->ST_statusHour = $this->commandParam[$byteOrder++];
                    $this->ST_statusMinute = $this->commandParam[$byteOrder++]; 
                    $this->ST_statusSecond =  $this->commandParam[$byteOrder++]; 
                    
                    $this->ST_VoltageMasterHiByte = $this->commandParam[$byteOrder++];
                    $this->ST_voltageMasterLoByte = $this->commandParam[$byteOrder++];
                    $this->ST_voltageSubHiByte = $this->commandParam[$byteOrder++];
                    $this->ST_voltageSubLoByte = $this->commandParam[$byteOrder++];
                    
                    $this->ST_nodeTemp = $this->commandParam[$byteOrder++];
                    $this->ST_policyActive = $this->commandParam[$byteOrder++];
                    $this->ST_nodeStatusBit = $this->commandParam[$byteOrder++];
                    
                    $this->ST_driverCurrentHiByte = $this->commandParam[$byteOrder++];
                    $this->ST_driverCurrentLoByte = $this->commandParam[$byteOrder++];
                    $this->ST_driverSubCurrentHiByte = $this->commandParam[$byteOrder++];
                    $this->ST_driverSubCurrentLoByte = $this->commandParam[$byteOrder++];
                    
                    $this->ST_driverTemp = $this->commandParam[$byteOrder++];
                    $this->ST_driverStatusBit = $this->commandParam[$byteOrder++];
                    $this->ST_WSTemp = $this->commandParam[$byteOrder++];
                    $this->ST_WSStatusBit = $this->commandParam[$byteOrder++];
                    
                    $this->ST_OC1Tem = $this->commandParam[$byteOrder++];
                    $this->ST_OC1StatusBit = $this->commandParam[$byteOrder++];
                    break;
            }

      }

}

?>
