<?php
// Class which handle all UDP packet functions
class UDP {
    
    

  // Available Commands
  //var $commands = array(LIGHT_ON=>"ON", LIGHT_OFF=>"OFF",LIGHT_FLASH=>"Flash",LIGHT_DIM=>"Dim",LIGHT_BRIGHTER=>"UP",LIGHT_DARKER=>"DOWN");
 /* var $commands = array(LIGHT_ON=>"Turn the light ON", LIGHT_OFF=>"Turn the light OFF",LIGHT_FLASH=>"Light Flash",LIGHT_DIM=>"Light Dim",LIGHT_BRIGHTER=>"Light Brighter",LIGHT_DARKER=>"Light Darker");*/
   var $commands = array(ON_BUTTON=>"Light ON", CANCEL_BUTTON=>"Light OFF",LIGHT_FLASH=>"Light Flash",LIGHT_DIM=>"Light Dim",LIGHT_BRIGHTER=>"Light Brighter",LIGHT_DARKER=>"Light Darker");
  // Packet
  var $packetId = PACKET_ID_MIN;

  // UDP socket
  var $sock = NULL;


  //*** Private Methods
  // Generate Random Packet ID
	// Cant be random it has to be sequential so packet_ID doesnt dublicate
  public function getPacketId(){
      $this->packetId++;
      if( $this->packetId > PACKET_ID_MAX ) $this->packetId = PACKET_ID_MIN;
      return $this->packetId;
  }


  //*** Public Methods
  // Send Packet
  public function send($ip, $port,$packet){
      //print_r($packet);
      // The response
      $status = false;

      // Open Connection
      //$fp = fsockopen($ip, $port, $errno, $errstr, 30);
      $fp = stream_socket_client("udp://$ip:$port", $errno, $errstr);
	  
	  //echo "Send Ip Address :::: $ip \n";

      // Check for error
      if (!$fp) {
          echo "UDP Connection Error: $errstr ($errno) \n";
      }
      else {
          // Write to stream as binary
          if( $packet != NULL) {
              
              //echo "Send:::::::: ".$packet->getDecimal();
              // Get Binary Data from packet
              $binaryData = $packet->getBinary();
              // Write to stream
              
              fwrite($fp, $binaryData);
              
              
              // Set status
              $status =  true;
          }

          // Close connection
          fclose($fp);

      } // End Else

      // Finally return result
      return $status;

  } // End Function
  
  //UDP Broadcast
  public function broadcast($ip,$packet,$port){
        $sock = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP); 

        $status =  false;	
	  
        // Check for error
        if (!$sock) {
            echo "UDP Connection Error: $errstr ($errno) \n";
        }
        else {
            // Write to stream as binary
            if( $packet != NULL) {
              
                //echo "Send:::::::: ".$packet->getDecimal();
                // Get Binary Data from packet
                $binaryData = $packet->getBinary();
                // Write to stream
              
                socket_set_option($sock, SOL_SOCKET, SO_BROADCAST, 1); 
                socket_sendto($sock, $binaryData, strlen($binaryData), 0, $ip, $port); 
              
              
                // Set status
                $status =  true;
            }
        } // End Else
		  
    return $status; 
  }
  /*public function broadcast($ip,$packet,$port){
	  $sock = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP); 
		
	  $status =  false;	
	  
		// Check for error
      if (!$sock) {
          echo "UDP Connection Error: $errstr ($errno) \n";
      }
      else {
          // Write to stream as binary
          if( $packet != NULL) {
              
              //echo "Send:::::::: ".$packet->getDecimal();
              // Get Binary Data from packet
              $binaryData = $packet->getBinary();
              // Write to stream
              
              socket_set_option($sock, SOL_SOCKET, SO_BROADCAST, 1); 
			  socket_sendto($sock, $binaryData, strlen($binaryData), 0, $ip, $port); 
              
              
              // Set status
              $status =  true;
          }


      } // End Else
		  
	  return $status; 
  }*/

  // UDP Listener
  public function listen($ip, $port, $onPacket){

    //Create a UDP socket
    if(!($this->sock = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP)))
    {
        $errorcode = socket_last_error();
        $errormsg = socket_strerror($errorcode);

        throw new Exception("Couldn't create socket: [$errorcode] $errormsg");
    }
    echo "Socket created \n";


    // Bind the source address
    if( !socket_bind($this->sock, $ip, $port) )
    {
        $errorcode = socket_last_error();
        $errormsg = socket_strerror($errorcode);

        throw new Exception("Could not bind socket : [$errorcode] $errormsg :(");
    }
    echo "Socket bind to Port: ".$port." OK :) \n";

    // Listen
    //Do some communication, this loop can handle multiple clients
    while(1)
    {
        //Receive some data
        $r = socket_recvfrom($this->sock, $buf, 512, 0, $remoteIp, $remotePort);

        // Callback
        $onPacket($remoteIp,$remotePort,$buf);

    } // End While


    // Close very rare
    socket_close($this->sock);
  }


  // Get Available Commands
  public function getCommandList(){
      return $this->commands;
  }

  // Get High Byte
  public static function getHiByte($dec){
  	 $intVal =  intval($dec);
	   $places = 8;
	   return $intVal >> $places;
  }

  // Get Low Byte
  public static function getLoByte($dec){
	   $intVal =  intval($dec);
	   $places = 0xFF;
	   return $intVal & $places;
  }


  // Get High Byte
  public static function getDecimal($hiByte, $loByte){
  	 $dec =  ( $hiByte * 256 ) + $loByte;
     return $dec;
  }
	//================time and date AK_TAG==========================
   public static function get_time_h($todayH){

		$todayH =date("H");
		$todayH -=4;		// patch to get correct hr
	   return ($todayH);	// patch since cant get current time for now 
  }
     public static function get_time_m($todaym){

		$todaym =date("i");
		//$todaym -=4;		// patch to get correct hr
	   return ($todaym);	// patch since cant get current time for now 
  }
     public static function get_time_s($todays){

		$todays =date("s");
		//$todays -=4;		// patch to get correct hr
	   return ($todays);	// patch since cant get current time for now 
  }
       public static function get_date_y($todayy){

		$todayy =date("y");
	   return ($todayy);	// patch since cant get current time for now 
  }
      public static function get_date_M($todayM){

		$todayM =date("m");
	   return ($todayM);	// patch since cant get current time for now 
  }
        public static function get_date_d($todayd){

		$todayd =date("d");
	   return ($todayd);	// patch since cant get current time for now 
  }
        public static function get_date_dw($todaydw){
		$todaydw = date("w");
	   return ($todaydw);	// patch since cant get current time for now 
  }
}
?>
