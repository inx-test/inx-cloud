<?php
session_start();
session_destroy();
clearstatcache();
session_unset();
unset($_SESSION);
$_SESSION = array();

session_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>InspeXtor cloud</title>
        <link href='https://fonts.googleapis.com/css?family=Nunito' rel='stylesheet'>
         <link href="css/site.css" rel="stylesheet" type="text/css"/>
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
        <link href="css/__minified.css" rel="stylesheet" type="text/css"/>
        <link href="css/__style.css" rel="stylesheet" type="text/css"/>
        
        <style type="text/css">
            .nn-alert-container{
                top: 0px !important;
                right: 0px !important;
                font-family: "Segoe UI", Arial, sans-serif;
            }
        </style>
        <script src="js/jquery.min.js" type="text/javascript"></script>
        <script src="js/raw/jquery.blockui.min.js" type="text/javascript"></script>
    </head>
    <body id="login-bg">
		
		<div class="row">
				<div class="col col-lg-2">
				</div>
				<div class="col col-lg-4" style="padding: 0px;">
				<div id="korfu" style="text-align: left;">
					<span><img src="img/inspextor.png" height="50px"/>&nbsp;<span class="h2" style="vertical-align: bottom;">cloud</span></span>
            <!--BEGIN LOGIN FORM-->
            <form class="login-form"  autocomplete="off">
                
                <div style="height: 100px"></div>
                <!--hr /-->
                <!--alert-->
                <div class="save-alert" id="alertBox">
                    <p class="alert-msg text-red hide"></p>
                </div>
                <label>Email address</label>
                <input tabindex=1 class="mar form-control" autofocus name="txtUsername" id="txtUsername" type="text" placeholder="info@gmail.com" autocomplete="off">
                <label>Password</label>
                <input tabindex=2 class="mar form-control" placeholder="password" type="password" name="txtPassword" id="txtPassword" type="text" autocomplete="off">
                <br />
                <button tabindex=3 type="button" name="btnLogin" id="btnLogin" class="btn btn btn-block text-uppercase btn-primary" onclick="return checkLoginUser();" >SIGN IN</button>
                <a href="javascript:void(0);" onclick="forgotPasssword();" class="pull-right">Forgot Password?</a>
				<br/><br/>
				<a href="javascript:void(0);" onclick="registerForm();" class="pull-bottom p-b-10">Register</a>
            </form>
            <!--END LOGIN FORM-->
			
			
            <!--FORGOT PASSWORD FORM-->
            <form class="forgot-form"  autocomplete="off">
                <center class="p-t-50"><h3>Forgot Password</h3></center>
                <p class="text-muted">
                    Enter your email address and we will send you a password.<br/>
					<font color = "red"> 
					** This is for cloud password reset only!** 
					</font>
                </p>
               
                <div class="save-alert" id="alertForgotBox">
                    <p class="alert-msg text-red hide"></p>
                </div>
                <label>Email address</label>
                <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email" id="txtEmail" style="border-radius:5px;"/>
				<br/>
                <button tabindex=3 type="button" name="btnForgotPass" id="btnForgotPass" onclick="return makeRandomPasssword();" class="btn btn btn-block text-uppercase btn-primary" >SUBMIT</button>
                <a href="javascript:void(0);" onclick="signIn();" class="pull-right">Sign In</a>
               
            </form>
            <!--END FORGOT PASSWORD FORM-->
			 
			 <form class="register-form"  autocomplete="off">
                
                <div class="save-alert" id="alertBox">
                    <p class="alert-msg text-red hide"></p>
                </div>
                <label>Inspextor Instance ID</label>
                <input tabindex=1 class="mar form-control" autofocus name="devId" id="devId" type="text" placeholder="xxxxxxx" autocomplete="off" onkeydown="clearAlert();">
                <label>Inspextor Instance Passcode</label>
                <input tabindex=1 class="mar form-control" autofocus name="devPass" id="devPass" type="password" placeholder="*****" autocomplete="off" onkeydown="clearAlert();">
                <label>First Name</label>
                <input tabindex=1 class="mar form-control" autofocus name="fname" id="fname" type="text" placeholder="Rob" autocomplete="off" onkeydown="clearAlert();">
                 <label>Last Name</label>
                <input tabindex=1 class="mar form-control" autofocus name="lname" id="lname" type="text" placeholder="Thomas" autocomplete="off" onkeydown="clearAlert();">
                <label>Email address</label>
                <input tabindex=1 class="mar form-control" autofocus name="user" id="user" type="text" placeholder="info@gmail.com" autocomplete="off" onkeydown="clearAlert();">
                <label>Password</label>
                <input tabindex=2 class="mar form-control" placeholder="*****" type="password" name="pass" id="pass" type="text" autocomplete="off" onkeydown="clearAlert();">
                <label>Confirm Password</label>
                <input tabindex=2 class="mar form-control" placeholder="*****" type="password" name="passC" id="passC" type="text" autocomplete="off" onkeydown="clearAlert();">
                <br />
                <button tabindex=3 type="button" name="btnLogin" class="btn btn btn-block text-uppercase btn-primary" onclick="return register();" >Register</button>
                <a href="javascript:void(0);" onclick="signIn();" class="pull-right">Sign In</a>
            </form>
        </div>
        </div>
			<div class="col col-lg-4" style="padding: 0px;">
				<div class="auth-page-sidebar"><div class="auth-user-testimonial"><div class=""></div><p class="font-size-24 font-weight-bold text-white mb-1"></p><p class="lead text-white"> Revolutionary Power over Ethernet System </p><ol class="lead text-white"><li>Smart Sensors</li><li>Real-time monitoring</li><li>Energy savings</li><li>Powerful, Patented, Secure 10-22-2020</li></ol></div></div>
			</div>
        </div>
    </body>
    <!--Javascript-->
    <script src="js/general-function.js" type="text/javascript"></script>
    <script type="text/javascript">

       $(document).ready(function(){

            $('.forgot-form').hide();
			$('.register-form').hide();

            $('#txtUsername').keypress(function(e){
              if(e.keyCode==13){
                $('#btnLogin').click();
                return false;
            }
            });

            $('#txtPassword').keypress(function(e){
              if(e.keyCode==13){
                $('#btnLogin').click();
                return false;
            }
            });
        });
		
		function register()
        {
			var insID = $("#devId").val();
            var insPass = $("#devPass").val();
            var fname = $("#fname").val();
            var lname = $("#lname").val();
            var username = $("#user").val();
            var password = $("#pass").val();
			var cpassword = $("#passC").val();

			if(insID == "")
            {
				showAlert("Please enter Inspextor Instance ID");
                $("#devId").focus();
                return false;
            }
            if(insPass == "")
            {
				showAlert("Please enter Inspextor Passcode");
                $("#devPass").focus();
                return false;
            }
            if(fname == "")
            {
				showAlert("Please enter First Name");
                $("#fname").focus();
                return false;
            }
            if(lname == "")
            {
				showAlert("Please enter Last Name");
                $("#lname").focus();
                return false;
            }
            if(username == "")
            {
                $("#alertBox p").html("Please enter Email Address");
                $("#alertBox p").removeClass("hide");
                $("#user").focus();
                return false;
            }
            if(password == "")
            {
				showAlert("Please enter Password");
                $("#pass").focus();
                return false;
            }
            else if ( password != cpassword )	{
				showAlert("Password and Confirm Password are not the same");
                $("#passC").focus();
                return false;
			}
			
            $.ajax({
                type:'POST',
                url: 'ajax.php',
                data:{ 'c':'User', 'a':'register', 'fname': fname, 'lname': lname,'username':username, 'password':password,'insID':insID, 'insPass':insPass },
                dataType: 'json',
                beforeSend : function(){__showLoadingAnimation();},
                success: function(response) {
                    if(response.status == 0){
						showAlert(response.message);
                    }
                    else if (response.status == 1) {
						showAlert("Succesfully registered","S");
						signIn();
                    }
                    return false;
                },
                complete:function(){__hideLoadingAnimation();},
                error: function(r) {
                    __hideLoadingAnimation();
                    console.log(r);
                }
            });
        }
        
        function checkLoginUser()
        {

            var username = $("#txtUsername").val();
            var password = $("#txtPassword").val();

            if(username == "")
            {
				showAlert("Please enter Email Address");
                $("#txtUsername").focus();
                return false;
            }
            if(password == "")
            {
				showAlert("Please enter Password");
                $("#txtPassword").focus();
                return false;
            }
            $.ajax({
                type:'POST',
                url: 'ajax.php',
                data:{ 'c':'User', 'a':'checkLoginExists', 'username':username, 'password':password },
                dataType: 'json',
                beforeSend : function(){__showLoadingAnimation();},
                success: function(response) {
                    if(response.status == 0){
                        $("#alertBox p").html("Incorrect Username or Password");
                        $("#alertBox p").removeClass("hide");
                    }
                    else if (response.status == 1) {
					   var time = (new Date).getTime();
					   window.location.href = "ws.php?t="+time;
                    }
                    return false;
                },
                complete:function(){__hideLoadingAnimation();},
                error: function(r) {
                    __hideLoadingAnimation();
                    console.log(r);
                }
            });
        }
		
		function registerForm()
		{
			$("#devId").val("");
            $("#devPass").val("");
            $("#user").val("");
            $("#pass").val("");
            $("#passC").val("");
               
            $("#alertBox p").html("");
			$('.login-form').hide();
            $('.forgot-form').hide();
            $('.register-form').show();
		}
		
        function forgotPasssword()
        {
            $("#txtUsername").val("");
            $("#txtPassword").val("");
            $("#alertBox p").html("");
            $('.login-form').hide();
            $('.forgot-form').show();
            $('.register-form').hide();
        }

        function makeRandomPasssword()
        {
            var email = $("#txtEmail").val();
            if (email == "" || email == null) {
               $("#alertForgotBox p").html("Please enter Email Address");
               $("#alertForgotBox p").removeClass("hide");
               $("#txtEmail").focus();
               return false;
            }
            if (!ValidateEmail(email.trim())) {
                $("#alertForgotBox p").html("Invalid Email Address");
                $("#alertForgotBox p").removeClass("hide");
                $("#txtEmail").focus();
                return false;
            }

            $.ajax({
                type:'POST',
                url:'ajax.php',
                data: { 'c':'User', 'a':'generatePassword','email':email},
                dataType: 'json',
                beforeSend : function(){__showLoadingAnimation();},
                success: function(response) {
                     if(response.status == 0){
                        $createAlert({title : "Failed",text : response.message});
                        $("#txtEmail").val("");
                        $("#alertForgotBox p").html("");
                        signIn();
                    }
                    else if (response.status == 1) {
                        $createAlert({title : "Success",text : response.message});
                        $("#txtEmail").val("");
                        $("#alertForgotBox p").html("");
                        signIn();
                    }
                },
                complete:function(){__hideLoadingAnimation();},
                error: function(r) {
                    __hideLoadingAnimation();
                    console.log(r);
                }
            });
        }

        function signIn()
        {
            $("#txtEmail").val("");
            $("#alertForgotBox p").html("");
            $('.login-form').show();
            $('.forgot-form').hide();
            $('.register-form').hide();
        }

		function clearAlert()
		{
			$("#alertBox p").html("");
            $("#alertBox p").addClass("hide");
		}
		
		function showAlert(message,type="E")
		{
			if ( type == "E" ) $("#alertBox p").addClass("text-red");
			else {
				$("#alertBox p").removeClass("text-red");
				$("#alertBox p").addClass("text-green");
			}
			$("#alertBox p").html(message);
            $("#alertBox p").removeClass("hide");
		}
		
        /*Notification Alert*/
        var $createAlert = function createAlert(opts){
            var status,title,text,alertTemplate;

            if(opts === undefined){
              opts = {};
            }

            status = opts.status || '';
            title = opts.title || '';
            text = opts.text || '';
            //color = opts.color || '';

            if($(".nn-alert-container").length == 0) {
                //console.log("container Created");
                $("body").append("<div class='nn-alert-container'></div>");
                var $nnAlert = $('.nn-alert-container');

                $nnAlert.on('click', '.nn-alert', function(){
                   hideAlert($(this));
                });
              }
            var $id = new Date().getTime();
            alertTemplate = '<div class="nn-alert cursor nn-alert-custom animated pgn-flip " id="__'+$id+'" ><strong>'+title+'</strong><p>'+text+'</p></div>';
            $(alertTemplate).prependTo('.nn-alert-container');
            setTimeout(function(){ hideAlert($("#__"+$id));},5000);//slideInDown

          }

        function hideAlert($alert){
            $alert.removeClass('pgn-flip');
            $alert.addClass('fadeOut');
            setInterval(function(){$alert.remove();},5000);
            //console.log($('.nn-alert-container  .nn-alert:last-child').length);
          }

        /************** Loader ***************/

         function __showLoadingAnimation(){
            $.blockUI({
                        message: '<div class="loading">'+
                                    '<div class="loading-bar"></div>'+
                                    '<div class="loading-bar"></div>'+
                                    '<div class="loading-bar"></div>'+
                                    '<div class="loading-bar"></div>'+
                                '</div>',
                        overlayCSS: {
                                opacity: .5,
                                backgroundColor: '#e4e7ea' ,
                                basez:	10000
                        },
                        css: {
                    border: 'none',
                    width:'250px',
                    height:'80px',
                    top:'40%',
                    padding:'30px 15px 15px 15px',
                    left:'40%',
                    backgroundColor: 'rgba(255, 255, 255, 0.33)',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: 1,
                    color: '#fff',
                }
                });
         }

         function __hideLoadingAnimation(){
             $.unblockUI();
         }

    </script>
</html>
