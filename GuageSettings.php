<?php $pers = $_REQUEST["unit"] == "%" ? "1" : "0"; ?>
<b><?php echo $_REQUEST["gaugeName"];?></b>
<br>
<div class="form-group form-group-default no-padding no-border m-b-10 m-t-15 col-sm-6">
	<label>Min</label>
	<input type='button' value='-' onclick="javascript:qtyminus_min_min();" class='qtyminus pull-left' data-field='txt-range-start' />
	<input class="pull-left" readonly data-incr="<?php echo $_REQUEST["g_minval"];?>" data-pers="<?php echo $pers; ?>" type="text" value="<?php echo $_REQUEST["min"];?>" id="txt-range-start" required="required" onchange="javascript:RangeChanged();" style="width:60px;">
	<input type='button' value='+' onclick="javascript:qtyplus_min_max();" class='qtyplus pull-left' data-field='txt-range-start' />
	<label class="error hide" id="lbl-error-range-start">Please enter start value</label>
</div>
<!--div class="m-t-25 col-sm-2 text-center">
 to
</div-->
<div class="form-group form-group-default no-padding no-border m-b-10 m-t-15 col-sm-6">
	<label>Max</label>
	<input type='button' value='-' onclick="javascript:qtyminus_max_min();" class='qtyminus pull-left' data-field='txt-range-end' />
	<input class="pull-left" type="text" data-pers="<?php echo $pers; ?>" readonly data-incr="<?php echo $_REQUEST["g_minval"]; ?>" value="<?php echo $_REQUEST["max"]; ?>" id="txt-range-end" required="required" onchange="javascript:RangeChanged();" style="width:60px;">
	<input type='button' value='+' onclick="javascript:qtyplus_max_max();" class='qtyplus pull-left' data-field='txt-range-end' />
	<label class="error hide" id="lbl-error-range-end">Please enter end value</label>
</div>
<div style="clear:both;"></div>

<div class="form-group form-group-default no-padding no-border m-b-10">
	<label>Tick Interval</label>
	<?php echo $_REQUEST["g_minval"]; ?>
</div>

<?php
 	$colors = $_REQUEST["color"];
 	if(isset($colors) && $colors != ""){
            $colors = urldecode($colors);
            //echo $colors;
 		//out.println(colors);
                //echo $colors.">>>>>";
 		$colorArr = json_decode($colors,TRUE);
                //print_r($colorArr);
                ?>
                <div class="form-group form-group-default no-padding no-border m-b-10">
			<label>Color Ranges</label>
		</div>
                <?php
 		//print_r($colorArr);
 		for ($i = 0; $i < count($colorArr); $i++)
 		{
                    //echo $i;
 			$col = $colorArr[$i];
 			$colorCode =$col["color"];
                        
 			$colorClass = "gauge-red";
 			$colorCurr = "red";
 			
 			if($colorCode == "#FFA07A") { $colorClass = "gauge-red"; $colorCurr = "red"; }
 			else if($colorCode == "#F0E68C") { $colorClass = "gauge-yellow"; $colorCurr = "yellow"; }
 			else if($colorCode == "#98F596") { $colorClass = "gauge-green"; $colorCurr = "green"; }
 			
 			$fromVal = $col["from"];
 			$toVal = $col["to"];
 			
 			?>
                            <div class="irs-wrapper <?php echo $colorClass; ?> m-t-10 div-color-ranger" id="div-color-ranger-<?php echo $i; ?>">
                                    <input type="hidden" id="hdn-color-<?php echo $i; ?>" name="hdn-color-<?php echo $i; ?>" value="0" data-from="<?php echo $fromVal; ?>" data-to="<?php echo $toVal; ?>" data-color="<?php echo $colorCurr; ?>" data-old-color="<?php echo $colorCurr; ?>" />
                                    <input type="text" id="inp-color-<?php echo $i; ?>" name="inp-color-<?php echo $i; ?>" class="ion_slider inp-color-range-values" value="0" data-from="<?php echo $fromVal; ?>" data-to="<?php echo $toVal; ?>" data-color="<?php echo $colorCurr; ?>" data-old-color="<?php echo $colorCurr; ?>" />
                            </div>
 			<?php
 		}
 	}
        else{
             ?>
            <!--div class="irs-wrapper gauge-red m-t-10 div-color-ranger" id="div-color-ranger-0">
                    <input type="text" id="inp-color-0" name="inp-color-0" class="ion_slider inp-color-range-values" value="0" data-from="0>" data-to="0" data-color="red" data-old-color="red" />
            </div>
            <div class="irs-wrapper gauge-yellow m-t-10 div-color-ranger" id="div-color-ranger-1">
                    <input type="text" id="inp-color-1" name="inp-color-1" class="ion_slider inp-color-range-values" value="0" data-from="0>" data-to="0" data-color="yellow" data-old-color="yellow" />
            </div>
            <div class="irs-wrapper gauge-green m-t-10 div-color-ranger" id="div-color-ranger-2">
                    <input type="text" id="inp-color-2" name="inp-color-2" class="ion_slider inp-color-range-values" value="0" data-from="0>" data-to="0" data-color="green" data-old-color="green" />
            </div-->
       <?php }
       
            $yesVisible = "";
            $noVisible = "";
            if(intval($_REQUEST["visible"]) == 1){
                $yesVisible = "checked=\"checked\"";
            }
            else{
                $noVisible = "checked=\"checked\"";
            }
       ?>


<div class="clearfix full-width text-right p-t-10 p-l-10 p-r-10 p-b-0">
	<a href="javascript:void(0);" class="btn btn-tag btn-tag-rounded btn-success btn-sm btnbtn-tag btn-success btn-tag-rounded" onclick="return ApplyGaugeSettings();">Apply</a>
	<a href="javascript:void(0);" class="btn btn-tag btn-tag-rounded btn-danger btn-sm btn m-l-10" onclick="CancelGaugeSettings();">Cancel</a>
</div>
<?php
	if ($_REQUEST["auto-k"] == 1 ){
        ?>
            <div class="col-sm-12 p-t-10 p-b-10 m-t-10 b-radius-5 bold"  style="background:#CFCFCF;">
                    <span><span>Auto convert to K<?php echo $_REQUEST["unit"]; ?> </span> <span class="pull-right"> <i class="fa fa-check"></i></span></span>		
            </div>
        <?php
 	}
 	
 	if ($_REQUEST["is-avg"] == 1 ){
        ?>
            <div class="col-sm-12 p-t-10col-sm-12 p-t-10 p-b-10 m-t-10 b-radius-5 bold" style="background:#CFCFCF;">
                    <span><span>24 Hour averaged </span> <span class="pull-right"> <i class="fa fa-check"></i></span></span>
            </div>
        <?php
 	}
 ?>


<script type="text/javascript">
	$(document).ready(function(){
            $(".inp-color-range-values").each(function(){
                    //alert(this.id + " => " + $("#txt-range-start").val() + " - " + $("#txt-range-end").val());
                    CreateSlider(this.id);
            });

            initColorScheme();
            SetColorChanger();
            //console.log(1);
	});
        
        function qtyminus_min_min(){
		
		var currentVal = parseInt($("#txt-range-start").val());
		var incr = parseInt($("#txt-range-start").data('incr'));
		
		if( currentVal > 0 ){
			var newVal = currentVal - incr;
			newVal = (newVal < 0) ? 0 : newVal;
			$("#txt-range-start").val(newVal);
		}
		RangeChanged();
	};
	
	function qtyplus_min_max(){
		
		var currentVal = parseInt($("#txt-range-start").val());
		var incr = parseInt($("#txt-range-start").data('incr'));
		var newVal = currentVal + incr;
		newVal = (newVal < 0) ? 0 : newVal;
		
		if($("#txt-range-start").data("pers") == 1 && newVal > 100) return false;
		
		$("#txt-range-start").val(newVal);
		
		if($("#txt-range-end").val() <= newVal) $("#txt-range-end").val(newVal + incr);
                RangeChanged();
	}
	
	function qtyminus_max_min(){
		
		var currentVal = parseInt($("#txt-range-end").val());
		var incr = parseInt($("#txt-range-end").data('incr'));
		
		var currentMinVal = parseInt($("#txt-range-start").val()) + incr;
		
		if( currentVal > currentMinVal ){
			var newVal = currentVal - incr;
			newVal = (newVal < 0) ? 0 : newVal;
			$("#txt-range-end").val(newVal);
		}
                RangeChanged();
	};
	
	function qtyplus_max_max(){
		
		var currentVal = parseInt($("#txt-range-end").val());
		var incr = parseInt($("#txt-range-end").data('incr'));
		var newVal = currentVal + incr;
		
		if($("#txt-range-end").data("pers") == 1 && newVal > 100) return false;
		
		$("#txt-range-end").val(newVal);
                RangeChanged();
	};
        
	function SetColorChanger(){
            $(".irs-from").contextColorMenu({
                menuSelector: "#contextColorMenu",
                autoHide: true
            });

            $(".irs-to").contextColorMenu({
                menuSelector: "#contextColorMenu",
                autoHide: true
            });

            $(".irs-single").contextColorMenu({
                menuSelector: "#contextColorMenu",
                autoHide: true
            });
        }
        
	//CreateSliders()
	function CreateSlider(elemID){
            __initSlider("#" + elemID,$("#txt-range-start").val(),$("#txt-range-end").val());
	}
	
	//Destroy Sliders()
	function DestroySlider(elemID){
            var $range = $("#" + elemID)
            var slider = $range.data("ionRangeSlider");

            slider && slider.destroy();
	}
	
	
	function  initColorScheme()
	{
	    
	    (function ($, window) {
	        var __prevMenuSelector = '';
	        var __activeContextId = 0;
	        var __targetObject = '';
	        
	    	$.fn.contextColorMenu = function (settings) {
	        	if( __prevMenuSelector != '' ) $(__prevMenuSelector).hide();
	        	
	        	__prevMenuSelector = settings.menuSelector;
	        	
	        	return this.each(function () {

		            // Open context menu
		            $(this).unbind('contextmenu');
		            $(this).on("contextmenu", function (e) {
		                // return native menu if pressing control
	                
	                	if (e.ctrlKey) return;
	                
		                $('.context-color-pick-backdrop').unbind('click');
		                $('.context-color-pick-backdrop').show();
		                $('.context-color-pick-backdrop').on('click',function(){
		                    $(__prevMenuSelector).hide();
		                    $('.context-color-pick-backdrop').hide();
		                });
	                
		                var contextWrapper =  $(e.target).closest( ".div-color-ranger" ).attr("id");
		                __activeContextId = contextWrapper.replace("div-color-ranger-",'');
		                __targetObject = contextWrapper;
		                
		                var oldClass = $("#inp-color-" + __activeContextId).data("color"); 
	                
		                //open menu
		                var $menu = $(settings.menuSelector)
		                    .data("invokedOn", $(e.target))
		                    .show()
		                    .css({
		                        position: "absolute",
		                        left: getMenuPosition(e.clientX, 'width', 'scrollLeft'),
		                        top: getMenuPosition(e.clientY, 'height', 'scrollTop')
		                    })
		                    .off('click')
		                    .on('click', 'a', function (e) {
		                        $menu.hide();
		                
		                        var $invokedOn = $menu.data("invokedOn");
		                        var $selectedMenu = $(e.target);
		                        
		                        settings.menuSelected.call(this, $invokedOn, $selectedMenu);
		                    });
                                    //make sure menu closes on any click
                                    $('select[id="prj-color-picker"]').simplecolorpicker({
                                        picker: false, 
                                        theme: 'fontawesome'
                                    }).unbind('change').on('change', function() {
	                
                                        var backgroundColor = $('select[id="prj-color-picker"]').val(); 
                                        var colorClass = '';

                                        if(backgroundColor == "#FFA07A") colorClass = "red";
                                                    else if(backgroundColor == "#F0E68C") colorClass = "yellow";
                                                    else if(backgroundColor == "#98F596") colorClass = "green";

                                        var oldClass = $("#inp-color-" + __activeContextId).data("color");  
                                        $("#" + contextWrapper).attr("class","irs-wrapper m-t-10 div-color-ranger gauge-" + colorClass);
                                        $("#inp-color-" + __activeContextId).data("color", colorClass); 
		                    
                                        $(__prevMenuSelector).hide();
                                        $('.context-color-pick-backdrop').hide();
                                    });
	        
                                    if(typeof(oldClass)!='undefined' && oldClass != '')
                                    {
                                            var selectedColor = "";

                                            if(oldClass == "red") selectedColor = "#FFA07A";
                                            else if(oldClass == "yellow") selectedColor = "#F0E68C";
                                            else if(oldClass == "green") selectedColor = "#98F596";

                                        $('select[id="prj-color-picker"]').simplecolorpicker('selectColor', selectedColor);
                                    }   
                                    else{ 
			                $("#contextColorMenu").find("span.color").removeAttr('data-selected');
			            }
				        
	                	return false;
            		});
	          
	        	});
	        
		        function getMenuPosition(mouse, direction, scrollDir) {
		            var win = $(window)[direction](),
		                scroll = $(window)[scrollDir](),
		                menu = $(settings.menuSelector)[direction](),
		                position = mouse + scroll;
		                        
		            // opening menu would pass the side of the page
		            if (mouse + menu > win && menu < mouse) 
		                position -= menu;
		            
		            return position;
		        }    

	   	 	};
	      
		})(jQuery, window);
	}
	
	
</script>
