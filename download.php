<?php
session_start();
$ad=false;
$loggedInUser = $_SESSION['LOGGED_USER_ID'].$_SESSION["LOGGED_IN_USERNAME"];
if(!empty($_GET['file'])){
    $fileName = basename($_GET['file']);
    $filePath = '/home/inspexto/public_html/poe/files/'.$fileName;
	$userFileName=$loggedInUser;
	
	$userfilePath = '/home/inspexto/public_html/poe/files/UserFileDownload/'.$loggedInUser;
    if(!empty($fileName) && file_exists($filePath)){
		// read to check if user ID text has been craeted 
		
		if(!file_exists($userfilePath))
		{
			// Define headers
			ob_start();
			header("Cache-Control: public");
			header("Content-Description: File Transfer");
			header("Content-Disposition: attachment; filename=$fileName");
			header("Content-Type: application/octet-stream");
			//header("Content-Type: application/zip");
			header("Content-Transfer-Encoding: binary");
			
			/*header("Content-type: application/force-download"); 
			header("Content-Disposition: attachment; filename=$fileName");
			header("Content-length: " . filesize($fileName));
			header("Pragma: no-cache"); 
			header("Expires: 0"); */
			while (ob_get_level()) {
					ob_end_clean();
				  }
			// Read the file
			readfile($filePath);
			// write file with user ID 
			$fname = "/home/inspexto/public_html/poe/files/UserFileDownload/".$loggedInUser;
			$myfile = fopen($fname, "w") or die("Unable to open file!");
			$txt = "x\n";
			fwrite($myfile, $txt);
			fclose($myfile);

			exit(0);
		}
		else
		{
			$ad=true;
		}
    }else echo "The file does not exist or empty Check with AK for further assistance ! \r\n";         
}
?>
<?php //include_once("tpl/header.tpl.php");
include_once("tpl/dashboard-top.tpl.php"); 

 ?>
 <style>
.slidecontainer {
    width: 100%;
}

.slider {
    -webkit-appearance: none;
    width: 100%;
    height: 15px;
    border-radius: 5px;
    background: #d3d3d3;
    outline: none;
    opacity: 0.7;
    -webkit-transition: .2s;
    transition: opacity .2s;
}

.slider:hover {
    opacity: 1;
}

.slider::-webkit-slider-thumb {
    -webkit-appearance: none;
    appearance: none;
    width: 25px;
    height: 25px;
    border-radius: 50%;
    background: #4CAF50;
    cursor: pointer;
}

.slider::-moz-range-thumb {
    width: 25px;
    height: 25px;
    border-radius: 50%;
    background: #4CAF50;
    cursor: pointer;
}
 </style>
<!-- START PAGE CONTENT -->
<div class="content clearfix p-b-0">
		
		

        <div class="panel panel-transparent">    
            <div class="panel-heading ">
                <div class="panel-title bold fs-16">
                    Download control
                </div>
            </div>
            <div class="panel-body">
			<div class="col-md-6 text-center">
			</div>
			<br>
			<?php if ( $ad )	{?> 
		<div class="alert alert-warning">
			<strong>Warning!</strong> This Document Was already downloaded ONCE Please check support team for further asssistance
		 </div>
		 <?php } ?>
			<br>
			<br>
			 <div class="row">
				<div class="col-md-12 text-center">
					<div class="alert alert-info">
					<font size="3">
					<strong>Info!</strong> CONFIDENTIALITY NOTICE: This document (and any attachment) is confidential and intended for the sole use of the individual or entity to which it is addressed. If you are not the intended recipient, you must not review, retransmit, convert to hard-copy, copy, use or disseminate this documeent or any of its attachments. If you viewed this document in error, please notify the inspeXtor support team immediately and delete it.You might only download this document ONCE. if you need special assistance contact inspeXtor support team @ inspextor.help@gmail.com
					</font>
					</div>
				</div>
			</div>
			<br>
			<br>
			<div class="row">
				<div class="col-md-12 text-center">
					<button class="btn btn-complete btn-cons bold btn-cons" 
					onclick="window.location.href='download.php?file=WARRANTY.zip'">Click me to download documents</button>
				</div>
			</div>
            </div>
        </div>
    <input type="hidden" id="hdnClusterID" name="hdnClusterID" value="">
 
</div>
<!-- END PAGE CONTENT -->
<?php include_once("tpl/footer.tpl.php"); ?>

<script src="js/jquery.path.js" type="text/javascript"></script>
<script type="text/javascript">
    $mergePolicyStatus =1;
    $(document).ready(function () {
        $ClusterTree.init(0);
    });
	

var slider = document.getElementById("myRange");
var output = document.getElementById("demo");
output.innerHTML = slider.value;

slider.oninput = function() {
  output.innerHTML = this.value;
}

var sliderAT = document.getElementById("myRangeAT");
var outputAT = document.getElementById("AT");
outputAT.innerHTML = sliderAT.value;

sliderAT.oninput = function() {
  outputAT.innerHTML = this.value;
}


//-------------
	var $clientReport = {
		__setDevice :function (){

			var selVal = $("#selDeviceType").select2("val");

			var $options = '';
			$('#selDev').html('');   
			$('#selDev').select2('data', null);
			if( selVal == 1 ){
				$('#selDev').append('<option value="-1">All Clusters</option>');
				$('#selDev').select2('val', "-1");
				$("#selDev").prop("disabled", true);
				$('#selFix').append('<option value="-1">All Fixtures</option>');
				$('#selFix').select2('val', "-1");
				$("#selFix").prop("disabled", true);
			}
			else{    
				$options ="";
				$('#selDev').select2('data', null);
				$("#selDev").prop("disabled", false);
				if( selVal == 2) $ClusterTree.getAllTagGroup("#selDev"); 
				else if( selVal == 3){
					$ClusterTree.getAllCluster("#selDev");  
				}					
				$('#selDev').select2('val', "1");
			}
			

		},
		__selectFix :function (){
			$options ="";
				$('#selFix').select2('data', null);
				$("#selFix").prop("disabled", false);
			$ClusterTree.getAllTagGroup("#selFix");		
			$('#selFix').select2('val', "1");

		},
		__send: function ($control,$dim) {
			var $type = $("#selDeviceType").val();
			var $clusterID = $("#selDev").val();
			var $tagID = $("#selFix").select2("val");
			
			//var $dim = $("#dimV").val();

                var $data = {
					'a': 'sendControl',
					'c': 'LocationSettings',
					t:$type,
					cluster: $clusterID,
					p1: $dim,
					tag: $tagID,
					command:$control
                };

                $.ajax({
                    type: 'POST',
                    url: 'ajax.php',
                    data: $data,
                    dataType: 'json',
                    beforeSend: function () {
                        __showLoadingAnimation();
                    },
                    success: function (res) {
                        var $status = res.status;
                        if ($status == 1) {

                            $createAlert({status: "success", title: "Successfully", text: "Send Command "});
							__hideLoadingAnimation();
                        }
                        else {
                            $createAlert({status: "fail", title: "Failed", text: " To Send Command"});
							__hideLoadingAnimation();
                        }
                    },
                    complete: function () {
                        __hideLoadingAnimation();
                    },
                    error: function (r) {
                        __hideLoadingAnimation();
                        $createAlert({status: "fail", title: "Failed", text: r});
                    }
                });

        }
	}
</script>
