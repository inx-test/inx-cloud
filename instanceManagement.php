<?php
include "config.php";
include_once("tpl/dashboard-top.tpl.php");

//$sql = "select * from pmi_device where deleted=0";
//$allDevices = $db->get_result_array($sql);
?>
   <div class="content">
        <!-- START PANEL -->
       <div id="deviceContainer" class="panel panel-default  panel-savings m-b-0">
            <div class="panel-heading clearfix">
                <div class="panel-title bold fs-16 upper">
                    Instance Management
                </div>

            </div>
            <div class="panel-body p-t-0">
                <div id="device-manage" class="panel panel-transparent m-b-0">
                    <div class="panel-body">
                        <div id="deviceList" class="">

                        </div>


                        <!-- MODAL CONFIRMATION ALERT -->
                        <div class="modal fade slide-up disable-scroll" id="modalCnfirmation" tabindex="-1" role="dialog" aria-hidden="false">
                          <div class="modal-dialog" style="width: 300px;">
                            <div class="modal-content-wrapper">
                              <div class="modal-content mdal-custom">
                                <div class="modal-body p-b-5">
                                    <p class="bold fs-20 p-t-20" id="cnCntent">
                                        Do you want to remove this instance?
                                    </p>
                                </div>
                                <div class=" modal-footer text-right">
                                    <button type="button" class="btn btn-sm btn-danger  pull-right" data-dismiss="modal">Cancel</button>
                                    <button type="button" class="btn btn-sm btn-success  pull-right m-r-10" onclick="removeConfirm();">OK</button>
                                </div>
                              </div>
                            </div>
                            <!-- /.modal-content -->
                          </div>
                        </div>
                        <!-- END CONFIRMATION ALERT -->

                        <input type="hidden" id="delDeviceId" name="delDeviceId" value="" />
                    </div>
                </div>
                <div class="col-centered">
                 <?php if($loggedUserRoleId == SUPER_ADMIN_ROLE) { ?>
                <button class="btn btn-complete btn-cons bold pull-right" id="btnAddDevice" onclick="addNewDevice();">Add New Instance</button>
                <!--button class="btn btn-tag btn-complete btn-tag-rounded m-l-20 pull-right" id="btnAddDateTime" onclick="setDateTime();"><i class="fa fa-clock-o"></i> Set date & time</button-->
                <?php } ?>
                
                <button class="btn btn-complete btn-cons bold pull-right" id="btnClaimDevice" onclick="displayClaimInstanceDialog();">Claim Another Instance</button>
                <!--button class="btn btn-tag btn-complete btn-tag-rounded m-l-20 pull-right" id="btnAddDateTime" onclick="setDateTime();"><i class="fa fa-clock-o"></i> Set date & time</button-->
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4">

            </div>
            <div class="col-sm-4">
                <div class="panel panel-default  panel-savings hide" id="addDevices">
                    <div class="panel-heading p-t-15 bold fs-16 upper b-b b-grey">
                        Enter instance details
                        <button onclick="closeAddDevice();" class="close" type="button">×</button>
                    </div>
                    <div class="panel-body p-b-15">
                        <div class="device-list">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group required " id="divName">
                                        <label>Instance Name</label>
                                        <input type="text" class="form-control" id="txtName" required="" placeholder="Enter instance name" onkeyup="removeError('divName');">
                                        <span class="text-danger sp_error hide"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group required " id="divID">
                                        <label>Instance ID</label>
                                        <input type="text" class="form-control" id="txtInstanceID" required="" placeholder="Enter instance id" onkeyup="removeError('divID');">
                                        <span class="text-danger sp_error hide"></span>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group required " id="divpassword">
                                        <label>Instance Password</label>
                                        <input type="password" class="form-control" id="txtpassword" required="" placeholder="Enter password" onkeyup="removeError('divpassword');">
                                        <span class="text-danger sp_error hide"></span>
                                    </div>
                                </div>
                            </div>
			
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group required " id="divDuration">
                                        <label>Data Duration (Day)</label>
											<input type="text" class="form-control" id="txtDuration" required="" placeholder="Enter duration days" onkeyup="removeError('divDuration');">
                                        <span class="text-danger sp_error hide"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group required " id="divSpace">
                                        <label>Data Space Limit (Mb)</label>
                                        <input type="text" class="form-control" id="txtSpace" required="" placeholder="Enter space limit" onkeyup="removeError('divSpace');">
                                        <span class="text-danger sp_error hide"></span>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group required " id="divEmail">
                                        <label>Notified Email Address</label>
                                        <input type="text" class="form-control" id="txtEmail" required="" placeholder="Enter email address" onkeyup="removeError('divSEmail');">
                                        <span class="text-danger sp_error hide"></span>
                                    </div>
                                </div>
                            </div>
							<div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group required " id="divClient">
                                        <label>Link with client</label>
                                        <select id="client-select" placeholder="Select Client" data-init-plugin="select2" name='client' style="width:350px;"  required>

										</select>
										<span class="text-danger sp_error hide"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="button" class="btn btn-danger pull-right m-r-15 m-t-10" onclick="closeAddDevice();">Close</button>
                        <button id="btnSaveDevice" type="button" class="btn btn-success pull-right m-r-10 m-t-10" onclick="return saveDevice();">Save</button>
                        <input type="hidden" id="deviceRowCount" name="deviceRowCount" value="0" />
                    </div>
                </div>
				
				 <div class="panel panel-default  panel-savings hide" id="claimDeviceDialog">
                    <div class="panel-heading p-t-15 bold fs-16 upper b-b b-grey">
                        Claim Instance
                        <button onclick="closeAddDevice();" class="close" type="button">×</button>
                    </div>
                    <div class="panel-body p-b-15">
                        <div class="device-list">
                           
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group required " id="divInsID">
                                        <label>Instance ID</label>
                                        <input type="text" class="form-control" id="insID" required="" placeholder="Enter instance id" onkeyup="removeError('divID');">
                                        <span class="text-danger sp_error hide"></span>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group required " id="divInsPass">
                                        <label>Instance Password</label>
                                        <input type="password" class="form-control" id="insPass" required="" placeholder="Enter password" onkeyup="removeError('divpassword');">
                                        <span class="text-danger sp_error hide"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="button" class="btn btn-danger pull-right m-r-15 m-t-10" onclick="closeClaimInstance();">Close</button>
                        <button id="btnSaveDevice" type="button" class="btn btn-success pull-right m-r-10 m-t-10" onclick="return claimInstance();">Claim</button>
                    </div>
                </div>
            </div>
            <div class="col-sm-2">

            </div>
        </div>

    <!-- END PANEL -->
   </div>
   
<!-- END PAGE CONTENT -->
<?php include_once("tpl/footer.tpl.php");?>
<script type="text/javascript">
    var loggedUserRole = '<?php echo $loggedUserRoleId; ?>';
    const randomIdGenerator = uniqueId();
    
    $(document).ready(function(){

        $(".content").css("min-height",($(window).height()));
        getActiveDeviceList(0);
    });
	
	function displayClaimInstanceDialog()
	{
		$("#deviceContainer").addClass("hide");
		//$("#device-manage .panel-body").removeClass("panel-savings");
		$("#claimDeviceDialog").removeClass("hide");
		$("#autoPingDevice").prop('checked',false);
	}
	
    function addNewDevice(){
		
        var n= $("#deviceRowCount").val();
		
		instanceId=randomIdGenerator();
		instancePass=randomIdGenerator();
		
        if(n == 0)
        {
            $("#deviceContainer").addClass("hide");
            //$("#device-manage .panel-body").removeClass("panel-savings");
            $("#addDevices").removeClass("hide");
            $("#autoPingDevice").prop('checked',false);
            n = 1;
			
			$("#txtInstanceID").val(instanceId);
			$("#txtpassword").val(instancePass);
			
            var addDevice='';

            for(var i=n; i<=5; i++){
                addDevice += "<div class='p-t-5 p-b-5 device-row' id='deviceRow_"+i+"'>";
                addDevice += "<div class='col-sm-3 p-l-10'><div class='form-group m-b-0'>";
                addDevice += "<input type='text' class='form-control' name='txtDeviceName[]' id='txtDeviceName_"+i+"' value=''/>";
                addDevice += "</div></div><div class='col-sm-2 p-l-0'><div class='form-group m-b-0 serial'>";
                addDevice += "<input type='text' class='form-control' name='txtSerialNo[]' id='txtSerialNo_"+i+"' value='' onblur='return checkSerial("+i+");' />";
                addDevice += "</div></div><div class='col-sm-3 p-l-0'>";
                addDevice += "<div class='form-group m-b-0 ip'>";
                addDevice += "<input type='text' class='form-control' name='txtIPaddress[]' id='txtIPaddress_"+i+"' onblur='return checkIPaddress("+i+")' value='' />";
                addDevice += "</div></div><div class='col-sm-4 p-l-0'><div class='form-group m-b-0'><input type='text' class='form-control' name='txtLocation[]' id='txtLocation_"+i+"' value='' onkeyup='removeAlertClass()' /></div></div></div>";

            }
            $(".list-title").append(addDevice);
            $("#deviceRowCount").val(i);
			var clientHTML = '';

			$("#client-select").html('');
			$.ajax ({
				type:'POST',
				url:'ajax.php',
				data: { 'c':'Client',
					'a':'fetchAllClient'
					},
				dataType: 'json',
				beforeSend: function() {__showLoadingAnimation();},
				success: function(response){
					var allClients = response.clientInfo;
					for (var i = 0; i < allClients.length; i++) {
						clientHTML += '<option value="' + allClients[i]['id'] + '">' + allClients[i]['firstname']+'' + allClients[i]['lastname'] + '</option>';
					}
					$("#client-select").append(clientHTML);
					$("#client-select").select2().select2('val', []);
				},
				complete:function (){__hideLoadingAnimation();},
				error:  function(r){
					__hideLoadingAnimation();
					console.log(r);
				}
			});
        }
    }

    function addNewRows(){
        var n= $("#deviceRowCount").val();

        var count=parseInt(n)+4;
        //alert(n+"----"+count);

        var addDevice='';
        for(var i=n; i<=count; i++){
            addDevice += "<div class='p-t-10 p-b-10 device-row' id='deviceRow_"+i+"'><div class='col-sm-3 p-l-10'><div class='form-group m-b-0'><input type='text' class='form-control' name='txtDeviceName[]' id='txtDeviceName_"+i+"' value=''/></div></div><div class='col-sm-2 p-l-0'><div class='form-group m-b-0 serial'><input type='text' class='form-control' name='txtSerialNo[]' id='txtSerialNo_"+i+"' value='' onblur='return checkSerial("+i+");' /></div></div><div class='col-sm-3 p-l-0'><div class='form-group m-b-0 ip'><input type='text' class='form-control' name='txtIPaddress[]' id='txtIPaddress_"+i+"' onblur='return checkIPaddress("+i+")' value='' /></div></div><div class='col-sm-4 p-l-0'><div class='form-group m-b-0'><input type='text' class='form-control' name='txtLocation[]' id='txtLocation_"+i+"' value='' onkeyup='removeAlertClass()' /></div></div></div>";

        }
        $(".list-title").append(addDevice);
        $("#deviceRowCount").val(i);
    }
	
	function claimInstance()
	{
		var $insID = $("#insID").val();
        var $insPass = $("#insPass").val();
        
        if($insID == ""){
            $("#divInsID").addClass("has-error");
            $("#divInsID .sp_error").html("Please enter Instance ID");
            $("#divInsID .sp_error").removeClass("hide");
            $("#insID").focus();
            return false;
        }
        if($insPass == ""){
            $("#divInsPass").addClass("has-error");
            $("#divInsPass .sp_error").html("Please enter Instance Password");
            $("#divInsPass .sp_error").removeClass("hide");
            $("#insPass").focus();
            return false;
        }
        
        $.ajax ({
            type:'POST',
            url:'ajax.php',
            data: { 'c':'Device',
                'a':'claimInstance',
                'insID':$insID,
                'insPass':$insPass
                },
            dataType: 'json',
            beforeSend: function() {__showLoadingAnimation();},
            success: function(response){
                var duplicate = response.duplicateEntry;
                //console.log(response);
                if(response.status == 1){
                    $createAlert({status :"success",title : "Success",text : response.message});
                    $("#claimDeviceDialog").addClass("hide");
                    getActiveDeviceList();
                }
                else{ $createAlert({status :"fail",title : "Failed",text : response.message}); }
            },
            complete:function (){__hideLoadingAnimation();},
            error:  function(r){
                __hideLoadingAnimation();
                console.log(r);
            }
        });
	}
	
    function saveDevice()
    {
        var $devName = $("#txtName").val();
        var $devID = $("#txtInstanceID").val();
        var $devPassword = $("#txtpassword").val();
        var $duration = $("#txtDuration").val();
        var $space = $("#txtSpace").val();
        var $email = $("#txtEmail").val();
		var $client = $("#client-select").select2().val();

        if($devName == "" || $devName == null){
            $("#divName").addClass("has-error");
            $("#divName .sp_error").html("Please enter instance name");
            $("#divName .sp_error").removeClass("hide");
            $("#txtName").focus();
            return false;
        }
        if($devID == "" || $devID == null){
            $("#divID").addClass("has-error");
            $("#divID .sp_error").html("Please enter instance ID");
            $("#divID .sp_error").removeClass("hide");
            $("#txtInstanceID").focus();
            return false;
        }
        if($devPassword == "" || $devPassword == null){
            $("#divpassword").addClass("has-error");
            $("#divpassword .sp_error").html("Please enter password");
            $("#divpassword .sp_error").removeClass("hide");
            $("#txtpassword").focus();
            return false;
        }
        if($duration == "" || $duration == null){
            $("#divDuration").addClass("has-error");
            $("#divDuration .sp_error").html("Please enter duration");
            $("#divDuration .sp_error").removeClass("hide");
            $("#txtDuration").focus();
            return false;
        }
        if(!isNumber($duration)){
            $("#divDuration").addClass("has-error");
            $("#divDuration .sp_error").html("Only numbers are allowed");
            $("#divDuration .sp_error").removeClass("hide");
            $("#txtDuration").focus();
            return false;
        }
        if($space == "" || $space == null){
            $("#divSpace").addClass("has-error");
            $("#divSpace .sp_error").html("Please enter data space limit");
            $("#divSpace .sp_error").removeClass("hide");
            $("#txtSpace").focus();
            return false;
        }
        if(!isNumber($space)){
            $("#divSpace").addClass("has-error");
            $("#divSpace .sp_error").html("Only numbers are allowed");
            $("#divSpace .sp_error").removeClass("hide");
            $("#txtSpace").focus();
            return false;
        }
        if($email == "" || $email == null){
            $("#divEmail").addClass("has-error");
            $("#divEmail .sp_error").html("Please enter email address");
            $("#divEmail .sp_error").removeClass("hide");
            $("#txtEmail").focus();
            return false;
        }
        if(!ValidateEmail($email))
        {
            $("#divEmail").addClass("has-error");
            $("#divEmail span").removeClass("hide");
            $("#divEmail span").html("Invalid Email Address");
            $('#txtEmail').focus();
            return false;
        }
		if($client == "" || $client == null){
            $("#divClient").addClass("has-error");
            $("#divClient .sp_error").html("Please Select Client");
            $("#divClient .sp_error").removeClass("hide");
            //$("#txtName").focus();
            return false;
        }
        //return false;
        $.ajax ({
            type:'POST',
            url:'ajax.php',
            data: { 'c':'Device',
                'a':'saveDeviceDetails',
                'devicename':$devName,
                'devID':$devID,
                'password':$devPassword,
                'duration':$duration,
                'spaceLimt':$space,
                'emailAddress':$email,
				'clientID': $client},
            dataType: 'json',
            beforeSend: function() {__showLoadingAnimation();},
            success: function(response){
                var duplicate = response.duplicateEntry;
                //console.log(response);
                if(response.status == 1){
                    $createAlert({status :"success",title : "Success",text : response.message});
                    getActiveDeviceList();
                }
                else{ $createAlert({status :"fail",title : "Failed",text : response.message}); }
            },
            complete:function (){__hideLoadingAnimation();},
            error:  function(r){
                __hideLoadingAnimation();
                console.log(r);
            }
        });
    }

    function closeAddDevice(){
        $("#addDevices").addClass("hide");
        //$("#device-manage .panel-body").addClass("panel-savings");
        $("#deviceContainer").removeClass("hide");
        $(".list-title").find("input:text").val("");
        $(".list-title").html("");
        $("#deviceRowCount").val(0);
        $("#autoPingDevice").prop('checked',false);
        getActiveDeviceList(0);
    }
    
    function closeClaimInstance()
    { 
		$("#claimDeviceDialog").addClass("hide");
        //$("#device-manage .panel-body").addClass("panel-savings");
        $("#deviceContainer").removeClass("hide");
        $(".list-title").find("input:text").val("");
        $(".list-title").html("");
        $("#deviceRowCount").val(0);
        $("#autoPingDevice").prop('checked',false);
        getActiveDeviceList(0);
	}
	

    function editDevice(deviceId) {

        if ($(".popover").is(":visible") == true){
            $('.popover').popover('hide');
        }

        $.ajax ({
            type: 'POST',
            url:'ajax.php',
            data:{ 'c':'Device', 'a':'fetchDeviceDetails', 'deviceid':deviceId },
            dataType:'json',
            async:false,
            beforeSend: function() {__showLoadingAnimation();},
            success: function(response){
                if(response.status == 1)
                {

                    $("#editPopover_"+deviceId).popover('destroy');
                    $("#editPopover_"+deviceId).popover({
                        html : true,
                        placement:"bottom",
                        container:"body",
                        template:'<div class="popover" style="max-width: 355px !important;"><div class="arrow"></div><h5 class="popover-title bold bg-white"></h5><div class="popover-content clearfix"></div></div>',
                        content: function() {
							if(loggedUserRole != 4) readonly="readonly";
							else readonly="";
                                
                            var $HTMLRow = '<div class="row m-t-10" id="editDevice_'+response.device[0]["id"]+'" style="width:350px;">';

                                $HTMLRow += '<div class="col-sm-12">';
                                $HTMLRow += '<div class="form-group  form-group-default m-b-0 name" style="padding:8px">';
                                $HTMLRow += '<label>Instance name</label>';
                                $HTMLRow += '<input type="text" class="form-control" value="'+response.device[0]["device_name"]+'" id="utxtNewDeviceName" onkeyup="removeErrorAlert('+response.device[0]["id"]+')" />';
                                $HTMLRow += '<span class="text-danger hide"></span>';
                                $HTMLRow += '</div>';
                                $HTMLRow += '</div>';

                                $HTMLRow += '<div class="col-sm-12">';
                                $HTMLRow += '<div class="form-group  form-group-default m-b-0 instance-id" style="padding:8px">';
                                $HTMLRow += '<label>Instance ID</label>';
                                $HTMLRow += '<input type="text" class="form-control" value="'+response.device[0]["inspextorID"]+'" id="utxtInstanceID" onkeyup="removeErrorAlert('+response.device[0]["id"]+')"  '+readonly+'/>';
                                $HTMLRow += '<span class="text-danger hide"></span>';
                                $HTMLRow += '</div>';
                                $HTMLRow += '</div>';

                                $HTMLRow += '<div class="col-sm-12">';
                                $HTMLRow += '<div class="form-group  form-group-default m-b-0 txt-pass" style="padding:8px">';
                                $HTMLRow += '<label>Password</label>';
                                $HTMLRow += '<input type="password" class="form-control" value="'+response.device[0]["Password"]+'" id="utxtPassword" onkeyup="removeErrorAlert('+response.device[0]["id"]+')"  '+readonly+'/>';
                                $HTMLRow += '<span class="text-danger hide"></span>';
                                $HTMLRow += '</div>';
                                $HTMLRow += '</div>';

                                $HTMLRow += '<div class="col-sm-6">';
                                $HTMLRow += '<div class="form-group  form-group-default m-b-0 txt-duration" style="padding:8px">';
                                $HTMLRow += '<label>Data duration</label>';
                                
                                $HTMLRow += '<input type="text" class="form-control" value="'+response.device[0]["DataDuration"]+'" id="utxtDataDuration" onkeyup="removeErrorAlert('+response.device[0]["id"]+')" '+readonly+'/>';
								
                                $HTMLRow += '<span class="text-danger hide"></span>';
                                $HTMLRow += '</div>';
                                $HTMLRow += '</div>';

                                $HTMLRow += '<div class="col-sm-6">';
                                $HTMLRow += '<div class="form-group  form-group-default m-b-0 txt-space-limit" style="padding:8px">';
                                $HTMLRow += '<label>Data space limit</label>';
                                $HTMLRow += '<input type="text" class="form-control" value="'+response.device[0]["DataSpaceLimit"]+'" id="utxtDataSpaceLimit" onkeyup="removeErrorAlert('+response.device[0]["id"]+')" '+readonly+'/>';
                                $HTMLRow += '<span class="text-danger hide"></span>';
                                $HTMLRow += '</div>';
                                $HTMLRow += '</div>';

                                $HTMLRow += '<div class="col-sm-12">';
                                $HTMLRow += '<div class="form-group  form-group-default m-b-0 txt-report" style="padding:8px">';
                                $HTMLRow += '<label>Notified email address</label>';
                                $HTMLRow += '<input type="text" class="form-control" value="'+response.device[0]["ReportMailID"]+'" id="utxtReportBy" onkeyup="removeErrorAlert('+response.device[0]["id"]+')"  '+readonly+'/>';
                                $HTMLRow += '<span class="text-danger hide"></span>';
                                $HTMLRow += '</div>';
                                $HTMLRow += '</div>';
								
								if(loggedUserRole == 4) 	{
								$HTMLRow += '<div class="col-sm-12">';
                                $HTMLRow += '<div class="form-group  form-group-default m-b-0 sel-client" style="padding:8px">';
                                $HTMLRow += '<label>LINK WITH CLIENT</label>';
                                $HTMLRow += '<select id="uclientSelect" placeholder="Select Client" data-init-plugin="select2" name="client" style="width:300px;" required > </select>';
                                $HTMLRow += '<span class="text-danger hide"></span>';
                                $HTMLRow += '</div>';
                                $HTMLRow += '</div>';
								}

                                $HTMLRow += '<p class="pull-right m-t-10 m-r-15"><button type="button" class="btn bt-sm btn-success" onclick="updateDeviceName('+response.device[0]["id"]+')">Update instance</button><button onclick="return closeDevicePopover('+response.device[0]["id"]+');" type="button" class="btn bt-sm btn-danger m-l-10">Cancel</button></p></div><span id="alertClass" style="display:none;"></span>';

                            return $HTMLRow;
                            //return $(this).find(".popovercontent").html();
                        },
                        trigger:'focus'

                    });

                    $("#editPopover_"+deviceId).popover("show");
					var allClients = response.clientInfo;
					$("#uclientSelect").html('');
					var clientHTML ='';
					for (var i = 0; i < allClients.length; i++) {
						clientHTML += '<option value="' + allClients[i]['id'] + '" >' + allClients[i]['firstname']+'' + allClients[i]['lastname'] + '</option>';
					}
					$("#uclientSelect").append(clientHTML);
					$("#uclientSelect").select2().select2('val', []);

					if(response.device[0]["ClientID"] != 0)
					{
						//alert(response.device[0]["ClientID"]);
						$("#uclientSelect").select2().select2('val', response.device[0]["ClientID"]);
					}
                }
                else if( response.status == 0 ){

                }

            },
            complete:function (){__hideLoadingAnimation();},
            error: function(r){
                __hideLoadingAnimation();
                console.log(r);
            }

        });
    }

    function closeDevicePopover(id){
        $("#editPopover_"+id).popover("hide");
    }

    function removeDevice(deleteid){
        $("#btnCnfmYes").addClass("btn-success");
        $("#cnCntent").html("Do you wish to remove this instance?");
        $("#modalCnfirmation").modal("show");
        $("#delDeviceId").val(deleteid);
    }
    
    function emailCloudConnectionInfo(id)
    {
		$.ajax ({
            type:'POST',
            url:'ajax.php',
            data: { 'c':'Device', 'a':'sendDeviceClaimDetails', 'devId':id },
            dataType: 'json',
            beforeSend: function() {__showLoadingAnimation();},
            success: function(response){
                if(response.status == 1){
                    $createAlert({status :"success",title : "Success",text : response.message});
                }
                else if(response.status == 0){
                    $createAlert({status :"fail",title : "Failed",text : response.message});
                }
            },
            complete:function (){__hideLoadingAnimation();},
            error: function(r){
                __hideLoadingAnimation();
                console.log(r);
            }
        })
	}

    function removeConfirm()
    {
        var deviceId = $("#delDeviceId").val();

        $.ajax ({
            type:'POST',
            url:'ajax.php',
            data: { 'c':'Device', 'a':'removeDevice', 'deleteid':deviceId },
            dataType: 'json',
            beforeSend: function() {__showLoadingAnimation();},
            success: function(response){
                if(response.status == 1){
                    $createAlert({status :"success",title : "Success",text : response.message});
                    $("#deviceBox_"+deviceId).hide();
                    $("#modalCnfirmation").modal("hide");
                    $("#delDeviceId").val("");
                    if(response.active){
                        $("#_popDeviceList").find(".deviceName").html("Select instance");
                        $("#_popDeviceList").find(".deviceStatus").removeClass("bg-green bg-green-ball blink");
                        $("#_popDeviceList").find(".deviceStatus").removeClass("bg-red bg-red-ball");
                        $('#_popDeviceList').find(".device").prop('title', 'Click here to select instance.');
                    }
                }
                else if(response.status == 0){
                    $createAlert({status :"fail",title : "Failed",text : response.message});
                    $("#modalCnfirmation").modal("hide");
                    $("#delDeviceId").val("");
                }
            },
            complete:function (){__hideLoadingAnimation();},
            error: function(r){
                __hideLoadingAnimation();
                console.log(r);
            }
        })
    }

    function updateDeviceName(deviceId){


        var $devName = $("#utxtNewDeviceName").val();
        var $devID = $("#utxtInstanceID").val();
        var $devPassword = $("#utxtPassword").val();
        var $duration = $("#utxtDataDuration").val();
        var $space = $("#utxtDataSpaceLimit").val();
        var $email = $("#utxtReportBy").val();
		var $client = (loggedUserRole == 4 ) ? $("#uclientSelect").select2().val(): 0;
       //alert($client);return false;
        if($devName == "" || $devName == null){
            $("#editDevice_"+deviceId+" .name").addClass("has-error");
            $("#editDevice_"+deviceId+" .name span").html("Please enter instance name");
            $("#editDevice_"+deviceId+" .name span").removeClass("hide");
            $("#alertClass").html("name");
            return false;
        }
        if($devID == "" || $devID == null){
            $("#editDevice_"+deviceId+" .instance-id").addClass("has-error");
            $("#editDevice_"+deviceId+" .instance-id span").html("Please enter instance ID");
            $("#editDevice_"+deviceId+" .instance-id span").removeClass("hide");
            $("#alertClass").html("instance-id");
            return false;
        }
        if($devPassword == "" || $devPassword == null){
            $("#editDevice_"+deviceId+" .txt-pass").addClass("has-error");
            $("#editDevice_"+deviceId+" .txt-pass span").html("Please enter password");
            $("#editDevice_"+deviceId+" .txt-pass span").removeClass("hide");
            $("#alertClass").html("txt-pass");
            return false;
        }
        if($duration == "" || $duration == null){
            $("#editDevice_"+deviceId+" .txt-duration").addClass("has-error");
            $("#editDevice_"+deviceId+" .txt-duration span").html("Please enter duration");
            $("#editDevice_"+deviceId+" .txt-duration span").removeClass("hide");
            $("#alertClass").html("txt-duration");
            return false;
        }
        if(!isNumber($duration)){
            $("#editDevice_"+deviceId+" .txt-duration").addClass("has-error");
            $("#editDevice_"+deviceId+" .txt-duration span").html("Only numbers are allowed");
            $("#editDevice_"+deviceId+" .txt-duration span").removeClass("hide");
            $("#alertClass").html("txt-duration");
            return false;
        }
        if($space == "" || $space == null){

            $("#editDevice_"+deviceId+" .txt-space-limit").addClass("has-error");
            $("#editDevice_"+deviceId+" .txt-space-limit span").html("Please enter data space limit");
            $("#editDevice_"+deviceId+" .txt-space-limit span").removeClass("hide");
            $("#alertClass").html("txt-space-limit");
            return false;
        }
        if(!isNumber($space)){
            $("#editDevice_"+deviceId+" .txt-space-limit").addClass("has-error");
            $("#editDevice_"+deviceId+" .txt-space-limit span").html("Only numbers are allowed");
            $("#editDevice_"+deviceId+" .txt-space-limit span").removeClass("hide");
            $("#alertClass").html("txt-space-limit");
            return false;
        }
        if($email == "" || $email == null){
            $("#editDevice_"+deviceId+" .txt-report").addClass("has-error");
            $("#editDevice_"+deviceId+" .txt-report span").html("Please enter email address");
            $("#editDevice_"+deviceId+" .txt-report span").removeClass("hide");
            $("#alertClass").html("txt-report");
            return false;
        }
        if(!ValidateEmail($email)){
            $("#editDevice_"+deviceId+" .txt-report").addClass("has-error");
            $("#editDevice_"+deviceId+" .txt-report span").html("Invalid Email Address");
            $("#editDevice_"+deviceId+" .txt-report span").removeClass("hide");
            $("#alertClass").html("txt-report");
            return false;
        }
		if( loggedUserRole == 4 && ($client == "" || $client == null)){
            $("#editDevice_"+deviceId+" .sel-client").addClass("has-error");
            $("#editDevice_"+deviceId+" .sel-client span.text-danger").html("Please select client");
            $("#editDevice_"+deviceId+" .sel-client span.text-danger").removeClass("hide");
            $("#alertClass").html("sel-client");
            return false;
        }

        $.ajax ({
            type:'POST',
            url:'ajax.php',
            data: { 'c':'Device', 'a':'updateDevice',
                'devicename':$devName,
                'devID':$devID,
                'password':$devPassword,
                'space':$space,
                'duration':$duration,
                'email':$email,
				'clientId':$client,
                'updateId':deviceId },
            dataType: 'json',
            beforeSend: function() {__showLoadingAnimation();},
            success: function(response){
                if(response.status == 1){
                    $createAlert({status :"success",title : "Success",text : response.message});
                    $("#editPopover_"+deviceId).popover("hide");
                    $("#deviceBox_"+deviceId+" .device-list-name").html($devName);
                    $("#deviceBox_"+deviceId+" .device-ins-id").html($devID);
                    $("#deviceBox_"+deviceId+" .dev-device-duration").html($duration);
                    $("#deviceBox_"+deviceId+" .dev-device-space-limit").html($space+" Mb");
                    $("#deviceBox_"+deviceId+" .dev-device-mail").html($email);
                    /*$("#deviceBox_"+deviceId+" .ping-device-in").attr("onclick","pingDevice('"+response.ip+"','"+deviceId+"','"+response.serial+"');");*/

                }
                else if(response.status == 0){
                    $createAlert({status :"fail",title : "Failed",text : response.message});
                }
            },
            complete:function (){__hideLoadingAnimation();},
            error: function(r){
                __hideLoadingAnimation();
                $createAlert({status :"fail",title : "Failed",text : "Failed to update instance"});
                console.log(r);
            }

        });


    }

    function removeErrorAlert(id){
        var alertClass = $("#alertClass").html();
        //alert(alertClass);
        $("#editDevice_"+id+" ."+alertClass).removeClass("has-error");
        $("#editDevice_"+id+" ."+alertClass+" span").html("");
        $("#editDevice_"+id+" ."+alertClass+" span").addClass("hide");
    }

    function searchDevice(){
        var name     = $("#deviceName").val();
        var serial   = $("#deviceSerialNo").val();
        var ip       = $("#deviceIP").val();
        var location = $("#deviceLocation").val();

        $.ajax({
            type:'POST',
            url:'ajax.php',
            data:{ 'c':'Device', 'a':'searchDevice', 'devicename':name, 'serialNo':serial, 'ipaddress':ip, 'location':location },
            dataType:'json',
            beforeSend: function() {__showLoadingAnimation();},
            success: function(response){
                if(response.status==1){
                    $(".device-box").addClass("hide");
                    $(".search-result").addClass("hide");
                    for(var j=0; j<response.searchResult.length; j++)
                    {
                        $("#deviceBox_"+response.searchResult[j]['id']+" .ping-status").addClass("hide");
                        $("#deviceBox_"+response.searchResult[j]['id']).removeClass("hide");
                    }
                }
                else if(response.status==0){
                    $(".device-box").addClass("hide");
                    $(".search-result").removeClass("hide");
                }
            },
            complete:function (){__hideLoadingAnimation();},
            error:function(r){
                __hideLoadingAnimation();
                console.log(r);
            }
        })
    }

    function pingDevice(ipAddress,id,serialNo){
        $("#deviceBox_"+id+" .ping-status").html("Ping device...");
        $("#deviceBox_"+id+" .ping-status").removeClass("hide");
        $("#deviceBox_"+id+" .ping-status").removeClass("text-green text-danger");
        $("#deviceBox_"+id+" .loading").removeClass("hide");

        var dataArr = {
            action: 'PingDevice',
            data: {
                    DeviceIP: ipAddress
            },
            DeviceIP: ipAddress,
            source : 3 ,//1 = device 2 = galileo 3 = supervisor
            serialNo: serialNo,
            id : id
        };

        $.ajax({
            type:'POST',
            url: "deviceLookup.php",
            data: dataArr,
            dataType:'json',
            success: function(response){
                if(response != null && response.status == 1){
                   if(response.responseData.PingStatus == 1){
                        $("#deviceBox_"+id+" .ping-status").addClass("text-green");
                        $("#deviceBox_"+id+" .loading").addClass("hide");
                        $("#deviceBox_"+id+" .ping-status").html("Active");
                        $("#deviceBox_"+id+" #deviceActive").attr("class","");
                        $("#deviceBox_"+id+" #deviceActive").addClass("notify-bubble bg-green bg-green-ball");
                        $("#deviceBox_"+id+" #deviceActive").attr("data-original-title","Instance is Active");
                   }
                   else if(response.responseData.PingStatus == 0){
                        $("#deviceBox_"+id+" .ping-status").addClass("text-danger");
                        $("#deviceBox_"+id+" .loading").addClass("hide");
                        $("#deviceBox_"+id+" .ping-status").html("Inactive");
                        $("#deviceBox_"+id+" #deviceActive").attr("class","");
                        $("#deviceBox_"+id+" #deviceActive").addClass("notify-bubble bg-red bg-red-ball");
                        $("#deviceBox_"+id+" #deviceActive").attr("data-original-title","Instance is Inactive");
                   }
                }
                else{
                    $("#deviceBox_"+id+" .loading").addClass("hide");
                    $("#deviceBox_"+id+" .ping-status").addClass("text-danger");
                    $("#deviceBox_"+id+" .ping-status").html("Not Available");
                    $("#deviceBox_"+id+" #deviceActive").attr("class","");
                    $("#deviceBox_"+id+" #deviceActive").addClass("notify-bubble bg-red bg-red-ball");
                    $("#deviceBox_"+id+" #deviceActive").attr("data-original-title","Instance is Inactive");
                }
            },
            timeout: 60000,
            error: function(r){
                console.log(r);
                $("#deviceBox_"+id+" .loading").addClass("hide");
                $("#deviceBox_"+id+" .ping-status").addClass("text-danger");
                $("#deviceBox_"+id+" .ping-status").html("Not Available");
                $("#deviceBox_"+id+" #deviceActive").attr("class","");
                $("#deviceBox_"+id+" #deviceActive").addClass("notify-bubble bg-red bg-red-ball");
                $("#deviceBox_"+id+" #deviceActive").attr("data-original-title","Instance is Inactive");
            }
        });
    }

    function getActiveDeviceList(autoping)
    {
        //alert(autoping);
        var addActiveDevices = '';
        var device = '';
        $("#addDevices").addClass("hide");
        //$("#device-manage .panel-body").addClass("panel-savings");
        $("#deviceContainer").removeClass("hide");
        $(".list-title").find("input:text").val("");
        $(".list-title").html("");
        $("#deviceRowCount").val(0);
        $("#deviceList").html("");

        $.ajax ({
            type:'POST',
            url:'ajax.php',
            data: { 'c':'Device', 'a':'getActiveDevices' },
            dataType: 'json',
            async:false,
            success: function(response){
                //console.log(response);
                if(response.status == 1){
                    device = response.allDevices;

                    /*addActiveDevices += '<div class="row p-l-20 p-r-20 p-b-0"><div id="fiterDiv" style="display: none;" class="btn-indic col-sm-9 pull-left p-l-0">';
                    addActiveDevices += '<div class="col-sm-3 p-r-0"><div class="form-group m-b-10"><input type="text" placeholder="Name" class="form-control input-sm" id="deviceName"></div></div>';
                    addActiveDevices += '<div class="col-sm-3 p-r-0"><div class="form-group m-b-10"><input type="text" placeholder="Serial #" class="form-control input-sm" id="deviceSerialNo"></div></div>';
                    addActiveDevices += '<div class="col-sm-3 p-r-0"><div class="form-group m-b-10"><input type="text" placeholder="IP #" class="form-control input-sm" id="deviceIP"></div></div>';
                    addActiveDevices += '<div class="col-sm-3 p-r-0"><div class="form-group m-b-10"><input type="text" placeholder="Location" class="form-control input-sm" id="deviceLocation"></div></div></div>';
                    addActiveDevices += '<div class="col-sm-3 pull-right p-r-0"><button type="button" class="btn btn-blue pull-right btn-filter" onclick="showFiterSection();" id="btn-filter"><i class="fa fa-filter"></i> Filter</button>';
                    addActiveDevices += '<button type="button" class="btn btn-danger pull-right btn-filter m-l-5 hide" onclick="hideFiterSection();" id="btn-filterCancel">Cancel</button>';
                    addActiveDevices += '<button type="button" class="btn btn-success pull-right btn-filter m-r-5 hide" id="btn-filterSearch" onclick="searchDevice();">Search</button>';
                    addActiveDevices += '<!--div class="checkbox check-success pull-right  m-t-5 m-b-0"><input type="checkbox" checked="checked" value="1" id="checkbox2"><label for="checkbox2" class="bold">Select All</label></div--></div></div>';
                    addActiveDevices += '<div class="accordion search-result hide m-t-15"> No Search Results Found </div>';*/

                    for(var i=0; i<device.length; i++){
                        //alert(device[i]['id']);
                        /*var $inxID = "'"+device[i]['inspextorID']+"'"
                        var $duraton = "'"+device[i]['DataDuration']+"'"
                        var $space = "'"+device[i]['DataSpaceLimit']+"'"
                        var $email = "'"+device[i]['ReportMailID']+"'"*/

                        addActiveDevices += '<div class="col-sm-4 padding-5 device-box" id="deviceBox_'+device[i]['id']+'"><div class="device-lit-item cursor">';
                        addActiveDevices += '<div class="full-width clearfix b-b b-grey device-title "><span class="bold device-list-name">'+device[i]['device_name']+'</span><div class="bold m-b-5 pull-right">';
                        addActiveDevices += '<a class="text-white cursor pull-right" data-placement="top" data-toggle="tooltip" title="Edit Instance" id="editPopover_'+device[i]['id']+'" onclick="editDevice('+device[i]['id']+');" ><i class="fa fa-pencil"></i></a>';
                        if(loggedUserRole == 4){
                            
                            addActiveDevices += '<a class="text-white cursor pull-right m-r-15" data-placement="top" data-toggle="tooltip" title="Remove Device." onclick="removeDevice('+device[i]['id']+');"><i class="fa fa-trash"></i></a>';
                        }
                        addActiveDevices += '<a class="text-white cursor pull-right m-r-15" data-placement="top" data-toggle="tooltip" title="View dashboard." onclick="return popDeviceSel(this,true);" id="viewDetails_'+device[i]['id']+'"><i class="fa fa-eye"></i></a>';
                        if(loggedUserRole == 4){
							addActiveDevices += '<a class="text-white cursor pull-right m-r-15" data-placement="top" data-toggle="tooltip" title="Email cloud connection information to client." onclick="return emailCloudConnectionInfo('+device[i]['id']+');" id="emailClient_'+device[i]['id']+'"><i class="fa fa-envelope"></i></a>';
						}
                        addActiveDevices += '</div></div>';
                        addActiveDevices += '<div class="device-content full-width clearfix">';
                        if(device[i]['Status'] == 1){
                            addActiveDevices += '<span class="notify-bubble bg-green blink  bg-green-ball" id="deviceActive" data-toggle="tooltip" data-trigger="hover" data-placement="left" title="" data-original-title="Instance is Active"></span>';
                        }
                        else if(device[i]['Status'] == 0){
                            addActiveDevices += '<span class="notify-bubble bg-red bg-red-ball" id="deviceActive" data-toggle="tooltip" data-trigger="hover" data-placement="left" title="" data-original-title="Instance is Inactive"></span>';
                        }

                        //addActiveDevices += '<span class="notify-bubble bg-grey-master bg-grey-ball m-r-10" id="deviceAlert" data-toggle="tooltip" data-trigger="hover" data-placement="left" title="" data-original-title="No Alerts"></span>';
                        addActiveDevices += '<div class="full-width clearfix"><div class="dev-content-left"><span class="pull-left">Instance ID</span><span class="pull-right p-r-5">:</span></div><div class="dev-content-right"><span class="device-ins-id">'+device[i]['inspextorID']+'</span></div></div>';
                        addActiveDevices += '<div class="full-width clearfix"><div class="dev-content-left"><span class="pull-left">Data Duration</span><span class="pull-right p-r-5">:</span></div><div class="content-right"><span class="dev-device-duration">'+device[i]['DataDuration']+'day</span></div></div>';
                        addActiveDevices += '<div class="full-width clearfix"><div class="dev-content-left"><span class="pull-left">Data Limit</span><span class="pull-right p-r-5">:</span></div><div class="content-right"><span class="dev-device-space-limit">'+device[i]['DataSpaceLimit']+'Mb</span></div></div>';
                        addActiveDevices += '<div class="full-width clearfix"><div class="dev-content-left"><span class="pull-left">Notified Email</span><span class="pull-right p-r-5">:</span></div><div class="content-right"><span class="dev-device-mail">'+device[i]['ReportMailID']+'</span></div></div></div>';
                        //addActiveDevices += '<div class="ping-device clearfix"><span class="ping-status hide pull-left bold">Pinging device</span>';
                        addActiveDevices += '<div class="loading pull-left hide p-l-5"><div class="loading-bar"></div><div class="loading-bar"></div><div class="loading-bar"></div><div class="loading-bar"></div></div>';
                        addActiveDevices += '</div></div></div>';

                    }

                    $("#deviceList").html(addActiveDevices).promise().done(function(){
                        /*if(autoping==1){
                            autoPingDevice(device);
                        }*/
                    });
                    $('[data-toggle="tooltip"]').tooltip({container:"body"});
                }
                else if(response.status == 0){
                }
            },
            error: function(r){
                console.log(r);
            }
        });


    }

    function autoPingDevice(device)
    {
        var id='';
        var ipAddress='';

        for(var i=0; i<device.length; i++)
        {
            id = device[i]['id'];
            ipAddress = device[i]['ip_address'];

            $("#deviceBox_"+id+" .ping-status").html("Ping device...");
            $("#deviceBox_"+id+" .ping-status").removeClass("hide");
            $("#deviceBox_"+id+" .loading").removeClass("hide");

            var dataArr = {
                action: 'PingDevice',
                data: {
                        DeviceIP: ipAddress
                },
                source : 3 ,//1 = device 2 = galileo 3 = supervisor
                serialNo: serialNo
            };
            $.ajax({
                type:'GET',
                url:'https://'+ ipAddress +':8443/PMI-SP2000/WebAPI.jsp',
                data: dataArr,
                async: false,
                success: function(response){
                if(response != null && response.status == 1){
                   if(response.responseData.PingStatus == 1){
                        $("#deviceBox_"+id+" .ping-status").addClass("text-green");
                        $("#deviceBox_"+id+" .loading").addClass("hide");
                        $("#deviceBox_"+id+" .ping-status").html("Active");
                        $("#deviceBox_"+id+" #deviceActive").attr("class","");
                        $("#deviceBox_"+id+" #deviceActive").addClass("notify-bubble bg-green bg-green-ball");
                        $("#deviceBox_"+id+" #deviceActive").attr("data-original-title","Instance is Active");
                   }
                   else if(response.responseData.PingStatus == 0){
                        $("#deviceBox_"+id+" .ping-status").addClass("text-danger");
                        $("#deviceBox_"+id+" .loading").addClass("hide");
                        $("#deviceBox_"+id+" .ping-status").html("Inactive");
                        $("#deviceBox_"+id+" #deviceActive").attr("class","");
                        $("#deviceBox_"+id+" #deviceActive").addClass("notify-bubble bg-red bg-red-ball");
                        $("#deviceBox_"+id+" #deviceActive").attr("data-original-title","Instance is Inactive");
                   }
                }
                else{
                    $("#deviceBox_"+id+" .loading").addClass("hide");
                    $("#deviceBox_"+id+" .ping-status").addClass("text-danger");
                    $("#deviceBox_"+id+" .ping-status").html("Not Available");
                    $("#deviceBox_"+id+" #deviceActive").attr("class","");
                    $("#deviceBox_"+id+" #deviceActive").addClass("notify-bubble bg-red bg-red-ball");
                    $("#deviceBox_"+id+" #deviceActive").attr("data-original-title","Instance is Inactive");
                }
            },
            error: function(r){
                console.log(r);
                $("#deviceBox_"+id+" .loading").addClass("hide");
                $("#deviceBox_"+id+" .ping-status").addClass("text-danger");
                $("#deviceBox_"+id+" .ping-status").html("Not Available");
                $("#deviceBox_"+id+" #deviceActive").attr("class","");
                $("#deviceBox_"+id+" #deviceActive").addClass("notify-bubble bg-red blink  bg-red-ball");
                $("#deviceBox_"+id+" #deviceActive").attr("data-original-title","Instance is Inactive");
            }
            });
        }
    }

    function checkSerial(id){
        //alert(id);
        var value = $.trim($("#txtSerialNo_"+id).val());
        if(value!=""){
            if(!isNumber(value)){
                $createAlert({status :"info",title : "",text : "Only numbers are allowed for instance Serial No"});
                setTimeout(function() {
                    $("#txtSerialNo_"+id).focus();
                    return false;
                }, 200);
            }
            else{
                return true;
            }
        }
    }

    function checkIPaddress(id){
        //alert(id);
        var ipvalue = $.trim($("#txtIPaddress_"+id).val());
        if(ipvalue!=""){
            if(!ValidateIPaddress(ipvalue)){
                $createAlert({status :"info",title : "",text : "Invalid IP Address"});
                setTimeout(function() {
                    $("#txtIPaddress_"+id).focus();
                    return false;
                }, 200);
            }
            else{
                return true;
            }
        }
    }

	function clientValidation() {
        var client = $('#client-select').select2().val();
        if (client == null) {
            $("#divClient").addClass("has-error");
            $("#divClient span").html("Please Select Client");
            $("#divClient span").removeClass("hide");
            $("#client-select").focus();
        }
        else {
            $("#divClient").removeClass("has-error");
            $("#divClient span").html("");
            $("#divClient span").addClass("hide");
        }
    }

</script>
