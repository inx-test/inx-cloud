<?php
    // Include Helper Class
    require_once('config.php');
    require_once('_classes/DBC.php');
    require_once('vendor/autoload.php');
    
    use UDPProcess\UDPQueueClient;

    $db = new \DBC();
    $sql = "SELECT ID,NodeID,IPAddress,SerialNum FROM pmi_tag WHERE TagTypeID=1 AND `Status`=1 AND Acknowledged=".FIXTURE_TYPE_NODE." AND Deleted=0";
    $DevArr = $db->get_result($sql);
    $devCount = sizeof($DevArr);
    
    echo "Num Node = ".$devCount."\n";
    
    
    // Initialize and start the workers
    for ($i=0; $i<$devCount; $i++) {
        $data = array(
            "ID" => $DevArr[$i]["ID"],
            "NodeID" => $DevArr[$i]["NodeID"],
            "IPAddress" => $DevArr[$i]["IPAddress"],
            "SerialNum" => $DevArr[$i]["SerialNum"]
        );
        echo "\n Queue Start ... \n";
        print_r($data);
        $notQue = new UDPQueueClient(NOTIF_QUEUE_SERVER,SYSTEMLOG_QUEUE_NAME);
        $notQue->queueNotificationRequest(SAVINGS_REQ, $data);
    }