<?php //include_once("tpl/header.tpl.php"); 
include_once("tpl/dashboard-top.tpl.php"); ?>
<div class="content clearfix">

    <?php include_once("tpl/leftTreeViewPanel.tpl.php"); ?>
    
    <div class="content-left ">
          <div class="panel panel-transparent">
              <div class="panel-body">
                    <h5 class="text-center text-l-help " >Please select a node on tree menu.</h5>
                    <div class="terminal hide" id="termal-container">
                        <div class="top-bar">
                            <div class="title text-center bold fs-14"></div>

                        </div>

                        <div class="window">
                            <div class="term-inputbox  p-t-10">

                            </div>
                        </div>
                    </div>
                 
              </div>
          </div>
    </div>
    
</div>
<!-- END PAGE CONTENT -->
<?php include_once("tpl/footer.tpl.php"); ?>
<script src="js/jquery.terminal-1.0.15.min.js" type="text/javascript"></script>
<script src="js/jquery.mousewheel-min.js" type="text/javascript"></script>
<link href="css/jquery.terminal-1.0.15.css" rel="stylesheet" type="text/css"/>
<script src="js/index.js" type="text/javascript"></script>
<script>keyboardeventKeyPolyfill.polyfill();</script>
<script type="text/javascript">
    $showTerminalStatus = 1;
    var $activeNode = 0;
    $(document).ready(function () {
        $ClusterTree.init(0);  
    });
    
</script>

