<?php
include "config.php";
//include_once("tpl/header.tpl.php");
include_once("tpl/dashboard-top.tpl.php"); 
?>
<div class="content clearfix p-b-0">
    <?php include_once("tpl/leftTreeViewPanel.tpl.php"); ?>
    <!-- START PANEL -->
    

    <?php include_once("tpl/rightTreeViewPanel.tpl.php"); ?>
</div>
<!-- END PANEL -->

<?php include_once("tpl/footer.tpl.php"); ?>

<script type="text/javascript">
    var loggedUserId = <?php echo $loggedInUserId; ?>;
    var loggedUserRole = <?php echo $loggedUserRoleId; ?>;
    $policyStatus = 1;

    $(document).ready(function () {
        $ClusterTree.init(0);
        $('[data-toggle="tooltip"]').tooltip();
        $Policy.int();
        $Policy.getAll();
    });

    
</script>




