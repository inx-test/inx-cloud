<?php include_once("tpl/header.tpl.php"); ?>
<div class="content clearfix">
    
    <?php include_once("tpl/leftTreeViewPanel.tpl.php"); ?>
    
    <div class="content-left ">
          <div class="panel panel-transparent">
              <div class="panel-body">
                  
                    <div class="panel panel-default panel-savings">
                        <div class="panel-body padding-0 p-t-5">
                            <div class="col-sm-1 col-md-1 col-lg-1 col-xl-1">
                                <div class="checkbox check-primary checkbox-circle">
                                    <input type="checkbox" value="1" id="chGaugesDev" onchange="$gauges.changeType(1);" checked="checked">
                                    <label for="chGaugesDev" class="bold">Node</label>
                                </div>
                            </div>
                            <div class="col-sm-11 col-md-3 col-lg-3 col-xl-3">
                                 <select class="full-width" id="selGaugesNode" onchange="$gauges.displayGuages();">
                                       
                                 </select>
                            </div>
                            
                            <div class="col-sm-1 col-md-1 col-lg-1 col-xl-1">
                                <div class="checkbox check-primary checkbox-circle">
                                    <input type="checkbox" value="1" id="chGaugesCluster" onchange="$gauges.changeType(2);">
                                    <label for="chGaugesCluster" class="bold">Cluster</label>
                                </div>
                            </div>
                            <div class="col-sm-11 col-md-4 col-lg-4 col-xl-4">
                                 <select class="full-width" id="selGaugesCluster" disabled onchange="$gauges.displayGuages();">
                                       
                                 </select>
                            </div>
                            
                            <?php if( $loggedUserRoleId == ADMIN_ROLE) { ?>
                                
                                <div class="col-sm-11 col-md-3 col-lg-3 col-xl-3 p-l-0">
                                    <select id='selGauge' data-init-plugin="select2" name='selGauge' class="full-width" onchange="__displayGaugeParam(this);">
                                        <option value="">Select a gauge to edit settings</option>
                                    </select>
                                </div>
                            <?php } ?>
                            
                        </div>
                    </div>
                    <div class="box-gague-settings m-t-10 clearfix panel-savings" id="div-box-gague-settings" style="display:none;">

                    </div>
                    <div class="clearfix" id="guageContainer">
                        <h5 class="text-center text-l-help">Please select node or cluster above.</h5>
                    </div>
              </div>
          </div>
    </div>
    
</div>


<!--canvas class="hide"></canvas-->
<div id="div-all-guage-popover-content" style="display: none;">
    <script src="js/time-line/timeline.js" type="text/javascript"></script>
    <link href="js/time-line/timeline.css" rel="stylesheet" type="text/css"/>
</div>
<!-- END PAGE CONTENT -->
<?php include_once("tpl/footer.tpl.php"); ?>
<script type="text/javascript">

    $showGaugesStatus = 1;
    $(document).ready(function () {
        $ClusterTree.init(0);  
        $gauges.init();
        //$ClusterTree.getAllCluster("#selLiveCluster");
        //$ClusterTree.getAllNode("#selLiveNode");
        //displayGuages();
    });

</script>

