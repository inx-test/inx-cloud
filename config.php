<?php
	$homePath = '/home/inspexto/';
	$path = $homePath.'public_html/poe';
	set_include_path(get_include_path() . PATH_SEPARATOR . $path);


	if( php_sapi_name() !== 'cli' ) session_start();

	ini_set("display_errors",0);

	// Session Timout
	set_time_limit ( 60*10 );

	// Set timezone
	$timezone = "America/New_York";
	//$timezone = "Asia/Kolkata";
	if(function_exists('date_default_timezone_set')) date_default_timezone_set($timezone);

	// Include some common files & classes
	require_once("_classes/DBC.php");
	require_once("_classes/ControllerBase.php");
	require_once("_classes/CommonFunction.php");
	
	define("LOG_DATA_FETCH_LIMIT",100);
	define("UTCTIME",'-05:00');

	define('APL',"/var/www/");

	//Role configuration
	define('ADMIN_ROLE', 1);
	define('ASSISTANT_ROLE', 2);
	define('USER_ROLE', 3);
	define('SUPER_ADMIN_ROLE', 4);

	// Email, URL & Logo
	define('FROM_EMAIL', "pmisupervisor1@gmail.com");
	define("COMPANY_NAME","PMI-Director");
	define("COMPANY_LOGO","pmi-logo.png");
	define("SITE_URL","http://localhost/InxCloud/poe/");

	// Root path of website & other image urls
	define('ROOTPATH',$path);
	define('PROFILEPIC_PATH',  ROOTPATH."img/profiles/");
	define('THUMBNAILS_PATH',  ROOTPATH."img/thumbnails/");
	define('FILE_PATH',  ROOTPATH."files/");
	define('CROP_THUMBNAIL_UPLOADURL',"img/thumbnails/");

	// Set selected instance name to session or read from sesssion if alreay there
	if( php_sapi_name() !== 'cli' ) $sessionDBName = ( isset($_SESSION) && isset($_SESSION['INX_CLUD_DB_NAME']) ) ? $_SESSION['INX_CLUD_DB_NAME'] : 'inspexto_ems_cloud';
	else $sessionDBName = 'inspexto_ems_cloud';

	if( empty($sessionDBName) ) $sessionDBName = 'inspexto_ems_cloud';

	// Root database instance
	define("DB_HOST_ROOT",'localhost');
	define("DB_USER_ROOT",'root');
	define("DB_PASSWD_ROOT",'');

	// Using same info to connect to other databases
	define("DB_HOST",DB_HOST_ROOT);
	define("DB_USER",DB_USER_ROOT);
	define("DB_PASSWD",DB_PASSWD_ROOT);
	define("DB_NAME",$sessionDBName);
	define("DB_NAME_CLOUD",'inspexto_ems_cloud');

	// Private Key Password
	define('PRIVATE_KEY','PmiTech@2k18$$!');

	// Cloud details
	// Where to upload local backup from local instance to cloud
	define('SERVER_URL',$homePath.'public_ftp/inspextor/');
	define('SERVER_IMPORT',"import");
	define('SERVER_LOCATION',"location");
	define('DATABASE_STRUCTURE',"structure.sql");
	define('DATABASE_DATA',"data.sql");

	//define('SERVER_DOMAIN','usm1341.sgded.com');
	define('SERVER_DOMAIN','162.241.154.213');
	//define('SERVER_PORT','18765');
	define('SERVER_PORT','22');
	define('SERVER_USER',"root");
	define('SERVER_PASSWORD','PmiTech@2k18$$!');

	//Data Polling Worker
	define('DATAWORKERCOUNT',2);

	//Notification Count
	define('NOTIFICATIONWORKERCOUNT',2);

	define('ALERT_DELAY_TIME',15);

	//Delete previous record befor LOG_DATA_DELETE_SPAN years, 1 - 1year; 2- 2yrs...
	define('LOG_DATA_DELETE_SPAN',3);

	define('INX_DB_PREFIX',"inspextor_local_");

	$modX = array();
	$modX[1]["ModelNo"] = "SP1000A";$modX[0]["Volt"] = "208";$modX[0]["Model"] = "1";
	$modX[2]["ModelNo"] = "SP1000A2";$modX[1]["Volt"] = "208";$modX[1]["Model"] = "1";
	$modX[3]["ModelNo"] = "SP1000B";$modX[2]["Volt"] = "208";$modX[2]["Model"] = "1";
	$modX[4]["ModelNo"] = "SP1000B2";$modX[3]["Volt"] = "208";$modX[3]["Model"] = "1";
	$modX[5]["ModelNo"] = "SP1000C";$modX[4]["Volt"] = "370";$modX[4]["Model"] = "2";
	$modX[6]["ModelNo"] = "SP1000C2";$modX[5]["Volt"] = "370";$modX[5]["Model"] = "2";
	$modX[7]["ModelNo"] = "SP1000D";$modX[6]["Volt"] = "370";$modX[6]["Model"] = "2";
	$modX[8]["ModelNo"] = "SP1000D2";$modX[7]["Volt"] = "370";$modX[7]["Model"] = "2";
	$modX[9]["ModelNo"] = "SP1000E";$modX[8]["Volt"] = "480";$modX[8]["Model"] = "3";
	$modX[10]["ModelNo"] = "SP1000E2";$modX[9]["Volt"] = "480";$modX[9]["Model"] = "3";
	$modX[11]["ModelNo"] = "SP1000E3";$modX[10]["Volt"] = "480";$modX[10]["Model"] = "3";
	$modX[12]["ModelNo"] = "SP1000E7";$modX[11]["Volt"] = "480";$modX[11]["Model"] = "3";
	$modX[13]["ModelNo"] = "SP1000E8";$modX[12]["Volt"] = "480";$modX[12]["Model"] = "3";
	//GLOBAL MODEL ARRAY
	$GLOBALS["MODELNO"] = $modX;

	$TIME_ZONES = ['GMT','CET','EET','AST','GST','MVT',
	  'BST','CXT','HKT','JST','AET','NCT',
	  'NZST','EGT','FNT','GFT','FKT','EST',
	  'CST','MST','PST','AKST','HAST','NUT','AoE'
	];

	$GLOBALS["TIME_ZONES"] = $TIME_ZONES;

	define('NOTIF_QUEUE_SERVER','inspextor.com:11300');
	define('SYSTEMLOG_INBOUND_QUEUE_NAME','inspecxtoInboundQueue');
	define('OUTBOUND_QUEUE_NAME','inspecxtoQueueOutboundQueue');
	define("QUEUE_HIGH_PRIORITY",0);

	//QUEUE PROCESS
	define('SYNCHRONIZE','synchronize');
	define('INITIAL_SYNCHRONIZE','initialSync');
	define('ALERTMSG_SYNCHRONIZE','alertMessage');

	define('PKT_ACK',2);
	define('EVENT_COM',3);
	define('GET_STATUS_RES',4);
	define('GET_CISCO_DATA',5);
	define('TAG_STATUS_REQ',6);
	define('IP_CHANGE_PROCESS',7);
	define('POLICY_PROCESS',8);
	define('POLICY_MERGE_CHECK_PROCESS',9);
	define('SAVINGS_REQ',10);
	define('SAVINGS_PROCESS',11);
	define('POLICY_TIMELINE_PROCESS',12);
	define('SET_TIME_REQ',13);
	define('RESTORE_TO_DEFAULT_REQ',14);
	define('CHECK_PORT',15);

	//Queue Resend Delay time
	define('RESEND_DELAY',1);
	define('POLICY_CHECK_DELAY',10);

	// Autoload
	define('CISCO_SSH_LOGIN','root');
	define('CISCO_SSH_PASSWORD','pmi2017');

	//Alert severity

	define('S_NORMAL',1);
	define('S_ACCEPTABLE',2);
	define('S_ABOVE_NORMAL',3);
	define('S_SERVER',4);
	define('S_AMBIENT',5);
	define('S_ABOVE_AMBIENT',6);
	define('S_CRITICAL',7);

	// Alert type
	define('ALERT_OTHER',0);
	define('ALERT_DEF',1);
	define('ALERT_POLICY',2);
	define('ALERT_POLICY_COMPLETE',3);
	define('ALERT_RESTORE_TO_DEFAULT',4);

	// Alert Message
	define("POLICY_ALERT_CR_TITLE","Failed to merge policy ");
	define("POLICY_ALERT_CR_MSG","[[POLICY_NAME]] merge attempt failed for [[NODE]].");
	define("POLICY_ALERT_REMOVE_TITLE","Failed to remove policy");
	define("POLICY_ALERT_REMOVE_MSG","[[POLICY_NAME]] remove attempt failed for [[NODE]].");
	define("POLICY_ALERT_COUNT_REACHED", "Maximum policy count reached.");

	$pmiSeverityColor = array(
	  S_NORMAL => "rgba(52, 168, 83, 1)",
	  S_ACCEPTABLE => "rgba(66, 133, 244, 1)",
	  S_ABOVE_NORMAL => "rgba(0, 79, 149, 1)",
	  S_SERVER => "rgba(251, 188, 5, 1)",
	  S_AMBIENT => "rgba(0, 79, 149, 1)",
	  S_ABOVE_AMBIENT => "rgba(52, 168, 83, 1)",
	  S_CRITICAL => "rgba(234, 67, 53, 1)",
	);
	$GLOBALS["PMI_SER_COLOR"]  = $pmiSeverityColor;

	$pmiSeverityName = array(
	  S_NORMAL => "Normal",
	  S_ACCEPTABLE => "Acceptable",
	  S_ABOVE_NORMAL => "Above Normal",
	  S_SERVER => "Severe",
	  S_AMBIENT => "Ambient",
	  S_ABOVE_AMBIENT => "Above Ambient",
	  S_CRITICAL => "Critical",
	);
	$GLOBALS["PMI_SER_NAME"]  = $pmiSeverityName;

	// Cloud SMTP Information
	//define("CLOUD_MAIL_SERVER","gator3132.hostgator.com");
	define("CLOUD_MAIL_SERVER","ins.inspextor.com");
	define("CLOUD_MAIL_PORT",465);
	define("CLOUD_MAIL_EMAIL","remo@adithyainfosoft.com");
	define("CLOUD_MAIL_PASSWORD","genius_rmo123");
	define("CLOUD_MAIL_FROM","inspextor-cloud@mhtlighting.com");
	
	$db = new DBC();
?>
