  <?php
    include_once("tpl/dashboard-top.tpl.php");
   ?>

	<div class="content clearfix m-b-40">
      <h3 class="page-title">Dashboard</h3>
        <!-- Overall Wrapper -->
        <div class='row'>
            <div class='col-sm-12 no-padding'>
                <div class="panel panel-transparent">
                    <div class="panel-body no-padding">

                        <!-- Daily Report -->
                        <div id="portlet-advance-1" class="panel panel-default cs-wrapper">
                            <div class="panel-heading ">
                                <div class="panel-title"><span id="reportFor"> Hourly</span> Report
                                </div>
                                <div class="panel-controls">
                                    <ul>
                                        <!-- Drop down starts for chart types --->
                                        <li>
                                            <select  data-init-plugin="select2" id="chartsDropDown" onchange="$dasboardreport.__chartToggle(this.value);">
                                                <option value="0">Bar Chart</option>
                                                <option value="1" selected="selected">Pie chart</option>
                                            </select>
                                        </li>
                                        <!-- Drop down ends --->
                                        <!-- Drop down starts for date/time range --->
                                        <li>

                                            <select  data-init-plugin="select2" id="dateRangeDropDown" onchange="$dasboardreport.__dateRangeToggle(this.value);">
                                                <option value="0">Hourly</option>
                                                <option value="1">Daily</option>
                                                <option value="2">Weekly</option>
                                                <option value="3">Monthly</option>
                                            </select>
                                        </li>
                                        <!-- Date/time range drop down ends --->
                                        <!-- Time picker starts --->
                                        <li id="timePicker">

                                            <select  data-init-plugin="select2" id="hourDropDown" onchange="$dasboardreport.__drHourChange(this.value);">
                                                <?php
                                                    $label = "";
                                                    $id = 0;
                                                    for ($i = 1; $i < 25; $i ++) {
                                                        if ($i == 1){
                                                            $label = '12 AM';
                                                            $id = 0;
                                                        }
                                                        else{
                                                            $label = ($i - 1) . ' AM';
                                                            $id = $i-1;
                                                        }

                                                        if ($i == 13)
                                                            $label = '12 PM';
                                                        else if ($i > 13)
                                                            $label = ($i - 13) . ' PM';

                                                    ?>
                                                    <option value="<?php echo $id; ?>"><?php echo $label; ?></option>
                                                <?php } ?>
                                            </select>
                                        </li>
                                        <!-- Time picker ends --->
                                        <li id="dateRangePickerWrapper" >
                                            <div id="dateRangePicker" class="m-r-10 dtRangePicker padding-10">
                                                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                                                <span></span> <b class="caret"></b>
                                            </div>
                                        </li>

                                        <li data-placement="left" data-toggle="tooltip"  data-original-title="Toggle full screen" ><a href="#" class="portlet-maximize" data-toggle="maximize">
                                                <img src="img/full-screen-icon.png"/>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="panel-body">

                                <div class="row" id="barChartWrapper" style="display:none;">
                                     <div class="col-lg-10 no-padding">
                                    <div id="div-report-chart-wrapper" class="m-t-15" style="width: 95%;">
                                    </div>
                                    </div>
                                  <div class="col-lg-2 no-padding m-t-15">
                                      <ul class="pull-right">
                                          <li><div class="hr-chart-leg-box" style="background: #00b156;"></div><span class="bold"> Normal </span></li>
                                          <li><div class="hr-chart-leg-box" style="background: #4179f8;"></div><span class="bold"> Acceptable </span></li>
                                          <li><div class="hr-chart-leg-box" style="background: #004f95;"></div><span class="bold"> Above Normal </span></li>
                                          <li><div class="hr-chart-leg-box" style="background: #ffbd00;"></div><span class="bold"> Severe </span></li>
                                          <li><div class="hr-chart-leg-box" style="background: #ff1407;"></div><span class="bold"> Critical </span></li>
                                      </ul>
                                  </div>
                                </div>


                                <!-- 3rd row -->
                                <div class="row m-b-2" id="pieChartWrapper">
                                    <div class="col-lg-4 no-padding">
                                        <div class="card card-block divTempChart_main">
                                            <h5 class="card-title report-chart-title col-md-10 p-l-0 p-b-5">Temperature</h5>
                                            <div id="divTempChart" ></div>
                                            <div id="pie_legend"></div>
                                            <div class="nodata-block hide"><strong>No Data Available.</strong></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 no-padding">
                                        <div class="card card-block divkwhChart_main">
                                            <h5 class="card-title report-chart-title col-md-10 p-l-0 p-b-5">kWh Consumption</h5>
                                            <div id="divkwhChart" ></div>
                                            <div class="nodata-block hide"><strong>No Data Available.</strong></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 no-padding">
                                        <div class="card card-block divoccupChart_main">
                                            <h5 class="card-title report-chart-title col-md-10 p-l-0 p-b-5">Occupancy</h5>
                                            <div id="divoccupChart" ></div>
                                            <div class="nodata-block hide"><strong>No Data Available.</strong></div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /3rd row -->

                                <div class="row hidden" id="pieChartWrapper" style="display:none;">
                                    <!-- Starts Pie chart 1 -->
                                    <div class='col-md-4 no-padding'>
                                        <div class="row">
                                            <div class='col-md-12 p-t-15'>
                                                <span class='fs-20'> Temperature </span>
                                            </div>
                                            <div class='col-md-12'>
                                                <canvas id="temperture-chart" width="400" height="400"></canvas>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Ends Pie chart 1 -->
                                    <!-- Starts Pie chart 1 -->
                                    <div class='col-md-4 no-padding '>
                                        <div class="row">
                                            <div class='col-md-12'>
                                                kWh Consumption
                                            </div>
                                            <div class='col-md-12'>
                                                <canvas id="kwh-consumption-chart" width="400" height="400"></canvas>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Ends Pie chart 1 -->
                                    <!-- Starts Pie chart 1 -->
                                    <div class='col-md-4 no-padding'>
                                        <div class="row">
                                            <div class='col-md-12'>
                                                Occupancy
                                            </div>
                                            <div class='col-md-12'>
                                                <canvas id="ocuupancy-chart" width="400" height="400"></canvas>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Ends Pie chart 1 -->
                                </div>

                                <div class="dash-loader daily-chart-loader">
                                    <div class="loading">
                                        <div class="loading-bar"></div>
                                        <div class="loading-bar"></div>
                                        <div class="loading-bar"></div>
                                        <div class="loading-bar"></div>
                            </div>
                        </div>
                            </div>
                        </div>
                        <!-- End Daily -->
                        <!-- kWh Report -->
                        <div id="portlet-advance-2" class="panel panel-default  cs-wrapper">
                            <div class="panel-heading ">
                                <div class="panel-title" id="clientReportTitle">kWh Per Hour
                                </div>
                                <div class="panel-controls">
                                    <ul>
                                        <!-- Drop down starts for device --->
                                        <li>
                                            <select  data-init-plugin="select2" id="selDeviceType" style="width: 100px;" onchange="$clientReport.__setDevice();">
                                                <option value="1">All</option>
                                                <option value="2">Node</option>
                                                <option value="3">Cluster</option>
                                            </select>
                                        </li>
                                        <!-- Drop down ends --->

                                        <!-- Drop down starts for device --->
                                        <li>
                                            <select data-init-plugin="select2" id="selDev" style="width: 200px;" onchange="$clientReport.changeDevlist(this);" disabled>
                                                <option value="-1">All selected</option>
                                            </select>
                                        </li>
                                        <!-- Drop down ends --->

                                        <!-- Drop down starts for chart parameters --->
                                        <li>

                                            <select  data-init-plugin="select2" id="selParam" style="width: 150px;" onchange="$clientReport.intBarchart();">
                                                <option value="1">kWh</option>
                                                <option value="2">Temperature</option>
                                                <option value="3">Occupancy</option>
                                                <option value="4">Savings</option>
                                            </select>
                                        </li>
                                        <!-- Drop down ends --->
                                        <!-- Drop down starts for date/time range --->
                                        <li>

                                            <select  data-init-plugin="select2" id="seldateRange" onchange="$clientReport.__dateRangeToggle(this.value)">
                                                <option value="0">Hourly</option>
                                                <option value="1">Daily</option>
                                                <option value="2">Weekly</option>
                                                <option value="3">Monthly</option>
                                            </select>
                                        </li>
                                        <!-- Date/time range drop down ends --->
                                        <!-- Time picker starts --->
                                        <li id="chartTimePicker">
                                            <select  data-init-plugin="select2" id="selTimeRange" onchange="$clientReport.intBarchart();">
                                                <?php
                                                    $label = "";
                                                    $id = 0;
                                                    for ($i = 1; $i < 25; $i ++) {
                                                        if ($i == 1){
                                                            $label = '12 AM';
                                                            $id = 0;
                                                        }
                                                        else{
                                                            $label = ($i - 1) . ' AM';
                                                            $id = $i-1;
                                                        }

                                                        if ($i == 13)
                                                            $label = '12 PM';
                                                        else if ($i > 13)
                                                            $label = ($i - 13) . ' PM';

                                                    ?>
                                                    <option value="<?php echo $id; ?>"><?php echo $label; ?></option>
                                                <?php } ?>
                                            </select>
                                        </li>
                                        <!-- Time picker ends --->

                                        <li id="chartDateRangePickerWrapper" >
                                            <div id="chartdateRangePicker" class="m-r-10 dtRangePicker padding-10" onchange="">
                                                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                                                <span></span> <b class="caret"></b>
                                            </div>
                                        </li>

                                        <li data-placement="left" data-toggle="tooltip"  data-original-title="Toggle full screen" ><a href="#" class="portlet-maximize" data-toggle="maximize">
                                                <img src="img/full-screen-icon.png"/>
                                            </a>
                                        </li>

                                    </ul>
                                </div>
                            </div>
                            <div class="panel-body" style="overflow: hidden;">
                                <div class=" m-t-15" id="div-chart-wrapper" style="background-color:#fff; border-radius:6px;min-height:228px">
                                </div>

                                <a href="javascript:void(0);" class="live-timeline offline hide" onclick="return $TimeLine.triggerLive();" data-toggle="tooltip" data-placement="top" data-title="Click to track live record." data-container="body"><span>offline</span></a>
                                <div class="full-width  text-center">

                                    <ul class="inline timeline-legend">
                                        <!--li class="inline timeline-legend-peak"></li>
                                        <li class="inline p-r-5">
                                            Peak Demand
                                        </li-->
                                        <li class="inline timeline-legend-event"></li>
                                        <li class="inline p-r-5">
                                            Events
                                        </li>
                                        <li class="inline timeline-legend-policy"></li>
                                        <li class="inline p-r-5">
                                            Policies
                                        </li>
                                        <li class="inline timeline-legend-savings"></li>
                                        <li class="inline p-r-5">
                                            Savings
                                        </li>
                                    </ul>
                                </div>
                                <div id="timeLineContainer" class="p-l-15">

                                </div>
                            </div>

                            <div class="dash-loader main-chart-loader">
                                <div class="loading">
                                    <div class="loading-bar"></div>
                                    <div class="loading-bar"></div>
                                    <div class="loading-bar"></div>
                                    <div class="loading-bar"></div>
                        </div>
                            </div>
                        </div>


                        <!-- Alerts Report -->
                        <div id="portlet-advance-3" class="panel panel-default bg-success cs-wrapper">
                            <div class="panel-heading nbb">
                                <div class="panel-title" id="alertsTitle">
                                </div>
                                <div class="panel-controls">
                                    <ul>
                                        <li>
                                            <a href="javascript:void(0);" id="btnClearAlertFilter" onclick="$TimeLine.__clearAlertFilter();" class="fs-15 p-r-10 hidden" ><u>Clear filters</u></a>
                                        </li>
                                        <li data-placement="left" data-toggle="tooltip"  data-original-title="Toggle full screen" ><a href="#" class="portlet-maximize" data-toggle="maximize">
                                                <img src="img/full-screen-icon.png"/>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="panel-body" style="height:200px; max-height:200px; overflow-y: auto;">

                                <!-- Alerts -->
                                <table class="table table-condensed table-hover">
                                    <tbody class="dashboardEvents">

									</tbody>
                                </table>

                            </div>
                            <div class="dash-loader timeline-loader" style="display:none;">
                                <div class="loading">
                                    <div class="loading-bar"></div>
                                    <div class="loading-bar"></div>
                                    <div class="loading-bar"></div>
                                    <div class="loading-bar"></div>
                        </div>
                            </div>
                        </div>


                    </div>
                </div>

            </div>
            <!-- BEGIN CORE TEMPLATE JS -->
            <script src="js/__minified.js"  type="text/javascript" charset="UTF-8"></script>
            <!-- END CORE TEMPLATE JS -->

            <!-- START TREE VIEW CONTENT -->

            <input type="hidden" id="hdnTreeViewActiveKey" name="hdnTreeViewActiveKey" value="">

            <!-- END TREE VIEW CONTENT -->

            <!--  BEGIN CHART FILES  -->

            <!-- CHART JS -->
            <script src="js/chartjs/Chart.bundle.min.js" type="text/javascript"></script>
             <!--script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.2.1/Chart.min.js" type="text/javascript"></script-->
			 <script src="js/chartjs/Chart.min.js" type="text/javascript"></script>
            <!--script src="js/chartjs/hammer.min.js" type="text/javascript"></script-->
            <script src="js/chartjs/Chart.Zoom.min.js" type="text/javascript"></script>
            <!--script src="js/chartjs/Chart.Scatter.min.js" type="text/javascript"></script-->
            <script src="js/chartjs/dateFormat.min.js" type="text/javascript"></script>
            <script src="js/chartjs/Please-compressed.js" type="text/javascript"></script>

            <script src="js/jquery-dynatree/jquery.dynatree.min.js" type="text/javascript"></script>




            <!--  END CHART FILES  -->
            <!-- BEGIN PAGE LEVEL JS -->
            <script src="js/__scripts.js" type="text/javascript" charset="UTF-8"></script>
            <!--script src="js/raw/bootstrap-datetimepicker.min.js" type="text/javascript"></script-->
            <!-- END PAGE LEVEL JS -->

            <!-- BEGIN BOOTSTRAP DATATABLE JS -->
            <script src="js/jquery.dataTables.min.js" type="text/javascript"></script>
            <script src="js/dataTables.bootstrap.min.js" type="text/javascript"></script>
            <script src="js/time-line/timeline.js" type="text/javascript"></script>
            <script>

            var $liveChartObj;
            var $liveChartIntCallObj;
			var __DashboardRunTime = 5000;
            $(document).ready(function(){
                $dasboardreport.init();
                $clientReport.init();
				$("#__notify").on("click" ,function(){
					var $url =__getBaseUrl()[0]+"alertList.php";
					window.location.replace($url);
				});
            });

            var $drStartDate = new Date();
            var $drEndDate = new Date();
            var $drLiveMode = true;
            var $pieChartTimer = null;
            var $drBarTimer = null;

            var $dasboardreport = {
                init: function () {
                    var current_time = new moment().format("HH");
                    $('#hourDropDown').select2("val",parseInt(current_time));
                    $dasboardreport.__initDateRangePicker(0);
                    //$('#barChartWrapper').hide();
                    //$('#pieChartWrapper').show();
                    //$dasboardreport.__initPieChart();
                    //$dasboardreport.__initBarParameter();
                    $dasboardreport.__initPortlet();
                    //$dasboardreport.__initBarChart();
                    //$dasboardreport.__generateRandEvents();
                },
                __initBarParameter: function () {
                    var maxConsumptionForSelectedRange = [];
                    var avgMaxConsumptionCurrentMonth = [];
                    var avgMinConsumptionCurrentMonth = [];
                    var avgOfSingleBar = [];

                    for (var i = 0; i < 24; i++) {
                        var date = new Date();
                        var ts = date.getTime();
                        var maxBar = Math.floor(Math.random() * 10) + 1;
                        var avgMinMonth = Math.floor(Math.random() * 5) + 1;
                        var avgMaxMonth = Math.floor(Math.random() * 10) + 5;
                        var avgOnTime = (maxBar + avgMaxMonth + avgMaxMonth) / 3;

                        maxConsumptionForSelectedRange.push({'x': ts, 'y': maxBar});
                        avgMaxConsumptionCurrentMonth.push({'x': ts, 'y': maxBar});
                        avgMinConsumptionCurrentMonth.push({'x': ts, 'y': maxBar});
                        avgOfSingleBar.push({'x': ts, 'y': maxBar});
                    }

                    var data = [
                        {
                            "key": "Max Consumption",
                            "color": "#1689c1",
                            "values": maxConsumptionForSelectedRange
                        },
                        {
                            "key": "Average Min",
                            "color": "#7db1d2",
                            "values": avgMinConsumptionCurrentMonth
                        },
                        {
                            "key": "Average Max",
                            "color": "#d40d10",
                            "values": avgMaxConsumptionCurrentMonth
                        },
                        {
                            "key": "Average",
                            "color": "#f0bb0b",
                            "values": avgMaxConsumptionCurrentMonth
                        }

                    ];
                },
                /*====================================================
                 * Toggle for charts -  0=>Bar chart, 1=>Pie Chart
                 ============================================*/
                __chartToggle: function ($type) {
                    //var $btnText = '<i class="fa fa-bar-chart" aria-hidden="true"></i> Bar Chart ';
                    if ($type == 1) {
                        //$btnText = '<i class="fa fa-pie-chart" aria-hidden="true"></i> Pie chart';
                        $('#barChartWrapper').hide();
                        $('#pieChartWrapper').show();
                        $dasboardreport.__initPieChart();
                    }
                    else {
                        $('#pieChartWrapper').hide();
                        $('#barChartWrapper').show();
                        $dasboardreport.__initBarChart();
                    }

                },
                /*====================================================
                 * Toggle for charts -  0=>Bar chart, 1=>Pie Chart
                 ============================================*/
                __dateRangeToggle: function (m) {
                    var $btnText = 'Hourly';
                    if (m == 0) {
                        $('#timePicker').show();
                        $dasboardreport.__initDateRangePicker(m);
                    }
                    else if (m == 1) {
                        $btnText = 'Daily';
                        $('#timePicker').hide();
                        $dasboardreport.__initDateRangePicker(m);
                    }
                    else if (m == 2) {
                        $btnText = 'Weekly';
                        $('#timePicker').hide();
                        $dasboardreport.__initDateRangePicker(m);
                    }
                    else if (m == 3) {
                        $btnText = 'Monthly';
                        $('#timePicker').hide();
                        $dasboardreport.__initDateRangePicker(m);
                    }
                    else {
                        $('#timePicker').show();
                    }

                    /*var selVal = $("#chartsDropDown").select2("val");
                    $dasboardreport.__chartToggle(selVal);*/

                    $('#reportFor').html($btnText);

                },
                __drHourChange: function (c) {
                    var $type = $("#chartsDropDown").val();
                    if ($type == 1) $dasboardreport.__initPieChart();
                    else $dasboardreport.__initBarChart();
                },
                /********************************************************
                 * Initialize date range picker -
                 * @return array - contains start date and end date
                 * with call back handlers
                 *********************************************************/
                __initDateRangePicker: function (r) {
                    //var $jsonData = $dasboardreport.getBarChartData();
                    var current_time = new moment().format("HH");
                    //console.log(current_time);
                    $('#hourDropDown').select2("val",parseInt(current_time));
                    //if (current_time > 13)

                    /*else
                        $('#hourDropDown').select2("val",new moment().format("H"));*/

                    if (r == 0) {

                        var start = moment();
                        var end = moment();

                        $('#dateRangePicker').daterangepicker({
                            "singleDatePicker": true,
                            startDate: start,
                            endDate: end,
                            "applyClass": "dt-picker-btn",
                            "opens": "left"
                        }, cb);
                        cb(start, end, 0);

                    }
                    else {

                        var $dailyRanges = {
                            'Today': [moment(), moment()],
                            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        };
                        var $weeklyRanges = {
                            'This week': [moment().startOf('weeks'), moment().endOf('weeks')],
                            'Last week': [moment().subtract(1, 'weeks').startOf('weeks'), moment().subtract(1, 'weeks').endOf('weeks')]
                        };
                        var $monthlyRanges = {
                            'This Month': [moment().startOf('month'), moment().endOf('month')],
                            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                        };

                        var $selectedRange = $dailyRanges;
                        var start = moment();
                        var end = moment();
                        var $days = 1;
                        if (r == 2) {
                            $selectedRange = $weeklyRanges;
                            start = moment().startOf('weeks');
                            end = moment().endOf('weeks');
                            $days = 7;
                        }
                        else if (r == 3) {
                            $selectedRange = $monthlyRanges;
                            start = moment().startOf('month');
                            end = moment().endOf('month');
                             $days = 31;
                        }
                        else
                            $selectedRange = $dailyRanges;

                        $('#dateRangePicker').daterangepicker({
                            startDate: start,
                            endDate: end,
                            ranges: $selectedRange,
                            "applyClass": "dt-picker-btn",
                            "opens": "left",
                            "dateLimit": {
                                "days": $days
                            },
                        }, cb);
                        cb(start, end, r);
                    }

                    function cb(start, end, r) {

                        var $sDate = start.format('YYYY-MM-DD');
                        var $eDate = end.format('YYYY-MM-DD');

                        //$drStartDate = new Date($sDate);
                        //$drEndDate = new Date($eDate);
						var $slitsD = $sDate.split("-");
						var $sliteD = $eDate.split("-");

                        $drStartDate = new Date($slitsD[0],parseInt($slitsD[1])-1,$slitsD[2]);
                        $drEndDate = new Date($sliteD[0],parseInt($sliteD[1])-1,$sliteD[2]);


                        if ((typeof r != "undefined" && r == 0) || ($sDate == $eDate))
                            $('#dateRangePicker span').html(start.format('MMMM D, YYYY'));
                        else
                            $('#dateRangePicker span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));

                        //init array object
                        /*var $datePickerObj = new Object();
                         $datePickerObj.StartDate =  $sDate;
                         $datePickerObj.EndDate = $eDate;
                         $('#filterStartDate_'+$windowID).val($sDate);
                         $('#filterEndDate_'+$windowID).val($eDate);

                         loadActivityStatsByDateRange($datePickerObj);*/

                        var $chartType = $("#chartsDropDown").val();
                        //console.log("$chartType === "+$chartType);
                        if($chartType == 0) $dasboardreport.__initBarChart();
                        else if($chartType == 1) $dasboardreport.__initPieChart();
                    }

                    /*var $chartType = $("#chartsDropDown").val();
                    if($chartType == 0) $dasboardreport.__initBarChart();
                    else if($chartType == 1) $dasboardreport.__initPieChart();*/
                },
                __removeAlertItem: function (c) {
                    $(c).closest('tr').fadeOut().remove();
                },
                __initPortlet: function () {
                    $('.cs-wrapper').portlet({
                    });
                },
                __initPieChart: function () {
                    $drLiveMode = false;
                    if($pieChartTimer != null) clearInterval($pieChartTimer);
                    clearInterval($drBarTimer);

                    // write text plugin
                    Chart.pluginService.register({
                      afterUpdate: function (chart) {
                        if (chart.config.options.elements.center) {
                          var helpers = Chart.helpers;
                          var centerConfig = chart.config.options.elements.center;
                          var globalConfig = Chart.defaults.global;
                          var ctx = chart.chart.ctx;

                          var fontStyle = helpers.getValueOrDefault(centerConfig.fontStyle, globalConfig.defaultFontStyle);
                          var fontFamily = helpers.getValueOrDefault(centerConfig.fontFamily, globalConfig.defaultFontFamily);

                          if (centerConfig.fontSize)
                            var fontSize = centerConfig.fontSize;
                            // figure out the best font size, if one is not specified
                          else {
                            ctx.save();
                            var fontSize = helpers.getValueOrDefault(centerConfig.minFontSize, 1);
                            var maxFontSize = helpers.getValueOrDefault(centerConfig.maxFontSize, 256);
                            var maxText = helpers.getValueOrDefault(centerConfig.maxText, centerConfig.text);

                            do {
                              ctx.font = helpers.fontString(fontSize, fontStyle, fontFamily);
                              var textWidth = ctx.measureText(maxText).width;

                              // check if it fits, is within configured limits and that we are not simply toggling back and forth
                              if (textWidth < chart.innerRadius * 2 && fontSize < maxFontSize)
                                fontSize += 1;
                              else {
                                // reverse last step
                                fontSize -= 1;
                                break;
                              }
                            } while (true)
                            ctx.restore();
                          }

                          // save properties
                          chart.center = {
                            font: helpers.fontString(fontSize, fontStyle, fontFamily),
                            fillStyle: helpers.getValueOrDefault(centerConfig.fontColor, globalConfig.defaultFontColor)
                          };
                        }
                      },
                      afterDraw: function (chart) {
                        if (chart.center) {
                          var ctx = chart.chart.ctx;

                          var i,a,s;
                          var n = chart;
                          var total = n.data.TempAVG;

                          /*for(i=0,a=(n.data.datasets||[]).length;a>i;++i){
                            s = n.getDatasetMeta(i);
                            var x;

                            console.log();
                            for(x=0; x<s.data.length; x++){
                              if (!s.data[x].hidden)
                                  console.log(n.data.datasets[i]);
                                total += n.data.datasets[i].data[x];
                                $ambVal = n.data.datasets[i].data[0];
                            }

                            total = Math.floor((($ambVal/total) * 100));
                            total = total > 70 ? total : 70;
                            total = total < 75 ? total : 72;
                          }*/

                          ctx.save();
                          ctx.font = chart.center.font;
                          ctx.fillStyle = chart.center.fillStyle;
                          ctx.textAlign = 'center';
                          ctx.textBaseline = 'middle';
                          var centerX = (chart.chartArea.left + chart.chartArea.right) / 2;
                          var centerY = (chart.chartArea.top + chart.chartArea.bottom) / 2;
                          ctx.fillText(total, centerX, centerY);
                          ctx.restore();
                        }
                      },
                    });
                    $(".daily-chart-loader").removeClass("hide");
                    $dasboardreport.getPieChartData(function($jsonData){

                    // TEMPERATURE
                    var tempconfig = {
                        type: 'doughnut',
                        data: $jsonData.TEMP,

                        options: {
                            responsive: true,
                            cutoutPercentage:75,
                            rotation:8,
                            elements: {
                                center: {
                                  // the longest text that could appear in the center
                                  maxText: '100%',
                                  text: function(){
                                    var text="";
                                    this.data.datasets.forEach(function(dataset, i) {
                                        var meta = chartInstance.controller.getDatasetMeta(i);
                                        var total = 0;
                                        var $ambVal = 0;
                                        meta.data.forEach(function(bar, index) {
                                          var data = dataset.data[index];
                                          $ambVal = dataset.data[0];
                                          total = total+data;
                                        });
                                        text = Math.floor((($ambVal/total) * 100));
                                      });

                                      return text;
                                  },
                                  fontColor: '#000',
                                  //fontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
                                  fontStyle: 'bold',
                                  // if a fontSize is NOT specified, we will scale (within the below limits) maxText to take up the maximum space in the center
                                  // if these are not specified either, we default to 1 and 256
                                  minFontSize: 1,
                                  maxFontSize: 256,
                                }
                            },
                            tooltips: {
                                callbacks: {
                                  label: function(tooltipItem, data) {
                                          var dataset = data.datasets[tooltipItem.datasetIndex];
                                    var total = dataset.data.reduce(function(previousValue, currentValue, currentIndex, array) {
                                      return previousValue + currentValue;
                                    });
                                    var lb = dataset.dlabel[tooltipItem.index];
                                    var currentValue = dataset.data[tooltipItem.index];
                                    var precentage = Math.floor(((currentValue/total) * 100)+0.5);
                                    return lb +': '+precentage + "%";
                                  }
                                },
                                bodyFontSize : 20,
                                xPadding : 10,
                                yPadding : 10
                            },
                             legend: {
                                position: 'left',
                                labels: {
                                    fontSize: 19,
                                    boxWidth :20
                                }
                            },
                            "hover": {
                                "animationDuration": 0
                            },
                        },
                    }

                    //initialize pie chart
                    $("#divTempChart").html('<canvas id="tempChart" class="db-pie-chart" ></canvas>');
                    var tctx = document.getElementById("tempChart").getContext("2d");
                    window.myPie = new Chart(tctx, tempconfig);


                    //kWh Consumption
                    var kwhconfig = {
                        type: 'pie',
                        data: $jsonData.KWH,
                        options: {
                            responsive: true,
                            rotation:8,
                            tooltips: {
                                callbacks: {
                                  label: function(tooltipItem, data) {
                                          var dataset = data.datasets[tooltipItem.datasetIndex];
                                    var total = dataset.data.reduce(function(previousValue, currentValue, currentIndex, array) {
                                      return previousValue + currentValue;
                                    });
                                    var lb = dataset.dlabel[tooltipItem.index];
                                    var currentValue = dataset.data[tooltipItem.index];
                                    var precentage = Math.floor(((currentValue/total) * 100)+0.5);
                                     return lb +': '+precentage + "%";
                                  }
                                },
                                bodyFontSize : 20,
                                xPadding : 10,
                                yPadding : 10
                             },
                            legend: {
                                position: 'left',
                                labels: {
                                    fontSize: 19,
                                    boxWidth :20
                                }
                            }
                        }
                    };

                    //initialize pie chart
                    $("#divkwhChart").html('<canvas id="kwhConsumptionChart" class="db-pie-chart" ></canvas>');
                    var kctx = document.getElementById("kwhConsumptionChart").getContext("2d");
                    window.kwhPie = new Chart(kctx, kwhconfig);

                    //Occupancy
                    var occupconfig = {
                        type: 'pie',
                        data: $jsonData.OCCUPANCY,
                        options: {
                            responsive: true,
                            rotation:1.5,
                            tooltips: {
                                callbacks: {
                                  label: function(tooltipItem, data) {
                                          var dataset = data.datasets[tooltipItem.datasetIndex];
                                    var total = dataset.data.reduce(function(previousValue, currentValue, currentIndex, array) {
                                      return previousValue + currentValue;
                                    });
                                    var lb = dataset.dlabel[tooltipItem.index];
                                    var currentValue = dataset.data[tooltipItem.index];
                                    var precentage = Math.floor(((currentValue/total) * 100)+0.5);
                                     return lb +': '+precentage + "%";
                                  }
                                },
                                bodyFontSize : 20,
                                xPadding : 10,
                                yPadding : 10
                            },
                            legend: {
                                position: 'left',
                                labels: {
                                    fontSize: 19,
                                    boxWidth: 20
                                }
                            }
                        }
                    };

                    //initialize pie chart
                    $("#divoccupChart").html('<canvas id="occupChart" class="db-pie-chart" ></canvas>');
                    var octx = document.getElementById("occupChart").getContext("2d");
                    window.occupPie = new Chart(octx, occupconfig);

                    //Aout load pie chart data
                    var $current_time = new moment().format("H");
                    var $selTime = $("#hourDropDown").val();
                    var $selDateRange = $("#dateRangeDropDown").val();

                    //if slected hour and selected current time same
                    if($selDateRange == 0 && (($selTime==$current_time) || ($drLiveMode && ($selTime == ( $current_time - 1))))){

                        var $sDate = $drStartDate.getFullYear()+$drStartDate.getMonth()+$drStartDate.getDate();
                        var $eDate = $drEndDate.getFullYear()+$drEndDate.getMonth()+$drEndDate.getDate();
                        var $dt = new Date();
                        var $cuurentDate = $dt.getFullYear()+$dt.getMonth()+$dt.getDate();

                        //if both date are same
                        if($sDate == $cuurentDate && $eDate == $cuurentDate){
                            $pieChartTimer = setInterval(function () {
                                $drLiveMode = true;
                                var current_time = new moment().format("HH");
                                $('#hourDropDown').select2("val",parseInt(current_time));

                                //get pie chart data
                                   $dasboardreport.getPieChartData(function($jsonUpData){

                                //update pie chart
                                //TEMP
                                        var $is_same = tempconfig.data.labels.length == $jsonUpData.TEMP.labels.length && tempconfig.data.labels.every(function(element, index) {
                                            return element === $jsonUpData.TEMP.labels[index];
                                });
                                if($is_same){
                                            tempconfig.data.datasets[0].data = $jsonUpData.TEMP.datasets[0].data;
                                            tempconfig.data.labels = $jsonUpData.TEMP.labels;
                                }
                                else{
                                            tempconfig.data = $jsonUpData.TEMP;
                                }
                                window.myPie.update();

                                //KWH
                                        var $is_same = kwhconfig.data.labels.length == $jsonUpData.KWH.labels.length && kwhconfig.data.labels.every(function(element, index) {
                                            return element === $jsonUpData.KWH.labels[index];
                                });
                                if($is_same){
                                            kwhconfig.data.datasets[0].data = $jsonUpData.KWH.datasets[0].data;
                                            kwhconfig.data.labels = $jsonUpData.KWH.labels;
                                }
                                else{
                                            kwhconfig.data = $jsonUpData.KWH;
                                }
                                window.kwhPie.update();

                                //OCCUPANCY
                                        var $is_same = occupconfig.data.labels.length == $jsonUpData.OCCUPANCY.labels.length && occupconfig.data.labels.every(function(element, index) {
                                            return element === $jsonUpData.OCCUPANCY.labels[index];
                                });
                                if($is_same){
                                            occupconfig.data.datasets[0].data = $jsonUpData.OCCUPANCY.datasets[0].data;
                                            occupconfig.data.labels = $jsonUpData.OCCUPANCY.labels;
                                }
                                else{
                                            occupconfig.data = $jsonUpData.OCCUPANCY;
                                }
                                window.occupPie.update();
                                    });


                            }, __DashboardRunTime);
                        }
                    }
                    else{
                        $drLiveMode = false;
                        $pieChartTimer = null;
                    }
                    });
                    //console.log($jsonData);


                },
                __initBarChart: function () {
                    //var d = $dasboardreport.__generateRandomDigits(100, 5);

                    if($drBarTimer != null) clearInterval($drBarTimer);
                    clearInterval($pieChartTimer);
                    $(".daily-chart-loader").removeClass("hide");
                    $dasboardreport.getBarChartData(function($jsonData){
                    var barChartData = {
                        labels: $jsonData.label,
                        datasets: [
                            {
                                backgroundColor: $jsonData.backgroundColor,
                                borderColor:  $jsonData.borderColor,
                                    data:  $jsonData.dataPer,
                                dlabel:  $jsonData.data,
                            }
                        ]
                    };
                    var __barChartconfig = {
                        type: 'horizontalBar',
                        data: barChartData,
                        options: {
                            responsive: true,
                            legend: {
                                display: false
                            },
                            tooltips: {
                                "enabled": false
                            }, "hover": {
                                "animationDuration": 0
                            },
                            "animation": {
                                "onComplete": function () {
                                    var chartInstance = this.chart,
                                            ctx = chartInstance.ctx;
                                        ctx.font = "13px Arial";
                                        //ctx.fillStyle="#FFF";
                                    this.data.datasets.forEach(function (dataset, i) {
                                        var meta = chartInstance.controller.getDatasetMeta(i);
                                        meta.data.forEach(function (bar, index) {
                                            var data = RoundOneDecimal(dataset.dlabel[index],3);
                                                ctx.fillText(data, bar._model.x +2, bar._model.y + 5);
                                        });
                                    });
                                }
                            },
                            scales: {
                                xAxes: [{
                                        display: false,
                                        ticks: {
                                            fontSize: 15,
											max:120
                                        }
                                    }],
                                yAxes: [{
                                        stacked: true,
                                        gridLines: {
                                            display: false
                                        },
                                        barThickness: 18,
                                        barPercentage: 0.8,
                                        ticks: {
                                            fontSize: 15
                                        }
                                    }]
                            }

                        }
                    };

                        $("#div-report-chart-wrapper").html('<canvas id="dailyReportBar" style="width: 995px; height: 120px;" width="995" height="120"></canvas>');
                    var bctx = document.getElementById("dailyReportBar").getContext("2d");

                    window.__barChart = new Chart(bctx, __barChartconfig);


                    var $rangeID = $("#dateRangeDropDown").val();
                    var $timeRange = $("#hourDropDown").val();
                    var $startDate = $drStartDate;
                    var $endDate = $drEndDate;
                    var $current_time = new moment().format("H");
                    var $retryStatus =false;

                    if($rangeID == 0 && (($timeRange==$current_time) || ($cientLiveMode && ($timeRange == ( $current_time - 1))))) $retryStatus = true;
                    else if($rangeID == 1 &&
                            (new Date($endDate).getTime() == new Date($startDate).getTime()) &&
                            (new Date($endDate).getDate() == new Date().getDate()) && (new Date($endDate).getMonth() == new Date().getMonth()))  $retryStatus = true;
                    if($retryStatus){
                        $drBarTimer = setInterval(function () {
                            $drLiveMode = true;
                            var $rangeID = $("#dateRangeDropDown").select2("val");
                            var $timeRange = $("#hourDropDown").select2("val");
                            var current_time = new moment().format("HH");
                            if($rangeID == 0 && ($timeRange == ( $current_time - 1))) $('#hourDropDown').select2("val",parseInt(current_time));
                                $dasboardreport.getBarChartData(function($jsonUpData){
                            //console.log($clientBarOption);
                                    __barChartconfig.data.datasets[0].data = $jsonUpData.dataPer;
                                    __barChartconfig.data.datasets[0].dlabel = $jsonUpData.data;
                                    __barChartconfig.data.datasets[0].borderColor = $jsonUpData.borderColor;
                                    __barChartconfig.data.datasets[0].backgroundColor = $jsonUpData.backgroundColor;
                                    __barChartconfig.data.labels = $jsonUpData.label;
                            window.__barChart.update();
                                });
                        }, __DashboardRunTime);
                    }
                    else{
                        $drLiveMode = false;
                        $drBarTimer = null;
                    }
                    });


                },
                __generateRandomDigits: function (max, count) {
                    var r = [];
                    var currsum = 0;
                    for (i = 0; i < count - 1; i++) {
                        r[i] = Math.floor(Math.random() * (max - (count - i - 1) - currsum - 2) + 1); //   randombetween(1, max-(thecount-i-1)-currsum);
                        currsum += r[i];
                    }
                    r[count - 1] = max - currsum;
                    return r;
                },

                getPieChartData: function ($obj) {
                    var $dateRange = $("#dateRangeDropDown").val();
                    var $hourRange = $("#hourDropDown").val();
                    var $retResult = [];
                    var $emptyData = { labels: [ ],  datasets: [ {  data: [], backgroundColor: [ ], hoverBackgroundColor: [ ], dlabel: [] } ]  };
                    $retResult["KWH"] = $emptyData;
                    $retResult["TEMP"] = $emptyData;
                    $retResult["OCCUPANCY"] = $emptyData;
                    $retResult["TEMP_AVG"] = 0;

                    try {
                        var $kwhData = {};
                        // URL for the ajax call
                        var $url = 'ajax.php';

                        //JSON Data to server
                        var $postdata = {
                            'a': 'getPieChartdata',
                            'c': 'Dashboard',
                            dateRange: $dateRange,
                            hour: $hourRange,
                            sDate: $drStartDate.getFullYear() + "-" + ($drStartDate.getMonth() + 1) + "-" + $drStartDate.getDate(),
                            eDate: $drEndDate.getFullYear() + "-" + ($drEndDate.getMonth() + 1) + "-" + $drEndDate.getDate()
                        };
                        //console.log($postdata);

                        //Success call back function for ajax
                        var $successCB = function (data)
                        {
                            if (data.status) {

                                if(data.kwhConsumption[0] != null){
                                    $retResult["KWH"] = data.kwhConsumption[0];
                                    $("#divkwhChart").removeClass("hide");
                                    $(".divkwhChart_main").find(".nodata-block").addClass("hide");
                                    //console.log("data available");
                                }
                                else{
                                    $("#divkwhChart").addClass("hide");
                                    $(".divkwhChart_main").find(".nodata-block").removeClass("hide");
                                    //console.log("data not available");
                                }
                                if(data.temp[0] != null){
                                    $retResult["TEMP"] = data.temp[0];
                                    $("#divTempChart").removeClass("hide");
                                    $(".divTempChart_main").find(".nodata-block").addClass("hide");
                                }
                                else{
                                    $("#divTempChart").addClass("hide");
                                    $(".divTempChart_main").find(".nodata-block").removeClass("hide");
                                }
                                if(data.occupancy[0] != null){
                                    $retResult["OCCUPANCY"] = data.occupancy[0];
                                    $("#divoccupChart").removeClass("hide");
                                    $(".divoccupChart_main").find(".nodata-block").addClass("hide");
                                }
                                else{
                                    $("#divoccupChart").addClass("hide");
                                    $(".divoccupChart_main").find(".nodata-block").removeClass("hide");

                                }
                            }
                            $obj($retResult);
                        };

                        //Error call back function for ajax
                        var $errorCB = function () {
                        };

                        //Complete callback function for ajax
                        var $completeCB = function () {
                            $(".daily-chart-loader").addClass("hide");
                        };

                        //ajax call
                        asyncPostToServer($url, $postdata, $successCB, $errorCB, $completeCB, true);
                    }

                    //catch start
                    catch (ex) {
                        alert(ex.message);
                    }

                },
                getBarChartData: function ($obj) {
                    var $dateRange = $("#dateRangeDropDown").val();
                    var $hourRange = $("#hourDropDown").val();
                    var $retResult = [];

                    try {
                        var $kwhData = {};
                        // URL for the ajax call
                        var $url = 'ajax.php';

                        //JSON Data to server
                        var $postdata = {
                            'a': 'getDailyReportBarChartdata',
                            'c': 'Dashboard',
                            dateRange: $dateRange,
                            hour: $hourRange,
                            sDate: $drStartDate.getFullYear() + "-" + ($drStartDate.getMonth() + 1) + "-" + $drStartDate.getDate(),
                            eDate: $drEndDate.getFullYear() + "-" + ($drEndDate.getMonth() + 1) + "-" + $drEndDate.getDate()
                        };
                        //console.log($postdata);

                        //Success call back function for ajax
                        var $successCB = function (data)
                        {
                            if (data.status == 1) {
                                $retResult = data.result;
                            }
                            $obj($retResult);
                        };

                        //Error call back function for ajax
                        var $errorCB = function () {
                        };

                        //Complete callback function for ajax
                        var $completeCB = function () {
                            $(".daily-chart-loader").addClass("hide");
                        };

                        //ajax call
                        asyncPostToServer($url, $postdata, $successCB, $errorCB, $completeCB, true);
                    }

                    //catch start
                    catch (ex) {
                        alert(ex.message);
                    }

                }
            };

            var $crStartDate = new Date();
            var $crEndDate = new Date();
            var $cientLiveMode = true;
            var $clientChartTimer = null;
            var $clientBarOption = [];
            var $clientReport = {
				__globalAlertsFilterApplied : true,
                init : function(){
                    $clientReport.__initDateRangePicker(0);
                    //$clientReport.intBarchart();
                },
                __dateRangeToggle: function (m) {
                    //console.log(m);
                    if (m == 0) {
                        $('#chartTimePicker').show();
                        $clientReport.__initDateRangePicker(m,1);
                    }
                    else if (m == 1) {
                        //$btnText = 'Daily';
                        $('#chartTimePicker').hide();
                        $clientReport.__initDateRangePicker(m,1);
                    }
                    else if (m == 2) {
                        //$btnText = 'Weekly';
                        $('#chartTimePicker').hide();
                        $clientReport.__initDateRangePicker(m,1);
                    }
                    else if (m == 3) {
                        //$btnText = 'Monthly';
                        $('#chartTimePicker').hide();
                        $clientReport.__initDateRangePicker(m,1);
                    }
                    else {
                        $('#chartTimePicker').show();
                    }
                    //$('#dateRangeDropDown, #reportFor').html($btnText);
                    $clientReport.intBarchart();
                },
                /********************************************************
                 * Initialize date range picker -
                 * @return array - contains start date and end date
                 * with call back handlers
                 *********************************************************/
                __initDateRangePicker: function (r,$changeStatus) {
                    var current_time = new moment().format("HH");
                    $('#selTimeRange').select2("val",parseInt(current_time));

                    if (r == 0) {

                        var start = moment();
                        var end = moment();

                        $('#chartdateRangePicker').daterangepicker({
                            "singleDatePicker": true,
                            startDate: start,
                            endDate: end,
                            "applyClass": "dt-picker-btn",
                            "opens": "left"
                        }, cb);
                        cb(start, end, 0);

                    }
                    else {

                        var $dailyRanges = {
                            'Today': [moment(), moment()],
                            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        };
                        var $weeklyRanges = {
                            'This week': [moment().startOf('weeks'), moment().endOf('weeks')],
                            'Last week': [moment().subtract(1, 'weeks').startOf('weeks'), moment().subtract(1, 'weeks').endOf('weeks')]
                        };
                        var $monthlyRanges = {
                            'This Month': [moment().startOf('month'), moment().endOf('month')],
                            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                        };

                        var $selectedRange = $dailyRanges;
                        var start = moment();
                        var end = moment();
                        if (r == 2) {
                            $selectedRange = $weeklyRanges;
                            start = moment().startOf('weeks');
                            end = moment().endOf('weeks');
                        }
                        else if (r == 3) {
                            $selectedRange = $monthlyRanges;
                            start = moment().startOf('month');
                            end = moment().endOf('month');
                        }
                        else
                            $selectedRange = $dailyRanges;

                        $('#chartdateRangePicker').daterangepicker({
                            startDate: start,
                            endDate: end,
                            ranges: $selectedRange,
                            "applyClass": "dt-picker-btn",
                            "opens": "left",
                            format: 'DD/MM/YYYY'
                        }, cb);
                        cb(start, end, r,$changeStatus);
                    }

                    function cb(start, end, r,$changeStatus) {

                        var $sDate = start.format('YYYY-MM-DD');
                        var $eDate = end.format('YYYY-MM-DD');


						var $slitsD = $sDate.split("-");
						var $sliteD = $eDate.split("-");

                        $crStartDate = new Date($slitsD[0],parseInt($slitsD[1])-1,$slitsD[2]);
                        $crEndDate = new Date($sliteD[0],parseInt($sliteD[1])-1,$sliteD[2]);
                        //$crStartDate = new Date($sDate);
                        //$crEndDate = new Date($eDate);
                        //console.log($crEndDate);
                        if ((typeof r != "undefined" && r == 0) || ($sDate == $eDate))
                            $('#chartdateRangePicker span').html(start.format('MMMM D, YYYY'));
                        else
                            $('#chartdateRangePicker span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                        if(typeof $changeStatus == "undefined") $clientReport.intBarchart();
                    }
                },

                /*******************************************************
                * Get client report
                * @return array;
                ********************************************************/
                getMainReport : function($obj){
                    var startDate =new Date($crStartDate);
                    var $endDate =$crEndDate;
                    var $timelineSDate = startDate.getFullYear() + "-" + (startDate.getMonth() + 1) + "-" + (startDate.getDate());
                    var $timelineEDate = $endDate.getFullYear() + "-" + ($endDate.getMonth() + 1) + "-" + ($endDate.getDate());
                    var $selType = $("#selDeviceType").select2("val");
                    var $devID = $("#selDev").select2("val");
                    var $paramID = $("#selParam").select2("val");
                    var $paramText = $("#selParam").select2('data').text;
                    var $rangeID = $("#seldateRange").select2("val");
                    var $rangeText = $("#seldateRange").select2('data').text;
                    var $timeRange = $("#selTimeRange").select2("val");

                    var $tickCount = 0;
                    var $rtnVal = [];
                    var $lables = [];
                    var $lineData = [];
                    var $resultData = [];
                    var $barPercentage = 1;

                    $rtnVal["paramText"] = $paramText;

                    if($rangeID == 0){
                        $tickCount = 60;
                        $barPercentage = 0.50;
                    }
                    else if($rangeID == 1 && (new Date($endDate).getTime() == new Date(startDate).getTime())){
                        $tickCount = 24;
                        $barPercentage = 0.50;
                    }
                    else {
                        var $diff  = new Date($endDate - startDate);
                        var $days  = $diff/1000/60/60/24;
                        $tickCount = $days + 1;

                        if( $rangeID == 2 ) $barPercentage = 0.50;
                        if( $rangeID == 3 ) $barPercentage = 0.50;
                    }

                    //Init Timeline
                    //$TimeLine.getEventData(1,"timeLineContainer",0,0,0,$timelineSDate,$timelineEDate);

                    $rtnVal["barPercentage"] = $barPercentage;
                    //Init Bar chart
                    $("#clientReportTitle").html($paramText+" Per "+ $rangeText);

                    try {
                        // URL for the ajax call
                        var $url = 'ajax.php';

                        //JSON Data to server
                        var $postdata = {
                            'c'           : 'Dashboard',
                            'a'          : 'getMainReport',
                            'type'        : $selType,
                            'device'      : $devID,
                            'param'       : $paramID,
                            'range'       : $rangeID,
                            'timeRange'   : $timeRange,
                            'startDate'   : $timelineSDate,
                            'endDate'     : $timelineEDate
                        };
                        //console.log($postdata);

                        //Success call back function for ajax
                        var $successCB = function (data)
                        {
                            $resultData = data;
                        };

                        //Error call back function for ajax
                        var $errorCB = function () {
                        };

                        //Complete callback function for ajax
                        var $completeCB = function () {
                            //console.log("test.....");
                            for(var $i = 0; $i < $tickCount; $i++){
                                //console.log("count === "+$tickCount);
                                var $labelStr = "";
                                var $findTime = $i;

								//console.log("<<=================================================1");
								//console.log($crStartDate);
								//console.log($timelineSDate);
								//console.log(startDate);
                                if($rangeID == 0 && $timeRange > 12) $labelStr = ($timeRange - 12) +":"+ (("0" + $i) .slice(-2)) +" PM";
                                else if($rangeID == 0)  $labelStr =  $timeRange +":"+ (("0" + $i) .slice(-2)) +" AM";

                                else if($rangeID == 1 && (new Date($endDate).getTime() == new Date(startDate).getTime())){
                                    if ($i == 0)  $labelStr = '12 AM';
                                    else if($i < 12) $labelStr = ($i) + ' AM';

                                    if ($i == 12) $labelStr = '12 PM';
                                    else if ($i > 12) $labelStr = ($i - 12) + ' PM';
                                    $findTime = $i;
                                }

                                else{
                                    if($i == 0 ){
                                        $labelStr = $to = ("0" + (startDate.getMonth() + 1)).slice(-2) + "/" + ("0" + startDate.getDate()).slice(-2) + "/" + startDate.getFullYear();
										//console.log("here 0001");
									}
                                    else {
										var nd = startDate;
                                        startDate =new Date(nd.setDate(nd.getDate() + 1));
                                        //console.log($startDate);
                                        $labelStr = $to = ("0" + (startDate.getMonth() + 1)).slice(-2) + "/" + ("0" + startDate.getDate()).slice(-2) + "/" + startDate.getFullYear();
										//console.log("here 0002");
                                    }
                                    if($labelStr != "") $labelStr = moment($labelStr).format('MMM DD, YYYY');
                                    $findTime = moment($labelStr).format('MM/DD/YYYY');
                                }
								//console.log("=================================================2");
								//console.log($crStartDate);

                                $lables.push($labelStr);
                                //console.log($findTime);
                                var AVGVal = "";
                                //console.log($resultData.length);
                                for(var $j = 0; $j < $resultData.length; $j++){
                                    var $itemVal = $resultData[$j];//SUMOC

                                    if($rangeID == 0){
                                        var $tm = $itemVal["minute"];
                                        //console.log($findTime +"=="+ $tm );
                                        if($findTime == $tm ){
                                            AVGVal = $itemVal["AVGVal"];
                                        }
                                    }
                                    else if($rangeID == 1 && (new Date($endDate).getTime() == new Date(startDate).getTime())){
                                        var $tm = $itemVal["hour"];
                                        if($findTime == $tm ) AVGVal = $itemVal["AVGVal"];
                                    }
                                    else{
                                        var $tm = (("0" + $itemVal["month"]).slice(-2)) + "/" + (("0" + $itemVal["day"]).slice(-2)) + "/" + $itemVal["year"];
                                        //console.log($findTime +"=="+ $tm);
                                        if($findTime == $tm ) AVGVal = $itemVal["AVGVal"];
                                    }

                                }
								//console.log("=================================================3");
								//console.log($crStartDate);

                                if(AVGVal == "") AVGVal == 0;
                                $lineData.push(AVGVal);
                            }
                            $rtnVal["lables"] = $lables;
                            $rtnVal["lineData"] = $lineData;
                            $obj($rtnVal);
                            $(".main-chart-loader").addClass("hide");
                        };

                        //ajax call
                        asyncPostToServer($url, $postdata, $successCB, $errorCB, $completeCB, true);
                    }

                    //catch start
                    catch (ex) {
                        alert(ex.message);
                    }
                },
                /*******************************************************
                 * Initialize client bar chart
                 * @returns html
                 *******************************************************/
                intBarchart : function(){
                    var $startDate =$crStartDate;
                    var $endDate =$crEndDate;
                    var $timelineSDate = $startDate.getFullYear() + "-" + ($startDate.getMonth() + 1) + "-" + $startDate.getDate();
                    var $timelineEDate = $endDate.getFullYear() + "-" + ($endDate.getMonth() + 1) + "-" + $endDate.getDate();
                    var $selType = $("#selDeviceType").select2("val");
                    var $devID = $("#selDev").select2("val");
                    var $rangeID = $("#seldateRange").select2("val");
                    var $timeRange = $("#selTimeRange").select2("val");
                    $cientLiveMode = false;
                    if($clientChartTimer != null) clearInterval($clientChartTimer);
                    $(".main-chart-loader").removeClass("hide");
                    $clientReport.getMainReport(function($jsonData){

                    var $dataset = [
                        {
                            type: 'bar',
                            label: 'Max '+$jsonData["paramText"],
                            data: $jsonData["lineData"],
                            borderColor:'rgba(1, 137, 189,0.8)',
                            backgroundColor:'rgb(0, 162, 232)',
                        }
                    ];


                    $clientBarOption = {
                        type: 'bar',
                        data : {
                            labels :  $jsonData["lables"],
                            datasets : $dataset
                        },
                        options: {
                            scales: {
                                xAxes: [{
                                    barPercentage: $jsonData["barPercentage"],
                                    ticks: {
                                      callback: function(value) {
                                        if( $rangeID == 0 ){
                                            var lbl = value.split(" ");
                                            if( lbl.length > 0 ){
                                                var minPart = lbl[0].split(":");
                                                if( minPart.length > 0 ){
                                                    var mm = minPart[1];
                                                    if( mm % 5 == 0 ) return value;
                                                    else return '';
                                                }
                                            }
                                        }
                                        return value;
                                      },
                                  },
                                }],
								yAxes: [{

									  stacked: true,
									   ticks: {
										  min: 0
									  }

								}]
                            },
                            legend:{
                                display:false
                            },
                            tooltips: {
                                mode: 'label',
                                callbacks: {
                                    label: function(tooltipItem, data) {
                                        //console.log( tooltipItem );
                                        var dtst = data.datasets[tooltipItem.datasetIndex];
                                        //console.log(dataset);
                                        var lb = dtst.label;
										var ylb = RoundOneDecimal(tooltipItem.yLabel , 3).toString();
										x = ylb.split('.');
										x1 = x[0];
										x2 = x.length > 1 ? '.' + x[1] : '';
										var rgx = /(\d+)(\d{3})/;
										while (rgx.test(x1)) {
											x1 = x1.replace(rgx, '$1' + ',' + '$2');
										}
										ylb = x1 + x2;
                                        return lb+" : "+ylb;
                                    }
                                }
                            }
                        }
                    }
                    $("#div-chart-wrapper").html("");
                    var $h = "175px";
                    $("#div-chart-wrapper").html('<canvas id="canvas-chart-client" style="width: 995px; height: '+$h+'" width="995" height="'+$h+'"></canvas>');
                    var ctx = document.getElementById("canvas-chart-client").getContext("2d");

                    window.clientChart = new Chart(ctx, $clientBarOption);

                    var $current_time = new moment().format("H");

                    var $retryStatus = false;

                    if($rangeID == 0 && (($timeRange==$current_time) || ($cientLiveMode && ($timeRange == ( $current_time - 1))))) $retryStatus = true;
                    else if($rangeID == 1 &&
                            (new Date($endDate).getTime() == new Date($startDate).getTime()) &&
                            (new Date($endDate).getDate() == new Date().getDate()) && (new Date($endDate).getMonth() == new Date().getMonth()))  $retryStatus = true;
                    if($retryStatus){
                        $clientChartTimer = setInterval(function () {
                            $cientLiveMode = true;
                            var $rangeID = $("#seldateRange").select2("val");
                            var $timeRange = $("#selTimeRange").select2("val");
                            var current_time = new moment().format("HH");
                            if($rangeID == 0 && ($timeRange == ( $current_time - 1))) $('#hourDropDown').select2("val",parseInt(current_time));
                                $clientReport.getMainReport(function($jsonUpData){
                            //console.log($clientBarOption);
                                    $clientBarOption.data.datasets[0].data = $jsonUpData["lineData"];
                                    $clientBarOption.data.labels = $jsonUpData["lables"];
                            window.clientChart.update();
                                });
                        }, __DashboardRunTime);
                    }
                    else{
                        $cientLiveMode = false;
                        $clientChartTimer = null;
                    }
                    });

                    //console.log($jsonData);
                    //Init Timeline
                    $TimeLine.getEventData(1,"timeLineContainer");


                },
                __setDevice :function (){

                    var selVal = $("#selDeviceType").select2("val");

                    var $options = '';
                    $('#selDev').html('');
                    $('#selDev').select2('data', null);
                    if( selVal == 1 ){
                        $('#selDev').append('<option value="-1">All selected</option>');
                        $('#selDev').select2('val', "-1");
                        $("#selDev").prop("disabled", true);
                    }
                    else{
                        $options ="";
                        $('#selDev').select2('data', null);
                        $("#selDev").prop("disabled", false);
                        if( selVal == 2) $ClusterTree.getAllTagGroup("#selDev");
                        else if( selVal == 3) $ClusterTree.getAllCluster("#selDev");
                        $('#selDev').select2('val', "1");
                    }
                    $clientReport.changeDevlist();

                },
                changeDevlist : function(){
                    var selDevType = 0;
                    var selType = $("#selDeviceType").select2("val");
                    if(selType == 2 ) selDevType = $("#selDev option:selected").attr("data-type");
                    var $optionHtml = "";
                    var $elm = "#selParam";
                    try {

                        // URL for the ajax call
                        var $url = 'ajax.php';

                        //JSON Data to server
                        var $postdata = {
                            'a': 'getParamVal',
                            'c': 'LocationSettings',
                            deviceType: selType,
                            tagType: selDevType
                        };

                        //Success call back function for ajax
                        var $successCB = function (data)
                        {
                            if (data.status == 1) {
                                var $resultData = data.result;
                                var $len = $resultData.length;
                                //console.log($len);
                                if ($len > 0) {
                                    for (var i = 0; i < $len; i++) {
                                        $optionHtml += '<option value="' + $resultData[i]["ID"] + '">' + $resultData[i]["value"] + '</option>';
                                    }
                                }
                            }
                        };

                        //Error call back function for ajax
                        var $errorCB = function () {
                            $($elm).html($optionHtml);
                            $($elm).select2();
                        };

                        //Complete callback function for ajax
                        var $completeCB = function () {
                            $($elm).html($optionHtml);
                            $($elm).select2();
							$clientReport.intBarchart();
                        };

                        //ajax call
                        asyncPostToServer($url, $postdata, $successCB, $errorCB, $completeCB, true);
                    }

                    //catch start
                    catch (ex) {
                        alert(ex.message);
                    }
                    //$clientReport.getMainReport();
                },
                __generateRandEvents: function ($state,$type,$timelineSDate,$timelineEDate,$selType,$devID,$rangeID,$filterType) {

                    try {
                        if($state == 2 && !$clientReport.__globalAlertsFilterApplied) return false;
                        if($state == 1) $('.dashboardEvents').html("");
                        $(".timeline-loader").removeClass("hide");
                        // URL for the ajax call
                        var $url = 'ajax.php';

                        //JSON Data to server
                        var $postdata = {
                            'c'           : 'Dashboard',
                            'a'          : 'getTimeLineData',
                            sDate:$timelineSDate.getFullYear() + "-" + ($timelineSDate.getMonth() + 1) + "-" +($timelineSDate.getDate() + 0)+" "+$timelineSDate.getHours()+":"+$timelineSDate.getMinutes()+":"+$timelineSDate.getSeconds(),
                            eDate:$timelineEDate.getFullYear() + "-" + ($timelineEDate.getMonth() + 1) + "-" +($timelineEDate.getDate() + 0)+" "+$timelineEDate.getHours()+":"+$timelineEDate.getMinutes()+":"+$timelineEDate.getSeconds(),
                            selectType:$selType,
                            devID : $devID,
                            state : $state,
                            range :$rangeID,
                            type  : $type,
                            filterType : $filterType
                        };
                        //console.log($postdata);

                        //Success call back function for ajax
                        var $successCB = function (res)
                        {
                            var $data = res.data;
                            if($data.length > 0){
                                $('.dashboardEvents').find(".ev_nodata").remove();
                                for(var $i=0;$i < $data.length;$i++){

                                    var classTxt = ($i % 2 == 0) ? 'even' : 'odd';
                                    var $titleStr = "";
                                    var $alertClass = 'alerts-events db-alerts';
                                    var $t = $data[$i]["type"];
                                    var $content = $data[$i]["content"];
                                    if($t == 0){
                                        $titleStr = '<span class="label label-inverse">EVENT</span>';
                                    }
                                    else if($t == 1){
                                        $alertClass = 'alerts-policy db-alerts';
                                        $titleStr = '<span class="label label-savings">SAVINGS</span>';
                                    }
                                    else if($t == 2){
                                        $titleStr = '<span class="label label-warning">POLICY</span>';
                                    }

                                    var $rowHTML = '<tr class="' + classTxt + ' ' + $alertClass + ' removeable-setup">';
                                    $rowHTML += '<td class="text-right b-r b-dashed b-grey cursor">';
                                    $rowHTML += '<a class=" text-danger" data-placement="bottom" href="javascript:void(0);" onclick="$dasboardreport.__removeAlertItem(this);" data-toggle="tooltip"  data-original-title="Remove this">X</a>';
                                    $rowHTML += '</td>';
                                    $rowHTML += '<td class="text-left" style="width:95% !important">';
                                    $rowHTML += '<span class="small"> ' + $titleStr + ' ' + $content + '</span>';
                                    $rowHTML += '</td>';
                                    $rowHTML += '</tr>';
                                    $('.dashboardEvents').prepend($rowHTML);
                                }
                            }
                            else {

                                if($state == 2 && $('.dashboardEvents').length <= 1){
                                    $('.dashboardEvents').html("<tr class='ev_nodata'><td><h5 class='text-center text-l-help'>No data available</h5></td></tr>");
                                }
                            }
                        };

                        //Error call back function for ajax
                        var $errorCB = function () {
                        };

                        //Complete callback function for ajax
                        var $completeCB = function () {
                            $(".timeline-loader").addClass("hide");
                        };

                        //ajax call
                        asyncPostToServer($url, $postdata, $successCB, $errorCB, $completeCB, true);
                    }

                    //catch start
                    catch (ex) {
                        alert(ex.message);
                    }
                    /*var $count = 1;
                    var closeIcon = '<td class="text-right b-r b-dashed b-grey cursor text-danger">';
                    closeIcon += '<a class=" text-danger" data-placement="bottom" href="javascript:void(0);" onclick="$dasboardreport.__removeAlertItem(this);" data-toggle="tooltip"  data-original-title="Remove this">X</a>';
                    closeIcon += '</td>';
                    bc = setInterval(function () {
                        $count++;
                        var rd = Math.floor(Math.random() * 90 + 10);
                        var $time = Math.floor(Math.random() * 10) + 1;
                        var $min = Math.floor(Math.random() * 16) + 5;


                        var $txt1 = '<span class="label label-inverse">EVENT</span>';
                        var $tx2 = 'Occupancy Sensor (Tag 123) Triggered @ ' + $time + '.' + $min + ' PM';
                        if (rd > 50) {
                            $txt1 = '<span class="label label-warning">POLICY</span>';
                            $tx2 = 'Policy Tuesday was activated @ ' + $time + '.' + $min + ' AM';
                            $alertClass = 'alerts-policy db-alerts';
                        }
                        var $rowHTML = '';
                        $rowHTML += '<tr class="' + classTxt + ' ' + $alertClass + ' removeable-setup">';
                        $rowHTML += closeIcon;
                        $rowHTML += '<td class="text-left" style="width:95% !important">';
                        $rowHTML += '<span class="small"> ' + $txt1 + ' ' + $tx2 + '</span>';
                        $rowHTML += '</td>';
                        $rowHTML += '</tr>';
                        if ($dasboardreport.__globalAlertsFilterApplied)
                            $('.dashboardEvents').prepend($rowHTML).hide().fadeIn('slow');
                    }, 5000);*/
                }

            }



            </script>
	</div>

              <?php
                include_once("tpl/dashboard-bottom.tpl.php");
               ?>
