<?php
  include_once("config.php");
  if(isset($_SESSION) && sizeof($_SESSION) == 0)
  {
		header('Location: logout.php');
  }
  else
  {
		$modelJson = json_encode($GLOBALS["MODELNO"]);
		$fixtureTypeArr = array();
		$fixtureTypeArr[FIXTURE_TYPE_NODE]["Namae"]="Node";$fixtureTypeArr[FIXTURE_TYPE_NODE]["ShortName"]="ND";
		$fixtureTypeArr[FIXTURE_TYPE_DRIVERA]["Namae"]="Driver A";$fixtureTypeArr[FIXTURE_TYPE_DRIVERA]["ShortName"]="DR_A";
		$fixtureTypeArr[FIXTURE_TYPE_DRIVERB]["Namae"]="Driver B";$fixtureTypeArr[FIXTURE_TYPE_DRIVERB]["ShortName"]="DR_B";
		$fixtureTypeArr[FIXTURE_TYPE_WALLSWITCH]["Namae"]="Wall switch";$fixtureTypeArr[FIXTURE_TYPE_WALLSWITCH]["ShortName"]="WS";
		$fixtureTypeArr[FIXTURE_TYPE_OCCUPANCYSENSOR]["Namae"]="Occupency sensor";$fixtureTypeArr[FIXTURE_TYPE_OCCUPANCYSENSOR]["ShortName"]="OS";
		$con_fxType = json_encode($fixtureTypeArr);// json_encode($GLOBALS["FX_TYPE_ARR"]);
		$currentPageName = substr($_SERVER["SCRIPT_NAME"],strrpos($_SERVER["SCRIPT_NAME"],"/")+1);
  }
?>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
        <meta http-equiv="content-type" content="text/html; charset=UTF8">

        <meta charset="utf-8">
        <title>POE Cloud-inspeXtor </title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-touch-fullscreen" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="default">
        <meta content="J. Joseph Inc." name="author" />

        <link href="css/site.css" rel="stylesheet" type="text/css"/>
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
        <!-- SINGLE CSS FILE: STRICTLY DO NOT INCLUDE ANY FILES HERE -->
        <link href="css/__minified.css" rel="stylesheet" type="text/css"/>
        <link href="css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/dataTables.foundation.min.css" rel="stylesheet" type="text/css"/>
        <link href="js/jquery-dynatree/skin/ui.dynatree.css" rel="stylesheet" type="text/css"/>
        <link href="css/shan1.css" rel="stylesheet" type="text/css"/>
        <link href="js/time-line/timeline.css" rel="stylesheet" type="text/css"/>

        <script src="js/general-function.js" type="text/javascript"></script>

        <?php
                if( !isset($_SESSION["LOGGED_IN_USERNAME"])  ){
                        header("Location:index.php");
                }

                $loggedInUserId     = $_SESSION['LOGGED_USER_ID'];
                $loggedUserRoleId   = $_SESSION['LOGGED_USER_ROLE_ID'];
                $loggedInUsername   = $_SESSION["LOGGED_IN_USERNAME"];
                $loggedInUserRoles  = implode(',',$_SESSION['LOGGED_USER_ROLES']);

        ?>
        <style>


			  @media (max-width: @screen-xs-min) {
  .modal-xs { width: @modal-sm; }
}
            .cs-wrapper{
                box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.26);
                border-radius: 0 !important;
                position: relative;
                -webkit-transition: all .2s linear;
                min-height: 150px;
                background:white;
                margin:0 auto;
                width:99%;
                margin:10px;
                padding:10px;
            }

           .panel-maximized{
             left: 60px !important;
             top: 54px !important;
             right: -10px !important;
            }

            .cs-wrapper.panel-maximized{
                width: auto !important;
            }

            .cs-wrapper .panel-title{
                text-transform: none !important;
                font-size:22px !important;
                font-weight: 300 !important;
                color:#8e8e8e !important;

            }
            .cs-wrapper .panel-heading {
                padding-top:10px;
                border-bottom: 1px solid #efefef;
            }
            #timeDropDown, .dashevents {
                max-height:200px;
                overflow:auto;
            }
            .dtRangePicker{
                background: #fff;
                cursor: pointer;
                border: 1px solid #ececec;
                border-radius:3px;
            }

            .dtRangePicker:hover{
                background: #fbfbfb;
                border: 1px solid #dad9d9;
            }
            .dt-picker-btn{
                background: #4ca1af;
                color:#fff;
            }
            .dt-picker-btn:hover{
                background: #4cb1c1;
                color:#fff;
            }
            .daterangepicker .ranges li{
                color:#4ca1af;
            }
            .daterangepicker .ranges li:hover{
                background:#4ca1af;
            }
            .input-mini.active {
                background-color: #fff !important;
            }

            .nomt{
                margin-top:0px;
            }
            .nbb{
                border-bottom: none !important;
            }
            .removeable-setup{
                moz-transition: -moz-transform 0.5s, opacity 0.2s;
                -o-transition: -o-transform 0.5s, opacity 0.2s;
                -webkit-transition: -webkit-transform 0.5s, opacity 0.2s;
                transition: transform 0.5s, opacity 0.2s;

                opacity: 1;
            }
            .removed-item-alerts {

                opacity: 0;
            }

            .ddl-datesel{
                border-bottom: 1px solid #dadada;
                width: 100%;
            }
            .db-pie-chart{
            width:300px !important;
            height:150px !important;
        }
        .report-chart-title{
            border-bottom: 1px solid #e6e5e5;
            width:75%;
        }
        .hr-chart-leg-box{
            width:15px;
            height:15px;
            display: inline-block;
            margin-right: 5px;
        }
        .fw-600{
            font-weight: 600;
        }
        .header,.sidebar-header {
          background-color: #34495e !important;
          height: 65px !important;
        }
        .page-sidebar .sidebar-menu .menu-items > li {

            border-bottom: 1px solid #434852;
        }
        .page-sidebar .sidebar-menu .menu-items > li a{
            color:white !important;
            margin-top:5px;
        }

        #btnClearAlertFilter{
            color: #626262;
            opacity: .4;
        }
        </style>
    </head>
    <body class="fixed-header   menu-behind">
      <div class="modal fade stick-up" id="device-sel-container" role="dialog" data-backdrop="static" aria-hidden="false" style="background-color: rgba(0, 0, 0, 0.51);">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header clearfix text-left">
                  <!--button type="button" class="close" data-dismiss="modal" style="font-size: 32px;">×</button-->
                  <h5 class="bold fs-18 m-t-0">Select an inspeXtor</h5>
              </div>
              <div class="modal-body p-t-5">
                  <div class="row p-l-15 p-r-15 clear-fix" id="device-sel-area">

                  </div>
                  <div class="">
                      <a href='instanceManagement.php' class='add-dev-a'>Manage InspeXtor Instances</a>
                  </div>
              </div>
            </div>

          </div>

      </div>
      
      <nav class="page-sidebar" data-pages="sidebar" >

        <!-- END SIDEBAR MENU TOP TRAY CONTENT-->
        <!-- BEGIN SIDEBAR MENU HEADER-->
        <div class="sidebar-header" style="font-size: 28px; font-weight: 500; font-family: arial;">
          inspeXtor
        </div>
        <!-- END SIDEBAR MENU HEADER-->
        <!-- START SIDEBAR MENU -->
        <div class="sidebar-menu">
          <!-- BEGIN SIDEBAR MENU ITEMS-->
          <ul class="menu-items">
            <li class="m-t-20 ">
				  <a href="launchpad.php" class="detailed">
					<span class="title">Dashboard</span>
				  </a>
				  <span class="icon-thumbnail bg-default"><i class="fa fa-home"></i></span>
            </li>
			<li class="" id="liMDevice">
				  <a href="ws.php" class="detailed">
					<span class="title">Control Lights and Shades</span>
				  </a>
				  <span class="icon-thumbnail "><i class="fa fa-mobile"></i></span>
            </li>
			
			<li class="" id="liMDevice">
				  <a href="cluster.php" class="detailed">
					<span class="title">Manage Clusters</span>
				  </a>
				  <span class="icon-thumbnail "><i class="fa fa-cogs"></i></span>
            </li>
            
			<li class="" id="liMDevice">
				  <a href="locationEditor.php" class="detailed">
					<span class="title">Manage Floor Plans</span>
				  </a>
				  <span class="icon-thumbnail "><i class="fa fa-map-marker"></i></span>
            </li>
			
			<?php if( $loggedUserRoleId != ASSISTANT_ROLE ) { ?>
				<li class="" id="liMDevice">
				  <a href="graph.php" class="detailed">
					<span class="title">Power Display</span>
				  </a>

				  <span class="icon-thumbnail "><i class="fa fa-plug"></i></span>
				</li>
			<?php  }  ?>
			

			  

			<li class="" id="liMDevice">
				  <a href="policy.php" class="detailed">
					<span class="title">Manage Policies</span>
				  </a>

				  <span class="icon-thumbnail "><i class="fa fa-plus"></i></span>
            </li>
			<li class="" id="liMDevice">
				  <a href="ATpolicy.php" class="detailed">
					<span class="title">Autotune Policy</span>
				  </a>
				  <span class="icon-thumbnail "><i class="fa fa-calendar"></i></span>
            </li>

			<li class="" id="liMDevice">
				  <a href="mergePolicy.php" class="detailed">
					<span class="title">Merge Policy</span>
				  </a>
				  <span class="icon-thumbnail "><i class="fa fa-link"></i></span>
            </li>

			<li class="" id="liMDevice">
				  <a href="sensor.php" class="detailed">
					<span class="title">Hardware Setting</span>
				  </a>
				  <span class="icon-thumbnail "><i class="fa fa-cogs"></i></span>
            </li>
			
			<li class="" id="liMDevice">
				  <a href="ATpolicy.php" class="detailed">
					<span class="title">Circadian rhythm schedule</span>
				  </a>
				  <span class="icon-thumbnail "><i class="fa fa-sun-o"></i></span>
            </li>

			

			<li class="" id="liMDevice">
				  <a href="alerts.php" class="detailed">
					<span class="title">Alerts</span>
				  </a>

				  <span class="icon-thumbnail "><i class="fa fa-bell-o"></i></span>
            </li>

			<!--<li class="" id="liMDevice">
				  <a href="user.php" class="detailed">
					<span class="title">User Control</span>
				  </a>
				  <span class="icon-thumbnail "><i class="fa fa-users"></i></span>
            </li>-->
			<?php if($loggedUserRoleId == SUPER_ADMIN_ROLE ||  $loggedUserRoleId == ADMIN_ROLE) { ?>
				<li class="" id="liMDevice">
					  <a href="cloudUser.php" class="detailed">
						<span class="title">Cloud User Control</span>
					  </a>
					  <span class="icon-thumbnail "><i class="fa fa-users"></i></span>
				</li>
			<?php  }  ?>
			<?php if( $loggedUserRoleId == SUPER_ADMIN_ROLE ) { ?>
				<li class="" id="liMDevice">
					  <a href="client.php" class="detailed">
						<span class="title">Client Control</span>
					  </a>
					  <span class="icon-thumbnail "><i class="fa fa-users"></i></span>
				</li>
			<?php  }  ?>

				<li class="" id="liMDevice">
					  <a href="maintenance.php" class="detailed">
						<span class="title">Support</span>
					  </a>
					  <span class="icon-thumbnail "><i class="fa fa-life-ring"></i></span>
				</li>
				<li class="" id="liMDevice">
					  <a href="facilities.php" class="detailed">
						<span class="title">Facilities</span>
					  </a>
					  <span class="icon-thumbnail "><i class="fa fa-life-ring"></i></span>
				</li>


			<?php if( $loggedUserRoleId == ADMIN_ROLE || $loggedUserRoleId == ASSISTANT_ROLE || $loggedUserRoleId == SUPER_ADMIN_ROLE) { ?>
					<li class="" id="liMDevice">
							  <a href="terminal.php" ><span class="title">Terminal</span></a>
							  <span class="icon-thumbnail"><i class="fa fa-terminal"></i></span>
					</li>
					<li class="" id="liMDevice">
					  <a href="download.php" ><span class="title">Download Docs</span></a>
					  <span class="icon-thumbnail"><i class="fa fa-download"></i></span>
					</li>
			<?php  }  ?>
			
					<li class="" id="liMDevice">
					  <a href="instanceManagement.php" ><span class="title">Instance Manager</span></a>
					  <span class="icon-thumbnail"><i class="fa fa-cogs"></i></span>
					</li>
			
          </ul>
          <div class="clearfix"></div>
        </div>
        <!-- END SIDEBAR MENU -->
      </nav>
      <!-- END SIDEBAR -->
      <!-- END SIDEBPANEL-->
      <div class="page-container">
        <!-- START HEADER -->
        <div class="header ">
          <!-- START MOBILE CONTROLS -->
          <!-- LEFT SIDE -->
          <div class="pull-left full-height visible-sm visible-xs">
            <!-- START ACTION BAR -->
            <div class="sm-action-bar">
              <a href="#" class="btn-link toggle-sidebar" data-toggle="sidebar">
                <span class="icon-set menu-hambuger"></span>
              </a>
            </div>
            <!-- END ACTION BAR -->
          </div>
          <!-- RIGHT SIDE -->
          <div class="pull-right full-height visible-sm visible-xs">
            <!-- START ACTION BAR -->
            <div class="sm-action-bar">
              <a href="#" class="btn-link" data-toggle="quickview" data-toggle-element="#quickview">
                <span class="icon-set menu-hambuger-plus"></span>
              </a>
            </div>
            <!-- END ACTION BAR -->
          </div>
          <!-- END MOBILE CONTROLS -->
          <div class=" pull-left sm-table">
            <div class="header-inner">
              <div class="brand inline" style="font-size: 28px; font-weight: 500; font-family: arial; margin-left: 50px; color: white;">
              <a href="javascript:void(0);"  onclick="openSideBar();">
                <i class="fa fa-bars" style="color: white; font-size: 21px; opacity: 0.8; margin-right: 10px;"></i>
              </a>
                inspeXtor
              </div>
              <!-- START NOTIFICATION LIST -->
              <ul class="notification-list no-margin hidden-sm hidden-xs b-grey b-l b-r no-style p-l-30 p-r-20">


              </ul>
            </div>
          </div>
            <?php
                $devClass = "";
                $devActiveName = "Select an Instance";


                if( isset($_SESSION['INX_CLUD_ID']) && !empty($_SESSION["INX_CLUD_ID"]) ){
                    $devClass = "dev-selected";
                    $devActiveName = $_SESSION["INX_CLUD_DEV_NAME"];
                }
            ?>


          <div class=" pull-left sm-table dev-sel-main <?php echo $devClass; ?>" data-placement="bottom" data-toggle="tooltip" title=""  data-original-title="Select an instance" onclick="_deviceSel.open();">
              <div class="dev-sel-main-content"><?php echo $devActiveName; ?> <i class="fa fa-angle-right" aria-hidden="true"></i></div>

          </div>

          <div class=" pull-right">
            <div class="header-inner">
              <a href="#" class="btn-link icon-set menu-hambuger-plus m-l-20 sm-no-margin hidden-sm hidden-xs" data-toggle="quickview" data-toggle-element="#quickview"></a>
            </div>
          </div>
          <div class=" pull-right">
          <!-- START User Info-->
          <div class="dropdown visible-lg visible-md m-t-5 cursor">
			  
            <?php if($loggedUserRoleId) { ?>
                <div class="thumbnail-wrapper d39 circular inline m-t-5 cursor-pointer"  data-toggle="dropdown"  data-placement="left" data-toggle="tooltip" data-trigger="hover" title="<?php echo $loggedInUsername;?> (<?php echo $loggedInUserRoles;?>)" >
                  <?php
                     $date = new DateTime();

                  	 $imgPath = "img/profiles/profile-".$loggedInUserId.".png";
                        if(!file_exists($imgPath)) $imgPath = "img/profiles/no-image.jpg";
                  ?>
                  <img src="<?php echo $imgPath;?>?<?php echo $date->getTimestamp();?>" alt="" data-src="<?php echo $imgPath;?>"  width="32" height="32">
                </div>
            <?php } ?>

            <div class="dropdown-menu notification-toggle drop-profile-info" role="menu" aria-labelledby="profile-info" style="left: -153px; top:55px;z-index:2000!important;" id="ddmenu">
                <div class="notification-panel" style="width:200px;">
                    <div class="notification-body">
                        <div class="notification-item  clearfix">
                          <div class="heading">
                            <a href="javascript:void(0);" onclick ="Javascript:window.location='profile.php';" class="">
                              <span class="span-text fs-16 m-b-0 bold">My Profile</span>
                            </a>
                          </div>
                        </div>
                        <div class="notification-item  clearfix">
                          <div class="heading">
                            <a href="logout.php" class="">
                              <span class="span-text fs-16 m-b-0 bold">Log Out</span>
                            </a>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
          </div>
          <!-- END User Info-->
          </div>

        <div class="pull-right">
            <div class="p-t-20 p-r-15">
                <a href="javascript:void(0)" id="__notify" class="" data-placement="left" data-toggle="tooltip" data-trigger="hover" title="alerts">
                    <div class="notification-bubble alert-bubbele hide">
                        <span>0</span>
                    </div>
                    <i class="fa fa-bell-o fa-lg text-white"></i>

                </a>
            </div>
        </div>
        </div>

        <!-- END HEADER -->
        <!-- START PAGE CONTENT WRAPPER -->
        <div class="page-content-wrapper hide">
          <!-- START PAGE CONTENT -->
          <div class="content">
            <div class="container-fluid p-l-25 p-t-25 p-r-25 p-b-0 ">
