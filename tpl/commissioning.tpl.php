<div class="row" style="margin-top: 75px;">
    <div class="col-xl-12 col-lg-12 col-md-12 com-sm-12 col-xs-12 text-center">    
         <button class="btn btn-success btn-cons padding-0">
            <input id="fileupload" type="file" name="files[]" class="inputfile inputfile-1">
            <label for="fileupload">Import pull schedule.</label>
        </button>
				<button class="btn btn-success btn-cons" onclick="return __SetPower.removeConfirm();">
            Set Switch Power 
        </button>
        <button class="btn btn-danger btn-cons" onclick="return __ClearTag.removeConfirm();">
            Clear tag
        </button>

    </div>    
</div>


<!-- MODAL CONFIRMATION ALERT -->
<div class="modal fade slide-up disable-scroll" id="modalClearCnfirmation" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog" style="width: 300px;">
        <div class="modal-content-wrapper">
            <div class="modal-content mdal-custom">
                <div class="modal-body p-b-5">
                    <p class="bold fs-20 p-t-20">
                        Do you want to clear all tag(s)?
                    </p>
                </div>
                <div class="modal-footer text-right">
                    <button type="button" class="btn btn-danger pull-right" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-success pull-right m-r-10" onclick="__ClearTag.clear_tag();">OK</button>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
</div>
<!-- END CONFIRMATION ALERT -->
<!-- MODAL CONFIRMATION ALERT -->
<div class="modal fade slide-up disable-scroll" id="modalClearpowerCnfirmation" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog" style="width: 300px;">
        <div class="modal-content-wrapper">
            <div class="modal-content mdal-custom">
                <div class="modal-body p-b-5">
                    <p class="bold fs-20 p-t-20">
                        Do you want to Set power for Switch(s)?
                    </p>
                </div>
                <div class="modal-footer text-right">
                    <button type="button" class="btn btn-danger pull-right" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-success pull-right m-r-10" onclick="__SetPower.Set_Power();">OK</button>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
</div>
<!-- END CONFIRMATION ALERT -->


