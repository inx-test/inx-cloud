<!-- START POLICY CREATION CONTAINER -->
<!--style> 
.irs-bar{
	background-color:#FAA644;
}
.irs-single{
	background-color:#FAA644;
}
</style-->
<form class="form-group">
<div class="row m-t-10">
    <div class="col-md-8">
        <div class="form-group required " id="divName">
            <label>Name</label>
            <input type="text" class="form-control" id="txtName" required="" placeholder="Enter Auto tune policy name"  onkeyup="removeError('divName');">
            <span class="text-danger sp_error hide"></span>
        </div>
    </div>
    
</div>

<div class="row">
    <div class="col-md-12">
        <div class="form-group required" id="divDim">
            <label class="">Select Color</label>
            <input type="text" id="dim_slider" data-type="ionSlide" name="dim_slider" value="0" onchange="$range.__color();">
            <span class="text-danger sp_error hide"></span>
        </div>
    </div>
</div>                                   

<div class="" id="dateTimeContainer">

    <div class="">  
        <div class=" col-md-6 " id="startDateContainer">
            
            <label class="bold fs-11 upper">Start Time</label>
            <div class="form-group input-group required" id="divStartTime" style="overflow:auto;">
                <input type="text" class="form-control" id="txtSatartTime" placeholder="Pick a time"  onfocus="return removeError('divStartTime');">
                <span class="input-group-addon">
                    <i class="fa fa-clock-o"></i>
                </span>
                <span class="text-danger sp_error hide"></span>
            </div>
        </div>

        <div class=" col-md-6 div-strat-time p-l-0">
			<label class="bold fs-11 upper">End Time</label>
			<div class="form-group input-group required" id="divEndTime"  style="overflow:auto;">
				<input type="text" class="form-control" id="txtEndTime" placeholder="Pick a time" onfocus="return removeError('divEndTime');">
				<span class="input-group-addon">
					<i class="fa fa-clock-o"></i>
				</span>
				<span class="text-danger sp_error hide"></span>
			</div>
        </div>
    </div>

</div>
<div class="row" id="repeatContainer">
    <label class="full-width clearfix m-b-10 bold fs-11 upper">Days of week</label>
    <div class="checkbox check-success pull-left ">
        <input type="checkbox" class="checkAll" onchange="return $ATPolicy.changeRepeatStatus(this);" value="ALL" id="checkSelectAll">
        <label for="checkSelectAll">Select All</label>
    </div>
    <div class="checkbox check-success pull-left ">
        <input type="checkbox" class="checkDay" onchange="return $ATPolicy.changeRepeatStatus(this);" value="1" id="checkMon">
        <label for="checkMon">Mon</label>
    </div>
    <div class="checkbox check-success pull-left ">
        <input type="checkbox" class="checkDay" onchange="return $ATPolicy.changeRepeatStatus(this);" value="2" id="checkTue">
        <label for="checkTue">Tue</label>
    </div>
    <div class="checkbox check-success pull-left ">
        <input type="checkbox" class="checkDay" onchange="return $ATPolicy.changeRepeatStatus(this);" value="3" id="checkWed">
        <label for="checkWed">Wed</label>
    </div>
    <div class="checkbox check-success  pull-left">
        <input type="checkbox" class="checkDay" onchange="return $ATPolicy.changeRepeatStatus(this);" value="4" id="checkThu">
        <label for="checkThu">Thu</label>
    </div>
    <div class="checkbox check-success pull-left ">
        <input type="checkbox" class="checkDay" onchange="return $ATPolicy.changeRepeatStatus(this);" value="5" id="checkFri">
        <label for="checkFri">Fri</label>
    </div>
    <div class="checkbox check-success pull-left ">
        <input type="checkbox" class="checkDay" onchange="return $ATPolicy.changeRepeatStatus(this);" value="6" id="checkSat">
        <label for="checkSat">Sat</label>
    </div>

    <div class="checkbox check-success pull-left ">
        <input type="checkbox" class="checkDay" onchange="return $ATPolicy.changeRepeatStatus(this);" value="7" id="checkSun">
        <label for="checkSun">Sun</label>
    </div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="form-group required" id="divCluster">
			<label class="">Cluster</label>
			<br>
				<select data-init-plugin="select2" id="selDev" style="width: 200px;"  >
					<option value="-1">All selected</option>
				</select>
			
			<span class="text-danger sp_error hide"></span>
		</div>
	</div>
</div>  

<input  type="hidden" id="hdnPolicyID" name="hdnPolicyID" value="">
<input  type="hidden" id="hdnPEditStat" name="hdnPEditStat" value="0">
<div class="row text-center">
    <div class="col-md-12 m-b-15 m-t-15">
        <button type="button" class="btn btn-cons btn-success btn-sm" onclick="return $ATPolicy.save();">Save-Send</button>
        <button type="button" class="btn btn-cons btn-danger btn-sm" id="btnPolicyDel" onclick='return $ATPolicy.removePolicy();'>Remove</button>
        <button type="button" class="btn btn-cons btn-default btn-sm " onclick='return $ATPolicy.clear();'>Cancel</button>
    </div>
</div>
<div class="row hide" id="removeErrorMSG">
    <div class="col-md-12 text-center">
        <span class="text-danger bold"><i class="fa fa-exclamation-triangle fs-16 p-r-5" aria-hidden="true"></i>You can not remove this since policy merged.</span>
    </div>
</div>
</form>


<!-- END POLICY CREATION CONTAINER -->