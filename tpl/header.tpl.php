<?php 
  include_once("config.php");
  $modelJson = json_encode($GLOBALS["MODELNO"]);
  $fixtureTypeArr = array();
    $fixtureTypeArr[FIXTURE_TYPE_NODE]["Namae"]="Node";$fixtureTypeArr[FIXTURE_TYPE_NODE]["ShortName"]="ND";
    $fixtureTypeArr[FIXTURE_TYPE_DRIVERA]["Namae"]="Driver A";$fixtureTypeArr[FIXTURE_TYPE_DRIVERA]["ShortName"]="DR_A";
    $fixtureTypeArr[FIXTURE_TYPE_DRIVERB]["Namae"]="Driver B";$fixtureTypeArr[FIXTURE_TYPE_DRIVERB]["ShortName"]="DR_B";
    $fixtureTypeArr[FIXTURE_TYPE_WALLSWITCH]["Namae"]="Wall switch";$fixtureTypeArr[FIXTURE_TYPE_WALLSWITCH]["ShortName"]="WS";
    $fixtureTypeArr[FIXTURE_TYPE_OCCUPANCYSENSOR]["Namae"]="Occupency sensor";$fixtureTypeArr[FIXTURE_TYPE_OCCUPANCYSENSOR]["ShortName"]="OS";
  $con_fxType = json_encode($fixtureTypeArr);// json_encode($GLOBALS["FX_TYPE_ARR"]);
  $currentPageName = substr($_SERVER["SCRIPT_NAME"],strrpos($_SERVER["SCRIPT_NAME"],"/")+1);
?>

<!DOCTYPE html>
<html>  
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta http-equiv="content-type" content="text/html; charset=UTF8">
    <meta charset="utf-8">
    <title>MHT- Lighting Director </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="J. Joseph Inc." name="author" />

        <link href="css/site.css" rel="stylesheet" type="text/css"/>
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
	<!-- SINGLE CSS FILE: STRICTLY DO NOT INCLUDE ANY FILES HERE -->
	<link href="css/__minified.css" rel="stylesheet" type="text/css"/>
        <link href="css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/dataTables.foundation.min.css" rel="stylesheet" type="text/css"/>
        <link href="js/jquery-dynatree/skin/ui.dynatree.css" rel="stylesheet" type="text/css"/>
        <script src="js/general-function.js" type="text/javascript"></script>
        

    <?php 
            if( !isset($_SESSION["LOGGED_IN_USERNAME"])  ){
                    header("Location:index.php");
            }
            
            $loggedInUserId     = $_SESSION['LOGGED_USER_ID'];
            $loggedUserRoleId   = $_SESSION['LOGGED_USER_ROLE_ID'];
            $loggedInUsername   = $_SESSION["LOGGED_IN_USERNAME"];
            $loggedInUserRoles  = implode(',',$_SESSION['LOGGED_USER_ROLES']);
            
    ?>
  </head>
  <body class="fixed-header   ">
<!--header-->
	 <div class="page-container">
				  	      
        <!--ul id="main-nav">
            <li class="current"><a href="#">Home</a></li>
            <li class="portfolio"><a href="portfolio.html">Portfolio</a></li>
            <li class="services"><a href="services.html">Services</a></li>
            <li class="about"><a href="about.html">About Us</a></li>
            <li class="contact"><a href="contact.html">Contact</a></li>
        </ul-->
      <!-- START HEADER -->
      <div class="header ">

        <!-- START MOBILE CONTROLS -->
        <!-- LEFT SIDE -->
        <div class="pull-left full-height visible-sm visible-xs">
          <!-- START ACTION BAR -->
          <div class="sm-action-bar">
            <a href="#" class="btn-link toggle-sidebar" data-toggle="sidebar">
              <span class="icon-set menu-hambuger"></span>
            </a>
          </div>
          <!-- END ACTION BAR -->
        </div>
        <!-- RIGHT SIDE -->
        <div class="pull-right full-height visible-sm visible-xs">
          <!-- START ACTION BAR -->
          <div class="sm-action-bar">
            <a href="#" class="btn-link" data-toggle="quickview" data-toggle-element="#quickview">
              <span class="icon-set menu-hambuger-plus"></span>
            </a>
          </div>
          <!-- END ACTION BAR -->

        </div>
        <!-- END MOBILE CONTROLS -->
        <div class=" pull-left sm-table">
          <div class="header-inner">
            <div class="brand inline">
                <a href="dashboard.php" onclick="">
                    <img src="img/pmi-logo.png" class="main_logo" alt="logo" data-src="img/pmi-logo.png" data-src-retina="img/pmi-logo.png" width="175" height="40" >
                </a>
            </div>
          </div>
        </div>
        
        <!--div class="pull-right">
            <div class="p-t-20">
                <a href="javascript:void(0)" id="__notify" class="hide" data-placement="left" data-toggle="tooltip" data-trigger="hover" title="Device alerts">
                    <i class="fa fa-bell-o fa-lg"></i>
                    <div class="notification-bubble">
                        <span>0</span>  
                    </div>
                </a>
            </div>
        </div-->
        <div class=" pull-right">
          <!-- START User Info-->
          <div class="dropdown visible-lg visible-md m-t-5 cursor">
            <?php if($loggedUserRoleId) { ?>
                <div class="thumbnail-wrapper d32 circular inline m-t-5 cursor-pointer"  data-toggle="dropdown"  data-placement="left" data-toggle="tooltip" data-trigger="hover" title="<?php echo $loggedInUsername;?> (<?php echo $loggedInUserRoles;?>)" >
                  <?php
                        $date = new DateTime();
                        
                  	 $imgPath = "img/profiles/profile-".$loggedInUserId.".png";
                        if(!file_exists($imgPath)) $imgPath = "img/profiles/no-image.jpg";
                  ?>
                  <img src="<?php echo $imgPath;?>?<?php echo $date->getTimestamp();?>" alt="" data-src="<?php echo $imgPath;?>"  width="32" height="32">
                </div>
            <?php } ?>
              
            <div class="dropdown-menu notification-toggle drop-profile-info" role="menu" aria-labelledby="profile-info" style="left: -153px; top:55px;z-index:2000!important;" id="ddmenu">
                <div class="notification-panel" style="width:200px;">
                    <div class="notification-body">
                        <div class="notification-item  clearfix">
                          <div class="heading">
                            <a href="javascript:void(0);" onclick ="Javascript:window.location='profile.php';" class="">
                              <span class="span-text fs-16 m-b-0 bold">My Profile</span>
                            </a>
                          </div>
                        </div>
                        <div class="notification-item  clearfix">
                          <div class="heading">
                            <a href="logout.php" class="">
                              <span class="span-text fs-16 m-b-0 bold">Log Out</span>
                            </a>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
          </div>
          <!-- END User Info-->
        </div>
        
        <div class="pull-right">
            <div class="p-t-20 p-r-15">
                <a href="javascript:void(0)" id="__notify" class="" data-placement="left" data-toggle="tooltip" data-trigger="hover" title="alerts">
                    <div class="notification-bubble alert-bubbele hide">
                        <span>0</span>  
                    </div>
                    <i class="fa fa-bell-o fa-lg"></i>
                    
                </a>
            </div>
        </div>
        
        <div class="navbar"style="width: 95%;margin-top: 0px;">
            <nav id="main_menu">
                <div class="menu_wrap">
                    <ul class="nav sf-menu">
                        <li class="<?php if($currentPageName == "dashboard.php") echo "current"; ?> first">
                            <a href="dashboard.php" class="small first main-m"><i class="fa fa-home" aria-hidden="true"></i> Home</a>
                        </li>
                        <li class="<?php if($currentPageName == "settings.php" || $currentPageName == "alerts.php") echo "current"; ?> sub-menu two">
                            <a href="javascript:void(0);" class="small main-m">
                                <i class="fa fa-sliders" aria-hidden="true"></i> Settings
                            </a>
                            <ul class="settings">
                                <li><a href="gauges.php" class="sub_menu"><span>-</span>Gauges</a></li>
                                <li><a href="graph.php" class="sub_menu"><span>-</span>Graph</a></li>
                                <li><a href="terminal.php" class="sub_menu"><span>-</span>Terminal Emulator</a></li>
                                <li><a href="alerts.php" class="sub_menu"><span>-</span>Alert Definition</a></li>
                            </ul>
                        </li>
                        <li class="<?php if($currentPageName == "chart.php") echo "current"; ?> sub-menu three">
                            <a href="chart.php" class="small main-m">
                                <i class="fa fa-bar-chart" aria-hidden="true"></i> Chart
                            </a>
                        </li>
                        <li class="<?php if($currentPageName == "user.php") echo "current"; ?> sub-menu four">
                            <a href="user.php" class="main-m">
                                <i class="fa fa-users" aria-hidden="true"></i> User control
                            </a>
                        </li>
                        <li class="<?php if($currentPageName == "locationSettings.php") echo "current"; ?> sub-menu five">
                            <a href="javascript:void(0);" class="main-m">
                                <i class="fa fa-map-marker" aria-hidden="true"></i> 
                                 Location settings
                            </a>
                            <ul class="location_settings">
                                <li><a href="cluster.php" class="sub_menu"><span>-</span>Cluster</a></li>
                                <li><a href="policy.php" class="sub_menu"><span>-</span>Policy</a></li>
                                <li><a href="override.php" class="sub_menu"><span>-</span>Override</a></li>
                                <li><a href="mergePolicy.php" class="sub_menu"><span>-</span>Merge Policy</a></li>
                            </ul>
                        </li>
                        <li class="<?php if($currentPageName == "locationEditor.php") echo "current"; ?> sub-menu six">
                            <a href="locationEditor.php" class="main-m">
                                <i class="fa fa-pencil" aria-hidden="true"></i>
                                 Location Editor
                            </a>
                        </li>
                        <li class="<?php if($currentPageName == "maintenance.php") echo "current"; ?> sub-menu seven">
                            <a href="javascript:void(0);" class="main-m">
                                <i class="fa fa-cogs" aria-hidden="true"></i>
                                Maintenance
                            </a>
                            <ul class="maintenance">
                                <li><a href="maintenance.php?t=commissioning" class="sub_menu"><span>-</span>Commissioning</a></li>
                                <li><a href="maintenance.php?t=restore" class="sub_menu"><span>-</span>Restore to default</a></li>
                                <li><a href="maintenance.php?t=help" class="sub_menu"><span>-</span>Help Menu</a></li>
                                <li><a href="maintenance.php?t=QA" class="sub_menu"><span>-</span>Q&A</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
      </div>
      <!-- END HEADER -->
      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper" >
        <!-- START PAGE CONTENT -->
    <!-- START PAGE-CONTAINER -->
   