<!-- START LEFT SIDE PANEL -->
<div class="left-tree-view-panel">
    <div class="form">
        <div class="form-group">
            <input type="text" id="txtNewCluster" class="form-control" placeholder="Type a cluster name & press enter" required="">
        </div>
    </div>
    <a href="javascript:void(0)" onclick="$ClusterTree.init(0,1)" class="tag-refresh" data-toggle="tooltip" data-placement="right" title="Refresh tree" data-container="body"><i class="fa fa-refresh" aria-hidden="true"></i></a>
    <div id="leftTreePanel" data-type="0">
       
        
    </div>
</div>
<!-- START LEFT SIDE PANEL -->