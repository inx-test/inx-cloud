<!-- START RIGHT SIDE PANEL -->
<div class="right-tree-view-panel">
    <div class="m-t-10 clearfix m-b-10">
        <!--div class="col-sm-12 p-l-30">
            <span class="semi-bold fs-16">IP Address : </span>
            <span class="bold fs-16 bg-grey p-l-5 p-r-5"><?php echo $_SERVER['SERVER_ADDR']; ?></span>
        </div-->
    </div>
    <div class="row b-b b-t b-grey m-t-5 m-l-0 m-r-0">
        <div class="col-sm-12 text-center p-t-5 p-b-5">
            <span class="bold fs-16" id="rightPanelTitle">
                <?php
                    if($currentPageName == "cluster.php") echo 'Cluster';
                    else if($currentPageName == "policy.php" || $currentPageName == "mergePolicy.php") echo 'Policy';
                    else if($currentPageName == "override.php") echo 'Scene Control Policy';
                    
                ?>
                
            </span>
        </div>
    </div>
    <div id="rightTreePanel"  data-type="1">
        
    </div>
</div>
<!-- END RIGHT SIDE PANEL -->