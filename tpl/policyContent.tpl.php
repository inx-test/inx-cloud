<!-- START POLICY CREATION CONTAINER -->
<form class="form-group">
<div class="row m-t-10">
    <div class="col-md-8">
        <div class="form-group required " id="divName">
            <label>Name</label>
            <input type="text" class="form-control" id="txtName" required="" placeholder="Enter policy name"  onkeyup="removeError('divName');">
            <span class="text-danger sp_error hide"></span>
        </div>
    </div>
    
    <div class="col-md-4">
        <div class="form-group required " id="divLightDefault">
            <label>Light Default</label>
            <div class="input-group">
                <input type="text" class="form-control" id="txtLightDefault" required="" placeholder="Enter Default"  onkeyup="removeError('divLightDefault');">
                <span class="input-group-addon bold">
                    %
                </span>
            </div>
            <span class="text-danger sp_error hide"></span>
        </div>
    </div>
</div>
<div class="row">  
    <div class=" col-md-6 ">
        <div class="form-group required" id="divFormate">
            <label class="">Formate</label>
            <select class="full-width" id="selFormate" data-placeholder="Select foramte" data-init-plugin="select2" onchange="return $Policy.changeFormate(this.value);">
                <option value="1" selected> Weekly</option>
                <option value="2">Single</option>
                <option value="3">Override</option>
            </select>
            <span class="text-danger sp_error hide"></span>
        </div>
    </div>
    <div class=" col-md-3 ">
        <div class="form-group required" id="divAction">
            <label class="">Action</label>
            <select class="full-width" id="selAction" data-placeholder="Select action" data-init-plugin="select2">
                <option value="1" selected> On</option>
                <option value="0">Off</option>
            </select>
            <span class="text-danger sp_error hide"></span>
        </div>
    </div>
    <div class=" col-md-3 ">
        <div class="form-group required" id="divFadeIn">
            <label class="">Fade In</label>
            <select class="full-width" id="selFadeIn" data-placeholder="Select time" data-init-plugin="select2">
                <option value="1" selected>0.1 sec</option>
                <option value="2">0.2 sec</option>
                <option value="3">0.5 sec</option>
                <option value="4">1 sec</option>
                <option value="5">2 sec</option>
                <option value="6">5 sec</option>
                <option value="7">10 sec</option>
            </select>
            <span class="text-danger sp_error hide"></span>
        </div>
    </div>
</div>
<div class="row" id="divMotion">
    <div class="col-sm-4 checkbox check-success  ">
        <input type="checkbox" checked="checked" value="1" id="chMotionLock" onchange="return $Policy.changeMotionStatus(this);">
        <label for="chMotionLock">Motion Locked</label>
    </div>
        <div class="form-group required hide" id="divduration">
            <label class="col-sm-2 p-t-10">Duration : </label>
            <div class="col-sm-3">
                <div class="input-group">
                    <input type="number" class="form-control" id="txtDurationH" required="" placeholder="Hour" onkeyup="removeError('divduration');">
                    <span class="input-group-addon"> HH </span>
                </div>
            </div>
            <div class="col-sm-3 p-r-0">
                <div class="input-group">
                    <input type="number" class="form-control" id="txtDurationM" required="" placeholder="Minute" onkeyup="removeError('divduration');">
                    <span class="input-group-addon"> MM </span>
                </div>
            </div>
            <span class="text-danger sp_error hide"></span>
        </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="form-group required" id="divDim">
            <label class="">Dim</label>
            <input type="text" id="dim_slider" data-type="ionSlide" name="dim_slider" value="0" />
            <span class="text-danger sp_error hide"></span>
        </div>
    </div>
</div>                                   

<div class="row hide" id="singleTimeContainer">  
    <div class=" col-md-4 ">
        <label class="bold fs-11 upper" id="lblTime">Time</label>
        <div class="form-group input-group required" id="DivTime"  style="overflow:auto;">
            <input type="text" class="form-control" id="txtTime" placeholder="Pick a time" onfocus="return removeError('DivTime');">
            <span class="input-group-addon">
                <i class="fa fa-clock-o"></i>
            </span>
            <span class="text-danger sp_error hide"></span>
        </div>
    </div>
</div> 
<div class="" id="dateTimeContainer">

    <div class="row">  
        <div class=" col-md-6" >
            <div class="form-group form-group-default-select2 hide required" id="divTimeFormate">
                <select class="full-width" id="seleTimeFormate" data-placeholder="Select formate" data-init-plugin="select2" onchange="return $Policy.changeTimeFormate(this.value);">
                    <option value="0" selected> No Repeat</option>
                    <option value="1">Weekly</option>
                    <option value="2">Monthly</option>
                    <option value="3">Yearly</option>
                </select>
                <span class="text-danger sp_error hide"></span>
            </div>
        </div>
    </div>
    <div class="">  
        <div class=" col-md-6 hide " id="startDateContainer">
            
            <label class="bold fs-11 upper">Start Date</label>
            <div class="form-group input-group required" id="divStartDate" style="overflow:auto;">
                <input type="text" class="form-control" id="txtStartDate" placeholder="Pick a date" onfocus="return removeError('divStartDate');">
                <span class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                </span>
                <span class="text-danger sp_error hide"></span>
            </div>
        </div>

        <div class=" col-md-4 div-strat-time p-l-0">
            <label class="bold fs-11 upper">Start Time</label>
            <div class="form-group input-group required" id="divStartTime" style="overflow:auto;">
                <input type="text" class="form-control" id="txtSatartTime" placeholder="Pick a time"  onfocus="return removeError('divStartTime');">
                <span class="input-group-addon">
                    <i class="fa fa-clock-o"></i>
                </span>
                <span class="text-danger sp_error hide"></span>
            </div>
        </div>
    </div>


    <div class="row">  
        <div class=" col-md-6 hide" id="endDateContainer">
            <label class="bold fs-11 upper">End Date</label>
            <div class="form-group input-group required" id="divEndDate"   style="overflow:auto;">
                <input type="text" class="form-control" id="txtEndDate" placeholder="Pick a date" onfocus="return removeError('divEndDate');">
                <span class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                </span>
                <span class="text-danger sp_error hide"></span>
            </div>
        </div>

        <div class=" col-md-4 ">
            <label class="bold fs-11 upper">End Time</label>
            <div class="form-group input-group required" id="divEndTime"  style="overflow:auto;">
                <input type="text" class="form-control" id="txtEndTime" placeholder="Pick a time" onfocus="return removeError('divEndTime');">
                <span class="input-group-addon">
                    <i class="fa fa-clock-o"></i>
                </span>
                <span class="text-danger sp_error hide"></span>
            </div>
        </div>
    </div>
</div>
<div class="row" id="repeatContainer">
    <label class="full-width clearfix m-b-10 bold fs-11 upper">Days of week</label>
    <div class="checkbox check-success pull-left ">
        <input type="checkbox" class="checkAll" onchange="return $Policy.changeRepeatStatus(this);" value="ALL" id="checkSelectAll">
        <label for="checkSelectAll">Select All</label>
    </div>
    <div class="checkbox check-success pull-left ">
        <input type="checkbox" class="checkDay" onchange="return $Policy.changeRepeatStatus(this);" value="1" id="checkMon">
        <label for="checkMon">Mon</label>
    </div>
    <div class="checkbox check-success pull-left ">
        <input type="checkbox" class="checkDay" onchange="return $Policy.changeRepeatStatus(this);" value="2" id="checkTue">
        <label for="checkTue">Tue</label>
    </div>
    <div class="checkbox check-success pull-left ">
        <input type="checkbox" class="checkDay" onchange="return $Policy.changeRepeatStatus(this);" value="3" id="checkWed">
        <label for="checkWed">Wed</label>
    </div>
    <div class="checkbox check-success  pull-left">
        <input type="checkbox" class="checkDay" onchange="return $Policy.changeRepeatStatus(this);" value="4" id="checkThu">
        <label for="checkThu">Thu</label>
    </div>
    <div class="checkbox check-success pull-left ">
        <input type="checkbox" class="checkDay" onchange="return $Policy.changeRepeatStatus(this);" value="5" id="checkFri">
        <label for="checkFri">Fri</label>
    </div>
    <div class="checkbox check-success pull-left ">
        <input type="checkbox" class="checkDay" onchange="return $Policy.changeRepeatStatus(this);" value="6" id="checkSat">
        <label for="checkSat">Sat</label>
    </div>

    <div class="checkbox check-success pull-left ">
        <input type="checkbox" class="checkDay" onchange="return $Policy.changeRepeatStatus(this);" value="7" id="checkSun">
        <label for="checkSun">Sun</label>
    </div>
</div>
<input  type="hidden" id="hdnPolicyID" name="hdnPolicyID" value="">
<input  type="hidden" id="hdnPEditStat" name="hdnPEditStat" value="0">
<div class="row text-center">
    <div class="col-md-12 m-b-15 m-t-15">
        <button type="button" class="btn btn-cons btn-success btn-sm" onclick="return $Policy.save();">Save</button>
        <button type="button" class="btn btn-cons btn-danger btn-sm" id="btnPolicyDel" onclick='return $Policy.removePolicy();'>Remove</button>
        <button type="button" class="btn btn-cons btn-default btn-sm " onclick='return $Policy.clear();'>Cancel</button>
    </div>
</div>
<div class="row hide" id="removeErrorMSG">
    <div class="col-md-12 text-center">
        <span class="text-danger bold"><i class="fa fa-exclamation-triangle fs-16 p-r-5" aria-hidden="true"></i>You can not remove this since policy merged.</span>
    </div>
</div>
</form>


<!-- END POLICY CREATION CONTAINER -->