<!-- END PAGE CONTENT -->
<!-- START COPYRIGHT -->
<!-- START CONTAINER FLUID -->
<div class="container-fluid container-fixed-lg footer">
  <div class="copyright sm-text-center">
    <p class="small no-margin pull-left sm-pull-reset">
      <span class="hint-text">Copyright © 2020 </span>
      <span class="font-montserrat">MHT International</span>.
      <span class="hint-text">All rights reserved. </span>
    </p>
    <div class="clearfix"></div>
  </div>
</div>
<script>
  function openSideBar(){
    $(".page-sidebar").css("transform","translate3d(210px, 0px, 0px)");
  }

</script>
<!-- END COPYRIGHT -->
</div>

<!-- END PAGE CONTENT WRAPPER -->
</div>
<!-- END PAGE CONTAINER -->
</div>
</div>
</div>
</body>

</html>
