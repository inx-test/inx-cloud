<div class="text-center ">
	<br /><br />
	<a href="files/INSPEXTORUserManual.pdf" target="_blank"
		onclick="openPdf(event, 'files/INSPEXTORUserManual.pdf', 'newpage.html');">
		<font size="6">Download Full Manual</font>
	</a>
</div>
<div class="row m-t-20">
    <div class="col-sm-12 col-xl-12 col-lg-12 col-md-12">
        <div class="form-group form-group-default input-group">
            <label>Search</label>
            <input type="email" class="form-control" autocomplete="off" onkeyup="$_maintenance.QASearch(this);" >
            <span class="input-group-addon">
                <i class="fa fa-search" aria-hidden="true"></i>
            </span>
        </div>
    </div>
</div>
<div class="panel panel-group panel-transparent qa " data-toggle="collapse" id="accordion">
    <?php 
        $result = file_get_contents("files/QAList.txt");
        $resultArr = explode("**********", $result);
        
        if(sizeof($resultArr) > 0){
            $count = 0;
            $htmStr = "";
            foreach ($resultArr as $item) {
                $splitItem = explode("||", $item);
                $question = $splitItem[0];
                $answer = $splitItem[1];
                if(!empty($question)){
                    $count++;
                    $htmStr .= '<div class="panel panel-default qaFilter" style="border-color: #e49327; border-width: 2px;">';
                    $htmStr .= '    <div class="panel-heading p-l-0">';
                    $htmStr .= '      <h4 class="panel-title">';
                    $htmStr .= '         <a class="collapsed qa-question" data-parent="#accordion" data-toggle="collapse" href="#collapse'.$count.'">';
                    $htmStr .= $question;
                    $htmStr .= '          </a>';
                    $htmStr .= '       </h4>';
                    $htmStr .= '     </div>';
                    $htmStr .= '     <div class="panel-collapse collapse" id="collapse'.$count.'" style="height: 0px;">';
                    $htmStr .= '        <div class="panel-body p-t-0 p-l-50 fs-15">';
                    $htmStr .= $answer ;
                    $htmStr .= '        </div>';
                    $htmStr .= '     </div>';
                    $htmStr .= '</div>';
                }
            }
            echo $htmStr;
			$hideCls = "hide";
        }
    ?>
</div>
<h5 class="text-center text-l-help qa-filter-empty <?php echo $hideCls; ?>">Word Not Found.</h5>