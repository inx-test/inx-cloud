<div class="action-card ">
    <div class="row">
        <h5 class="semi-bold text-center">
            Are you sure want to restore
        </h5>
    </div>
    <div class="row text-center m-t-15">
        <button type="button" class="btn btn-cons btn-success btn-xs bold" onclick="$maintenance.restor();">Yes</button>
        <button type="button" class="btn btn-cons btn-danger btn-xs bold" id="btnPolicyDel" >No</button>
    </div>
    
</div>

<div class="action-card-process">
    <div class="row">
        <h5 class="semi-bold text-center">
            Restore process queued, Please wait.
        </h5>
    </div>
</div>
