<?php 
include "config.php";
include_once("tpl/header.tpl.php");
?>
   <div class="content">
      <!-- START PANEL -->
       <div class="panel panel-transparent">
         <div class="panel-heading ">
         </div>
          <!-- START Demo Code  -->  
<div class="container">	
			<!-- Codrops top bar -->
			<!--div class="codrops-top clearfix">
				<a class="codrops-icon codrops-icon-prev" href="http://tympanus.net/Development/PageTransitions/"><span>previous Demo</span></a>
				<span class="right"><a class="codrops-icon codrops-icon-drop" href="http://tympanus.net/codrops/?p=14991"><span>Back to the Codrops Article</span></a></span>
			</div-->
			<header>
				<h1>Welcome to MHT lighitng Director's Dashboard page </h1>	
			</header>

		</div><!-- /container -->
            <!-- END Demo Code -->

         <div class="panel-body">               
           
		</div>
  	</div>
</div>
<!-- END PANEL -->
   </div>
   <!-- END PAGE CONTENT -->
   <?php include_once("tpl/footer.tpl.php");?>

<!--script src="js/jquery.min.js" type="text/javascript"></script-->


<script type="text/javascript">
var loggedUserId = '<?php echo $loggedInUserId; ?>';
var loggedUserRole = '<?php echo $loggedUserRoleId; ?>';
var allRolesId = new Array();
var allRolesName = new Array();
        $(document).ready(function ()
        {
            $(".content").css("min-height",($(window).height()));
            loadUser();
            
            $('.form-control').keypress(function(e){
                if(e.keyCode==13){
                  $('#btnSaveUser').click();
                  return false;
            }
            });
            
            $.ajax ({
                type:'POST',
                url:'ajax.php',
                data: { 'c':'User', 'a':'fetchAllUserRole' },
                dataType:'json',
                success: function(response){
                    if( response.status == 1 ){
                        for(i=0;i<response.userrole.length;i++){
                            allRolesId.push(response.userrole[i]['id']) ;
                            allRolesName.push(response.userrole[i]['role_name']) ;
                        }
                        //alert(allRolesId+"---"+allRolesName);
                    }
                    else if( response.status == 0 ){
                        
                    }
                },
                error: function(r){
                    console.log(r);
                }
            });
            
        });
       
       $('html').on('click', function(e) {
            if (typeof $(e.target).data('original-title') == 'undefined' &&
               !$(e.target).parents().is('.popover.in')) {
              $('.popover').popover('hide');
            }
        }); 
    //  The function to change the class
   var changeClass = function (r,className1,className2) {
    var regex = new RegExp("(?:^|\\s+)" + className1 + "(?:\\s+|$)");
    if( regex.test(r.className) ) {
     r.className = r.className.replace(regex,' '+className2+' ');
       }
       else{
     r.className = r.className.replace(new RegExp("(?:^|\\s+)" + className2 + "(?:\\s+|$)"),' '+className1+' ');
       }
       return r.className;
   };       
   
</script>

       