<?php
    include_once("config.php");

    class DashboardController extends ControllerBase
    {
        # Constructor Method
        function __constructor(){
        }

        //Get current pmi log data and all guage data
    function GetGuages() {
        $db = new DBC();
        $type = $_REQUEST["type"];
        $nodeID = isset($_REQUEST["nodeID"]) ? $_REQUEST["nodeID"] : 0;
        $clusterID = isset($_REQUEST["clusterID"]) ? $_REQUEST["clusterID"] : 0;
        try {

            $date = new DateTime();
            $startDate = $date->format('Y-m-d H:i:s');
            $date->sub(new DateInterval('PT5S'));
            $endDate = $date->format('Y-m-d H:i:s');

            $where = "";
            $joinTable = "LEFT JOIN pmi_node_status NS ON (N.ID = NS.NodeID AND NS.CreatedOn BETWEEN '".$endDate."' AND '".$startDate."'  ) ";
            $joinTable .= "LEFT JOIN pmi_tag_status TS ON (NS.ID = TS.NodeStatusID)";
            //AND NS.Status=0
            $param = "";

            if($type == 1) $where .= " N.ID = ".$nodeID;
            else if($type == 2){
                $where .= " C.ClusterID = ".$clusterID." AND C.Deleted=0";
                $joinTable .= " LEFT JOIN pmi_cluster_mapping C ON (C.NodeID = N.ID)";
            }

            $sql =<<<EOF
                SELECT
                    N.ID,
                    NS.NodeID,
                    NS.VoltageMaster,
                    NS.DriverCurrent,
                    NS.SubVoltage,
                    NS.DriverSubCurrent,
                    AVG(TS.Temp) AS Temp,
                    AVG(TS.`Status`) AS Status,
                    N.Name
                FROM pmi_node N
                    $joinTable
                WHERE
                    $where
EOF;
            //die($sql);
            $guageVal = $db->get_result($sql);

            $sql = "SELECT * FROM  pmi_guage WHERE display = 1";
            $guage = $db->get_result($sql);

            //print_r($guage);
            $sql = "SELECT * FROM pmi_guage ORDER BY display_order";
            $allGuage = $db->get_result($sql);

            $result['getValue'] = $guageVal;
            $result['reuslt'] = $guage;
            $result['allGuage'] = $allGuage;
            $result['status'] = 1;
            die(json_encode($result));
        } catch (Exception $exc) {
            echo $exc->getMessage();
            $result['status'] = 0;
            die(json_encode($result));
        }
    }

    function SetGuageVisibility(){
            $db = new DBC();
            $guageID = $_REQUEST['guageID'];
            $checkedStatus = $_REQUEST['checkedStatus'];
            $modelNo = 3;
            if($guageID != 0){
                $dbrec["display"] = $checkedStatus;
                $where["ID"] = $guageID;
            }
            else{
                $dbrec["display"] = $checkedStatus;
                $where["label"] = "Voltage";
            }

            $status = $db->update_query($dbrec , 'pmi_guage',$where);

            if($status == 1){
                if(isset($_SESSION["activeDeviceID"]) &&  $_SESSION["activeDeviceID"] != 0){
                     $HBsql = "SELECT *,(SELECT  CAST(SUM(HB.kwh_24)/COUNT( DISTINCT HB.deviceID) AS DECIMAL(10,1)) FROM pmi_device D LEFT JOIN pmi_heart_beat HB ON(HB.deviceID = D.id) WHERE D.deleted=0) AS 'KWH',"

                        ."(SELECT  CAST(SUM(HB.kva_24)/COUNT( DISTINCT HB.deviceID) AS DECIMAL(10,1)) FROM pmi_device D LEFT JOIN pmi_heart_beat HB ON(HB.deviceID = D.id) WHERE D.deleted=0) AS 'KVA',"

                        ."(SELECT CAST(SUM(HB.kvar_24)/COUNT( DISTINCT HB.deviceID) AS DECIMAL(10,1)) FROM pmi_device D LEFT JOIN pmi_heart_beat HB ON(HB.deviceID = D.id) WHERE D.deleted=0) AS 'KVAR'"

                        . " FROM pmi_heart_beat WHERE deviceID = ".$_SESSION["activeDeviceID"];
                    $HBData = $db->get_result($HBsql);
                    if(count($HBData) > 0 )$modelNo = $GLOBALS["MODELNO"][$HBData[0]["model_number"]]["Model"];
                }
                else{
                    $HBData = "";
                }
                if($guageID != 0) $sql = "SELECT * FROM  pmi_guage WHERE ID = $guageID";
                else $sql = "SELECT * FROM  pmi_guage WHERE model_no= ".$modelNo;
                $guage = $db->get_result($sql);
            }
            else{
                $logData = "";
                $guage = "";
            }

            $result['getValue'] = $HBData;
            $result['reuslt'] = $guage;
            $result['status'] = $status;
            die(json_encode($result));
        }

    function SetAllGuagesVisibility(){
        $db = new DBC();
        $checkedStatus = $_REQUEST['checkedStatus'];
        $modelNo = 3;

        //$dbrec["display"] = $checkedStatus;
        $sql = "UPDATE pmi_guage SET display = ".$checkedStatus." WHERE label NOT IN ('KWH','KVA','KVAR')";
        $status = $db->query($sql);

        if($status){
            if(isset($_SESSION["activeDeviceID"]) &&  $_SESSION["activeDeviceID"] != 0){
                $HBsql = "SELECT *,(SELECT IFNULL(CAST(SUM(HB.kwh_24) AS DECIMAL(10,1)),0) FROM pmi_device D LEFT JOIN pmi_heart_beat HB ON(HB.deviceID = D.id) WHERE D.deleted=0 AND HB.logdate > DATE_SUB(NOW(), INTERVAL 24 HOUR)) AS 'KWH',"

                    ."(SELECT IFNULL(CAST(SUM(HB.kva_24) AS DECIMAL(10,1)),0) FROM pmi_device D LEFT JOIN pmi_heart_beat HB ON(HB.deviceID = D.id) WHERE D.deleted=0 AND HB.logdate > DATE_SUB(NOW(), INTERVAL 24 HOUR)) AS 'KVA',"

                    ."(SELECT IFNULL(CAST(SUM(HB.kvar_24) AS DECIMAL(10,1)),0) FROM pmi_device D LEFT JOIN pmi_heart_beat HB ON(HB.deviceID = D.id) WHERE D.deleted=0 AND HB.logdate > DATE_SUB(NOW(), INTERVAL 24 HOUR)) AS 'KVAR'"

                    . " FROM pmi_heart_beat WHERE deviceID = ".$_SESSION["activeDeviceID"]." and deleted = 0";

                $HBData = $db->get_result($HBsql);
                if(count($HBData) > 0 )$modelNo = $GLOBALS["MODELNO"][$HBData[0]["model_number"]]["Model"];
            }
            else{
                $HBData = "";
            }

            $sql = "SELECT * FROM  pmi_guage WHERE (model_no= ".$modelNo." OR model_no= 0 )";
            $guage = $db->get_result($sql);
        }
        else{
            $logData = "";
            $guage = "";
        }

        $result['getValue'] = $HBData;
        $result['reuslt'] = $guage;
        $result['status'] = $status ? 1 : 0;
        die(json_encode($result));
    }

    function SaveGaugeSettings(){
        $db = new DBC();
        $guageID = $_REQUEST['guageID'];
        $min = $_REQUEST['min'];
        $max = $_REQUEST['max'];
        $majorticks = $_REQUEST['majorticks'];
        $color = $_REQUEST['color'];
        $label = $_REQUEST['label'];
		$status = 0;

        $dbrec["min_val"] = $min;
        $dbrec["max_val"] = $max;
        if($majorticks != "") $dbrec["markers"] = $majorticks;
        if(urldecode($color) != "") $dbrec["color"] = urldecode($color);
        if(urldecode($color) != "") $dbrec["custom"] = urldecode($color) != "" ? TRUE : FALSE;
        $where["ID"] = $guageID;
        $status = $db->update_query($dbrec , 'pmi_guage',$where);

        if($status == 1){
            /*if(isset($_SESSION["activeDeviceID"]) &&  $_SESSION["activeDeviceID"] != 0){
                $sql = "SELECT * FROM pmi_log WHERE ID = (SELECT MAX(ID) FROM pmi_log WHERE deviceID = ".$_SESSION["activeDeviceID"].")";
                $logData = $db->get_result($sql);
            }
            else{
                $logData = "";
            }*/
            /*if($label == 'Voltage'){
                $modelNo = ($_SESSION["activeDeviceModelNo"] != NULL && $_SESSION["activeDeviceModelNo"] != "") ? $GLOBALS["MODELNO"][$_SESSION["activeDeviceModelNo"]]["Model"] : 3;
                $sql = "SELECT * FROM  pmi_guage WHERE model_no=" . $modelNo.")";
            }
            else{*/
                $sql = "SELECT * FROM  pmi_guage WHERE ID = ".$guageID;
            //}
            //echo $sql;
            $guage = $db->get_result($sql);
        }
        else{
            //$logData = "";
            $guage = "";
        }

        //$result['getValue'] = $logData;
        $result['result'] = $guage;
        $result['status'] = $status;
		$result['message'] = ($status == 0)?'Failed to update gauge settings':'Successfully updated gauge settings';
        die(json_encode($result));
    }

    function GetGuageValue() {
        $db = new DBC();
        $type = isset($_REQUEST["type"]) ? $_REQUEST["type"] : 0;;
        $nodeID = isset($_REQUEST["nodeID"]) ? $_REQUEST["nodeID"] : 0;
        $clusterID = isset($_REQUEST["clusterID"]) ? $_REQUEST["clusterID"] : 0;
        $pageName = $_REQUEST['pagename'];
        //$fromDate = date("Y-m-d H:m:s", strtotime('-24 hours', time()));
        //$toDate = date("Y-m-d H:m:s");

        try {
            $date = new DateTime();
            $startDate = $date->format('Y-m-d H:i:s');
            $date->sub(new DateInterval('PT5S'));
            $endDate = $date->format('Y-m-d H:i:s');

            $guageVal = array();

            if($type != 0 && $pageName == "gauges"){
                $where = "";
                $joinTable = "LEFT JOIN pmi_node_status NS ON (N.ID = NS.NodeID AND NS.CreatedOn BETWEEN '".$endDate."' AND '".$startDate."' ) ";
            $joinTable .= "LEFT JOIN pmi_tag_status TS ON (NS.ID = TS.NodeStatusID)";
                //AND NS.Status=0
                $param = "";

                if($type == 1) $where .= " N.ID = ".$nodeID;
                else if($type == 2){
                    $where .= " C.ClusterID = ".$clusterID." AND C.Deleted=0";
                    $joinTable .= " LEFT JOIN pmi_cluster_mapping C ON (C.NodeID = N.ID)";
                }

                $sql =<<<EOF
                    SELECT
                        N.ID,
                        NS.NodeID,
                        NS.VoltageMaster,
                        NS.DriverCurrent,
                        NS.SubVoltage,
                        NS.DriverSubCurrent,
                        AVG(TS.Temp) AS Temp,
                        AVG(TS.`Status`) AS Status,
                        N.Name
                    FROM pmi_node N
                        $joinTable
                    WHERE
                        $where
EOF;
               // die($sql);
                $guageVal = $db->get_result($sql);
            }
             $where = "AND A.CratedOn >= NOW() - INTERVAL 5 SECOND";
             $PolicyComplete = ALERT_POLICY_COMPLETE;
             $sql =<<<EOF
                    SELECT A.ID,
                        A.CratedOn,
                        A.Title,
                        A.Severity,
                        A.Description,
                        T.Name AS device_name,
                        A.DeviceID,
                        ( SELECT COUNT(ID) FROM pmi_device_alerts WHERE AlertType!=$PolicyComplete AND Resolved=0 AND `Status`=0) AS TCount,
                        A.Status,
                        A.Status,
                        A.AlertType,
                        A.TypeAction,
                        A.TypeMasterID,
                        A.TypeDBID

                    FROM
                        pmi_device_alerts A
                    LEFT JOIN pmi_alert_definition D ON(D.id = A.AlertDefID AND D.deleted = 0)
                    LEFT JOIN pmi_tag T ON(T.ID = A.DeviceID AND T.deleted = 0 )
                    WHERE
                        A.`Status` = 0 AND
                        A.Resolved=0
                        $where
                    ORDER BY A.ID DESC
EOF;
            //die($sql);
            $alertData = $db->get_result($sql);

            $sql = "SELECT COUNT(ID) FROM pmi_device_alerts WHERE AlertType!=$PolicyComplete AND Resolved=0 AND `Status`=0";
            //die($sql);
            $alertCount = $db->get_single_result($sql);
            $sql = "SELECT * FROM  pmi_guage WHERE display = 1";
            $guage = $db->get_result($sql);

            if(sizeof($guageVal) > 0) $result['reuslt'] = $guageVal;
            else  $result['reuslt'] = "";
            $result['allGauges'] = $guage;
            $result['alerts'] = $alertData;
            $result['alertsCount'] = $alertCount;
            $result['status'] = 1;
            //print_r($result);
            die(json_encode($result));

        } catch (Exception $exc) {
             echo $exc->getMessage();
            $result['status'] = 0;
            die(json_encode($result));
        }

    }

    function getAllDevice(){
            $db = new DBC();
            $sql = "SELECT D.*,H.model_number FROM  pmi_device D LEFT JOIN pmi_heart_beat H ON(H.deviceID = D.id) where D.deleted = 0";
            $deviceData = $db->get_result($sql);

            $activeDeviceId = 0;
            if(isset($_SESSION["activeDeviceID"])){
                $activeDeviceId = $_SESSION["activeDeviceID"];
            }
            $result['deviceData'] = $deviceData;
            $result['activeDeviceID'] = $activeDeviceId;
            die(json_encode($result));
        }

        function setActiveDevice(){
            $db = new DBC();
            $deviceID = $_REQUEST['device'];
            $sql = "SELECT D.device_name,D.serial_no,D.ip_address,D.location,D.status,H.model_number FROM  pmi_device D "
                    ."LEFT JOIN pmi_heart_beat H ON(H.deviceID = D.id) WHERE D.ID = ".$deviceID;
            $result = $db->get_result($sql);
            if(count($result) > 0){
                $_SESSION["activeDeviceID"] = $deviceID;
                $_SESSION["activeDeviceName"] =  $result[0]["device_name"];
                $_SESSION["activeDeviceSerialNo"] =  $result[0]["serial_no"];
                $_SESSION["activeDeviceIPAddress"] =  $result[0]["ip_address"];
                $_SESSION["activeDeviceLocation"] =  $result[0]["location"];
                $_SESSION["activeDevicePingStatus"] =  $result[0]["status"];
                $_SESSION["activeDeviceModelNo"] =  $result[0]["model_number"];
                $Status = 1;
                //echo $_SESSION["activeDeviceModelNo"];
            }
            else{
                $_SESSION["activeDeviceID"] = 0;
                $_SESSION["activeDeviceName"] =  "";
                $_SESSION["activeDeviceSerialNo"] =  "";
                $_SESSION["activeDeviceIPAddress"] =  "";
                $_SESSION["activeDeviceLocation"] =  "";
                $_SESSION["activeDevicePingStatus"] =  "";
                $_SESSION["activeDeviceModelNo"] =  "";
                $Status = 0;
            }
            $result['status'] = $Status;
            $result['result'] = $result;
            die(json_encode($result));
        }

        function getAllParameter(){
            $db = new DBC();
            $sql = "SELECT ID,label,display,unit,auto_k FROM pmi_guage";
            $allparam = $db->get_result($sql);

            $result['reuslt'] = $allparam;
            die(json_encode($result));
        }

        function getVoltageGuage(){
            $db = new DBC();
            $modelNo = 3;
            $modelNo = ($_SESSION["activeDeviceModelNo"] != NULL && $_SESSION["activeDeviceModelNo"] != "") ? $GLOBALS["MODELNO"][$_SESSION["activeDeviceModelNo"]]["Model"] : 3;
            $sql = "SELECT * FROM  pmi_guage WHERE model_no = ".$modelNo." AND display = 1";
            $guage = $db->get_result($sql);
            $result['reuslt'] = $guage;
            $result['status'] = 1;
            die(json_encode($result));
        }
 //Get event live record
          function getEventData(){
            
            $db = new DBC();
            $sDate = new DateTime($_REQUEST['sDate']);
            $eDate = new DateTime($_REQUEST['eDate']);
            $selectType = $_REQUEST["selectType"];
            $devID = isset($_REQUEST['devID']) ? $_REQUEST['devID'] : 0;   
            $state = $_REQUEST["state"];      
            $rangeID = $_REQUEST["range"];     
            
            $Ewhere = "";
            $EjoinTable = "";
            $Egroup = "";
            
            $Ewhere = "";
            $EjoinTable = "";
            $Egroup = "";
            
            $Pwhere = "";
            $Pgroup = "";
            
            $Swhere = "";
            $Sgroup = "";
            $SjoinTable = "";
            
            $filterType = 1;//0:sec,1:min,2:hour,3:day
            
            $result =  array(); 
            
            if($state == 1){
                $Ewhere = " ( E.CreatedOn BETWEEN'".$sDate->format('Y-m-d H:i:s')."' AND '".$eDate->format('Y-m-d H:i:s')."' )"; 
                $Pwhere = " ( PT.OnDate BETWEEN'".$sDate->format('Y-m-d H:i:s')."' AND  '".$eDate->format('Y-m-d H:i:s')."')";
                $Swhere = " AND  ( S.CreatedOn BETWEEN '".$sDate->format('Y-m-d H:i:s')."' AND  '".$eDate->format('Y-m-d H:i:s')."')";
            }
            else if($state == 2){
                $Ewhere = " E.CreatedOn >= NOW() - INTERVAL 5  SECOND"; 
                $Swhere = " AND S.CreatedOn >= NOW() - INTERVAL 5  SECOND ";
                $filterType = 0;
            }
            
            if($rangeID == 0){
                $Egroup = "year,month,day,hour,minute";
                $Pgroup = "year,month,day,hour,minute";
                $Sgroup = "year,month,day,hour,minute";
                if($filterType != 0) $filterType = 1;
            }
            else if($rangeID == 1){
                if($sDate->format('Y-m-d') == $eDate->format('Y-m-d')){
                    $Egroup = "year,month,day,hour";
                    $Pgroup = "year,month,day,hour";
                    $Sgroup = "year,month,day,hour";
                    if($filterType != 0) $filterType = 2;
                }
                else{
                    $Egroup = "year,month,day";
                    $Pgroup = "year,month,day";
                    $Sgroup = "year,month,day";
                    if($filterType != 0) $filterType = 3;
                }
            }
            else {
                $Egroup = "year,month,day";
                $Pgroup = "year,month,day";
                $Sgroup = "year,month,day";
                if($filterType != 0) $filterType = 3;
            }
            if($selectType == 1){
				$EjoinTable .= "LEFT JOIN pmi_tag T ON (E.NodeID = T.NodeID) ";
                $Ewhere .= " AND T.Deleted=0 AND T.Acknowledged=1 AND T.TagTypeID=".FIXTURE_TYPE_NODE;
				$Swhere .= " AND TG.TagTypeID=".FIXTURE_TYPE_NODE;
                $Pwhere .= " AND T.TagTypeID=".FIXTURE_TYPE_NODE;
			}
            else if($selectType == 2){
                $EjoinTable .= "LEFT JOIN pmi_tag T ON (E.NodeID = T.NodeID) ";
                $Ewhere .= " AND T.ID=$devID AND T.Deleted=0 AND T.Acknowledged=1 ";
                $Pwhere .= " AND T.ID=$devID";
                $SjoinTable = "";
                $Swhere .= " AND TG.ID=$devID";
            }
            else if($selectType == 3){
                $EjoinTable .= "LEFT JOIN pmi_cluster_mapping CM ON (E.NodeID = CM.NodeID) ";
                $EjoinTable .= "LEFT JOIN pmi_cluster C ON (CM.ClusterID=C.ID) ";
                $Ewhere .= " AND C.ID=$devID AND CM.Deleted=0 AND C.Deleted=0";
                $Pwhere .= " AND C.ID=$devID";
                
                $SjoinTable .= "LEFT JOIN pmi_cluster_mapping CM ON (TG.NodeID = CM.NodeID) ";
                $SjoinTable .= "LEFT JOIN pmi_cluster C ON (CM.ClusterID=C.ID) ";
                $Swhere .= " AND C.ID=$devID AND C.Deleted=0";
            }
            $sql =<<<EOF
                    SELECT 
                        YEAR(E.CreatedOn) AS year,
                        MONTH(E.CreatedOn) AS month,
                        DAY(E.CreatedOn) AS day,
                        HOUR(E.CreatedOn) AS hour,
                        MINUTE(E.CreatedOn) AS minute,
                        SECOND(E.CreatedOn) AS second,
                        COUNT(E.ID) As EventCount,
                        E.CreatedOn 
                    FROM 
                        pmi_event E 
                        $EjoinTable
                    WHERE 
                        $Ewhere
                    GROUP BY $Egroup
EOF;
            //die($sql);
            $eventCount = $db->get_result($sql);
			//echo"here >>>>\n";
			//print_r($sql);
            if($state != 2){
                $sql =<<<EOF
                    SELECT 
                        YEAR(PT.OnDate) AS year,
                        MONTH(PT.OnDate) AS month,
                        DAY(PT.OnDate) AS day,
                        HOUR(PT.OnDate) AS hour,
                        MINUTE(PT.OnDate) AS minute,
                        SECOND(PT.OnDate) AS second,
                        COUNT(PT.PolicyID) AS CountPolicy,
                        PT.OnDate 
                    FROM 
                        pmi_policy_timeline PT
                        LEFT JOIN 
                            pmi_policy P ON (P.ID=PT.PolicyID)
                        LEFT JOIN 
                            pmi_policy_mapping PM ON (PM.PolicyID = P.ID) 
                        LEFT JOIN 
                            pmi_cluster_mapping CM ON (CM.ClusterID = PM.ClusterID) 
                        LEFT JOIN 
                            pmi_cluster C ON (C.ID = CM.ClusterID)
                        LEFT JOIN 
                            pmi_tag T ON (T.NodeID=CM.NodeID) 
                    WHERE
                        P.Deleted=0 AND
                        PM.Deleted=0 AND 
                        C.Deleted=0 AND
                        CM.Deleted=0 AND 
                        T.Deleted=0  AND
                        $Pwhere
                    GROUP BY $Pgroup
EOF;

                //die($sql);
                $PolcyCount = $db->get_result($sql);
            }
            else $PolcyCount = array();
            
            //print_r($PolcyCount);
            $sql =<<<EOF
                SELECT 
                    YEAR(S.CreatedOn) AS year,
                    MONTH(S.CreatedOn) AS month,
                    DAY(S.CreatedOn) AS day,
                    HOUR(S.CreatedOn) AS hour,
                    MINUTE(S.CreatedOn) AS minute,
                    SECOND(S.CreatedOn) AS second,
                    COUNT(N.ID) AS SavingsCount,
                    S.CreatedOn
                FROM 
                    pmi_node_savings S 
                LEFT JOIN 
                    pmi_node N ON (N.ID=S.NodeID)
                LEFT JOIN 
                    pmi_tag TG ON (TG.NodeID = N.ID) 
                $SjoinTable
                WHERE 
                TG.Deleted=0 AND  
                TG.Acknowledged=1 AND 
                N.Deleted=0
                $Swhere
                GROUP BY $Sgroup
EOF;
            //die($sql);
            $SavingsCountArr = $db->get_result($sql);
            
            $eventCountArr = array();
            if(sizeof($eventCount) > 0){
                for ($i = 0; $i < sizeof($eventCount); $i++) {
                    $item = $eventCount[$i];
                    array_push($eventCountArr, array(
                        "content" => "E_".$item["EventCount"],
                        "className" => "timeline-event-tag",
                        "start" => $item["CreatedOn"],
                        "t" => 0,
                        "filterType" => $filterType
                    ));
                }
            }
            //print_r($PolcyCount);
            if(sizeof($PolcyCount) > 0){
                for ($i = 0; $i < sizeof($PolcyCount); $i++) {
                    $item = $PolcyCount[$i];
                    array_push($eventCountArr, array(
                        "content" => "P_".$item["CountPolicy"],
                        "className" => "timeline-policy-tag",
                        "start" => $item["OnDate"],
                        "t" => 2,
                        "filterType" => $filterType
                    ));
                }
            }
            
            if(sizeof($SavingsCountArr) > 0){
                for ($i = 0; $i < sizeof($SavingsCountArr); $i++) {
                    $item = $SavingsCountArr[$i];
                    array_push($eventCountArr, array(
                        "content" => "S_".$item["SavingsCount"],
                        "className" => "timeline-savings-tag",
                        "start" => $item["CreatedOn"],
                        "t" => 1,
                        "filterType" => $filterType
                    ));
                }
            }
            
            $result["eventCount"] = $eventCountArr;
            //print_r($result);
            die(json_encode($result));
        }
        //Get All Gauge
        function GetAllGauge(){
            $result = array();
            try{
                $db = new DBC();
            
                $sql = "SELECT * FROM  pmi_guage WHERE display = 1";
                //echo $sql;
                $result["result"] = $db->get_result($sql);
                $result["status"] = 1;
                
            } catch (Exception $ex) {
                $result["errorMsg"] = $ex->getMessage();
                $result["status"] = 0;
            }
            die(json_encode($result));
        }
        
        //Get dashboard client report (Bar chart and time line)
        function getMainReport(){
            
            $db = new DBC();
            $type = isset($_REQUEST['type']) ? $_REQUEST['type'] : -1;
            $device = isset($_REQUEST['device']) ? $_REQUEST['device'] : 1;
            $param = isset($_REQUEST['param']) ? $_REQUEST['param'] : 0;
            $range = isset($_REQUEST['range']) ? $_REQUEST['range'] : 0;
            $timeRange = isset($_REQUEST['timeRange']) ? $_REQUEST['timeRange'] : 0;
            $startDate = (isset($_REQUEST['startDate']) ? new DateTime($_REQUEST['startDate']): new DateTime());
            $ednDate = (isset($_REQUEST['endDate']) ? new DateTime($_REQUEST['endDate']) : new DateTime());
            //print_r($startDate);
            //die("startDate === 0 ::: ednDate ==== 0");
            $tagTypeID = 0;
            $nodeID = 0;
            if($type == 2){
                $sql = "SELECT TagTypeID,NodeID FROM pmi_tag WHERE ID=$device";
                $getTagDetails = $db->get_result_array($sql);
                if(sizeof($getTagDetails) > 0){
                    $tagTypeID = $getTagDetails[0]["TagTypeID"];
                    $nodeID = $getTagDetails[0]["NodeID"];
				}
            }
            /*if($param != 4){
                $joinTable  = " pmi_node_status NS  LEFT JOIN  pmi_tag_status TS ON (NS.ID = TS.NodeStatusID) LEFT JOIN pmi_node N ON (N.ID=NS.NodeID)  LEFT JOIN pmi_tag T ON (T.NodeID = N.ID) ";
            }
            else $joinTable  = " pmi_node_savings NS LEFT JOIN pmi_node N ON (N.ID=NS.NodeID)  LEFT JOIN pmi_tag T ON (T.NodeID = N.ID) ";*/
			//------------------ADDED here by AK to pull data from different DB depend on range ---------------------------//
            if($param != 4){
				if($range ==0){
					 $joinTable  = " pmi_node_status_5m NS  LEFT JOIN  pmi_tag_status TS ON (NS.ID = TS.NodeStatusID) LEFT JOIN pmi_node N ON (N.ID=NS.NodeID)  LEFT JOIN pmi_tag T ON (T.NodeID = N.ID) ";
					
				}else if($range ==1){
					 $joinTable  = " pmi_node_status_5m NS  LEFT JOIN  pmi_tag_status TS ON (NS.ID = TS.NodeStatusID) LEFT JOIN pmi_node N ON (N.ID=NS.NodeID)  LEFT JOIN pmi_tag T ON (T.NodeID = N.ID) ";
				}
				else if($range ==2){
					 $joinTable  = " pmi_node_status_1hour NS  LEFT JOIN  pmi_tag_status TS ON (NS.ID = TS.NodeStatusID) LEFT JOIN pmi_node N ON (N.ID=NS.NodeID)  LEFT JOIN pmi_tag T ON (T.NodeID = N.ID) ";
				}
				else if($range ==3){
					 $joinTable  = " pmi_node_status_1day NS  LEFT JOIN  pmi_tag_status TS ON (NS.ID = TS.NodeStatusID) LEFT JOIN pmi_node N ON (N.ID=NS.NodeID)  LEFT JOIN pmi_tag T ON (T.NodeID = N.ID) ";
				}
               
            }
            else $joinTable  = " pmi_node_savings NS LEFT JOIN pmi_node N ON (N.ID=NS.NodeID)  LEFT JOIN pmi_tag T ON (T.NodeID = N.ID) ";
			
			$sqlParam   = "";
            $whereCon   = "";
            $groupStr   = "";
            
            //PARAMETRE (kWh,temp,occupency,savings and space utilization)
            if($tagTypeID < 2 && $param == 1){
				//blocked because KWh was always wrong when tyoe was 1 
				$sqlParam = ",AVG((NS.VoltageMaster * NS.DriverCurrent)+(NS.SubVoltage * NS.DriverSubCurrent)) AS AVGVal";//Total AVG
				//if($type == 1) $sqlParam = ",SUM((NS.VoltageMaster * NS.DriverCurrent)+(NS.SubVoltage * NS.DriverSubCurrent)) AS AVGVal";//Total sum
				//else $sqlParam = ",AVG((NS.VoltageMaster * NS.DriverCurrent)+(NS.SubVoltage * NS.DriverSubCurrent)) AS AVGVal";//Total AVG
                
                    //MAX((NS.VoltageMaster * NS.DriverCurrent)+(NS.SubVoltage * NS.DriverSubCurrent)) AS MAX_kWh,
                    //MIN((NS.VoltageMaster * NS.DriverCurrent)+(NS.SubVoltage * NS.DriverSubCurrent)) AS CUREENT_kWh";
            }
            else if($param == 2){ 
                if($tagTypeID > 1) $sqlParam .= ",AVG(TS.Temp) AVGVal";//AVGTemp
                else $sqlParam .= ",AVG(NS.NodeTemp) AVGVal";//AVGTemp
            }
            else if($param == 3) {
				
				$joinTable  = " pmi_event NS  LEFT JOIN pmi_tag T ON (T.ID = NS.TagID) LEFT JOIN pmi_node N ON (N.ID=T.NodeID)   ";
                $sqlParam .= ",COUNT(NS.ID) AVGVal";//SUMOC
                if($type == 1)
                    $whereCon .= "AND T.TagTypeID=".FIXTURE_TYPE_OCCUPANCYSENSOR." ";
                else if($type == 2 && $tagTypeID == 1) 
                    $whereCon .= "AND T.TagTypeID=".FIXTURE_TYPE_OCCUPANCYSENSOR." AND T.NodeID=$nodeID ";
                 else if($type == 3){
                      $whereCon .= "AND T.NodeID=CM.NodeID AND T.TagTypeID=".FIXTURE_TYPE_OCCUPANCYSENSOR." ";
                 }
                //else if($tagTypeID < 2) $whereCon .= "TS.TagID = $device AND"; 
            }
            
            if($type == 2 && $param != 3){
                $whereCon .= "AND T.ID = $device ";
                if($tagTypeID > 1) $whereCon .= "AND TS.TagID = $device "; 
				//else if ($tagTypeID > 1 && $param == 3)$whereCon .= "AND NS.TagID = $device "; 
                //die($whereCon);
            }
            else if($type == 3){
                $joinTable .= " LEFT JOIN pmi_cluster_mapping CM ON (CM.NodeID = N.ID)
                            LEFT JOIN pmi_cluster C ON (C.ID = CM.ClusterID) ";
                $whereCon .= "AND C.ID = $device AND C.Deleted=0 AND CM.Deleted=0 ";
            }
            
            //filter for weekly, monthly ( have both start date & end date )
            if($range !=0){
                $startDate->setTime(0, 0, 0);
                $ednDate->setTime(23, 59, 59);
                $whereCon .= "AND ( NS.CreatedOn BETWEEN '".$startDate->format('Y-m-d H:i:s')."' AND '".$ednDate->format('Y-m-d H:i:s')."' ) ";
            }
            else if($range ==0){
                
                //die("startDate === $startDate ::: ednDate ==== $ednDate");
                $startDate->setTime($timeRange, 0, 0);
                $ednDate->setTime($timeRange, 59, 59);

                $whereCon .= "AND ( NS.CreatedOn BETWEEN '".$startDate->format('Y-m-d H:i:s')."' AND '".$ednDate->format('Y-m-d H:i:s')."' ) "; 
            }
            if($range == 0 ) $groupStr = "GROUP BY `year`,`month`,`day`,`hour`,`minute`";
            else if($range == 1) $groupStr = "GROUP BY `year`,`month`,`day`,`hour`";
            else $groupStr = "GROUP BY `year`,`month`,`day`";
            
            if($param != 4){
                $query = <<<EOF
                    SELECT 
                        YEAR(NS.CreatedOn) AS `year`,
                        MONTH(NS.CreatedOn) AS `month`,
                        DAY(NS.CreatedOn) AS `day`,
                        HOUR(NS.CreatedOn) AS `hour`,
                        MINUTE(NS.CreatedOn) AS `minute`,
                        SECOND(NS.CreatedOn) AS `second`
                        $sqlParam 
                    FROM 
                    $joinTable
                    WHERE 
                        T.Deleted=0 AND 
                        T.Acknowledged=1 AND 
                        N.Deleted=0 AND 
                        N.Acknowledged=1 
                    $whereCon  
                    $groupStr
EOF;
            }
            else{
                $sqlParam .= ",AVG(NS.Savings) AVGVal";//AVGTemp
                $query = <<<EOF
                    SELECT 
                        YEAR(NS.CreatedOn) AS `year`,
                        MONTH(NS.CreatedOn) AS `month`,
                        DAY(NS.CreatedOn) AS `day`,
                        HOUR(NS.CreatedOn) AS `hour`,
                        MINUTE(NS.CreatedOn) AS `minute`,
                        SECOND(NS.CreatedOn) AS `second`
                        $sqlParam 
                    FROM 
                    $joinTable
                    WHERE 
                        T.Deleted=0 AND 
                        T.Acknowledged=1 AND 
                        N.Deleted=0 AND 
                        N.Acknowledged=1 
                    $whereCon  
                    $groupStr
EOF;
            }
			//echo $query;
            $result = $db->get_result($query);
            die(json_encode($result));
        }
        /*=========================== Starts handle server side calls of launchpad.php ===============================*/
        
        /**************************************************************************
         * Function to get pie chart data (KWh consumption , Temperature,Occupency)  
         **************************************************************************/
        
        function getPieChartdata(){
            $result = array();
            $result["status"] = 0;
            try
            {
                $this->logWrite("\n Entering function getPieChartdata in DashboardController");
                
                //init db connection
                $db = new DBC();
                $sql= "";
                $subqry = "";
                
                $hour = $_POST['hour'];
                $daterange = $_POST['dateRange'];
                $sDate = $_POST['sDate'];
                $eDate = $_POST['eDate'];
                
                $subqry = "";
                //filter for weekly, monthly ( have both start date & end date )
                if( isset($sDate) && $sDate != "" && isset($eDate) && $eDate != "" ){
                    
                    $sortSdate = new DateTime($sDate);
                    $sortEdate = new DateTime($eDate);
					
                    $sortSdate->setTime(0, 0, 0);
                    $sortEdate->setTime(23, 59, 59);
                    
                    $subqry = "AND ( NS.CreatedOn BETWEEN '".$sortSdate->format('Y-m-d H:i:s')."' AND '".$sortEdate->format('Y-m-d H:i:s')."' )";
                }
               /* else if(isset($sDate) && $sDate != "" && isset($eDate) && $eDate != "" && $daterange ==0){ // above code had $daterange !=0 not sure why since both conditions will be same 
                    
                    $sortSdate = new DateTime($sDate);
                    $sortEdate = new DateTime($eDate);
                    
                    $sortSdate->setTime($hour, 0, 0);
                    $sortEdate->setTime($hour, 59, 59);
                    
                    $subqry = "AND ( NS.CreatedOn BETWEEN '".$sortSdate->format('Y-m-d H:i:s')."' AND '".$sortEdate->format('Y-m-d H:i:s')."' )"; 
                }*/
                //echo $subqry;
                $kwh = $this->calcKWHConsumption($subqry,$daterange);
                //print_r($kwh);
				//echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>1 \n";
                $temp = $this->calcTemp($subqry,$daterange);
                //print_r($temp);
				//echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>2 \n";
                //die;
                $occupancy = $this->calcOccupancyStatus($subqry,$daterange);
                //print_r($occupancy);
				//echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>3 \n";
                $result["kwhConsumption"] = $kwh;
                $result["temp"] = $temp;
                $result["occupancy"] = $occupancy;
                $result["status"] = 1;
                        
                $this->logWrite("Exit function getPieChartdata in DashboardController");
            }
            catch (Exception $ex) {
                $this->logWrite("\n Error in function calcKWHConsumption() in DashboardController, error: " . $ex->getMessage());
                $result["error"] = $ex->getMessage();
            }  
            die(json_encode($result));
        }
        
        /*******************************************
         * Function to calculate kwh consumption 
         * - looping all clusters, and calculate percenatge of 
         * - each cluster kwh consumption 
         */
        
        function calcKWHConsumption($subqry,$rangee){
            $clusterArr = array();
            try
            {
                
                $this->logWrite("\n Entering function calcKWHConsumption in DashboardController");
                $table = "pmi_node_status_5m"; 
				if ($rangee == 2 )  $table = "pmi_node_status_1hour"; 
				else if ($rangee == 3 )  $table = "pmi_node_status_1day";
				
                //init db connection
                $db = new DBC();
                $sql= "";
                
                $sql =<<<EOF
                SELECT 
                    PCM.ClusterID,
                    PCM.NodeID,
                    SUM( (NS.VoltageMaster * NS.DriverCurrent) +  (NS.SubVoltage * NS.DriverSubCurrent ) ) AS Total_kwh,
                    C.Name
                FROM 
                    pmi_cluster_mapping PCM
                JOIN $table NS 
                    ON (PCM.NodeID = NS.NodeID)
                JOIN pmi_cluster C 
                    ON ( C.ID = PCM.ClusterID)
                WHERE 
                    PCM.Deleted = 0 
                    $subqry
                GROUP BY 
                    PCM.ClusterID 
                ORDER BY PCM.ClusterID ASC
EOF;
                //echo $sql;
                $kwhData = $db->get_result($sql);

                $totalKWHArr = array();
            
                //starts calculateing kwh consumption of each clusters
                if( sizeof($kwhData) > 0){

                    //calculating sum of kwh consumptions by looping all records
                    foreach ($kwhData as $kwh=>$total) {
                       foreach ($total as $id=>$value) {
                          $totalKWHArr['Total_kwh']+=$value;
                        }
                    }

                     //calculating each clusters kwh consumption
                    $index = 1;
                    $labels = array();
                    $datasets = array();
                    $dataVal = array();
                    $bgColor = array();
                    $dlabels = array();
					$totPerc=0;
					
                    foreach ($kwhData as $kwh=>$cluster) {

                        $clusRow = array();
                        $clusterVal = $cluster['Total_kwh'];
                        $clustreName= $cluster['Name'];
                        $clusterPerc = (int)(100 *  ( $clusterVal ) ) / ( $totalKWHArr['Total_kwh'] );
						$totPerc+=round($clusterPerc);
                        array_push($labels, $clustreName.': '.round($clusterPerc).'%');
                        array_push($dataVal, $clusterPerc);
                        array_push($bgColor, '#'.substr(md5($index*2), 0, 6));
                        array_push($dlabels, $clustreName);

                        $index++;
                        //array_push($clusterArr, $clusRow);
                    }
					
					// check for 100% fill, if not add afiller
					if ( $totPerc < 100 )	{
						$pr=100-$totPerc;
						array_push($labels, 'kWh Saved: '.$pr.'%');
                        array_push($dataVal, $pr);
                        array_push($bgColor, '#52c352');
                        array_push($dlabels, 'kWh Saved');
					}   
                    array_push($datasets, 
                        array(
                            "data" => $dataVal,
                            "backgroundColor" => $bgColor,
                            "dlabel" => $dlabels
                        )
                    );
                    array_push($clusterArr, 
                        array(
                            "labels" => $labels,
                            "datasets" => $datasets
                        )
                    );
                }
                
                $this->logWrite("Exit function calcKWHConsumption in DashboardController");
              
            }
             catch (Exception $ex) {
                $this->logWrite("\n Error in function calcKWHConsumption() in DashboardController, error: " . $ex->getMessage());
                throw new $ex;
            }
            return  $clusterArr;
            
        }
        
        
        /*******************************************
         * Function to calculate temperature consumption 
         * - looping all tag, and calculate percenatge of 
         * - each tag temp  
         */
        
        function calcTemp($subqry,$rangee){
            $tempdataArr = array();
            try
            {
                
                $this->logWrite("\n Entering function calcTemp in DashboardController");
                $table = "pmi_node_status_5m"; 
				if ($rangee == 2 )  $table = "pmi_node_status_1hour"; 
				else if ($rangee == 3 )  $table = "pmi_node_status_1day";
                //init db connection
                $db = new DBC();
                $sql= "";
                         
                /*$sql =<<<EOF
                    SELECT  
                        ( NS.NodeTemp) AS Temp1 
                    FROM 
                        pmi_node T 
                        LEFT JOIN pmi_node_status NS ON (T.ID=NS.NodeID) 
                    WHERE
                        T.`Deleted`=0 AND 
                        T.`Acknowledged`=1 AND 
                        T.`Status`=1 
                        $subqry
                    GROUP BY NS.ID
EOF;*/
//LEFT JOIN pmi_tag_status TS ON (TS.NodeStatusID = NS.ID) 
				/*$tempData = array();
                $tempData = $db->get_result("SELECT ( NS.NodeTemp) AS Temp1 FROM pmi_node T LEFT JOIN pmi_node_status NS ON (T.ID=NS.NodeID) WHERE T.`Deleted`=0  GROUP BY NS.ID limit 500"); */
				//echo "11111";
                //print_r($tempData);
				//die;
                $sql =<<<EOF
                    SELECT  
                        AVG(NS.NodeTemp) TempAvg
                    FROM 
                        pmi_node T 
                        LEFT JOIN $table NS ON (T.ID=NS.NodeID) 
                    WHERE
                        T.Deleted=0 AND 
                        T.Acknowledged=1 AND 
                        T.`Status`=1 
                        $subqry
EOF;
				//LEFT JOIN pmi_tag_status TS ON (TS.NodeStatusID = NS.ID) 
				//echo "<<<<<<<<<<";
                //die($sql);
                $TempAvg = $db->get_single_result($sql);
                //echo ">>>>> $TempAvg \n";
                $totalTempArr = array();
                $totalSum = 0;
                
				/*if(sizeof($tempData) > 0){
                    if(sizeof($tempData) > 0){
                            for ($j = 0; $j < count($tempData); $j++) {
                                $curVal = $tempData[$j]["Temp"];
                                    $tCount++;
                                    $tTotal += $curVal;
                                }   
                            }
                        //echo "tTotal == $tTotal ::::: tCount == $tCount \n";
                        $avgTepm = $tTotal/$tCount;
                        $totalSum += $avgTepm;
                        //echo "avgTepm == $avgTepm ::: totalSum == $totalSum \n";
                    $totalTempArr["Temperature"] = $avgTepm;
				}*/
                
                $labels = array();
                $datasets = array();
                $dataVal = array();
                $bgColor = array();
                $dlabels = array();
                
				if($TempAvg > 0){
                //if(sizeof($totalTempArr) > 0){
                   // foreach ($totalTempArr as $key => $value) {
                        //$tempRow = array();
                        //$preVal = (int)(100 *  ( $value ) ) / ($totalSum); 
						$preVal = (int)(100 *  ( (int)$TempAvg ) ) / ((int)$TempAvg); 
                        $severityName = "Temperature";

                        array_push($labels, $severityName.': '.round($preVal).'%');
                        array_push($dataVal, $preVal);
                       // array_push($bgColor, "rgba(52, 168, 83, 1)");	//this is it ? temp pie chart color
						array_push($bgColor, "rgba(56, 242, 87, 1)");
                        array_push($dlabels, $severityName);
                    //}
                    array_push($datasets, 
                        array(
                            "data" => $dataVal,
                            "backgroundColor" => $bgColor,
                            "dlabel" => $dlabels
                        )
                    );
                    array_push($tempdataArr, 
                        array(
                            "labels" => $labels,
                            "datasets" => $datasets,
                            "TempAVG" => (empty($TempAvg) ? 0 : (int)$TempAvg)
                        )
                    );
                }
				
				
                //print_r($tempdataArr);
                
                $this->logWrite("Exit function calcTemp in DashboardController");
              
            }
             catch (Exception $ex) {
                $this->logWrite("\n Error in function calcTemp() in DashboardController, error: " . $ex->getMessage());
                throw new $ex;
            }
            return  $tempdataArr;
            
        }
        
        /*******************************************
         * Function to calculate occupancy consumption 
         * - looping all tag, and calculate percenatge of 
         * - each tag occupancy  
         */
        
        function calcOccupancyStatus($subqry,$rangee){
           $clusterArr = array();
            try
            {
                
                $this->logWrite("\n Entering function calcOccupancyStatus in DashboardController");
                $table = "pmi_node_status_5m"; 
				if ($rangee == 2 )  $table = "pmi_node_status_1hour"; 
				else if ($rangee == 3 )  $table = "pmi_node_status_1day";
                //init db connection
                $db = new DBC();
                $sql= "";
				$eventID =  EV_MOTION;
                
                $sql =<<<EOF
                SELECT 
                    COUNT(NS.ID) AS OccStatus ,C.Name
                FROM 
                    pmi_cluster C 
                    LEFT JOIN pmi_cluster_mapping CM ON (C.ID=CM.ClusterID)
                    LEFT JOIN pmi_node N ON (CM.NodeID = N.ID) 
                    LEFT JOIN pmi_tag T ON(N.ID = T.NodeID) 
					LEFT JOIN pmi_event NS ON (NS.TagID=T.ID)
                WHERE
                    T.Deleted=0 AND 
                    T.Acknowledged=1 AND 
                    T.`Status`=1 AND 
                    N.Deleted=0 AND 
                    N.Acknowledged=1 AND 
                    N.`Status`=1 AND 
                    T.TagTypeID=5 AND 
                    C.Deleted=0 AND 
                    CM.Deleted = 0 AND
					NS.EventID=$eventID 
                    $subqry
                GROUP BY C.ID
EOF;
                //echo $sql;
                $OCCData = $db->get_result($sql);
                $totalOCCArr = 0;
                //print_r($OCCData);
                //starts calculateing kwh consumption of each clusters
                if( sizeof($OCCData) > 0){

                    //calculating sum of kwh consumptions by looping all records
                    for ($i = 0; $i < count($OCCData); $i++) {
                        $occStatus = $OCCData[$i]["OccStatus"];
                        //echo " occStatus ::: $occStatus \n";
                        $totalOCCArr += $occStatus;
                    }
                    //die(">>>> $totalOCCArr");
                    //
                    //calculating each clusters kwh consumption
                    $index = 1;
                    $labels = array();
                    $datasets = array();
                    $dataVal = array();
                    $bgColor = array();
                    $dlabels = array();
					$totPerc=0;
					
                    foreach ($OCCData as $kwh=>$cluster) {

                        $clusRow = array();
                        $clusterVal = $cluster['OccStatus'];
                        $clustreName= $cluster['Name'];
                        $clusterPerc = (int)(100 *  ( $clusterVal ) ) / ( $totalOCCArr );
						$totPerc+=round($clusterPerc);
                        array_push($labels, $clustreName.': '.round($clusterPerc).'%');
                        array_push($dataVal, $clusterPerc);
                        array_push($bgColor, '#'.substr(md5($index*10), 0, 6)); // oc color
                        array_push($dlabels, $clustreName);

                        $index++;
                        //array_push($clusterArr, $clusRow);
                    }
					
					
					// check for 100% fill, if not add afiller
					if ( $totPerc < 100 )	{
						$pr=100-$totPerc;
						array_push($labels, 'Space Not Occupied : '.$pr.'%');
                        array_push($dataVal, $pr);
                        array_push($bgColor, '#52c352');
                        array_push($dlabels, 'Scape Not Occupied');
					}     
                    array_push($datasets, 
                        array(
                            "data" => $dataVal,
                            "backgroundColor" => $bgColor,
                            "dlabel" => $dlabels
                        )
                    );
                    array_push($clusterArr, 
                        array(
                            "labels" => $labels,
                            "datasets" => $datasets
                        )
                    );
                }
                
                $this->logWrite("Exit function calcKWHConsumption in DashboardController");
              
            }
             catch (Exception $ex) {
                $this->logWrite("\n Error in function calcKWHConsumption() in DashboardController, error: " . $ex->getMessage());
                throw new $ex;
            }
            //print_r($clusterArr);
            return  $clusterArr;
        }
        
        /*****************************************
         * Function to get  daily report data
         *****************************************/
        
        function getDailyReportBarChartdata(){
            $result = array();
            $result["status"] = 0;
            $db =  new DBC();
            try
            {
                $this->logWrite("\n Entering function getDailyReportBarChartdata in DashboardController");
                
                //init db connection
                $db = new DBC();
                $sql= "";
                $subqry = "";
                
                $hour = $_POST['hour'];
                $daterange = $_POST['dateRange'];
                $sDate = $_POST['sDate'];
                $eDate = $_POST['eDate'];
				$eventID =  EV_MOTION;
                
                $subqry = "";
                //filter for weekly, monthly ( have both start date & end date )
                if( isset($sDate) && $sDate != "" && isset($eDate) && $eDate != "" && $daterange !=0){
                    
                    $sortSdate = new DateTime($sDate);
                    $sortEdate = new DateTime($eDate);
                    $sortSdate->setTime(0, 0, 0);
                    $sortEdate->setTime(23, 59, 59);
                    
                    $subqry = "AND ( NS.CreatedOn BETWEEN '".$sortSdate->format('Y-m-d H:i:s')."' AND '".$sortEdate->format('Y-m-d H:i:s')."' )";
				}
                else if(isset($sDate) && $sDate != "" && isset($eDate) && $eDate != "" && $daterange ==0){
                    
                    $sortSdate = new DateTime($sDate);
                    $sortEdate = new DateTime($eDate);
                    
                    $sortSdate->setTime($hour, 0, 0);
                    $sortEdate->setTime($hour, 59, 59); 
                    
                    $subqry = "AND ( NS.CreatedOn BETWEEN'".$sortSdate->format('Y-m-d H:i:s')."' AND '".$sortEdate->format('Y-m-d H:i:s')."' )"; 
                }
                $sql =<<<EOF
                SELECT 
                    AVG((NS.VoltageMaster * NS.DriverCurrent) + (NS.SubVoltage * NS.DriverSubCurrent)) AS Kwh,
                    AVG(NS.NodeTemp) AS Temp
                FROM 
                    pmi_node T 
                    LEFT JOIN pmi_node_status NS ON (T.ID=NS.NodeID)
                    LEFT JOIN pmi_tag_status TS ON (TS.NodeStatusID = NS.ID) 
                WHERE
                    T.Deleted=0 AND 
                    T.Acknowledged=1 AND 
                    T.`Status`=1 
                    $subqry
EOF;
                //die($sql);
                $dailyReport = $db->get_result($sql);
				
                $sql =<<<EOF
                SELECT 
                    AVG(NS.Savings) AS Savings
                FROM 
                    pmi_tag T 
					LEFT JOIN pmi_node_savings NS ON (NS.NodeID=T.NodeID)
                WHERE
                    T.Deleted=0 AND 
                    T.Acknowledged=1 AND 
                    T.`Status`=1 
                    $subqry
EOF;
                //die($sql); 
                $dailyReportSavings = $db->get_result($sql);
				//print_r(dailyReportSavings);
				
				
                $sql =<<<EOF
                SELECT 
                    COUNT(NS.ID) AS OccCount
                FROM 
                    pmi_tag T 
					LEFT JOIN pmi_event NS ON (NS.TagID=T.ID)
                WHERE
                    T.Deleted=0 AND 
                    T.Acknowledged=1 AND 
                    T.`Status`=1 AND 
					NS.EventID=$eventID 
                    $subqry
EOF;
                //die($sql);
                $dailyReportOC = $db->get_result($sql);
				
                //print_r($dailyReport);
                $dailyDataSet = array();
                $dailyDataSet["label"] = array("Occupancy", "Temperture", "KW Consumption", "Savings");
                $dailyDataSet["backgroundColor"] = array("#4179f8", "#4179f8", "#4179f8", "#4179f8");
                $dailyDataSet["borderColor"] = array("#4179f8", "#4179f8", "#4179f8", "#4179f8");
                
                $kwh = 0;
                $temp = 0;
                $occ = 0;
                $savings = 0;
                
                if(sizeof($dailyReport) > 0){
                    
                    $kwh = (empty($dailyReport[0]["Kwh"]) ? 0: $dailyReport[0]["Kwh"]);
                    $temp = (empty($dailyReport[0]["Temp"]) ? 0: $dailyReport[0]["Temp"]);
                    //$dailyDataSet["data"] = array((int)$occ,(int)$temp,(int)$kwh,(int)$savings);
                }
                //else $dailyDataSet["data"] = array("0", "0", "0", "0");
				
				//print_r($dailyReportOC);
				if(sizeof($dailyReportOC) > 0){
					$occ = (empty($dailyReportOC[0]["OccCount"]) ? 0: $dailyReportOC[0]["OccCount"]);
                        //$dailyDataSet["data"][0] = $occ;
				}
				
				if(sizeof($dailyReportSavings) > 0){
					$savings = (empty($dailyReportSavings[0]["Savings"]) ? 0: $dailyReportSavings[0]["Savings"]);
                    //$dailyDataSet["data"][3] = (int)$savings;
				}
                $tTotal = $kwh + $temp + $occ + $savings; 
                $kwhPer = ($kwh/$tTotal) * 100;
                $tempPer = ($temp/$tTotal) * 100;
                $occPer = ($occ/$tTotal) * 100;
                $savingsPer = ($savings/$tTotal) * 100;
                $dailyDataSet["data"] = array($occ,$temp,$kwh,$savings);
                $dailyDataSet["dataPer"] = array($occPer,$tempPer,$kwhPer,$savingsPer);
                //$dailyDataSet["data"] = array("250", "50", "20", "10");
                $result["result"] = $dailyDataSet;
                $result["status"] = 1;
                        
                $this->logWrite("Exit function getDailyReportBarChartdata in DashboardController");
            }
            catch (Exception $ex) {
                $this->logWrite("\n Error in function getDailyReportBarChartdata() in DashboardController, error: " . $ex->getMessage());
                $result["error"] = $ex->getMessage();
            }  
            die(json_encode($result));
        }
		
        
        
        //Get event timeline Data
        function getTimeLineData(){
            
            $db = new DBC();
            $sDate = new DateTime($_REQUEST['sDate']);
            $eDate = new DateTime($_REQUEST['eDate']);
            $selectType = $_REQUEST["selectType"];
            $devID = isset($_REQUEST['devID']) ? $_REQUEST['devID'] : 0;   
            $state = $_REQUEST["state"];      
            $rangeID = $_REQUEST["range"];     
            $type = $_REQUEST["type"];   
            $filterType = $_REQUEST["filterType"];
            
            $Ewhere = "";
            $EjoinTable = "";
            $Egroup = "";
            
            $Ewhere = "";
            $EjoinTable = "";
            $Egroup = "";
            
            $Pwhere = "";
            $Pgroup = "";
            
            $Swhere = "";
            $Sgroup = "";
            $SjoinTable = "";
            
            $result =  array(); 
            
            
            if($state == 1){
                if($filterType == 0){
                    //$sDate->sub(new DateInterval('PT10H30S'));
                    $sDate->sub(new DateInterval('PT5S'));
                }
                else if($filterType == 1){
                    $sDate->setTime($sDate->format("H"), $sDate->format("i"), 0);
                    $eDate->setTime($eDate->format("H"), $eDate->format("i"), 59);
                }
                else if($filterType == 2){
                    $sDate->setTime($sDate->format("H"), 0, 0);
                    $eDate->setTime($eDate->format("H"), 59, 59);
                }
                
                else if($filterType == 3){
                    $sDate->setTime(0, 0, 0);
                    $eDate->setTime(23, 59, 59);
                }
                $Ewhere = " ( E.CreatedOn BETWEEN '".$sDate->format('Y-m-d H:i:s')."' AND '".$eDate->format('Y-m-d H:i:s')."' )"; 
                $Pwhere = " ( PT.OnDate BETWEEN'".$sDate->format('Y-m-d H:i:s')."' AND  '".$eDate->format('Y-m-d H:i:s')."')";
                $Swhere = " AND  ( S.CreatedOn BETWEEN '".$sDate->format('Y-m-d H:i:s')."' AND '".$eDate->format('Y-m-d H:i:s')."')";
            }
            else if($state == 2){
                $Ewhere = " ( E.CreatedOn >= NOW() - INTERVAL 5  SECOND AND E.CreatedOn <= NOW())";
                $Pwhere = " ( PT.OnDate >= NOW() - INTERVAL 5  SECOND AND PT.OnDate <= NOW())";
                $Swhere = " AND ( S.CreatedOn >= NOW() - INTERVAL 5  SECOND AND S.CreatedOn <= NOW())";
            }
            
            if($selectType == 1){
				$EjoinTable .= "LEFT JOIN pmi_tag T ON (E.NodeID = T.NodeID) ";
                $Ewhere .= " AND T.Deleted=0 AND T.Acknowledged=1 AND T.TagTypeID=".FIXTURE_TYPE_NODE;
				$Swhere .= " AND TG.TagTypeID=".FIXTURE_TYPE_NODE;
                $Pwhere .= " AND T.TagTypeID=".FIXTURE_TYPE_NODE;
			}
            else if($selectType == 2){
                $EjoinTable .= "LEFT JOIN pmi_tag T ON (E.NodeID = T.NodeID) ";
                $Ewhere .= " AND T.ID=$devID AND T.Deleted=0 AND T.Acknowledged=1 ";
                $Pwhere .= " AND T.ID=$devID";
                $SjoinTable = "";
                $Swhere .= " AND TG.ID=$devID";
            }
            else if($selectType == 3){
                $EjoinTable .= "LEFT JOIN pmi_cluster_mapping CM ON (E.NodeID = CM.NodeID) ";
                $EjoinTable .= "LEFT JOIN pmi_cluster C ON (CM.ClusterID=C.ID) ";
                $Ewhere .= " AND C.ID=$devID AND CM.Deleted=0 AND C.Deleted=0";
                $Pwhere .= " AND C.ID=$devID";
                
                $SjoinTable .= "LEFT JOIN pmi_cluster_mapping CM ON (TG.NodeID = CM.NodeID) ";
                $SjoinTable .= "LEFT JOIN pmi_cluster C ON (CM.ClusterID=C.ID) ";
                $Swhere .= " AND C.ID=$devID AND C.Deleted=0";
            }
            if($type == -1 || $type == 0){
            $sql =<<<EOF
                    SELECT 
                        E.Message,
                        E.CreatedOn 
                    FROM 
                        pmi_event E 
                        $EjoinTable
                    WHERE 
                        $Ewhere
EOF;
            //die($sql);
			//echo $sql;
            $eventCount = $db->get_result($sql);
            }
            else $eventCount = array();
            if($type == -1 || $type == 2){
                $sql =<<<EOF
                    SELECT 
                        N.Name,
                        PT.Message,
                        PT.OnDate 
                    FROM 
                        pmi_policy_timeline PT
                        LEFT JOIN 
                            pmi_policy P ON (P.ID=PT.PolicyID)
                        LEFT JOIN 
                            pmi_policy_mapping PM ON (PM.PolicyID = P.ID) 
                        LEFT JOIN 
                            pmi_cluster_mapping CM ON (CM.ClusterID = PM.ClusterID) 
                        LEFT JOIN 
                            pmi_cluster C ON (C.ID = CM.ClusterID)
                        LEFT JOIN 
                            pmi_node N ON (N.ID=CM.NodeID) 
                        LEFT JOIN 
                            pmi_tag T ON (T.NodeID=N.ID) 
                    WHERE
                        P.Deleted=0 AND
                        PM.Deleted=0 AND 
                        C.Deleted=0 AND
                        CM.Deleted=0 AND 
                        T.Deleted=0  AND
                        $Pwhere
EOF;

                //die($sql);
				//echo $sql;
                $PolcyCount = $db->get_result($sql);
            }
            else $PolcyCount = array();
            //print_r($PolcyCount);
            if($type == -1 || $type == 1){
            $sql =<<<EOF
                SELECT 
                    N.Name,
                    S.Savings,
                    S.CreatedOn
                FROM 
                    pmi_node_savings S 
                LEFT JOIN 
                    pmi_node N ON (N.ID=S.NodeID)
                LEFT JOIN 
                    pmi_tag TG ON (TG.NodeID = N.ID) 
                $SjoinTable
                WHERE 
                TG.Deleted=0 AND  
                TG.Acknowledged=1 AND 
                N.Deleted=0
                $Swhere
EOF;
            //die($sql); 
            $SavingsCountArr = $db->get_result($sql);
            }
            else $SavingsCountArr = array();
            
            $timelineData = array();
            if(sizeof($eventCount) > 0){
                for ($i = 0; $i < sizeof($eventCount); $i++) {
                    $item = $eventCount[$i];
                    $dt = new DateTime($item["CreatedOn"]);
                    array_push($timelineData, array(
                        "content" => $item["Message"]." @ ".$dt->format("Y-m-d h:i:s a"),
                        "type" => 0
                    ));
                }
            }
            //print_r($PolcyCount);
            if(sizeof($PolcyCount) > 0){
                for ($i = 0; $i < sizeof($PolcyCount); $i++) {
                    $item = $PolcyCount[$i];
                    array_push($timelineData, array(
                        "content" => $item["Name"]." : ". $item["Message"],
                        "type" => 2
                    ));
                }
            }
            
            if(sizeof($SavingsCountArr) > 0){
                for ($i = 0; $i < sizeof($SavingsCountArr); $i++) {
                    $item = $SavingsCountArr[$i];
                    $dt = new DateTime($item["CreatedOn"]);
                    array_push($timelineData, array(
                        "content" => $item["Name"]." savings= ". $item["Savings"]."% @ ".$dt->format("Y-m-d h:i:s a"),
                        "type" => 1
                    ));
                }
            }
            
            $result["data"] = $timelineData;
            
            die(json_encode($result));
        }
       

		function GetAlertMessages() {
			$db = new DBC();
			$db->autocommit(false);
			try {

				$sql =<<<EOF
					SELECT
						ID,
						Status,
						ResponseMessages,
						Type
					FROM pmi_response_messages
					Where Deleted = 0
EOF;
				//die($sql);
				$guageVal = $db->get_result($sql);
				//print_r($guageVal);
				if(sizeof($guageVal) > 0)
				{
					sleep(1);
					for($i=0;$i < sizeof($guageVal);$i++)
					{
						$updateQry = "Update pmi_response_messages set Deleted = 1 where ID = ".$guageVal[$i]['ID'];
						$db->query($updateQry);
					}
				}
				$db->commit();

			} catch (Exception $exc) {
				echo $exc->getMessage();
				 $db->rollback();
				$result['status'] = 0;
				die(json_encode($result));
			}
			die(json_encode($guageVal));
			$db->close();
		}


    }
?>
