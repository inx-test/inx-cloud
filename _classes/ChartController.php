<?php
    include_once("config.php");
    
    class ChartController extends ControllerBase 
    {	
        # Constructor Method 
        function __constructor(){ }
        
        function GetDeviceChartValue(){
            $db = new DBC();
            $timeRange = $_REQUEST["timeRange"];
            if($_REQUEST['devices']!=0){
                $devices = $_REQUEST['devices'];//implode(",",$_REQUEST['devices']);
            }
            else{
                $devices = "0";
            }
            //echo $devices;
            //print_r($_REQUEST['devices']);
            $startDateArr = new DateTime($_REQUEST["dateBegin"]);
            $endDateArr = new DateTime($_REQUEST["dateEnd"]);
            //$hrs1 = date('H', strtotime($_REQUEST["dateBegin"]));
            //$hrs2 = date('H', strtotime($_REQUEST["dateEnd"]));
            //$startDateArr->setTime($hrs1, 00, 00);
            //$endDateArr->setTime($hrs2, 59, 59);
            $startDate = $startDateArr->format('Y-m-d H:i:s');
            $endDate = $endDateArr->format('Y-m-d H:i:s');
            
            $tableName = "pmi_node_status NS ";
            $tableName .= "LEFT JOIN pmi_tag_status TS ON (NS.ID = TS.NodeStatusID) ";
            $tableName .= "LEFT JOIN pmi_tag T ON (T.ID = TS.TagID)";
            $sqlWhere = "";
            $sqlGroup = "";
            $sqlOrder = "";
            $addSelection = "";
            
            if($timeRange == "5m" || $timeRange == "1m"){
                $addSelection = " YEAR(NS.DevDateTime) AS year,MONTH(NS.DevDateTime) AS month,DAY(NS.DevDateTime) AS day,HOUR(NS.DevDateTime) AS hour,MINUTE(NS.DevDateTime) AS minute,SECOND(NS.DevDateTime) AS second, ";
                $sqlGroup = " GROUP BY NS.NodeID,year,month,day,hour,minute,second ";
                $sqlOrder = " ORDER BY NS.NodeID,year, month, day, hour,minute,second ";
            }
            else if($timeRange == "30m" || $timeRange == "1h"){
                $addSelection = " YEAR(NS.DevDateTime) AS year,MONTH(NS.DevDateTime) AS month,DAY(NS.DevDateTime) AS day,HOUR(NS.DevDateTime) AS hour,MINUTE(NS.DevDateTime) AS minute, ";
                $sqlGroup = " GROUP BY NodeID,year,month,day,hour,minute ";
                $sqlOrder = " ORDER BY NodeID,year, month, day, hour,minute ";
            }
            else if($timeRange == "12h" || $timeRange == "24h"){
                $addSelection = " YEAR(NS.DevDateTime) AS year,MONTH(NS.DevDateTime) AS month,DAY(NS.DevDateTime) AS day,HOUR(NS.DevDateTime) AS hour,TRUNCATE(MINUTE(NS.DevDateTime)/5,0) AS minute, ";
                $sqlGroup = " GROUP BY NS.NodeID,year,month,day,hour,minute ";
                $sqlOrder = " ORDER BY NS.NodeID,year, month, day, hour,minute ";
            }
            else if($timeRange == "7d"){                
                $addSelection = " YEAR(NS.DevDateTime) AS year,MONTH(NS.DevDateTime) AS month,DAY(NS.DevDateTime) AS day,HOUR(NS.DevDateTime) AS hour, ";
                $sqlGroup = " GROUP BY NS.NodeID,year,month,day,hour ";
                $sqlOrder = " ORDER BY NS.NodeID,year, month, day, hour ";
                //$tableName = "pmi_hourly_log";
            }
            else if($timeRange == "30d" ){
                $addSelection = " YEAR(NS.DevDateTime) AS year,MONTH(NS.DevDateTime) AS month,DAY(NS.DevDateTime) AS day,TRUNCATE(HOUR(NS.DevDateTime)/4,0) AS hour, ";
                $sqlGroup = " GROUP BY NS.NodeID,year,month,day,hour ";
                $sqlOrder = " ORDER BY NS.NodeID,year, month, day, hour ";
                //$tableName = "pmi_hourly_log";
            }
            else{
                $addSelection = " YEAR(NS.DevDateTime) AS year,MONTH(NS.DevDateTime) AS month,DAY(NS.DevDateTime) AS day, ";
                $sqlGroup = " GROUP BY NS.NodeID,year,month,day ";
                $sqlOrder = " ORDER BY NS.NodeID,year, month, day";
                //$tableName = "pmi_daily_log";
            }
            
            if($startDate == $endDate){
                $sqlWhere = " WHERE NS.DevDateTime >= '" .$startDate. "' AND NS.DevDateTime <= '" .$endDate . "' AND NS.NodeID IN (".$devices.")"; 
            }
            else{
                $sqlWhere = " WHERE NS.DevDateTime >= '" . $startDate . "' AND NS.DevDateTime <= '" . $endDate . "' AND NS.NodeID IN (".$devices.")"; 
            }
            $sql = "SELECT NS.NodeID AS deviceID,". $addSelection . "AVG(NS.VoltageMaster) AS VoltageMaster,"
                    . "AVG(NS.DriverCurrent) AS DriverCurrent,"
                    . "AVG(NS.SubVoltage) AS SubVoltage,"
                    . "AVG(NS.DriverSubCurrent) AS DriverSubCurrent,"
                    . "AVG(TS.Temp + NS.NodeTemp) AS Temp,"
                    . "AVG(IF(T.TagTypeID=5,TS.`Status`,0)) AS OCStatus "
                    . "FROM ".$tableName . $sqlWhere . $sqlGroup . $sqlOrder;
            //echo $sql;die;
            $chartData = $db->get_result($sql);
            $result['result'] = $chartData;
            die(json_encode($result));
        }
        
        
        //GET TAG LIVE STATUS
        function getTagLiveStatus(){
            $db = new DBC();
            $rtnResult = array();
            //$status = $_REQUEST["status"];
            $type = $_REQUEST["type"];
            $nodeID = isset($_REQUEST["nodeID"]) ? $_REQUEST["nodeID"] : 0;
            $clusterID = isset($_REQUEST["clusterID"]) ? $_REQUEST["clusterID"] : 0;
            $unit = isset($_REQUEST["unit"]) ? $_REQUEST["unit"] : 0;
            
            $date = new DateTime();
            $startDate = $date->format('Y-m-d H:i:s');
            $date->sub(new DateInterval('PT5S'));
            $endDate = $date->format('Y-m-d H:i:s');
            
            $where = "NS.DevDateTime BETWEEN '".$endDate."' AND '".$startDate."' ";
            $joinTb = "";
            $param = "";
            $nodeSql = "";
            
            if($unit == 1){
                $param = " AVG(NS.VoltageMaster* NS.DriverCurrent) AS Driver_A_KW";
                $param .= " , AVG(NS.SubVoltage* NS.DriverSubCurrent) AS Driver_B_KW";
            } 
            else if($unit == 2) $param = " AVG(NS.NodeTemp+NS.OCTemp+NS.DriverTemp) AS Temperature";
            else if($unit == 3) $param = " NS.OCStatus AS Motion";
            
            if($type == 2){//SELECT NODE
                //$joinTb = "LEFT JOIN pmi_tag T ON (T.ID = NS.NodeID) LEFT JOIN pmi_node N ON (N.ID = T.NodeID)";
                $where .= "AND NS.NodeID = ".$nodeID;
                $nodeSql ="SELECT N.ID,N.Name AS NodeName,T.Name TagName FROM pmi_node N LEFT JOIN pmi_tag T ON (T.NodeID = N.ID AND T.TagTypeID=".FIXTURE_TYPE_OCCUPANCYSENSOR." AND T.Status=1 AND T.Acknowledged=1) WHERE N.ID=".$nodeID;
            }
            else if($type == 3){//CLUSTER SELECT
                $joinTb = "LEFT JOIN pmi_tag T ON (T.ID = NS.NodeID) LEFT JOIN pmi_node N ON (N.ID = T.NodeID) LEFT JOIN pmi_cluster_mapping C ON (C.NodeID = N.ID)";
                $where .= "AND C.ClusterID = ".$clusterID." AND C.Deleted=0";
                
                $nodeSql ="SELECT N.ID,N.Name AS NodeName,T.Name AS TagName FROM pmi_node N LEFT JOIN pmi_cluster_mapping C ON (C.NodeID = N.ID) LEFT JOIN pmi_tag T ON (T.NodeID = N.ID AND T.TagTypeID=".FIXTURE_TYPE_OCCUPANCYSENSOR." AND T.Status=1 AND T.Acknowledged=1) WHERE C.ClusterID = ".$clusterID." AND C.Deleted=0";
            }
            
            
            $sql =<<<EOF
                SELECT 
                    $param
                    FROM pmi_node_status NS 
                        $joinTb 
                    WHERE 
                        $where
EOF;
            
            /*,
                NS.DevDateTime,
                YEAR(NS.DevDateTime) AS year,
                MONTH(NS.DevDateTime) AS month,
                DAY(NS.DevDateTime) AS day,
                HOUR(NS.DevDateTime) AS hour,
                MINUTE(NS.DevDateTime) AS minute,
                SECOND(NS.DevDateTime) AS second 
            */
            //die($sql);
            $result = $db->get_result($sql);
            $nodeResult = array();
            if($nodeSql != "") $nodeResult = $db->get_result($nodeSql);
            
            if(sizeof($result) > 0){
                $rtnResult["status"] = 1;
                $rtnResult["result"] = $result;
            }
            else $rtnResult["status"] = 0;
            $rtnResult["nodeResult"] = $nodeResult;
            
            die(json_encode($rtnResult));
        }
    }
?>