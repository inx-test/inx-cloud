<?php
	include_once("config.php");

	class ClientController extends ControllerBase
	{
		function __constructor()
		{

		}

		public function fetchAllClient()
		{
			$db = new \DBC(DB_HOST,DB_USER,DB_PASSWD,DB_NAME_CLOUD);                
			$loggedUser = $_REQUEST['loggedUser'];
			$imgPath = array();
			
			$where = "deleted=0";
			if($loggedUser != ""){
				$where .= " and id!='$loggedUser' and added_by='$loggedUser'";
			}
			
			$sql = "select * from pmi_client where $where";
			$clientInfo = $db->get_result($sql);
			$i=0;
			
			/*foreach($clientInfo as $client){
				$userID = $client['id'];
				$imgPath[$i] = "img/profiles/profile-".$client['id'].".png";
				if(!file_exists($imgPath[$i])) $imgPath[$i] = "img/profiles/no-image.jpg";
				$i++;
			}*/
			
			if($clientInfo){
				$result['status']   = '1';
				$result['clientInfo'] = $clientInfo;
				$result['imagePath'] = $imgPath;
				die(json_encode($result));
			}
			else{
				$result['status']   = '0';
				die(json_encode($result));
			}
		}

		public function saveClientInfo()
		{			
			$db = new \DBC(DB_HOST,DB_USER,DB_PASSWD,DB_NAME_CLOUD);
			
			$loggedUserId       = $_SESSION['LOGGED_USER_ID'];
			$firstName          = $_REQUEST['firstname'];
			$lastName           = $_REQUEST['lastname'];
			$email              = $_REQUEST['email'];
			$contactno          = $_REQUEST['contact'];
			$clientID             = $_REQUEST["clientID"];
			$sql = '';

			if($clientID  != '') $sql = "and id not in ('$clientID')";
			
			$sql1 = "select id from pmi_client where  email = '$email' and deleted=0 $sql";
			$clientExist = $db->get_single_result($sql1);
			
			if($clientExist){
				$result['status'] = '0';
				$result['message'] = "Email Already Exist";
				die(json_encode($result));
			}
			
			$dbrec = array();
			$dbrec["firstname"] = mysqli_real_escape_string($db,$firstName);
			$dbrec["lastname"]  = mysqli_real_escape_string($db,$lastName);
			$dbrec["email"]     = $email;
			$dbrec["phone"]     = $contactno;
			$dbrec["added_by"]  = $loggedUserId;

			if($clientID == ''){			
				$saveClient = $db->insert_query($dbrec , 'pmi_client');
			}
			else {
				$where["id"] = $clientID;
                $saveClient = $db->update_query($dbrec , 'pmi_client',$where);
			}
			
			if($saveClient){
				$result['status'] = '1';
				$result['message'] = ($clientID == '')?"Successfully Added Client": "Successfully updated user";
				die(json_encode($result));
			}
			else{
				$result['status'] = '0';
				$result['message'] = ($clientID == '')?"Failed to Add Client":"Failed to update user";
				die(json_encode($result));
			}           
		}

		//Fetch User data by user ID
		function fetchClientData(){
			$db = new \DBC(DB_HOST,DB_USER,DB_PASSWD,DB_NAME_CLOUD);			
			$userId = $_REQUEST['clientid'];			
			$sql = "SELECT firstname,lastname,email,phone FROM pmi_client WHERE id = '$userId'";
			$existClientdata = $db->get_result($sql);			
			if($existClientdata){
				$result["status"] = '1';
				$result["result"] = $existClientdata;
				die(json_encode($result));
			}
			else{
				$result["status"] = '0';
				die(json_encode($result));
			}			
		}
		
		//Remove User
		function removeClientInfo()
		{
			$db = new \DBC(DB_HOST,DB_USER,DB_PASSWD,DB_NAME_CLOUD);
			
			$clientId = $_REQUEST['clientid'];
			$loggedUser = $_SESSION['LOGGED_USER_ID'];
			
			$dbrec = array();
			$where = array();
			$dbrec["deleted"]       = 1;
			$dbrec["deleted_by"]    = $loggedUser;
			$where["id"]            = $clientId;
			
			$removeUser = $db->update_query($dbrec , 'pmi_client',$where);
			
			if($removeUser)
			{
				$result['status'] = '1';
				$result['id'] = $clientId;
				$result['message'] = "Successfully Removed Client";
				die(json_encode($result));
			}
			else{
				$result['status'] = '0';
				$result['message'] = "Failed to Remove Client";
				die(json_encode($result));
			}
		} 
	}
?>
