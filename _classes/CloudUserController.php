<?php
				
    include_once("config.php");
    //require_once("Mail.php");
    
    class CloudUserController extends ControllerBase 
    {	
            # Constructor Method 
            function __constructor(){
            }               
            
            
            //Fetch User Informations
            function fetchUserDetails()
            {
                $db = new \DBC(DB_HOST,DB_USER,DB_PASSWD,DB_NAME_CLOUD);
                $userId = $_SESSION['LOGGED_USER_ID'];
                
                $sql = "select * from pmi_user where id='$userId'";
                $userInfo = $db->get_result($sql);
                
                if($userInfo){
                    $result['status'] = '1';
                    $result['firstname'] = $userInfo[0]['firstname'];
                    $result['lastname'] = $userInfo[0]['lastname'];
                    $result['email'] = $userInfo[0]['email'];
                    $result['phoneno'] = $userInfo[0]['phone'];
                    die(json_encode($result));
                }
                else{
                    $result['status'] = '0';
                    die(json_encode($result));
                } 
            }
            
            //Fetch All Users
            function fetchAllUser()
            {
                $db = new \DBC(DB_HOST,DB_USER,DB_PASSWD,DB_NAME_CLOUD);
                
                $loggedUser = $_SESSION['LOGGED_USER_ID'];
				$loggedUserRoles = $_SESSION['LOGGED_USER_ROLEIDS'];
                $imgPath = array();
                $userRoleArr = array();
                
                $where = "deleted=0";
                if($loggedUser!=""){
                    $where .= " and id!='$loggedUser' ";
                }
				if(in_array(ADMIN_ROLE,$loggedUserRoles))
				{
					$where .= " and added_by='$loggedUser'";
				}
                
                $sql = "select * from pmi_user where $where";
				//echo $sql;
                $userInfo = $db->get_result($sql);
                $i=0;
               // print_r($userInfo);die;
                foreach($userInfo as $user){
                    $userID = $user['id'];
                    
                    $sql3 = "select b.role_name from pmi_user_roles a left join pmi_role_master b on a.role_id=b.id where a.user_id='$userID' order by a.role_id asc";
                    $userRole = $db->get_result($sql3);
                    $userRoleArr[$i] = $userRole[0]['role_name'];
                            
                    $imgPath[$i] = "img/profiles/profile-".$user['id'].".png";
                    if(!file_exists($imgPath[$i])) $imgPath[$i] = "img/profiles/no-image.jpg";
                    $i++;
                }
                
                if($userInfo){
                    $result['status']   = '1';
                    $result['userInfo'] = $userInfo;
                    $result['userRole'] = $userRoleArr;
                    $result['imagePath'] = $imgPath;
                    die(json_encode($result));
                }
                else{
                    $result['status']   = '0';
                    die(json_encode($result));
                }
            }            
                        
            function saveUserInfo()
            {
                $db = new \DBC(DB_HOST,DB_USER,DB_PASSWD,DB_NAME_CLOUD);
                
                $loggedUserId       = $_SESSION['LOGGED_USER_ID'];
                $firstName          = $_REQUEST['firstname'];
                $lastName           = $_REQUEST['lastname'];
                $email              = $_REQUEST['email'];
                $contactno          = $_REQUEST['contact'];
                $username           = $_REQUEST['username'];
                $password           = $_REQUEST['password']; 
				$clientID           = $_REQUEST["clientUserID"];
                
                $sql1 = "select id from pmi_user where (username ='$username' OR email = '$email') and deleted=0";
                $userExist = $db->get_single_result($sql1);
                
                if($userExist){
                    $result['status'] = '0';
                    $result['message'] = "Username or Email Already Exist";
                    die(json_encode($result));
                }
                
                $dbrec = array();
                $dbrec["firstname"] = $firstName;
                $dbrec["lastname"]  = $lastName;
                $dbrec["email"]     = $email;
                $dbrec["phone"]     = $contactno;
                $dbrec["username"]  = $username;
                $dbrec["password"]  = $password;
                $dbrec["active"]    = 1;
                $dbrec["added_by"]  = $loggedUserId;
				$dbrec["clientId"]  = $clientID;
                
                $saveUser = $db->insert_query($dbrec , 'pmi_user');
                
                $userrole = $_REQUEST['userrole']; 
                $sql2 = "select id from pmi_user where username ='$username' and deleted = 0";
                $lastInsertId = $db->get_single_result($sql2);
                
                $limit = sizeof($userrole);
                $roleRec = array();
                
                for($i=0; $i<$limit; $i++)
                {
                   // echo 'role'.$userrole[$i].'>>>ID...'.$lastInsertId;
                   $roleRec["role_id"] = $userrole[$i];
                   $roleRec["user_id"] = $lastInsertId;
                   $saveUserRole = $db->insert_query($roleRec , 'pmi_user_roles');
                }
                
                if($saveUser && $saveUserRole){
                    $result['status'] = '1';
                    $result['message'] = "Successfully Added User";
                    die(json_encode($result));
                }
                else{
                    $result['status'] = '0';
                    $result['message'] = "Failed to Add User";
                    die(json_encode($result));
                }
            }
            
            function updateUserInfo()
            {
                $db = new \DBC(DB_HOST,DB_USER,DB_PASSWD,DB_NAME_CLOUD);
                
                $loggedUserId       = $_SESSION['LOGGED_USER_ID'];
                $firstName          = $_REQUEST['firstname'];
                $lastName           = $_REQUEST['lastname'];
                $email              = $_REQUEST['email'];
                $contactno          = $_REQUEST['contact'];
                $username           = $_REQUEST['username'];
                $password           = $_REQUEST['password']; 
                $userID             = $_REQUEST["userID"];
				$clientID           = $_REQUEST["clientUserID"];
                
                $sql1 = "select id from pmi_user where username ='$username' and deleted = 0 and id not in ('$userID')";
                $userExist = $db->get_single_result($sql1);

                if($userExist){
                    $result['status'] = '0';
                    $result['message'] = "Username Already Exist";
                    die(json_encode($result));
                }

                $dbrec = array();
                $dbrec["firstname"] =  mysqli_real_escape_string($db,$firstName);
                $dbrec["lastname"]  =  mysqli_real_escape_string($db,$lastName);
                $dbrec["email"]     = $email;
                $dbrec["phone"]     = $contactno;
                $dbrec["username"]  = $username;
                $dbrec["password"]  = $password;
                $dbrec["added_by"]  = $loggedUserId;
				$dbrec["clientId"]  = $clientID;
                
                $where["id"] = $userID;
                $saveUser = $db->update_query($dbrec , 'pmi_user',$where);
                
                $userrole = $_REQUEST['userrole']; 
                $sql2 = "delete from pmi_user_roles where user_id ='$userID'";
                $oldRoles = $db->query($sql2);

                $limit = sizeof($userrole);
                $roleRec = array();

                for($i=0; $i<$limit; $i++)
                {
                    $roleRec["role_id"] = $userrole[$i];
                    $roleRec["user_id"] = $userID;
                    $saveUserRole = $db->insert_query($roleRec , 'pmi_user_roles');
                }

                if($saveUser && $saveUserRole){
                    $result['status'] = '1';
                    $result['message'] = "Successfully updated user";
                    die(json_encode($result));
                }
                else{
                    $result['status'] = '0';
                    $result['message'] = "Failed to update user";
                    die(json_encode($result));
                }
            }
            
            //Change User Status (Enable/Disable)
            function updateUserStatus()
            {
                $db = new \DBC(DB_HOST,DB_USER,DB_PASSWD,DB_NAME_CLOUD);
                
                $userId = $_REQUEST['userid'];
                $userStatus = $_REQUEST['userstatus'];
                
                $dbrec = array();
                $where = array();
                
                if($userStatus == 0)
                {
                    $dbrec["active"]    = 1;
                    $status = "enabled";
                }
                else{
                    $dbrec["active"]    = 0;
                    $status = "disabled";
                }
                
                $where["id"] = $userId;
                $updateStatus = $db->update_query($dbrec , 'pmi_user',$where);
                
                if($updateStatus)
                {
                    $result['status'] = '1';
                    $result['userstatus'] = $userStatus;
                    $result['message'] = "Successfully ".$status." user";
                    die(json_encode($result));
                }
                else{
                    $result['status'] = '0';
                    $result['message'] = "Failed to ".$status." user";
                    die(json_encode($result));
                }
            } 
            
            //Remove User
            function removeUserInfo()
            {
                $db = new \DBC(DB_HOST,DB_USER,DB_PASSWD,DB_NAME_CLOUD);
                
                $userId = $_REQUEST['userid'];
                
                $dbrec = array();
                $where = array();
                $dbrec["deleted"]       = 1;
                $dbrec["deleted_by"]    = $userId;
                $where["id"]            = $userId;
                
                $removeUser = $db->update_query($dbrec , 'pmi_user',$where);
                
                if($removeUser)
                {
                    $result['status'] = '1';
                    $result['id'] = $userId;
                    $result['message'] = "Successfully Removed User";
                    die(json_encode($result));
                }
                else{
                    $result['status'] = '0';
                    $result['message'] = "Failed to Remove User";
                    die(json_encode($result));
                }
            } 
            
            //Fetch User Roles
            function fetchUserRole(){
                $db = new \DBC(DB_HOST,DB_USER,DB_PASSWD,DB_NAME_CLOUD);
                
                $userId = $_REQUEST['userid'];
                
                $sql = "select role_id from pmi_user_roles where user_id='$userId'";
                $existUserRoles = $db->get_result($sql);
                
                if($existUserRoles){
                    $result["status"] = '1';
                    $result["role"] = $existUserRoles;
                    die(json_encode($result));
                }
                else{
                    $result["status"] = '0';
                    die(json_encode($result));
                }
                
            }
            
            //Fetch User data by user ID
            function fetchUserData(){

                $db = new \DBC(DB_HOST,DB_USER,DB_PASSWD,DB_NAME_CLOUD);                
                $userId = $_REQUEST['userid'];
                
                $sql = "SELECT firstname,lastname,email,phone,username,password,clientId FROM pmi_user WHERE id = '$userId'";
                $existUserdata = $db->get_result($sql);
                
                $sql = "select role_id from pmi_user_roles where user_id='$userId'";
                $existUserRoles = $db->get_result($sql);
                
                if($existUserRoles){
                    $result["status"] = '1';
                    $result["result"] = $existUserdata;
                    $result["role"] = $existUserRoles;
                    die(json_encode($result));
                }
                else{
                    $result["status"] = '0';
                    die(json_encode($result));
                }                
            }
            
            //Fetch All User
            function fetchAllUserRole()
            {
                $db = new \DBC(DB_HOST,DB_USER,DB_PASSWD,DB_NAME_CLOUD);
                $sql = "select id,role_name from pmi_role_master";
                $allUserRoles = $db->get_result($sql);
				
				$loggedUser = $_SESSION['LOGGED_USER_ID'];
				$loggedUserRoles = $_SESSION['LOGGED_USER_ROLEIDS'];
				$where = "deleted=0";
				if($loggedUser != ""){
					$where .= " and id!='$loggedUser' and added_by='$loggedUser'";
				}

				if(in_array(ADMIN_ROLE,$loggedUserRoles))
				{
					$sql = 'SELECT  C.* FROM pmi_client C left join pmi_user U on (C.ID = U.clientId) WHERE C.deleted=0 AND U.ID = '.$loggedUser;
					//print_r($_SESSION);die;
				}
				else {
					$sql = "select * from pmi_client where $where";
				}
				//echo $sql;die;
				$clientInfo = $db->get_result($sql);				
                
                if($allUserRoles){
                    $result["status"] = '1';
                    $result["userrole"] = $allUserRoles;
					$result['clientInfo'] = $clientInfo;
                    die(json_encode($result));
                }
                else{
                    $result["status"] = '0';
                    die(json_encode($result));
                }
            }
            
            //Update User Roles
            function updateUserRole()
            {
               $db = new \DBC(DB_HOST,DB_USER,DB_PASSWD,DB_NAME_CLOUD);
                
                $userId = $_REQUEST['userid'];
                $userRole = $_REQUEST['roles'];
                
                if(sizeof($userRole))
                {
                    $sql = "delete from pmi_user_roles where user_id='$userId'";
                    $delExistUserRole = $db->query($sql);
                    
                    $dbrec = array();
                    
                    for($i=0;$i<sizeof($userRole);$i++)
                    {
                        $dbrec["role_id"] = $userRole[$i];
                        $dbrec["user_id"] = $userId;
                        $updateRole = $db->insert_query($dbrec , 'pmi_user_roles');
                        //echo $userRole[$i];
                    }
                   
                    if($updateRole){
                        $sql3 = "select a.role_id, b.role_name from pmi_user_roles a left join pmi_role_master b on a.role_id=b.id where a.user_id='$userId' order by a.role_id asc";
                        $existingRoles = $db->get_result($sql3);
                        
                        /*$priorRole = $existingRoles[0][role_name];
                        $allRoleArray = array();
                        for($j=0;$j<sizeof($existingRoles);$j++){
                            $allRoleArray[$j] =  $existingRoles[$j]['role_name'];
                        }
                        $allRole = implode(",",$allRoleArray); */
                        
                        $result["status"] = '1';
                        $result["userid"] = $userId;
                        $result['message'] = "Successfully updated user role";
                        die(json_encode($result));
                    }
                    else{
                        $result["status"] = '0';
                        $result['message'] = "Failed to update user role";
                        die(json_encode($result));
                    }
                }
                else{
                    $result["status"] = '0';
                    $result['message'] = "Please select atleast one role";
                    die(json_encode($result));
                }
                
            }            
    }
?>