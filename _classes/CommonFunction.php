<?php 
include_once("config.php");
//require_once('excel/Spreadsheet/Excel/Reader/OLERead.php');
require_once('phpMailer/PHPMailerAutoload.php');
require_once("excel/PHPExcel.php");

class CommonFunction
{
    # Constructor Method 
    function __constructor(){
    }
    
    //Generate Random Password
    public function random_password( $length = 8 ) {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        if($length == 4)$chars = "0123456789";
        $password = substr( str_shuffle( $chars ), 0, $length );
        return $password;
    }
    
    //Send Mail
    public function sendEmail($subject,$body,$to,$from,$host,$password,$port ){
        //date_default_timezone_set('Etc/UTC');
        $result = 0;
        try
        {

            //Create a new PHPMailer instance
            $mail = new PHPMailer;
            //Tell PHPMailer to use SMTP
            $mail->isSMTP();
            //Enable SMTP debugging
            // 0 = off (for production use)
            // 1 = client messages
            // 2 = client and server messages
            $mail->SMTPDebug = 0;
            //Ask for HTML-friendly debug output
            //$mail->Debugoutput = 'html';
            //Set the hostname of the mail server
            $mail->Host = $host;
            //Set the SMTP port number - likely to be 25, 465 or 587
            $mail->Port = $port;//587; //$port;
            //Whether to use SMTP authentication
            $mail->SMTPAuth = true;
            //Username to use for SMTP authentication
            $mail->Username = $from;
            //Password to use for SMTP authentication
            $mail->Password = $password;
            //Set who the message is to be sent from
            $mail->setFrom($from, '');
            $mail->SMTPSecure = "tls";
			
            //Set an alternative reply-to address
            $mail->addReplyTo($from, '');
            //Set who the message is to be sent to
            $mail->addAddress($to, '');
            //Set the subject line
            $mail->Subject = $subject;
            //Read an HTML message body from an external file, convert referenced images to embedded,
            //convert HTML into a basic plain-text alternative body
            $mail->msgHTML($body, dirname(__FILE__));
            
            //send the message, check for errors
            if (!$mail->send()) {
				
               $result = 0;
            } else {
                $result = 1;
            }
        }
        catch(Exception $e)
        {
              echo $errorMessage =  "Error in send the email to `".$to."` : ".$mail->ErrorInfo;
        }
        return $result;
    } 

    /************************************************
    * Send response as datatable format            *
    ************************************************/
    function setDataTableContent($dataArray)
    {
        $sOutput = "";
        $data = $dataArray['Results'];
        $totalRecords = $dataArray['TotalRecords'];
        if( sizeof($data) > 0 )	
        {
            $iTotal = sizeof($data);
            $iTotalDisplayRecord = ( $totalRecords != '' ) ? $totalRecords : $iTotal ;

            $sOutput = '{'; 
            $sOutput .= '"sEcho": '.intval($_REQUEST['sEcho']).', ';
            $sOutput .= '"iTotalRecords": '.$iTotal.', ';
            $sOutput .= '"iTotalDisplayRecords": '.$iTotalDisplayRecord.', ';
            $sOutput .= '"aaData":  ';
            $sOutput .= json_encode($data);
            $sOutput .= '}';			

        }
        else
        {
            $sOutput = '{';
            $sOutput .= '"sEcho": '.intval($_REQUEST['sEcho']).', ';
            $sOutput .= '"iTotalRecords": 0, ';
            $sOutput .= '"iTotalDisplayRecords": 0, ';
            $sOutput .= '"aaData":  []}';
        }

        echo $sOutput;
    }
    
    function resizeImage($image,$width,$height,$scale) 
    {
        //echo $image."-".$width."-".$height."-".$scale;
        $image_data = getimagesize($image);
        $imageType = image_type_to_mime_type($image_data[2]);
        $newImageWidth = ceil($width * $scale);
        $newImageHeight = ceil($height * $scale);
        echo $newImageWidth."<br>";
        echo $newImageHeight;
        $newImage = imagecreatetruecolor($newImageWidth,$newImageHeight);
        
        switch($imageType) {
                case "image/gif":
                        $source=imagecreatefromgif($image); 
                        break;
                case "image/pjpeg":
                case "image/jpeg":
                case "image/jpg":
                        $source=imagecreatefromjpeg($image); 
                        break;
                case "image/png":
                case "image/x-png":
                        $source=imagecreatefrompng($image); 
                        break;
        }
        imagecopyresampled($newImage,$source,0,0,0,0,$newImageWidth,$newImageHeight,$width,$height);

        switch($imageType) {
                case "image/gif":
                        imagegif($newImage,$image); 
                        break;
                case "image/pjpeg":
                case "image/jpeg":
                case "image/jpg":
                        imagejpeg($newImage,$image,90); 
                        break;
                case "image/png":
                case "image/x-png":
                        imagepng($newImage,$image);  
                        break;
        }

        chmod($image, 0777);
        return $image;
    }
    
    /****************************************
	 * Crop the image resized here and Return thumb image name
     ***************************************/
    function resizeThumbnailImage($thumb_image_name, $image, $width, $height, $start_width, $start_height, $scale,$location)
    {
        $image_data = getimagesize($image);
        $imageType = image_type_to_mime_type($image_data[2]);		
        $newImageWidth = ceil($width * $scale);
        $newImageHeight = ceil($height * $scale);

        //echo $width . " X " .$height . "X" . $scale;

        $newImage = imagecreatetruecolor($newImageWidth,$newImageHeight) or die('Cannot Initialize new GD image stream');

        //imagejpeg($newImage,NULL,90);

        switch($imageType) {
                case "image/gif":
                        $source=imagecreatefromgif($image); 
                        break;
                case "image/pjpeg":
                case "image/jpeg":
                case "image/jpg":
                        $source=imagecreatefromjpeg($image); 
                        break;
                case "image/png":
                case "image/x-png":
                        $source=imagecreatefrompng($image); 
                        break;
        }
        imagecopyresampled($newImage,$source,0,0,$start_width,$start_height,$newImageWidth,$newImageHeight,$width,$height);
        switch($imageType) {
                case "image/gif":
                        imagegif($newImage,$thumb_image_name); 
                        break;
                case "image/pjpeg":
                case "image/jpeg":
                case "image/jpg":
                        imagejpeg($newImage,$thumb_image_name,90); 
                        break;
                case "image/png":
                case "image/x-png":
                        imagepng($newImage,$thumb_image_name);  
                        break;
        }
        chmod($thumb_image_name, 0777);
        //echo $location ."==". THUMBNAILS_PATH;
        if($location == THUMBNAILS_PATH)
        {
                unlink($image);
        }
        imagepng(imagecreatefromstring(file_get_contents($thumb_image_name)), $image);
        return $thumb_image_name;
    }
    
    public function readExcelSheet($fileName){
        $excelReader = PHPExcel_IOFactory::createReaderForFile($fileName);
        $excelObj = $excelReader->load($fileName);
         //die($fileName."11111111");
        return $excelObj;
        /*$data = new Spreadsheet_Excel_Reader();
        $data->setOutputEncoding('CP1251');
        $data->read($fileName);
        return $data;*/
    }
    
    
    public function getUDPPacketID($dao){
        try{
            $sql="SELECT PacketID FROM pmi_packet_settings;";
            //echo $sql;
            $rtnVal = $dao->get_single_result($sql);
            $rtnVal = !empty($rtnVal) ? $rtnVal : 5;
            //echo "\n\nPACKET ID === $rtnVal \n\n";
            $updateVal = $rtnVal == 255 ? 5 : $rtnVal+1;
            $dao->update("UPDATE pmi_packet_settings SET PacketID = $updateVal");
            
            //echo "UPDATE pmi_packet_settings SET PacketID = $rtnVal \n\n";
            return $rtnVal;//$rtnVal["PacketID"];
        } catch (Exception $ex) {
            throw $ex;
        }
    }
	
    public function insertErrorLog($param=array(),$db=null) {
        
        $insertArr = array();
        $status = 0;
        if(sizeof($param) > 0){
            
            if(isset($param["DeviceID"])) $insertArr["DeviceID"] = $param["DeviceID"];
            if(isset($param["AlertDefID"])) $insertArr["AlertDefID"] = $param["AlertDefID"];
            if(isset($param["Severity"])) $insertArr["Severity"] = $param["Severity"];
            if(isset($param["Title"])) $insertArr["Title"] = $param["Title"];
            if(isset($param["Description"])) $insertArr["Description"] = $param["Description"];
            if(isset($param["Value"])) $insertArr["Value"] = $param["Value"];
            if(isset($param["Status"])) $insertArr["Status"] = $param["Status"];
            if(isset($param["AlertType"])) $insertArr["AlertType"] = $param["AlertType"];
            if(isset($param["TypeDBID"])) $insertArr["TypeDBID"] = $param["TypeDBID"];
            if(isset($param["TypeAction"])) $insertArr["TypeAction"] = $param["TypeAction"];
            if(isset($param["PolicyType"])) $insertArr["PolicyType"] = $param["PolicyType"];
            
            if(sizeof($insertArr) > 0) $status = $db->insert_query($insertArr, "pmi_device_alerts");
            
        }
        
    }
    


}