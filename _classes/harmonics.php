<?php
   include_once("config.php");
        //------------------------------Calculate Harmonic distortion--------------
        ///  <summary>                                                                                                
        ///  Represent a class that performs real or complex valued Fast Fourier                                      
        ///  Transforms. Instantiate it and use the FFT or TableFFT methods to                                        
        ///  compute complex to complex FFTs. Use FFTReal for real to complex                                         
        ///  FFTs which are much faster than standard complex to complex FFTs.                                        
        ///  Properties A and B allow selecting various FFT sign and scaling                                          
        ///  conventions.                                                                                             
        ///  </summary> 
    
    class LomontFFT{
        
        public $A = 1;
        public $B = 1;

        function calc_harmanics($sig){
            //$fft1 = new LomontFFT();
            $hdata = array();
            $hsig = array();
            $v1hsig = array();
            //return 1;
            //-------Load the wave signal into buffer depending on selected Harmanic --------
            //$sig = explode(",", $sig);
            
            for ($i = 0; $i < 128; $i++)
            { 
                $hdata[$i * 2] = 20 * $sig[$i];
                $hdata[$i * 2 + 1] = 0;
            }
            //---------------END LOAD SIGNAL------------

            // Run FFT
            $hdata = $this->FFT($hdata, true);
            // Select Transform data
            $peak = 0;
            $peakID = 0;
            $sum = 0;
            
            $hsig[0] = 99;
           
 /*
            for ($i = 0; $i < 26; $i++)
            {   
                $fdum=0;
                $fdum = $hsig[$i*2+1] = sqrt($hdata[$i * 2 + 1] * $hdata[$i * 2 + 1]+$hdata[$i * 2] * $hdata[$i * 2]);
                $v1hsig[$i] = $fdum;
                $sum += $fdum;
                
                if ($hsig[$i] > $peak) $peak = $hsig[$i];
            }  */
	
            
            $peak = 0;

            for ($i = 1; $i < 53; $i++)
            {
                if ($hdata[$i] > $peak)
                {
                    $peak = $hdata[$i];
                    $peakID = $i;
                }
            }



            for ($i = 1; $i < 53; $i++)
            {
        	$hdata[i] = ( 100 * $hdata[i] / $peak );
            }
		
	    $total_harm_dist = 0;
            
            $rtnVal["value"] = $hdata;
            $rtnVal["Pval"] = $peak;
            $rtnVal["PID"] = $peakID;
            $rtnVal["THDval"] = $total_harm_dist;
            
            return $rtnVal;
        }
        function FFT($dataa,$forward){
            $n = count($dataa);
            // checks n is a power of 2 in 2's complement format                                                 
            if (($n & ($n - 1)) != 0) throw new Exception("data length " + n + " in FFT is not a power of 2");
             $n /= 2;    // n is the number of samples    
             
            $dataa = $this->Reverse($dataa, $n); // bit index data reversal
            // do transform: so single point transforms, then doubles, etc.                                      
             $sign = $forward ? B : -B;
             $sign = floatval("1.0F");     // modification to correct problem
             $mmax = 1;
              
             while ($n > $mmax)
             {
                
                 $istep = 2 * $mmax;
                 $theta = $sign * pi() / $mmax;
                 $wr = 1;
                 $wi = 0;
                 $wpr = cos($theta);
                 $wpi = sin($theta);
                 for ($m = 0; $m < $istep; $m += 2)
                 {
                     for ($k = $m; $k < 2 * $n; $k += 2 * $istep)
                     {
                         $j = $k + $istep;
                         $tempr = $wr * $dataa[$j] - $wi * $dataa[$j + 1];
                         $tempi = $wi * $dataa[$j] + $wr * $dataa[$j + 1];
                         $dataa[$j] = $dataa[$k] - $tempr;
                         $dataa[$j + 1] = $dataa[$k + 1] - $tempi;
                         $dataa[$k] = $dataa[$k] + $tempr;
                         $dataa[$k + 1] = $dataa[$k + 1] + $tempi;
                     }
                     $t = $wr; // trig recurrence                                                               
                     $wr = ($wr * $wpr - $wi * $wpi);
                     $wi = ($wi * $wpr + $t * $wpi);
                 }
                 $mmax = $istep;
             }

             // perform data scaling as needed                                                                    
             $dataa = $this->Scale($dataa, $n, $forward);

             return $dataa;
        }
        function Reverse($data,$n){
             // bit reverse the indices. This is exercise 5 in section                                            
             // 7.2.1.1 of Knuth's TAOCP the idea is a binary counter                                             
             // in k and one with bits reversed in j                                                              
             $j = 0;
             $k = 0; // Knuth R1: initialize                                                            
             $top = $n / 2;  // this is Knuth's 2^(n-1)  
             while (true)
             {
                 // Knuth R2: swap - swap j+1 and k+2^(n-1), 2 entries each                                       
                 $t = $data[$j + 2];
                 $data[$j + 2] = $data[$k + $n];
                 $data[$k + $n] = t;
                 $t = $data[$j + 3];
                 $data[$j + 3] = $data[$k + $n + 1];
                 $data[$k + $n + 1] = t;
                 if ($j > $k)
                 { // swap two more                                                                               
                     // j and k                                                                                   
                     $t = $data[$j];
                     $data[$j] = $data[$k];
                     $data[$k] = $t;
                     $t = $data[$j + 1];
                     $data[$j + 1] = $data[$k + 1];
                     $data[$k + 1] = $t;
                     // j + top + 1 and k+top + 1                                                                 
                     $t = $data[$j + $n + 2];
                     $data[$j + $n + 2] = $data[$k + $n + 2];
                     $data[$k + $n + 2] = $t;
                     $t = $data[$j + $n + 3];
                     $data[$j + $n + 3] = $data[$k + $n + 3];
                     $data[$k + $n + 3] = $t;
                 }
                 // Knuth R3: advance k                                                                           
                 $k += 4;
                 if ($k >= $n){break;} 
                //break;
                 // Knuth R4: advance j                                                                           
                 $h = $top;
                 while ($j >= $h)
                 {
                     $j -= $h;
                     $h /= 2;
                 }
                 $j += $h;
             } 
             return $data;
        }


         function Scale($data, $n, $forward) {

             // forward scaling if needed                                                                         
             if (($forward) && ($A != 1))
             {
                $scale = pow($n, ($A - 1) / 2.0);
               
                for ($i = 0; $i < count($data); ++$i)$data[$i] *= $scale;
                 //echo count($data);
                //die(">>>>>>>");
                     
            }

             // inverse scaling if needed                                                                         
             if ((!$forward) && ($A != -1))
             {
                 $scale = pow($n, -($A + 1) / 2.0);
                 for ($i = 0; $i < count($data); ++$i) $data[$i] *= $scale;
                    
             }

             return $data;
         }
         
        function getA(){
            return $A;
        }
        function setA($a){
            $A = $a;
        }
        
        function getB() {
            return $B;
        }
        function setB($b) {
            $B = $b;
        }
    }

?>
