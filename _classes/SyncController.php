<?php
    include_once("config.php"); 
    include_once("_classes/InspextorLocalDB.php");
    
    class SyncController extends ControllerBase 
    {	
		# Constructor Method 
		function __constructor(){
		}
			
		function verifyConnection($ARGS)
		{
			$result["status"] = "0";
			
			$db = new DBC();
			$insId=$ARGS["instanceId"];
			$insPass=$ARGS["instancePass"];
			
			//print_r("select id from pmi_inx_device where inspextorID ='$insId' and Password='$insPass' and deleted = 0");
            $inxDeviceId = $db->get_single_result("select id from pmi_inx_device where inspextorID ='$insId' and Password='$insPass' and deleted = 0");
            
           
            if ( $inxDeviceId != "" )	{
				// Make device active,if valid
				$dbrec = array();
				$dbrec["Status"] = 1;
				$where["id"] = $inxDeviceId;
				$db->update_query($dbrec , 'pmi_inx_device',$where);
				
				$dbname = $db->get_single_result("select DeviceDBName from pmi_inx_device_db where DeviceID ='$inxDeviceId'");
				
				$ildb = new InspextorLocalDB();
				if ( !$ildb->create($dbname) ) $result["status"] = "0";
				
				$result["status"] = "1";
			}
			die(json_encode($result));
		}
    }
?>
