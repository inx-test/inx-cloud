<?php
// Packet Types
define('PT_REQUESTS',101); // Request
define('PT_COMMANDS',103); // Commands
define('PT_EVENTS',105); // Events
define('PT_EVENTS',107); // Policy
define('PT_ACK',109); // ACK

// Commands
define('LIGHT_ON',110); // Direct command of turn the light ON
define('LIGHT_OFF',111); // Direct Command to turn the light OFF
define('LIGHT_FLASH',112); // num of flash		LIGHT FLASH
define('LIGHT_DIM',113);	// dim level % of max	LIGHT DIM
define('LIGHT_BRIGHTER',114);	// raise level by 1 %
define('LIGHT_DARKER',115);	// raise level by 1 %

//Events array
define('LIGHT_ON',110); // Direct command of turn the light ON
define('LIGHT_OFF',111); // Direct Command to turn the light OFF
define('LIGHT_FLASH',112); // num of flash		LIGHT FLASH
define('LIGHT_DIM',113);	// dim level % of max	LIGHT DIM
define('LIGHT_BRIGHTER',114);	// raise level by 1 %
define('LIGHT_DARKER',115);	// raise level by 1 %

// Class which handle all UDP packet commands
class UDPHelper{
  /*--------------------------------------------------------------
  * From Packet Format Doc:
  *
  Packet Format:
  1) 255				preamble
  2) 0X55			Start Byte
  3) packet ID			pkt_id		// this is a rolodex ID 255
  4) device Tag ID high	byte	Tg_id_hb
  5) device Tag ID low byte 	Tg_id_lb
  6) Packet Type	 		pkt_type
  7) Number of bytes to follow. (Packet Parameters)	n
  a) Packet Data (specific to command or event)	b1, b2, b3 ...
  8) CS: check sum		tx_cs
  e.g. 255,85,1,4,26,103,1,117,80
  the above packet is a blink command.
  PS: blink = 117 we will supply all commands list to send.
  --------------------------------------------------------------*/
  // Available Commands
  /*var $commands = array(LIGHT_ON=>"Turn the light ON", LIGHT_OFF=>"Turn the light OFF",LIGHT_FLASH=>"Light Flash",LIGHT_DIM=>"Light Dim",LIGHT_BRIGHTER=>"Light Brighter",LIGHT_DARKER=>"Light Darker");*/
  
var $commands = array(LIGHT_ON=>"Turn the light ON", LIGHT_OFF=>"Turn the light OFF",LIGHT_DIM=>"Light Dim",LIGHT_BRIGHTER=>"Light Brighter",LIGHT_DARKER=>"Light Darker");

  // Packet
  var $packet = array();
  var $packetIndex = 0;


  //*** Private Methods
  // Generate Random Packet ID
  private function __getPacketId($cmd){
      return rand(1,255);
  }

  // Get Packet Type
  private function __getPacketType($cmd){
      //TODO:
      return PT_COMMANDS;
  }
  
  // Calculate Highbyte of decimal
  public function __getHiByte($num){
      $number = decbin($num);
      return $number >> 8;
  }

  // Calculate Highbyte of decimal
  public function __getLowByte($num){
    $number = decbin($num);
    return $number & 0xFF;
  }


  //*** Public Methods
  // Send Command to UDP
  public function send($ip, $port, $cmd, $tag){
      // The response
      $result = "";

      // Open Connection
      //$fp = fsockopen($ip, $port, $errno, $errstr, 30);
      $fp = stream_socket_client("udp://$ip:$port", $errno, $errstr);

      // Check for error
      if (!$fp) {
          echo "$errstr ($errno)<br />";
      }
      else {
            // Reset array & index
            $this->packet = array(); // Defaut
            $this->packetIndex = 0; // Defaut

            // Prepare Packet
            $this->packet[$this->packetIndex++] = 255; // Preamble
            $this->packet[$this->packetIndex++] = 85;  // Start byte
            $this->packet[$this->packetIndex++] = $this->__getPacketId($cmd); // Packet ID
            $this->packet[$this->packetIndex++] = 255;  // Tag High Byte
            $this->packet[$this->packetIndex++] = 255;  // Tag Low Byte
            $this->packet[$this->packetIndex++] = $this->__getPacketType($cmd);  // Packet Type

            // Add Command to Packet
            $this->packet[$this->packetIndex++] = 1;  // Number of bytes to follow. (Packet Parameters)
            $this->packet[$this->packetIndex++] = $cmd; // Command to Execute

            // Calcualte & Add Checksum
            $packetData = implode($this->packet,"");
            $checksum = strlen($packetData);     // Calculate checksum
            $this->packet[$this->packetIndex] = $checksum; // Checksum

            // Final packet
            $packedPacket = NULL;
            for($i = 0; $i < sizeof($this->packet); $i++ ){
                $packedPacket .= pack("C*", $this->packet[$i]);
            }

            //var_dump($packedPacket);

            // Write to stream as binary
            if( $packedPacket != NULL) {
                fwrite($fp, $packedPacket);
            }
            else {
                $checksum = 0;
            }

            // Close connection
            fclose($fp);

        } // End Else

      // Finally return result
      return $checksum;
  } // End Function


  // Get Available Commands
  public function getCommandList(){
      return $this->commands;
  }


}


// Sample Implementation

/*$udpHelper = new UDPHelper();
$commands = $udpHelper->getCommandList();

// Execute function
$cmd = ( isset($_POST["__cmd"]) ) ? $_POST["__cmd"] : "";
if( !empty($cmd) ) {
  $response = $udpHelper->send("192.168.1.11", "10026", $cmd);
  if( $response > 0 )  echo "Y";
  else echo "N";
}*/

?>


