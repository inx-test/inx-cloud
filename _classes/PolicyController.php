<?php

include_once("config.php");
require_once('UDP/PMIPacket.php');
require_once('UDP/UDP.php');

class PolicyController extends ControllerBase {
    # Constructor Method 

    function __constructor() {
        
    }
    
    Public function createPolicy($nodeID,$policyID,$pmiPolicyID){
        try {
            $udpHelper = new UDP();
            $policySql = <<<EOF
                    SELECT 
                        P.ID,
                        P.Name,
                        PL.*  
                    FROM pmi_policy P 
                        LEFT JOIN 
                            pmi_policy_line PL ON P.ID=PL.PolicyID 
                    WHERE 
                        P.ID=$policyID AND 
                        P.Deleted=0 AND 
                        PL.Deleted=0
EOF;
            $policyData = $db->get_result($policySql);
            
            if(sizeof($policyData)){
                $nodeTypeID = FIXTURE_TYPE_NODE;
                $sql = "SELECT * FROM pmi_tag T WHERE T.NodeID = 1 AND T.Deleted=0 AND T.TagTypeID=$nodeTypeID AND T.Acknowledged=1 ";
                $nodeData = $db->get_result($sql);
                if(sizeof($nodeData) > 0){
                    
                    $setTagPkt = new PMIPacket(null,null);
                    $setTagPkt->ipAddress = $nodeData[0]["IPAddress"];
                    $setTagPkt->packetId = $this->getPacketID($this->dao); // uniqe id between 5 to 255 
                    
                    // Set Parameter Can be any order 
                    $setTagPkt->setLowHightByte($nodeID);
                    
                    // Bytes
                    $setTagPacketBytes = array(
                        PT_POLICY, // Packet Type
                        CREATE_POLICY, 
                        $pmiPolicyID,
                    );
                    
                    $splitName = str_split($policyData[0]["Name"]);
                    if(sizeof($splitName) > 0){
                        foreach ($splitName as $value) {
                            array_push($setTagPacketBytes, $value);
                        }
                    }
                    
                }
            }
        } catch (Exception $ex) {
            throw $ex->getMessage();
        }
    }

}
?>
        
