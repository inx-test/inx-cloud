<?php
    include_once("config.php");
    include_once('vendor/autoload.php');
    require_once('UDP/PMIPacket.php');
    require_once('UDP/UDP.php');
    
    // Load namespace
    use phpseclib\Net\SSH2;
    
    class TerminalController extends ControllerBase 
    {	
        # Constructor Method 
        function __constructor(){ }
        
        function createTerminal(){
            $db = new DBC();
            $jsonResult = array();
            $jsonResult["status"] = 0;
            $nodeID = isset($_REQUEST["nodeID"]) && !empty($_REQUEST["nodeID"]) ?  $_REQUEST["nodeID"] : 0;
            if($nodeID != 0){
                
                $sql="SELECT IPAddress,Name FROM pmi_tag WHERE NodeID=$nodeID AND TagTypeID=".FIXTURE_TYPE_NODE." AND Deleted=0";
                $nodeResult = $db->get_result($sql);
                
                if(sizeof($nodeResult) > 0){
                    //$ipAddress = $nodeResult[0]["IPAddress"];
                    $name = $nodeResult[0]["Name"];
                    $ipAddress =  $nodeResult[0]["IPAddress"];
                    $result['name'] = $name;
                    $result['ipAddress'] = $ipAddress;
                    $result['status'] = 1;
                }
            }
            die(json_encode($result));
        }
        
        
        //GET TAG LIVE STATUS
        function getTagLiveStatus(){
            $db = new DBC();
            $rtnResult = array();
            //$status = $_REQUEST["status"];
            $type = $_REQUEST["type"];
            $nodeID = isset($_REQUEST["nodeID"]) ? $_REQUEST["nodeID"] : 0;
            $clusterID = isset($_REQUEST["clusterID"]) ? $_REQUEST["clusterID"] : 0;
            $unit = isset($_REQUEST["unit"]) ? $_REQUEST["unit"] : 0;
            
            $date = new DateTime();
            $startDate = $date->format('Y-m-d H:i:s');
            $date->sub(new DateInterval('PT5S'));
            $endDate = $date->format('Y-m-d H:i:s');
            
            $where = "NS.DevDateTime BETWEEN '".$endDate."' AND '".$startDate."' ";
            $joinTb = "";
            $param = "";
            $nodeSql = "";
            
            if($unit == 1){
                $param = " AVG(NS.VoltageMaster* NS.DriverCurrent) AS Driver_A_KW";
                $param .= " , AVG(NS.SubVoltage* NS.DriverSubCurrent) AS Driver_B_KW";
            } 
            else if($unit == 2) $param = " AVG(NS.NodeTemp+NS.OCTemp+NS.DriverTemp) AS Temperature";
            else if($unit == 3) $param = " NS.OCStatus AS Motion";
            
            if($type == 2){//SELECT NODE
                //$joinTb = "LEFT JOIN pmi_tag T ON (T.ID = NS.NodeID) LEFT JOIN pmi_node N ON (N.ID = T.NodeID)";
                $where .= "AND NS.NodeID = ".$nodeID;
                $nodeSql ="SELECT N.ID,N.Name AS NodeName,T.Name TagName FROM pmi_node N LEFT JOIN pmi_tag T ON (T.NodeID = N.ID AND T.TagTypeID=".FIXTURE_TYPE_OCCUPANCYSENSOR." AND T.Status=1 AND T.Acknowledged=1) WHERE N.ID=".$nodeID;
            }
            else if($type == 3){//CLUSTER SELECT
                $joinTb = "LEFT JOIN pmi_tag T ON (T.ID = NS.NodeID) LEFT JOIN pmi_node N ON (N.ID = T.NodeID) LEFT JOIN pmi_cluster_mapping C ON (C.NodeID = N.ID)";
                $where .= "AND C.ClusterID = ".$clusterID." AND C.Deleted=0";
                
                $nodeSql ="SELECT N.ID,N.Name AS NodeName,T.Name AS TagName FROM pmi_node N LEFT JOIN pmi_cluster_mapping C ON (C.NodeID = N.ID) LEFT JOIN pmi_tag T ON (T.NodeID = N.ID AND T.TagTypeID=".FIXTURE_TYPE_OCCUPANCYSENSOR." AND T.Status=1 AND T.Acknowledged=1) WHERE C.ClusterID = ".$clusterID." AND C.Deleted=0";
            }
            
            
            $sql =<<<EOF
                SELECT 
                    $param
                    FROM pmi_node_status NS 
                        $joinTb 
                    WHERE 
                        $where
EOF;
            
            $result = $db->get_result($sql);
            $nodeResult = array();
            if($nodeSql != "") $nodeResult = $db->get_result($nodeSql);
            
            if(sizeof($result) > 0){
                $rtnResult["status"] = 1;
                $rtnResult["result"] = $result;
            }
            else $rtnResult["status"] = 0;
            $rtnResult["nodeResult"] = $nodeResult;
            
            die(json_encode($rtnResult));
        } 
        
        //COMMAND SENT TO NODE
        function sentToNode(){
            $db = new DBC();
            
            $nodeID =  isset($_REQUEST["nodeID"]) && !empty($_REQUEST["nodeID"]) ?  $_REQUEST["nodeID"] : 0;
            $cmd = strtolower($_REQUEST["cmd"]);
            $split_cmd = str_split($cmd);
            $packetQueueID = 0;
            $retnVal["status"] = 0;
            
            $retnVal = array();
            $retnVal["status"]=0;
            try {
                $sql = "SELECT ID,IPAddress FROM pmi_tag WHERE NodeID=$nodeID AND TagTypeID=".FIXTURE_TYPE_NODE." AND Acknowledged=1";
                $tagDetails = $db->get_result($sql);
                if(sizeof($tagDetails) > 0){
                    $tagID = $tagDetails[0]["ID"];
                    $broadcastIp =  $tagDetails[0]["IPAddress"];

                // UDP Helper Class
					$udpHelper = new UDP();
					$cmdPacket = new PMIPacket();
                    $cmf =  new CommonFunction();
                    $cmdPacket->packetId = $cmf->getUDPPacketID($db);
					$cmdPacket->tagIdHighByte = $udpHelper->getHiByte($tagID);
					$cmdPacket->tagIdLowByte = $udpHelper->getLoByte($tagID);
                    $cmdPacket->setPacketType(PT_REQUESTS);
                    $cmdPacket->setCommand(TX_TERM);
                    $cmdPacket->setTXParam($cmd);
                
                
                    //Packet queue updation
                    $insertArr = array();
                    $insertArr["Direction"] = UDP_SEND;
                    $insertArr["TagID"] = $tagID;
                    $insertArr["PacketID"] = $cmdPacket->packetId;
                    $insertArr["BinaryData"] = $cmdPacket->getBinary();
                    $insertArr["DecimalData"] = $cmdPacket->getDecimal();
                    $insertArr["CMD"] = TX_TERM;
                    $insertArr["SendCount"] = 1;
                    $insertArr["Proceed"] = 1;
                    $insertArr["ProceedOn"] = gmdate('Y-m-d H:i:s');
                    $status = $db->insert_query($insertArr, "pmi_packet_queue");
                    $packetQueueID = $db->get_insert_id();
                     // Send
                    $status = $udpHelper->send($broadcastIp, NODE_LISTENER_PORT, $cmdPacket);
                
                    if($status){
                        $ackCheckerCount = 0;
                        while ($ackCheckerCount < 3){
                            sleep(RESEND_DELAY);
                            $RX_TERM = RX_TERM;
                            $qry = <<<EOF
                                SELECT * FROM pmi_packet_queue WHERE ID > $packetQueueID AND TagID=$tagID AND CMD = $RX_TERM
EOF;

                            $res = $db->get_result($qry);
                            //print_r($res);
                            
                            if(sizeof($res) > 0){
								$cmdStrArr = array();
                                for ($i = 0; $i < count($res); $i++) {
                                    $decimal = explode(",", $res[$i]["DecimalData"]);
                                    $len = $decimal[8];
                                    $len = $len + 8;
									//echo "len ::: $len \n";
									//print_r($decimal);
                                    for ($j = 9; $j <= $len; $j++) {
										//echo $decimal[$j]."\n";
										//echo  "char :::: ".chr($decimal[$j])."\n";
										array_push($cmdStrArr,$decimal[$j]);
                                        //$cmdStr .= chr($decimal[$j]);
                                    }
									
									//Update current packet queue to db
									$updateArr = array();
									$where = array();
									$updateArr["Proceed"] = 1;
									$updateArr["ProceedOn"] = gmdate('Y-m-d H:i:s');
									$where["ID"] = $res[$i]["ID"];
									$status = $db->update_query($updateArr, "pmi_packet_queue",$where);
                                    
                                }
								if( sizeof($cmdStrArr) > 0){
									//echo ">>>> $cmdStr >>> \n";
									$retnVal["result"] = $cmdStrArr;
									$retnVal["status"] = 1;
									//print_r($retnVal);
								}
                                break;
                            }
                            $ackCheckerCount++;
                        }
                    }
                }
                //print_r($retnVal);
            } catch (Exception $exc) {
                $retnVal["result"] = $exc->getMessage();
            }
            die(json_encode($retnVal));
        }
    }
?>