<?php
include_once("config.php");


class InspextorLocalDB
{
	private $pmi_alert_definition="
	CREATE TABLE IF NOT EXISTS `pmi_alert_definition` (
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `device_type` tinyint(4) NOT NULL DEFAULT '1',
	  `device_id` int(11) DEFAULT NULL,
	  `parameter` varchar(50) DEFAULT NULL,
	  `parameter_condition` varchar(20) DEFAULT NULL,
	  `parameter_from_value` float(10,2) DEFAULT '0.00',
	  `parameter_to_value` float(10,2) DEFAULT '0.00',
	  `StartTime` time NOT NULL,
	  `EndTime` time NOT NULL,
	  `Day1` tinyint(4) NOT NULL DEFAULT '0',
	  `Day2` tinyint(4) NOT NULL DEFAULT '0',
	  `Day3` tinyint(4) NOT NULL DEFAULT '0',
	  `Day4` tinyint(4) NOT NULL DEFAULT '0',
	  `Day5` tinyint(4) NOT NULL DEFAULT '0',
	  `Day6` tinyint(4) NOT NULL DEFAULT '0',
	  `Day7` tinyint(4) NOT NULL DEFAULT '0',
	  `severity` varchar(20) DEFAULT NULL,
	  `description` varchar(200) DEFAULT NULL,
	  `user_notified` varchar(50) DEFAULT NULL,
	  `added_by` int(11) DEFAULT NULL,
	  `deleted` int(11) NOT NULL DEFAULT '0',
	  `deleted_by` int(11) DEFAULT NULL,
	  PRIMARY KEY (`id`),
	  KEY `device_id` (`device_id`),
	  KEY `deleted` (`deleted`)
	)";

	private $pmi_alert_lookup ="
	CREATE TABLE IF NOT EXISTS `pmi_alert_lookup` (
	  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
	  `UserID` bigint(20) DEFAULT NULL,
	  `DeviceID` bigint(20) DEFAULT NULL,
	  `AlertDefID` bigint(20) DEFAULT NULL,
	  `CreatedOn` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
	  PRIMARY KEY (`ID`),
	  KEY `UserID` (`UserID`),
	  KEY `DeviceID` (`DeviceID`),
	  KEY `AlertDefID` (`AlertDefID`)
	)";

	private $pmi_at_policy="
	CREATE TABLE IF NOT EXISTS `pmi_at_policy` (
	  `ID` bigint(20) NOT NULL,
	  `PName` varchar(250) DEFAULT NULL,
	  `ClusterID` int(11) NOT NULL,
	  `Color` int(3) DEFAULT '0',
	  `StartTime` time DEFAULT NULL,
	  `EndTime` time DEFAULT NULL,
	  `Day1` tinyint(4) DEFAULT '0',
	  `Day2` tinyint(4) DEFAULT '0',
	  `Day3` tinyint(4) DEFAULT '0',
	  `Day4` tinyint(4) DEFAULT '0',
	  `Day5` tinyint(4) DEFAULT '0',
	  `Day6` tinyint(4) DEFAULT '0',
	  `Day7` tinyint(4) DEFAULT '0',
	  `Createdby` bigint(20) DEFAULT NULL,
	  `CreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	  `Deleted` tinyint(1) NOT NULL DEFAULT '0',
	  `DeletedBy` bigint(20) DEFAULT NULL,
	  `DeletedOn` datetime DEFAULT NULL,
	  `ModifiedBy` bigint(20) DEFAULT NULL,
	  `ModiFiedOn` datetime DEFAULT NULL,
	  PRIMARY KEY (`ID`)
	)";

	private $pmi_cluster="
	CREATE TABLE IF NOT EXISTS `pmi_cluster` (
	  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
	  `Name` varchar(250) DEFAULT NULL,
	  `Order` int(11) NOT NULL DEFAULT '0',
	  `CreatedBy` bigint(20) DEFAULT NULL,
	  `CreatedOn` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
	  `Deleted` tinyint(4) DEFAULT '0',
	  `DeletedBy` bigint(20) DEFAULT NULL,
	  `DeletedON` datetime DEFAULT NULL,
	  `ModifiedBy` bigint(20) DEFAULT NULL,
	  `ModifiedOn` datetime DEFAULT NULL,
	  PRIMARY KEY (`ID`)
	)";

	private $pmi_cluster_mapping="
	CREATE TABLE IF NOT EXISTS `pmi_cluster_mapping` (
	  `ID` bigint(11) NOT NULL AUTO_INCREMENT,
	  `ClusterID` bigint(20) DEFAULT NULL,
	  `NodeID` bigint(20) DEFAULT NULL,
	  `PeripheralID` int(11) DEFAULT NULL,
	  `Order` int(11) DEFAULT NULL,
	  `CreateAction` tinyint(1) NOT NULL DEFAULT '1',
	  `CreatedBy` bigint(20) NOT NULL DEFAULT '0',
	  `CreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	  `Deleted` tinyint(4) NOT NULL DEFAULT '0',
	  `DeletedBy` bigint(20) DEFAULT NULL,
	  `DeletedOn` datetime DEFAULT NULL,
	  PRIMARY KEY (`ID`),
	  KEY `ClusterID` (`ClusterID`),
	  KEY `NodeID` (`NodeID`),
	  KEY `Deleted` (`Deleted`),
	  KEY `Order` (`Order`),
	  KEY `ClusterID_2` (`ClusterID`),
	  KEY `Deleted_2` (`Deleted`),
	  KEY `PeripheralID` (`PeripheralID`)
	)";

	private $pmi_communication="
	CREATE TABLE IF NOT EXISTS `pmi_communication` (
		`Communication_Time` datetime DEFAULT NULL
	)";

	private $pmi_device="
	CREATE TABLE IF NOT EXISTS `pmi_device` (
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `device_name` varchar(30) DEFAULT NULL,
	  `type` tinyint(4) DEFAULT '0' COMMENT '0:other;1:sisco switch;',
	  `serial_no` varchar(50) DEFAULT NULL,
	  `ip_address` varchar(50) DEFAULT NULL,
	  `module` varchar(100) DEFAULT NULL,
	  `available_watts` double DEFAULT NULL,
	  `used_watts` double DEFAULT NULL,
	  `remaining_watts` double DEFAULT NULL,
	  `oper_power_total` double DEFAULT NULL,
	  `mac_address` varchar(50) DEFAULT NULL,
	  `assembly_no` varchar(50) DEFAULT NULL,
	  `model_no` varchar(50) DEFAULT NULL,
	  `serial_no_sys` varchar(50) DEFAULT NULL,
	  `location` varchar(50) DEFAULT NULL,
	  `ping_status` int(11) NOT NULL DEFAULT '0',
	  `AlertStatus` tinyint(1) NOT NULL DEFAULT '0',
	  `AlertCondition` tinyint(4) DEFAULT NULL,
	  `StartTime` time DEFAULT NULL,
	  `EndTime` time DEFAULT NULL,
	  `AlertSnapshot` varchar(255) DEFAULT NULL,
	  `AlertUsers` varchar(50) NOT NULL,
	  `created_by` int(11) DEFAULT NULL,
	  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	  `deleted` int(11) NOT NULL DEFAULT '0',
	  `deleted_by` int(11) DEFAULT NULL,
	  `galileo_id` int(11) DEFAULT '0',
	  `daily_log_id` int(11) DEFAULT '0',
	  `hourly_log_id` int(11) DEFAULT '0',
	  `status` int(11) NOT NULL DEFAULT '0' COMMENT '0:online,1:offline,2:removed',
	  `modified_on` datetime DEFAULT NULL,
	  PRIMARY KEY (`id`),
	  KEY `serial_no` (`serial_no`),
	  KEY `ip_address` (`ip_address`),
	  KEY `ping_status` (`ping_status`),
	  KEY `deleted` (`deleted`),
	  KEY `galileo_id` (`galileo_id`),
	  KEY `daily_log_id` (`daily_log_id`),
	  KEY `hourly_log_id` (`hourly_log_id`),
	  KEY `status` (`status`),
	  KEY `mac_address` (`mac_address`),
	  KEY `assembly_no` (`assembly_no`),
	  KEY `model_no` (`model_no`),
	  KEY `serial_no_sys` (`serial_no_sys`)
	) ";

	private $pmi_device_alerts="
	CREATE TABLE IF NOT EXISTS `pmi_device_alerts` (
	  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
	  `DeviceID` bigint(20) DEFAULT '0',
	  `AlertDefID` bigint(20) DEFAULT '0',
	  `Severity` int(11) DEFAULT '1' COMMENT ';1 : normal; 2 : Acceptable; 3 : Above Normal; 4 : Severe; 5 : Ambient; 6:Above ambient;7:Critical',
	  `Title` varchar(100) DEFAULT NULL,
	  `Description` text,
	  `Value` varchar(50) DEFAULT NULL,
	  `Status` tinyint(4) DEFAULT NULL,
	  `AlertType` tinyint(4) DEFAULT '1' COMMENT '1:alert def alert;0: other alert;2:PolicyAlert',
	  `TypeAction` int(11) DEFAULT NULL,
	  `TypeMasterID` bigint(20) DEFAULT NULL,
	  `TypeDBID` bigint(20) DEFAULT NULL,
	  `PolicyType` tinyint(4) DEFAULT '0' COMMENT '1:weekly;2:Single;3:override;',
	  `CratedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	  `Resolved` tinyint(4) DEFAULT '0',
	  `ResolvedBy` tinyint(4) DEFAULT NULL,
	  `ResolvedOn` datetime DEFAULT NULL,
	  `EmailProcess` bit(1) NOT NULL DEFAULT b'0',
	  PRIMARY KEY (`ID`),
	  KEY `DeviceID` (`DeviceID`),
	  KEY `AlertDefID` (`AlertDefID`),
	  KEY `Status` (`Status`),
	  KEY `ResolvedBy` (`ResolvedBy`),
	  KEY `AlertType` (`AlertType`),
	  KEY `TypeAction` (`TypeAction`),
	  KEY `TypeMasterID` (`TypeMasterID`),
	  KEY `TypeDBID` (`TypeDBID`),
	  KEY `EmailProcess` (`EmailProcess`),
	  KEY `ResolvedOn` (`ResolvedOn`),
	  KEY `CratedOn` (`CratedOn`),
	  KEY `Severity` (`Severity`),
	  KEY `PolicyType` (`PolicyType`)
	)";

	private $pmi_device_log="
	CREATE TABLE IF NOT EXISTS `pmi_device_log` (
	  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
	  `DeviceID` bigint(20) DEFAULT NULL,
	  `Interface` varchar(50) DEFAULT NULL,
	  `Port` int(11) DEFAULT NULL,
	  `AdminState` varchar(10) DEFAULT NULL,
	  `OperState` tinyint(4) DEFAULT '0' COMMENT '0:OFF;1ON;3:N/A',
	  `AdminPolice` varchar(10) DEFAULT NULL,
	  `OperPolice` varchar(10) DEFAULT NULL,
	  `CutoffPower` double DEFAULT NULL,
	  `OperPower` double DEFAULT NULL,
	  `LogDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	  `Status` tinyint(4) DEFAULT '1',
	  PRIMARY KEY (`ID`),
	  KEY `DeviceID` (`DeviceID`),
	  KEY `TimeStamp` (`LogDate`),
	  KEY `Interface` (`Interface`),
	  KEY `Status` (`Status`)
	)";

	private $pmi_event="
	CREATE TABLE IF NOT EXISTS `pmi_event` (
	  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
	  `NodeID` bigint(20) DEFAULT NULL,
	  `TagID` bigint(20) DEFAULT NULL,
	  `EventID` int(11) DEFAULT NULL,
	  `PacketID` bigint(20) DEFAULT NULL,
	  `Message` varchar(200) DEFAULT NULL,
	  `EventDirection` tinyint(4) NOT NULL DEFAULT '0',
	  `ParamOther` tinyint(4) NOT NULL DEFAULT '0',
	  `CreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	  PRIMARY KEY (`ID`),
	  KEY `NodeID` (`NodeID`),
	  KEY `TagID` (`TagID`),
	  KEY `EventID` (`EventID`),
	  KEY `CreatedOn` (`CreatedOn`)
	) ";

	private $pmi_inx_cloud="
	CREATE TABLE IF NOT EXISTS `pmi_inx_cloud` (
	  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
	  `Name` varchar(50) DEFAULT NULL,
	  `Password` varchar(100) DEFAULT NULL,
	  `CloudUrl` varchar(250) DEFAULT NULL,
	  `Status` tinyint(4) DEFAULT '0',
	  `CreatedBy` bigint(20) DEFAULT NULL,
	  `CreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	  `SyncStatus` tinyint(4) DEFAULT '0',
	  PRIMARY KEY (`ID`),
	  KEY `Name` (`Name`),
	  KEY `Password` (`Password`)
	)";

	private $pmi_location_image="
	CREATE TABLE IF NOT EXISTS `pmi_location_image` (
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `ClusterID` bigint(20) DEFAULT NULL,
	  `filename` varchar(50) DEFAULT NULL,
	  `uploaded_by` int(11) DEFAULT NULL,
	  `uploaded_on` date DEFAULT NULL,
	  PRIMARY KEY (`id`),
	  KEY `uploaded_by` (`uploaded_by`)
	) ";

	private $pmi_map_nodes="
	CREATE TABLE IF NOT EXISTS `pmi_map_nodes` (
	  `id` bigint(20) NOT NULL AUTO_INCREMENT,
	  `map_id` bigint(20) DEFAULT NULL,
	  `ClusterID` bigint(20) DEFAULT NULL,
	  `TagID` bigint(20) DEFAULT NULL,
	  `x_pos` float DEFAULT '0',
	  `y_pos` float DEFAULT '0',
	  `enabled` int(11) DEFAULT '0',
	  `created_by` int(11) DEFAULT NULL,
	  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	  PRIMARY KEY (`id`),
	  KEY `map_id` (`map_id`),
	  KEY `device_id` (`ClusterID`),
	  KEY `enabled` (`enabled`),
	  KEY `created_by` (`created_by`),
	  KEY `TagID` (`TagID`),
	  KEY `created_on` (`created_on`)
	)";

	private $pmi_node="
	CREATE TABLE IF NOT EXISTS `pmi_node` (
	  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
	  `Name` varchar(250) DEFAULT NULL,
	  `SerialNum` bigint(20) DEFAULT NULL,
	  `IPAddress` varchar(50) DEFAULT NULL,
	  `TagRomID` int(11) DEFAULT NULL,
	  `Acknowledged` tinyint(4) DEFAULT '0',
	  `Deleted` tinyint(4) NOT NULL DEFAULT '0',
	  `Status` tinyint(4) NOT NULL DEFAULT '1',
	  PRIMARY KEY (`ID`),
	  KEY `IPAdd` (`IPAddress`),
	  KEY `SerialNum` (`SerialNum`),
	  KEY `Acknowledged` (`Acknowledged`),
	  KEY `Deleted` (`Deleted`),
	  KEY `Status` (`Status`)
	) ";

	private $pmi_outbound_log="
	CREATE TABLE IF NOT EXISTS `pmi_outbound_log` (
	  `Data` longtext,
	  `Status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=Pending, 1=Processing, 2=Completed',
	  `LastModified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
	  PRIMARY KEY (`ID`)
	) ";

	private $pmi_peripheral="
	CREATE TABLE IF NOT EXISTS `pmi_peripheral` (
	  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
	  `Name` varchar(100) NOT NULL,
	  `NodeID` bigint(20) NOT NULL DEFAULT '0',
	  `PTypeID` tinyint(2) NOT NULL DEFAULT '0',
	  `PNumber` tinyint(2) NOT NULL DEFAULT '0',
	  `Deleted` tinyint(1) NOT NULL DEFAULT '0',
	  PRIMARY KEY (`ID`),
	  KEY `NodeID` (`NodeID`)
	) ";

	private $pmi_policy="
	CREATE TABLE IF NOT EXISTS `pmi_policy` (
	  `ID` bigint(3) NOT NULL,
	  `PolicyID` tinyint(2) DEFAULT '0',
	  `Name` varchar(250) DEFAULT NULL,
	  `Formate` tinyint(1) DEFAULT '0' COMMENT '1=Weekly Policy;Single Policy;3=OverRide Policy',
	  `ColorStatus` tinyint(1) DEFAULT '0',
	  `Color` int(4) DEFAULT '0',
	  `Dim` int(3) DEFAULT '0',
	  `MotionStatus` tinyint(1) DEFAULT '0' COMMENT '0=Motion Locked;1=Motion Unlocked',
	  `DurationHour` int(11) DEFAULT '0',
	  `DurationMinute` int(11) DEFAULT '0',
	  `TimeFormate` tinyint(1) DEFAULT '0' COMMENT '0=No Repeat;1=Weekly;2=Monthly;3=Yearly;',
	  `StartDate` datetime DEFAULT NULL,
	  `StartTime` time DEFAULT NULL,
	  `EndTime` time DEFAULT NULL,
	  `Day1` tinyint(4) DEFAULT '0',
	  `Day2` tinyint(4) DEFAULT '0',
	  `Day3` tinyint(4) DEFAULT '0',
	  `Day4` tinyint(4) DEFAULT '0',
	  `Day5` tinyint(4) DEFAULT '0',
	  `Day6` tinyint(4) DEFAULT '0',
	  `Day7` tinyint(4) DEFAULT '0',
	  `LightDefault` int(11) DEFAULT NULL,
	  `Createdby` bigint(20) DEFAULT NULL,
	  `CreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	  `Deleted` tinyint(1) DEFAULT '0',
	  `DeletedBy` bigint(20) DEFAULT NULL,
	  `DeletedOn` datetime DEFAULT NULL,
	  `ModifiedBy` bigint(20) DEFAULT NULL,
	  `ModiFiedOn` datetime DEFAULT NULL,
	  KEY `Formate` (`Formate`),
	  KEY `Action` (`ColorStatus`),
	  KEY `Deleted` (`Deleted`),
	  KEY `DeletedOn` (`DeletedOn`)
	) ";

	private $pmi_policy_cluster_mapping="
	CREATE TABLE IF NOT EXISTS `pmi_policy_cluster_mapping` (
	  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
	  `NodeID` bigint(20) DEFAULT NULL,
	  `PolicyID` bigint(20) DEFAULT NULL,
	  `PMIPolicyID` int(11) DEFAULT NULL,
	  `Synchronize` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	  PRIMARY KEY (`ID`)
	) ";

	private $pmi_policy_mapping="
	CREATE TABLE IF NOT EXISTS `pmi_policy_mapping` (
	  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
	  `ClusterID` bigint(20) DEFAULT NULL,
	  `PolicyID` bigint(20) DEFAULT NULL,
	  `SourceID` bigint(20) DEFAULT '0',
	  `Deleted` tinyint(4) DEFAULT '0',
	  `CreatedBy` bigint(20) DEFAULT NULL,
	  `CreatedOn` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
	  `DeletedBy` bigint(20) DEFAULT NULL,
	  `DeletedOn` datetime DEFAULT NULL,
	  PRIMARY KEY (`ID`),
	  KEY `ClusterID` (`ClusterID`),
	  KEY `PolicyID` (`PolicyID`),
	  KEY `Deleted` (`Deleted`),
	  KEY `CreatedOn` (`CreatedOn`),
	  KEY `DeletedOn` (`DeletedOn`)
	) ";

	private $pmi_policy_timeline="
	CREATE TABLE IF NOT EXISTS `pmi_policy_timeline` (
	  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
	  `PolicyID` bigint(20) DEFAULT NULL,
	  `Message` varchar(255) DEFAULT NULL,
	  `OnDate` datetime DEFAULT NULL,
	  PRIMARY KEY (`ID`),
	  KEY `PolicyID` (`PolicyID`),
	  KEY `OnDate` (`OnDate`)
	) ";

	private $pmi_remote_programming="
	CREATE TABLE IF NOT EXISTS `pmi_remote_programming` (
	  `ID` int(11) NOT NULL AUTO_INCREMENT,
	  `FirmwareFilename` varchar(255) DEFAULT NULL,
	  `FirmwareVersion` varchar(50) NOT NULL,
	  `TargetType` tinyint(4) DEFAULT NULL,
	  `TargetCluster` int(11) DEFAULT NULL,
	  `TargetNode` int(11) DEFAULT NULL,
	  `DownloadSent` tinyint(4) DEFAULT '0',
	  `DownloadSentOn` timestamp NULL DEFAULT NULL,
	  `ApplySent` tinyint(4) DEFAULT NULL,
	  `ApplySentOn` timestamp NULL DEFAULT NULL,
	  `VerifySent` tinyint(4) DEFAULT NULL,
	  `ApplyStartTime` datetime DEFAULT NULL,
	  `VerifySentOn` timestamp NULL DEFAULT NULL,
	  `DownloadSentCount` int(11) NOT NULL DEFAULT '0',
	  `DownloadDoneCount` int(11) NOT NULL DEFAULT '0',
	  `ApplySentCount` int(11) NOT NULL DEFAULT '0',
	  `ApplyDoneCount` int(11) NOT NULL DEFAULT '0',
	  `VerifySentCount` int(11) NOT NULL DEFAULT '0',
	  `VerifyDoneCount` int(11) NOT NULL DEFAULT '0',
	  `Deleted` tinyint(4) NOT NULL DEFAULT '0',
	  PRIMARY KEY (`ID`)
	) ";

	private $pmi_remote_programming_log="
	CREATE TABLE IF NOT EXISTS `pmi_remote_programming_log` (
	  `ID` int(11) NOT NULL AUTO_INCREMENT,
	  `RPID` int(11) NOT NULL,
	  `IPAddress` varchar(100) NOT NULL,
	  `DownloadVersion` varchar(100) NOT NULL,
	  `ApplyVersion` varchar(100) NOT NULL,
	  `DownloadVersionTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	  `ApplyVersionTime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
	  PRIMARY KEY (`ID`)
	)";

	private $pmi_response_messages="
	CREATE TABLE IF NOT EXISTS `pmi_response_messages` (
	  `ID` int(11) NOT NULL AUTO_INCREMENT,
	  `ResponseMessages` varchar(255) DEFAULT NULL,
	  `Status` varchar(255) DEFAULT NULL,
	  `Type` varchar(255) DEFAULT NULL,
	  `CreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	  `Deleted` tinyint(1) NOT NULL DEFAULT '0',
	  `Result` text,
	  PRIMARY KEY (`ID`)
	) ";

	private $pmi_role_master="
	CREATE TABLE IF NOT EXISTS `pmi_role_master` (
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `role_name` varchar(20) DEFAULT NULL,
	  `dev_role` tinyint(4) NOT NULL DEFAULT '0',
	  PRIMARY KEY (`id`)
	) ";

	private $pmi_sensor_setting="
	CREATE TABLE IF NOT EXISTS `pmi_sensor_setting` (
	  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
	  `ClusterID` bigint(20) DEFAULT NULL,
	  `tagID` bigint(20) DEFAULT NULL,
	  `EventID` int(10) DEFAULT NULL,
	  `Param1` int(10) DEFAULT '0',
	  `Param2` int(10) DEFAULT '0',
	  `Param3` int(10) DEFAULT '0',
	  `CreatedBy` bigint(20) DEFAULT NULL,
	  `CreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	  `ModifiedBy` bigint(20) DEFAULT NULL,
	  `ModifiedOn` datetime DEFAULT NULL,
	  PRIMARY KEY (`ID`),
	  KEY `ModifiedBy` (`ModifiedBy`),
	  KEY `ModifiedOn` (`ModifiedOn`)
	) ";

	private $pmi_shades="
	CREATE TABLE IF NOT EXISTS `pmi_shades` (
	  `ID` int(11) NOT NULL AUTO_INCREMENT,
	  `Name` varchar(100) NOT NULL,
	  `IPAddress` varchar(20) NOT NULL,
	  `ClusterID` int(11) NOT NULL,
	  `CurrentLevel` tinyint(4) NOT NULL DEFAULT '-1',
	  `LevelFetchedOn` timestamp NULL DEFAULT NULL,
	  `CreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	  `CreatedBy` int(11) NOT NULL,
	  `UpdatedOn` timestamp NULL DEFAULT '0000-00-00 00:00:00',
	  `UpdatedBy` int(11) DEFAULT NULL,
	  `Deleted` tinyint(4) NOT NULL DEFAULT '0',
	  PRIMARY KEY (`ID`)
	)";

	private $pmi_shade_schedule="
	CREATE TABLE IF NOT EXISTS `pmi_shade_schedule` (
	  `ID` int(11) NOT NULL AUTO_INCREMENT,
	  `Name` varchar(100) NOT NULL,
	  `ClusterID` int(11) NOT NULL,
	  `ShadeLevel` tinyint(11) NOT NULL,
	  `StartTime` time NOT NULL,
	  `EndTime` time NOT NULL,
	  `Day1` tinyint(4) NOT NULL,
	  `Day2` tinyint(4) NOT NULL,
	  `Day3` tinyint(4) NOT NULL,
	  `Day4` tinyint(4) NOT NULL,
	  `Day5` tinyint(4) NOT NULL,
	  `Day6` tinyint(4) NOT NULL,
	  `Day7` tinyint(4) NOT NULL,
	  `CreatedBy` int(11) NOT NULL,
	  `CreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	  `UpdatedOn` int(11) DEFAULT NULL,
	  `UpdatedBy` int(11) DEFAULT NULL,
	  `Deleted` tinyint(4) NOT NULL DEFAULT '0',
	  PRIMARY KEY (`ID`)
	)";

	private $pmi_smtp_config="
	CREATE TABLE IF NOT EXISTS `pmi_smtp_config` (
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `smtp_server` varchar(50) DEFAULT NULL,
	  `smtp_port` int(11) DEFAULT NULL,
	  `from_email` varchar(50) DEFAULT NULL,
	  `password` varchar(20) DEFAULT NULL,
	  `alert_notify` int(11) DEFAULT '0',
	  `status` int(11) DEFAULT '0',
	  `Synchronize` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	  PRIMARY KEY (`id`)
	)";

	private $pmi_tag="
	CREATE TABLE IF NOT EXISTS `pmi_tag` (
	  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
	  `Name` varchar(50) DEFAULT NULL,
	  `NodeID` bigint(20) DEFAULT NULL,
	  `TagTypeID` bigint(20) DEFAULT NULL,
	  `PeripheralID` int(11) DEFAULT NULL,
	  `TagRomID` int(11) DEFAULT NULL,
	  `IPAddress` varchar(50) DEFAULT NULL,
	  `Port` int(11) DEFAULT NULL,
	  `TagNum` bigint(20) DEFAULT NULL,
	  `SerialNum` bigint(20) DEFAULT NULL,
	  `Status` tinyint(1) NOT NULL DEFAULT '1',
	  `Programmed` tinyint(1) DEFAULT '0',
	  `StatusOn` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
	  `Acknowledged` tinyint(1) NOT NULL DEFAULT '0',
	  `Deleted` tinyint(1) NOT NULL DEFAULT '0',
	  PRIMARY KEY (`ID`),
	  KEY `NodeID` (`NodeID`),
	  KEY `TagTypeID` (`TagTypeID`),
	  KEY `Acknowledged` (`Acknowledged`),
	  KEY `Deleted` (`Deleted`),
	  KEY `StatusOn` (`StatusOn`),
	  KEY `StatusType` (`Status`)
	)";

	private $pmi_user="
	CREATE TABLE IF NOT EXISTS `pmi_user` (
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `firstname` varchar(50) DEFAULT NULL,
	  `lastname` varchar(50) DEFAULT NULL,
	  `email` varchar(50) DEFAULT NULL,
	  `phone` varchar(20) DEFAULT NULL,
	  `username` varchar(50) DEFAULT NULL,
	  `password` varchar(50) DEFAULT NULL,
	  `active` int(11) DEFAULT '0',
	  `added_by` int(11) DEFAULT NULL,
	  `deleted` int(11) DEFAULT '0',
	  `deleted_by` int(11) DEFAULT NULL,
	  `Synchronize` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	  PRIMARY KEY (`id`),
	  KEY `email` (`email`),
	  KEY `active` (`active`),
	  KEY `deleted` (`deleted`),
	  KEY `username` (`username`),
	  KEY `password` (`password`),
	  KEY `added_by` (`added_by`)
	)";

	private $pmi_user_roles="
	CREATE TABLE IF NOT EXISTS `pmi_user_roles` (
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `user_id` int(11) DEFAULT NULL,
	  `role_id` int(11) DEFAULT NULL,
	  `Synchronize` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	  PRIMARY KEY (`id`),
	  KEY `user_id` (`user_id`),
	  KEY `role_id` (`role_id`)
	)";
	
	function create($dbname)
	{
		
		if ( !DBC::createDB($dbname) )	return false;
		
		$db = new DBC(DB_HOST,DB_USER,DB_PASSWD,$dbname);
		$db->autocommit(false);
		
		$tables = array( $this->pmi_alert_definition,$this->pmi_alert_lookup,$this->pmi_at_policy,$this->pmi_cluster,$this->pmi_cluster_mapping,$this->pmi_communication,$this->pmi_device,$this->pmi_device_alerts,$this->pmi_device_log,$this->pmi_event,$this->pmi_inx_cloud,$this->pmi_map_nodes,$this->pmi_node,$this->pmi_outbound_log,$this->pmi_peripheral,$this->pmi_policy,$this->pmi_policy_cluster_mapping,$this->pmi_policy_mapping,$this->pmi_policy_timeline,$this->pmi_remote_programming,$this->pmi_remote_programming_log,$this->pmi_response_messages,$this->pmi_role_master,$this->pmi_sensor_setting,$this->pmi_shades,$this->pmi_shade_schedule,$this->pmi_smtp_config,$this->pmi_tag,$this->pmi_user,$this->pmi_user_roles );
		
		//print_r($tables);
		foreach($tables as $tblScr)
		{
			//echo $tblScr;
			if ( !$db->excSql($tblScr) ) {
				$db->rollback();
				return false;
			}
		}
		$db->commit();
		return true;
	}
}

?>
