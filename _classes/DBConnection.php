<?php
class DBConnection extends mysqli
{
    private $queryresult;

    //Create DB connection
    function __construct($host=DB_HOST,$dbUser=DB_USER,$dbPass=DB_PASSWD,$dbName=DB_NAME){ 
            parent::__construct($host,$dbUser,$dbPass,$dbName);
            //Connection is possible
            if (mysqli_connect_error())
            {
                    //die("Failed to connect to MySQL: " .mysqli_connect_error());
                    system("echo 'MySQL IS DOWN'| mail -s 'MYSQL IS DOWN - CRITICAL' operations@planitwith.me");
                    header('Location: 500.php');
            }
    }

    // All queries should be prepared and executed to ensure no SQL injection happens.
    public function update($query)
    {
            $error="";
            $res=0;
            if( $stmt = $this->prepare($query))	{
                    try	{
                            $stmt->execute();
                            $res=$stmt->affected_rows;
                            $stmt->close();
                    }
                    catch(Exception $ex)	{
                            $stmt->close();
                            throw $ex;
                    }
                    return $res;
            }
            throw new Exception("Query seems to be erroneous or malicious => ".$query);
    }

    function insert_query($data, $table)
    {
    $query = 'insert  into ' . $table . ' (';
            while (list($columns, ) = each($data)) {
                            $query .= $columns . ', ';
            }

    $query = substr($query, 0, -2) . ') values (';
    reset($data);
            echo $query."\n";
    while (list(, $value) = each($data))
    {
        switch ((string)$value) {
            case 'now()':
            $query .= 'now(), ';
            break;

            case 'null':
            $query .= 'null, ';
            break;

            default:
            $query .= '\'' . addslashes($value) . '\', ';
            break;
        }
    }
    $query = substr($query, 0, -2) . ')';
    $this->queryresult = $this->update($query);
    if ($this->queryresult) { return true; } else { return false; }
}

    function update_query($mas, $table, $whereFields)
    {
        if (is_array($whereFields))
        {
            while(list($idn,$idv)=each($whereFields))
            {
                $where[] = $idn."='$idv'";
            }
        }
        else
        {
            $where[] = "$whereFields";
        }

        while(list($k,$v)=each($mas))
        {
                $to[] = $k."='$v'";
        }

        $sql = "UPDATE $table SET ".implode(',',$to)." WHERE ".implode(" AND ",$where);
        return $this->query($sql);
    }

    function get_array($sql='')
    {
        if ($sql) {
                        $this->queryresult = $this->query($sql);
                        if ($this->queryresult) return $this->queryresult->fetch_assoc();
        }
        return array();
    }

    function free_result()
    {
        return $this->queryresult->free();
    }

    function get_result( $sql = '' )
    {
        if ($sql) {
			$this->queryresult=$this->query($sql);
			$c = 0;
			$res = array();
			while ($row = $this->queryresult->fetch_assoc())
			{
					$res[$c] = $row;
					$c++;
			}
			$this->free_result();
		}
        return $res;
    }

    function get_single_result( $sql = '',$col='')
    {
        if ($sql)
        {
            $this->queryresult=$this->query($sql);
            if ($this->queryresult->num_rows>0)
                        {
                                $row = $this->queryresult->fetch_assoc();
                                $this->free_result();
                                return @implode("",$row);
                        }
                        else return false;
        }
        else{
            return false;
        }
    }

    function get_result_array( $sql = '' )
    {
        $res = array();
        if ($sql)
        {
            $this->queryresult=$this->query($sql);
			$c = 0;
			while ($row = $this->queryresult->fetch_array())
			{
				$res[$c] = $row;
				$c++;
			}
			$this->free_result();
        }
        return $res;
    }

    function get_assoc_array( $sql = '' )
    {
        $res = array();
        if ($sql)
        {
            $this->queryresult=$this->query($sql);
			$c = 0;
			while ($row = $this->queryresult->fetch_assoc())
			{
				$res[$c] = $row;
				$c++;
			}
			$this->free_result();
		}
        return $res;
    }

    function get_double_array( $sql = '' )
    {
		$res = array();
        if ($sql) {
			$this->queryresult=$this->query($sql);
			while ($row = $this->queryresult->fetch_array())
			{
				$res[$row[0]] = $row[1];
			}
			$this->free_result();
		}
        return $res;
     }

    /** Escape string used in sql query */
    function sql_escape($msg)
    {
		return $this->real_escape_string($msg);
    }

    /*is query result set empty ?*/
    function is_empty($sql = '')
    {
        if ($sql) { $this->queryresult=$this->query($sql); }
        if ($this->queryresult && $this->queryresult->num_rows>0)	return true;
        else return false;
    }

    /*is query result set valid ?*/
    function not_empty($sql = '')
    {
        if ($sql) { $this->queryresult=$this->query($sql); }
        if ($this->queryresult && 0 == $this->queryresult->num_rows>0)
        {
                return false;
        }
        else
        {
                return true;
        }
    }

    function get_insert_id()
    {
        return $this->insert_id;
    }
}
?>
