<?php

include_once("config.php");
require_once('vendor/autoload.php');
require_once('UDP/PMIPacket.php');
require_once('UDP/UDP.php');
use UDPProcess\UDPQueueClient;

class SettingsController extends ControllerBase {
    # Constructor Method 

    function __constructor() {
        
    }

    /**************************************
     * GET SWITCH GRAPH DETAILS
     *************************************/
    Public function getSwitchGraph() {
        try {
            $db = new DBC();
            $filter = $_REQUEST['filter'];
            
            $qry = <<<EOF
                SELECT D.id AS DevID,D.device_name,D.oper_power_total,D.`status` FROM pmi_device D
                WHERE  D.id = $filter AND D.type = 1 
EOF;
            //echo $qry;die;
            $devResult = $db->get_result($qry);
            
            //print_r($result);
            if (sizeof($devResult) > 0) {
                $time = UTCTIME;
                $sql = <<<EOF
                    SELECT L.* , NOW()
                        FROM  pmi_device_log L 
                        WHERE L.DeviceID = $filter AND L.`Status`=1 AND L.LogDate >= (CONVERT_TZ(L.LogDate,'+00:00','$time')) - INTERVAL 1  MONTH
                        ORDER BY L.Port ASC
EOF;
               // die($sql);
			  
                $logResult = $db->get_result($sql);
                //print_r(  $logResult); 
                
                $sql = <<<EOF
                    SELECT SS.* 
                        FROM pmi_switch_schedule SS 
                        WHERE SS.DeviceID = $filter 
                    ORDER BY SS.Port ASC
EOF;
                $scheduleResult = $db->get_result($sql);
                
                $rtnResult["status"] = 1;
                $rtnResult["result"] = $devResult;
                $rtnResult["logResult"] = $logResult;
                $rtnResult["scheduleResult"] = $scheduleResult;
            } else
                $rtnResult["Status"] = 0;

            die(json_encode($rtnResult, TRUE));
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }
    
    /***************************************************************
     * Read updated schedule excel and update currectsopnding switch
     ***************************************************************/
    Public function updateSwitchSchedule() {
        $db = new DBC();
        $CMF = new CommonFunction();
        $db->autocommit(false);
        $fileName = FILE_PATH. (urldecode($_REQUEST['fileName']));
        $status = 0;
        
        try {

            if (file_exists($fileName)) {
               
                $excelObj = $CMF->readExcelSheet($fileName);
                $worksheet = $excelObj->getSheet(0);
                $lastRow = $worksheet->getHighestRow();
                $colmnCount = $worksheet->getHighestColumn();
                $startCount=3;
                $deletedIDs = array();
                $insertQuery = "INSERT INTO pmi_switch_schedule (DeviceID,Item,Name,`Floor`,Room,Rack,PatchPanel,`Power`,EnginePort,Port,CableID,CableType,Color,NodeSerialNum,Vendor,Model,`Type`,HWVer,SWVer,`Status`,TalkerPort,NonTalkerPort,LocationID,LocationName,LocationPath,DiscoveryCount,TooManyDiscoveries,PodID,PodStatus,CreatedBy,CreatedOn) VALUES ";
                
                if($lastRow > 1){
                    $ColumA1 = $worksheet->getCell('A1')->getFormattedValue();
                    $ColumB1 = $worksheet->getCell('B1')->getFormattedValue();
                    $createdOn = gmdate('Y-m-d H:i:s');
                    
                    //find startup row
                    if(($ColumA1 == "" && $ColumA1 ==  NULL) && ($ColumB1 == "" && $ColumB1 ==  NULL)) $startCount = 3;
                    elseif($ColumA1 == "item" && $ColumB1 == "Floor") $startCount = 2;
                    
                    for($i = $startCount; $i <= $lastRow; $i++){
                        
                        $item = $worksheet->getCell('A'.$i)->getFormattedValue();
                        $enginName = $worksheet->getCell('G'.$i)->getFormattedValue();
                        $enginePort = $worksheet->getCell('H'.$i)->getFormattedValue();
                        
                        //if row not empty
                        if($item != "" && $enginName != "" && $enginePort != ""){

                            $devDetails = $db->get_result("SELECT id,device_name FROM pmi_device WHERE `type`=1 AND device_name='". urlencode($enginName) ."'");
                            
                            //if device exists
                            if(sizeof($devDetails) > 0){
                                if(sizeof($deletedIDs) > 0) $insertQuery .= ",";
                                //Device ID
                                $insertQuery .= "(".$devDetails[0]["id"].",";
                                
                                //Item
                                $insertQuery .= ($item != "" ? $item : 0).",";
                                
                                //Name
                                $name = $worksheet->getCell('L'.$i)->getFormattedValue();
                                $insertQuery .= ($name != "" ? ("'".urlencode($name)."'") : "NULL").",";
                                
                                //Floor
                                $floor = $worksheet->getCell('B'.$i)->getFormattedValue();
                                $insertQuery .= ($floor != "" ? $floor : 0).",";
                                
                                //Room
                                $room = $worksheet->getCell('C'.$i)->getFormattedValue();
                                $insertQuery .= ($room != "" ? $room : 0).",";
                                
                                //Rack
                                $rack = $worksheet->getCell('D'.$i)->getFormattedValue();
                                $insertQuery .= ($rack != "" ? $rack : 0).",";
                                
                                //PatchPanel
                                $patchPanel = $worksheet->getCell('E'.$i)->getFormattedValue();
                                $insertQuery .= ($patchPanel != "" ? ("'".urlencode($patchPanel)."'") : "NULL").",";
                                
                                //Power
                                $power = $worksheet->getCell('F'.$i)->getFormattedValue();
                                $insertQuery .= ($power != "" ? ("'".urlencode($power)."'") : "NULL").",";
                                
                                //EnginePort && //Port
                                if($enginePort != ""){
                                    $insertQuery .= "'". urlencode($enginePort) ."',";
                                    
                                    $prot = explode("-", $enginePort);
                                    if(sizeof($prot) > 1) $insertQuery .= $prot[1].",";
                                    else $insertQuery .= "0,";
                                }
                                else {
                                    $insertQuery .= "NULL,";
                                    $insertQuery .= "0,";
                                }
                                
                                //CableID
                                $cableID = $worksheet->getCell('I'.$i)->getFormattedValue();
                                $insertQuery .= ($cableID != "" ? ("'".urlencode($cableID)."'") : "NULL").",";
                                
                                //CableType && //Color
                                $cabletype = $worksheet->getCell('J'.$i)->getFormattedValue();
                                if($cabletype != ""){
                                    $cabletypeSplit = explode("/", $cabletype);
                                    if(sizeof($cabletypeSplit) > 0) $insertQuery .= "'". urlencode($cabletypeSplit[0]) ."',";
                                    else $insertQuery .= "NULL,";
                                    if(sizeof($cabletypeSplit) > 1) $insertQuery .= "'". urlencode($cabletypeSplit[1]) ."',";
                                    else $insertQuery .= "NULL,";
                                }
                                else {
                                    $insertQuery .= "NULL,";
                                    $insertQuery .= "0,";
                                }
                                //NodeSerialNum
                                $nodeSerialNum = $worksheet->getCell('K'.$i)->getFormattedValue();
                                $insertQuery .= ($nodeSerialNum != "" ? ("'".urlencode($nodeSerialNum)."'") : "NULL").",";
                                
                                //Vendor
                                $vendor = $worksheet->getCell('M'.$i)->getFormattedValue();
                                $insertQuery .= ($vendor != "" ? ("'".urlencode($vendor)."'") : "NULL").",";
                                
                                //Model
                                $model = $worksheet->getCell('N'.$i)->getFormattedValue();
                                $insertQuery .= ($model != "" ? ("'".urlencode($model)."'") : "NULL").",";
                                
                                //Type
                                $type = $worksheet->getCell('O'.$i)->getFormattedValue();
                                $insertQuery .= ($type != "" ? ("'".urlencode($type)."'") : "NULL").",";
                                
                                //HWVer
                                $hwVer = $worksheet->getCell('P'.$i)->getFormattedValue();
                                $insertQuery .= ($hwVer != "" ? ("'".urlencode($hwVer)."'") : "NULL").",";
                                
                                //SWVer
                                $swVer = $worksheet->getCell('Q'.$i)->getFormattedValue();
                                $insertQuery .= ($swVer != "" ? ("'".urlencode($swVer)."'") : "NULL").",";
                                
                                //Status
                                $status = $worksheet->getCell('R'.$i)->getFormattedValue();
                                $insertQuery .= ($status != "" ? ("'".urlencode($status)."'") : "NULL").",";
                                
                                //TalkerPort
                                $tport = $worksheet->getCell('T'.$i)->getFormattedValue();
                                $insertQuery .= ($tport != "" ? ("'".urlencode($tport)."'") : "NULL").",";
                                
                                //NonTalkerPort
                                $nTPort = $worksheet->getCell('U'.$i)->getFormattedValue();
                                $insertQuery .= ($nTPort != "" ? ("'".urlencode($nTPort)."'") : "NULL").",";
                                
                                //LocationID
                                $locationID = $worksheet->getCell('V'.$i)->getFormattedValue();
                                $insertQuery .= ($locationID != "" ? $locationID : "NULL").",";
                                
                                //LocationName
                                $loactionName = $worksheet->getCell('W'.$i)->getFormattedValue();
                                $insertQuery .= ($loactionName != "" ? ("'".urlencode($loactionName)."'") : "NULL").",";
                                
                                //LocationPath
                                $loactionPath = $worksheet->getCell('X'.$i)->getFormattedValue();
                                $insertQuery .= ($loactionPath != "" ? ("'".urlencode($loactionPath)."'") : "NULL").",";
                                
                                //DiscoveryCount
                                $discoveryCount = $worksheet->getCell('Y'.$i)->getFormattedValue();
                                $insertQuery .= ($discoveryCount != "" ? $discoveryCount : "0").",";
                                
                                //TooManyDiscoveries
                                $tDiscovery = $worksheet->getCell('Z'.$i)->getFormattedValue();
                                $insertQuery .= ($tDiscovery != "" ? ("'".urlencode($tDiscovery)."'") : "NULL").",";
                                
                                //PodID
                                $podID = $worksheet->getCell('AA'.$i)->getFormattedValue();
                                $insertQuery .= ($podID != "" ? ("'".urlencode($podID)."'") : "NULL").",";
                                
                                //PodStatus
                                $podStatus = $worksheet->getCell('AB'.$i)->getFormattedValue();
                                $insertQuery .= ($podStatus != "" ? ("'".urlencode($podStatus)."'") : "NULL").",";
                                
                                //CreateBy
                                $insertQuery .= $_SESSION['LOGGED_USER_ID'].",";
                                
                                //CreatedOn
                                $insertQuery .= "'".$createdOn."')";
                                
                                //Deleted records IDs
                                if(! in_array($devDetails[0]["id"], $deletedIDs )) array_push($deletedIDs, $devDetails[0]["id"]);
                                
                            }
                            
                        }
                    }
                    if(sizeof($deletedIDs) > 0){
                        $delIDs = "";
                        foreach ($deletedIDs as $value) { $delIDs .= ($delIDs != "" ? "," : "").$value;}
                        $delSql = "DELETE FROM pmi_switch_schedule WHERE DeviceID IN (".$delIDs.")";
                        
                        //Delete exixted records
                        $db->query($delSql);
                        
                        //Inser new records
                        $db->query($insertQuery);
                        $status = 1;
                    }
                }
                $db->commit();
                
            } else {
                echo "The file $fileName does not exist \n\n\n";
            }
            unlink($fileName);
            $rtnResult["msg"] = ($status == 0)?'Failed to update records':'Successfully updated records';
			$rtnResult["Status"] = $status;
            die(json_encode($rtnResult, TRUE));
        } catch (Exception $exc) {
            echo $exc->getMessage();
            $db->rollback();
            $rtnResult["Status"] = $status;
            die(json_encode($rtnResult, TRUE));
        }
        $db->close();
    }
    
    /***************************************************************
     * Remove existing file
     ***************************************************************/
    Public function removeExistFile() {
        $fileName = FILE_PATH. (urldecode($_REQUEST['fileName']));
        $status = 0;
        try {

            if (file_exists($fileName)) {
               
                unlink($fileName);
                $status = 1;
            }
            $rtnResult["Status"] = $status;
            die(json_encode($rtnResult, TRUE));
        } catch (Exception $exc) {
            throw $exc->getMessage();
        }
    }
    
    /***********************************
     * Update switch name from database
     ***********************************/
    Public function saveSwitchName() {
        $db = new DBC();
        $db->autocommit(false);
        $id = $_REQUEST['deviceID'];
        $name = $_REQUEST['name'];
        $status = 0;
        
        try {
            $updateArr["device_name"] = urlencode(trim($name));
            $where["id"] = $id;
            $status = $db->update_query($updateArr, "pmi_device", $where);
            $db->commit();
            $rtnResult["Status"] = $status;
			$rtnResult["message"] = ($status == 0)?'Failed to update name' : 'Successfully updated name';
            die(json_encode($rtnResult, TRUE));
        } catch (Exception $exc) {
            echo $exc->getMessage();
            $db->rollback();
            $rtnResult["Status"] = $status;
            die(json_encode($rtnResult, TRUE));
        }
        $db->close();
    }
    
    /******************************
     * Get first switch element
     *****************************/
    public function  getSwitchSort(){
        $db = new DBC();
        $status = 0;
        try {
            $sql= "SELECT id,device_name FROM pmi_device WHERE `type`=1 AND `status` != 2 AND deleted=0  AND (device_name!=NULL OR device_name!='') ORDER BY device_name";
            $result = $db->get_result($sql);
            $devArr = array();
            
            if(sizeof($result) > 0 ){
                for ($i= 0; $i < sizeof($result); $i++) {
                    $devName = $result[$i]["device_name"];
                    $splitName = explode("-", $devName);
                    $devNo = $splitName[1];
                    array_push($devArr, array("DevNo"=>$devNo,"ID" => $result[$i]["id"],"Name" => $devName));
                }
                $status = 1;
            }
            else $status = 0;
            
            $res["result"] = $devArr;
            $res["status"] = $status;
            
            die(json_encode($res, TRUE));
        } catch (Exception $ex) {
            //echo $exc->getMessage();
            $status = 0;
            die(json_encode(array("status" => $status ), TRUE));
        }
    }

    public function retoreToDefault(){
        $db = new DBC();
        $status = 0;
        $nodeType = FIXTURE_TYPE_NODE;
        try {
            $sql =<<<EOF
                 SELECT ID,Name,IPAddress FROM pmi_tag WHERE Acknowledged=1 AND Deleted=0 AND TagTypeID=$nodeType
EOF;
            $nodeArr = $db->get_result($sql);
            if(sizeof($nodeArr) > 0){
                $dbrec = array();
                $dbrec["Type"] = 4; //restore
                $dbrec["TypeID"] = 0;
                $dbrec["CreatedBy"] = $_SESSION['LOGGED_USER_ID'];
                $dbrec["CreatedOn"] = date("Y-m-d H:m:s");
                $status = $db->insert_query($dbrec, "pmi_merge_request");
                $packeReqID = $db->get_insert_id();
				
                for ($i = 0; $i < sizeof($nodeArr); $i++) {
                    $data = array("tagID" =>$nodeArr[$i]["ID"],"Name"=>$nodeArr[$i]["Name"],"IPAddress"=>$nodeArr[$i]["IPAddress"],"requestedID"=>$packeReqID);
                    $notQue = new UDPQueueClient(NOTIF_QUEUE_SERVER,SYSTEMLOG_QUEUE_NAME);
                    $notQue->queueNotificationRequest(RESTORE_TO_DEFAULT_REQ, $data);
                }
                $status = 1;
            }
        } catch (Exception $exc) {
            //echo $exc->getMessage();
        }
        die(json_encode(array("status" => $status)));
    }
    Public function wordsearch() {
		$search = $_REQUEST['search'];
		//echo "search .... ".$search.".....";
        $status = 0;
        try {
			if (!empty($search)){ 
				$lines = file('files/QAListAK.txt');
				$found = false;
				foreach($lines as $line)
				{
				  if(strpos($line, $search) !== false)
				  {
						$found = true;
						$devArr = array();
						//echo $line;
						$resultArr = explode("**********", $line);
						if(sizeof($resultArr) > 0){

							$count = 0;
							$htmStr = "";
							foreach ($resultArr as $item) {
								$splitItem = explode("||", $item);
								$question = $splitItem[0];
								$answer = $splitItem[1];
								if(!empty($question)){
									$count++;
									array_push($devArr, array("count"=>$count,"question" => $question,"answer" => $answer));				
								}
							}
							$status = 2;	// object found 
						}
				  }
				  	
				}
				// If the text was not found, show a message
				if(!$found)
				{
					$status = 3;
				}							
			}
			else {
				$result = file_get_contents("files/QAListAK.txt");	
				$resultArr = explode("**********", $result);	
				$devArr = array();
				if(sizeof($resultArr) > 0){

					$count = 0;
					foreach ($resultArr as $item) {
						$splitItem = explode("||", $item);
						$question = $splitItem[0];
						$answer = $splitItem[1];
						if(!empty($question)){
							$count++;
							array_push($devArr, array("count"=>$count,"question" => $question,"answer" => $answer));
						}
					}
					
				}
				$status = 1;		// object default 
			}
			
            $rtnResult["Status"] = $status;
			$rtnResult['word'] = $search;
			$rtnResult['lines'] = $devArr;
            die(json_encode($rtnResult, TRUE));
        } catch (Exception $exc) {
            throw $exc->getMessage();
        }
    }
	
    public function clearTag(){
        $db = new DBC();
        $db->autocommit(false);
        $retnArr = array("status" => 0);
        $nodeType = FIXTURE_TYPE_NODE;
        try {
            if($_SESSION['LOGGED_USER_ROLE_ID'] == ADMIN_ROLE){
                $sql =<<<EOF
                     SELECT ID,Name,IPAddress FROM pmi_tag WHERE Acknowledged=1 AND Deleted=0 AND TagTypeID=$nodeType
EOF;
                $nodeArr = $db->get_result($sql);
                 $truncateSQL = array(
                    "pmi_alert_definition",
                    "pmi_alert_lookup",
                    "pmi_cluster_mapping",
                    "pmi_device_alerts",
                    "pmi_event",
                    "pmi_map_nodes",
                    "pmi_merge_request",
                    "pmi_node",
                    "pmi_node_savings",
                    "pmi_node_status",
                    "pmi_override",
                    "pmi_override_cluster_mapping",
                    "pmi_packet_queue",
                    "pmi_policy_line_mapping",
                    "pmi_policy_mapping",
                    "pmi_policy_node_mapping",
                    "pmi_tag",
                    "pmi_tag_status");
                foreach ($truncateSQL as $value) {
                     $db->excSql("TRUNCATE TABLE $value");
                }
                $this->dao->excSql("INSERT INTO `pmi_mapping_pid` (`MappID`) VALUES (4)");

                $udpHelper = new UDP();
                $requestPkt = new PMIPacket(null,null);
                $cmf = new CommonFunction();
                
                $ipAdress = shell_exec("ifconfig | awk '/broadcast / {print $6}'");
                $requestPkt->packetId = $cmf->getUDPPacketID($db); // uniqe id between 5 to 255 

                $requestPkt->tagIdHighByte = $udpHelper->getHiByte(0);
                $requestPkt->tagIdLowByte = $udpHelper->getLoByte(0);

                // Set Parameter Can be any order 
                $requestPkt->setPacketType(PT_COMMANDS);
                $requestPkt->setCommand(CLEAR_TAG);

                $udpHelper->broadcast($ipAdress,$requestPkt,NODE_LISTENER_PORT);
                $retnArr["status"] = 1;
            }
            $db->commit();
        } catch (Exception $exc) {
            $db->rollback();
            echo $exc->getMessage();
        }
        $db->close();
        die(json_encode($retnArr));
    }
	    /******************************
     *  Clear Alert 
     ******************************/
    public function ClearAlert(){
        $db = new DBC();
        $db->autocommit(false);
        $retnArr = array("status" => 0,"message"=>'Failed to remove alert');

        $dbID = $_REQUEST["dbID"];
		$elm = $_REQUEST["elm"];		// 1 is single alert 2= all cluster 

		try {
			 if($_SESSION['LOGGED_USER_ROLE_ID'] == ADMIN_ROLE){
				if ($elm == 2) {
					$db->excSql("TRUNCATE TABLE pmi_device_alerts");
					$retnArr["status"] = 2;
					$retnArr["message"] = 'Successfully removed all alerts';
				}
				else if ($elm == 1) {
					 $delSql = "DELETE FROM pmi_device_alerts WHERE ID=$dbID ";
                    $db->query($delSql);
					$retnArr["status"] = 1;
					$retnArr["message"] = 'Successfully removed alert';
				}               
			}
            $db->commit();
        } catch (Exception $exc) {
            $db->rollback();
            echo $exc->getMessage();
        }
        $db->close();
        die(json_encode($retnArr));
		
    }
		    /******************************
     * SEND COMMENT EMAIL TO SUPPORT
     *****************************/
	public function sendEmail(){
		$name 		= $_REQUEST['Name'];
		$company 	= $_REQUEST['CompanyName'];
		$phone 		= $_REQUEST['PhoneNumber'];
		$email 		= $_REQUEST['Email'];
		$body 		= $_REQUEST['Comment'];
		$cntname 		= $_REQUEST['contrct'];
		$Ttitle 		= $_REQUEST['Title'];
		//echo "name:".$name.".......company:".$company."....email:".$email."......body:".$body;
		//echo "ttle:".$Ttitle;
		$emailTemplate="";
		$this->emailTemplate = file_get_contents('tpl/emailTemplate.html');
		$msgHtmlContent = "<b>Request From </b>: ".$name."<br>";
		$msgHtmlContent .= "<b>Company Name</b>: ".$company."<br>";
		$msgHtmlContent .= "<b>Phone Number</b>: ".$phone."<br>";
		$msgHtmlContent .= "<b>Issue Title</b>: ".$Ttitle."<br>";
		$msgHtmlContent .= "<b>Discription </b>: <br>";
		$msgHtmlContent .= "<b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$body."</b><br><br>";
		$msgText =  ""  ;           
		$messgHtml = str_replace("[[EMAIL_TITLE]]",$msgText,$this->emailTemplate);
		$messgHtml = str_replace("[[EMAIL_CONTENT]]",$msgHtmlContent,$messgHtml);
			//echo $messgHtml;
        $mail = new PHPMailer();
        $status = 0;	
		$mail->IsSMTP();
		$mail->CharSet="UTF-8";
		$mail->SMTPSecure = 'tsl';
		$mail->Host = 'smtp.gmail.com';
		$mail->Port = 587;
		$mail->IsHTML(true);
		$mail->Username = 'inspextor.help@gmail.com';
		$mail->Password = 'pmi123456';
		$mail->SMTPAuth = true;

		$mail->setFrom = 'inspextor.help@gmail.com';
		$mail->FromName = 'inspeXtor';
		$mail->AddAddress('inspextor.help@gmail.com');
		$mail->AddAddress($email);
		//$mail->AddReplyTo('phoenixd110@gmail.com', 'Information');
		$ticket_num = rand(1,302000000);	// same as hitting jackpot 
		$mail->Subject    =  "Ticket ".$ticket_num." inquiry from ".$cntname;
		$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!";
		$mail->Body    = $messgHtml;

		
		if($mail->Send())$status = 1;
		

        die(json_encode(array("status" => $status)));
    }
	//------------------set power to switchs ------------------------------
	public function setPower(){
        $db = new DBC();
        $retnArr = array("status" => 0,'message' => 'Failed to set power switche(s)');

        try {
			//----------------------------find id and IP and name ---------------------------------------------
			$sql = "select id,device_name,ip_address,model_no from pmi_device where deleted=0 and type=1";
			$ciscoDevArr = $db->get_result($sql);
			$devCount = count($ciscoDevArr);
			//echo "devcount".$devCount."\n";
			
            if(sizeof($ciscoDevArr) > 0){	
				
				for ($i=0; $i<$devCount; $i++) {					
					$data = array(
						"ciscoID" => $ciscoDevArr[$i]["id"],
						"ciscoIP" => $ciscoDevArr[$i]["ip_address"],
						"ciscoName" => $ciscoDevArr[$i]["device_name"],
						"num_port" => $ciscoDevArr[$i]["model_no"],
						"action"=> 1	 
					);
					
					$notQue = new UDPQueueClient(NOTIF_QUEUE_SERVER,SYSTEMLOG_QUEUE_NAME);
					$notQue->queueNotificationRequest(CHECK_PORT, $data);
					$retnArr["status"] = 1;				
					$retnArr["message"] = 'Successfully set power to active switche(s)';
				}				
			}

        } catch (Exception $exc) {
            $db->rollback();
        }
        $db->close();
        die(json_encode($retnArr));
    }
}
?>

