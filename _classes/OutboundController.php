<?php
	include_once("config.php");

	class OutboundController extends ControllerBase
	{

		function __constructor(){
		}

		// Save data to array
		public function saveOutboundData($data) {
				// Return flag default.
				$return = 0;

				try {
					// Create DB Instance of cloud db.
					$dbOutboud = new DBC();

					// Prepare Data with default status = 0 ( pending )
					$dataToInsert = array();
					$dataToInsert['Data'] = json_encode($data);

					// Save to database
					if( $dbOutboud->insert_query($dataToInsert, 'pmi_outbound_log') ){
							// Get last inserted log 
							$outLogId = $dbOutboud->get_insert_id();
							
							// Success
							$return = $outLogId;
					}
					// IF
				}
				catch(Exception $ex){
						$return = 0;
				}

				// Return
				return $return;
		}
		// End save data

		// Save data to array
		public function getInstance($instanceId) {
				// Return flag default.
				$return = array();

				try {
					// Create DB Instance of cloud db.
					$dbOutboud = new DBC(DB_HOST,DB_USER,DB_PASSWD,DB_NAME_CLOUD);

					// Prepare Data with default status = 0 ( pending )
					$return = $dbOutboud->get_result("SELECT * FROM pmi_inx_device WHERE inspextorID = '$instanceId'");

				}
				catch(Exception $ex){
				}

				// Return
				return $return;
		}

		// Save data to array
		public function getOneLocked($status=0, &$dbOutboud) {
				// Return flag default.
				$return = array();

				try {

					// Prepare Data with default status = 0 ( pending )
					$return = $dbOutboud->get_result("SELECT * FROM pmi_outbound_log WHERE `Status` = $status ORDER BY ID ASC LIMIT 1");

				}
				catch(Exception $ex){
				}

				// Return
				return $return;
		}
		// End save data
		
		// Get by ID 
		public function getLogReply( $id ) {
				// Return flag default.
				$return = array();

				try {
					// Create DB Instance of cloud db.
					$dbOutboud = new DBC();
					// echo "SELECT * FROM pmi_outbound_log WHERE ID = $id and `Status` = 3";
					// Prepare Data with default status = 0 ( pending )
					$return = $dbOutboud->get_result("SELECT * FROM pmi_outbound_log WHERE ID=$id and `Status`=3");

				}
				catch(Exception $ex){
				}
				//print_r($return);

				// Return
				return $return;
		}
		// End save data


		// Save data to array
		public function updateOneAndUnlock($status, $id, &$dbOutboud) {
				// Return flag default.
				$return = false;

				try {
					// Prepare Data with default status = 0 ( pending )
					$dataToUpdate = array();
					$dataToUpdate['Status'] = $status;

					$dataToUpdateCondition = array();
					$dataToUpdateCondition["ID"]= $id;
									
					// Save to database
					if( $dbOutboud->update_query($dataToUpdate, 'pmi_outbound_log', $dataToUpdateCondition) ){
							// Success
							$return = true;
					}
					// IF
				}
				catch(Exception $ex){
					var_dump($ex);
						$return = false;
				}

				// Return
				return $return;
		}
		// End save data

	}
?>
