<?php
    include_once("config.php");
    //include_once("_classes/curl.php");
    require_once 'excel/PHPExcel.php';

    class DeviceController extends ControllerBase
    {
            # Constructor Method
            function __constructor(){
            }
			
			function claimInstance()
			{
				$result['status'] = '0';
				$db = new DBC();
				
				$insId = $_REQUEST['insID'];
                $insPass = $_REQUEST['insPass'];
                $devInfo = $db->get_result("select * from pmi_inx_device where inspextorID ='$insId' and Password='$insPass' and deleted = 0 and Claimed='N'");
                if ( $devInfo[0]["id"] == "" )	$result['message'] = 'Inspextor instance not found';
				else {
					$devInfo[0]["Claimed"]='Y';
					$where["id"] = $devInfo[0]["id"];
					unlink($devInfo[0]["id"]);
					//print_r($devInfo);
					$db->update_query($devInfo[0] , 'pmi_inx_device',$where);
					$result['status']=1;
				}
				die(json_encode($result));
			}
			
            //Add New Device Alert
            function addDeviceAlert()
            {
                $db = new DBC();

                $loggedUserId   = $_SESSION['LOGGED_USER_ID'];
                $deviceId       = $_REQUEST['device'];
                $parameter      = $_REQUEST['parameter'];
                $condition      = $_REQUEST['condition'];
                $fromValue      = $_REQUEST['fromValue'];
                $toValue        = $_REQUEST['toValue'];
                $type           = $_REQUEST['type'];
                $severity       = $_REQUEST['severity'];
                $description    = $_REQUEST['description'];
                if($_REQUEST['userNotified']!=0){
                    $sendMail       = implode(",",$_REQUEST['userNotified']);
                }
                else{
                    $sendMail = "";
                }

                //Check Duplicate Exist
                //$existsql = "select id from pmi_alert_definition where device_id='$deviceId' and parameter='$parameter' and parameter_condition='$condition' and deleted=0";
                $existsql = "select id from pmi_alert_definition where device_id='$deviceId' and parameter='$parameter' and severity=$severity and deleted=0";
                $existAlert = $db->get_single_result($existsql);

                if($existAlert[0]!=""){
                    $result['status'] = '0';
                    $result['message'] = "Already defined an Alert for this Parameter condition.";
                    die(json_encode($result));
                }

                $dbrec = array();
                $dbrec["device_type"]         = $type;
                $dbrec["device_id"]         = $deviceId;
                $dbrec["parameter"]         = $parameter;
                $dbrec["parameter_condition"]         = $condition;
                $dbrec["parameter_from_value"]    = $fromValue;
                if($toValue != "") $dbrec["parameter_to_value"]    = $toValue;
                $dbrec["severity"]          = $severity;
                $dbrec["description"]       = $description;
                $dbrec["user_notified"]     = $sendMail;
                $dbrec["added_by"]          = $loggedUserId;

                $addDeviceAlert = $db->insert_query($dbrec , 'pmi_alert_definition');

                if($addDeviceAlert){
                    $result['status'] = '1';
                    $result['message'] = "Successfully added Device Alert";
                    die(json_encode($result));
                }
                else{
                    $result['status'] = '0';
                    $result['message'] = "Failed to add Device Alert";
                    die(json_encode($result));
                }
            }

            //Fetch All Alerts
            function fetchAllAlertInfo()
            {

                $db = new DBC();
                $CMF = new CommonFunction();

                $limit = $_REQUEST['iDisplayLength'];
                if ( isset( $_REQUEST['iDisplayStart'] ))
                {
                    $offset =  $_REQUEST['iDisplayStart'] ;
                }
                $sql = "select a.*,IF(a.device_type = 2,N.Name,C.Name )AS DeviceName from pmi_alert_definition a left join pmi_node N on a.device_id=N.ID AND N.Deleted=0 left join pmi_tag T on N.ID = T.NodeID AND T.TagTypeID = ".FIXTURE_TYPE_NODE." and T.Deleted=0 LEFT JOIN pmi_cluster C ON a.device_id = C.ID AND C.Deleted=0 where a.deleted=0   limit ". $offset .",". $limit;
                //die($sql);
              //  print_r( $db->get_result($sql) );

                $result["Results"] = $db->get_result($sql);
                $qry = "select count(*) as totalRec from pmi_alert_definition where deleted = 0 ";
                $totalRecords = $db->get_result($qry);
                $result["TotalRecords"] = $totalRecords[0]["totalRec"];

                if(sizeof($result)!= "")
                {
                    $CMF->setDataTableContent($result);
                }
                else {
                     echo 0;
                }
            }

            //Fetch Alert Data to Edit
            function fetchAlertData()
            {
                $db = new DBC();

                $alertId = $_REQUEST['alertId'];

                $sql = "select * from pmi_alert_definition where id='$alertId'";
                $editAlertData = $db->get_result($sql);
                $userNotified   =   array();
                $userNotified   =   explode(",",$editAlertData[0]['user_notified']);

                if($editAlertData){
                    $result['status']       = '1';
                    $result['type']       = $editAlertData[0]['device_type'];
                    $result['device']       = $editAlertData[0]['device_id'];
                    $result['parameter']    = $editAlertData[0]['parameter'];
                    $result['condtn']       = $editAlertData[0]['parameter_condition'];
                    $result['fromValue']        = $editAlertData[0]['parameter_from_value'];
                    $result['toValue']        = $editAlertData[0]['parameter_to_value'];
                    $result['severity']     = $editAlertData[0]['severity'];
                    $result['description']  = $editAlertData[0]['description'];
                    $result['sendmail']     = $userNotified;
                    die(json_encode($result));
                }
                else{
                    $result['status'] = '0';
                    die(json_encode($result));
                }

            }

            //Update Alert Details
            function updateAlert(){
                $db = new DBC();

                $alertId        = $_REQUEST['alertId'];
                $deviceId       = $_REQUEST['device'];
                $parameter      = $_REQUEST['parameter'];
                $condition      = $_REQUEST['condition'];
                $fromValue      = $_REQUEST['fromValue'];
                $toValue        = $_REQUEST['toValue'];
                $severity       = $_REQUEST['severity'];
                $description    = $_REQUEST['description'];
                $type           = $_REQUEST['type'];

                if($_REQUEST['userNotified']!=0){
                    $sendMail       = implode(",",$_REQUEST['userNotified']);
                }
                else{
                    $sendMail = "";
                }


                $dbrec = array();
                $where = array();

                $where["id"] = $alertId;

                $dbrec["device_type"]        = $type;
                $dbrec["device_id"]          = $deviceId;
                $dbrec["parameter"]          = $parameter;
                $dbrec["parameter_condition"]= $condition;
                $dbrec["parameter_from_value"]    = $fromValue;
                if($toValue != "") $dbrec["parameter_to_value"]    = $toValue;
                $dbrec["severity"]           = $severity;
                $dbrec["description"]        = mysqli_real_escape_string($db,$description);;
                $dbrec["user_notified"]      = $sendMail;

                $updateAlert = $db->update_query($dbrec , 'pmi_alert_definition',$where);

                if($updateAlert){
                    $result['status']       = '1';
                    $result['message']      = 'Successfully updated device alert';
                    $result['id']           = $alertId;
                    $result['device']       = $deviceId;
                    $result['parameter']    = $parameter;
                    $result['condtn']       = $condition.$value;
                    $result['severity']     = ($severity == 1)?"Critical":"Caution";
                    $result['description']  = $description;
                    die(json_encode($result));
                }
                else{
                    $result['status']       = '0';
                    $result['message']      = 'Failed to update device alert';
                    die(json_encode($result));
                }
            }

            //Delete Alert Information
            function deleteAlertInfo()
            {
                $db = new DBC();

                $delteId        = $_REQUEST['delId'];
                $loggedUserId   = $_SESSION['LOGGED_USER_ID'];

                if($delteId){
                    $dbrec = array();
                    $where = array();

                    $where["id"] = $delteId;

                    $dbrec["deleted"]         = 1;
                    $dbrec["deleted_by"]      = $loggedUserId;

                    $updateAlert = $db->update_query($dbrec , 'pmi_alert_definition',$where);
                }
                if($updateAlert){
                    $result['status'] = '1';
                    $result['message'] = 'Successfully deleted alert';
                    die(json_encode($result));
                }
                else{
                    $result['status'] = '0';
                    $result['message'] = 'Failed to delete alert';
                    die(json_encode($result));
                }

            }

            //Get All Active Device Details
            function getActiveDevices()
            {
                $db = new \DBC(DB_HOST,DB_USER,DB_PASSWD,DB_NAME_CLOUD);
				$loggedUser = $_SESSION['LOGGED_USER_ID'];
				 $sql1 = "select ClientID from pmi_user where deleted = 0 and id='$loggedUser' ";
                $userExist = $db->get_single_result($sql1);
				if ($userExist >0 ){
					$client = $userExist['ClientID'];
					$sql = "select * from pmi_inx_device where deleted=0 and ClientID = '$client'";
					$allActiveDevices = $db->get_result($sql);
				}
                else 
				{
					$sql = "select * from pmi_inx_device where deleted=0";
					$allActiveDevices = $db->get_result($sql);
				}

                if(sizeof($allActiveDevices)>0){
                    $result['status']       = '1';
                    $result['allDevices']   = $allActiveDevices;
                    $result['activeDeviceID']   = isset($_SESSION["activeDeviceID"]) ? $_SESSION["activeDeviceID"] : 0 ;
                    die(json_encode($result));
                }
                else{
                    $result['status']       = '0';
                    die(json_encode($result));
                }
            }
			
			function sendDeviceClaimDetails()
			{
				$db = new \DBC(DB_HOST,DB_USER,DB_PASSWD,DB_NAME_CLOUD);
				$CMF = new CommonFunction();
				
				$devId = $_REQUEST['devId'];
                $devRec = $db->get_result("select inspextorID,Password,ReportMailID,ClientID from pmi_inx_device where id=$devId");
                $clientId=$devRec[0]['ClientID'];
                $instanceId=$devRec[0]['inspextorID'];
                $instancePass=$devRec[0]['Password'];
                $tomail = $devRec[0]['ReportMailID'];
                
                $subject = "Instructions for registering and claiming your Inspextor instance on cloud";
                
                $HTMLContent = "Hi $tomail,<br/>
                <br/>
                Your Inspextor instance has been allocated on the cloud. The credentials needed to register and claim your instance are as follows: <br/><br/>
                InstanceID: $instanceId<br/>
                InstancePassword: $instancePass<br/>
                <br/> 
                Please follow this 2 step process for registering and claiming your inspextor instance on the cloud.<br/>
                <br/>
                <ol>
					<li><h4>Register your instance to cloud</h4>
						Log into your inspextor instance using the default username and password.Then select Cloud Manager functionality under Support menu. Enter the InstanceID and InstancePassword mentioned above in the email and click Connect.Once connected the status indicator will display 'Connected' text. <br/>
					</li>
					<br/>
					<li><h4>Create your cloud account</h4>
						Open <a href='http://localhost/InxCloud/poe' target='_new'>Inspextor Cloud</a> on your browser and select the <b>Register</b> link at the bottom left of the login box. Enter the InstanceID,InstancePassword and the other required information to create your cloud account.<br/>
						<br/>
						NOTE: This step is required only if you havent registered before. If done already, just login to your cloud account and your registered instance will show up automatically.
					</li>
                </ol>
                <br/>
                <br/>
                Thanks<br/>
                MHT International<br/>
                ";
				
				
				$body = file_get_contents('tpl/emailTemplate.html');
				/*Replace Emaile Template file*/
				$body = str_replace("[[EMAIL_TITLE]]","",$body);
				$body = str_replace("[[EMAIL_CONTENT]]",$HTMLContent,$body);
				
				file_put_contents("emailContent.html",$body);
				
				$sendMail = $CMF->sendEmail($subject,$body,$tomail,CLOUD_MAIL_EMAIL,CLOUD_MAIL_SERVER,CLOUD_MAIL_PASSWORD,CLOUD_MAIL_PORT );
				
				if($sendMail){
					$result['status'] = '1';
					$result['message'] = "Cloud connection information has been emailed to client";
				}
				else{
					$result['status'] = '0';
					$result['message'] = "Mail Sending Failed";
				}
				die(json_encode($result));
			}
			
            //Add New Devices
            function saveDeviceDetails()
            {
                $db = new \DBC(DB_HOST,DB_USER,DB_PASSWD,DB_NAME_CLOUD);

                $loggedUserId   = $_SESSION['LOGGED_USER_ID'];

                $devicename = $_REQUEST['devicename'];
                $devID = $_REQUEST['devID'];
                $password = $_REQUEST['password'];
                $duration = $_REQUEST['duration'];
                $spaceLimt = $_REQUEST['spaceLimt'];
                $emailAddress = $_REQUEST['emailAddress'];
				$clientID = $_REQUEST['clientID'];
                $status =  false;

                $sql = "select id from pmi_inx_device where (device_name='$devicename' or inspextorID='$devID') and deleted=0";
                $existDuplicate = $db->get_single_result($sql);


                if($existDuplicate){
                    $result['status']       = '0';
                    $result['message']      = 'This instance already exists.';
                    die(json_encode($result));
                }
                else{
                    $dbrec = array();

                    $dbrec["device_name"] = $devicename;
                    $dbrec["inspextorID"] = $devID;
                    $dbrec["Password"]     = $password;
                    $dbrec["DataDuration"]     = $duration;
                    $dbrec["DataSpaceLimit"]   = $spaceLimt;
                    $dbrec["ReportMailID"] = $emailAddress;
					$dbrec["clientID"] = $clientID;
                    $dbrec["CreatedBy"] = $loggedUserId;
                    $dbrec["CreatedOn"] = date("Y-m-d");

                    $status = $db->insert_query($dbrec , 'pmi_inx_device');
                    if($status){
                        $insertID = $db->get_insert_id();
                        $dbrec = array();
                        $dbrec["DeviceID"] = $insertID;
                        $dbrec["DeviceDBName"] = INX_DB_PREFIX.$insertID;
                        $status = $db->insert_query($dbrec , 'pmi_inx_device_db');
                        if($status){
                            $result['status']       = '1';
                            $result['message']      = 'Successfully added instance.';
                            $result['duplicateEntry']        = $indexArr;
                            die(json_encode($result));
                        }
                    }

                }


            }

            //Fetch Device Details
            function fetchDeviceDetails()
            {
                $db =  new \DBC(DB_HOST,DB_USER,DB_PASSWD,DB_NAME_CLOUD);

                $deviceId = $_REQUEST['deviceid'];

                $sql = "select * from pmi_inx_device where id='$deviceId'";
                $deviceDetails = $db->get_result($sql);

				$loggedUser = $_SESSION['LOGGED_USER_ID'];
				$where = "deleted=0";
				if($loggedUser!=""){
					$where .= " and id!='$loggedUser' and added_by='$loggedUser'";
				}

				$sql = "select * from pmi_client where $where";
				$clientInfo = $db->get_result($sql);

                if($deviceDetails){
                    $result['status'] = '1';
                    $result['device'] = $deviceDetails;
					$result['clientInfo'] = $clientInfo;
                    die(json_encode($result));
                }
                else{
                    $result['status'] = '0';
                    die(json_encode($result));
                }
            }

            //Remove Device
            function removeDevice()
            {
                $db =new \DBC(DB_HOST,DB_USER,DB_PASSWD,DB_NAME_CLOUD);

                $deleteId       = $_REQUEST['deleteid'];
                $loggedUserId   = $_SESSION['LOGGED_USER_ID'];
                $activeDevice = 0;
                if($deleteId){
                    $dbrec = array();
                    $where = array();

                    $where["id"] = $deleteId;

                    $dbrec["deleted"]         = 1;
                    $dbrec["deleted_by"]      = $loggedUserId;

                    $removeDevice = $db->update_query($dbrec , 'pmi_inx_device',$where);
                }
                if($removeDevice){

                    //Deleted All Alerts of the removed device
                    $where1["device_id"] = $deleteId;

                    $dbrec1["deleted"]         = 1;
                    $dbrec1["deleted_by"]      = $loggedUserId;

                    //$removeAlerts = $db->update_query($dbrec1 , 'pmi_alert_definition',$where1);
                    //$updateLastLogIDs = $db->update_query(array("deleted"=>1) , 'pmi_heart_beat',array("deviceID"=>$deleteId));
                    //$deletedDevSumTbl = $db->query("DELETE FROM pmi_device_sum WHERE DeviceID = '$deleteId'");

                    //Clear session data
                    if($_SESSION["activeDeviceID"] == $deleteId){
                        unset($_SESSION["activeDeviceID"],
                        $_SESSION["activeDeviceName"],
                        $_SESSION["activeDeviceSerialNo"],
                        $_SESSION["activeDeviceIPAddress"],
                        $_SESSION["activeDeviceLocation"],
                        $_SESSION["activeDevicePingStatus"],
                        $_SESSION["activeDeviceModelNo"]);
                        $activeDevice = 1;
                    }

                    $result['status'] = '1';
                    $result['active'] = $activeDevice;
                    $result['message'] = 'Successfully removed device';
                    die(json_encode($result));
                }
                else{
                    $result['status'] = '0';
                    $result['message'] = 'Failed to remove device';
                    die(json_encode($result));
                }


            }

            //Update Device
            function updateDevice(){
                //print_r($_REQUEST);
                //die();
                $db = new \DBC(DB_HOST,DB_USER,DB_PASSWD,DB_NAME_CLOUD);

                $loggedUserId   = $_SESSION['LOGGED_USER_ID'];


                $updateId   = $_REQUEST['updateId'];
                $devicename = $_REQUEST['devicename'];
                $devID = $_REQUEST['devID'];
                $password = $_REQUEST['password'];
                $duration = $_REQUEST['duration'];
                $spaceLimt = $_REQUEST['space'];
                $emailAddress = $_REQUEST['email'];
				$clientID = $_REQUEST['clientId'];

                if($updateId){
                    try {
                        
                            $sql1 = "select id from pmi_inx_device where device_name = '$devicename' and deleted=0 and id != $updateId";
                            $existSerialno = $db->get_single_result($sql1);
                            if($existSerialno){
                                $result['status'] = '0';
                                $result['message'] = 'Failed to update. instance ID already exists';
                                die(json_encode($result));
                            }

                            $dbrec = array();
                            $where = array();

                            $where["id"] = $updateId;

                            $dbrec["device_name"] = $devicename;
                            $dbrec["inspextorID"] = $devID;
                            $dbrec["Password"]     = $password;
                            $dbrec["DataDuration"]     = $duration;
                            $dbrec["DataSpaceLimit"]   = $spaceLimt;
                            $dbrec["ReportMailID"] = $emailAddress;
							$dbrec["ClientID"] = $clientID;

                            $updateDevice = $db->update_query($dbrec , 'pmi_inx_device',$where);
                            if($updateDevice){
                                $result['status']   = '1';
                                $result['message']  = 'Successfully updated instance';

                                /*if($_SESSION["activeDeviceID"] == $updateId){

                                    $_SESSION["activeDeviceName"] =  $device;
                                    $_SESSION["activeDeviceSerialNo"] =  $serialNo;
                                    $_SESSION["activeDeviceIPAddress"] =  $ipAddress;
                                    $_SESSION["activeDeviceLocation"] =  $location;
                                }*/

                                die(json_encode($result));
                            }
                            else{
                                $result['status'] = '0';
                                $result['message'] = 'Failed to update instance';
                                die(json_encode($result));
                            }
                        //}
                        /*else {
                            $result['status'] = '0';
                            $result['message'] = 'Failed to update device';
                            die(json_encode($result));
                        }*/
                    } catch (Exception $ex) {
                        die($ex->getMessage());
                    }
                }

            }

            //Search Device
            function searchDevice()
            {
                $db = new DBC();

                $where      = '';
                $device     = $_REQUEST['devicename'];
                $serialNo   = $_REQUEST['serialNo'];
                $ipAddress  = $_REQUEST['ipaddress'];
                $location   = $_REQUEST['location'];

                if($device!=""){
                    $where =  "device_name LIKE '%".$device."%'";
                }
                if($serialNo!=""){
                    if($where!=''){
                        $where .=  " and serial_no='".$serialNo."'";
                    }
                    else{
                        $where =  "serial_no='".$serialNo."'";
                    }
                }
                if($ipAddress!=""){
                    if($where!=''){
                        $where .=  " and ip_address='".$ipAddress."'";
                    }
                    else{
                        $where =  "ip_address='".$ipAddress."'";
                    }
                }
                if($location!=""){
                    if($where!=''){
                        $where .=  " and location LIKE '%".$location."%'";
                    }
                    else{
                        $where =  "location LIKE '%".$location."%'";
                    }
                }

                $sql = "select id from pmi_inx_device where ".$where;
                $searchResult = $db->get_result($sql);

                if($searchResult){
                    $result['status'] = '1';
                    $result['searchResult'] = $searchResult;
                    die(json_encode($result));
                }
                else{
                    $result['status'] = '0';
                    die(json_encode($result));
                }
            }

            //Pinging Device
            function pingDevice()
            {
                $ipArr = array("192.168.2.26", "192.168.2.23", "192.168.2.32", "192.168.2.325", "192.168.2.123", "192.168.1.64", "192.168.2.326");
                $errCodeArr = array(
                    "192.168.2.26" => 0,
                    "192.168.2.30" => 1,
                    "192.168.2.23" => 0,
                    "192.168.2.22" => 0,
                    "192.168.2.27" => 0,
                    "192.168.2.32" => 0,
                    "192.168.2.325" => 0,
                    "192.168.2.123" => 1,
                    "192.168.3.45" => 0,
                    "192.168.1.64" => 0,
                    "192.168.2.33" => 1,
                    "192.168.2.326" => 0
                );

                $deviceIP = $_REQUEST['ipaddress'];
                $deviceId = $_REQUEST['deviceid'];

                $errorCode = $errCodeArr[$deviceIP];

                if($errorCode == 0){
                    if(in_array($deviceIP, $ipArr))
                    {
                        //$key = array_search($deviceIP, $ipArr);   //get index of array
                        $pingStatus = 1; //Ping Success
                    }
                    else{
                        $pingStatus = 0; //Ping Fail
                    }
                }
                else if($errorCode == 1){
                    $pingStatus = 0;
                    $errorMsg = "Failed to Ping. Device not available.";
                }

                $result['errorCode'] = $errorCode;
                $result['IP'] = $deviceIP;
                $result['id'] = $deviceId;
                $result['pingStatus'] = $pingStatus;
                $result['errorMsg'] = $errorMsg;

                die(json_encode($result));
            }

            //Fetch Panel Parameter Details
            function getPanelParameters(){
                $db = new DBC();

                $activeDeviceId = $_REQUEST['deviceID'];

                $sql = "select * from pmi_parameters where device_id='$activeDeviceId'";
                $parameterRec = $db->get_result($sql);

                if(sizeof($parameterRec)>0){
                    $result['status'] = '1';
                    $result['parameters'] = $parameterRec;
                    die(json_encode($result));
                }
                else{
                    $result['status'] = '0';
                    die(json_encode($result));
                }

            }

            //Save Parameter Values
            function saveParameters(){

                $db = new DBC();

                $loggedUserId = $_SESSION['LOGGED_USER_ID'];

                $model              =   $_REQUEST['model'];
                $deviceId           =   $_REQUEST['deviceID'];
                $installation       =   $_REQUEST['installation'];
                $cableSize          =   $_REQUEST['cableSize'];
                $cableMaterial      =   $_REQUEST['cableMaterial'];
                $startUp            =   $_REQUEST['startUp'];
                $calibration        =   $_REQUEST['calibration'];
                $responsePeriod     =   $_REQUEST['responsePeriod'];
                $parallelSys        =   $_REQUEST['parallelSys'];
                $operation          =   $_REQUEST['operation'];

                //Check whether device parameters exists
                $sql = "select id from pmi_parameters where device_id='$deviceId'";
                $existsParameters = $db->get_single_result($sql);

                if($existsParameters[0]!="")
                {
                    $dbrec = array();
                    $where = array();

                    $where["id"] = $existsParameters[0];

                    $dbrec["model_number"]          = $model;
                    $dbrec["device_id"]             = $deviceId;
                    $dbrec["installation_format"]   = $installation;
                    $dbrec["cable_type"]            = $cableSize;
                    $dbrec["cable_material"]        = $cableMaterial;
                    $dbrec["default_startup"]       = $startUp;
                    $dbrec["calibration"]           = $calibration;
                    $dbrec["response_period"]       = $responsePeriod;
                    $dbrec["parallel_system"]       = $parallelSys;
                    $dbrec["daily_operation"]       = $operation;

                    $updateParameter = $db->update_query($dbrec , 'pmi_parameters',$where);
                }
                else
                {
                    $dbrec = array();
                    $dbrec["model_number"]          = $model;
                    $dbrec["device_id"]             = $deviceId;
                    $dbrec["installation_format"]   = $installation;
                    $dbrec["cable_type"]            = $cableSize;
                    $dbrec["cable_material"]        = $cableMaterial;
                    $dbrec["default_startup"]       = $startUp;
                    $dbrec["calibration"]           = $calibration;
                    $dbrec["response_period"]       = $responsePeriod;
                    $dbrec["parallel_system"]       = $parallelSys;
                    $dbrec["daily_operation"]       = $operation;
                    $dbrec["created_on"]            = date("Y-m-d");;
                    $dbrec["craeted_by"]            = $loggedUserId;

                    $savePanelParameter = $db->insert_query($dbrec , 'pmi_parameters');
                }

                if($savePanelParameter){
                    $result['status'] = '1';
                    $result['message'] = "Successfully added Panel Parameters";
                    die(json_encode($result));
                }
                else if($updateParameter) {
                    $result['status'] = '1';
                    $result['message'] = "Successfully modified Panel Parameters";
                    die(json_encode($result));
                }
                else{
                    $result['status'] = '0';
                    $result['message'] = "Failed to add Panel Parameters";
                    die(json_encode($result));
                }

            }

            //Device Bank Switch
            function deviceBankSwicth()
            {
                $type = $_REQUEST['type'];
                $deviceIP = $_SESSION["activeDeviceIPAddress"];

                $bankSwitchStatus = array(
                    "192.168.2.26" => true,
                    "192.168.2.30" => false,
                    "192.168.2.23" => false,
                    "192.168.2.22" => true,
                    "192.168.2.27" => true,
                    "192.168.1.64" => false,
                    "192.168.2.325" => true,
                    "192.168.2.326" => false
                );

                $success = $bankSwitchStatus[$deviceIP];

                $result['success'] = $success;
                $result['type'] = $type;
                die(json_encode($result));
            }



            //Get Device Model Number
            /*function getDeviceModel(){
                $db = new DBC();

                $deviceId   = $_SESSION["activeDeviceID"];
                $deviceName =   $_SESSION["activeDeviceName"];
                $deviceIP   =   $_SESSION["activeDeviceIPAddress"];

                $sql = "select model_number from pmi_parameters where device_id='$deviceId'";
                $deviceModel = $db->get_single_result($sql);

                $result['status'] = 1;
                $result['message'] = "Successfully reset device memory";
                die(json_encode($result));
            } */

            /*Get Active Device*/
            function getSelectedDevice(){
               $result['deviceID'] = $_SESSION["activeDeviceID"];
               $result['deviceName'] =  $_SESSION["activeDeviceName"];
               $result['serialNo'] =  $_SESSION["activeDeviceSerialNo"];
               $result['IDAddress'] =  $_SESSION["activeDeviceIPAddress"];
               $result['location'] =  $_SESSION["activeDeviceLocation"];
               die(json_encode($result));
            }


            function GetAlerts(){
                $db = new DBC();
                $ID = $_REQUEST['ID'];
                $limit = $_REQUEST['showCount'];
                $condition = "";

                if($ID != 0){
                    $condition = "AND A.ID < $ID ";
                }

                $sql =<<<EOF
                    SELECT A.ID,
                        A.CratedOn,
                        A.Title,
                        A.Severity,
                        A.Description,
                        T.Name AS device_name,
                        A.DeviceID,
                        ( SELECT COUNT(ID) FROM pmi_device_alerts) AS TCount,
                        A.Status,
                        A.AlertType,
                        A.TypeAction,
                        A.TypeMasterID,
                        A.TypeDBID

                    FROM
                        pmi_device_alerts A
                    LEFT JOIN pmi_alert_definition D ON(D.id = A.AlertDefID)
                    LEFT JOIN pmi_tag T ON(T.ID = A.DeviceID AND T.deleted = 0 AND T.deleted = 0)
                    WHERE
                        A.Resolved=0
                        $condition
                    ORDER BY A.ID DESC LIMIT $limit
EOF;
                //die($sql);
                //$sql = "SELECT A.ID,A.CratedOn,D.parameter,D.severity,D.description,P.device_name,(SELECT COUNT(ID) FROM pmi_device_alerts) AS TCount "
                //        . "FROM pmi_device_alerts A LEFT JOIN pmi_alert_definition D ON(D.id = A.AlertDefID) LEFT JOIN pmi_device P ON(P.id = A.DeviceID) "
                //        . "WHERE A.ID BETWEEN '".$startID ."' AND '".$endID."' ORDER BY A.ID DESC";
                //die($sql);
                $alertData = $db->get_result($sql);

                $result['results'] = $alertData;
                die(json_encode($result));
            }

            function exportData(){
                $db = new DBC();
                $startDate  = date( 'Y-m-d 00:00:00', strtotime( $_REQUEST['StartDate'] ) );
                $endDate    = date( 'Y-m-d 23:59:59', strtotime( $_REQUEST['EndDate'] ) );
                $activeDevice = $_SESSION["activeDeviceID"];
                $activeDevName = $_SESSION["activeDeviceName"];
                $activeDevSerialNo = $_SESSION["activeDeviceSerialNo"];

                $qry = "SELECT * FROM pmi_log WHERE deviceID = '$activeDevice' AND logdate BETWEEN '$startDate' AND '$endDate' ORDER BY logdate ASC";

                $deviceLogData = $db->get_result($qry);

                if(!empty($deviceLogData))
                {
                    ini_set('memory_limit', '2048M');

                    $objPHPExcel = new PHPExcel();
                    $objPHPExcel->getActiveSheet()->setTitle('Device Log Records');
                    $rowIndex=1;
                    $objPHPExcel->setActiveSheetIndex(0);
                    $objPHPExcel->getActiveSheet()->mergeCells('A1:J1');//Heading
                    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(18);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(18);

                    $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(18);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(25);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(25);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(15);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(15);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(18);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(18);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(18);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(20);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(20);

                    $objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(18);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(18);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(18);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(15);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setWidth(15);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setWidth(15);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setWidth(15);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('AB')->setWidth(15);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('AC')->setWidth(15);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('AD')->setWidth(15);

                    $objPHPExcel->getActiveSheet()->getStyle('A1:J1')->getFill()->getStartColor()->setARGB('F2B843');
                    $objPHPExcel->getActiveSheet()->setCellValue('A'.$rowIndex, $activeDevName.'  :    Log Data  ('.date('Y-m-d', strtotime($_REQUEST['StartDate'])).'  -  '.date('Y-m-d', strtotime($_REQUEST['EndDate'])).')');
                    $objPHPExcel->getActiveSheet()->getStyle('A'.$rowIndex)->getFont()->getColor()->setARGB('FFCD5C5C');
                    $objPHPExcel->getActiveSheet()->getStyle('A'.$rowIndex)->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle('A'.$rowIndex)->getFont()->setUnderline(true);
                    $objPHPExcel->getActiveSheet()->getStyle('A'.$rowIndex)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    $rowIndex++;
                    $rowIndex++;
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$rowIndex,"Serial # \r\n");
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$rowIndex,"Log Date \r\n");
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$rowIndex,"Time Zone \r\n");
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$rowIndex,"Voltage Ph 1 \r\n");
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$rowIndex,"Voltage Ph 2 \r\n");
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$rowIndex,"Voltage Ph 3 \r\n");
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$rowIndex,"Current Ph 1 \r\n");
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$rowIndex,"Current Ph 2 \r\n");
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.$rowIndex,"Current Ph 3 \r\n");
                  //  $objPHPExcel->getActiveSheet()->getStyle('H'.$rowIndex)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.$rowIndex,"Power Factor Ph 1 \r\n");
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('K'.$rowIndex,"Power Factor Ph 2 \r\n");

                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('L'.$rowIndex,"Power Factor Ph 3 \r\n");
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('M'.$rowIndex,"Ambient Temperature \r\n");
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('N'.$rowIndex,"Internal Temperature \r\n");
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('O'.$rowIndex,"Voltage THD \r\n");
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('P'.$rowIndex,"Current THD \r\n");
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('Q'.$rowIndex,"Capacitance Ph 1 \r\n");
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('R'.$rowIndex,"Capacitance Ph 2 \r\n");
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('S'.$rowIndex,"Capacitance Ph 3 \r\n");
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('T'.$rowIndex,"Voltage Imbalance \r\n");
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('U'.$rowIndex,"Current Imbalance \r\n");

                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('V'.$rowIndex,"Apparent Power \r\n");
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('W'.$rowIndex,"Real Power \r\n");
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('X'.$rowIndex,"Reactive Power \r\n");
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('Y'.$rowIndex,"Neutral Amps \r\n");
                    /*$objPHPExcel->setActiveSheetIndex(0)->setCellValue('Z'.$rowIndex,"Reserved 1 \r\n");
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AA'.$rowIndex,"Reserved 2 \r\n");
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AB'.$rowIndex,"Reserved 3 \r\n");
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AC'.$rowIndex,"Reserved 4 \r\n");
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AD'.$rowIndex,"Check Sum 1 \r\n");
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AE'.$rowIndex,"Check Sum 2 \r\n");*/

                    $objPHPExcel->getActiveSheet()->getStyle('A3:AE3')->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle('A3:AE3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                    $rowIndex++;
                    foreach ($deviceLogData as $data)
                    {
                        $dbDate = new DateTime($data['logdate']);
                        //$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$rowIndex,$data['status']);
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$rowIndex,$activeDevSerialNo);
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$rowIndex,$dbDate->format('m/d/Y H:i:s'));
                        $objPHPExcel->getActiveSheet()->getStyle('B'.$rowIndex)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

                        $tz = 0;

                        if($data['timez'] == null || $data['timez'] == '' || $data['timez'] < 0) $tz = 0;
                        else if($data['timez'] >24) $tz = 0;
                        else {
                            $tz = (int)$data['timez'];
                            if($tz > 12) $tz = $tz - 12;
                            else if($tz <= 11) $tz = $tz + 12;
                            else if($tz == 12) $tz = 0;
                        }

                        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$rowIndex,$GLOBALS["TIME_ZONES"][$tz]);
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$rowIndex,$data['volts_ph1']);
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$rowIndex,$data['volts_ph2']);
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$rowIndex,$data['volts_ph3']);
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$rowIndex,$data['amps1']);
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$rowIndex,$data['amps2']);
                       // $objPHPExcel->getActiveSheet()->getStyle('G'.$rowIndex)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.$rowIndex,$data['amps3']);
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.$rowIndex,$data['pf1']);
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('K'.$rowIndex,$data['pf2']);

                        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('L'.$rowIndex,$data['pf3']);
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('M'.$rowIndex,$data['tamb']);
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('N'.$rowIndex,$data['tint']);

                        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('O'.$rowIndex,$this->sigdig($data['vthd'],'.',2,TRUE));
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('P'.$rowIndex,$this->sigdig($data['athd'],'.',2,TRUE));
                        $objPHPExcel->setActiveSheetIndex(0)->getStyle('O'.$rowIndex)->getNumberFormat()->setFormatCode('0.00');
                        $objPHPExcel->setActiveSheetIndex(0)->getStyle('P'.$rowIndex)->getNumberFormat()->setFormatCode('0.00');
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('Q'.$rowIndex,$data['caps1']);
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('R'.$rowIndex,$data['caps2']);
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('S'.$rowIndex,$data['caps3']);
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('T'.$rowIndex,$data['volts_imb']);
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('U'.$rowIndex,$data['amps_imb']);

                        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('V'.$rowIndex,$data['apar_pwr']);
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('W'.$rowIndex,$data['real_pwr']);
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('X'.$rowIndex,$data['react_pwr']);
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('Y'.$rowIndex,$data['neutral_amps']);
                        /*$objPHPExcel->setActiveSheetIndex(0)->setCellValue('Z'.$rowIndex,$data['res1']);
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AA'.$rowIndex,$data['res2']);
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AB'.$rowIndex,$data['res3']);
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AC'.$rowIndex,$data['res4']);
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AD'.$rowIndex,$data['cs1']);
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AE'.$rowIndex,$data['cs2']); */

                        $rowIndex++;
                    }

                    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
                    ob_end_clean();
                    // Redirect output to a client�s web browser (Excel5)
                    header('Content-Type: application/vnd.ms-excel');
                    header('Content-Disposition: attachment;filename="device-log.xls"');
                    $objWriter->save('php://output');

                }
                else{
                    echo "No Log Records Found for '".$activeDevName."' between ".date('Y-m-d', strtotime($_REQUEST['StartDate']))." and ".date('Y-m-d', strtotime($_REQUEST['EndDate']));

                }
            }

            function setDateTime(){
                try {
                    if(!isset($_SESSION["activeDeviceID"]) || $_SESSION["activeDeviceID"] == "" || $_SESSION["activeDeviceID"] == "0"){
                    $result['status'] = 2; //no device
                    die(json_encode($result));
                    }
                    //print_r($_REQUEST);
                    //die(1212);
                    $clientDate = new DateTime( urldecode($_REQUEST["dateTime"]));
                    //die($clientDate->format("Y/m/d H:i:s"));
                    $data = array(
                        "action" => "SetDateTime",
                        "date" => $clientDate->format("Y/m/d"),
                        "time" => $clientDate->format("H:i:s"),
                        "source" => 3
                    );

                    $result = invokeWebMethod(getWebURL($_SESSION["activeDeviceIPAddress"], $data));

                    die($result);
                } catch (Exception $ex) {
                    die($ex->getMessage());
                }

            }

            function ResetDeviceMemory(){
                $ipAddress = $_SESSION["activeDeviceIPAddress"];
                $_REQUEST["serialNo"] = $_SESSION["activeDeviceSerialNo"];

                $data = array(
                    "action" => "ResetDeviceMemory",
                    "source" => 3
                );

                $result = array();
                $result["timeStart"] = date('Y-m-d H:i:s');

                $db = new \DBC();
                $db->autocommit(false);

                try{
                    $json = invokeWebMethod(getWebURL($_SESSION["activeDeviceIPAddress"], $data));
                    $result["gReturnAt"] = date('Y-m-d H:i:s');
                    //if()
                    $reader = json_decode($json, true);
                    //print_r($reader);
                    if( $reader["status"] == 1){
                        $result["gStatus"] = 1;
                        if( $reader["responseData"]["ResetStatus"] ){
                            $result["gResetStatus"] = 1;
                            $logDlelete = "DELETE FROM pmi_log WHERE deviceID IN (SELECT ID FROM pmi_device WHERE ip_address = '$ipAddress')";
                            $db->query($logDlelete);
                            $result["sLogDelete"] = 1;
                            //$result["query"] = $logDlelete;

                            $hourDelete = "DELETE FROM pmi_hourly_log WHERE deviceID IN (SELECT ID FROM pmi_device WHERE ip_address = '$ipAddress')";
                            $db->query($hourDelete);
                            $result["sHLogDelete"] = 1;
                            $dailyDelete = "DELETE FROM pmi_daily_log WHERE deviceID IN (SELECT ID FROM pmi_device WHERE ip_address = '$ipAddress')";
                            $db->query($dailyDelete);
                            $result["sDLogDelete"] = 1;
                            $alertsDelete = "DELETE FROM pmi_device_alerts WHERE deviceID IN (SELECT ID FROM pmi_device WHERE ip_address = '$ipAddress')";
                            $db->query($alertsDelete);
                            $result["sAlertDelete"] = 1;
                            $notifDelete = "DELETE FROM pmi_alert_lookup WHERE deviceID IN (SELECT ID FROM pmi_device WHERE ip_address = '$ipAddress')";
                            $db->query($notifDelete);
                            $result["sAlertLookUpDelete"] = 1;
                            $udateDevice = "UPDATE pmi_device SET galileo_id = 0, hourly_log_id = 0, daily_log_id = 0  WHERE ip_address = '$ipAddress'";
                            $db->query($udateDevice);
                            $result["sUpdateDeviceIDs"] = 1;
                            $udateDeviceMinMax = "UPDATE pmi_device_sum set KWH=0,KVA=0,KVAR=0 WHERE DeviceID IN (SELECT ID FROM pmi_device WHERE ip_address = '$ipAddress')";
                            $db->query($udateDeviceMinMax);
                            $result["sUpdateDeviceMinMax"] = 1;
                            $db->commit();
                            $result["sCommitComplete"] = 1;
                            $result["status"] = 1;
                        }
                        else{
                            $result["gResetStatus"] = 0;
                            $result["status"] = 0;
                        }
                    }
                    else{
                        $result["gStatus"] = 0;
                        $result["status"] = 0;
                    }
                }
                catch(Exception $ex)
                {
                    $db->rollback();
                    $result["status"] = 0;
                    $result["error"] = $ex;
                }

                $result["timeEnd"] = date('Y-m-d H:i:s');
                $db->close();
                die(json_encode($result));
            }

            function sigdig($num, $sep='.', $sig=2, $ret=FALSE)
            {
                //return $num;
                $numSplit = explode(".", $num);
                if(count($numSplit) > 1) $cut = substr($num, 0, ( (strpos($num, $sep)+1)+$sig ));
                else $cut = $num;

                if($ret){ return $cut; }
                else{ echo $cut; }
            }


            function getDevParam(){
                $type = $_REQUEST["type"];
                $devID = $_REQUEST["deviceID"];

                $db = new \DBC();
                $joinStr = "";
                $where = "";
                $reader = array();
                $result["status"] = 0;
                if($type == 1){
                    $where = "AND T.NodeID=$devID";
    }
                else if($type == 2){
                    $joinStr =" LEFT JOIN pmi_cluster_mapping CM ON (CM.NodeID = T.NodeID) ";
                    $where = "AND CM.Deleted=0 AND CM.ClusterID=$devID";
                }

                try{
                   $sql =<<<EOF
                           SELECT GROUP_CONCAT( DISTINCT T.TagTypeID,"") AS TypeID FROM pmi_tag T
                           $joinStr
                           WHERE
                                T.Deleted=0 AND
                                T.`Status`=1
                                $where
EOF;
                   //echo $sql;
                   $paramResult = $db->get_single_result($sql);

                   if(count($paramResult) > 0){
                       $result["status"] = 1;
                       $result["result"] = $paramResult;
                   }
                }
                catch(Exception $ex)
                {
                    $result["error"] = $ex->getMessage();
                }
                die(json_encode($result));
            }

            function getAllinstance(){
                $result["status"] = 0;
                $db = new \DBC(DB_HOST,DB_USER,DB_PASSWD,DB_NAME_CLOUD);
                try{

					$loggedUserRoles = $_SESSION['LOGGED_USER_ROLEIDS'];
					$loggedUserID = $_SESSION['LOGGED_USER_ID'];
					//print_r($_SESSION);die;
					if(!in_array(SUPER_ADMIN_ROLE,$loggedUserRoles))
					{
						$sql = 'SELECT  D.id,D.device_name,D.`Status` FROM pmi_inx_device D left join pmi_user U on (D.ClientID = U.clientId) WHERE D.deleted=0 AND D.Status=1 AND U.ID = '.$loggedUserID;
						//print_r($_SESSION);die;
					}
					else {
						$sql = 'SELECT  id,device_name,`Status` FROM pmi_inx_device WHERE deleted=0 AND Status=1';
					}
					//$sql = 'SELECT  id,device_name,`Status` FROM pmi_inx_device WHERE deleted=0 AND Status=1';
                   //echo $sql;die;
                   $resData = $db->get_result($sql);

                   if(count($resData) > 0){
                       $result["status"] = 1;
                       $result["result"] = $resData;
                       if(isset($_SESSION["INX_CLUD_ID"])){
                           $result["activeDevID"] =$_SESSION["INX_CLUD_ID"];
                       }
                       else{$result["activeDevID"] = 0;}
                   }
                }
                catch(Exception $ex)
                {
                    $result["error"] = $ex->getMessage();
                }
                die(json_encode($result));
            }
            function selInstance(){
                $result["status"] = 0;
                $db = new \DBC(DB_HOST,DB_USER,DB_PASSWD,DB_NAME_CLOUD);
                $instanceID = $_REQUEST["selIns"];
                $currentPage = $_REQUEST["currentPage"];
                try{
                   $sql =<<<EOF
                        SELECT
                            INX_D.id,INX_D.device_name,INX_DB.DeviceDBName, INX_D.inspextorID
                        FROM
                            pmi_inx_device INX_D
                        LEFT
                            JOIN pmi_inx_device_db INX_DB ON (INX_DB.DeviceID = INX_D.id)
                        WHERE INX_D.deleted=0 AND INX_D.id=$instanceID
EOF;
                  //echo $sql;
                   $resData = $db->get_result($sql);
                   //print_r($resData);
                   if(sizeof($resData) > 0){
                       IF(!isset($_SESSION["INX_CLUD_ID"]) || $_SESSION["INX_CLUD_ID"] != $instanceID){
                            $_SESSION["INX_CLUD_ID"] = $resData[0]["id"];
                            $_SESSION["INX_CLUD_DEV_NAME"] = $resData[0]["device_name"];
                            $_SESSION["INX_CLUD_DB_NAME"] = $resData[0]["DeviceDBName"];
                            $_SESSION["INX_CLUD_DB_UNQID"] = $resData[0]["inspextorID"];
                            $result["status"] = 1;
                       }
                       else $result["status"] = 2;


                   }
                }
                catch(Exception $ex)
                {
                    $result["error"] = $ex->getMessage();
                }
                //print_r($result);
                die(json_encode($result));
            }
    }
?>
