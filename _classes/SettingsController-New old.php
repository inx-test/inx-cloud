<?php

include_once("config.php");

class SettingsController extends ControllerBase {
    # Constructor Method 

    function __constructor() {
        
    }

    /**************************************
     * GET SWITCH GRAPH DETAILS
     *************************************/
    /*Public function getSwitchGraph() {
        try {
            $db = new DBC();
            $filter = $_REQUEST['filter'];
            
            $qry = <<<EOF
                SELECT D.id AS DevID,D.device_name,D.oper_power_total,SS.status AS PS_Status,SS.*,L.* 
                FROM 
                    pmi_device D 
                    LEFT JOIN pmi_device_log L ON (D.id = L.DeviceID AND L.`Status`=1)
                    LEFT JOIN pmi_switch_schedule SS ON (D.id = SS.DeviceID AND L.Port = SS.Port)
                WHERE 
                        D.id = $filter AND D.type = 1 ORDER BY D.id,L.Port ASC
EOF;
           // echo $qry;die;
            $result = $db->get_result($qry);

            if (sizeof($result) > 0) {

                $rtnResult["status"] = 1;
                $rtnResult["result"] = $result;
                
            } else
                $rtnResult["Status"] = 0;

            die(json_encode($rtnResult, TRUE));
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }*/
	Public function getSwitchGraph() {
        try {
            $db = new DBC();
            $filter = $_REQUEST['filter'];
            
            $qry = <<<EOF
                SELECT D.id AS DevID,D.device_name,D.oper_power_total FROM pmi_device D
                WHERE  D.id = $filter AND D.type = 1
EOF;
            //echo $qry;die;
            $devResult = $db->get_result($qry);
            
            //print_r($result);
            if (sizeof($devResult) > 0) {
                
                /*$sql = <<<EOF
                    SELECT L.* 
                        FROM  pmi_device_log L 
                        WHERE L.DeviceID = $filter AND L.`Status`=1 AND L.LogDate BETWEEN DATE_SUB(NOW(),INTERVAL 1 MINUTE) AND NOW()
                        ORDER BY L.Port ASC
EOF;*/

$sql = <<<EOF
                    SELECT L.* 
                        FROM  pmi_device_log L 
                        WHERE L.DeviceID = $filter AND L.`Status`=1 AND L.LogDate  >= NOW() - INTERVAL 1  MINUTE
                        ORDER BY L.Port ASC
EOF;
                $logResult = $db->get_result($sql);
                
               // echo $sql; 
                $sql = <<<EOF
                    SELECT SS.* 
                        FROM pmi_switch_schedule SS 
                        WHERE SS.DeviceID = $filter 
                    ORDER BY SS.Port ASC
EOF;
                $scheduleResult = $db->get_result($sql);
                
                $rtnResult["status"] = 1;
                $rtnResult["result"] = $devResult;
                $rtnResult["logResult"] = $logResult;
                $rtnResult["scheduleResult"] = $scheduleResult;
            } else
                $rtnResult["Status"] = 0;

            die(json_encode($rtnResult, TRUE));
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }
    
    /***************************************************************
     * Read updated schedule excel and update currectsopnding switch
     ***************************************************************/
    Public function updateSwitchSchedule() {
        $db = new DBC();
        $CMF = new CommonFunction();
        $db->autocommit(false);
        $fileName = FILE_PATH. (urldecode($_REQUEST['fileName']));
        $status = 0;
        
        try {

            if (file_exists($fileName)) {
               
                $excelObj = $CMF->readExcelSheet($fileName);
                $worksheet = $excelObj->getSheet(0);
                $lastRow = $worksheet->getHighestRow();
                $colmnCount = $worksheet->getHighestColumn();
                $startCount=3;
                $deletedIDs = array();
                $insertQuery = "INSERT INTO pmi_switch_schedule (DeviceID,Item,Name,`Floor`,Room,Rack,PatchPanel,`Power`,EnginePort,Port,CableID,CableType,Color,NodeSerialNum,Vendor,Model,`Type`,HWVer,SWVer,`Status`,TalkerPort,NonTalkerPort,LocationID,LocationName,LocationPath,DiscoveryCount,TooManyDiscoveries,PodID,PodStatus,CreatedBy,CreatedOn) VALUES ";
                
                if($lastRow > 1){
                    $ColumA1 = $worksheet->getCell('A1')->getFormattedValue();
                    $ColumB1 = $worksheet->getCell('B1')->getFormattedValue();
                    $createdOn = gmdate('Y-m-d H:i:s');
                    
                    //find startup row
                    if(($ColumA1 == "" && $ColumA1 ==  NULL) && ($ColumB1 == "" && $ColumB1 ==  NULL)) $startCount = 3;
                    elseif($ColumA1 == "item" && $ColumB1 == "Floor") $startCount = 2;
                    
                    for($i = $startCount; $i <= $lastRow; $i++){
                        
                        $item = $worksheet->getCell('A'.$i)->getFormattedValue();
                        $enginName = $worksheet->getCell('G'.$i)->getFormattedValue();
                        $enginePort = $worksheet->getCell('H'.$i)->getFormattedValue();
                        
                        //if row not empty
                        if($item != "" && $enginName != "" && $enginePort != ""){

                            $devDetails = $db->get_result("SELECT id,device_name FROM pmi_device WHERE `type`=1 AND device_name='". urlencode($enginName) ."'");
                            
                            //if device exists
                            if(sizeof($devDetails) > 0){
                                if(sizeof($deletedIDs) > 0) $insertQuery .= ",";
                                //Device ID
                                $insertQuery .= "(".$devDetails[0]["id"].",";
                                
                                //Item
                                $insertQuery .= ($item != "" ? $item : 0).",";
                                
                                //Name
                                $name = $worksheet->getCell('L'.$i)->getFormattedValue();
                                $insertQuery .= ($name != "" ? ("'".urlencode($name)."'") : "NULL").",";
                                
                                //Floor
                                $floor = $worksheet->getCell('B'.$i)->getFormattedValue();
                                $insertQuery .= ($floor != "" ? $floor : 0).",";
                                
                                //Room
                                $room = $worksheet->getCell('C'.$i)->getFormattedValue();
                                $insertQuery .= ($room != "" ? $room : 0).",";
                                
                                //Rack
                                $rack = $worksheet->getCell('D'.$i)->getFormattedValue();
                                $insertQuery .= ($rack != "" ? $rack : 0).",";
                                
                                //PatchPanel
                                $patchPanel = $worksheet->getCell('E'.$i)->getFormattedValue();
                                $insertQuery .= ($patchPanel != "" ? ("'".urlencode($patchPanel)."'") : "NULL").",";
                                
                                //Power
                                $power = $worksheet->getCell('F'.$i)->getFormattedValue();
                                $insertQuery .= ($power != "" ? ("'".urlencode($power)."'") : "NULL").",";
                                
                                //EnginePort && //Port
                                if($enginePort != ""){
                                    $insertQuery .= "'". urlencode($enginePort) ."',";
                                    
                                    $prot = explode("-", $enginePort);
                                    if(sizeof($prot) > 1) $insertQuery .= $prot[1].",";
                                    else $insertQuery .= "0,";
                                }
                                else {
                                    $insertQuery .= "NULL,";
                                    $insertQuery .= "0,";
                                }
                                
                                //CableID
                                $cableID = $worksheet->getCell('I'.$i)->getFormattedValue();
                                $insertQuery .= ($cableID != "" ? ("'".urlencode($cableID)."'") : "NULL").",";
                                
                                //CableType && //Color
                                $cabletype = $worksheet->getCell('J'.$i)->getFormattedValue();
                                if($cabletype != ""){
                                    $cabletypeSplit = explode("/", $cabletype);
                                    if(sizeof($cabletypeSplit) > 0) $insertQuery .= "'". urlencode($cabletypeSplit[0]) ."',";
                                    else $insertQuery .= "NULL,";
                                    if(sizeof($cabletypeSplit) > 1) $insertQuery .= "'". urlencode($cabletypeSplit[1]) ."',";
                                    else $insertQuery .= "NULL,";
                                }
                                else {
                                    $insertQuery .= "NULL,";
                                    $insertQuery .= "0,";
                                }
                                //NodeSerialNum
                                $nodeSerialNum = $worksheet->getCell('K'.$i)->getFormattedValue();
                                $insertQuery .= ($nodeSerialNum != "" ? ("'".urlencode($nodeSerialNum)."'") : "NULL").",";
                                
                                //Vendor
                                $vendor = $worksheet->getCell('M'.$i)->getFormattedValue();
                                $insertQuery .= ($vendor != "" ? ("'".urlencode($vendor)."'") : "NULL").",";
                                
                                //Model
                                $model = $worksheet->getCell('N'.$i)->getFormattedValue();
                                $insertQuery .= ($model != "" ? ("'".urlencode($model)."'") : "NULL").",";
                                
                                //Type
                                $type = $worksheet->getCell('O'.$i)->getFormattedValue();
                                $insertQuery .= ($type != "" ? ("'".urlencode($type)."'") : "NULL").",";
                                
                                //HWVer
                                $hwVer = $worksheet->getCell('P'.$i)->getFormattedValue();
                                $insertQuery .= ($hwVer != "" ? ("'".urlencode($hwVer)."'") : "NULL").",";
                                
                                //SWVer
                                $swVer = $worksheet->getCell('Q'.$i)->getFormattedValue();
                                $insertQuery .= ($swVer != "" ? ("'".urlencode($swVer)."'") : "NULL").",";
                                
                                //Status
                                $status = $worksheet->getCell('R'.$i)->getFormattedValue();
                                $insertQuery .= ($status != "" ? ("'".urlencode($status)."'") : "NULL").",";
                                
                                //TalkerPort
                                $tport = $worksheet->getCell('T'.$i)->getFormattedValue();
                                $insertQuery .= ($tport != "" ? ("'".urlencode($tport)."'") : "NULL").",";
                                
                                //NonTalkerPort
                                $nTPort = $worksheet->getCell('U'.$i)->getFormattedValue();
                                $insertQuery .= ($nTPort != "" ? ("'".urlencode($nTPort)."'") : "NULL").",";
                                
                                //LocationID
                                $locationID = $worksheet->getCell('V'.$i)->getFormattedValue();
                                $insertQuery .= ($locationID != "" ? $locationID : "NULL").",";
                                
                                //LocationName
                                $loactionName = $worksheet->getCell('W'.$i)->getFormattedValue();
                                $insertQuery .= ($loactionName != "" ? ("'".urlencode($loactionName)."'") : "NULL").",";
                                
                                //LocationPath
                                $loactionPath = $worksheet->getCell('X'.$i)->getFormattedValue();
                                $insertQuery .= ($loactionPath != "" ? ("'".urlencode($loactionPath)."'") : "NULL").",";
                                
                                //DiscoveryCount
                                $discoveryCount = $worksheet->getCell('Y'.$i)->getFormattedValue();
                                $insertQuery .= ($discoveryCount != "" ? $discoveryCount : "0").",";
                                
                                //TooManyDiscoveries
                                $tDiscovery = $worksheet->getCell('Z'.$i)->getFormattedValue();
                                $insertQuery .= ($tDiscovery != "" ? ("'".urlencode($tDiscovery)."'") : "NULL").",";
                                
                                //PodID
                                $podID = $worksheet->getCell('AA'.$i)->getFormattedValue();
                                $insertQuery .= ($podID != "" ? ("'".urlencode($podID)."'") : "NULL").",";
                                
                                //PodStatus
                                $podStatus = $worksheet->getCell('AB'.$i)->getFormattedValue();
                                $insertQuery .= ($podStatus != "" ? ("'".urlencode($podStatus)."'") : "NULL").",";
                                
                                //CreateBy
                                $insertQuery .= $_SESSION['LOGGED_USER_ID'].",";
                                
                                //CreatedOn
                                $insertQuery .= "'".$createdOn."')";
                                
                                //Deleted records IDs
                                if(! in_array($devDetails[0]["id"], $deletedIDs )) array_push($deletedIDs, $devDetails[0]["id"]);
                                
                            }
                            
                        }
                    }
                    if(sizeof($deletedIDs) > 0){
                        $delIDs = "";
                        foreach ($deletedIDs as $value) { $delIDs .= ($delIDs != "" ? "," : "").$value;}
                        $delSql = "DELETE FROM pmi_switch_schedule WHERE DeviceID IN (".$delIDs.")";
                        
                        //Delete exixted records
                        $db->query($delSql);
                        
                        //Inser new records
                        $db->query($insertQuery);
                        $status = 1;
                    }
                }
                $db->commit();
                
            } else {
                echo "The file $fileName does not exist \n\n\n";
            }
            unlink($fileName);
            $rtnResult["Status"] = $status;
            die(json_encode($rtnResult, TRUE));
        } catch (Exception $exc) {
            echo $exc->getMessage();
            $db->rollback();
            $rtnResult["Status"] = $status;
            die(json_encode($rtnResult, TRUE));
        }
        $db->close();
    }
    
    /***************************************************************
     * Remove existing file
     ***************************************************************/
    Public function removeExistFile() {
        $fileName = FILE_PATH. (urldecode($_REQUEST['fileName']));
        $status = 0;
        try {

            if (file_exists($fileName)) {
               
                unlink($fileName);
                $status = 1;
            }
            $rtnResult["Status"] = $status;
            die(json_encode($rtnResult, TRUE));
        } catch (Exception $exc) {
            throw $exc->getMessage();
        }
    }
    
    /***********************************
     * Update switch name from database
     ***********************************/
    Public function saveSwitchName() {
        $db = new DBC();
        $db->autocommit(false);
        $id = $_REQUEST['deviceID'];
        $name = $_REQUEST['name'];
        $status = 0;
        
        try {
            $updateArr["device_name"] = urlencode(trim($name));
            $where["id"] = $id;
            $status = $db->update_query($updateArr, "pmi_device", $where);
            $db->commit();
            $rtnResult["Status"] = $status;
            die(json_encode($rtnResult, TRUE));
        } catch (Exception $exc) {
            echo $exc->getMessage();
            $db->rollback();
            $rtnResult["Status"] = $status;
            die(json_encode($rtnResult, TRUE));
        }
        $db->close();
    }
    
    /******************************
     * Get first switch element
     *****************************/
    public function  getSwitchSort(){
        $db = new DBC();
        $status = 0;
        try {
            $sql= "SELECT id,device_name FROM pmi_device WHERE `type`=1 AND deleted=0 ORDER BY device_name";
            $result = $db->get_result($sql);
            $devArr = array();
            
            if(sizeof($result) > 0 ){
                for ($i= 0; $i < sizeof($result); $i++) {
                    $devName = $result[$i]["device_name"];
                    $splitName = explode("-", $devName);
                    $devNo = $splitName[1];
                    array_push($devArr, array("DevNo"=>$devNo,"ID" => $result[$i]["id"],"Name" => $devName));
                }
                $status = 1;
            }
            else $status = 0;
            
            $res["result"] = $devArr;
            $res["status"] = $status;
            
            die(json_encode($res, TRUE));
        } catch (Exception $ex) {
            //echo $exc->getMessage();
            $status = 0;
            die(json_encode(array("status" => $status ), TRUE));
        }
    }

}
?>

