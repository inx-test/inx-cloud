<?php
				
    include_once("config.php");
    //require_once("Mail.php");
    
    class UserController extends ControllerBase 
    {	
            # Constructor Method 
            function __constructor(){
            }
                
            //Check Logged in User Exists
            function checkLoginExists()
            {
                $db = new DBC();
                /*START Demo purpose*/
                if(isset($_SESSION['deviceList']) && isset($_SESSION['activeDeviceCount'])){
                    unset($_SESSION['deviceList']);
                    unset($_SESSION['activeDeviceCount']);
                } 
                /*END*/
                
                $username = $_REQUEST['username'];
                $password = $_REQUEST['password'];
                $sql1 = "select id from pmi_user where username ='$username' and password='$password' and deleted = 0 and active = 1";
                $existUser = $db->get_single_result($sql1);
                $sql2 = "select U.role_id,M.dev_role from pmi_user_roles U LEFT JOIN pmi_role_master M ON (M.id = U.role_id) where U.user_id='$existUser' order by U.role_id asc";
                $userRoles = $db->get_result($sql2);
                $roleArray = array();
                $roleNameArray = array();
                $i=0;
                foreach($userRoles as $role)
                {
                    $roleArray[$i] = $role['role_id'];
                    $sql3 = "select role_name from pmi_role_master where id='$roleArray[$i]'";
                    $userRoleName = $db->get_single_result($sql3);
                    $roleNameArray[$i] = $userRoleName;
                    $i++;
                }

                $sql4 = "select firstname,lastname from pmi_user where id='$existUser'";
                $userInfo = $db->get_result($sql4);
                //echo $userInfo[0]['firstname']." ".$userInfo[0]['lastname'];
                
                if($existUser!="")
                {
                    //die(print_r($userRoles));
                    $result['status'] = '1';
                    $result['userID'] = $existUser;
                    $_SESSION['LOGGED_IN_USERNAME'] = $userInfo[0]['firstname']." ".$userInfo[0]['lastname'];
                    $_SESSION['LOGGED_USER_ID'] = $existUser;
                    $_SESSION['LOGGED_USER_ROLE_ID'] = $roleArray[0];
                    $_SESSION['LOGGED_USER_DEVICE_ROLE'] = $userRoles[0]["dev_role"];
                    $_SESSION['LOGGED_USER_ROLES'] = $roleNameArray;
					$_SESSION['LOGGED_USER_ROLEIDS'] = $roleArray;
                    die(json_encode($result));
                }
                else {
                    $result['status'] = '0';
                    die(json_encode($result));
                }
            }
            
            function register()
            {
                $db = new DBC();
                /*START Demo purpose*/
                if(isset($_SESSION['deviceList']) && isset($_SESSION['activeDeviceCount'])){
                    unset($_SESSION['deviceList']);
                    unset($_SESSION['activeDeviceCount']);
                } 
                /*END*/
                
                $result['status'] = 0;
                $fname = $_REQUEST['fname'];
                $lname = $_REQUEST['lname'];
                $username = $_REQUEST['username'];
                $password = $_REQUEST['password'];
                $insId = $_REQUEST['insID'];
                $insPass = $_REQUEST['insPass'];
                
                $userId = $db->get_single_result("select id from pmi_user where username ='$username' and deleted = 0 and active = 1");
                
                if ($userId!="")	$result['message'] = 'Username or Instance ID already in use';
                else
				{
					
					$devInfo = $db->get_result("select * from pmi_inx_device where inspextorID ='$insId' and Password='$insPass' and deleted = 0 and Status=1 and Claimed='N'");
					
					if ( $devInfo[0]["id"] == "" )	$result['message'] = 'Inspextor instance not found';
					else {
						
						// create a client record
						$dbrec = array();
						$dbrec["firstname"] = $fname;
						$dbrec["lastname"]  = $lname;
						$dbrec["email"]     = $username;
						$dbrec["phone"]     = "";
						$dbrec["added_by"]  = 0;
						
						$saveClient = $db->insert_query($dbrec , 'pmi_client');
						$clientId=$db->get_insert_id();
						
						$dbrec = array();
						$dbrec["firstname"] = $fname;
						$dbrec["lastname"]  = $lname;
						$dbrec["email"]     = $username;
						$dbrec["phone"]     = "";
						$dbrec["username"]  = $username;
						$dbrec["password"]  = $password;
						$dbrec["active"]    = 1;
						$dbrec["clientId"]    = $clientId;
						$dbrec["added_by"]  = 0;
						
						$saveUser = $db->insert_query($dbrec , 'pmi_user');
						
						$dbrec = array();
						$dbrec["user_id"] = $db->get_insert_id();
						$dbrec["role_id"]  = ADMIN_ROLE;
						
                        $saveRole = $db->insert_query($dbrec , 'pmi_user_roles');
						
						$devInfo[0]["Claimed"]='Y';
						$devInfo[0]["ClientId"]=$clientId;
						
						$where["id"] = $devInfo[0]["id"];
						unlink($devInfo[0]["id"]);
						//print_r($devInfo);
						$db->update_query($devInfo[0] , 'pmi_inx_device',$where);
						
						$result['status']=1;
					}
				}
                die(json_encode($result));
            }
            
            //Fetch User Informations
            function fetchUserDetails()
            {
                $db = new DBC();
                $userId = $_SESSION['LOGGED_USER_ID'];
                
                $sql = "select * from pmi_user where id='$userId'";
                $userInfo = $db->get_result($sql);
                
                if($userInfo){
                    $result['status'] = '1';
                    $result['firstname'] = $userInfo[0]['firstname'];
                    $result['lastname'] = $userInfo[0]['lastname'];
                    $result['email'] = $userInfo[0]['email'];
                    $result['phoneno'] = $userInfo[0]['phone'];
                    die(json_encode($result));
                }
                else{
                    $result['status'] = '0';
                    die(json_encode($result));
                }                   
                
            }
            
            //Fetch All Users
            function fetchAllUser()
            {
                $db = new DBC();
                
                $loggedUser = $_REQUEST['loggedUser'];
                $imgPath = array();
                $userRoleArr = array();
                
                $where = "deleted=0";
                if($loggedUser!=""){
                    $where .= " and id!='$loggedUser' and added_by='$loggedUser'";
                }
                
                $sql = "select * from pmi_user where $where";
                $userInfo = $db->get_result($sql);
                $i=0;
                
                foreach($userInfo as $user){
                    $userID = $user['id'];
                    
                    $sql3 = "select b.role_name from pmi_user_roles a left join pmi_role_master b on a.role_id=b.id where a.user_id='$userID' order by a.role_id asc";
                    $userRole = $db->get_result($sql3);
                    $userRoleArr[$i] = $userRole[0]['role_name'];
                            
                    $imgPath[$i] = "img/profiles/profile-".$user['id'].".png";
                    if(!file_exists($imgPath[$i])) $imgPath[$i] = "img/profiles/no-image.jpg";
                    $i++;
                }
                
                if($userInfo){
                    $result['status']   = '1';
                    $result['userInfo'] = $userInfo;
                    $result['userRole'] = $userRoleArr;
                    $result['imagePath'] = $imgPath;
                    die(json_encode($result));
                }
                else{
                    $result['status']   = '0';
                    die(json_encode($result));
                }
            }
            
            //Generate Random Password for Forgot Password
            function generatePassword()
            {
                $db = new DBC();
                $CMF = new CommonFunction();
                
                $emailId = $_REQUEST['email'];
                
                $sql1 = "select id,email,username,firstname,lastname from pmi_user where email ='$emailId' and deleted = 0";
                $existEmail = $db->get_result($sql1);
                //print_r($existEmail);
                if(sizeof($existEmail) > 0)
                {
					
                    /*$sql = "SELECT * FROM pmi_smtp_config WHERE status = 1";
                    $smtpInfo = $db->get_result($sql);*/
                    //print_r($smtpInfo);
                    //echo"here1!";
                    //if($smtpInfo != NULL && count($smtpInfo)>0)
						 $userId     = $existEmail[0]['id'];
                        $email      = $existEmail[0]['email'];  
                        $username   = $existEmail[0]['username'];
                        $fristName  = $existEmail[0]['firstname'];
                        $lastName   = $existEmail[0]['lastname'];
					if($email != null)
                    {

                        $randomPassword = $CMF->random_password();

                        //Update Password
                        $dbrec = array();
                        $where = array();
                        $dbrec["password"] = $randomPassword;
                        $where["id"] = $userId;
                        $updatePassword = $db->update_query($dbrec , 'pmi_user',$where);

                        $body = file_get_contents('tpl/emailTemplate.html');
                        $tomail = $email;
                        $subject = "Your password has been changed!";

                         /*Email Content */
                        $HTMLContent = "Hello ".$fristName." ".$lastName."<br/>";
                        $HTMLContent .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; This email confirms that your password has been changed.<br/> ";
                        $HTMLContent .= "To log on to the site, use the following credentials:<br/><br/>";
                        $HTMLContent .= "Username : ".$username."<br/><br/>";
                        $HTMLContent .= "Password : ".$randomPassword."<br/><br/>";
                        $HTMLContent .= "<br /><br />Thanks.";

                        /*Replace Emaile Template file*/
                        $body = str_replace("[[EMAIL_TITLE]]",$subject,$body);
                        $body = str_replace("[[EMAIL_CONTENT]]",$HTMLContent,$body);

                        /*Send Mail*/
                        //$sendMail = $CMF->sendEmail($subject,$body,$tomail,$smtpInfo[0]["from_email"],$smtpInfo[0]["smtp_server"],$smtpInfo[0]["password"],$smtpInfo[0]["smtp_port"]);
                        $from ="inspextor.help@gmail.com";
						$host ="smtp.gmail.com";
						$password ="pmi123456";
						$port = 587;
						$sendMail = $CMF->sendEmail($subject,$body,$tomail,$from,$host,$password,$port );
                        
                        if($sendMail){
                            $result['status'] = '1';
                            $result['message'] = "We have emailed you a new password";
                            die(json_encode($result));
                        }
                        else{
                            $result['status'] = '0';
                            $result['message'] = "Mail Sending Failed";
                            die(json_encode($result));
                        }
                    }
                    else{
                        $result['status'] = '0';
                        $result['message'] = "Mail process not configured, please contact administrator";
                        die(json_encode($result));
                    }
                }
                else{
                    $result['status'] = '0';
                    $result['message'] = "Wrong email provided!";
                    die(json_encode($result));
                }
            }
            
            //Check Old Password
            function checkOldPassword()
            {
                $db = new DBC();
                
                $oldPass = $_REQUEST['oldPassword'];
                $userId = $_SESSION['LOGGED_USER_ID'];
                
                $sql = "select id from pmi_user where password='$oldPass' and id='$userId'";
                $existPass = $db->get_single_result($sql);
                
                if($existPass)
                {
                    $result['status'] = '1';
                    die(json_encode($result));
                }
                else{
                    $result['status'] = '0';
                    $result['message'] = "Old Password does not match!";
                    die(json_encode($result));
                }
            }
            
            //Save New Password
            function saveNewPassword()
            {
                $db = new DBC(DB_HOST,DB_USER,DB_PASSWD,DB_NAME_CLOUD);	//password save is against cloud database
                
                $newPass = $_REQUEST['newPassword'];
                $userId = $_SESSION['LOGGED_USER_ID'];
                
                //Update Password
                $dbrec = array();
                $where = array();
                $dbrec["password"] = $newPass;
                $where["id"] = $userId;
                $updatePassword = $db->update_query($dbrec , 'pmi_user',$where);
                
                if($updatePassword)
                {
                    $result['status'] = '1';
                    $result['message'] = "Successfully Update Password";
                    die(json_encode($result));
                }
                else{
                    $result['status'] = '0';
                    $result['message'] = "Failed to update Password";
                    die(json_encode($result));
                }
            }
            
            //Save User Informations
            function saveUserDetails()
            {
                $db = new DBC();
                
                $userId     = $_SESSION['LOGGED_USER_ID'];
                $firstName  = $_REQUEST['firstname'];
                $lastName   = $_REQUEST['lastname'];
                $email      = $_REQUEST['email'];
                $phoneNo    = $_REQUEST['contact'];
                $name       = $firstName." ".$lastName;
                //Update User Details
                $dbrec = array();
                $where = array();
                
                $dbrec["firstname"] = mysqli_real_escape_string($db,$firstName);
                $dbrec["lastname"]  = mysqli_real_escape_string($db,$lastName);
                $dbrec["email"]     = $email;
                $dbrec["phone"]     = $phoneNo;
                
                $where["id"]        = $userId;
                $files = $_REQUEST['image'];
                
                if( $files != "")
                {
                    
                    $fileSize = $this->saveProfileImage($userId,$files);
                        
                }
                $updateUser = $db->update_query($dbrec , 'pmi_user',$where);
                
                if($updateUser){
                    $_SESSION["LOGGED_IN_USERNAME"] = $name;
                    $result['status'] = '1';
                    $result['message'] = "Successfully updated user details";
                    $result['name'] = $name;
                    die(json_encode($result));
                }
                else{
                    $result['status'] = '0';
                    $result['message'] = "Failed to update user details";
                    die(json_encode($result));
                }
            } 
            
            function saveProfileImage($userID, $files)
            {
               
                    /*if (!file_exists(PROFILEPICTURE_PATH)) {
                            mkdir($profileDestPath, FOLDER_CREATE_PERMISSION, true);
                    }*/
                    $info = pathinfo($files);
                    $ext = $info['extension']; // get the extension of the file
                    $currentFile = $info['basename'];
                    $targetFilename="profile-".$userID.".png";

                    $src = PROFILEPIC_PATH . "/" . $currentFile;
                    $target = PROFILEPIC_PATH . "/" . $targetFilename;
                    // echo $src ." -- ". $target;
                    rename( $src , $target);

                    //echo $src ." -- ". $target;

                    $fileSize = 0;
                    $srcThumbnail = THUMBNAILS_PATH . DIRECTORY_SEPARATOR . $files;

                    if ( file_exists($src) )	{

                            if ( file_exists($target) )	unlink($target);	// delete previous version
                                    // convert image to png

                            imagepng(imagecreatefromstring(file_get_contents($src)), $target);

                            //if ( file_exists($srcThumbnail) ) unlink($srcThumbnail);// delete src thumbnail as we dont need it.
                    }

                    $fileSize += filesize($target);
                    if(file_exists($srcThumbnail) ) {
                            $fileSize += filesize($srcThumbnail);
                            unlink($srcThumbnail);
                    }

                    if ( file_exists($src) ){
                            unlink($src);
                    }
                    return $fileSize;
            }
            
            function saveUserInfo()
            {
                $db = new DBC();
                
                $loggedUserId       = $_SESSION['LOGGED_USER_ID'];
                $firstName          = $_REQUEST['firstname'];
                $lastName           = $_REQUEST['lastname'];
                $email              = $_REQUEST['email'];
                $contactno          = $_REQUEST['contact'];
                $username           = $_REQUEST['username'];
                $password           = $_REQUEST['password']; 
                
                $sql1 = "select id from pmi_user where (username ='$username' OR email = '$email') and deleted=0";
                $userExist = $db->get_single_result($sql1);
                
                if($userExist){
                    $result['status'] = '0';
                    $result['message'] = "Username or Email Already Exist";
                    die(json_encode($result));
                }
                
                $dbrec = array();
                $dbrec["firstname"] = $firstName;
                $dbrec["lastname"]  = $lastName;
                $dbrec["email"]     = $email;
                $dbrec["phone"]     = $contactno;
                $dbrec["username"]  = $username;
                $dbrec["password"]  = $password;
                $dbrec["active"]    = 0;
                $dbrec["added_by"]  = $loggedUserId;
                
                $saveUser = $db->insert_query($dbrec , 'pmi_user');
                
                $userrole = $_REQUEST['userrole']; 
                $sql2 = "select id from pmi_user where username ='$username' and deleted = 0";
                $lastInsertId = $db->get_single_result($sql2);
                
                $limit = sizeof($userrole);
                $roleRec = array();
                
                for($i=0; $i<$limit; $i++)
                {
                   // echo 'role'.$userrole[$i].'>>>ID...'.$lastInsertId;
                   $roleRec["role_id"] = $userrole[$i];
                   $roleRec["user_id"] = $lastInsertId;
                   $saveUserRole = $db->insert_query($roleRec , 'pmi_user_roles');
                }
                
                if($saveUser && $saveUserRole){
                    $result['status'] = '1';
                    $result['message'] = "Successfully Added User";
                    die(json_encode($result));
                }
                else{
                    $result['status'] = '0';
                    $result['message'] = "Failed to Add User";
                    die(json_encode($result));
                }
            }
            
            function updateUserInfo()
            {
                $db = new DBC();
                
                $loggedUserId       = $_SESSION['LOGGED_USER_ID'];
                $firstName          = $_REQUEST['firstname'];
                $lastName           = $_REQUEST['lastname'];
                $email              = $_REQUEST['email'];
                $contactno          = $_REQUEST['contact'];
                $username           = $_REQUEST['username'];
                $password           = $_REQUEST['password']; 
                $userID             = $_REQUEST["userID"];
                
                $sql1 = "select id from pmi_user where username ='$username' and deleted = 0 and id not in ('$userID')";
                $userExist = $db->get_single_result($sql1);

                if($userExist){
                    $result['status'] = '0';
                    $result['message'] = "Username Already Exist";
                    die(json_encode($result));
                }

                $dbrec = array();
                $dbrec["firstname"] =  mysqli_real_escape_string($db,$firstName);
                $dbrec["lastname"]  =  mysqli_real_escape_string($db,$lastName);
                $dbrec["email"]     = $email;
                $dbrec["phone"]     = $contactno;
                $dbrec["username"]  = $username;
                $dbrec["password"]  = $password;
                $dbrec["added_by"]  = $loggedUserId;
                
                $where["id"] = $userID;
                $saveUser = $db->update_query($dbrec , 'pmi_user',$where);
                
                $userrole = $_REQUEST['userrole']; 
                $sql2 = "delete from pmi_user_roles where user_id ='$userID'";
                $oldRoles = $db->query($sql2);

                $limit = sizeof($userrole);
                $roleRec = array();

                for($i=0; $i<$limit; $i++)
                {
                    $roleRec["role_id"] = $userrole[$i];
                    $roleRec["user_id"] = $userID;
                    $saveUserRole = $db->insert_query($roleRec , 'pmi_user_roles');
                }

                if($saveUser && $saveUserRole){
                    $result['status'] = '1';
                    $result['message'] = "Successfully updated user";
                    die(json_encode($result));
                }
                else{
                    $result['status'] = '0';
                    $result['message'] = "Failed to update user";
                    die(json_encode($result));
                }
            }
            
            //Change User Status (Enable/Disable)
            function updateUserStatus()
            {
                $db = new DBC();
                
                $userId = $_REQUEST['userid'];
                $userStatus = $_REQUEST['userstatus'];
                
                $dbrec = array();
                $where = array();
                
                if($userStatus == 0)
                {
                    $dbrec["active"]    = 1;
                    $status = "enabled";
                }
                else{
                    $dbrec["active"]    = 0;
                    $status = "disabled";
                }
                
                $where["id"] = $userId;
                $updateStatus = $db->update_query($dbrec , 'pmi_user',$where);
                
                if($updateStatus)
                {
                    $result['status'] = '1';
                    $result['userstatus'] = $userStatus;
                    $result['message'] = "Successfully ".$status." user";
                    die(json_encode($result));
                }
                else{
                    $result['status'] = '0';
                    $result['message'] = "Failed to ".$status." user";
                    die(json_encode($result));
                }
            } 
            
            //Remove User
            function removeUserInfo()
            {
                $db = new DBC();
                
                $userId = $_REQUEST['userid'];
                
                $dbrec = array();
                $where = array();
                $dbrec["deleted"]       = 1;
                $dbrec["deleted_by"]    = $userId;
                $where["id"]            = $userId;
                
                $removeUser = $db->update_query($dbrec , 'pmi_user',$where);
                
                if($removeUser)
                {
                    $result['status'] = '1';
                    $result['id'] = $userId;
                    $result['message'] = "Successfully Removed User";
                    die(json_encode($result));
                }
                else{
                    $result['status'] = '0';
                    $result['message'] = "Failed to Remove User";
                    die(json_encode($result));
                }
            } 
            
            //Fetch User Roles
            function fetchUserRole(){
                $db = new DBC();
                
                $userId = $_REQUEST['userid'];
                
                $sql = "select role_id from pmi_user_roles where user_id='$userId'";
                $existUserRoles = $db->get_result($sql);
                
                if($existUserRoles){
                    $result["status"] = '1';
                    $result["role"] = $existUserRoles;
                    die(json_encode($result));
                }
                else{
                    $result["status"] = '0';
                    die(json_encode($result));
                }
                
            }
            
            //Fetch User data by user ID
            function fetchUserData(){
                $db = new DBC();
                
                $userId = $_REQUEST['userid'];
                
                $sql = "SELECT firstname,lastname,email,phone,username,password FROM pmi_user WHERE id = '$userId'";
                $existUserdata = $db->get_result($sql);
                
                $sql = "select role_id from pmi_user_roles where user_id='$userId'";
                $existUserRoles = $db->get_result($sql);
                
                if($existUserRoles){
                    $result["status"] = '1';
                    $result["result"] = $existUserdata;
                    $result["role"] = $existUserRoles;
                    die(json_encode($result));
                }
                else{
                    $result["status"] = '0';
                    die(json_encode($result));
                }
                
            }
            
            //Fetch All User
            function fetchAllUserRole()
            {
                $db = new DBC();
                $sql = "select id,role_name from pmi_role_master";
                $allUserRoles = $db->get_result($sql);
                //print_r($existUserRoles);
               // die();
                
                if($allUserRoles){
                    $result["status"] = '1';
                    $result["userrole"] = $allUserRoles;
                    die(json_encode($result));
                }
                else{
                    $result["status"] = '0';
                    die(json_encode($result));
                }
            }
            
            //Update User Roles
            function updateUserRole()
            {
                $db = new DBC();
                
                $userId = $_REQUEST['userid'];
                $userRole = $_REQUEST['roles'];
                
                if(sizeof($userRole))
                {
                    $sql = "delete from pmi_user_roles where user_id='$userId'";
                    $delExistUserRole = $db->query($sql);
                    
                    $dbrec = array();
                    
                    for($i=0;$i<sizeof($userRole);$i++)
                    {
                        $dbrec["role_id"] = $userRole[$i];
                        $dbrec["user_id"] = $userId;
                        $updateRole = $db->insert_query($dbrec , 'pmi_user_roles');
                        //echo $userRole[$i];
                    }
                   
                    if($updateRole){
                        $sql3 = "select a.role_id, b.role_name from pmi_user_roles a left join pmi_role_master b on a.role_id=b.id where a.user_id='$userId' order by a.role_id asc";
                        $existingRoles = $db->get_result($sql3);
                        
                        /*$priorRole = $existingRoles[0][role_name];
                        $allRoleArray = array();
                        for($j=0;$j<sizeof($existingRoles);$j++){
                            $allRoleArray[$j] =  $existingRoles[$j]['role_name'];
                        }
                        $allRole = implode(",",$allRoleArray); */
                        
                        $result["status"] = '1';
                        $result["userid"] = $userId;
                        $result['message'] = "Successfully updated user role";
                        die(json_encode($result));
                    }
                    else{
                        $result["status"] = '0';
                        $result['message'] = "Failed to update user role";
                        die(json_encode($result));
                    }
                }
                else{
                    $result["status"] = '0';
                    $result['message'] = "Please select atleast one role";
                    die(json_encode($result));
                }
                
            }
            
            //Save Thumbnail Profile Pic
            function saveThumbnail()
            {
                $CMF = new CommonFunction();
                
                $croppedImage = '';
                $cropped = '';
                
                $x1 = $_REQUEST["x1"];
                $y1 = $_REQUEST["y1"];
                $x2 = $_REQUEST["x2"];
                $y2 = $_REQUEST["y2"];
                $w =  $_REQUEST["w"];
                $h =  $_REQUEST["h"];
                $imagePath = $_REQUEST["imagePath"]; // Uploaded image path
                
                $thumbnailPath =  THUMBNAILS_PATH; 
   
                $thumb_image_location = THUMBNAILS_PATH; // Thumb Image location 

                $expansionScale = 1/$_SESSION["image_scale"];

                $x1 = floor($_REQUEST["x1"]*$expansionScale);
                $y1 = floor($_REQUEST["y1"]*$expansionScale);
                $w =  ceil($_REQUEST["w"]*$expansionScale);
                $h =  ceil($_REQUEST["h"]*$expansionScale);

                $scale = 200/$w;
                if($imagePath != '')
                {
                        $thumbImgLocation = explode('/',$imagePath);

                        if(sizeof($thumbImgLocation) > 0)
                        {
                                $croppedImage = $thumbImgLocation[sizeof($thumbImgLocation) -1];
                                $thumb_image_location.= "/".$croppedImage;


                                $cropped = $CMF->resizeThumbnailImage($thumb_image_location, $imagePath,$w,$h,$x1,$y1,$scale,$thumbnailPath);

                        }
                }
		
                $result["croppedImage"] = $croppedImage;	
		die(json_encode($result));
            }
            
    }
?>
