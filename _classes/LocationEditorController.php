<?php
    include_once("config.php");
    
    class LocationEditorController extends ControllerBase 
    {	
        # Constructor Method 
        function __constructor(){
        }

        //Get menu details
        function getMenuDetails(){
            $db = new DBC();
            $roleID = $_SESSION['LOGGED_USER_ROLE_ID'];
            
            $sql = "SELECT M.ID,
                    M.MenuName,
                    M.Activity,
                    M.DisplayOrder AS MenuOrder,
                    M.CanAdd,
                    M.CanEdit,
                    M.CanDelete,
                    M.Visible,
                    CM.ID AS ChildID,
                    CM.ParentID,
                    CM.ChildMenuName,
                    CM.ChildActivity,
                    CM.DisplayOrder AS ChildOrder
                    FROM 
                    pmi_menu_mapping M 
                    LEFT JOIN pmi_child_menu_mapping CM ON M.ID = CM.ParentID AND CM.Deleted = 0 
                    WHERE M.RoleID = ".$roleID." AND M.Deleted = 0  
                    ORDER BY M.DisplayOrder,CM.DisplayOrder ";
            //die($sql);
            $treeview = $db->get_result($sql);  
            $result['reuslt'] = $treeview;
            $result['status'] = 0;
            die(json_encode($result));
        }
        
        // ADD TREE MENU
        function addMenu(){
            $db = new DBC();
            $id = $_REQUEST['id'];
            $menuName = $_REQUEST['menuName'];
            $activity = $_REQUEST['activity'];
            if($id == "id1"){
                
                $sql = "SELECT MAX(DisplayOrder) AS Displayorder FROM pmi_menu_mapping";
                $order = $db->get_result($sql);
                $incOrder = (int) $order[0]["Displayorder"];
                $dbrec = array();
                $dbrec["RoleID"] = $_SESSION['LOGGED_USER_ROLE_ID'];
                $dbrec["MenuName"] = $menuName;
                $dbrec["Activity"] = $activity;
                $dbrec["DisplayOrder"] = $incOrder + 1;
                $dbrec["CanAdd"] = 1;
                $dbrec["CanEdit"] = 1;
                $dbrec["CanDelete"] = 1;
                $dbrec["CreatedBy"] = $_SESSION['LOGGED_USER_ID'];
                $inserResult = $db->insert_query($dbrec, "pmi_menu_mapping");
                if($inserResult){
                    $result['status'] = '1';
                    $result['id'] = $db->get_insert_id();
                    $result['message'] = "Successfully added menu";
                    die(json_encode($result));
                }
                else{
                    $result['status'] = '0';
                    $result['message'] = "Failed to add menu";
                    die(json_encode($result));
                }
            }
            else{
                $sql = "SELECT MAX(DisplayOrder) AS Displayorder FROM pmi_child_menu_mapping WHERE ParentID = ".$id;
                //echo $sql;
                $order = $db->get_result($sql);
                $incOrder = (int) $order[0]["Displayorder"];
                
                $sql =  "SELECT MAX(DisplayOrder) AS Displayorder FROM pmi_menu_mapping WHERE ParentID = ".$id;
                //echo $incOrder;
                $dbrec = array();
                $dbrec["ParentID"] = $id;
                $dbrec["ChildMenuName"] = $menuName;
                $dbrec["ChildActivity"] = $activity;
                $dbrec["DisplayOrder"] = $incOrder + 1;
                $dbrec["CreatedBy"] = $_SESSION['LOGGED_USER_ID'];
                $inserResult = $db->insert_query($dbrec, "pmi_child_menu_mapping");
                if($inserResult){
                    $result['status'] = '1';
                    $result['id'] = $db->get_insert_id();
                    $result['message'] = "Successfully added menu";
                    die(json_encode($result));
                }
                else{
                    $result['status'] = '0';
                    $result['message'] = "Failed to add menu";
                    die(json_encode($result));
                }
            }
            
        }
        
        // EDIT TREE MENU NAME
        function updateMenuName(){
            $db = new DBC();
            $id = $_REQUEST['id'];
            $menuName = $_REQUEST['menuName'];
            $activity = $_REQUEST['activity'];
            $splitID = explode("_", $id);
            
            $dbrec = array();
            $where = array();
            if($splitID[0] == "id"){
                
                $dbrec["MenuName"] = $menuName;
                $dbrec["Activity"] = $activity;
                $where["ID"] = $splitID[1];
                $updateResult = $db->update_query($dbrec , 'pmi_menu_mapping',$where);
                if($updateResult){
                    $result['status'] = '1';
                    $result['message'] = "Successfully updated menu name";
                    die(json_encode($result));
                }
                else{
                    $result['status'] = '0';
                    $result['message'] = "Failed to edit menu name";
                    die(json_encode($result));
                }
            }
            else{
                $dbrec["ChildMenuName"] = $menuName;
                $dbrec["ChildActivity"] = $activity;
                $where["ID"] = $splitID[1];
                $updateResult = $db->update_query($dbrec , 'pmi_child_menu_mapping',$where);
                if($updateResult){
                    $result['status'] = '1';
                    $result['message'] = "Successfully updated menu name";
                    die(json_encode($result));
                }
                else{
                    $result['status'] = '0';
                    $result['message'] = "Failed to edit menu name";
                    die(json_encode($result));
                }
            }
            
        }
        
        
        // DELETE TREE MENU 
        function delMenu(){
            $db = new DBC();
            $id = $_REQUEST['id'];
            $splitID = explode("_", $id);
            
            $dbrec = array();
            $where = array();
            if($splitID[0] == "id"){
                
                $dbrec["Deleted"] = 1;
                $where["ID"] = $splitID[1];
                $updateResult = $db->update_query($dbrec , 'pmi_menu_mapping',$where);
                if($updateResult){
                    $result['status'] = '1';
                    $result['message'] = "Successfully updated menu name";
                    die(json_encode($result));
                }
                else{
                    $result['status'] = '0';
                    $result['message'] = "Failed to edit menu name";
                    die(json_encode($result));
                }
            }
            else{
                $dbrec["Deleted"] = 1;
                $where["ID"] = $splitID[1];
                $updateResult = $db->update_query($dbrec , 'pmi_child_menu_mapping',$where);
                if($updateResult){
                    $result['status'] = '1';
                    $result['message'] = "Successfully updated menu name";
                    die(json_encode($result));
                }
                else{
                    $result['status'] = '0';
                    $result['message'] = "Failed to edit menu name";
                    die(json_encode($result));
                }
            }
            
        }
        
    }
?>
