<?php
    include_once("config.php");    
    require_once('UDP/PMIPacket.php');
    require_once('UDP/UDP.php');
    
    class LocationController extends ControllerBase 
    {	
            # Constructor Method 
            function __constructor(){
            }
            
            //Fetch Device Information
            function fetchDeviceInfo(){
                $db = new DBC();
                $clusterID = 0;
                if(isset($_REQUEST['clusterID'])) $clusterID = $_REQUEST['clusterID'];
                $nodeType = FIXTURE_TYPE_NODE;
                $wsType = FIXTURE_TYPE_WALLSWITCH;
                $sql = <<<EOF
                        SELECT C.ID AS ClusterID, T.ID AS TagID,N.Name AS NodeName,T.Name, MP.x_pos, MP.y_pos, MP.enabled,T.TagTypeID,T.NodeID
                        FROM pmi_cluster C
                        LEFT JOIN pmi_cluster_mapping CM ON C.ID = CM.ClusterID AND CM.Deleted=0
                        LEFT JOIN pmi_node N ON CM.NodeID = N.ID
                        LEFT JOIN pmi_tag T ON N.ID = T.NodeID
                        LEFT JOIN pmi_map_nodes MP ON T.ID = MP.TagID AND MP.ClusterID = C.ID
                        WHERE C.Deleted=0 AND T.Deleted=0 AND N.Deleted=0 AND C.ID=$clusterID AND 
                        (TagTypeID = $nodeType OR TagTypeID = $wsType)
EOF;
                
                //die($sql);
                //$sql = "select a.id, a.device_name, a.serial_no, a.ip_address, a.location, b.x_pos, b.y_pos, b.enabled from pmi_device a left join pmi_map_nodes b on a.id=b.device_id where a.deleted=0";
                $allDevices = $db->get_result($sql);
                
                //print_r($allDevices);
                //die();
                
                $sql2 = "select filename from pmi_location_image where ClusterID=$clusterID  order by id desc";
                $filename = $db->get_single_result($sql2);
                
                if(sizeof($allDevices)>0){
                    $result['status']       = '1';
                    $result['device']       = $allDevices;
                    $result['image']        = $filename;
                    die(json_encode($result));
                }
                else{
                    $result['status']       = '0';
                    $result['image']        = $filename;
                    die(json_encode($result));
                }
            }
            
            //Add new uploaded location image
            function saveUploadImage()
            {
                $db = new DBC();
                
                $loggedUserId   = $_SESSION['LOGGED_USER_ID'];
                $imageName      = $_REQUEST['imagename'];
                $clusterID      = $_REQUEST['clusterId'];
                
                $sql = "select id from pmi_location_image where filename='$imageName' AND ClusterID=$clusterID";
                $existDuplicate = $db->get_single_result($sql);
                
                if($imageName!="" && $existDuplicate==""){
                    $dbrec = array();
                    $dbrec["ClusterID"] = $clusterID;
                    $dbrec["filename"]         = $imageName;
                    $dbrec["uploaded_by"]      = $loggedUserId;
                    $dbrec["uploaded_on"]      = date("Y-m-d");
                    
                    $addImage = $db->insert_query($dbrec , 'pmi_location_image');
                }
                if($addImage){
                    $result['status']       = '1';
                    $result['message']      = 'Successfully uploaded location map.';
                    die(json_encode($result));
                }
                else{
                    $result['status']       = '0';
                    $result['message']      = 'Failed to upload location map.';
                    die(json_encode($result));
                }
            }
            
            //Save Device Node Positions
            function saveMapNode(){
                $db = new DBC();
                
                $loggedUserId       = $_SESSION['LOGGED_USER_ID'];
                $nodeMapArr         = $_REQUEST['nodes'];
                $clusterID          = $_REQUEST['clusterID'];
                
                //$qry = "select id from pmi_location_image order where by id desc";
                //$mapId = $db->get_single_result($qry);
                
                $sql = "select TagID from pmi_map_nodes where ClusterID=$clusterID";
                $existNodes = $db->get_result($sql);
                
                $nodeArr = array();
                $k=0;
                foreach($existNodes as $item){
                    $nodeArr[$k] = $item['TagID'];
                    $k++;
                }
                
                if(sizeof($existNodes)==0){
                    for($i=0; $i<sizeof($nodeMapArr); $i++)
                    {
                        $id   = $nodeMapArr[$i]["tagID"];
                        $xpos = $nodeMapArr[$i]["xPos"];
                        $ypos = $nodeMapArr[$i]["yPos"];
                        
                        $dbrec = array();
                        
                        //$dbrec["map_id"]    = $mapId;
                        $dbrec["ClusterID"] = $clusterID;
                        $dbrec["TagID"] = $id;
                        $dbrec["x_pos"]     = $xpos;
                        $dbrec["y_pos"]     = $ypos;
                        $dbrec["enabled"]   = 1;
                        $dbrec["created_by"] = $loggedUserId;
                        $dbrec["created_on"] = date("Y-m-d");
                        
                        $addNodes = $db->insert_query($dbrec , 'pmi_map_nodes');
                    }
                }
                else{
                    for($i=0; $i<sizeof($nodeMapArr); $i++)
                    {
                        $id   = $nodeMapArr[$i]["tagID"];
                        $xpos = $nodeMapArr[$i]["xPos"];
                        $ypos = $nodeMapArr[$i]["yPos"];
                        if(in_array($id,$nodeArr)){
                            $dbrec = array();
                            $where = array();

                            $where["TagID"] = $id;
                            $where["ClusterID"] = $clusterID;

                            //$dbrec["map_id"]    = $mapId;
                            $dbrec["x_pos"]     = $xpos;
                            $dbrec["y_pos"]     = $ypos;
                            $dbrec["enabled"]   = 1;

                            $updateNode = $db->update_query($dbrec , 'pmi_map_nodes',$where); 
                        }
                        else{
                            
                            $dbrec = array();
                        
                            //$dbrec["map_id"]    = $mapId;
                            $dbrec["TagID"] = $id;
                            $dbrec["ClusterID"] = $clusterID;
                            $dbrec["x_pos"]     = $xpos;
                            $dbrec["y_pos"]     = $ypos;
                            $dbrec["enabled"]   = 1;
                            $dbrec["created_by"] = $loggedUserId;
                            $dbrec["created_on"] = date("Y-m-d");

                            $addNodes = $db->insert_query($dbrec , 'pmi_map_nodes');
                        }
                    }
                    
                    for($j=0; $j<sizeof($nodeArr); $j++)
                    {
                        $tagID = $nodeArr[$j];
                        
                        $nodeMapIDArr = array();
                        $k=0;
                        foreach($nodeMapArr as $item){
                            $nodeMapIDArr[$k] = $item['tagID'];
                            $k++;
                        }
                        
                        if(in_array($tagID,$nodeMapIDArr) == 0){
                            $dbrec = array();
                            $where = array();
                            echo "stat == ".$tagID;
                            $where["TagID"] = $tagID;
                            $where["ClusterID"] = $clusterID;

                            $dbrec["enabled"]   = 0;

                            $updateStatus = $db->update_query($dbrec , 'pmi_map_nodes',$where); 
                        }
                    }
                }
                
                
                
                if($addNodes || $updateNode){
                    $result['status']    =  '1';
                    $result['message']   =  "Successfully updated tag location.";
                    die(json_encode($result));
                }
                else{
                    $result['status']    =  '0';
                    $result['message']   =  "Failed to update tag location.";
                    die(json_encode($result));
                }
                
            }
            
            
            //GET COMMENT EXECUTE BUTTON
            function getExeCommands(){
                try {
                    
                    $UDPHelper = new UDP();
                    $commands = $UDPHelper->getCommandList();
                    $result['reuslt'] = $commands;
                    $result['status'] = 1;
                    die(json_encode($result));
                } catch (Exception $exc) {
                    $result['status'] = 0;
                    $result['error'] = $exc->getMessage();
                }
            }
            
            //GET COMMENT EXECUTE BUTTON
                    
            //GET COMMENT EXECUTE BUTTON
            function exeCommands(){
                $db = new DBC();
                try {
                    $devID =  isset($_REQUEST['devID']) ? $_REQUEST['devID'] : 0;
                    $devType =  isset($_REQUEST['type']) ? $_REQUEST['type'] : 0;
                    $cmd = $_REQUEST['cmd'];
                    $range = $_REQUEST['rangeVal'];
                    $nodeType = FIXTURE_TYPE_NODE;
                    $packetQueueID = 0;
                    $name = "";
                    $result['status'] = 0;
                    $TGStatusArr = array();
                    $qry = "";
                    $cmdStr = $UDPHelper->commands[$cmd];
                    
                    if($devType == 1){
                    $qry = <<<EOF
                                SELECT 
                                    T.ID,
                                    T.Name,
                                    T.IPAddress 
                                FROM pmi_cluster_mapping C 
                                    LEFT JOIN pmi_node N ON (C.NodeID = N.ID)
                                    LEFT JOIN pmi_tag T ON (N.ID = T.NodeID)
                                WHERE 
                                    C.ClusterID=$devID AND 
                                    C.Deleted=0 AND 
                                    T.Acknowledged=1 AND 
                                    T.Deleted=0 AND 
                                    N.Deleted=0 AND 
                                    T.TagTypeID=$nodeType
EOF;
                    }
                    else if($devType == 2){
                        $qry = <<<EOF
                            SELECT ID,IPAddress,Name FROM pmi_tag WHERE Deleted=0 AND Acknowledged=1 AND NodeID=$devID AND TagTypeID=$nodeType
EOF;
                    }
                    //echo $qry;
                    $nodes = $db->get_result_array($qry);
                    //print_r($nodes);
                    $packetDetails =  array();
                    $status =false;
                    if(sizeof($nodes)){
                        for ($i = 0; $i < count($nodes); $i++) {
                            $node = $nodes[$i];
                            $tagID = $node["ID"];
                            $ipAddress = $node["IPAddress"];
                            $name = $node["Name"];
                            
                            $cmdPacket = new PMIPacket();
                            $UDPHelper = new UDP();
                            $cmf =  new CommonFunction();
                            $packetID = $cmf->getUDPPacketID($db);
                            $cmdPacket->packetId = $packetID;
                            $cmdPacket->tagIdHighByte = $UDPHelper->getHiByte($tagID);
                            $cmdPacket->tagIdLowByte = $UDPHelper->getLoByte($tagID);
                            $cmdPacket->setPacketType(PT_COMMANDS);
                            $cmdPacket->setCommand($cmd);
                            if($range >= 0)$cmdPacket->setDim($range);
                            
                            //Packet queue updation
                            $insertArr = array();
                            $insertArr["Direction"] = UDP_SEND;
                            $insertArr["TagID"] = $tagID;
                            $insertArr["PacketID"] = $packetID;
                            $insertArr["BinaryData"] = $cmdPacket->getBinary();
                            $insertArr["DecimalData"] = $cmdPacket->getDecimal();
                            $insertArr["CMD"] = $cmd;
                            $insertArr["SendCount"] = 1;
                            $status = $db->insert_query($insertArr, "pmi_packet_queue");
                            $packetQueueID = $db->get_insert_id();
                            
                            $statArr = array("TagID"=>$tagID,"Name"=>$name,"Status"=>0,"QID"=>$packetQueueID,"PacketID"=>$packetID,"MSG"=>"Failed","ipAddress"=>$ipAddress);
                            array_push($TGStatusArr, $statArr);
                            // Send
                            $status = $UDPHelper->send($ipAddress, NODE_LISTENER_PORT, $cmdPacket);
                            //echo ">>>>> $status \n";
                        }
                    }
                    
                    if(sizeof($TGStatusArr) > 0){
                        $ackCheckerCount = 0;
                        $ProcComplete = false;
                        while ($ackCheckerCount < 3){
                            if($ProcComplete) break;
                            $ProcComplete = TRUE;
                            sleep(RESEND_DELAY);
                            for($j=0; $j < sizeof($TGStatusArr); $j++){
                                $StaItem = $TGStatusArr[$j];
                                $tStat = $StaItem["Status"];
                                $tID = $StaItem["TagID"];;
                                $qID = $StaItem["QID"];
                                $tName = $StaItem["Name"];
                                $packetID = $StaItem["PacketID"];
                                $ipAddress = $StaItem["ipAddress"];
                            
                                if($tStat == 0){
                            $qry = <<<EOF
                                        SELECT Acked,Error,SendCount FROM pmi_packet_queue WHERE ID=$qID AND Error=0
EOF;
                            $ackArr = $db->get_result($qry);
                            //print_r($ackArr);
                            if(sizeof($ackArr) > 0){
                                $ack = $ackArr[0]["Acked"];
                                        $sendCout = $ackArr[0]["SendCount"]; 
                                
                                        if($ack == 1) $tStat = 1;  
                                        else{
                                            if($sendCout == 3) $tStat = 2; 
                                            else{
                                                $ProcComplete = FALSE;
                                                $cmdPacket = new PMIPacket();
                                                $UDPHelper = new UDP();
                                                $cmdPacket->packetId = $packetID;
                                                $cmdPacket->tagIdHighByte = $UDPHelper->getHiByte($tID);
                                                $cmdPacket->tagIdLowByte = $UDPHelper->getLoByte($tID);
                                                $cmdPacket->setPacketType(PT_COMMANDS);
                                                $cmdPacket->setCommand($cmd);
                                                if($range >= 0)$cmdPacket->setDim($range);

                                                $updateArr = array();
                                                $updateArr["SendCount"] = $sendCout+1;
                                                $db->update_query($updateArr, "pmi_packet_queue", array("ID"=>$qID));

                                                // Send
                                                $status = $UDPHelper->send($ipAddress, NODE_LISTENER_PORT, $cmdPacket);
                            }
                        }
                    }
                                    if($tStat == 2){
                        //Update error log
                        $UDPHelper = new UDP();
                        $param = array();
                        $errorMsg =  $cmdStr." execution failed for ".$tName;
                        $param["DeviceID"] = $tID;
                        $param["AlertDefID"] = 0;
                        $param["Severity"] = S_CRITICAL;
                        $param["Title"] = "Failed to execute command";
                        $param["Description"] = $errorMsg;
                        $param["Value"] = 0;
                        $param["Status"] = 0;
                        $param["AlertType"] = ALERT_OTHER;
                                        $param["TypeDBID"] = $qID;
                        $param["TypeAction"] = 0;//Policy creation
                        $param["TypeMasterID"] = 0;//Requested ID
                        $param["PolicyType"] = 0;
                        $cmf = new \CommonFunction();
                        $alertStatus = $cmf->insertErrorLog($param, $db);
                                        
                                        $TGStatusArr[$j]["Status"] = 2;
                                        $TGStatusArr[$j]["MSG"] = $errorMsg; 
                                        
                                        $updateArr = array();
                                        $updateArr["Error"] = 1;
                                        $updateArr["ErrorOn"] = gmdate('Y-m-d H:i:s');
                                        $updateArr["Proceed"] = 1;
                                        $updateArr["ProceedOn"] = gmdate('Y-m-d H:i:s');
                                        $db->update_query($updateArr, "pmi_packet_queue", array("ID"=>$qID));
                                        
                    }
                                    if($tStat == 1){
                                        $TGStatusArr[$j]["Status"] = 1;
                                        $TGStatusArr[$j]["MSG"] = $cmdStr." execution complete for ".$tName;
                                    }
                                }
                            }
                            
                            $ackCheckerCount++;
                        }
                        
                        $result['status'] = 1;
                        $result['data'] = $TGStatusArr;
                    }
                    
                    die(json_encode($result));
                } catch (Exception $exc) {
                    $result['status'] = 0;
                    $result['error'] = $exc->getMessage();
                }
            }
    }
?>
