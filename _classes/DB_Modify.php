ALTER TABLE `pmi_alert_definition`ALTER `parameter_value` DROP DEFAULT;
ALTER TABLE `pmi_alert_definition`CHANGE COLUMN `parameter_value` `parameter_from_value` FLOAT(10,2) NOT NULL AFTER `parameter_condition`,ADD COLUMN `parameter_to_value` FLOAT(10,2) NOT NULL AFTER `parameter_from_value`;
<!--  12 feb 2016  --->
ALTER TABLE `pmi_device` ADD `daily_log_id` INT NOT NULL , ADD `hourly_log_id` INT NOT NULL ;

CREATE TABLE IF NOT EXISTS `pmi_daily_log` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `deviceID` bigint(20) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `volts_ph1` int(11) DEFAULT NULL,
  `volts_ph2` int(11) DEFAULT NULL,
  `volts_ph3` int(11) DEFAULT NULL,
  `amps1` int(11) DEFAULT NULL,
  `amps2` int(11) DEFAULT NULL,
  `amps3` int(11) DEFAULT NULL,
  `pf1` int(11) DEFAULT NULL,
  `pf2` int(11) DEFAULT NULL,
  `pf3` int(11) DEFAULT NULL,
  `tamb` int(11) DEFAULT NULL,
  `tint` int(11) DEFAULT NULL,
  `vthd` int(11) DEFAULT NULL,
  `athd` int(11) DEFAULT NULL,
  `caps1` int(11) DEFAULT NULL,
  `caps2` int(11) DEFAULT NULL,
  `caps3` int(11) DEFAULT NULL,
  `volts_imb` int(11) DEFAULT NULL,
  `amps_imb` int(11) DEFAULT NULL,
  `apar_pwr` int(11) DEFAULT NULL,
  `real_pwr` int(10) DEFAULT NULL,
  `react_pwr` int(11) DEFAULT NULL,
  `neutral_amps` int(11) DEFAULT NULL,
  `res1` int(11) DEFAULT NULL,
  `res2` int(11) DEFAULT NULL,
  `res3` int(11) DEFAULT NULL,
  `res4` int(11) DEFAULT NULL,
  `cs1` int(11) DEFAULT NULL,
  `cs2` int(11) DEFAULT NULL,
  `logdate` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `pmi_hourly_log` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `deviceID` bigint(20) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `volts_ph1` int(11) DEFAULT NULL,
  `volts_ph2` int(11) DEFAULT NULL,
  `volts_ph3` int(11) DEFAULT NULL,
  `amps1` int(11) DEFAULT NULL,
  `amps2` int(11) DEFAULT NULL,
  `amps3` int(11) DEFAULT NULL,
  `pf1` int(11) DEFAULT NULL,
  `pf2` int(11) DEFAULT NULL,
  `pf3` int(11) DEFAULT NULL,
  `tamb` int(11) DEFAULT NULL,
  `tint` int(11) DEFAULT NULL,
  `vthd` int(11) DEFAULT NULL,
  `athd` int(11) DEFAULT NULL,
  `caps1` int(11) DEFAULT NULL,
  `caps2` int(11) DEFAULT NULL,
  `caps3` int(11) DEFAULT NULL,
  `volts_imb` int(11) DEFAULT NULL,
  `amps_imb` int(11) DEFAULT NULL,
  `apar_pwr` int(11) DEFAULT NULL,
  `real_pwr` int(10) DEFAULT NULL,
  `react_pwr` int(11) DEFAULT NULL,
  `neutral_amps` int(11) DEFAULT NULL,
  `res1` int(11) DEFAULT NULL,
  `res2` int(11) DEFAULT NULL,
  `res3` int(11) DEFAULT NULL,
  `res4` int(11) DEFAULT NULL,
  `cs1` int(11) DEFAULT NULL,
  `cs2` int(11) DEFAULT NULL,
  `logdate` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

<!-- 13 Feb 2016 -->
ALTER TABLE `pmi_device_alerts` CHANGE `Status` `Status` INT NULL;

ALTER TABLE `pmi_device` ADD `status` INT NOT NULL ;

<!-- 15 Feb 2016 ---->
ALTER TABLE `pmi_daily_log` ADD `daily_LogID` INT NOT NULL ;

ALTER TABLE `pmi_hourly_log` ADD `hour_LogID` INT NOT NULL ;