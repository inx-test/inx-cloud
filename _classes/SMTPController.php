<?php
    include_once("config.php");
    
    class SMTPController extends ControllerBase 
    {	
            # Constructor Method 
            function __constructor(){
            }
            
            function getSmtpConfigInfo(){
                $db = new DBC();
                
                $sql = "select * from pmi_smtp_config where status=1 order by id desc";
                $smtpInfo = $db->get_result($sql);
                
                if(sizeof($smtpInfo)>0){
                    $result['status'] = 1;
                    $result['smtpInfo'] = $smtpInfo;
                    die(json_encode($result));
                }
                else{
                    $result['status'] = 0;
                    die(json_encode($result));
                }
            }
            
            function saveSmtpConfig(){
                $db = new DBC();
                $CMF = new CommonFunction();
                
                $hostName = $_REQUEST['server'];
                $serverport = $_REQUEST['port'];
                $fromEmail = $_REQUEST['fromemail'];
                $authpassword = $_REQUEST['password'];
                $notify = $_REQUEST['alertnotify'];
                
                $body = file_get_contents('tpl/emailTemplate.html');
                $tomail = $fromEmail;
                $subject = "SMTP Configuration";

                 /*Email Content */
                $HTMLContent = "Hi,<br/>";
                $HTMLContent .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Successfully configured SMTP for sending emails from this account.<br/><br/> ";
                $HTMLContent .= "<br /><br />Thanks.";

                /*Replace Emaile Template file*/
                $body = str_replace("[[EMAIL_TITLE]]",$subject,$body);
                $body = str_replace("[[EMAIL_CONTENT]]",$HTMLContent,$body);

                /*Send Mail*/
                $sendMail = $CMF->sendEmail($subject,$body,$tomail,$fromEmail,$hostName,$authpassword,$serverport);
                if ($sendMail != 1) {   
                    $result['status'] = 0;
                    $result['message'] = "SMTP Server Connection Failed";
                    die(json_encode($result));
                } 
                else { 
                    $sql = "select id,status from pmi_smtp_config where smtp_server='$hostName' and smtp_port='$serverport' and from_email='$fromEmail' and password='$authpassword'";
                    $smtpInfo = $db->get_result($sql);
                    
                    if(sizeof($smtpInfo)>0){
                        $rec = array();
                        $where = array();
                        $where['id'] = $smtpInfo[0]['id'];
                        $rec["smtp_server"] = $hostName;
                        $rec["smtp_port"]  = $serverport;
                        $rec["from_email"]     = $fromEmail;
                        $rec["password"]     = $authpassword;
                        $rec["alert_notify"]  = $notify;
                        $rec["status"]  = 1;
                        
                        $updateSql = "update pmi_smtp_config set status=0";
                        $updateStatus = $db->query($updateSql);
                        
                        $updateConfig = $db->update_query($rec , 'pmi_smtp_config',$where);
                        
                        /*if($smtpInfo[0]['status'] == 1){
                            
                        }
                        else{
                            $updateSql = "update pmi_smtp_config set status=0";
                            $updateStatus = $db->query($updateSql);
                            $addConfig = $db->insert_query($rec , 'pmi_smtp_config');
                        }*/
                        
                    }
                    else{
                        $sql1 = "SELECT id FROM pmi_smtp_config";
                        $existRec = $db->get_result($sql1);
                        
                        if(sizeof($existRec)>0){
                            $updateSql = "update pmi_smtp_config set status=0";
                            $updateStatus = $db->query($updateSql);
                        }

                        $dbrec = array();
                        $dbrec["smtp_server"] = $hostName;
                        $dbrec["smtp_port"]  = $serverport;
                        $dbrec["from_email"]     = $fromEmail;
                        $dbrec["password"]     = $authpassword;
                        $dbrec["alert_notify"]  = $notify;
                        $dbrec["status"]  = 1;

                        $saveConfig = $db->insert_query($dbrec , 'pmi_smtp_config');
                    }
                    
                    $result['status'] = 1;
                    $result['message'] = "Successfully Connected to SMTP Server";
                    die(json_encode($result));
                }
                
            }
    }
?>