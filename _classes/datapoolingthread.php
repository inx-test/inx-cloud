<?php

class DeviceDataFetch extends Thread    
{
    private $index = 0;
    private $devices = array();
    public $deviceList = array();
    private $dev;
    
    public function __construct() {
    }
    
    public function getData(){
        $db = new \DBC();
        $sql = "select id,serial_no,ip_address,galileo_id,daily_log_id,hourly_log_id from pmi_device where deleted=0";
        $this->devices = $db->get_result($sql);
        print_r($this->devices);
        $db->close();
    }
    
    public function getNewDeviceData(&$ct){
        
        $this->synchronized(
            function($thread,&$ct)
            {
                echo "IN sync function ".$thread->index."\n";
                if($thread->index < count($thread->devices)) {
                    $ct->setDevice($thread->devices[$thread->index++]);
                }
                else {
                    $ct->setDevice(null);
                }
            }, $this,&$ct
        );
    }
    
    public function run()       
    {
    }
    
    public function deviceAlerts()
    {
        echo "Alerts Detected";
    }
}
