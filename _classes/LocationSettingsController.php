<?php

include_once("config.php");
require_once('vendor/autoload.php');
use UDPProcess\UDPQueueClient;
require_once('UDP/PMIPacket.php');
require_once('UDP/UDP.php');

class LocationSettingsController extends ControllerBase {
    # Constructor Method 

    function __constructor() {
        
    }
/***********************************************************************
     * Get all available node are depending on cluster and unassigned list
     * **********************************************************************/

    Public function GetNodeTreeList() {
        try {
            $db = new DBC();
            $type = $_REQUEST['type'];

            //get all cluster,node and tag details;

            $qry = <<<EOF
                        Select 
                            DISTINCT
                            N.ID AS NodeID,
                            N.Name AS NodeName,
                            C.ID AS ClusterID,
                            C.Name As ClusterName,
                            T.ID AS TagID,
                            T.TagNum,
                            T.Name AS TagName,
                            T.TagTypeID,
                            T.Acknowledged,
                            N.Acknowledged AS NodeAck,
                            C.`Order` AS ClusterOrder,
                            CM.`Order` AS NodeOrder
                        FROM pmi_node N 
                            LEFT JOIN pmi_tag T ON (T.NodeID = N.ID)
                            LEFT JOIN pmi_cluster_mapping CM ON (CM.NodeID = N.ID AND CM.Deleted = 0)
                            LEFT JOIN pmi_cluster C ON (C.ID = CM.ClusterID AND C.Deleted = 0) 
                        WHERE N.Deleted=0 AND T.Deleted = 0 AND T.`Status`=1 AND T.StatusOn > NOW() - INTERVAL 10 Minute 
                        ORDER BY C.Name,N.Name ASC
EOF;
            //echo $qry; die;// S.CreatedOn > NOW() - INTERVAL 10 Minute AND 
            $result = $db->get_result($qry);
            //print_r($result);
            // Get All Cluster

            $qry = "SELECT ID,Name,`Order` AS ClusterOrder FROM pmi_cluster WHERE Deleted = 0";

            $clusterResult = $db->get_result($qry);

            //print_r($result);
            if (sizeof($result) > 0) {
                $treeList = array();
                $preClusterID = 0;
                $preNodeID = 0;
                $preTagID = 0;
                //$rowCount = -1;
                $nodeCount = -1;
                $tagCount = -1;

                foreach ($result as $item) {
                    $time = uniqid();
                    $curClusterId = is_null($item["ClusterID"]) ? -1 : $item["ClusterID"];

                    $clusterName = "Unassinged";
                    $clusterID = 0;
                    if (!is_null($item["ClusterID"])) {
                        $clusterName = $item["ClusterName"];
                        $clusterID = $item["ClusterID"];
                    }
                    //echo "clusterID == $clusterID \n";
                    //Cluster 
                    if ($preClusterID != $curClusterId) {
                        $nodeCount = -1;
                        $preNodeID = 0;
                        $preTagID = 0;
                        $childCount = 0;
                        $rowCount = is_null($item["ClusterOrder"]) ? 0 : $item["ClusterOrder"];
                        //die("curClusterId === ".$curClusterId."   preClusterID   == ".$preClusterID);
                        if ($curClusterId == -1 || ($preClusterID == 0 && $curClusterId != -1)) {
                            //$sql = "SELECT COUNT(DISTINCT N.ID) FROM pmi_node N LEFT JOIN pmi_tag T ON (T.NodeID = N.ID) WHERE N.ID NOT IN (SELECT DISTINCT CM.NodeID FROM pmi_cluster_mapping CM WHERE CM.Deleted = 0) AND T.Acknowledged=1 AND N.Acknowledged=1";
							$sql =<<<EOF
								SELECT 
									COUNT(DISTINCT N.ID)
								FROM pmi_node N
								LEFT JOIN pmi_tag T ON (T.NodeID = N.ID)
                                    WHERE N.ID NOT IN ( SELECT DISTINCT CM.NodeID FROM pmi_cluster_mapping CM WHERE CM.Deleted = 0) 
                                    AND T.Acknowledged=1 AND N.Acknowledged=1 AND T.`Status`=1 AND T.StatusOn > NOW() - INTERVAL 10 Minute 
EOF;
							
                            $childCount = $db->get_single_result($sql);

                            $cTitlte = "<span class='t-content'><span class='t-title'>Unassinged</span><span class='t-count'>(<span class='t-count-num'>" . $childCount . "</span>)</span></span>";
                        }
                        if ($preClusterID == 0 && $curClusterId != -1) {
                            $treeList[0]["key"] = "C_0";
                            $treeList[0]["title"] = $cTitlte;
                            $treeList[0]["tname"] = "Unassinged";
                            $treeList[0]["id"] = 0;
                            $treeList[0]["isFolder"] = TRUE;
                            $treeList[0]["nodeType"] = 1;
                            $treeList[0]["clusterID"] = 0;
                        }
                        if ($curClusterId != -1) {
							
							//$sql = "SELECT COUNT(DISTINCT N.ID) FROM pmi_cluster_mapping CM LEFT JOIN pmi_node N ON (CM.NodeID = N.ID) LEFT JOIN pmi_tag T ON (N.ID = T.NodeID) WHERE CM.ClusterID = $clusterID AND  CM.Deleted = 0 AND T.Acknowledged=1 AND N.Acknowledged=1";
                            //$sql = "SELECT COUNT(DISTINCT N.ID) FROM pmi_cluster_mapping CM LEFT JOIN pmi_tag T ON (CM.NodeID = T.NodeID) WHERE CM.ClusterID = $clusterID AND CM.Deleted = 0 AND T.Acknowledged=1";
							
							$sql =<<<EOF
								SELECT 
									COUNT(DISTINCT N.ID)
								FROM 
									pmi_cluster_mapping CM
								LEFT 
									JOIN pmi_node N ON (CM.NodeID = N.ID)
								LEFT 
									JOIN pmi_tag T ON (N.ID = T.NodeID)
								WHERE CM.ClusterID = $clusterID AND CM.Deleted = 0 AND T.Acknowledged=1 AND N.Acknowledged=1 AND T.`Status`=1 AND
                                                                    T.StatusOn > NOW() - INTERVAL 10 Minute 
EOF;

							//echo $sql."\n";
                            $childCount = $db->get_single_result($sql);
							//echo $childCount."\n";
                            $cTitlte = self::genTitleContent($clusterName, "C_" . $clusterID . "_" . $time, 1, 1, $childCount, $type);
                        }
						//echo $cTitlte;

                        $treeList[$rowCount]["key"] = "C_" . $clusterID;
                        $treeList[$rowCount]["title"] = $cTitlte;
                        $treeList[$rowCount]["tname"] = $clusterName;
                        $treeList[$rowCount]["id"] = $clusterID;
                        $treeList[$rowCount]["isFolder"] = TRUE;
                        $treeList[$rowCount]["nodeType"] = 1;
                        $treeList[$rowCount]["clusterID"] = $clusterID;
                        $preClusterID = $curClusterId;


                        foreach ($clusterResult as $key => $cItem) {
                            if ($cItem["ID"] == $clusterID) {
                                unset($clusterResult[$key]);
                            }
                        }
                    }

                    //Node
                    if ($preNodeID != $item["NodeID"] && $item["NodeAck"] == 1) {
                        $tagCount = -1;
                        $nodeCount++;
                        //echo "N == ".$nodeCount . " NID == ".$item["NodeID"]." \n";
                        $nodeChildCount = 0;
                        $sql = "SELECT COUNT(DISTINCT ID) FROM pmi_tag WHERE NodeID = " . $item["NodeID"] . " AND Deleted = 0 AND TagTypeID>1 AND Acknowledged=1 AND `Status`=1 AND StatusOn > NOW() - INTERVAL 10 Minute ";
                        //echo $sql;
                        $nodeChildCount = $db->get_single_result($sql);
                        //echo $nodeChildCount;

                        $nTitlte = self::genTitleContent($item["NodeName"], "N_" . $item["NodeID"] . "_" . $time, 1, $clusterID == 0 ? 0 : 1, $nodeChildCount, $type);


                        $treeList[$rowCount]["children"][$nodeCount]["key"] = "N_" . $item["NodeID"] . "_" . $time;
                        $treeList[$rowCount]["children"][$nodeCount]["title"] = $nTitlte;
                        $treeList[$rowCount]["children"][$nodeCount]["tname"] = $item["NodeName"];
                        $treeList[$rowCount]["children"][$nodeCount]["id"] = $item["NodeID"];
                        $treeList[$rowCount]["children"][$nodeCount]["isFolder"] = FALSE;
                        $treeList[$rowCount]["children"][$nodeCount]["nodeType"] = 2;
                        $treeList[$rowCount]["children"][$nodeCount]["clusterID"] = $clusterID;
                        $preNodeID = $item["NodeID"];
                    }

                    //Tag
                    if ($preTagID != $item["TagID"] && $item["TagTypeID"] != FIXTURE_TYPE_NODE && $item["Acknowledged"] == 1) {

                        $tagCount++;
                        //echo "T == ".$tagCount . " TID == ".$item["TagTypeID"]." \n";
						
                        $tTitlte = self::genTitleContent($item["TagName"], "T_" . $item["TagID"] . "_" . $time, 1, 0, -1, $type);

                        $treeList[$rowCount]["children"][$nodeCount]["children"][$tagCount]["key"] = "T_" . $item["TagID"] . "_" . $time;
                        $treeList[$rowCount]["children"][$nodeCount]["children"][$tagCount]["title"] = $tTitlte;
                        $treeList[$rowCount]["children"][$nodeCount]["children"][$tagCount]["tname"] = $item["TagName"];
                        $treeList[$rowCount]["children"][$nodeCount]["children"][$tagCount]["id"] = $item["TagID"];
                        $treeList[$rowCount]["children"][$nodeCount]["children"][$tagCount]["isFolder"] = FALSE;
                        $treeList[$rowCount]["children"][$nodeCount]["children"][$tagCount]["nodeType"] = 3;
                        $preTagID = $item["TagID"];
                    }
                }
            }
            if (sizeof($clusterResult) > 0) {


                foreach ($clusterResult as $cItem) {
                    $clusterID = $cItem['ID'];
                    $sql = <<<EOF
                            SELECT COUNT(DISTINCT CM.NodeID)
                            FROM pmi_cluster_mapping CM 
                            LEFT JOIN pmi_tag T ON (CM.NodeID = T.NodeID)
                            WHERE ClusterID = $clusterID AND CM.Deleted = 0 AND T.StatusOn > NOW() - INTERVAL 10 Minute 
                            AND T.Deleted=0 AND T.Status = 1 AND T.Acknowledged=1
EOF;
                    //echo $sql;
                            // WHERE ClusterID = $clusterID AND Deleted = 0  AND S.CreatedOn > NOW() - INTERVAL 10 Minute AND S.NodeStatus = 1
                           // "SELECT COUNT(DISTINCT NodeID) FROM pmi_cluster_mapping WHERE ClusterID = " . $cItem["ID"] . " AND Deleted = 0";
                    $childCount = $db->get_single_result($sql);
					$time1 = uniqid();	// ADDED AK 
					$cTitlte = self::genTitleContent($cItem["Name"], "C_" . $cItem["ID"] . "_" . $time1, 1, 1, $childCount, $type);
                    $treeList[$cItem["ClusterOrder"]]["key"] = "C_" . $cItem["ID"];
                    $treeList[$cItem["ClusterOrder"]]["title"] = $cTitlte;
                    $treeList[$cItem["ClusterOrder"]]["tname"] = $cItem["Name"];
                    $treeList[$cItem["ClusterOrder"]]["id"] = $cItem["ID"];
                    $treeList[$cItem["ClusterOrder"]]["isFolder"] = TRUE;
                    $treeList[$cItem["ClusterOrder"]]["nodeType"] = 1;
                    $treeList[$cItem["ClusterOrder"]]["clusterID"] = $cItem["ID"];
                }
            }

            //print_r($treeList);
            //die;
            if (sizeof($treeList) > 0) {

                //$jsonData = json_encode($treeList, TRUE);
                if ($type == 1)
                    unset($treeList[0]);

                ksort($treeList);
                //print_r($treeList);

                $rtnResult["status"] = 0;
                $rtnResult["result"] = array_values($treeList);
                //print_r($rtnResult);
            } else
                $rtnResult["Status"] = 1;

            die(json_encode($rtnResult, TRUE));
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }

    /*     * ************************************
     * Generate title of cluster tree view
     * ************************************ */

    private function genTitleContent($nmae, $id, $edit = 0, $delete = 0, $childCount = -1, $type = 0) {
        $titleHtml = "<span class='t-content'><span class='t-title'>" . $nmae . "</span>";
        if ($childCount != -1)
            $titleHtml .= "<span class='t-count'>(<span class='t-count-num'>" . $childCount . "</span>)</span>";
        $titleHtml .= "</span> ";
        $titleHtml .= "<span class='tv-btn-control hide'>";
        if ($edit == 1 && $type != 1)
            $titleHtml .= "<i class='fa fa-pencil p-l-10 tv-edit' aria-hidden='true' id='menu_edit_" . $id . "'></i> ";
        if ($delete == 1)
            $titleHtml .= "<i class='fa fa-trash-o p-l-10 tv-delete' id='menu_del_" . $id . "' aria-hidden='true'></i>";
        $titleHtml .= "</span>";
        return $titleHtml;
    }

    /*     * *****************************************
     * Save Tree node from DB
     * **************************************** */

    public function saveTreeNodeName() {
        $db = new DBC();
        $db->autocommit(false);
        try {

            $id = isset($_REQUEST['id']) ? $_REQUEST['id'] : 0;
            $nodeName = $_REQUEST['nodeName'];
            $type = $_REQUEST['type'];
            $where["ID"] = $id;
            $dbrec["Name"] = $nodeName;
            $status = 0;
            if ($type == 1 && $id == 0) {//cluster insert
				//-------- delete not used clusters ------insert available cluster 
				$qry1 = "DELETE FROM pmi_cluster WHERE Deleted = 1";
				$result = $db->query($qry1);
				if ($result == 1){
					for ($i=1; $i < 255; $i++){
						$qry2 = "SELECT COUNT(*) FROM pmi_cluster WHERE ID =$i";
						$id_cluster = $db->get_single_result($qry2);
						if ($id_cluster == 0){
							$dbrec["ID"] = $i;
							//echo "ID wil be inserted ".$i;
							break;
						}
					}
				}	
				if ($i < 255 ) {
					$sql = "SELECT MAX(`Order`) AS C_Order FROM pmi_cluster";
					$maxOrderCount = $db->get_single_result($sql);

					$dbrec["`Order`"] = intval($maxOrderCount) + 1;
					$dbrec["CreatedBy"] = $_SESSION['LOGGED_USER_ID'];
					$status = $db->insert_query($dbrec, 'pmi_cluster');
					if ($status == 1){
						$clusterID = $db->get_insert_id();
						$respoData["clusterID"] = $clusterID;
						$respoData["htmlContent"] = self::genTitleContent($nodeName, $clusterID, 1, 1, 0);
					}
				}else $status =  2;

            } else if ($type == 1) {// cluster update
                $dbrec["ModifiedBy"] = $_SESSION['LOGGED_USER_ID'];
                $dbrec["ModifiedOn"] = date("Y-m-d H:m:s");
                $status = $db->update_query($dbrec, 'pmi_cluster', $where);
            } else if ($type == 2) {// node update
                $status = $db->update_query($dbrec, 'pmi_node', $where);
                $where = array();
                $where["NodeID"] = $id;
                $where["TagTypeID"] = FIXTURE_TYPE_NODE;
                $where["Deleted"] = 0;
                
                $status = $db->update_query($dbrec, 'pmi_tag',$where);
                
            } else if ($type == 3) {// tag update
                $status = $db->update_query($dbrec, 'pmi_tag', $where);
            }
            $respoData["status"] = $status;
            $db->commit();

            die(json_encode($respoData));
        } catch (Exception $exc) {
            $db->rollback();
            die($exc->getMessage());
        }
        $db->close();
    }

    /*     * *****************************************
     * Delete Tree node from DB
     * **************************************** */

    public function deleteTreeNode() {

        $db = new DBC();
        $db->autocommit(false);
        try {

            $id = isset($_REQUEST['id']) ? $_REQUEST['id'] : 0;
            $type = isset($_REQUEST['type']) ? $_REQUEST['type'] : 0;
            $clusterId = isset($_REQUEST['clusterId']) ? $_REQUEST['clusterId'] : 0;
            $child = isset($_REQUEST['child']) ? $_REQUEST['child'] : 0;

            $resResult["unassignedNode"] = array();
            $status = 0;

            if ($type == 1) {

                // update cluster delete flag  from db
                $where["ID"] = $id;
                $dbrec["Deleted"] = 1;
                $dbrec["DeletedBy"] = $_SESSION['LOGGED_USER_ID'];
                $dbrec["DeletedON"] = date("Y-m-d H:m:s");
                $status = $db->update_query($dbrec, 'pmi_cluster', $where);

                if ($status == 1 && $child == 1) {
                    //get unassigned nodes in particular cluster
                    $sql = <<<EOF
                        SELECT 
                            N.ID AS NodeID,
                            N.Name AS NodeName,
                            0 AS ClusterID,
                            C.Name AS ClusterName,
                            T.ID AS TagID,
                            T.TagNum,
                            T.Name AS TagName,
                            T.Acknowledged,
                            N.Acknowledged AS NodeAck
                        FROM pmi_node N
                            LEFT JOIN pmi_tag T ON (T.NodeID = N.ID)
                            LEFT JOIN pmi_cluster_mapping CM ON (CM.NodeID = N.ID)
                            LEFT JOIN pmi_cluster C ON (C.ID = CM.ClusterID AND C.Deleted = 0)
                        WHERE N.Deleted=0 AND T.`Status`= 1 AND T.Deleted = 0 AND 
                            CM.ClusterID = $id AND CM.Deleted = 0 
                            AND CM.NodeID NOT IN (SELECT NodeID FROM pmi_cluster_mapping WHERE ClusterID != $id AND Deleted = 0)
                        ORDER BY C.`Order`,CM.`Order`,N.ID ASC
EOF;
                    $getNodeRec = $db->get_result_array($sql);

                    //update cluster nodes delete flag from db 
                    $CMwhere["ClusterID"] = $id;
                    $CMdbrec["Deleted"] = 1;
                    $status = $db->update_query($CMdbrec, 'pmi_cluster_mapping', $CMwhere);

                    //generate unassigned tree node array
                    if (sizeof($getNodeRec) > 0)
                        $resResult["unassignedNode"] = self::getClusterChildNode($getNodeRec, $db);
                    $status = 1;
                }
                
                
                //Remove policy
                $sql = "SELECT PolicyID FROM pmi_policy_mapping WHERE ClusterID=$id";
                $existsCluster = $db->get_result_array($sql);
				//echo"AK Tag ";
				//print_r($existsCluster);
                if(sizeof($existsCluster) > 0 ){
                    
                    $dbrec = array();
                    $dbrec["Type"] = 1; // cluster
                    $dbrec["TypeID"] = $id;
                    $dbrec["CreatedBy"] = $_SESSION['LOGGED_USER_ID'];
                    $dbrec["CreatedOn"] = date("Y-m-d H:m:s");
                    $status = $db->insert_query($dbrec, "pmi_merge_request");
                    $packeReqID = $db->get_insert_id();
                    
                    $dbres = array();
                    $where =  array();
                    $where["ClusterID"] = $id;
                    $dbrec["Deleted"] = 1;
                    $dbrec["DeletedBy"] = $_SESSION['LOGGED_USER_ID'];
                    $dbrec["DeletedOn"] = date("Y-m-d H:m:s");
                    $status = $db->update_query($dbrec, "pmi_policy_mapping", $where);
                    if($status){
                        for ($i = 0; $i < sizeof($existsCluster); $i++) {
                            $data = array(
                                "ID" => $id,
                                "Type" => 1, //Cluster;2 Node;
                                "Action" => 2,//Remove Policy
                                "PolicyID" => $existsCluster[$i]["PolicyID"],
                                "requestedID" => $packeReqID
                            );
                            $notQue = new UDPQueueClient(NOTIF_QUEUE_SERVER,SYSTEMLOG_QUEUE_NAME);
                            $notQue->queueNotificationRequest(POLICY_PROCESS, $data);
						}
                        $notQue = new UDPQueueClient(NOTIF_QUEUE_SERVER,SYSTEMLOG_QUEUE_NAME);
                        $notQue->queueNotificationRequest(POLICY_MERGE_CHECK_PROCESS, array("requestedID"=>$packeReqID),POLICY_CHECK_DELAY);
                    }
                }
            }
            if ($type == 2) {

                $existsNode = "SELECT NodeID FROM pmi_cluster_mapping WHERE ClusterID != $clusterId AND NodeID= $id AND Deleted = 0";
                $getExistsNodeRec = $db->get_result_array($existsNode);
                if (sizeof($getExistsNodeRec) == 0) {
                    //generate unassigned tree node array
                    $sql = <<<EOF
                            SELECT 
                                N.ID AS NodeID,
                                N.Name AS NodeName,
                                0 AS ClusterID,
                                C.Name AS ClusterName,
                                T.ID AS TagID,
                                T.TagNum,
                                T.Name AS TagName,
                                T.TagTypeID,
                                T.Acknowledged,
                                N.Acknowledged AS NodeAck
                            FROM pmi_node N
                                LEFT JOIN pmi_tag T ON (T.NodeID = N.ID)
                                LEFT JOIN pmi_cluster_mapping CM ON (CM.NodeID = N.ID)
                                LEFT JOIN pmi_cluster C ON (C.ID = CM.ClusterID AND C.Deleted = 0)
                            WHERE N.Deleted=0 AND T.`Status`= 1 AND T.Deleted = 0 AND 
                                CM.ClusterID = $clusterId AND CM.Deleted = 0 
                                AND CM.NodeID = $id 
                            ORDER BY C.`Order`,CM.`Order`,N.ID ASC
EOF;

                    //die($sql);
                    $getNodeRec = $db->get_result_array($sql);
                    if (sizeof($getNodeRec) > 0)
                        $resResult["unassignedNode"] = self::getClusterChildNode($getNodeRec, $db);
                    }

                //Remove policy
                $sql = "SELECT PolicyID FROM pmi_policy_mapping WHERE ClusterID=$clusterId AND Deleted=0";
                $existsCluster = $db->get_result_array($sql);

                //print_r($getExistsNodeRec);
                
                $sql = "SELECT ID FROM pmi_tag WHERE NodeID=$id AND Deleted=0 AND Acknowledged=1";
                $existsTag = $db->get_result_array($sql);
                
                //print_r($existsTag);
                $newPolicyIDs = "";
                $dbrec = array();
                $dbrec["Type"] = 2; // Node
                $dbrec["TypeID"] = $id;
                $dbrec["CreatedBy"] = $_SESSION['LOGGED_USER_ID'];
                $dbrec["CreatedOn"] = date("Y-m-d H:m:s");
                $status = $db->insert_query($dbrec, "pmi_merge_request");
                $packeReqID = $db->get_insert_id();

                if(sizeof($existsCluster) > 0 && sizeof($existsTag) > 0){
                    for ($i = 0; $i < sizeof($existsCluster); $i++) {
                        $data = array(
                            "ID" => $existsTag[0]["ID"],
                            "Type" => 2, //Cluster;2 Node;
                            "Action" => 2,//Remove Policy
                            "PolicyID" => $existsCluster[$i]["PolicyID"],
                            "requestedID" => $packeReqID
                        );
                        $notQue = new UDPQueueClient(NOTIF_QUEUE_SERVER,SYSTEMLOG_QUEUE_NAME);
                        $notQue->queueNotificationRequest(POLICY_PROCESS, $data);
                    }
                    
                    $notQue = new UDPQueueClient(NOTIF_QUEUE_SERVER,SYSTEMLOG_QUEUE_NAME);
                    $notQue->queueNotificationRequest(POLICY_MERGE_CHECK_PROCESS, array("requestedID"=>$packeReqID),POLICY_CHECK_DELAY);
                }
                
                //update delete flag nodes from DB
                unset($where);
                unset($dbrec);
                $where["ClusterID"] = $clusterId;
                $where["NodeID"] = $id;
                $dbrec["Deleted"] = 1;
                $dbrec["DeletedBy"] = $_SESSION['LOGGED_USER_ID'];
                $dbrec["DeletedOn"] = date("Y-m-d H:m:s");
                $status = $db->update_query($dbrec, 'pmi_cluster_mapping', $where);
                $status = 1;

				//=========================send cluster update to node ===========================
				$sql = "SELECT * FROM pmi_node N LEFT JOIN pmi_tag T ON (T.NodeID = N.ID) WHERE N.Deleted=0 AND N.Acknowledged=1 AND T.TagTypeID=" . FIXTURE_TYPE_NODE . " AND T.Status=1 AND T.Acknowledged=1 AND N.ID=$id ";
				$result = $db->get_result($sql);
				//print_r($result);
				
				$clusterID = "SELECT ClusterID FROM pmi_cluster_mapping WHERE NodeID=$id AND Deleted=0";
                $existsClusterID = $db->get_result($clusterID);
				
				$tagID = $result[0]["ID"];
				$broadcastIp =$result[0]["IPAddress"];
				
				//UDP Helper Class
				$udp = new UDP();
				$statusPacket = new PMIPacket();

				$statusPacket->preamble = 255; // Optional
				$statusPacket->startByte = 85; // Optional
				$statusPacket->packetId = rand(51, 255); // Get Incremental Packet ID - Mandatory
				$statusPacket->tagIdHighByte = $udp->getHiByte($tagID);
				$statusPacket->tagIdLowByte =  $udp->getLoByte($tagID);
				$clusters = array (255,0,0,0,0,0,0,0,0,0);
				$statusPacket->cluster = $clusters;

				$statusPacketBytes=array();
				for ($j=0;$j < 12 ; $j++)  $statusPacketBytes[$j] = 0;
				
				$statusPacketBytes[0]= 23;
				$statusPacketBytes[1]=162;
				
				for ($i = 0; $i < 13; $i++) {
					$statusPacketBytes[$i+2] = $existsClusterID[$i]["0"];
				}
				//$statusPacketBytes = array(23,162,$nodeClusterID,$sourceNodeClusterID);

				$statusPacket->numberOfBytes = sizeof($statusPacketBytes);

				// Set Bytes
				$statusPacket->commandParam = $statusPacketBytes;
				
				//echo $statusPacket->getDecimal();
				// Send
				//$status = $udp->send($broadcastIp, INX_LISTENER_PORT, $statusPacket);
				$broadcastIp = shell_exec("ifconfig | awk '/broadcast / {print $6}'");
						      
				$status = $udp->broadcast($broadcastIp, $statusPacket, NODE_LISTENER_PORT);	
				

            }

            $db->commit();
            $resResult["status"] = $status;

            //return ajax respons data
            die(json_encode($resResult));
        } catch (Exception $exc) {
            $db->rollback();
            die($exc->getMessage());
        }
        $db->close();
    }

    /*     * *********************************
     * Get particular cluster node
     * ******************************** */

    public function getClusterChildNode($NodeRec, $db, $editStatus = 1, $deleteStatus = 0) {
        try {
            //print_r($NodeRec);die;
            if (sizeof($NodeRec) > 0) {
                $treeList = array();
                $preNodeID = 0;
                $preTagID = 0;
                $nodeCount = -1;
                $tagCount = -1;
                foreach ($NodeRec as $item) {
                    $curClusterId = is_null($item["ClusterID"]) ? -1 : $item["ClusterID"];
                    $time = uniqid();
                    $clusterName = "Unassinged";
                    $clusterID = is_null($item["ClusterID"]) ? 0 : $item["ClusterID"];
                    $nodeChildCount = 0;
                    $sql = "SELECT COUNT(DISTINCT ID) FROM pmi_tag WHERE NodeID = " . $item["NodeID"] . " AND Deleted = 0 AND TagTypeID>1 AND Acknowledged=1";
                    
                    $nodeChildCount = $db->get_single_result($sql);
                    //Node
                    if ($preNodeID != $item["NodeID"] && $item["NodeAck"] == 1) {
                        $tagCount = -1;
                        $nodeCount++;
                        $nTitlte = self::genTitleContent($item["NodeName"], "N_" . $item["NodeID"] . "_" . $time, $editStatus, $deleteStatus, $nodeChildCount);
                        $treeList[$nodeCount]["key"] = "N_" . $item["NodeID"];
                        $treeList[$nodeCount]["title"] = $nTitlte;
                        $treeList[$nodeCount]["tname"] = $item["NodeName"];
                        $treeList[$nodeCount]["id"] = $item["NodeID"];
                        $treeList[$nodeCount]["isFolder"] = FALSE;
                        $treeList[$nodeCount]["nodeType"] = 2;
                        $treeList[$nodeCount]["clusterID"] = $clusterID;
                        $preNodeID = $item["NodeID"];
                    }

                    //Tag
                    if ($preTagID != $item["TagID"] && $item["TagTypeID"] != FIXTURE_TYPE_NODE && $item["Acknowledged"] == 1) {

                        $tagCount++;

                        $tTitlte = self::genTitleContent($item["TagName"], "T_" . $item["TagID"] . "_" . $time, $editStatus, 0);

                        $treeList[$nodeCount]["children"][$tagCount]["key"] = "T_" . $item["TagID"];
                        $treeList[$nodeCount]["children"][$tagCount]["title"] = $tTitlte;
                        $treeList[$nodeCount]["children"][$tagCount]["tname"] = $item["TagName"];
                        $treeList[$nodeCount]["children"][$tagCount]["id"] = $item["TagID"];
                        $treeList[$nodeCount]["children"][$tagCount]["isFolder"] = FALSE;
                        $treeList[$nodeCount]["children"][$tagCount]["nodeType"] = 3;
                        $preTagID = $item["TagID"];
                    }
                }
            }
            //print_r($treeList);
            return $treeList;
        } catch (Exception $exc) {
            throw $exc->getMessage();
        }
    }

    /**********************************
	* Update tree view status        *
     * ******************************* */

    public function treeViewChangeOrder() {

        $db = new DBC();
        $db->autocommit(false);
        try {
			//echo"hete ";
            //print_r($_REQUEST);
            $tree = json_decode($_REQUEST['tree'],true);
            $nodeID = $_REQUEST['nodeID'];
            $nodeType = $_REQUEST['nodeType'];
            $nodeClusterID = $_REQUEST['nodeClusterID'];
            $sourceNodeID = $_REQUEST['sourceNodeID'];
            $sourceNodeType = $_REQUEST['sourceNodeType'];
            $sourceNodeClusterID = $_REQUEST['sourceNodeClusterID'];
            $status = 0;
			//print_r($tree);
//echo ">>>>>>>>>> $sourceNodeType"; 


            $nodeCount = sizeof($tree);
            if ($sourceNodeType == 1) {
                $order = 1;
                for ($i = 0; $i < $nodeCount; $i++) {
                    if ($tree[$i]["clusterID"] != 0) {

                        $dbrec["`Order`"] = $order;
                        $where["ID"] = $tree[$i]["id"];
                        $status = $db->update_query($dbrec, 'pmi_cluster', $where);
                        $order++;
                    }
                }
                $status = 1;
            }
            //Update node position from curretn cluster to anthor cluster
            else if ($sourceNodeType == 2) {
				//echo "Iside ";
                //remove node from current cluster list
                $removeStatus = 1;
                $inserStatus = 0;
                $sql = <<<EOF
                        SELECT COUNT(NodeID) FROM pmi_cluster_mapping WHERE ClusterID=$nodeClusterID AND NodeID=$sourceNodeID AND Deleted = 0
EOF;
                //echo $sql;
                $existsnode = $db->get_single_result($sql);
				//var_dump($existsnode);
                //echo "EXR: ".$existsnode;
               // die;
                if ($existsnode == 0 && ($nodeClusterID != $sourceNodeClusterID)) {
                    if ($sourceNodeClusterID != 0) {
                        $dbrec["Deleted"] = 1;
                        $dbrec["DeletedBy"] = $_SESSION['LOGGED_USER_ID'];
                        $dbrec["DeletedOn"] = date("Y-m-d H:m:s");

                        $where["ClusterID"] = $sourceNodeClusterID;
                        $where["NodeID"] = $sourceNodeID;
                        $removeStatus = $db->update_query($dbrec, 'pmi_cluster_mapping', $where);
                        
                        if($removeStatus){
                            
                            $sql = "SELECT PolicyID FROM pmi_policy_mapping WHERE ClusterID=$nodeClusterID AND Deleted=0";
                            $existPolicy = $db->get_result_array($sql);
                            if(sizeof($existPolicy) > 0){
                                
                                $newPolicyIDs = "";
                                $dbrec = array();
                                $dbrec["Type"] = 2; // Node
                                $dbrec["TypeID"] = $sourceNodeClusterID;
                                $dbrec["CreatedBy"] = $_SESSION['LOGGED_USER_ID'];
                                $dbrec["CreatedOn"] = date("Y-m-d H:m:s");
                                $status = $db->insert_query($dbrec, "pmi_merge_request");
                                $packeReqID = $db->get_insert_id();
                                
                                for ($k = 0; $k < count($existPolicy); $k++) {

                                    $data = array(
                                        "ID" => $sourceNodeClusterID,
                                        "Type" => 2, //Cluster;2 Node;
                                        "Action" => 2,//Remove Policy
                                        "PolicyID" => $existPolicy[$k]["PolicyID"],
                                        "requestedID" => $packeReqID
                                    );
                                    $notQue = new UDPQueueClient(NOTIF_QUEUE_SERVER,SYSTEMLOG_QUEUE_NAME);
                                    $notQue->queueNotificationRequest(POLICY_PROCESS, $data);
										}

                                $notQue = new UDPQueueClient(NOTIF_QUEUE_SERVER,SYSTEMLOG_QUEUE_NAME);
                                $notQue->queueNotificationRequest(POLICY_MERGE_CHECK_PROCESS, array("requestedID"=>$packeReqID),POLICY_CHECK_DELAY);
                            }
                        }
                        
                    }

                    //Insert node under new cluster 
                    if ($removeStatus == 1 && $nodeClusterID != 0) {
                        unset($dbrec);
                        unset($where);
                        $nodeOrderSql = "SELECT MAX(`Order`) AS NodeOrder FROM pmi_cluster_mapping WHERE ClusterID=" . $nodeClusterID . " AND Deleted=0";
                        $nodeOrder = $db->get_single_result($nodeOrderSql);
                        $dbrec["`Order`"] = intval($nodeOrder) + 1;
                        $dbrec["ClusterID"] = $nodeClusterID;
                        $dbrec["NodeID"] = $sourceNodeID;
                        $dbrec["CreatedBy"] = $_SESSION['LOGGED_USER_ID'];
                        $table = "pmi_cluster_mapping";
                        $inserStatus = $db->insert_query($dbrec, $table);
                        
                        if($inserStatus){
                            $sql = "SELECT PolicyID FROM pmi_policy_mapping WHERE ClusterID=$nodeClusterID AND Deleted=0";
                            $existPolicy = $db->get_result_array($sql);
                            if(sizeof($existPolicy) > 0){
                                
                                $dbrec = array();
                                $dbrec["Type"] = 2; // Node
                                $dbrec["TypeID"] = $sourceNodeID;
                                $dbrec["CreatedBy"] = $_SESSION['LOGGED_USER_ID'];
                                $dbrec["CreatedOn"] = date("Y-m-d H:m:s");
                                $status = $db->insert_query($dbrec, "pmi_merge_request");
                                $packeReqID = $db->get_insert_id();
                                
                                for ($k = 0; $k < count($existPolicy); $k++) {
                                    $data = array(
                                        "ID" => $sourceNodeID,
                                        "Type" => 2, //Cluster;2 Node;
                                        "Action" => 1,//Add Policy
                                        "PolicyID" => $existPolicy[$k]["PolicyID"],
                                        "requestedID" => $packeReqID
                                    );
                                    $notQue = new UDPQueueClient(NOTIF_QUEUE_SERVER,SYSTEMLOG_QUEUE_NAME);
                                    $notQue->queueNotificationRequest(POLICY_PROCESS, $data);
                                }
                                
                                $notQue = new UDPQueueClient(NOTIF_QUEUE_SERVER,SYSTEMLOG_QUEUE_NAME);
                                $notQue->queueNotificationRequest(POLICY_MERGE_CHECK_PROCESS, array("requestedID"=>$packeReqID),POLICY_CHECK_DELAY);
                            }
                        }
                        
                    }
                } else {
                    $db->commit();

                    $result["status"] = 0;
                    die(json_encode($result));
                }

                //Update Cluster node order
                for ($i = 0; $i < $nodeCount; $i++) {
                    if ($tree[$i]["clusterID"] == $nodeClusterID) {
                        $child = $tree[$i]["children"];
                        //print_r($child);
                        for ($j = 0; $j < count($child); $j++) {
                            $order = $j + 1;

                            $status = self::updateTreeViewOrder($child[$j]["id"], $order, $child[$j]["clusterID"], $db);
                        }
                        break;
                    }
                }

                $status = 1;
				
				//=========================send cluster update to node ===========================
				$sql = "SELECT * FROM pmi_node N LEFT JOIN pmi_tag T ON (T.NodeID = N.ID) WHERE N.Deleted=0 AND N.Acknowledged=1 AND T.TagTypeID=" . FIXTURE_TYPE_NODE . " AND T.Status=1 AND T.Acknowledged=1 AND N.ID=$sourceNodeID ";
				$result = $db->get_result($sql);
				//print_r($result);
				
				$clusterID = "SELECT ClusterID FROM pmi_cluster_mapping WHERE NodeID=$sourceNodeID AND Deleted=0";
                $existsClusterID = $db->get_result($clusterID);
				
				$tagID = $result[0]["ID"];
				$broadcastIp =$result[0]["IPAddress"];
				
				//UDP Helper Class
				$udp = new UDP();
				$statusPacket = new PMIPacket();

				$statusPacket->preamble = 255; // Optional
				$statusPacket->startByte = 85; // Optional
				$statusPacket->packetId =69;// rand(50, 255); // Get Incremental Packet ID - Mandatory
				$statusPacket->tagIdHighByte = $udp->getHiByte($tagID);
				$statusPacket->tagIdLowByte =  $udp->getLoByte($tagID);
				$clusters = array (255,0,0,0,0,0,0,0,0,0);
				$statusPacket->cluster = $clusters;

				$statusPacketBytes=array();
				for ($j=0;$j < 12 ; $j++)  $statusPacketBytes[$j] = 0;
				
				$statusPacketBytes[0]= 23;
				$statusPacketBytes[1]=162;
				
				for ($i = 0; $i < 13; $i++) {
					$statusPacketBytes[$i+2] = $existsClusterID[$i]["0"];
				}
				//$statusPacketBytes = array(23,162,$nodeClusterID,$sourceNodeClusterID);

				$statusPacket->numberOfBytes = sizeof($statusPacketBytes);

				// Set Bytes
				$statusPacket->commandParam = $statusPacketBytes;
				
				//echo $statusPacket->getDecimal();
				// Send
				$broadcastIp = shell_exec("ifconfig | awk '/broadcast / {print $6}'");
				//$status = $udp->send($broadcastIp, INX_LISTENER_PORT, $statusPacket);			      
				$status = $udp->broadcast($broadcastIp, $statusPacket, NODE_LISTENER_PORT);	
            }
            $db->commit();

            $result["status"] = $status;
            die(json_encode($result));
        } catch (Exception $exc) {
            $db->rollback();
            die($exc->getMessage());
        }
        $db->close();
    }

    public function updateTreeViewOrder($nodeID, $order, $clusterId, $db) {
        unset($dbrec);
        unset($where);
        $dbrec["Order"] = $order;

        $where["ClusterID"] = $nodeClusterID;
        $where["NodeID"] = $nodeID;
        $where["Deleted"] = 0;

        return $db->update_query($dbrec, 'pmi_cluster_mapping', $where);
    }

    /********************************************
     *    CLUSTER
     * ****************************************** */

    public function insertNodeToCluster() {
        $db = new DBC();
        $db->autocommit(false);
        try {
            $clusterID = $_REQUEST['clusterID'];
            $nodeIDs = $_REQUEST['nodeIDs'];
            $nodeArr = explode(",", $nodeIDs);
            $inserStatus = 0;
            $result["status"] = 0;
            $newNodeIds = "";

            $sql = "SELECT NodeID FROM pmi_cluster_mapping WHERE ClusterID=$clusterID AND Deleted=0 AND NodeID IN ($nodeIDs)";
            $existID = $db->get_result_array($sql);

            foreach ($nodeArr as $NID) {
                $existSataus = 0;
                foreach ($existID as $existItem) {
                    if ($existItem["NodeID"] == $NID) {
                        $existSataus = 1;
                        break;
                    }
                }
                //echo 'NID === '.$NID ." \n";
                //echo $existSataus;
                if ($existSataus == 0) {
                    //echo $existSataus;
                    $nodeOrderSql = "SELECT MAX(`Order`) AS NodeOrder FROM pmi_cluster_mapping WHERE ClusterID=" . $clusterID . " AND Deleted=0";
                    $nodeOrder = $db->get_single_result($nodeOrderSql);
					
					if(empty($nodeOrder) || $nodeOrder == "") $nodeOrder =1;
					else $nodeOrder++;
					
                    $dbrec["ClusterID"] = $clusterID;
                    $dbrec["NodeID"] = $NID;
                    $dbrec["`Order`"] = $nodeOrder;
                    $dbrec["CreatedBy"] = $_SESSION['LOGGED_USER_ID'];
                    //print_r($dbrec);
                    $table = "pmi_cluster_mapping";
                    $inserStatus = $db->insert_query($dbrec, $table);
                                        
                    if($inserStatus){
						
                        $newNodeIds .= ($newNodeIds != "" ? "," : "") . $NID;
                        $sql = "SELECT PolicyID FROM pmi_policy_mapping WHERE ClusterID=$clusterID AND Deleted=0";
                        $existPolicy = $db->get_result_array($sql);
                        
                        if(sizeof($existPolicy) > 0){
                            
                            $dbrec = array();
                            $dbrec["Type"] = 2; // Node
                            $dbrec["TypeID"] = $NID;
                            $dbrec["CreatedBy"] = $_SESSION['LOGGED_USER_ID'];
                            $dbrec["CreatedOn"] = date("Y-m-d H:m:s");
                            $status = $db->insert_query($dbrec, "pmi_merge_request");
                            $packeReqID = $db->get_insert_id();
                            
                            for ($k = 0; $k < count($existPolicy); $k++) {

                                $data = array(
                                    "ID" => $NID,
                                    "Type" => 2, //Cluster;2 Node;
                                    "Action" => 1,//Add Policy
                                    "PolicyID" => $existPolicy[$k]["PolicyID"],
                                    "requestedID" => $packeReqID
                                );
                                $notQue = new UDPQueueClient(NOTIF_QUEUE_SERVER,SYSTEMLOG_QUEUE_NAME);
                                $notQue->queueNotificationRequest(POLICY_PROCESS, $data);
                            }
                            
                            $notQue = new UDPQueueClient(NOTIF_QUEUE_SERVER,SYSTEMLOG_QUEUE_NAME);
                            $notQue->queueNotificationRequest(POLICY_MERGE_CHECK_PROCESS, array("requestedID"=>$packeReqID),POLICY_CHECK_DELAY);
                        }
                    }
                }
            }

            if ($newNodeIds != "") {
                //generate unassigned tree node array
                $sql = <<<EOF
                            SELECT 
                                N.ID AS NodeID,
                                N.Name AS NodeName,
                                C.ID AS ClusterID,
                                C.Name AS ClusterName,
                                T.ID AS TagID,
                                T.TagNum,
                                T.Name AS TagName,
                                T.Acknowledged,
                                N.Acknowledged AS NodeAck
                            FROM pmi_node N
                                LEFT JOIN pmi_tag T ON (T.NodeID = N.ID)
                                LEFT JOIN pmi_cluster_mapping CM ON (CM.NodeID = N.ID)
                                LEFT JOIN pmi_cluster C ON (C.ID = CM.ClusterID AND C.Deleted = 0)
                            WHERE N.Deleted=0 AND T.`Status`= 1 AND T.Deleted = 0 AND 
                                CM.ClusterID = $clusterID AND CM.Deleted = 0 
                                AND CM.NodeID IN ($newNodeIds)
                            ORDER BY C.`Order`,CM.`Order`,N.ID ASC
EOF;

                //echo $sql;
                //die($sql);
                $getNodeRec = $db->get_result_array($sql);
                if (sizeof($getNodeRec) > 0) {

                    $result["leftSideNodes"] = self::getClusterChildNode($getNodeRec, $db, 1, 1);
                    $result["rightSideNodes"] = self::getClusterChildNode($getNodeRec, $db, 0, 1);
                }
            }
            if ($inserStatus)
                $result["status"] = 1;

            $db->commit();
            die(json_encode($result));
        } catch (Exception $exc) {
            $db->rollback();
            echo $exc->getMessage();
        }
        $db->close();
    }
    //*****************************************
	//>>>>>>>>>>>>>>>Autotune start <<<<<<<<<<
	//*****************************************/
	public function insertOrUpdateATPolicy() {
        $db = new DBC();
        $db->autocommit(true);
        try {
            $where = array();$dbrec= array();$tableName = "pmi_at_policy";
            $id = 0;
            $result["status"] = 0;
            $result["msg"] = "Faild to save policy.";
	
            if(isset($_REQUEST['id'])){
                $id = $_REQUEST['id'];
                $where["ID"]= $id;
                $dbrec["ModifiedBy"] = $_SESSION['LOGGED_USER_ID'];
                $dbrec["ModiFiedOn"] = date("Y-m-d H:m:s");
            }
            else  $dbrec["Createdby"] = $_SESSION['LOGGED_USER_ID'];
            if(isset($_REQUEST['name'])){
                $dbrec["PName"]=$_REQUEST['name'];
                    $sql ="SELECT ID FROM pmi_at_policy WHERE PName='".$_REQUEST['name']."' AND Deleted=0";
                    if($id != 0 ) $sql .= " AND ID!=".$id;
                    $existRec = $db->get_result_array($sql);
                    if(sizeof($existRec) > 0){
                        $result["status"] = 0;
                        $result["msg"] = "Policy name already exists.";
                        die(json_encode($result));
                    }
            }
            
            if(isset($_REQUEST['dim'])) $dbrec["Color"]=$_REQUEST['dim'];
			if(isset($_REQUEST['Cluster'])) $dbrec["ClusterID"]=$_REQUEST['Cluster'];
            if(isset($_REQUEST['startTime'])) $dbrec["StartTime"]=date("H:i:s",  strtotime ($_REQUEST['startTime']));
            if(isset($_REQUEST['time'])) $dbrec["StartTime"]=date("H:i:s",  strtotime ($_REQUEST['time']));
            if(isset($_REQUEST['endTime'])) $dbrec["EndTime"]=date("H:i:s",  strtotime ($_REQUEST['endTime']));
			if(isset($_REQUEST['day1'])) $dbrec["Day1"]=$_REQUEST['day1'];
            if(isset($_REQUEST['day2'])) $dbrec["Day2"]=$_REQUEST['day2'];
            if(isset($_REQUEST['day3'])) $dbrec["Day3"]=$_REQUEST['day3'];
            if(isset($_REQUEST['day4'])) $dbrec["Day4"]=$_REQUEST['day4'];
            if(isset($_REQUEST['day5'])) $dbrec["Day5"]=$_REQUEST['day5'];
            if(isset($_REQUEST['day6'])) $dbrec["Day6"]=$_REQUEST['day6'];
            if(isset($_REQUEST['day7'])) $dbrec["Day7"]=$_REQUEST['day7'];
			//week of day convert to binary
            $DOW=0;
            $dowSel = array($dbrec["Day7"],$dbrec["Day1"],$dbrec["Day2"],$dbrec["Day3"],$dbrec["Day4"],$dbrec["Day5"],$dbrec["Day6"]); // M, T, W
            $dowSel = array_reverse($dowSel); // Reverse order
            $dowBits = implode('',$dowSel); // Join values without delimiter
            $down = str_pad($dowBits, 8, '0', STR_PAD_LEFT); // 8 Bit pad with 0
            $DOW = bindec($down); // Convert to decimal 
			
            //print_r($dbrec);
            if(sizeof($where) >0 ){
                $status = $db->update_query ($dbrec, $tableName, $where);
                if($status){
						$result["status"] = 1;
						$result["id"] = $_REQUEST['id'];
						$result["msg"] = "Successfully updated Autotune  policy.";
									
						//-------------send POlicy to nodes -----------------// 

                }
            }
            else if(sizeof($dbrec) > 0){
				//-------------deletee unused ID from table 
				$qry1 = "DELETE FROM pmi_at_policy WHERE Deleted = 1";
				$res = $db->query($qry1);
				if ($res == 1){
					for ($i=1; $i < 255; $i++){
						$qry2 = "SELECT COUNT(*) FROM pmi_at_policy WHERE ID =$i";
						$id_cluster = $db->get_single_result($qry2);
						if ($id_cluster == 0){
							$dbrec["ID"] = $i;
							//echo "ID will be inserted ".$i;
							break;
						}
					}
				}	
				if ($i < 255 ) {
					/*$sql = "SELECT MAX(`Order`) AS C_Order FROM pmi_at_policy";
					$maxOrderCount = $db->get_single_result($sql);

					$dbrec["`Order`"] = intval($maxOrderCount) + 1;
					$dbrec["CreatedBy"] = $_SESSION['LOGGED_USER_ID'];*/
					
					$status = $db->insert_query($dbrec, 'pmi_at_policy');
					//print_r($dbrec);
					if($status){
						$result["status"] = 1;
						//$id = $db->get_insert_id();
						$result["id"] = $dbrec["ID"];
						$result["msg"] = "Successfully saved Autotune policy.";
					} 
				}else $status =  2;	
//echo "status ".$status;	
			
            }
			//----------send data to Nodes -----------------------------	
			
			/*$broadcastIp = shell_exec("ifconfig | awk '/broadcast / {print $6}'");
			$pkid = rand(50, 255); // Get Incremental Packet ID - Mandatory
			for ($j=0 ; $j<1;$j++){
				$udp = new UDP();
				$statusPacket = new PMIPacket();

				$statusPacket->preamble = 255; // Optional
				$statusPacket->startByte = 85; // Optional
				$statusPacket->packetId = $pkid;
				$statusPacket->tagIdHighByte = 255;
				$statusPacket->tagIdLowByte = 255;
				$clusters = array ($_REQUEST['Cluster'],0,0,0,0,0,0,0,0,0);
				$statusPacket->cluster = $clusters;
				$splitSTime = new DateTime($_REQUEST['startTime']);
				$splitETime = new DateTime($_REQUEST['endTime']);
				$s_hour = $splitSTime->format("H");
				$s_min = $splitSTime->format("i");
				$s_sec =$splitSTime->format("s");
				$color = $_REQUEST['dim'];
				$color_hi = UDP::getHiByte($color);;
				$color_lo = UDP::getloByte($color);;
				$e_hour = $splitETime->format("H");
				$e_min = $splitETime->format("i");
				$e_sec =$splitETime->format("s");
				$cluster =$_REQUEST['Cluster'];
				$param1 =255;
				$param2 =255;
				$trig = 1;
				$statusPacketBytes = array(PT_COMMANDS,ADD_AT_POLICY,$id,$id,$DOW,$s_hour,$s_min,$s_sec,$color_hi,$color_lo,$e_hour,$e_min,$e_sec,$cluster,255,255,1);
				$statusPacket->numberOfBytes = sizeof($statusPacketBytes);

				// Set Bytes
				$statusPacket->commandParam = $statusPacketBytes;

				//echo $statusPacket->getDecimal()."\n\n";
				// Send
				$status = $udp->broadcast($broadcastIp, $statusPacket, NODE_LISTENER_PORT);
				usleep(1000);
			}*/
			//----------------------------------------------------------
            //$db->commit();
            die(json_encode($result));
        } catch (Exception $exc) {
            
            //$db->rollback();
            $result = array();
            $result["status"] = 0;
            $result["error"] = $exc->getMessage();
        }
    }
	public function GetAllATPolicy() {
        $db = new DBC();
        try {
            $result["status"] = 0;
            
            $clusterID = 0;
            $policyType = 0;
            $whereCon = "";
            
            if(isset($_REQUEST['clusterID'])) $clusterID = $_REQUEST['clusterID'];
            if(isset($_REQUEST['policyType'])){
                $policyType = $_REQUEST['policyType'];
                $whereCon =  " AND P.Formate IN ($policyType)";
            }
            
            
                //$sql = "SELECT ID,PName,StartTime,EndTime FROM pmi_at_policy  WHERE Deleted=0 ORDER BY NAME ASC";
				$sql=<<<EOF
                 SELECT  P.ID,
          P.PName ,
          P.StartTime,
          P.EndTime,
          P.ClusterID,
          CM.Name
                FROM 
                    pmi_at_policy P
                LEFT JOIN 
                    pmi_cluster CM ON (P.ClusterID=CM.ID) 
                WHERE 
                    P.Deleted=0 
                                      
EOF;

            //die($sql);
            $data = $db->get_result($sql);
            
            if(sizeof($data) > 0){
                $result["status"] = 1;
                $result["data"] = $data;
            }
            
            die(json_encode($result));
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }
	
	public function GetATPolicyDetails() {
        $db = new DBC();
        try {
            $result["status"] = 0;
            
            $id = 0;
            if(isset($_REQUEST['id'])) $id = $_REQUEST['id'];
            //echo "id".$id;
            $sql = "SELECT * FROM pmi_at_policy WHERE ID=".$id." AND Deleted=0";
            $data = $db->get_result($sql);
            
            //$sql = "SELECT COUNT(ID) AS PolcyCount FROM pmi_policy_node_mapping WHERE PolicyID=$id AND Deleted=0";
            //$existsNode = $db->get_single_result($sql);
            
            if(sizeof($data) > 0){
                $result["status"] = 1;
                $result["data"] = $data;
                //$result["existsNode"] = $existsNode;
            }
            
            die(json_encode($result));
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }
	/*     * *******************************
     * REMOVE POLICY
     * ****************************** */

    public function removeATPolicy() {
        $db = new DBC();
        $db->autocommit(false);
        try {
            $where = array();
            $dbrec = array();
            $tableName = "pmi_at_policy";
            $id = 0;
            $result["status"] = 0;
            $result["msg"] = "Faild to remove policy.";

            if (isset($_REQUEST['id'])) {
                
                $id = $_REQUEST['id'];
                
                $sql = "SELECT ClusterID FROM pmi_at_policy WHERE ID=$id";
                $existsCluster = $db->get_result_array($sql);
                    
                $where["ID"] = $id;
                $dbrec["Deleted"] = 1;
                $dbrec["DeletedBy"] = $_SESSION['LOGGED_USER_ID'];
                $dbrec["DeletedOn"] = date("Y-m-d H:m:s");
                $status = $db->update_query($dbrec, $tableName, $where);
                if ($status) {  
                    $result["status"] = 1;
                    $result["id"] = $_REQUEST['id'];
                    $result["msg"] = "Successfully removed policy.";
					
					//--------send remove command to cluster ---------
					
					$broadcastIp = shell_exec("ifconfig | awk '/broadcast / {print $6}'");
					$pkid = rand(50, 255); // Get Incremental Packet ID - Mandatory
					for ($j=0 ; $j<10;$j++){
						$udp = new UDP();
						$statusPacket = new PMIPacket();

						$statusPacket->preamble = 255; // Optional
						$statusPacket->startByte = 85; // Optional
						$statusPacket->packetId = $pkid;
						$statusPacket->tagIdHighByte = 255;
						$statusPacket->tagIdLowByte = 255;
						$clusters = array ($existsCluster[0]["ClusterID"],0,0,0,0,0,0,0,0,0);
						$statusPacket->cluster = $clusters;
						$statusPacketBytes = array(PT_COMMANDS,REMOVE_AT_POLICY,$id);		// remove all autotune policy 
						$statusPacket->numberOfBytes = sizeof($statusPacketBytes);

						// Set Bytes
						$statusPacket->commandParam = $statusPacketBytes;

						//echo $statusPacket->getDecimal()."\n\n";
						// Send
						$status = $udp->broadcast($broadcastIp, $statusPacket, NODE_LISTENER_PORT);
						usleep(1000);
					}
					//-------------------------------------------------
                }
            }

            $db->commit();
            die(json_encode($result));
        } catch (Exception $exc) {

            $db->rollback();
            echo $exc->getMessage();
        }
    }
    /*****************************************
    *  GET ALL CLUSTER
    ****************************************/
    public function getAllCluster() {
        $db = new DBC();
        try {
            $result["status"] = 0;
			
            $sql = <<<EOF
                           SELECT ID,Name FROM pmi_cluster WHERE Deleted=0 ORDER BY `Order` ASC 
						   
						   
EOF;
            $data = $db->get_result($sql);
			$dept= array ();
		foreach ($data as $cItem) {
                    $clusterID = $cItem['ID'];
					//echo "cluster:".$clusterID  ;
                    $qry = <<<EOF
                            SELECT COUNT(DISTINCT CM.NodeID)
                            FROM pmi_cluster_mapping CM 
                            LEFT JOIN pmi_tag T ON (CM.NodeID = T.NodeID)
                            WHERE ClusterID = $clusterID AND CM.Deleted = 0 AND T.StatusOn > NOW() - INTERVAL 10 Minute 
                            AND T.Deleted=0 AND T.Status = 1 AND T.Acknowledged=1
EOF;
				
			  $dept[] = $db->get_single_result($qry);
			  //$dept[]=$clusterID;
			//$data["count"] = $db->get_single_result($qry);
			
			
			
		}
			//print_r ($dept);
            if(sizeof($data) > 0){
                $result["status"] = 1;
                $result["data"] = $data;
				$result["child"]=$dept;
            }
            die(json_encode($result));
        } catch (Exception $exc) {
            throw $exc->getMessage();
        }
    }
    /*     * *******************************
     * POLICY CREATION SAVE
     * ****************************** */

     public function insertOrUpdatePolicy() {
        $db = new DBC();
        $db->autocommit(false);
		
		
        try {
            $where = array();$dbrec= array();$tableName = "pmi_policy";
            $id = 0;
            $result["status"] = 0;
            $result["msg"] = "Faild to save policy.";
            
            if(isset($_REQUEST['id'])){
				//----------modify policy -------------
                $id = $_REQUEST['id'];
                $where["ID"]= $id;
                $dbrec["ModifiedBy"] = $_SESSION['LOGGED_USER_ID'];
                $dbrec["ModiFiedOn"] = date("Y-m-d H:m:s");
            }
            else {
				//----------create policy so get ID------------
				
				$dbrec["Createdby"] = $_SESSION['LOGGED_USER_ID'];
			}
			
            if(isset($_REQUEST['name'])){
                $dbrec["Name"]=$_REQUEST['name'];
                    $sql ="SELECT ID FROM pmi_policy WHERE Name='".$_REQUEST['name']."' AND Deleted=0";
                    if($id != 0 ) $sql .= " AND ID!=".$id;
                    $existRec = $db->get_result_array($sql);
                    if(sizeof($existRec) > 0){
                        $result["status"] = 0;
                        $result["msg"] = "Policy name already exists.";
                        die(json_encode($result));
                    }
            }
           //---------------------Format -------------------------------------- 
            if(isset($_REQUEST['formate'])) $dbrec["Formate"]=$_REQUEST['formate'];
			//--------------------Motion triggered + duration --------------------
            if(isset($_REQUEST['motionStatus']) || (isset($_REQUEST['formate']) && $_REQUEST['formate']=3)) {
                $dbrec["MotionStatus"]=$_REQUEST['motionStatus'];
                if(isset($_REQUEST['durationHour'])) $dbrec["DurationHour"]= $_REQUEST['durationHour'];
                if(isset($_REQUEST['durationMinute'])) $dbrec["DurationMinute"]= $_REQUEST['durationMinute'];
            }
			//------------------------Dim Level ---------------------------------
			if(isset($_REQUEST['dim'])) $dbrec["Dim"]=$_REQUEST['dim']; 
			//--------------------Start Date and Start End Time -------------------
            if(isset($_REQUEST['startDate'])) $dbrec["StartDate"]= date("Y-m-d H:i:s",  strtotime ($_REQUEST['startDate']));
            if(isset($_REQUEST['startTime'])) $dbrec["StartTime"]=date("H:i:s",  strtotime ($_REQUEST['startTime']));
            if(isset($_REQUEST['endTime'])) $dbrec["EndTime"]=date("H:i:s",  strtotime ($_REQUEST['endTime']));
           //----------------------Day of the week -------------------------------------
            if(isset($_REQUEST['day1'])) $dbrec["Day1"]=$_REQUEST['day1'];
            if(isset($_REQUEST['day2'])) $dbrec["Day2"]=$_REQUEST['day2'];
            if(isset($_REQUEST['day3'])) $dbrec["Day3"]=$_REQUEST['day3'];
            if(isset($_REQUEST['day4'])) $dbrec["Day4"]=$_REQUEST['day4'];
            if(isset($_REQUEST['day5'])) $dbrec["Day5"]=$_REQUEST['day5'];
            if(isset($_REQUEST['day6'])) $dbrec["Day6"]=$_REQUEST['day6'];
            if(isset($_REQUEST['day7'])) $dbrec["Day7"]=$_REQUEST['day7'];
			//--------------------Light default ----------------------------------
			if(isset($_REQUEST['lightDefault'])) $dbrec["LightDefault"]=$_REQUEST['lightDefault'];
			//-------Color ---------------------------------------------
			if(isset($_REQUEST['colorStatus'])) {$dbrec["ColorStatus"]=$_REQUEST['colorStatus'];}
			if(isset($_REQUEST['color'])) $dbrec["Color"]=$_REQUEST['color'];
			
			
            //print_r($dbrec);
            if(sizeof($where) >0 ){				// modify policy 
                $status = $db->update_query ($dbrec, $tableName, $where);
                if($status){
						$result["status"] = 1;
						$result["id"] = $_REQUEST['id'];
						$result["msg"] = "Successfully updated policy.";
						//$this->savePolicyLine($dbrec, $id,$db);
						if($dbrec["Formate"] != 3){
							$sql = "SELECT ClusterID FROM pmi_policy_mapping WHERE PolicyID=$id AND Deleted=0";
							$existCluster = $db->get_result_array($sql);
		
							//print_r($existCluster);
								if(sizeof($existCluster) > 0){
								
								$dbrec = array();
								$dbrec["Type"] = 3; // policy
								$dbrec["TypeID"] = $id;
								$dbrec["CreatedBy"] = $_SESSION['LOGGED_USER_ID'];
								$dbrec["CreatedOn"] = date("Y-m-d H:m:s");
								$status = $db->insert_query($dbrec, "pmi_merge_request");
								$packeReqID = $db->get_insert_id();
								
								for ($k = 0; $k < count($existCluster); $k++) {
									
									
									$data = array(
										"ID" => $existCluster[$k]["ClusterID"],
										"Type" => 1, //Cluster;2 Node;
										"Action" => 3,//Modified Policy
										"PolicyID" => $id,
										"requestedID" => $packeReqID
									);
									//$notQue = new UDPQueueClient(NOTIF_QUEUE_SERVER,SYSTEMLOG_QUEUE_NAME);
									//$notQue->queueNotificationRequest(POLICY_PROCESS, $data);
								}
								//$notQue = new UDPQueueClient(NOTIF_QUEUE_SERVER,SYSTEMLOG_QUEUE_NAME);
							   //$notQue->queueNotificationRequest(POLICY_MERGE_CHECK_PROCESS, array("requestedID"=>$packeReqID),POLICY_CHECK_DELAY);
						}
                        
                    
                       // $notQue = new UDPQueueClient(NOTIF_QUEUE_SERVER,SYSTEMLOG_QUEUE_NAME);
                      //  $notQue->queueNotificationRequest(POLICY_TIMELINE_PROCESS, array("policyID" => $id));

                        
					}
                    else{
                        $sql = "SELECT O.SwitchId,O.SourceType,O.PolicyID,O.ClusterID,T.ID AS NodeID FROM pmi_override O LEFT JOIN pmi_tag T ON (T.NodeID IN (SELECT TG.NodeID FROM pmi_tag TG WHERE TG.ID = O.SwitchId) AND T.TagTypeID=1) WHERE O.PolicyID=$id AND T.Deleted=0 AND O.Deleted=0";
                        //echo $sql;
                        $existOverride = $db->get_result_array($sql);
                        //print_r($existOverride);
                        //
                        //add to queue
                        $dbrec = array();
                        $dbrec["Type"] = 3; // Policy
                        $dbrec["TypeID"] = $id;
                        $dbrec["CreatedBy"] = $_SESSION['LOGGED_USER_ID'];
                        $dbrec["CreatedOn"] = date("Y-m-d H:m:s");
                        $status = $db->insert_query($dbrec, "pmi_merge_request");
                        $packeReqID = $db->get_insert_id();
                        
                        for ($i = 0; $i < sizeof($existOverride); $i++) {
                            
                            
                            $data = array(
                                "ID" => $existOverride[$i]["NodeID"],
                                "Type" => 2, //1 Cluster;2 Node;
                                "Action" => 3,//Modified Policy
                                "PolicyID" => $id,
                                "SourceType" => $existOverride[$i]["SourceType"],
                                "OverrideID" => $existOverride[$i]["ID"],
                                "requestedID" => $packeReqID
                            );
                            //$notQue = new UDPQueueClient(NOTIF_QUEUE_SERVER,SYSTEMLOG_QUEUE_NAME);
                           // $notQue->queueNotificationRequest(POLICY_PROCESS, $data);
						}
                        
                       // $notQue = new UDPQueueClient(NOTIF_QUEUE_SERVER,SYSTEMLOG_QUEUE_NAME);
                       // $notQue->queueNotificationRequest(POLICY_MERGE_CHECK_PROCESS, array("requestedID"=>$packeReqID),POLICY_CHECK_DELAY);
                    }
                }
            }
            else if(sizeof($dbrec) > 0){
				//----------------new policy look for ID ----------------
				// get deleted one 
		
				$qry1 = "DELETE FROM pmi_policy WHERE Deleted = 1";
				$res = $db->query($qry1);
				if ($res == 1){
					for ($i=7; $i < 255; $i++){
						$qry2 = "SELECT COUNT(*) FROM pmi_policy WHERE ID =$i";
						$id_cluster = $db->get_single_result($qry2);
						if ($id_cluster == 0){
							$dbrec["ID"] = $i;
							//echo "ID will be inserted ".$i;
							break;
						}
					}
				}
				if ($i < 255 ) {
					$status = $db->insert_query($dbrec, 'pmi_policy');
				
					if($status){
						$result["status"] = 1;
						//$id = $db->get_insert_id();
						$result["id"] = $dbrec["ID"];
						$result["msg"] = "Successfully saved  policy.";
					} 
				}else {
					$status =  2;
					$result["msg"] = "Too many policies please remove some ";					
				}
				//-----------------------------------------------------              
                
                //$notQue = new UDPQueueClient(NOTIF_QUEUE_SERVER,SYSTEMLOG_QUEUE_NAME);
                //$notQue->queueNotificationRequest(POLICY_TIMELINE_PROCESS, array("policyID" => $id));
            }
            $db->commit();
            die(json_encode($result));
        } catch (Exception $exc) {
            
            $db->rollback();
            $result = array();
            $result["status"] = 0;
            $result["error"] = $exc->getMessage();
        }
    }
    
    /*****************************************
     *      Insert policy line to db
     *****************************************/
    public function savePolicyLine($data,$policyID,$db){
        //print_r($data);
        //$db = new DBC();
        $tableName = "pmi_policy_line";
        $format = $data["Formate"];
        $action = $data["Action"];
        $dim = $data["Dim"];
        $fadeIN = $data["FadeIn"];
        $timeFormate = $data["TimeFormate"];
        $lightDefault = $data["LightDefault"];
        $startDate = isset($data["StartDate"]) ? $data["StartDate"] : "";
        $endDate = isset($data["EndDate"]) ? $data["EndDate"] : "";
        $startTime = isset($data["StartTime"]) ? $data["StartTime"] : "";
        $motion = isset($data["MotionStatus"]) ? $data["MotionStatus"] : 0;
        $durationH = isset($data["DurationHour"]) ? $data["DurationHour"] : "";
        $durationM = isset($data["DurationMinute"]) ? $data["DurationMinute"] : "";
        $endTime = isset($data["EndTime"]) ? $data["EndTime"] : "";
        $dayArr["Day1"] = isset($data["Day1"]) ? $data["Day1"] : 0;
        $dayArr["Day2"]  = isset($data["Day2"]) ? $data["Day2"] : 0;
        $dayArr["Day3"]  = isset($data["Day3"]) ? $data["Day3"] : 0;
        $dayArr["Day4"]  = isset($data["Day4"]) ? $data["Day4"] : 0;
        $dayArr["Day5"]  = isset($data["Day5"]) ? $data["Day5"] : 0;
        $dayArr["Day6"]  = isset($data["Day6"]) ? $data["Day6"] : 0;
        $dayArr["Day7"]  = isset($data["Day7"]) ? $data["Day7"] : 0;
        $policyLineID = 0;
        
        //print_r($data);
        $db->update_query(array("Deleted" => 2), $tableName, array("PolicyID" => $policyID,"Deleted" => 1));
        $db->update_query(array("Status" => 0), $tableName, array("PolicyID" => $policyID));
        
        //Change Policy
        $changePolicy = array();
        $changePolicy["PolicyID"] = $policyID;
        $changePolicy["PolicyLineID"] = $policyLineID;
        $changePolicy["CMD"] = CHANGE_POLICY;
        $changePolicy["Param1"] = $policyID;
        $changePolicy["Param2"] = $fadeIN;
        $changePolicy["Param3"] = $fadeIN;
        $changePolicy["Param4"] = $lightDefault;
        $changePolicy["CMDAction"] = 0;
        $policyLineID++;
        
        //Weekly
        if(($format == 1 && $startTime != "") || ($format == 2 && $timeFormate==1 && $startTime != "")){
            $splitSTime = new DateTime($startTime);
            $splitETime = new DateTime($endTime);
            $chnagePDate = new DateTime($startTime);
            $chnagePDate->sub(new DateInterval('PT1M'));
            
            //Change policy : Set time
            $changePolicy["Hour"]=$chnagePDate->format('H');
            $changePolicy["Minute"]=$chnagePDate->format('i');
            $changePolicy["Second"]=$chnagePDate->format("s");
            
            //week of day convert to binary
            $DOW=0;
            $dowSel = array($dayArr["Day7"],$dayArr["Day1"],$dayArr["Day2"],$dayArr["Day3"],$dayArr["Day4"],$dayArr["Day5"],$dayArr["Day6"]); // M, T, W
            $dowSel = array_reverse($dowSel); // Reverse order
            $dowBits = implode('',$dowSel); // Join values without delimiter
            $down = str_pad($dowBits, 8, '0', STR_PAD_LEFT); // 8 Bit pad with 0
            $DOW = bindec($down); // Convert to decimal            
            $changePolicy["DOW"]=$DOW;
            
            //Weekly change policy updation
            $condition = " AND Day=255 AND Month=255 AND CMD=".CHANGE_POLICY." AND Param1=".$policyID;
            $this->PolicyLineUpdatation($changePolicy,$condition,$db);
            
            //If motion is unlocked
            //echo ">>>>> $motion \n";
            if($motion == 1){
                //Duration convert to binary
                $durationH = decbin(is_null($durationH) ? 0 : $durationH);
                $durationM = decbin(is_null($durationM) ? 0 : $durationM);
                $duration = (($durationH*60)+($durationM));//(2 ^ $durationH) + (2 ^ $durationM);
                
               
                $PolicyLineArr = array();            
                $PolicyLineArr["PolicyID"] = $policyID;
                $PolicyLineArr["PolicyLineID"] = $policyLineID;
                $PolicyLineArr["Year"]= 0;
                $PolicyLineArr["Month"]= 0;
                $PolicyLineArr["Day"]= 0;
                $PolicyLineArr["DOW"]= 0;
                $PolicyLineArr["Hour"]= 0;
                $PolicyLineArr["Minute"]= 0;
                $PolicyLineArr["Second"]= 0;
                $PolicyLineArr["CMD"]= EV_MOTION;
                $PolicyLineArr["Param2"]= $dim;
                $PolicyLineArr["Param3"]= $duration;
                $PolicyLineArr["Param4"]= $lightDefault;
                $PolicyLineArr["CMDAction"]= MOTION_DETECTED;
                $policyLineID++;
                $condition = " AND Day=255 AND Month=255 AND CMD=".EV_MOTION." AND CMDAction=".MOTION_DETECTED;
                $this->PolicyLineUpdatation($PolicyLineArr,$condition,$db);
                
            }
            else{
                //Policy line (Start time)
                
                $PolicyLineArr = array();  
                $PolicyLineArr["PolicyID"] = $policyID;
                $PolicyLineArr["PolicyLineID"] = $policyLineID;
                $PolicyLineArr["DOW"]=$DOW;
                $PolicyLineArr["Hour"]=$splitSTime->format("H");
                $PolicyLineArr["Minute"]=$splitSTime->format("i");
                $PolicyLineArr["Second"]=$splitSTime->format("s");
                if($action == 1) $PolicyLineArr["CMD"] = LIGHT_ON;
                else if($action == 0) $PolicyLineArr["CMD"] = LIGHT_OFF;
                $PolicyLineArr["Param2"]= $dim;
                $PolicyLineArr["Param4"]= $lightDefault;
                $PolicyLineArr["CMDAction"]= EV_CMD_ACTION; 
                $policyLineID++;

                $condition = " AND Day=255 AND Month=255 AND CMDAction=".EV_CMD_ACTION;
                $this->PolicyLineUpdatation($PolicyLineArr,$condition,$db);
                
                //Policy line (end time)
                
                $PolicyLineArr = array();  
                $PolicyLineArr["PolicyID"] = $policyID;
                $PolicyLineArr["PolicyLineID"] = $policyLineID;
                $PolicyLineArr["DOW"]=$DOW;
                $PolicyLineArr["Hour"]=$splitETime->format("H");
                $PolicyLineArr["Minute"]=$splitETime->format("i");
                $PolicyLineArr["Second"]=$splitETime->format("s");
                if($action == 0) $PolicyLineArr["CMD"] = LIGHT_ON;
                else if($action == 1) $PolicyLineArr["CMD"] = LIGHT_OFF;
                $PolicyLineArr["Param2"]= $dim;
                $PolicyLineArr["Param4"]= $lightDefault;
                $PolicyLineArr["CMDAction"]= EV_CMD_ACTION; 
                $policyLineID++;
                
                $condition = " AND Day=255 AND Month=255 AND CMDAction=".EV_CMD_ACTION;
                $this->PolicyLineUpdatation($PolicyLineArr,$condition,$db);
            
                
                //Policy line (end time)
                
                $PolicyLineArr = array();  
                $PolicyLineArr["PolicyID"] = $policyID;
                $PolicyLineArr["PolicyLineID"] = $policyLineID;
                $PolicyLineArr["Year"]= 0;
                $PolicyLineArr["Month"]= 0;
                $PolicyLineArr["Day"]= 0;
                $PolicyLineArr["DOW"]= 0;
                $PolicyLineArr["Hour"]= 0;
                $PolicyLineArr["Minute"]= 0;
                $PolicyLineArr["Second"]= 0;
                $PolicyLineArr["CMD"] = EV_MOTION;
                $PolicyLineArr["Param2"]= $dim;
                $PolicyLineArr["Param4"]= $lightDefault;
                $PolicyLineArr["CMDAction"]= EV_CMD_ACTION; 
                $policyLineID++;
                
                $condition = " AND Day=255 AND Month=255 AND CMDAction=".EV_CMD_ACTION;
                $this->PolicyLineUpdatation($PolicyLineArr,$condition,$db);
               
            }
            
            //default Policy
            $defaultPolicy = array();
            $defaultPolicy["PolicyID"] = $policyID;
            $defaultPolicy["PolicyLineID"] = $policyLineID;
            $defaultPolicy["Hour"]=$splitETime->format("H");
            $defaultPolicy["Minute"]=$splitETime->format("i");
            $defaultPolicy["Second"]=$splitETime->format("s");
            $defaultPolicy["CMD"] = CHANGE_POLICY;
            $defaultPolicy["Param1"] = DEFAULT_POLICY;
            
            $condition = " AND Day=255 AND Month=255 AND CMD=".CHANGE_POLICY." AND Param1=".DEFAULT_POLICY;
            $this->PolicyLineUpdatation($defaultPolicy,$condition,$db);
        }
        
        //single || Mothly || Year
        else if($format == 2 && ($timeFormate == 0 || $timeFormate == 2 || $timeFormate == 3) ){
            //die($startDate);
            $SDate = new DateTime($startDate); 
            $EDate = new DateTime($endDate);
            $STime = new DateTime($startTime);
            $ETime = new DateTime($endTime);
            $chnagePDate =  new DateTime($startDate); 
            $chnagePDate->setTime($STime->format("H"), $STime->format("i"));
            $chnagePDate->sub(new DateInterval('PT1M'));
            //die($chnagePDate->format("y"));
            if($timeFormate == 3 || $timeFormate == 0) $changePolicy["Year"]= $chnagePDate->format("y");
            if($timeFormate != 1 || $timeFormate != 2 ) $changePolicy["Month"]= $chnagePDate->format("n");
            $changePolicy["Day"]= $chnagePDate->format("j");
            $changePolicy["Hour"]=$chnagePDate->format('H');
            $changePolicy["Minute"]=$chnagePDate->format('i');
            $changePolicy["Second"]=$chnagePDate->format('s');

            $condition = " AND DOW=255 AND CMD=".CHANGE_POLICY." AND Param1=".$policyID;
            $this->PolicyLineUpdatation($changePolicy,$condition,$db);
            
            if($motion == 1){
                //Policy line 1
                $durationH = decbin(is_null($durationH) ? 0 : $durationH);
                $durationM = decbin(is_null($durationM) ? 0 : $durationM);
                $duration = (($durationH*60)+($durationM));//(2 ^ $durationH) + (2 ^ $durationM);

                $PolicyLineArr = array();            
                $PolicyLineArr["PolicyID"] = $policyID;
                $PolicyLineArr["PolicyLineID"] = $policyLineID;
                $PolicyLineArr["CMD"]= EV_MOTION;
                $PolicyLineArr["Year"]= 0;
                $PolicyLineArr["Month"]= 0;
                $PolicyLineArr["Day"]= 0;
                $PolicyLineArr["DOW"]= 0;
                $PolicyLineArr["Hour"]= 0;
                $PolicyLineArr["Minute"]= 0;
                $PolicyLineArr["Second"]= 0;
                $PolicyLineArr["Param2"]= $dim;
                $PolicyLineArr["Param3"]= $duration;
                $PolicyLineArr["Param4"]= $lightDefault;
                $PolicyLineArr["CMDAction"]= MOTION_DETECTED;
                $policyLineID++;
                $condition = " AND DOW=255 AND CMD=".EV_MOTION." AND CMDAction=".MOTION_DETECTED;
                $this->PolicyLineUpdatation($PolicyLineArr,$condition,$db);

            }
            else{
                //Policy line (Start time)
                
                $PolicyLineArr = array();  
                $PolicyLineArr["PolicyID"] = $policyID;
                $PolicyLineArr["PolicyLineID"] = $policyLineID;
                if($timeFormate == 3 || $timeFormate == 0) $PolicyLineArr["Year"]= $SDate->format("y");
                if($timeFormate != 1 || $timeFormate != 2 ) $PolicyLineArr["Month"]= $SDate->format("n");
                $PolicyLineArr["Day"]= $SDate->format("j");
                $PolicyLineArr["Hour"]=$STime->format("H");
                $PolicyLineArr["Minute"]=$STime->format("i");
                $PolicyLineArr["Second"]=$STime->format("s");
                if($action == 1) $PolicyLineArr["CMD"] = LIGHT_ON;
                else if($action == 0) $PolicyLineArr["CMD"] = LIGHT_OFF;
                $PolicyLineArr["Param2"]= $dim;
                $PolicyLineArr["Param4"]= $lightDefault;
                $PolicyLineArr["CMDAction"]= EV_CMD_ACTION; 
                $policyLineID++;
                
                $condition = " AND DOW=255 AND CMDAction=".EV_CMD_ACTION;
                $this->PolicyLineUpdatation($PolicyLineArr,$condition,$db);
                
                //Policy line (end time)
                
                $PolicyLineArr = array();  
                $PolicyLineArr["PolicyID"] = $policyID;
                $PolicyLineArr["PolicyLineID"] = $policyLineID;
                if($timeFormate == 3 || $timeFormate == 0) $PolicyLineArr["Year"]= $EDate->format("y");
                if($timeFormate != 1 || $timeFormate != 2 ) $PolicyLineArr["Month"]= $EDate->format("n");
                $PolicyLineArr["Day"]= $EDate->format("j");
                $PolicyLineArr["Hour"]=$ETime->format("H");
                $PolicyLineArr["Minute"]=$ETime->format("i");
                $PolicyLineArr["Second"]=$ETime->format("s");
                if($action == 0) $PolicyLineArr["CMD"] = LIGHT_ON;
                else if($action == 1) $PolicyLineArr["CMD"] = LIGHT_OFF;
                $PolicyLineArr["Param2"]= $dim;
                $PolicyLineArr["Param4"]= $lightDefault;
                $PolicyLineArr["CMDAction"]= EV_CMD_ACTION; 
                $policyLineID++;
                
                $condition = " AND DOW=255 AND CMDAction=".EV_CMD_ACTION;
                $this->PolicyLineUpdatation($PolicyLineArr,$condition,$db);
                
                //Policy line (end time)
                
                $PolicyLineArr = array();  
                $PolicyLineArr["PolicyID"] = $policyID;
                $PolicyLineArr["PolicyLineID"] = $policyLineID;
				 $PolicyLineArr["Year"]= 0;
                $PolicyLineArr["Month"]= 0;
                $PolicyLineArr["Day"]= 0;
                $PolicyLineArr["DOW"]= 0;
                $PolicyLineArr["Hour"]= 0;
                $PolicyLineArr["Minute"]= 0;
                $PolicyLineArr["Second"]= 0;
                $PolicyLineArr["CMD"] = EV_MOTION;
                $PolicyLineArr["Param2"]= $dim;
                $PolicyLineArr["Param4"]= $lightDefault;
                $PolicyLineArr["CMDAction"]= EV_CMD_ACTION; 
                $policyLineID++;
                
                $condition = " AND DOW=255 AND CMDAction=".EV_CMD_ACTION;
                $this->PolicyLineUpdatation($PolicyLineArr,$condition,$db);
            }
            
            
            //default Policy
            $defaultPolicy = array();
            $defaultPolicy["PolicyID"] = $policyID;
            $defaultPolicy["PolicyLineID"] = $policyLineID;
            if($timeFormate == 3 || $timeFormate == 0) $defaultPolicy["Year"]= $EDate->format("y");
            if($timeFormate != 1 || $timeFormate != 2 ) $defaultPolicy["Month"]= $EDate->format("n");
            $defaultPolicy["Day"]= $EDate->format("j");
            $defaultPolicy["Hour"]=$ETime->format("H");
            $defaultPolicy["Minute"]=$ETime->format("i");
            $defaultPolicy["Second"]=$ETime->format("s");
            $defaultPolicy["CMD"] = CHANGE_POLICY;
            $defaultPolicy["Param1"] = DEFAULT_POLICY;

            $condition = " AND DOW=255 AND CMD=".CHANGE_POLICY." AND Param1=".DEFAULT_POLICY;
            $this->PolicyLineUpdatation($defaultPolicy,$condition,$db);
        }
       
        //Override policy
        else if($format == 3){
            //echo "durationH >> $durationH >>> durationH >> $durationM \n";
            $changePolicy["CMD"] = CHANGE_TIMED_POLICY;
            $changePolicy["Year"]= 255;
            $changePolicy["Month"]= 255;
            $changePolicy["Day"]= 255;
            $changePolicy["DOW"]= 127;
            $changePolicy["Hour"]= 255;
            $changePolicy["Minute"]= 255;
            $changePolicy["Second"]= 255;
            $changePolicy["Param1"] = $durationH;
            $changePolicy["Param2"] = $durationM;
            $changePolicy["Param3"] = 0;
            $changePolicy["Param4"] = 0;

            $condition = " AND DOW=255 AND CMD=".CHANGE_TIMED_POLICY." AND Param3=0";
            $this->PolicyLineUpdatation($changePolicy,$condition,$db);
            
            $PolicyLineArr = array();   
            $PolicyLineArr["PolicyID"] = $policyID;
            if($action == 1) $PolicyLineArr["CMD"] = LIGHT_ON;
            else if($action == 0) $PolicyLineArr["CMD"] = LIGHT_OFF;
            $duration = (($durationH*60)+($durationM));
            $PolicyLineArr["PolicyLineID"] = $policyLineID;
            $PolicyLineArr["Year"]= 255;
            $PolicyLineArr["Month"]= 255;
            $PolicyLineArr["Day"]= 255;
            $PolicyLineArr["DOW"]= 0;
            $PolicyLineArr["Hour"]= 255;
            $PolicyLineArr["Minute"]= 255;
            $PolicyLineArr["Second"]= 255;
            $PolicyLineArr["Param2"]= $dim;
            $PolicyLineArr["Param3"]= $duration;
            $PolicyLineArr["Param4"]= $lightDefault;
            $PolicyLineArr["CMDAction"]= EV_CMD_ACTION;

            $condition = " AND DOW=255 AND CMD=".$PolicyLineArr["CMD"];
            $this->PolicyLineUpdatation($PolicyLineArr,$condition,$db);
            $policyLineID++;
            
            $PolicyLineArr = array();  
            $PolicyLineArr["PolicyID"] = $policyID;
            $PolicyLineArr["PolicyLineID"] = $policyLineID;
			$PolicyLineArr["Year"]= 0;
			$PolicyLineArr["Month"]= 0;
			$PolicyLineArr["Day"]= 0;
			$PolicyLineArr["DOW"]= 0;
			$PolicyLineArr["Hour"]= 0;
			$PolicyLineArr["Minute"]= 0;
			$PolicyLineArr["Second"]= 0;
            $PolicyLineArr["CMD"] = EV_MOTION;
            $PolicyLineArr["Param2"]= 1;
            $PolicyLineArr["Param3"]= $dim;
            $PolicyLineArr["Param4"]= $lightDefault;
            $PolicyLineArr["CMDAction"]= MOTION_DETECTED;

            $condition = " AND DOW=255 AND CMD=".EV_MOTION;
            $this->PolicyLineUpdatation($PolicyLineArr,$condition,$db);
            $policyLineID++;
            
            $PolicyLineArr = array();  
            $PolicyLineArr["PolicyID"] = $policyID;
            $PolicyLineArr["PolicyLineID"] = $policyLineID;
			$PolicyLineArr["Year"]= 0;
			$PolicyLineArr["Month"]= 0;
			$PolicyLineArr["Day"]= 0;
			$PolicyLineArr["DOW"]= 0;
			$PolicyLineArr["Hour"]= 0;
			$PolicyLineArr["Minute"]= 0;
			$PolicyLineArr["Second"]= 0;
            $PolicyLineArr["CMD"] = CANCEL_BUTTON;

            $condition = " AND DOW=255 AND CMD=".CANCEL_BUTTON;
            $this->PolicyLineUpdatation($PolicyLineArr,$condition,$db);
        }
       
        $db->update_query(array("Deleted" => 1,"DeletedOn"=>date("Y-m-d H:m:s")), $tableName, array("PolicyID" => $policyID,"Status"=>0,"Deleted" => 0));
    }
    
    /*********************************
    *   Insert or update policy line
    *********************************/
    public function  PolicyLineUpdatation($dataArr,$condition,$db){
        try {
            $tableName = "pmi_policy_line";
            //Insert policy line
            $PLID = 0;
            $sql="SELECT ID FROM $tableName WHERE PolicyID=".$dataArr["PolicyID"]." AND PolicyLineID=".$dataArr["PolicyLineID"]." AND Deleted=0".$condition;
            //echo $sql." \n";
            $existsPL = $db->get_result($sql);
            if(sizeof($existsPL) > 0){
                $PLID = $existsPL[0]["ID"];
                $dataArr["Status"]= 2;
            }
            else $dataArr["Status"]= 1;
            if($PLID == 0) $status = $db->insert_query($dataArr, $tableName);
            else $status = $db->update_query ($dataArr, $tableName, array("ID"=>$PLID));
        } catch (Exception $ex) {
            throw $ex;
        }
    }


    /*********************************
     * GET ALL POLICY
     * ****************************** */

    public function GetAllPolicy() {
        $db = new DBC();
        try {
            $result["status"] = 0;
            
            $clusterID = 0;
            $policyType = 0;
            $whereCon = "";
            
            if(isset($_REQUEST['clusterID'])) $clusterID = $_REQUEST['clusterID'];
            if(isset($_REQUEST['policyType'])){
                $policyType = $_REQUEST['policyType'];
                $whereCon =  " AND P.Formate IN ($policyType)";
            }
            
           if($clusterID == 0)
                $sql = "SELECT P.ID,P.Name,P.Formate FROM pmi_policy P WHERE P.Deleted=0  ORDER BY P.NAME ASC";
            else
                $sql = <<<EOF
                           SELECT 
                                P.ID,
                                P.Name,
                                P.Formate 
                            FROM 
                                pmi_policy P
                            JOIN pmi_policy_mapping PM ON (PM.PolicyID = P.ID)
                            WHERE 
                                PM.ClusterID = $clusterID AND PM.Deleted=0 AND P.Deleted=0 
EOF;
            //die($sql);
            $data = $db->get_result($sql);
            
            if(sizeof($data) > 0){
                $result["status"] = 1;
                $result["data"] = $data;
            }
            
            die(json_encode($result));
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }

    /*********************************
     * GET POLICY BY POLICY ID
     * ****************************** */

    public function GetPolicyDetails() {
        $db = new DBC();
        try {
            $result["status"] = 0;
            
            $id = 0;
            if(isset($_REQUEST['id'])) $id = $_REQUEST['id'];
            
            $sql = "SELECT * FROM pmi_policy WHERE ID=".$id." AND Deleted=0";
            $data = $db->get_result($sql);
            
            $sql = "SELECT COUNT(ID) AS PolcyCount FROM pmi_policy_node_mapping WHERE PolicyID=$id AND Deleted=0";
            $existsNode = $db->get_single_result($sql);
            
            if(sizeof($data) > 0){
                $result["status"] = 1;
                $result["data"] = $data;
                $result["existsNode"] = $existsNode;
            }
            
            die(json_encode($result));
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }
    
    /*     * *******************************
     * REMOVE POLICY
     * ****************************** */

    public function removePolicy() {
		/*
		remove policy from cluster mapping 
		remove policy from event line 
		remove policy from merge request
		
		*/
	
        $db = new DBC();
        $db->autocommit(false);
        try {
            $where = array();
            $dbrec = array();
            $tableName = "pmi_policy";
            $id = 0;
            $result["status"] = 0;
            $result["msg"] = "Faild to remove policy.";

            if (isset($_REQUEST['id'])) {
                
                $id = $_REQUEST['id'];
                
                $sql = "SELECT ClusterID FROM pmi_policy_mapping WHERE PolicyID=$id";
                $existsCluster = $db->get_result_array($sql);
                    
                $where["ID"] = $id;
                $dbrec["Deleted"] = 1;
                $dbrec["DeletedBy"] = $_SESSION['LOGGED_USER_ID'];
                $dbrec["DeletedOn"] = date("Y-m-d H:m:s");
                $status = $db->update_query($dbrec, $tableName, $where);
                if ($status) {
                    
                    //Remove policy line
                    /*$where["PolicyID"] = $id;
                    $dbrec["Deleted"] = 1;
                    $dbrec["DeletedBy"] = $_SESSION['LOGGED_USER_ID'];
                    $dbrec["DeletedOn"] = date("Y-m-d H:m:s");
                    $status = $db->update_query($dbrec, "pmi_policy_line", $where);*/
                    
                    //Remove cluster mapping
                    $where = array();
                    $dbrec = array();
                    $where["PolicyID"] = $id;
                    $dbrec["Deleted"] = 1;
                    $dbrec["DeletedBy"] = $_SESSION['LOGGED_USER_ID'];
                    $dbrec["DeletedOn"] = date("Y-m-d H:m:s");
                    $status = $db->update_query($dbrec, "pmi_policy_mapping", $where);
                    
                    if(sizeof($existsCluster) > 0){
                        
                        $dbrec = array();
                        $dbrec["Type"] = 1; // cluster
                        $dbrec["TypeID"] = $clusterID;
                        $dbrec["CreatedBy"] = $_SESSION['LOGGED_USER_ID'];
                        $dbrec["CreatedOn"] = date("Y-m-d H:m:s");
                        $status = $db->insert_query($dbrec, "pmi_merge_request");
                        $packeReqID = $db->get_insert_id();
                        
                        for ($i = 0; $i < sizeof($existsCluster); $i++) {
                            $data = array(
                                "ID" => $existsCluster[$i]["ClusterID"],
                                "Type" => 1, //Cluster;2 Node;
                                "Action" => 2,//Remove Policy
                                "PolicyID" => $id,
                                "requestedID" => $packeReqID
                            );
                            $notQue = new UDPQueueClient(NOTIF_QUEUE_SERVER,SYSTEMLOG_QUEUE_NAME);
                            $notQue->queueNotificationRequest(POLICY_PROCESS, $data);
                        }
                        
                        $notQue = new UDPQueueClient(NOTIF_QUEUE_SERVER,SYSTEMLOG_QUEUE_NAME);
                        $notQue->queueNotificationRequest(POLICY_MERGE_CHECK_PROCESS, array("requestedID"=>$packeReqID),POLICY_CHECK_DELAY);
                    }
                    $sql = "SELECT O.SwitchId,O.SourceType,O.PolicyID,O.ClusterID,T.ID AS NodeID FROM pmi_override O LEFT JOIN pmi_tag T ON (T.NodeID IN (SELECT TG.NodeID FROM pmi_tag TG WHERE TG.ID = O.SwitchId) AND T.TagTypeID=1) WHERE O.PolicyID=$id AND T.Deleted=0 AND O.Deleted=0";
                    $existOverride = $db->get_result_array($sql);

                    //add to queue
                    if(sizeof($existOverride) > 0){
                        $dbrec = array();
                        $dbrec["Type"] = 2; // Node
                        $dbrec["TypeID"] = $existOverride[$i]["NodeID"];
                        $dbrec["CreatedBy"] = $_SESSION['LOGGED_USER_ID'];
                        $dbrec["CreatedOn"] = date("Y-m-d H:m:s");
                        $status = $db->insert_query($dbrec, "pmi_merge_request");
                        $packeReqID = $db->get_insert_id();
                        
                        for ($i = 0; $i < sizeof($existOverride); $i++) {
                            $data = array(
                                "ID" => $existOverride[$i]["NodeID"],
                                "Type" => 2, //1 Cluster;2 Node;
                                "Action" => 2,//Remove Policy
                                "PolicyID" => $id,
                                "SourceType" => $existOverride[$i]["SourceType"],
                                "OverrideID" => $existOverride[$i]["ID"],
                                "requestedID" => $packeReqID
                            );
                            $notQue = new UDPQueueClient(NOTIF_QUEUE_SERVER,SYSTEMLOG_QUEUE_NAME);
                            $notQue->queueNotificationRequest(POLICY_PROCESS, $data);
                        }
                        
                        $notQue = new UDPQueueClient(NOTIF_QUEUE_SERVER,SYSTEMLOG_QUEUE_NAME);
                        $notQue->queueNotificationRequest(POLICY_MERGE_CHECK_PROCESS, array("requestedID"=>$packeReqID),POLICY_CHECK_DELAY);
                    }
                    
                    $notQue = new UDPQueueClient(NOTIF_QUEUE_SERVER,SYSTEMLOG_QUEUE_NAME);
                    $notQue->queueNotificationRequest(POLICY_TIMELINE_PROCESS, array("policyID" => $id));
                    
                    $result["status"] = 1;
                    $result["id"] = $_REQUEST['id'];
                    $result["msg"] = "Successfully removed policy.";
                }
            }

            $db->commit();
            die(json_encode($result));
        } catch (Exception $exc) {

            $db->rollback();
            echo $exc->getMessage();
        }
    }
    
    /******************************
    * MERGE POLICY
    ******************************/
    public function mergepolicy() {
           
        $db = new DBC();
        //$db->autocommit(false);
        try {
            $where = array();
            $dbrec = array();
            $tableName = "pmi_policy_mapping";
            $result["status"] = 0;
            $result["msg"] = "";

            $clusterID = 0;
            if(isset($_REQUEST['clusterID'])) $clusterID = $_REQUEST['clusterID'];	// Is an array of cluster IDs
			//$sourceId = 0;
			//if(isset($_REQUEST['sourceId'])) $sourceId = $_REQUEST['sourceId'];
            $policy =  $_REQUEST['policy'];
			
			//print_r($policy);
			
			// get policy details
			$qry = "SELECT * FROM pmi_policy WHERE ID = $policy";
			$policyRecord = $db->get_result($qry);	
			
			//print_r($policyRecord);
				
			$cluster_lengh=count($clusterID);
			for ($i = 0; $i < $cluster_lengh; $i++) {
				
				$qry = "SELECT Name FROM pmi_cluster WHERE ID = $clusterID[$i]";
				$clusterRecord = $db->get_result($qry);
				$cname =  ( $clusterID[$i] == "1" ) ? substr($clusterRecord[0]['Name'],1) : $clusterRecord[0]['Name'];
			
				$qry = "SELECT ID FROM $tableName WHERE PolicyID = $policy AND ClusterID = $clusterID[$i] AND Deleted=0";
				$stat = $db->get_result($qry);
				if (sizeof($stat) > 0 ) {
					$result["msg"] = "Policy Already merged to cluster $cname";
					break;
				}
				
				
					// check for overlap
				$qry = "select count(*) as ct from pmi_policy_mapping pm join pmi_policy p on (pm.PolicyID = p.ID) where pm.Deleted=0 and pm.ClusterID = $clusterID[$i]";
				if ( $policyRecord["Formate"] == "1" )	{
					$qry.=" and (";
					for($x=1; $x<=7;$x++)
					{
						if ( $policyRecord[0]["Day".$x] == "1" )	{
							if ( $x != 1 ) $qry.= " or ";
							$qry .= " p.Day$x = 1 ";
						}
					}	
					$qry.=" )";
				}
				//else $qry .= " p.StartDate is NOT NULL";	
				$qry .= " AND ( (p.StartTime >= '".$policyRecord[0]['StartTime']."' AND p.StartTime <= '".$policyRecord[0]['EndTime']."') OR (p.EndTime >= '".$policyRecord[0]['StartTime']."' AND p.EndTime <= '".$policyRecord[0]['EndTime']."') )";
				
				//echo($qry);
				//print_r($clusterRecord);
				
				$stat = $db->get_result($qry);
				//print_r($stat);
				//die();
				if ( isset($stat[0]) && $stat[0]["ct"] != "0" ) {
					$result["msg"] = "Policy overlaps with other policies in cluster $cname";
					break;
				}
			}
			
			//print_r ($qry);
			
			if ( $result["msg"] == "" ) 	{
				if(!empty ($clusterID)){    
					$cluster_lengh=count($clusterID);
					
					for ($i = 0; $i < $cluster_lengh; $i++) {
						
						// overlap check
						
						// all good, insert
						$dbrec = array();
						$dbrec["ClusterID"] = $clusterID[$i];
						$dbrec["PolicyID"] = $policy;
						//$dbrec["SourceID"] = $sourceId;
						$dbrec["CreatedBy"] = $_SESSION['LOGGED_USER_ID'];
						$status = $db->insert_query($dbrec, $tableName);
						if($status){
							$sql = "SELECT ID,Name,Formate FROM pmi_policy WHERE ID = $policy AND Deleted=0";
							$result["status"] = 1;
							$result["updateStatus"] = 1;
							$result["msg"] = "Successfully merged policy.";
							$result["data"] = $db->get_result($sql);
						}
					}   
					$data = array(
						"ClusterID" => $clusterID,
						//"sourceID" => $sourceId, //hardware ;2 Time match;
						"Action" => 1,//Add Policy //2 Remove 
						"PolicyID" => $policy,
						"requestedID" => $packeReqID
					);
					//$db->commit();
				}
			}
            die(json_encode($result));
        } catch (Exception $exc) {

            $db->rollback();
            echo $exc->getMessage();
        }
    }
    
    /*     * *******************************
     * REMOVE MERGE POLICY
     * ****************************** */

    public function removeMergePolicy() {
        $db = new DBC();
        $db->autocommit(false);
        try {
            $where = array();
            $dbrec = array();
            $tableName = "pmi_policy_mapping";
            $id = 0;
            $flag = 0;
            $result["status"] = 0;
            $result["msg"] = "Faild to remove policy.";

            if (isset($_REQUEST['policyID'])) {

                $clusterID = 0;
				if(isset($_REQUEST['clusterID'])) $clusterID = $_REQUEST['clusterID'];	// Is an array of cluster IDs
                $policyID = $_REQUEST['policyID'];

				$cluster_lengh=count($clusterID);
				for ($i = 0; $i < $cluster_lengh; $i++) {
					$where["PolicyID"] = $policyID;
					$where["ClusterID"] = $clusterID[$i];
					$dbrec["Deleted"] = 1;
					$dbrec["DeletedBy"] = $_SESSION['LOGGED_USER_ID'];
					$dbrec["DeletedOn"] = date("Y-m-d H:m:s");
					$status = $db->update_query($dbrec, $tableName, $where);
					if ($status) {
						$result["status"] = 1;
						$result["id"] = $_REQUEST['id'];
						$result["msg"] = "Successfully removed policy.";
					}
				}
            }
            $db->commit();
            die(json_encode($result));
        } catch (Exception $exc) {

            $db->rollback();
            echo $exc->getMessage();
        }
    }
    public function GetPolicyType() {
        $db = new DBC();
        try {
            $result["status"] = 0;
            if(isset($_REQUEST['id'])){
				$id = $_REQUEST['id'];
                 $sql = "SELECT Formate FROM pmi_policy  WHERE ID = $id and Deleted=0";
				$data = $db->get_single_result($sql);
            } 
            if(sizeof($data) > 0){
                $result["status"] = 1;
                $result["data"] = $data;
            }
            
            die(json_encode($result));
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }
    /*********************************
     * GET TAG BY TAGTYPEID
     * ****************************** */

    public function getTagByTagID() {
        $db = new DBC();
        try {
            $result["status"] = 0;
            
            $typeID = 1;
            $where = "Deleted=0";
            if(isset($_REQUEST['type'])){
                $typeID = $_REQUEST['type'];
                $where .= " AND TagTypeID = $typeID"; 
            }
            
            $sql = "SELECT ID,NodeID,Name,TagNum,SerialNum,`Status` FROM pmi_tag  WHERE $where";
            $data = $db->get_result($sql);
            
            if(sizeof($data) > 0){
                $result["status"] = 1;
                $result["data"] = $data;
            }
            
            die(json_encode($result));
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }
    
    /*     * *******************************
     * OVERRIDE POLICY CREATION SAVE
     * ****************************** */

    public function insertOrUpdateOverriderPolicy() {
        $db = new DBC();
        $db->autocommit(false);
        try {
            $where = array();
			$dbrec= array();
			$tableName = "pmi_policy_mapping";
            $id = 0;
            $result["status"] = 0;
            $result["msg"] = "Faild to save Hardware Policy.";
            
            
            if(isset($_REQUEST['id'])){
                $id = $_REQUEST['id'];
                $where["ID"]= $id;
                $dbrec["ModifiedBy"] = $_SESSION['LOGGED_USER_ID'];
                $dbrec["ModifiedOn"] = date("Y-m-d H:m:s");
            }
            else  $dbrec["CreadtedBy"] = $_SESSION['LOGGED_USER_ID'];
            if(isset($_REQUEST['name'])){
                $dbrec["Name"]=$_REQUEST['name'];
                    $sql ="SELECT ID FROM pmi_override WHERE Name='".$_REQUEST['name']."' AND Deleted=0";
                    if($id != 0 ) $sql .= " AND ID!=".$id;
                    $existRec = $db->get_result_array($sql);
                    if(sizeof($existRec) > 0){
                        $result["status"] = 0;
                        $result["msg"] = "Override name already exists.";
                        die(json_encode($result));
                    }
            }
           
            if(isset($_REQUEST['sourceId'])) $dbrec["SourceType"]=$_REQUEST['sourceId'];
            if(isset($_REQUEST['policyId'])) $dbrec["PolicyID"]=$_REQUEST['policyId'];
            if(isset($_REQUEST['clusterId'])){
                //$dbrec["ClusterID"]=$_REQUEST['clusterId'];
                $clusterIDs = $_REQUEST['clusterId'];
            }
            else $clusterIDs = array();
            
            if(sizeof($where) >0 ){
                $status = $db->update_query ($dbrec, $tableName, $where);
                if($status){
                    //Update cluster to db
                    $this->saveOverrideCluster($clusterIDs, $_REQUEST['id'],$dbrec["PolicyID"],$dbrec["SourceType"],$db);
                    
                    $result["status"] = 1;
                    $result["id"] = $_REQUEST['id'];
                    $result["msg"] = "Successfully updated override.";
                    
                    //$sql = "SELECT T.ID AS NodeID FROM pmi_tag T WHERE T.NodeID IN (SELECT TG.NodeID FROM pmi_tag TG WHERE TG.ID = ".$dbrec["SwitchId"].") AND T.TagTypeID=1 AND T.Deleted=0";
                    //echo $sql;
                    ///$nodeArr = $db->get_result_array($sql);
                    if(sizeof($clusterIDs) > 0){
                        
                        $pdbrec = array();
                        $pdbrec["Type"] = 4; // Override
                        $pdbrec["TypeID"] = $_REQUEST['id'];
                        $pdbrec["CreatedBy"] = $_SESSION['LOGGED_USER_ID'];
                        $pdbrec["CreatedOn"] = date("Y-m-d H:m:s");
                        $status = $db->insert_query($pdbrec, "pmi_merge_request");
                        $packeReqID = $db->get_insert_id();
                        
                        foreach ($clusterIDs as $Ckey => $Cvalue) {
                            
                        $data = array(
                                "ID" => $Cvalue,
                                "Type" => 1, //1 Cluster;2 Node;
                            "Action" => 3,//Modify Policy
                            "PolicyID" => $dbrec["PolicyID"],
                            "SourceType" => $dbrec["SourceType"],
                            "OverrideID" => $id,
                            "requestedID" => $packeReqID
                        ); 
                        $notQue = new UDPQueueClient(NOTIF_QUEUE_SERVER,SYSTEMLOG_QUEUE_NAME);
                        $notQue->queueNotificationRequest(POLICY_PROCESS, $data);
                        
                        }
                        $notQue->queueNotificationRequest(POLICY_MERGE_CHECK_PROCESS, array("requestedID"=>$packeReqID),POLICY_CHECK_DELAY);
                        
                }
            }
            }
            else if(sizeof($dbrec) > 0){
                $status = $db->insert_query($dbrec, $tableName);
                if($status){
                    $overrideID = $db->get_insert_id();
                    //save cluster to db
                    $this->saveOverrideCluster($_REQUEST['clusterId'], $overrideID,$dbrec["PolicyID"],$dbrec["SourceType"], $db);
                    
                    $result["status"] = 1;
                    $result["id"] = $overrideID;
                    $result["msg"] = "Successfully saved override.";
                    
                    //$sql = "SELECT T.ID AS NodeID FROM pmi_tag T WHERE T.NodeID IN (SELECT TG.NodeID FROM pmi_tag TG WHERE TG.ID = ".$dbrec["SwitchId"].") AND T.TagTypeID=1 AND T.Deleted=0";
                    
                    //$nodeArr = $db->get_result_array($sql);
                    if(sizeof($clusterIDs) > 0){
                        
                        $pdbrec = array();
                        $pdbrec["Type"] = 4; // Cluster
                        $pdbrec["TypeID"] = $overrideID;
                        $pdbrec["CreatedBy"] = $_SESSION['LOGGED_USER_ID'];
                        $pdbrec["CreatedOn"] = date("Y-m-d H:m:s");
                        $status = $db->insert_query($pdbrec, "pmi_merge_request");
                        $packeReqID = $db->get_insert_id();
                
                        foreach ($clusterIDs as $Ckey => $Cvalue) {
                            
                        $data = array(
                                "ID" => $Cvalue,
                                "Type" => 1, //1 Cluster;2 Node;
                            "Action" => 1,//Add Policy
                            "PolicyID" => $dbrec["PolicyID"],
                            "SourceType" => $dbrec["SourceType"],
                                "OverrideID" =>$overrideID,
                            "requestedID" => $packeReqID
                        );
                        
                        $notQue = new UDPQueueClient(NOTIF_QUEUE_SERVER,SYSTEMLOG_QUEUE_NAME);
                        $notQue->queueNotificationRequest(POLICY_PROCESS, $data);
                        }
                        
                        $notQue->queueNotificationRequest(POLICY_MERGE_CHECK_PROCESS, array("requestedID"=>$packeReqID),POLICY_CHECK_DELAY);
                }                
            }
            }
            $db->commit();
            die(json_encode($result));
        } catch (Exception $exc) {
            
            $db->rollback();
            echo $exc->getMessage();
        }
    }
    
    /***********************************
    *  Save override cluster mapping
    ***********************************/
    
    public function saveOverrideCluster($data,$id,$policyID,$sourceType,$db) {

        try {

            $tableName = "pmi_override_cluster_mapping";
            $sql = "SELECT ID,OverrideID,ClusterID FROM pmi_override_cluster_mapping WHERE Deleted=0 AND OverrideID=$id";
            $existCluster = $db->get_result_array($sql);
            $delCusterArr = $existCluster;
            foreach ($data as $key => $value ) {
                $updateStatus = FALSE;

                foreach ($existCluster as $Ckey => $Cvalue) {
                    //print_r($Cvalue);
                    $oldClusterID = $Cvalue["ClusterID"];
                    $oldID = $Cvalue["ID"];
                    //echo " :::: $oldClusterID == $value :::::";
                    if ($oldClusterID == $value) {
                        $updateArr = array();
                        $where = array();
                        $updateArr["Deleted"] = 0;
                        $where["ID"] = $oldID;
                        $updateStatus = $db->update_query($updateArr, $tableName, $where);
                        unset($existCluster[$Ckey]);
                    }
                }
                if (!$updateStatus) {
                    //echo $value;
                    $insertArr = array();
                    $insertArr["OverrideID"] = $id;
                    $insertArr["ClusterID"] = $value;
                    $status = $db->insert_query($insertArr, $tableName);
                }
            }
            if(sizeof($existCluster) > 0){
            
                $pdbrec = array();
                $pdbrec["Type"] = 1; // Cluster
                $pdbrec["TypeID"] = $id;
                $pdbrec["CreatedBy"] = $_SESSION['LOGGED_USER_ID'];
                $pdbrec["CreatedOn"] = date("Y-m-d H:m:s");
                $status = $db->insert_query($pdbrec, "pmi_merge_request");
                $packeReqID = $db->get_insert_id();
                foreach ($existCluster as $key => $value) {
                
                $oldID = $Cvalue["ID"];
                $updateArr = array();
                $where = array();
                $updateArr["Deleted"] = 1;
                $updateArr["DeletedOn"] = date("Y-m-d H:m:s");
                $where["ID"] = $oldID;
                $db->update_query($updateArr, $tableName, $where);


                    $data = array(
                        "ID" => $oldID,
                        "Type" => 1, //1 Cluster;2 Node;
                        "Action" => 2, //Add Policy
                        "PolicyID" => $policyID,
                        "SourceType" => $sourceType,
                        "OverrideID" => $id,
                        "requestedID" => $packeReqID
                    );
                    $notQue = new UDPQueueClient(NOTIF_QUEUE_SERVER, SYSTEMLOG_QUEUE_NAME);
                    $notQue->queueNotificationRequest(POLICY_PROCESS, $data);
            }
            
                $notQue->queueNotificationRequest(POLICY_MERGE_CHECK_PROCESS, array("requestedID" => $packeReqID), POLICY_CHECK_DELAY);
            }
            
            
        } catch (Exception $exc) {
            throw $exc;
        }
    }
    
    /**********************************
     * GET ALL OVERRIDE POLICY
    **********************************/

    public function GetAllOverridePolicy() {
        $db = new DBC();
        try {
            $result["status"] = 0;
            $id=0;
            $clusterID = 0;
            $weher = "O.Deleted=0 AND CM.Deleted=0 AND T.Deleted=0 ";
            
            if(isset($_REQUEST['clusterID'])) $clusterID = $_REQUEST['clusterID'];
            if(isset($_REQUEST['id'])) $id = $_REQUEST['id'];
            
            if($clusterID != 0) $weher .= "AND CM.ClusterID = $clusterID";
            if($id != 0) $weher .= "AND O.ID = $id";
            
            $sql = <<<EOF
                        SELECT 
                        DISTINCT 
                            O.ID,
                            O.Name,
                            O.SwitchId,
                            O.SourceType,
                            O.PolicyID,
                        (SELECT GROUP_CONCAT(CT.Name SEPARATOR ',') 
                            FROM pmi_cluster CT 
                            LEFT JOIN pmi_override_cluster_mapping OM ON (OM.ClusterID = CT.ID) 
                            WHERE OM.OverrideID=O.ID AND OM.Deleted=0
                        ) AS ClusterName,
                        (SELECT GROUP_CONCAT(CT.ID SEPARATOR ',') 
                            FROM pmi_cluster CT 
                            LEFT JOIN pmi_override_cluster_mapping OM ON (OM.ClusterID = CT.ID) 
                            WHERE OM.OverrideID=O.ID AND OM.Deleted=0
                        ) AS ClusterID,
                        T.Name AS TagName
                    FROM pmi_override O 
                        LEFT JOIN pmi_override_cluster_mapping CM ON CM.OverrideID = O.ID 
                        LEFT JOIN pmi_tag T ON O.SwitchId =  T.ID
                    WHERE $weher
                    ORDER BY Name ASC
EOF;
            //die($sql);
            $data = $db->get_result($sql);
            
            if(sizeof($data) > 0){
                $result["status"] = 1;
                $result["data"] = $data;
            }
            
            die(json_encode($result));
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }
    
    
    /*     * *******************************
     * REMOVE OVERRIDE POLICY 
     * ****************************** */

    public function removeOverridePolicy() {
        $db = new DBC();
        $db->autocommit(false);
        try {
            $where = array();
            $dbrec = array();
            $tableName = "pmi_override";
            $id = 0;
            $result["status"] = 0;
            $result["msg"] = "Faild to remove override policy.";

            if (isset($_REQUEST['id'])) {
                
                $id = $_REQUEST['id'];
                
                $sql =<<<EOF
                    SELECT 
                        O.PolicyID,
                        O.SourceType,
                        CM.ClusterID 
                    FROM 
                        pmi_override O  
                    LEFT JOIN 
                        pmi_override_cluster_mapping CM ON (O.ID=CM.OverrideID) 
                    WHERE 
                        O.ID=$id AND 
                        CM.Deleted=0
EOF;
                $existOverride = $db->get_result_array($sql);

                $where["ID"] = $id;
                $dbrec["Deleted"] = 1;
                $dbrec["DeletedBy"] = $_SESSION['LOGGED_USER_ID'];
                $dbrec["DeletedOn"] = date("Y-m-d H:m:s");
                $status = $db->update_query($dbrec, $tableName, $where);
                
                if ($status) {
                    $result["status"] = 1;
                    $result["id"] = $_REQUEST['id'];
                    $result["msg"] = "Successfully removed override policy.";
                    
                    $dbrec = array();
                    $dbrec["Type"] = 4; // override
                    $dbrec["TypeID"] = $id;
                    $dbrec["CreatedBy"] = $_SESSION['LOGGED_USER_ID'];
                    $dbrec["CreatedOn"] = date("Y-m-d H:m:s");
                    $status = $db->insert_query($dbrec, "pmi_merge_request");
                    $packeReqID = $db->get_insert_id();
                    //print_r($existOverride);
                    //add to queue
                    for ($i = 0; $i < sizeof($existOverride); $i++) {
                        $data = array(
                            "ID" => $existOverride[$i]["ClusterID"],
                            "Type" => 1, //1 Cluster;2 Node;
                            "Action" => 2,//Remove Policy
                            "PolicyID" => $existOverride[$i]["PolicyID"],
                            "SourceType" => $existOverride[$i]["SourceType"],
                            "OverrideID" => $id,
                            "requestedID" => $packeReqID
                        );
                       // print_r($data);
                        $notQue = new UDPQueueClient(NOTIF_QUEUE_SERVER,SYSTEMLOG_QUEUE_NAME);
                        $notQue->queueNotificationRequest(POLICY_PROCESS, $data);
                }
                    $notQue->queueNotificationRequest(POLICY_MERGE_CHECK_PROCESS, array("requestedID"=>$packeReqID),POLICY_CHECK_DELAY);
            }
            }

            $db->commit();
            die(json_encode($result));
        } catch (Exception $exc) {

            $db->rollback();
            echo $exc->getMessage();
        }
    }
    
    
    /***************
    * GET ALL NODE 
    ***************/
    
    public function getAllNodes(){
        $db = new DBC();
        try {
            $result["status"] = 0;
            $tagTypeID = FIXTURE_TYPE_NODE;
            $sql = <<<EOF
                SELECT 
                    N.ID,
                    N.Name 
                FROM 
                    pmi_node N 
                WHERE 
                    N.Deleted = 0 AND 
                    N.ID IN (SELECT DISTINCT NodeID FROM pmi_tag WHERE TagTypeID = $tagTypeID AND Acknowledged = 1 AND Deleted=0)
EOF;
            $data = $db->get_result($sql);
            if(sizeof($data) > 0){
                $result["status"] = 1;
                $result["data"] = $data;
            }
            die(json_encode($result));
        } catch (Exception $exc) {
            throw $exc->getMessage();
        }
    }

    public function getAllTag(){
        $db = new DBC();
        $result = array("status"=>0);
        try {
			//echo " here!!!\n";
			$clusterID = 0;
            if(isset($_REQUEST['cID'])) $clusterID = $_REQUEST['cID'];
			//echo "cluster ID >>>>>>>>>>>".$clusterID;
				// old query AK was changed so we can pull nodes based on cluster seleection 
            //$query = "SELECT ID,Name,TagTypeID FROM pmi_tag WHERE Acknowledged=1 AND `Status`=1 AND Deleted=0 ORDER BY NodeID,ID";
            //$tagArr = $db->get_result_array($query);
			$query = <<<EOF
			SELECT T.ID,T.Name,T.TagTypeID FROM pmi_cluster_mapping CM 
                                    LEFT JOIN pmi_tag T ON (T.NodeID = CM.NodeID AND T.TagTypeID=1)
                                    WHERE 
                                    CM.Deleted=0 AND 
                                    T.Deleted=0 AND 
									T.Acknowledged=1 AND
									T.`Status`=1 AND T.StatusOn > NOW() - INTERVAL 10 MINUTE  AND
                                    CM.ClusterID=$clusterID ;
EOF;
			$tagArr = $db->get_result($query);
            //print_r($tagArr);
            if(sizeof($tagArr) > 0){
                $result["status"] = 1;
                $result["result"] = $tagArr;
            }

		}
        catch (Exception $exc) {
            echo $exc->getMessage();
        }
        die(json_encode($result));
    }
    
    public function getParamVal(){
        $db = new DBC();
        $deviceType = isset($_REQUEST["deviceType"]) ? $_REQUEST["deviceType"] :0;
        $tagType = isset($_REQUEST["tagType"]) ? $_REQUEST["tagType"] :0;
        $data = unserialize(PMI_PARAM_ARR);
        $paramArr = array();
        $returnVal = array();
        $returnVal["status"] = 0;
        try {
            if(sizeof($data) > 0){
                if($deviceType == 2){
                    if($tagType == FIXTURE_TYPE_DRIVERA || $tagType == FIXTURE_TYPE_DRIVERB || $tagType == FIXTURE_TYPE_WALLSWITCH) array_push($paramArr, $data[1]);      
                    else if($tagType == FIXTURE_TYPE_OCCUPANCYSENSOR){
                        array_push($paramArr, $data[1]);     
                        array_push($paramArr, $data[2]);     
                    }
                }
                //echo "deviceType === $deviceType :::: tagType === $tagType";
                if(($deviceType == 2 && ($tagType == FIXTURE_TYPE_NODE || $tagType == 0 )) || $deviceType != 2){
                    //die("111111111111111111");
                    foreach ($data as $value) {
                        array_push($paramArr, $value); 
                    }
                }
            }
            
            if(sizeof($paramArr) > 0){
               $returnVal["status"] = 1;
               $returnVal["result"] = $paramArr;
            }
        } 
        catch (Exception $exc) {
            $returnVal["error"] = $exc->getMessage();
        }
        die(json_encode($returnVal));
    }
	
	/********************************8
		Send command to cluster 
	*********************************/
	public function sendCluster(){
        
		
		$type = $_REQUEST["t"];	//1 = all 3=cluster ID 
		$clusterID = $_REQUEST["cluster"];
		
		$cmd = $_REQUEST["command"];
        $param1 = $_REQUEST["p1"];
		$param2 = $_REQUEST["p2"];
		$param3 = $_REQUEST["p3"];		
		//echo"Comd ".$cmd." p1 ".$param1." p2 ".$param2." p3 ".$param3."\n";
        $returnVal["status"] = 0;
        try {
			$broadcastIp = shell_exec("ifconfig | awk '/broadcast / {print $6}'");

			//UDP Helper Class
			$pkid = rand(50, 255); // Get Incremental Packet ID - Mandatory
			for ($j=0 ; $j<10;$j++){
				$udp = new \UDP();
				$statusPacket = new \PMIPacket();

				$statusPacket->preamble = 255; // Optional
				$statusPacket->startByte = 85; // Optional
				$statusPacket->packetId = $pkid;
				$statusPacket->tagIdHighByte = 255;
				$statusPacket->tagIdLowByte = 255;
				
				if($type == 3 )$clusters = array ($clusterID,0,0,0,0,0,0,0,0,0);
				else $clusters = array (255,0,0,0,0,0,0,0,0,0);
				$statusPacket->cluster = $clusters;
				switch ($cmd){
					case RESTORE_TO_DEFAULT:
						$statusPacketBytes = array(PT_COMMANDS,RESTORE_TO_DEFAULT);		// verify last line command sent 
					break;
					case SET_FIXTURE_TYPE:
						$statusPacketBytes = array(PT_COMMANDS,SET_FIXTURE_TYPE,$param1,$param2);// set fixture  papr 30Watt par2 pwm/10
					break;
					case SET_DEF_DIM:
						$statusPacketBytes = array(PT_COMMANDS,SET_DEF_DIM,$param1,$param2,$param3);		// set def dim to 100% for 10 Minute  then go back.
					break;
					case TEST_SENSOR:
						$statusPacketBytes = array(PT_COMMANDS,TEST_SENSOR,$param1);		// 
					break;
					
					case AUTOTUNE_ENABLE:
						$statusPacketBytes = array(PT_COMMANDS,AUTOTUNE_ENABLE,$param1);		// 
					break;
					
					case SET_SENSOR_TYPE:
						if($param1 > 100 || $param1 < 11)$param1 = 22; // protection 
						$statusPacketBytes = array(PT_COMMANDS,SET_SENSOR_TYPE,$param1);		// 
					break;
					case CLEAR_TAG:
						if($param1==123456 )$statusPacketBytes = array(PT_COMMANDS,CLEAR_TAG,$param1); // protection 
							// 
					break;
					case SET_COLOR:	//autotune AK
						 if ($param1 < 3000 ) $param1 = 3000;
						 if ($param1 > 5000 ) $param1 = 5000;
						$hicolor = UDP::getHiByte($param1);
						$locolor = UDP::getLoByte($param1);
						$statusPacketBytes = array(PT_COMMANDS,SET_COLOR,$hicolor,$locolor);// set fixture  papr 30Watt par2 pwm/10
					break;

				}
				//$statusPacketBytes = array(25,183,7,0,0);		// run test 7 
				//$statusPacketBytes = array(25,183,8,11);			// run test 8 	
				//$statusPacketBytes = array(PT_COMMANDS,179);		// verify last line command sent 
				
				$statusPacket->numberOfBytes = sizeof($statusPacketBytes);
				$statusPacket->commandParam = $statusPacketBytes;

				//echo $statusPacket->getDecimal()."\n\n";
				$status = $udp->broadcast($broadcastIp, $statusPacket, NODE_LISTENER_PORT);
				usleep(1000);
			}
			$returnVal["status"] = 1;

        } 
        catch (Exception $exc) {
            $returnVal["error"] = $exc->getMessage();
        }
        die(json_encode($returnVal));
    }
    
		/***********
	Control send  sensor setting 
	***********/
	public function sendControl(){
        $db = new DBC();

		$type = isset($_REQUEST["t"]) ? $_REQUEST["t"] :0;	//1 = all 3=cluster ID 
		$clusterID = isset($_REQUEST["cluster"]) ? $_REQUEST["cluster"] :0;
		$tagD = isset($_REQUEST["tag"]) ? $_REQUEST["tag"] :0;
		
		$cmd = $_REQUEST["command"];isset($_REQUEST["command"]) ? $_REQUEST["command"] :0;
        $param1 =isset($_REQUEST["p1"]) ? $_REQUEST["p1"] :0;
		$param2 =isset($_REQUEST["p2"]) ? $_REQUEST["p2"] :0;
		$param3 =isset($_REQUEST["p3"]) ? $_REQUEST["p3"] :0;
		//echo"Cluster ".$clusterID."Comd ".$cmd." p1 ".$param1." p2 ".$param2." p3 ".$param3."\n";
		//--------------- save data to db first before sending it out . 
		if($cmd == 12 ){ // only save data if command is set sensor param 
			$sql ="SELECT ID FROM pmi_sensor_setting WHERE ClusterID = $clusterID AND tagID = $tagD";
			$existRec = $db->get_result_array($sql);
			
			$dbrec = array();
			$where = array ();
			$dbrec["ClusterID"] = $clusterID; 
			$dbrec["tagID"] = $tagD; 
			$dbrec["Param1"] = $param1;
			$dbrec["Param2"] = $param2;
			$dbrec["Param3"] = $param3;
			
			if (sizeof($existRec)>0){//modify
			
				$where["clusterID"]= $clusterID;
				$where ["tagID"]=$tagD;
				$dbrec["ModifiedBy"] = $_SESSION['LOGGED_USER_ID'];
				$dbrec["ModiFiedOn"] = date("Y-m-d H:m:s");
				
				 $status = $db->update_query ($dbrec, "pmi_sensor_setting", $where);
			}
			else{//creaaaate 
			
				$dbrec["CreatedBy"] = $_SESSION['LOGGED_USER_ID'];
				$dbrec["CreatedOn"] = date("Y-m-d H:m:s");
				$status = $db->insert_query($dbrec, "pmi_sensor_setting");
			}
		}
			
        $returnVal["status"] = 0;
        try {
			$broadcastIp = shell_exec("ifconfig | awk '/broadcast / {print $6}'");

			//UDP Helper Class
			$pkid = rand(50, 255); // Get Incremental Packet ID - Mandatory
			for ($j=0 ; $j<10;$j++){
				$udp = new \UDP();
				$statusPacket = new \PMIPacket();

				$statusPacket->preamble = 255; // Optional
				$statusPacket->startByte = 85; // Optional
				$statusPacket->packetId = $pkid;
				/*$statusPacket->tagIdHighByte = 255;
				$statusPacket->tagIdLowByte = 255;
				if($type == 3 )$clusters = array ($clusterID,0,0,0,0,0,0,0,0,0);
				else $clusters = array (255,0,0,0,0,0,0,0,0,0);
				$statusPacket->cluster = $clusters;*/
				
				if($type == 1){// All
					$statusPacket->tagIdHighByte = 255;
					$statusPacket->tagIdLowByte = 255;
					$clusters = array (255,0,0,0,0,0,0,0,0,0);
					$statusPacket->cluster = $clusters;
				}
				else  {
					if ($tagD>0){ //Fixture
						
						$statusPacket->tagIdHighByte = $udp->getHiByte($tagD);
						$statusPacket->tagIdLowByte =  $udp->getLoByte($tagD);
						$clusters = array (255,0,0,0,0,0,0,0,0,0);
						$statusPacket->cluster = $clusters;
					}else { //cluster
						$statusPacket->tagIdHighByte = 255;
						$statusPacket->tagIdLowByte = 255;
						$clusters = array ($clusterID,0,0,0,0,0,0,0,0,0);
						$statusPacket->cluster = $clusters;

					}					
					
					
				}
				switch ($cmd){
					case ON_BUTTON:
						$statusPacketBytes = array(PT_COMMANDS,ON_BUTTON,100,255,100);		// verify last line command sent 
					break;
					case CANCEL_BUTTON:
						$statusPacketBytes = array(PT_COMMANDS,CANCEL_BUTTON,0,255,0);	// set fixture  papr 30Watt par2 pwm/10
					break;
					case 111:
						$statusPacketBytes = array(PT_COMMANDS,LIGHT_DIM,3,$param1,22,$param1);		// set def dim to 100% for 1 minute  then go back.
					break;
					case 1:
						$statusPacketBytes = array(PT_COMMANDS,OVERRIDE_1);		// 
					break;
					case 2:
						$statusPacketBytes = array(PT_COMMANDS,OVERRIDE_2);		// 
					break;
					case 3:
						$statusPacketBytes = array(PT_COMMANDS,OVERRIDE_3);		// 
					break;
					//-----sensor setting 
					case 12:
						$statusPacketBytes = array(PT_COMMANDS,SET_DEF_DIM,$param1,$param2,$param3);		// set def dim to 100% for 1 minute  then go back.
					break;
					case 13:
						$statusPacketBytes = array(PT_COMMANDS,LIGHT_FLASH);		// set def dim to 100% for 1 minute  then go back.
					break;
					
					case 186:
					if ($param1 < 3000 ) $param1 = 3000;
						 if ($param1 > 5000 ) $param1 = 5000;
						$hicolor = UDP::getHiByte($param1);
						$locolor = UDP::getLoByte($param1);
						$statusPacketBytes = array(PT_COMMANDS,SET_COLOR,$hicolor,$locolor);//
							// 
					break;

				}
				//$statusPacketBytes = array(25,183,7,0,0);		// run test 7 
				//$statusPacketBytes = array(25,183,8,11);			// run test 8 	
				//$statusPacketBytes = array(PT_COMMANDS,179);		// verify last line command sent 
				
				$statusPacket->numberOfBytes = sizeof($statusPacketBytes);
				$statusPacket->commandParam = $statusPacketBytes;

				//echo $statusPacket->getDecimal()."\n\n";
				$status = $udp->broadcast($broadcastIp, $statusPacket, NODE_LISTENER_PORT);
				usleep(1000);
			}
			$returnVal["status"] = 1;

        } 
        catch (Exception $exc) {
            $returnVal["error"] = $exc->getMessage();
        }
        die(json_encode($returnVal));
    }
	/////////////////////////shade controller //////////////////
	
		/***********
	Control send  sensor setting 
	***********/
	function socket_write_all($sock, $data) {
		$len=strlen($data);
		print_r($len);
		while($len>0){
			$written=socket_write($sock,$data);
			if($written===false){
				throw new RuntimeException('socket_write failed. errno: '.socket_last_error($sock).'. error: '.socket_strerror(socket_last_error($sock)));
			}
			$len-=$written;
			$data=substr($data,$written);
		}
		return;//all data written
	}
	public function shadeControl(){
     
		$type = isset($_REQUEST["t"]) ? $_REQUEST["t"] :0;	//1 = all 3=cluster ID 
		$clusterID = isset($_REQUEST["cluster"]) ? $_REQUEST["cluster"] :0;
		$tagD = isset($_REQUEST["tag"]) ? $_REQUEST["tag"] :0;
		
		$cmd = $_REQUEST["command"];isset($_REQUEST["command"]) ? $_REQUEST["command"] :0;
        $param1 =isset($_REQUEST["p1"]) ? $_REQUEST["p1"] :0;
		$param2 =isset($_REQUEST["p2"]) ? $_REQUEST["p2"] :0;
		$param3 =isset($_REQUEST["p3"]) ? $_REQUEST["p3"] :0;
		//echo"Cluster ".$clusterID."Comd ".$cmd." p1 ".$param1." p2 ".$param2." p3 ".$param3."\n";

        $returnVal["status"] = 0;
        try {
			switch ($cmd){
				
				case 1:
				$data = array("method" => "sdn.move.up","params" => array("targetID" => "*"),"id" => 3);
					
				break;
				
				case 2:
				$data = array("method" => "sdn.move.down","params" => array("targetID" => "*"),"id" => 3);
					
				break;
				
				case 3:
				$data = array("method" => "sdn.move.down","params" => array("targetID" => "*"),"id" => 3);
					
				break;
				
				case 4:
				$data = array("method" => "sdn.move.stop","params" => array("targetID" => "*"),"id" => 3);
					
				break;
			

			}
			
			$data_json = json_encode($data);

			$sock=socket_create(AF_INET,SOCK_STREAM,SOL_TCP);
			socket_set_option($sock,SOL_SOCKET, SO_RCVTIMEO, array("sec"=>3, "usec"=>0));
			socket_connect($sock,'192.168.1.8',44100);
			//print_r($data_json);
			socket_write_all($sock,$data_json);//???
			//echo "\n\n";
			$read_total='';
			$i=0;
			if( false!==($read_now=socket_read($sock,999,PHP_BINARY_READ))){
			//	echo "READ $i ".$read_now."\n";
				$read_total.=$read_now;
			}
				
				$arr=json_decode($read_total);
			//	print_r($arr);
			echo $arr->id.",".$arr->result."\n";
			socket_close($sock);
							
			$returnVal["status"] = 1;
        } 
        catch (Exception $exc) {
            $returnVal["error"] = $exc->getMessage();
        }
        die(json_encode($returnVal));
    }
	
	
	
    
	/**************************
		Control send  sensor setting 
	***********/
	public function sendHardwarePolicy(){
        $db = new DBC();

		$type = isset($_REQUEST["t"]) ? $_REQUEST["t"] :0;	//1 = all 3=cluster ID 
		$clusterID = isset($_REQUEST["cluster"]) ? $_REQUEST["cluster"] :0;
		$tagD = isset($_REQUEST["tag"]) ? $_REQUEST["tag"] :0;
		
		$cmd = $_REQUEST["command"];isset($_REQUEST["command"]) ? $_REQUEST["command"] :0;
        $param1 =isset($_REQUEST["p1"]) ? $_REQUEST["p1"] :0;
		$param2 =isset($_REQUEST["p2"]) ? $_REQUEST["p2"] :0;
		$param3 =isset($_REQUEST["p3"]) ? $_REQUEST["p3"] :0;
		$event =isset($_REQUEST["eventID"]) ? $_REQUEST["eventID"] :0;
		//echo"Cluster:".$clusterID." Comd:".$cmd." p1: ".$param1." p2: ".$param2." p3: ".$param3." EventID: ".$event ." Tag D: ".$tagD ."\n";

		//--------------- save data to db first before sending it out . 
		$sql ="SELECT ID FROM pmi_sensor_setting WHERE ClusterID = $clusterID AND tagID = $tagD AND EventID = $event";
		$existRec = $db->get_result_array($sql);
		
		$dbrec = array();
		$where = array ();
		$dbrec["ClusterID"] = $clusterID; 
		$dbrec["tagID"] = $tagD; 
		$dbrec["Param1"] = $param1;
		$dbrec["Param2"] = $param2;
		$dbrec["Param3"] = $param3;
		$dbrec["EventID"] = $event;
		
		if (sizeof($existRec)>0){//modify
		
			$where["clusterID"]= $clusterID;
			$where ["tagID"]=$tagD;
			$dbrec["ModifiedBy"] = $_SESSION['LOGGED_USER_ID'];
			$dbrec["ModiFiedOn"] = date("Y-m-d H:i:s");
			
			 $status = $db->update_query ($dbrec, "pmi_sensor_setting", $where);
		}
		else{//creaaaate 
		
			$dbrec["CreatedBy"] = $_SESSION['LOGGED_USER_ID'];
			$dbrec["CreatedOn"] = date("Y-m-d H:i:s");
			$status = $db->insert_query($dbrec, "pmi_sensor_setting");
		}
		//--------------------------------------------------------------------
        $returnVal["status"] = 0;
        try {
			$broadcastIp = shell_exec("ifconfig | awk '/broadcast / {print $6}'");

			//UDP Helper Class
			$pkid = rand(50, 255); // Get Incremental Packet ID - Mandatory
			for ($j=0 ; $j<10;$j++){
				$udp = new \UDP();
				$statusPacket = new \PMIPacket();

				$statusPacket->preamble = 255; // Optional
				$statusPacket->startByte = 85; // Optional
				$statusPacket->packetId = $pkid;
				if($type == 1){// All
					$statusPacket->tagIdHighByte = 255;
					$statusPacket->tagIdLowByte = 255;
					$clusters = array (255,0,0,0,0,0,0,0,0,0);
					$statusPacket->cluster = $clusters;
				}
				else  {
					if ($tagD>0){ //Fixture
						
						$statusPacket->tagIdHighByte = $udp->getHiByte($tagD);
						$statusPacket->tagIdLowByte =  $udp->getLoByte($tagD);
						$clusters = array (255,0,0,0,0,0,0,0,0,0);
						$statusPacket->cluster = $clusters;
					}else { //cluster
						$statusPacket->tagIdHighByte = 255;
						$statusPacket->tagIdLowByte = 255;
						$clusters = array ($clusterID,0,0,0,0,0,0,0,0,0);
						$statusPacket->cluster = $clusters;

					}					
				}
				switch ($event){
					case SET_DEF_DIM:
						$statusPacketBytes = array(PT_COMMANDS,SET_DEF_DIM,$param1,$param2,$param3);		// set def 
					break;
					
					case SET_SC1_POL:
					$statusPacketBytes = array(PT_COMMANDS,SET_SC1_POL,$param1,$param2,$param3);		// set def 
					break;
					
					case SET_SC2_POL:
					$statusPacketBytes = array(PT_COMMANDS,SET_SC2_POL,$param1,$param2,$param3);		// set def 
					break;
					
					case SET_SC3_POL:
					$statusPacketBytes = array(PT_COMMANDS,SET_SC3_POL,$param1,$param2,$param3);		// set def 
					break;

				}

				$statusPacket->numberOfBytes = sizeof($statusPacketBytes);
				$statusPacket->commandParam = $statusPacketBytes;

				//echo $statusPacket->getDecimal()."\n\n";
				$status = $udp->broadcast($broadcastIp, $statusPacket, NODE_LISTENER_PORT);
				usleep(1000);
			}
			$returnVal["status"] = 1;

        }  
        catch (Exception $exc) {
            $returnVal["error"] = $exc->getMessage();
        }
        die(json_encode($returnVal));
    }
	
	public function getSensorData(){
        $db = new DBC();
      
        try {
			
			$result["status"] = 0;  
            $clusterID = 0;
            if(isset($_REQUEST['clusterID'])) $clusterID = $_REQUEST['clusterID'];
			if(isset($_REQUEST['tag']))  $tagD = $_REQUEST['tag'];
			if(isset($_REQUEST['evnt']))  $evID = $_REQUEST['evnt'];
			//echo "cluster! ID ".$clusterID;
            $query = "SELECT Param1,Param2,Param3,ModifiedOn,CreatedOn FROM pmi_sensor_setting WHERE ClusterID = $clusterID AND tagID= $tagD AND EventID = $evID";
            $data = $db->get_result($query);
            //print_r($data);
            if(sizeof($data) > 0){
                $result["status"] = 1;
                $result["data"] = $data;
            }
			else {
				$result["status"] = 2;
			}

		}
        catch (Exception $exc) {
            echo $exc->getMessage();
        }
        die(json_encode($result));
    }
	
    /******************************
     *  Retry failed policy
     ******************************/
    public function retryPolicy(){
        $db = new DBC();
        $result = array();
        $result["status"] = 0;
        $dbID = $_REQUEST["dbID"];
        $sql = "SELECT * FROM pmi_device_alerts WHERE ID=$dbID";
        $alertData = $db->get_result($sql);
        //print_r($alertData);
        //die;
        if(sizeof($alertData) > 0){
            $alertType = $alertData[0]["AlertType"];
            $typeAction = $alertData[0]["TypeAction"];
            
            if($alertType == 2){
                $policyType = $alertData[0]["PolicyType"];
                $tagID = $alertData[0]["DeviceID"];
                $policyId = $alertData[0]["TypeDBID"];
                $override =0;
                $sourceType =0;
                
                $data = array();
                
                $UpdateArr = array();
                $where = array();
                $UpdateArr["Resolved"] = 1;
                $where["ID"] = $dbID;
                $ResolveStatus = $db->update_query($UpdateArr, "pmi_device_alerts", $where);
                
                if($policyType == 3){
                    $sql ="SELECT PolicyID,SourceType FROM pmi_override WHERE ID=$policyId";
                    $overrideArr = $db->get_result($sql);
                    //print_r($overrideArr);
                    if(sizeof($overrideArr) > 0){
                        $override = $policyId;
                        $policyId = $overrideArr[0]["PolicyID"];
                        $sourceType= $overrideArr[0]["SourceType"];
                    }
                }
                if($ResolveStatus){
                    
                    $dbrec = array();
                    $dbrec["Type"] = 2; // cluster
                    $dbrec["TypeID"] = $tagID;
                    $dbrec["CreatedBy"] = $_SESSION['LOGGED_USER_ID'];
                    $dbrec["CreatedOn"] = date("Y-m-d H:m:s");
                    $status = $db->insert_query($dbrec, "pmi_merge_request");
                    $packeReqID = $db->get_insert_id();

                    
                    if($typeAction == 1){
                         $data = array(
                            "ID" => $tagID,
                            "Type" => 2, //1:Cluster;2 Node;
                            "Action" => 3,//remove Policy
                            "PolicyID" => $policyId,
                            "requestedID" => $packeReqID,
                            "OverrideID" => $override,
                            "SourceType" => $sourceType
                        );
                    }
                    else if($typeAction == 2){
                        $data = array(
                            "ID" => $tagID,
                            "Type" => 2, //1:Cluster;2 Node;
                            "Action" => 2,//remove Policy
                            "PolicyID" => $policyId,
                            "requestedID" => $packeReqID,
                            "OverrideID" => $override,
                            "SourceType" => $sourceType
                        );
                    }
                    
                    $notQue = new UDPQueueClient(NOTIF_QUEUE_SERVER,SYSTEMLOG_QUEUE_NAME);
                    $notQue->queueNotificationRequest(POLICY_PROCESS, $data);

                    $notQue->queueNotificationRequest(POLICY_MERGE_CHECK_PROCESS, array("requestedID"=>$packeReqID),POLICY_CHECK_DELAY);
                    
                    $result["status"] = 1;
                }
            }
        }
        
        die(json_encode($result));
    }
    
    	public function getLocalIPAddr() {
        $db = new DBC();
        
        try {
            $result["status"] = 0;
			$db->excSql("TRUNCATE TABLE pmi_local_ip");
			$localIPs = array();
			$localIPs = shell_exec("ifconfig | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' | grep -v '127.0.0.1'");
			//print_r($localIPs);
			$localIP = explode("\n", $localIPs);
			//print_r($localIP);
			
			for ($i = 0 ; $i < sizeof($localIP);$i++)
			{
				$dbrec = array();
				$dbrec["IPAddr"] = $localIP[$i];
				$dbrec["CreatedBy"] = $_SESSION['LOGGED_USER_ID'];
				$dbrec["CreatedOn"] = date("Y-m-d H:m:s");
				$status = $db->insert_query($dbrec, "pmi_local_ip");
				//echo "IP$i: ".$localIP[$i]."\n";
			}
			if ($status) $result["status"] = 1;
			
            die(json_encode($result));
        } catch (Exception $exc) {
            throw $exc->getMessage();
        }
    }
    /*****************************************
    *  GET ALL CLUSTER BY TAGID
    ****************************************/
    public function getAllClusterByTagID() {
        $db = new DBC();
        $tagID = isset($_REQUEST["tagID"]) ? $_REQUEST["tagID"] : 0;
        try {
            $result["status"] = 0;
            $sql = <<<EOF
                SELECT DISTINCT C.ID,
                    C.Name 
                FROM 
                    pmi_cluster C
                LEFT JOIN 
                    pmi_cluster_mapping CM ON (C.ID=CM.ClusterID) 
                LEFT JOIN 
                    pmi_tag T ON (T.NodeID = CM.NodeID) 
                WHERE 
                    T.ID=$tagID AND 
                    T.Deleted=0 AND 
                    CM.Deleted=0 AND 
                    C.Deleted=0
EOF;
            $data = $db->get_result($sql);
            if(sizeof($data) > 0){
                $result["status"] = 1;
                $result["data"] = $data;
            }
            die(json_encode($result));
        } catch (Exception $exc) {
            throw $exc->getMessage();
        }
    }
    

}
?>
