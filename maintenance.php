<?php //include_once("tpl/header.tpl.php");
include_once("tpl/dashboard-top.tpl.php"); ?>
<div class="content clearfix">

    <?php //include_once("tpl/leftTreeViewPanel.tpl.php"); ?>

    <!--div class="content-left "-->
        <div class="panel panel-transparent">
            <div class="panel-body p-b-0">

                <div class="panel panel-default panel-savings m-b-0">
                    <div class="panel-body padding-0">


                        <!-- Starts Supporting system popup Modal -->
                        <div class="modal-header p-b-10 p-t-5 text-left" style="background: #cccccc;border-top-left-radius: 3px;border-top-right-radius: 3px;">
                            <h4 class="modal-title semi-bold" style="font-size:19px;">Maintenance 
                            </h4>
                        </div>
                        <div class="modal-body p-t-15" id="supportSystemContent">

                            <div id="SupportHeader"> 
                                <div class="row m-b-10"> 
                                    <div class="col-xs-12 col-sm-3 col-md-2"> </div> 
                                    <div class="col-xs-12 col-sm-3 col-md-2  cursor sprtIcon" onclick="return $maintenance.showsettings(1);"> 
                                        <img id="addHightLightClass_1" class="icon-swing removeHighlightClass" src="img/commissioning.png" border="0" alt=""> 
                                    </div> 
                                    <div class="col-xs-12 col-sm-3 col-md-2 cursor sprtIcon" onclick="return $maintenance.showsettings(2);"> 
                                        <img id="addHightLightClass_2" class="icon-swing removeHighlightClass" src="img/restore-default.png" border="0" alt=""> 
                                    </div> 
                                    <div class="col-xs-12 col-sm-3 col-md-2 cursor sprtIcon" onclick="return $maintenance.showsettings(3);" >
                                    <!--div class="col-xs-12 col-sm-3 col-md-2 cursor sprtIcon" onclick="window.location.href='http://www.inspextor.com/app/help/index.php'"--> 									
                                        <img id="addHightLightClass_3" class="icon-swing removeHighlightClass" src="img/help-menu.png" border="0" alt=""> 
                                    </div> 
                                    <div class="col-xs-12 col-sm-3 col-md-2  cursor sprtIcon" onclick="return $maintenance.showsettings(4);"> 
                                        <img id="addHightLightClass_4" class="icon-swing removeHighlightClass" src="img/view-qa.png" border="0" alt=""> 
                                    </div> 
                                </div> 
                            </div>
                            <div id="supportContent">
                                <div class="row m-b-10">
                                    <div class="col-xs-12 col-sm-12 col-md-12 support-content-item" id="div_commissioning">
                                        <?php include_once("tpl/commissioning.tpl.php"); ?>
                                    </div>

                                    <div class="col-xs-12 col-sm-12 col-md-12 support-content-item hide" id="div_restore">
                                        <?php include_once("tpl/restore-to-default.tpl.php"); ?>
                                    </div>

                                    <div class="col-xs-12 col-sm-12 col-md-12 support-content-item hide" id="div_help">
                                        <?php include_once("tpl/help-menu.tpl.php"); ?>
                                    </div>

                                    <div class="col-xs-12 col-sm-12 col-md-12 support-content-item hide" id="div_QaA">
                                        <?php include_once("tpl/QA.tpl.php"); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <!--/div-->

</div>


<!-- END PAGE CONTENT -->
<?php include_once("tpl/footer.tpl.php"); ?>
<script type="text/javascript">
    var $__maitenancetype = "<?php echo $_GET['t']; ?>";
    var $__loggedUserRole = '<?php echo $loggedUserRoleId; ?>';
    var $__adRole = "<?php echo ADMIN_ROLE; ?>";
	var $__sadRole = "<?php echo SUPER_ADMIN_ROLE; ?>";
    $(document).ready(function () {
        $ClusterTree.init(0);
        if($__maitenancetype == "restore") $maintenance.showsettings(2);
        else if($__maitenancetype == "help") $maintenance.showsettings(3);
        else if($__maitenancetype == "QA") $maintenance.showsettings(4);
        else $maintenance.showsettings(1);
        $GraphEvn.fileUpload();
    });
    var $maintenance = {
        showsettings : function($type){
            $(".support-content-item").addClass("hide");
            $(".icon-swing").removeClass("active");
            
            if($type == 1){
                $("#div_commissioning").removeClass("hide");
                $("#addHightLightClass_1").addClass("active");
            }
            else if($type == 2){
                $("#div_restore").removeClass("hide");
                $("#addHightLightClass_2").addClass("active");
            }
            else if($type == 3){
                $("#div_help").removeClass("hide");
                $("#addHightLightClass_3").addClass("active");
            }
            else if($type == 4){
                $("#div_QaA").removeClass("hide");
                $("#addHightLightClass_4").addClass("active");
            }
        },
        restor : function(){
            $('.action-card').addClass('card-close');
            
            
            try {
                // URL for the ajax call
                var $url = 'ajax.php';

                //JSON Data to server
                var $postdata = {
                    'a': 'retoreToDefault',
                    'c': 'Settings'
                };

                //Success call back function for ajax
                var $successCB = function (data)
                {
                    if (data.component != '') {
                        $('.action-card-process').addClass('open');
						$createAlert({status: "success", title: "Successfully Queued", text: "Successfully queued retore process."});
                    }
                    else{
						$('.action-card').removeClass('card-close');
						$createAlert({status: "fail", title: "Failed", text: "Failed to restore."});
					}

                };

                //Error call back function for ajax
                var $errorCB = function () {
                    $('.action-card').removeClass('card-close');
                };

                //Complete callback function for ajax
                var $completeCB = function () {

                };

                //ajax call
                asyncPostToServer($url, $postdata, $successCB, $errorCB, $completeCB, true,true);
            }

            //catch start
            catch (ex) {
                alert(ex.message);
            }
        },
		 send : function(){
			var $Name = $("#EmailName").val();
			var $title  = $("#titleA").val();
			 var $contract = '<?php echo $_SESSION["INX_CLUD_DEV_NAME"]; ?>';
			var $CompanyName = $("#CompanyName").val();
			var $PhoneNumber = $("#PhoneNumber").val();
			var $Email = $("#FromEmail").val();
			var $Comment = $("#Comment").val();

            var $data = {
                c: 'Settings',
                a: 'sendEmail',
                Name: $Name,
				CompanyName: $CompanyName,
				PhoneNumber: $PhoneNumber,
				Email: $Email,
				contrct:$contract,
				Comment: $Comment,
				Title:$title
				
            };
            $.ajax({
                type: 'POST',
                url: 'ajax.php',
                data: $data,
                dataType: 'json',
                beforeSend: function () {
                   __showLoadingAnimation();
                },
                success: function (res) {

					if(res.status == 1){	
					// object default 
						$createAlert({status : "success",title : "Success",text :"Ticket Submitted! Someone will get back to you shortly"});
						//$("#resultContainer").html("<h5 class='text-center bold m-t-20 hint-text'> Sent Email .</h5>");
						$('#EmailName').val("");
						$('#CompanyName').val("");
						$('#PhoneNumber').val("");
						$('#FromEmail').val("");
						$('#Comment').val("");
						$("#titleA").val("");
                    }

                    else {
						//match not found 
                        $createAlert({status : "fail",title : "Failed",text :"Error Sending Email Try Again "});	
						//$("#resultContainer").html("<h5 class='text-center bold m-t-20 hint-text'> Error.</h5>");
                    }
					
                },
                complete: function () {
                    __hideLoadingAnimation();
                },
                error: function (r) {
                   __hideLoadingAnimation();
                    $createAlert({status : "fail",title : "Failed",text :r});
                }
            });
			
        }
        
        
    };
</script>