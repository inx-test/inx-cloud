<?php //include_once("tpl/header.tpl.php");
include_once("tpl/dashboard-top.tpl.php");  ?>
<!-- START PAGE CONTENT -->
<div class="content clearfix p-b-0">

    <?php include_once("tpl/leftTreeViewPanel.tpl.php"); ?>

    <div class="content-center ">
        <div class="panel panel-transparent">    
            <div class="panel-heading ">
                <div class="panel-title bold fs-16">
                    Link Policy to Cluster
                </div>
            </div>
            <div class="panel-body">
                <div class="row m-t-10">
                    <div class="col-xl-5 col-l-5 col-md-5 col-sm-12">
                        <div class="form-group" id='divCluster'>
                          <label >Cluster</label>
                          <select class=" full-width" data-init-plugin="select2" id='selCluster' onchange="return $mergePolicy.clusterIDChange(this.value);">
                          </select>
                          <span class="text-danger sp_error hide"></span>
                        </div>
                    </div>
                    
                    <div class="col-xl-7 col-l-7 col-md-7 col-sm-12">
                        <div class="form-group" id='divPolicy'>
                          <label>Policy</label>
                          <select class=" full-width" data-init-plugin="select2" id='selPolicy' multiple onchange="return selectValidation('selPolicy','policy','divPolicy');">
                          </select>
                          <span class="text-danger sp_error hide"></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <button class="btn btn-complete btn-cons bold btn-cons" onclick="return $mergePolicy.merge();">Link</button>
                    </div>
                </div>
                <div class="row m-t-10" id="clusterShelf">
                    
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="hdnClusterID" name="hdnClusterID" value="">
    <!-- MODAL CONFIRMATION ALERT -->
    <div class="modal fade slide-up disable-scroll" id="modalDelCnfirmation" tabindex="-1" role="dialog" aria-hidden="false">
        <div class="modal-dialog" style="width: 300px;">
            <div class="modal-content-wrapper">
                <div class="modal-content mdal-custom">
                    <div class="modal-body p-b-5">
                        <p class="bold fs-20 p-t-20" id="cnDelCntent">
                            Do you want to remove this policy?
                        </p>
                        <input type="hidden" id="delPolicyID" >
                    </div>
                    <div class="modal-footer text-right">
                        <button type="button" class="btn btn-danger pull-right" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-success pull-right m-r-10" onclick="$mergePolicy.remove();">OK</button>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
    </div>
    <!-- END CONFIRMATION ALERT -->
    <?php include_once("tpl/rightTreeViewPanel.tpl.php"); ?>

</div>
<!-- END PAGE CONTENT -->
<?php include_once("tpl/footer.tpl.php"); ?>
<script src="js/jquery.path.js" type="text/javascript"></script>
<script type="text/javascript">
    $mergePolicyStatus =1;
    $(document).ready(function () {
        $ClusterTree.init(0);
        $mergePolicy.getAll();
        $ClusterTree.getAllCluster("#selCluster")
    });
</script>