<?php //include_once("tpl/header.tpl.php");
include_once("tpl/dashboard-top.tpl.php"); 

 ?>
 <style>
.slidecontainer {
    width: 100%;
}

.slider {
    -webkit-appearance: none;
    width: 100%;
    height: 15px;
    border-radius: 5px;
    background: #d3d3d3;
    outline: none;
    opacity: 0.7;
    -webkit-transition: .2s;
    transition: opacity .2s;
}

.slider:hover {
    opacity: 1;
}

.slider::-webkit-slider-thumb {
    -webkit-appearance: none;
    appearance: none;
    width: 25px;
    height: 25px;
    border-radius: 50%;
    background: #4CAF50;
    cursor: pointer;
}

.slider::-moz-range-thumb {
    width: 25px;
    height: 25px;
    border-radius: 50%;
    background: #4CAF50;
    cursor: pointer;
}



 </style>
<!-- START PAGE CONTENT -->
<div class="content clearfix p-b-0">
	

        <div class="panel panel-transparent">    
            <div class="panel-heading ">
                <div class="panel-title bold fs-16">
                    Hardware Policy Setting
                </div>
            </div>
            <div class="panel-body">
			<p> Which Cluster or Fixture you want to apply this policy to? </p>
			<div class="col-md-12 text-center">
			    
               <!-- Drop down starts for device --->
					
				<select  data-init-plugin="select2" id="selDeviceType" style="width: 100px;" onchange="$clientReport.__setDevice();">
					<option value="1">All</option>
					<!--option value="2">Fixture</option-->
					<option value="3">Cluster</option>
				</select>
					
				<select data-init-plugin="select2" id="selDev" style="width: 200px;"  disabled onchange="$clientReport.__selectFix(this.value) ;">
					<option value="-1">All selected</option>
				</select>

				<select data-init-plugin="select2" id="selFix" style="width: auto;"  disabled onchange="$ClusterTree.sensor_data(this.value) ;" >
					<option value="-1">All Fixtures</option>
				</select>
					
			</div>
			    <br>
				<br>
				<div class="col-md-12 text-center">

               <!-- Drop down starts for device --->
					Which hardware you want to configure ?
				<select  data-init-plugin="select2" id="selEventType" style="width: 150px;" onchange="$clientReport.__setEventType(this.value);">
					<option value="184">OC sensor</option>
					<!--option value="210">On Button</option-->
					<!--option value="211">Cancel Button</option-->
					<option value="188">Scene1 Button</option>
					<option value="189">Scene2 Button</option>
					<option value="190">Scene3 Button</option>
				</select>

					
				</div>
				<br>
				
				<div class="row">
					what dim level would you like when hardware is triggered ?
                   <div class="""slidecontainer">
					<input type="range" min="1" max="100" value="100" class="slider" id="myRange" >
					<p> <span id="range"></span></p>
					</div>
                </div>
				<br>
				
				<div class="row">
					How long should this event last (in minutes) ?
                   <div class="""slidecontainer">
					<input type="range" min="1" max="250" value="1" class="slider" id="MyDur" >
					<p> <span id="Dur"></span></p>
					</div>
                </div>
				<br>
				
				<div class="row">
					what dim level should the Fixture go to after the duration is finished ? 
                   <div class="""slidecontainer">
					<input type="range" min="1" max="100" value="50" class="slider" id="offvalue">
					<p> <span id="off"></span></p>
					</div>
                </div>
				<br>
				<br>
				<div class="row">
                    <div class="col-md-12 text-center">
                        <button class="btn btn-complete btn-cons bold btn-cons" onclick="return $clientReport.__send(12,0);">Lock Settings</button>
                    </div>
                </div>

				<div class="row">
				
				<p1>Last Modified:</p1>
                    <span id="LastMod"></span>
                </div>
				
            </div>
        </div>
    <input type="hidden" id="hdnClusterID" name="hdnClusterID" value="">
 
</div>
<!-- END PAGE CONTENT -->
<?php include_once("tpl/footer.tpl.php"); ?>
<script src="js/jquery.path.js" type="text/javascript"></script>
<script type="text/javascript">
    $mergePolicyStatus =1;
    $(document).ready(function () {
        $ClusterTree.init(0);
    });
//------ dim level ---------------------
var slider = document.getElementById("myRange");
var output = document.getElementById("range");
output.innerHTML = slider.value;

slider.oninput = function() {
  output.innerHTML = this.value;
}
//--------vacancy ------------------------
var slider2 = document.getElementById("MyDur");
var output2 = document.getElementById("Dur");
output2.innerHTML = slider2.value;

slider2.oninput = function() {
  output2.innerHTML = this.value;
}
//-------------off value -------------------
var slider3 = document.getElementById("offvalue");
var output3 = document.getElementById("off");
output3.innerHTML = slider3.value;

slider3.oninput = function() {
  output3.innerHTML = this.value;
}



//-------------
	var $clientReport = {
		__setDevice :function (){

			var selVal = $("#selDeviceType").select2("val");

			var $options = '';
			$('#selDev').html('');   
			$('#selDev').select2('data', null);
			if( selVal == 1 ){
				$('#selDev').append('<option value="-1">All selected</option>');
				$('#selDev').select2('val', "-1");
				$("#selDev").prop("disabled", true);
				$('#selFix').append('<option value="-1">All Fixtures</option>');
				$('#selFix').select2('val', "-1");
				$("#selFix").prop("disabled", true);
			}
			else{    
				$options ="";
				$('#selDev').select2('data', null);
				$("#selDev").prop("disabled", false);
				if( selVal == 2) $ClusterTree.getAllTagGroup("#selDev"); 
				else if( selVal == 3){
					$ClusterTree.getAllCluster("#selDev");  
					//fetch data for selected cluster 
					$ClusterTree.sensor_data("#selDev");  
				}					
				$('#selDev').select2('val', "1");
			}
			

		},
		
		__selectFix :function (){
			$options ="";
				$('#selFix').select2('data', null);
				$("#selFix").prop("disabled", false);

			//$ClusterTree.getFixbyCluster("#selFix"); 
			$ClusterTree.getAllTagGroup("#selFix");	
						
			$('#selFix').select2('val', "1");

		},
		__setEventType :function (){
			$ClusterTree.sensor_data("#selDev"); 
			
		},
		__send: function ($control,$dim) {
			var $type = $("#selDeviceType").val();
			var $clusterID = $("#selDev").val();
			var $tagID = $("#selFix").select2("val");
			var $param1 = $("#myRange").val();
			var $param2 = $("#MyDur").val();
			var $param3 = $("#offvalue").val();
			var $eventT =  $("#selEventType").val();

                var $data = {
					'a': 'sendHardwarePolicy',
					'c': 'LocationSettings',
					t:$type,
					cluster: $clusterID,
					p1: $param1,
					p2: $param2,
					p3: $param3,
					tag: $tagID,
					command:$control,
					eventID:$eventT
                };

                $.ajax({
                    type: 'POST',
                    url: 'ajax.php',
                    data: $data,
                    dataType: 'json',
                    beforeSend: function () {
                        __showLoadingAnimation();
                    },
                    success: function (res) {
                        var $status = res.status;
                        if ($status == 1) {

                            $createAlert({status: "success", title: "Successfully Sent", text: "System will reboot please allow 1 minute before sending another command "});
							__hideLoadingAnimation();
                        }
                        else {
                            $createAlert({status: "fail", title: "Failed", text: " To Send Command"});
							__hideLoadingAnimation();
                        }
                    },
                    complete: function () {
                        __hideLoadingAnimation();
                    },
                    error: function (r) {
                        __hideLoadingAnimation();
                        $createAlert({status: "fail", title: "Failed", text: r});
                    }
                });

        }
	}
</script>