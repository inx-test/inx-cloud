<?php
include "config.php";
include_once("tpl/dashboard-top.tpl.php"); 
	
?>
   <div class="content">
      <!-- START PANEL -->
       <div class="panel panel-transparent">
         <div class="panel-body">
            <div class="accordion-wrap p-t-0">
                <div class="accordion" id="userProfile">
                    <a href="#" class="active">
                        <i class="fa fa-user"></i> Profile
                        <button type="button" class="close" data-dismiss="modal" onclick="location.href='launchpad.php';">×</button>
                    </a>
                    <div class="sub-nav active">
                            <div class="html about-me">

                                <!-- image preview area-->
                                        <img id="uploadPreview" style="display:none;"/>
                                        <input id="Profilefilename" type="hidden" value="">
                                    <div class="hide save-alert" id="alertBox">
                                        <p class="alert-msg"></p>
                                    </div>
                                    <?php 
                                        $date = new DateTime();

                                        $imgPath = "img/profiles/profile-".$loggedInUserId.".png";
                                        if(!file_exists($imgPath)) $imgPath = "img/profiles/no-image.jpg";
                                    ?>

                                    <div class="photo" data-placement="bottom" data-toggle="tooltip" data-trigger="hover" title="Click here to upload new profile picture.">
                                        <img src="<?php echo $imgPath; ?>?<?php echo $date->getTimestamp();?>" style="width:100%; height:100%;" id="profilePic"/>
                                        <div class="photo-overlay">
                                            <input id="fileupload" type="file" name="files[]">
                                        </div>
                                    </div>
                                    <div class="form-group  form-group-default m-b-10 m-t-10" id="divFirstName">
                                            <label>First Name</label>
                                            <input type="text" class="form-control profile-save"  id="txtFirstName" name="txtFirstName" onkeyup="removeErrorAlert('divFirstName','txtFirstName');" value='' /><span class="text-danger hide"></span>
                                    </div>

                                    <div class="form-group  form-group-default m-b-10" id="divLastName">
                                            <label>Last Name</label>
                                            <input type="text" class="form-control profile-save"  id="txtLastName" name="txtLastName" onkeyup="removeErrorAlert('divLastName','txtLastName');" value='' /><span class="text-danger hide"></span>
                                    </div>
                                    <div class="form-group  form-group-default m-b-10" id="divEmail">
                                            <label>Email</label>
                                            <input type="email" class="form-control profile-save"  id="txtUserEmail" name="txtUserEmail" onkeyup="removeErrorAlert('divEmail','txtUserEmail');" value='' /><span class="text-danger hide"></span>
                                    </div>
                                    <div class="form-group  form-group-default m-b-10" id="divContactNo">
                                            <label>Contact number</label>
                                            <input type="text" class="form-control profile-save" id="txtContact" name="txtContact" onkeyup="removeErrorAlert('divContactNo','txtContact');" value='' /><span class="text-danger hide"></span>
                                    </div>
                                    <p class='pull-right m-t-10'>
                                            <button class="btn btn-complete btn-cons m-b-10" id="btnSaveProfile" name="btnSaveProfile" onclick="return saveUserProfile();" >Save profile</button>
                                    </p>


                            </div>
                    </div>
                    <a href="#">
                        <i class="fa fa-lock"></i> Change password
                    </a>
                    <div class="sub-nav">
                            <div class="html invite">
                                    <h5>Please enter your old password to confirm. </h5>
                                    <!--<div class="hide save-alert" id="change-alertBox">
                                        <p class="alert-msg"></p>
                                    </div>-->
                                    <div class="form-group  form-group-default m-b-10" id="divOldPassword">
                                            <label>Old password</label>
                                            <input type="password" id="txtOldPassword" name="txtOldPassword" class="form-control change-pass"  onkeyup="removeErrorAlert('divOldPassword','txtOldPassword');" value='' /><span class="text-danger hide"></span>
                                    </div>
                                    <div class="form-group  form-group-default m-b-10" id="divNewPassword">
                                            <label>New password</label>
                                            <input type="password" id="txtNewPassword" name="txtNewPassword" class="form-control change-pass"  onkeyup="removeErrorAlert('divNewPassword','txtNewPassword')" value='' /><span class="text-danger hide"></span>
                                    </div>
                                    <div class="form-group  form-group-default m-b-10" id="divConfirmPassword">
                                            <label>Confirm new password</label>
                                            <input type="password" id="txtConfirmPassword" name="txtConfirmPassword" class="form-control change-pass"  onkeyup="removeErrorAlert('divConfirmPassword','txtConfirmPassword')" value='' /><span class="text-danger hide"></span>
                                    </div>
                                    <p class='pull-right m-t-10'>
                                            <button name="btnChangePassword" id="btnChangePassword" onclick="return changePassword();" class="btn btn-complete btn-cons m-b-10" >Change password</button>
                                    </p>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- END PANEL -->

        
        
</div>
<!-- END PAGE CONTENT -->

<!-- IMAGE CROP MODAL -->
<div class="modal fade stick-up in" id="imageCropModal" data-backdrop="static" data-keyboard="false" role="dialog"">
        <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header clearfix text-left">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        <i class="pg-close fs-14"></i></button>
                        <div id="image-crop-heading" class="row" style="display: block;">
                            <div class="col-sm-7 p-l-5">
                                <h5>Crop image</h5>
                            </div>
                            <div class="col-sm-4 p-l-15">
                                <h5>Crop Preview</h5>
                            </div>
                        </div>
                    </div>
                    <div class="modal-body" style="padding-left:18px !important; margin-left:12px;">
                        <div class="row image-crop-content">
                            <div id="imageCrop"></div>
                            <p>
                                <div id="thumbnail_form" style="" class="pull-left m-t-50">
                                    <form name="form" action="" method="post">
                                        <input type="hidden" name="x1" value="" id="x1" />
                                        <input type="hidden" name="y1" value="" id="y1" />
                                        <input type="hidden" name="x2" value="" id="x2" />
                                        <input type="hidden" name="y2" value="" id="y2" />
                                        <input type="hidden" name="w" value="" id="w" />
                                        <input type="hidden" name="h" value="" id="h" />
                                        <input id="cropedImage" type="hidden" value="" name="cropedImage">
                                        <input type="button" class="btn btn-success " name="save_thumb" value="Save" id="save_thumb" />
                                        <input type="button" class="btn btn-danger " name="close_thumb" value="Close" id="close_thumb" onclick="hidePreview();" />
                                    </form>
                                </div>
                            </p>
                        </div>
                    </div>			
                </div>
        </div>
</div> 
<!-- IMAGE CROP MODAL END -->
<?php

	include("tpl/footer.tpl.php"); 
	
?>


<script type="text/javascript">
    
    
    $(document).ready(function()
    {
        fileUpload();
        $('.change-pass').keypress(function(e){
          if(e.keyCode==13){
            $('#btnChangePassword').click(); 
            return false;
        }
        });
        
        $('.profile-save').keypress(function(e){
          if(e.keyCode==13){
            $('#btnSaveProfile').click(); 
            return false;
        }
        });
       
        $.ajax ({
            type:'POST',
            url:'ajax.php',
            data: { 'c':'User', 'a':'fetchUserDetails' },
            dataType:'json',
            success: function(response){
                if( response.status == 1 ){
                    $('#txtFirstName').val(response.firstname);
                    $('#txtLastName').val(response.lastname);
                    $('#txtUserEmail').val(response.email);
                    $('#txtContact').val(response.phoneno);
                }
                else if( response.status == 0 ){

                }
            },
            error: function(r){
                console.log(r);
            }
        })
    });
    
    function fileUpload(){
    // Call the fileupload widget and set some parameters
        $('#fileupload').fileupload({
            url: 'profileImage.php',
            dataType: 'json',
            done: function (e, data) {
                $.each(data.result.files, function (index, file) {
                    var imageName = file.name;
                    
                    if(file.error){
                        $createAlert({status : "fail",title : "Failed",text :file.error});
                        $('#imageCropModal').modal("hide");
                    }
                    else if(file.width <= 513){
                        var imagepath = 'img/profiles/'+file.name;
                        $('#imageCropModal').modal("hide");
                        //$("#imageCropModal").hide();
                        $("#profilePic").attr("src",imagepath);
                        $("#Profilefilename").val(file.name);
                    }
                    else{
                        //$('#userProfile').addClass("hide");
                        $('#imageCropModal').modal("show");
                        var cropHtml = '<div class="col-sm-7 m-b-20 no-padding">';
                        cropHtml += '<p class="hint-text">Crop the below image and save</p>';
                        cropHtml += '<img src="img/profiles/'+file.name+'" class="m-r-10 thumbImage" id="thumbnail" path="'+file.path+'" alt="Create Thumbnail" />';
                        cropHtml += '</div><div class="col-sm-4"><p class="hint-text">See the crop image preview here</p></div>';
                        cropHtml += '<div class="col-sm-2">';
                        cropHtml += '';
                        cropHtml += '<div class="crop-preview" style="border:1px #e5e5e5 solid; float:left; position:relative; overflow:hidden;width:100px;height:100px; ">';
                        cropHtml += '<img src="img/profiles/'+file.name+'" id="thumbnail_preview" alt="Thumbnail Preview" /></div></div>';
                        $('#imageCrop').html(cropHtml);

                        //find the image inserted above, and allow it to be cropped							
                        $('#thumbnail_form').show();

                        $('#x1').val(0);
                        $('#y1').val(0);
                        $('#x2').val(115);
                        $('#y2').val(100);
                        $('#w').val(100);
                        $('#h').val(100);
                        $('#imageCrop').find('#thumbnail').imgAreaSelect({
                            aspectRatio: '1:1',
                            onSelectChange: preview,
                            x1: 0,
                            x2: 115,
                            y1: 0,
                            y2: 100,
                            parent: '#imageCrop'
                        });
                        
                            saveCropImage('profileImg');
                       
                        
                    }
                });
            },
            progressall: function (e, data) {
                // Update the progress bar while files are being uploaded
                var progress = parseInt(data.loaded / data.total * 100, 10);
                    //$("#image-crop-heading").hide();
                    //$("#imageCropModal").show();
                     $('#imageCropModal').modal("show");
                    var loadingContent = getLoadingAnimContent();
                    $('#imageCrop').html(loadingContent);
            }
        });
    }
    
    function preview(img, selection)
    {
        //get width and height of the uploaded image.
        var current_width = $('#imageCrop').find('#thumbnail').width();
        var current_height = $('#imageCrop').find('#thumbnail').height();
        var scaleX = 100 / selection.width;
        var scaleY = 100 / selection.height;

        $('#imageCrop').find('#thumbnail_preview').css({
            width: Math.round(scaleX * current_width) + 'px',
            height: Math.round(scaleY * current_height) + 'px',
            marginLeft: '-' + Math.round(scaleX * selection.x1) + 'px',
            marginTop: '-' + Math.round(scaleY * selection.y1) + 'px'
        });

        $('#x1').val(selection.x1);
        $('#y1').val(selection.y1);
        $('#x2').val(selection.x2);
        $('#y2').val(selection.y2);
        $('#w').val(selection.width);
        $('#h').val(selection.height);
    }
    
    function hidePreview(){
        //$("#imageCropModal").hide();
        //$('#userProfile').removeClass("hide");
         $('#imageCropModal').modal("hide");
    }
    
    //Save cropped Image
    function saveCropImage(responseImageID)
    {
        //create the thumbnail
        $("#save_thumb").unbind('click');
        $('#save_thumb').click(function ()
        {
            console.log("saved crop Image");

            var imagePath = $('#thumbnail').attr('path');

            var x1 = $('#x1').val();
            var y1 = $('#y1').val();
            var x2 = $('#x2').val();
            var y2 = $('#y2').val();
            var w = $('#w').val();
            var h = $('#h').val();
            if (x1 == "" || y1 == "" || x2 == "" || y2 == "" || w == "" || h == "")
            {
                $createAlert({status : "info",title : "",text :"You must make a selection first"});
                return false;
            }
            else
            {
                //hide the selection and disable the imgareaselect plugin
                $('#imageCrop').find('#thumbnail').imgAreaSelect({disable: true, hide: true});
                
                $.ajax({
                    type: 'POST',
                    url: 'ajax.php',
                    data: { 'c':'User', 'a':'saveThumbnail', 'x1': x1, 'y1': y1, 'x2': x2, 'y2': y2, 'w': w, 'h': h, 'imagePath': imagePath },
                    dataType: 'json',
                    success: function(response){
                        var imagepath = 'img/thumbnails/'+response.croppedImage;
                         $('#imageCropModal').modal("hide");
                        //$('#imageCropModal').hide();
                        //$('#userProfile').removeClass("hide");
                        //$(".accordion .about-me .photo").css("background","url('"+imagepath+"') no-repeat scroll center center rgba(0, 0, 0, 0)");
                        $("#profilePic").attr("src",imagepath);
                        $("#Profilefilename").val(response.croppedImage);
                    },
                    error: function(r){
                        console.log(r);
                    }
                    
                });
            } 
        });
    } 
    
    
    /*******************************
    * Get Loading animation html
    ******************************/
   function getLoadingAnimContent() {
       return '<div id="LoadingAnimDiv"><h5 class="text-center m-t-20">Please wait ...</h5><p class="text-center"><br/><img alt="Progress" src="img/loading_spinner.gif" class="image-responsive-height demo-mw-50"></p></div>';
   }

    $(function(){
            Profile.load();
            $(".page-content-wrapper").attr('id','login-bg');
    });

    Profile = {
            load:function(){
                    this.links();
                    this.social();
                    this.accordion();
            },
            links:function(){
                    $('a[href="#"]').click(function(e){
                            e.preventDefault();
                    });
            },
            social:function(){
                    $('.accordion .about-me .photo .photo-overlay .plus').click(function(){
                            $('.social-link').toggleClass('active');
                            $('.about-me').toggleClass('blur');
                    });
                    $('.social-link').click(function(){
                            $(this).toggleClass('active');
                            $('.about-me').toggleClass('blur');
                    });
            },
            accordion:function(){
                    var subMenus = $('.accordion .sub-nav').hide();
                    $('.accordion > a').each(function(){
                            if($(this).hasClass('active')){
                                    $(this).next().slideDown(100);
                            }
                    });
                    $('.accordion > a').click(function(){
                            $this = $(this);
                            $target =  $this.next();
                            $this.siblings('a').removeAttr('class');
                            $this.addClass('active');
                            if(!$target.hasClass('active')){
                                    subMenus.removeClass('active').slideUp(100);
                                    $target.addClass('active').slideDown(100);
                            }
                            return false;
                    });
            }
    }
    $(function(){
            $(".content").css("min-height",($(window).height()));
            Profile.load();
            $(".page-content-wrapper").attr('id','login-bg');
    });
    
    function checkOldPassword()
    {
        var oldPassword = $.trim($('#txtOldPassword').val());
        
        
        
        $.ajax({
            type: 'POST',
            url: 'ajax.php',
            data: { 'c':'User', 'a':'checkOldPassword', 'oldPassword':oldPassword },
            dataType: 'json',
            success: function(response) {
                if( response.status == 1 ){
                    $('#txtNewPassword').focus();
                }
                else if( response.status == 0 ){
                    //alert(response.message);
                    $("#divOldPassword").addClass("has-error");
                    $("#divOldPassword span").removeClass("hide");
                    $("#divOldPassword span").html("Old password does not match.");
                    $('#txtOldPassword').focus();
                }
            },
            error: function(r){
                console.log(r);
            }
        })
    }
    


    function changePassword()
    {
        var oldPassword         = $.trim($('#txtOldPassword').val());
        var newPassword         = $.trim($('#txtNewPassword').val());
        var confirmPassword     = $.trim($('#txtConfirmPassword').val());
        var password            = newPassword.length;
        
        if(oldPassword == ""){
            $("#divOldPassword").addClass("has-error");
            $("#divOldPassword span").removeClass("hide");
            $("#divOldPassword span").html("Enter Your Old Password");
            $('#txtOldPassword').focus();
            return false;
        }
        
        checkOldPassword();
        
        if(newPassword == "")
        {
            $("#divNewPassword").addClass("has-error");
            $("#divNewPassword span").removeClass("hide");
            $("#divNewPassword span").html("Enter Your New Password");
            $('#txtNewPassword').focus();
            return false;
        }
        if(password<8)
        {
            $("#divNewPassword").addClass("has-error");
            $("#divNewPassword span").removeClass("hide");
            $("#divNewPassword span").html("New Password must contain atleast 8 characters");
            $('#txtNewPassword').focus();
            return false;
        }
        if(newPassword == oldPassword){
            $("#divNewPassword").addClass("has-error");
            $("#divNewPassword span").removeClass("hide");
            $("#divNewPassword span").html("New password and Old password are same.");
            $('#txtNewPassword').focus();
            return false;
        }
        if(confirmPassword == "")
        {
            $("#divConfirmPassword").addClass("has-error");
            $("#divConfirmPassword span").removeClass("hide");
            $("#divConfirmPassword span").html("Confirm Your New Password");
            $('#txtConfirmPassword').focus();
            return false;
        }
        if(newPassword != confirmPassword)
        {
            $("#divConfirmPassword").addClass("has-error");
            $("#divConfirmPassword span").removeClass("hide");
            $("#divConfirmPassword span").html("Confirm Password does not match with New Password");
            $('#txtConfirmPassword').focus();
            return false;
        }
        
        $.ajax ({
            type: 'POST',
            url: 'ajax.php',
            data: { 'c':'User', 'a':'saveNewPassword', 'newPassword':newPassword },
            dataType: 'json',
            beforeSend: function() {__showLoadingAnimation();},
            success: function(response) {
                if( response.status == 1 ){
                    window.location.href = "index.php";
                    $createAlert({status : "success",title : "Success",text :response.message});
                }
                else if( response.status == 0 ){
                    $createAlert({status : "fail",title : "Failed",text :response.message});
                }
            },
            complete:function (){__hideLoadingAnimation();},
            error: function(r){
                __hideLoadingAnimation();
                console.log(r);
            }
        })
    }
    
    function saveUserProfile()
    {
        var firstName   = $.trim($('#txtFirstName').val());
        var lastName    = $.trim($('#txtLastName').val());
        var email       = $.trim($('#txtUserEmail').val());
        var contactno   = $.trim($('#txtContact').val());
        
        if(firstName == ""){
            $("#divFirstName").addClass("has-error");
            $("#divFirstName span").removeClass("hide");
            $("#divFirstName span").html("Enter First Name");
            $('#txtFirstName').focus();
            return false;
        }
        
        if(lastName == ""){
            $("#divLastName").addClass("has-error");
            $("#divLastName span").removeClass("hide");
            $("#divLastName span").html("Enter Last Name");
            $('#txtLastName').focus();
            return false;
        }
        
        if(email == ""){
            $("#divEmail").addClass("has-error");
            $("#divEmail span").removeClass("hide");
            $("#divEmail span").html("Enter Email Address");
            $('#txtUserEmail').focus();
            return false;
        }
        
        if(!ValidateEmail(email))
        {
            $("#divEmail").addClass("has-error");
            $("#divEmail span").removeClass("hide");
            $("#divEmail span").html("Invalid Email Address");
            $('#txtUserEmail').focus();
            return false;
        }
            
        if(contactno == "" || !isNumber(contactno)){
            $("#divContactNo").addClass("has-error");
            $("#divContactNo span").removeClass("hide");
            $("#divContactNo span").html("Enter Contact Number");
            $('#txtContact').focus();
            return false;
        }
        
        $.ajax ({
            type: 'POST',
            url: 'ajax.php',
            data: { 'c':'User', 'a':'saveUserDetails', 'firstname':firstName, 'lastname':lastName, 'email':email, 'contact':contactno,'image': $("#Profilefilename").val() },
            dataType: 'json',
            beforeSend: function() {__showLoadingAnimation();},
            success: function(response) {
                if( response.status == 1 ){
                    $(".txt-username").html(response.name);
                    $(".thumbnail-wrapper img").attr("src","img/profiles/profile-<?php echo $_SESSION["LOGGED_USER_ID"];?>.png?"+(new Date()).getTime())
                    $createAlert({status : "success",title : "Success",text :response.message});
                }
                else if( response.status == 0 ){
                    $createAlert({status : "fail",title : "Failed",text :response.message});
                }
            },
            complete:function (){__hideLoadingAnimation();},
            error: function(r){
                __hideLoadingAnimation();
                console.log(r);
            }
        })
    } 
    
    function removeErrorAlert(divID,txtID){
        $('#'+txtID).keypress(function(e){
            if(e.keyCode!=13){
                $("#"+divID).removeClass("has-error");
                $("#"+divID+" span").addClass("hide");
                $("#"+divID+" span").html("");
            }
        });
    }
    
</script>

       