<?php
// Include Helper Class
include_once("config.php");
require_once("_classes/256.php");
require_once("_classes/DBC.php");

// Posted Response 
$postData = file_get_contents("php://input");

__log( date("F j, Y, g:i a") );

// Response to Log 
function __log($msg){
    file_put_contents( 'response-terminal.txt', $msg."\n\n", FILE_APPEND );
}

__log($postData);


// Update Record 
if( !empty($postData) ){
    // Decode data 
    $record = json_decode($postData,true);

    __log(print_r($record,true));

    if( sizeof($record) > 0 ) {
        // To avoid delay directly update the outbound log instead of queue. 
        $obLogId = $record['obLogId'];
                            
        if( $obLogId >  0 ) {

            // Read hash from request & decrypt to find instance 
            $encryptStr = $record["p"];
            $instanceName = $record["i"];
			$action = $record["a"];
            $decryptStr = decrypt( $encryptStr, PRIVATE_KEY );
            
            $sql =<<<EOD
					select PID.id,PIDD.DeviceDBName,PID.device_name,PID.inspextorID FROM pmi_inx_device PID
					LEFT JOIN  pmi_inx_device_db  PIDD ON (PIDD.DeviceID = PID.id)
					WHERE PID.inspextorID = '$instanceName' AND PID.`Password` = '$decryptStr'
EOD;
            // Connect cloud database 
            $dao = new DBC();
            $instanceDetails = $dao->get_result($sql);
            
            // Check instance 
			if(sizeof($instanceDetails)>0){
                $dbName = $instanceDetails[0]["DeviceDBName"];
                
                __log("Database: $dbName");


                // Connect to instance db. 
                $db = new DBC(DB_HOST,DB_USER,DB_PASSWD,$dbName);


                // Update Fields  
                $dataToUpdate = array();
                $dataToUpdate['Status'] = 3; // Replied 
                $dataToUpdate['Data'] = $record['result']; // Reply msg - overwrite data 

                // Where condition 
                $dataToUpdateCondition = array();
                $dataToUpdateCondition["ID"]= $obLogId;

                // Save to database
                if( $db->update_query($dataToUpdate, 'pmi_outbound_log', $dataToUpdateCondition) ){
                    echo 'Y'; // Nothing to do with caller just for debug 
                } // End if 

            } // End if

        } // End if 

    }// End if 
}

// End of story 
exit;
?>