<?php
include "config.php";
//include_once("tpl/header.tpl.php");
include_once("tpl/dashboard-top.tpl.php"); 
?>
<div class="content" style=min-height: calc(100% - 200px);>
    <?php if ($loggedUserRoleId == ADMIN_ROLE) { ?>
        <div class="r-button tooltips" data-placement="top" data-trigger="hover" data-toggle="tooltip" title="" data-original-title="Click here to add new user"> 
            <input type="checkbox" id="your-id" class="r-button-checkbox" onclick="return addNewUser(0);"> 
            <label class="r-button-trigger" for="your-id"> 
                <i class="fa fa-plus material-icons1 fa-lg"></i>
            </label> 
        </div>
    <?php } ?>
    <!-- START PANEL -->
    <div class="panel panel-transparent">
        <div class="panel-heading ">
            <div class="panel-title full-width">
                <h5 class="semi-bold bold m-l-25 m-b-0 m-t-0">
                    Manage users 
                </h5>
            </div>
        </div>
        <div class="panel-body">
            <div id="portlet-advance" class="panel panel-default  panel-savings usermanage">
                <div class="panel-body " id="userManage" style="padding:15px 5px 5px 5px;">
                    <!--User Space-->	
                </div>

                <!-- MODAL CONFIRMATION ALERT -->
                <div class="modal fade slide-up disable-scroll" id="modalCnfirmation" tabindex="-1" role="dialog" aria-hidden="false">
                    <div class="modal-dialog" style="width: 300px;">
                        <div class="modal-content-wrapper">
                            <div class="modal-content mdal-custom">
                                <div class="modal-body p-b-5">
                                    <p class="bold fs-20 p-t-20" id="cnCntent">
                                        Do you want to remove this User?
                                    </p>
                                </div>
                                <div class="modal-footer text-right">
                                    <button type="button" class="btn btn-danger btn-sm pull-right" data-dismiss="modal">Cancel</button>
                                    <button type="button" class="btn btn-success btn-sm pull-right m-r-10" onclick="removeConfirm();">OK</button>
                                </div>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                </div>
                <!-- END CONFIRMATION ALERT -->

                <input type="hidden" id="delUserId" name="delUserId" value="" />
            </div>


            <!-- Add New User Starts -->
            <div id="div-new-user-modal" class="hide">
                <div class="accordion" style="max-width:850px;">
                    <a class="active" href="#">
                        <i class="fa fa-user"></i>
                        <span id="spTitle">Enter user details</span>
                        <button type="button" class="close" onclick="closeAddUser();">&times;</button>
                    </a>
                    <div class="sub-nav active p-b-15" style="display: block;">
                        <div class="html about-me">
                            <form id="frm-new-user" name="formAddUser" id="formAddUser">
                                <div class="hide save-alert" id="alertBox">
                                    <p class="alert-msg"></p>
                                </div>
                                <div class="add-user-section">
                                    <div class="user-div">
                                        <div class="form-group form-group-default m-b-10" id="divFirstName">
                                            <label>First Name</label>
                                            <input class="form-control" type="text" value="" id="txt-user-firstname" name="firstname" onblur="return checkValidation('txt-user-firstname');" onkeyup="removeErrorAlert('divFirstName');" required>
                                            <span class="text-danger hide"></span>
                                        </div>
                                        <div class="form-group form-group-default m-b-10" id="divLastName">
                                            <label>Last Name</label>
                                            <input class="form-control" type="text" value="" id="txt-user-lastname" name="lastname" onblur="return checkValidation('txt-user-lastname');" onkeyup="removeErrorAlert('divLastName');" required>
                                            <span class="text-danger hide"></span>
                                        </div>
                                        <div class="form-group form-group-default m-b-10 m-t-10" id="divEmailID">
                                            <label>Email ID</label>
                                            <input class="form-control" type="email" value="" id="txt-user-email" name="email" onblur="return checkValidation('txt-user-email');" onkeyup="removeErrorAlert('divEmailID');" required>
                                            <span class="text-danger hide"></span>
                                        </div>
                                        <div class="form-group form-group-default m-b-10 m-t-10" id="divContactNo">
                                            <label>Contact number</label>
                                            <input class="form-control" type="tel" value="" id="txt-user-contact" name="contact" onblur="return checkValidation('txt-user-contact');" onkeyup="removeErrorAlert('divContactNo');" required>
                                            <span class="text-danger hide"></span>
                                        </div>
                                        <div class="form-group form-group-default m-b-10 m-t-10" id="divUserName">
                                            <label>Username</label>
                                            <input class="form-control" type="text" value="" id="txt-user-username" name="username" onblur="return checkValidation('txt-user-username');" onkeyup="removeErrorAlert('divUserName');" required>
                                            <span class="text-danger hide"></span>
                                        </div>
                                        <div class="form-group form-group-default m-b-10 m-t-10" id="divPassword">
                                            <label>Password</label>
                                            <input class="form-control" type="password" value="" id="txt-user-password" name="password" onblur="return checkValidation('txt-user-password');" onkeyup="removeErrorAlert('divPassword');" required>
                                            <span class="text-danger hide"></span>
                                        </div>
                                    </div>
                                    <div class="role-div">
                                        <div class="form-group form-group-default" id="divRole">
                                            <label>User Role(s)</label>
                                            <select id="role-select" placeholder="Select Role(s)" data-init-plugin="select2" name='userrole' style="width:325px;" multiple="multiple" onchange="return roleValidation();" required>

                                            </select>
                                            <span class="text-danger hide"></span>
                                        </div>
                                    </div>
                                    <input type="hidden" id="hdnUserID" value="">
                                </div>
                                <div class="p-t-10">
                                    <button type="button" class="btn btn-danger pull-right" onclick="closeAddUser();">Close</button>
                                    <button onclick="SaveUser();" id="btnSaveUser" type="button" class="btn btn-success pull-right m-r-10">Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Add New User Ends -->
        </div>
    </div>
    <!-- END PANEL -->
</div>
<!-- END PAGE CONTENT -->
<?php include_once("tpl/footer.tpl.php"); ?>

<!--script src="js/jquery.min.js" type="text/javascript"></script-->


<script type="text/javascript">
    var loggedUserId = '<?php echo $loggedInUserId; ?>';
    var loggedUserRole = '<?php echo $loggedUserRoleId; ?>';
    var allRolesId = new Array();
    var allRolesName = new Array();
    $(document).ready(function ()
    {
       // $(".content").css("min-height", ($(window).height()));
        loadUser();

        $('.form-control').keypress(function (e) {
            if (e.keyCode == 13) {
                $('#btnSaveUser').click();
                return false;
            }
        });

        $.ajax({
            type: 'POST',
            url: 'ajax.php',
            data: {'c': 'User', 'a': 'fetchAllUserRole'},
            dataType: 'json',
            success: function (response) {
                if (response.status == 1) {
                    for (i = 0; i < response.userrole.length; i++) {
                        allRolesId.push(response.userrole[i]['id']);
                        allRolesName.push(response.userrole[i]['role_name']);
                    }
                    //alert(allRolesId+"---"+allRolesName);
                }
                else if (response.status == 0) {

                }
            },
            error: function (r) {
                console.log(r);
            }
        });

    });

    $('html').on('click', function (e) {
        if (typeof $(e.target).data('original-title') == 'undefined' &&
                !$(e.target).parents().is('.popover.in')) {
            $('.popover').popover('hide');
        }
    });

    function loadUser() {
        $.ajax({
            type: 'POST',
            url: 'ajax.php',
            data: {'c': 'User', 'a': 'fetchAllUser'},
            dataType: 'json',
            success: function (response) {
                if (response.status == 1)
                {
                    var id = '';
                    var name = '';
                    var role = '';
                    var email = '';
                    var active = '';
                    var enableClass = '';
                    var enableTitle = '';
                    var imgPath = '';
                    var userHTML = '';
                    var count = 1;
                    var userArr = response.userInfo;
                    var imagePathArr = response.imagePath;
                    var userRoleArr = response.userRole;
                    var d = new Date();

                    $("#userManage").html("");

                    for (var i = 0; i < userArr.length; i++)
                    {
                        id = userArr[i]['id'];
                        name = userArr[i]['firstname'] + " " + userArr[i]['lastname'];
                        email = userArr[i]['email'];
                        active = userArr[i]['active'];
                        imgPath = imagePathArr[i];
                        role = userRoleArr[i];

                        if (active == 1) {
                            enableClass = 'fa fa-check tooltips enable-user-class';
                            enableTitle = 'Disable User';
                        }
                        else {
                            enableClass = 'fa fa-times tooltips disable-user-class';
                            enableTitle = 'Enable User';
                        }

                        if (id != loggedUserId)
                        {
                            var mod = count % 3;
                            if (mod != 0) {
                                userHTML += '<div class="col-sm-4 p-r-5 m-b-15" id="userProfile_' + id + '">';
                            }
                            else {
                                userHTML += '<div class="col-sm-4 m-b-15" id="userProfile_' + id + '">';
                            }
                            userHTML += '<div class="user-card padding-20"><input type="hidden" id="hdnCompanyUserID_4" value="">';
                            userHTML += '<div class="col-sm-2"><img src="' + imgPath + '?' + d.getTime() + '" class="imgWrap thumbnail-wrapper d48 circular m-r-10" alt="img"></div>';
                            userHTML += '<div class="col-sm-10"><span>' + name + '<span class="p-l-5 bold">(' + role + ')</span><br> <small>' + email + '</small></span>';
                            if (loggedUserRole == 1) {
                                userHTML += '<span class="pull-right"><span class="accept cursor" style=""><a class="' + enableClass + '" data-placement="left" data-toggle="tooltip" title="' + enableTitle + '" id="acceptedInv_4" userstatus="' + active + '" onclick="return changeUserStatus(' + id + ');"></a></span>';
                                userHTML += '<a class="fa fa-trash removeUser  p-r-15 text-danger curPoint cursor" data-placement="left" data-toggle="tooltip" title="Remove User" dbid="0" id="projectUser_4" onclick="return removeUser(' + id + ');"></a>';
                                userHTML += '<a class="fa fa-pencil cursor" data-placement="left" data-toggle="tooltip" title="Edit User" id="rolePopover_' + id + '" onclick="showUserEdit(' + id + ');"></a>';
                                userHTML += '</span>';
                            }
                            userHTML += '</div></div></div>';
                            count++;
                        }

                    }

                    $("#userManage").append(userHTML);
                    $('[data-toggle="tooltip"]').tooltip();
                }
                else if (response.status == 0) {

                }
            },
            error: function (r) {
                console.log(r);
            }
        });
    }

    function addNewUser()
    {
        var roleHTML = '';
        $(".form-group").removeClass("has-error");
        $(".form-group .text-danger").addClass("hide");
        $('#txt-user-firstname').val("");
        $('#txt-user-firstname').focus();
        $('#txt-user-lastname').val("");
        $('#txt-user-email').val("");
        $('#txt-user-contact').val("");
        $('#txt-user-username').val("");
        $('#txt-user-password').val("");
        $("#role-select").select2().select2('val', '');
        $('.usermanage').hide();
        $('#div-new-user-modal').removeClass("hide");
        $('#btnAddUser').hide();
        $("#alertBox").addClass("hide");
        $("#role-select").html("");
        $("#hdnUserID").val("");
        $("#spTitle").html("Enter user details");

        for (var i = 0; i < allRolesId.length; i++) {
            roleHTML += '<option value="' + allRolesId[i] + '">' + allRolesName[i] + '</option>';
        }
        $("#role-select").append(roleHTML);
    }

    function closeAddUser()
    {
        $('#txt-user-firstname').val("");
        $('#txt-user-lastname').val("");
        $('#txt-user-email').val("");
        $('#txt-user-contact').val("");
        $('#txt-user-username').val("");
        $('#txt-user-password').val("");
        $("#alertBox p").html("");
        $('.usermanage').show();
        $('#div-new-user-modal').addClass("hide");
        $('#btnAddUser').show();
    }

    function SaveUser()
    {
        var firstname = $('#txt-user-firstname').val();
        var lastname = $('#txt-user-lastname').val();
        var email = $('#txt-user-email').val();
        var contactno = $('#txt-user-contact').val();
        var username = $('#txt-user-username').val();
        var password = $('#txt-user-password').val();
        var userrole = $('#role-select').select2().val();
        var userID = $("#hdnUserID").val();
        if (firstname == "")
        {
            $("#divFirstName").addClass("has-error");
            $("#divFirstName span").html("Please enter first name");
            $("#divFirstName span").removeClass("hide");
            $("#txt-user-firstname").focus();

            return false;
        }
        if (lastname == "")
        {
            $("#divLastName").addClass("has-error");
            $("#divLastName span").html("Please enter last name");
            $("#divLastName span").removeClass("hide");
            $("#txt-user-lastname").focus();

            return false;
        }
        if (email == "")
        {
            $("#divEmailID").addClass("has-error");
            $("#divEmailID span").html("Please enter email address");
            $("#divEmailID span").removeClass("hide");
            $("#txt-user-email").focus();
            return false;
        }
        if (!ValidateEmail(email))
        {
            $("#divEmailID").addClass("has-error");
            $("#divEmailID span").html("Invalid email address");
            $("#divEmailID span").removeClass("hide");
            $("#txt-user-email").focus();
            return false;
        }
        if (contactno == "") {
            $("#divContactNo").addClass("has-error");
            $("#divContactNo span").html("Please enter contact number");
            $("#divContactNo span").removeClass("hide");
            $("#txt-user-contact").focus();

            return false;
        }
        if (!isNumber(contactno)) {
            $("#divContactNo").addClass("has-error");
            $("#divContactNo span").html("Invalid contact number");
            $("#divContactNo span").removeClass("hide");
            $("#txt-user-contact").focus();
            return false;
        }

        if (username == "")
        {
            $("#divUserName").addClass("has-error");
            $("#divUserName span").html("Please enter a valid username");
            $("#divUserName span").removeClass("hide");
            $("#txt-user-username").focus();
            return false;
        }
        if (password == "")
        {
            $("#alertBox p").html("Please Enter Password");
            $("#alertBox").removeClass("hide");
            $('#txt-user-password').focus();
            return false;
        }
        if (password.length < 3) {
            $("#divPassword").addClass("has-error");
            $("#divPassword span").html("min 3 characters");
            $("#divPassword span").removeClass("hide");
            $("#txt-user-password").focus();
        }

        if (userrole == null) {
            $("#divRole").addClass("has-error");
            $("#divRole span").html("Please Select atleast one Role");
            $("#divRole span").removeClass("hide");
            $("#role-select").focus();
            return false;
        }
        var dataArr = {'c': 'User',
            'a': 'saveUserInfo',
            'firstname': firstname,
            'lastname': lastname,
            'email': email,
            'contact': contactno,
            'username': username,
            'password': password,
            'userrole': userrole};

        if (userID != "") {
            dataArr["a"] = 'updateUserInfo';
            dataArr["userID"] = userID;
        }

        $.ajax({
            type: 'POST',
            url: 'ajax.php',
            data: dataArr,
            dataType: 'json',
            beforeSend: function () {
                __showLoadingAnimation();
            },
            success: function (response) {
                if (response.status == 1) {
                    $createAlert({status: "success", title: "Success", text: response.message});
                    $('#div-new-user-modal').addClass("hide");
                    $('.usermanage').show();
                    $('#btnAddUser').show();
                    loadUser();
                }
                else if (response.status == 0) {
                    $createAlert({status: "fail", title: "Failed", text: response.message});
                }
            },
            complete: function () {
                __hideLoadingAnimation();
            },
            error: function (r) {
                __hideLoadingAnimation();
                console.log(r);
            }
        })
    }

    function changeUserStatus(userId)
    {
        var userStatus = $('#userProfile_' + userId + ' #acceptedInv_4').attr("userstatus");
        //alert("user: "+userId+" status: "+userStatus);
        // return false;

        $.ajax({
            type: 'POST',
            url: 'ajax.php',
            data: {'c': 'User', 'a': 'updateUserStatus', 'userstatus': userStatus, 'userid': userId},
            dataType: 'json',
            beforeSend: function () {
                __showLoadingAnimation();
            },
            success: function (response) {
                if (response.status == 1) {
                    $createAlert({status: "success", title: "Success", text: response.message});
                    loadUser();
                }
                else if (response.status == 0) {
                    $createAlert({status: "fail", title: "Failed", text: response.message});
                }
            },
            complete: function () {
                __hideLoadingAnimation();
            },
            error: function (r) {
                __hideLoadingAnimation();
                console.log(r);
            }
        })
    }

    function removeUser(removeUserId)
    {
        $("#btnCnfmYes").addClass("btn-success");
        $("#cnCntent").html("Do you wish to remove this user?");
        $("#modalCnfirmation").modal("show");
        $("#delUserId").val(removeUserId);
    }

    function removeConfirm()
    {
        var removeId = $("#delUserId").val();
        $.ajax({
            type: 'POST',
            url: 'ajax.php',
            data: {'c': 'User', 'a': 'removeUserInfo', 'userid': removeId},
            dataType: 'json',
            beforeSend: function () {
                __showLoadingAnimation();
            },
            success: function (response) {
                if (response.status == 1) {
                    $createAlert({status: "success", title: "Success", text: response.message});
                    $("#modalCnfirmation").modal("hide");
                    $("#delUserId").val("");
                    loadUser();
                }
                else if (response.status == 0) {
                    $createAlert({status: "fail", title: "Failed", text: response.message});
                    $("#modalCnfirmation").modal("hide");
                    $("#delUserId").val("");
                }
            },
            complete: function () {
                __hideLoadingAnimation();
            },
            error: function (r) {
                __hideLoadingAnimation();
                console.log(r);
            }
        })
    }

    function showUserEdit(UserId)
    {
        var userRole = new Array();
        $.ajax({
            type: 'POST',
            url: 'ajax.php',
            data: {'c': 'User', 'a': 'fetchUserData', 'userid': UserId},
            dataType: 'json',
            success: function (response) {
                if (response.status == 1) {
                    for (var i = 0; i < response.role.length; i++) {
                        userRole[i] = response.role[i]['role_id'];
                    }
                    addNewUser();
                    if (response.result.length > 0) {
                        $('#txt-user-firstname').val(response.result[0].firstname);
                        $('#txt-user-lastname').val(response.result[0].lastname);
                        $('#txt-user-email').val(response.result[0].email);
                        $('#txt-user-contact').val(response.result[0].phone);
                        $('#txt-user-username').val(response.result[0].username);
                        $('#txt-user-password').val(response.result[0].password);
                        $("#role-select").select2().select2('val', userRole);
                        $('#div-new-user-modal').removeClass("hide");
                        $('#btnAddUser').hide();
                        $("#alertBox").addClass("hide");
                        $("#hdnUserID").val(UserId);
                        $("#spTitle").html("Edit user details");
                    }
                }
            },
            error: function (r) {
                console.log(r);
            }
        });
    }

    function closeUserRolePopover(id) {
        $("#rolePopover_" + id).popover("hide");
    }

    function saveUserRole(userId)
    {
        var checkedVals = new Array();
        var i = 0;
        $('#role-list_' + userId + ' input:checkbox:checked').map(function () {
            checkedVals[i] = $(this).val();
            i++;
        });
        //alert(checkedVals);

        $.ajax({
            type: 'POST',
            url: 'ajax.php',
            data: {'c': 'User', 'a': 'updateUserRole', 'roles': checkedVals, 'userid': userId},
            dataType: 'json',
            beforeSend: function () {
                __showLoadingAnimation();
            },
            success: function (response) {
                if (response.status == 1) {
                    $createAlert({status: "success", title: "Success", text: response.message});
                    $("#rolePopover_" + response.userid).popover("hide");
                    loadUser();
                }
                else if (response.status == 0) {
                    $createAlert({status: "fail", title: "Failed", text: response.message});
                }
            },
            complete: function () {
                __hideLoadingAnimation();
            },
            error: function (r) {
                __hideLoadingAnimation();
                console.log(r);
            }
        });

    }

    function removeErrorAlert(id) {
        $("#" + id).removeClass("has-error");
        $("#" + id + " span").html("");
        $("#" + id + " span").addClass("hide");
    }

    function checkValidation($eleID) {
        if ($eleID == "txt-user-firstname") {
            var $eleVal = $("#txt-user-firstname").val();
            if ($eleVal.trim() == "") {
                $("#divFirstName").addClass("has-error");
                $("#divFirstName span").html("Please enter first name");
                $("#divFirstName span").removeClass("hide");
                $("#txt-user-firstname").focus();
            }
        }
        else if ($eleID == "txt-user-lastname") {
            var $eleVal = $("#txt-user-lastname").val();
            if ($eleVal.trim() == "") {
                $("#divLastName").addClass("has-error");
                $("#divLastName span").html("Please enter last name");
                $("#divLastName span").removeClass("hide");
                $("#txt-user-lastname").focus();
            }
        }
        else if ($eleID == "txt-user-email") {
            var $eleVal = $("#txt-user-email").val();
            if ($eleVal.trim() == "") {
                $("#divEmailID").addClass("has-error");
                $("#divEmailID span").html("Please enter email address");
                $("#divEmailID span").removeClass("hide");
                $("#txt-user-email").focus();
            }
            else if (!ValidateEmail($eleVal)) {
                $("#divEmailID").addClass("has-error");
                $("#divEmailID span").html("Invalid email address");
                $("#divEmailID span").removeClass("hide");
                $("#txt-user-email").focus();
            }
        }
        else if ($eleID == "txt-user-contact") {
            var $eleVal = $("#txt-user-contact").val();
            if ($eleVal == "" || !isNumber($eleVal)) {
                $("#divContactNo").addClass("has-error");
                $("#divContactNo span").html("Please enter contact number");
                $("#divContactNo span").removeClass("hide");
                $("#txt-user-contact").focus();
            }
            else if (!isNumber($eleVal)) {
                $("#divContactNo").addClass("has-error");
                $("#divContactNo span").html("Invalid contact number");
                $("#divContactNo span").removeClass("hide");
                $("#txt-user-contact").focus();
            }
        }
        else if ($eleID == "txt-user-username") {
            var $eleVal = $("#txt-user-username").val();
            if ($eleVal == "") {
                $("#divUserName").addClass("has-error");
                $("#divUserName span").html("Please enter a valid username");
                $("#divUserName span").removeClass("hide");
                $("#txt-user-username").focus();
            }
        }
        else if ($eleID == "txt-user-password") {
            var $eleVal = $("#txt-user-password").val();
            if ($eleVal.trim() == "") {
                $("#divPassword").addClass("has-error");
                $("#divPassword span").html("Please enter a valid password");
                $("#divPassword span").removeClass("hide");
                $("#txt-user-password").focus();
            }
            else if ($eleVal.length <= 3) {
                $("#divPassword").addClass("has-error");
                $("#divPassword span").html("min 3 characters");
                $("#divPassword span").removeClass("hide");
                $("#txt-user-password").focus();
            }
        }
    }

    function roleValidation() {
        var userrole = $('#role-select').select2().val();
        if (userrole == null) {
            $("#divRole").addClass("has-error");
            $("#divRole span").html("Please Select atleast one Role");
            $("#divRole span").removeClass("hide");
            $("#role-select").focus();
        }
        else {
            $("#divRole").removeClass("has-error");
            $("#divRole span").html("");
            $("#divRole span").addClass("hide");
        }
    }
</script>

