<?php include_once("tpl/header.tpl.php"); ?>
<div class="content clearfix">

    <?php include_once("tpl/leftTreeViewPanel.tpl.php"); ?>
    
    <div class="content-left ">
          <div class="panel panel-transparent">
              <div class="panel-body">
                    <div class="panel panel-default panel-savings hide">
                        <div class="panel-body padding-0">
                            <a href="javascript:void(0);" class="live-timeline live" onclick="return $TimeLine.triggerLive();" data-toggle="tooltip" data-placement="top" data-title="Click to track live record." data-container="body"><span>Live</span></a>
                            <div id="timeLineContainer">

                            </div>
                        </div>
                    </div>
                  
                    <div class="panel panel-default panel-savings">
                        <div class="panel-body padding-0 p-t-15">
                            <div class="col-sm-1 col-md-1 col-lg-1 col-xl-1">
                                <div class="checkbox check-primary checkbox-circle">
                                    <input type="checkbox" value="1" id="chLiveChartDev" onchange="$liveChart.changeType(1);" checked="checked">
                                    <label for="chLiveChartDev" class="bold">Node</label>
                                </div>
                            </div>
                            <div class="col-sm-11 col-md-3 col-lg-3 col-xl-3">
                                 <select class="full-width" id="selLiveNode" onchange="$liveChart.getJsonData(1);">
                                       
                                 </select>
                            </div>
                            
                            <div class="col-sm-1 col-md-1 col-lg-1 col-xl-1">
                                <div class="checkbox check-primary checkbox-circle">
                                    <input type="checkbox" value="1" id="chLiveChartCluster" onchange="$liveChart.changeType(2);">
                                    <label for="chLiveChartCluster" class="bold">Cluster</label>
                                </div>
                            </div>
                            <div class="col-sm-11 col-md-4 col-lg-4 col-xl-4">
                                 <select class="full-width" id="selLiveCluster" disabled onchange="$liveChart.getJsonData(1);">
                                       
                                 </select>
                            </div>
                            <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                 <select class="full-width" id="selLiveUnit" onchange="$liveChart.getJsonData(1);">
                                     <option value="1">Kilo walt</option>
                                     <option value="2">Temperature</option>
                                     <option value="3">Motion</option>
                                 </select>
                            </div>
                        </div>
                        <div class="full-width" id="divLiveChart">
                            <div id="div-chart-wrapper" style="background-color:#fff; border-radius:6px;min-height:300px;" class="m-t-10">
                            </div>
                        </div>
                    </div>
              </div>
          </div>
    </div>
    
</div>


<!--canvas class="hide"></canvas-->
<div id="div-all-guage-popover-content" style="display: none;">
    <script src="js/time-line/timeline.js" type="text/javascript"></script>
    <link href="js/time-line/timeline.css" rel="stylesheet" type="text/css"/>
</div>
<!-- END PAGE CONTENT -->
<?php include_once("tpl/footer.tpl.php"); ?>
<script type="text/javascript">

    $(document).ready(function () {
        $ClusterTree.init(0);  
        //$TimeLine.getEventData(1,"timeLineContainer",0,0);
        $liveChart.init();
        displayGuages();
        //__intknobBar("#__temMax",30,"#99F969");
        __intknobBar("#__temint", 80, "#F97E7A");
        __intknobBar("#__temAmb", 66, "#F7A044");
        if ("<?php echo $_SESSION["activeDeviceID"] ?>" == "0" || "<?php echo $_SESSION["activeDeviceID"] ?>" == "") {
            $('#_popDeviceList').click();
        }
        if (window.location.href.indexOf("alert") > -1) {
            var Sheight = $(document).height();
            $("body").animate({
                scrollTop: Sheight
            });
        }
        
        
    });

    function gotoChart($type) {
        //var $dtearang = $('#__dashDaterae ').val();
        //var $startdateStr = $dtearang.split("-")[0].trim();
        //var $enddateStr = $dtearang.split("-")[1].trim();
        var $startdate = new Date().getTime();
        var $enddate = new Date().getTime();
        //$dtearang = $dtearang.replace("/","-");
        var $url = __getBaseUrl()[0] + "chart.php?parameter=" + $type + "_" + $startdate + "_" + $enddate;
        window.location.replace($url);
    }

    function showAlert($alerts) {
        if (typeof ($alerts) != 'undefined') {
            var $HTMLStr = "";
            if ($alerts.length > 0) {
                var $len = $alerts.length > 3 ? 3 : $alerts.length;
                for (var i = 0; i < $len; i++) {
                    var $severity = $alerts[i].severity;
                    var $createdOn = $alerts[i].CratedOn;
                    var alertClass = $severity == "Caution" ? "bg-yellow bg-yellow-ball" : "flash bg-red bg-red-ball";
                    $HTMLStr += '<div class="nn-alert cursor" data-placement="top" data-toggle="tooltip" data-trigger="hover" title="" data-original-title="More Alerts" onclick="return showMoreAlerts(\'' + escape(JSON.stringify($alerts)) + '\');">';
                    $HTMLStr += '    <div class="notify-buble-lg pull-left p-t-5 m-r-5 ' + alertClass + '"></div>';
                    $HTMLStr += '    <strong>' + $alerts[i].parameter + ' ' + $alerts[i].severity;
                    $HTMLStr += '        <span class="fs-12 pull-right">' + $alerts[i].device_name + '</span>';
                    $HTMLStr += '    </strong>';
                    $HTMLStr += '    <p>' + $alerts[i].description;
                    $HTMLStr += '    <span class="fs-12 pull-right">' + localDateFromate($createdOn, 1) + '</span></p>';
                    $HTMLStr += '</div>';
                }
                $("#portlet-alert .panel-title").html("Alert (" + $alerts.length + ")");
                $("#__notify span").html($alerts.length);
                $("#alertContainer").html($HTMLStr);
                $("#__notify").removeClass("hide");
                $("#portlet-alert").removeClass("hide");
                $('[data-toggle="tooltip"]').tooltip();
            }
            else {
                $("#portlet-alert panel-title").html("No Alerts");
                $("#__notify").addClass("hide");
                $("#portlet-alert").addClass("hide");
            }

        }
    }

    function Sceenshot() {
        $("#btnSelguage").addClass("hide");
        $(".btn-snapshot").addClass("hide");
        html2canvas($("#snapshotArea"), {
            onrendered: function (canvas) {
                var data = canvas.toDataURL('image/png');
                $("#btnSelguage").removeClass("hide");
                $(".btn-snapshot").removeClass("hide");
                var a = document.createElement("a");
                a.download = "dashboard.png";
                a.href = data;
                a.click();
                //window.open(data);
            }
        });
    }

    function showMoreAlerts($obj) {
        $alerts = JSON.parse(unescape($obj));
        //console.log($alerts);
        if (typeof ($alerts) != 'undefined') {
            var $HTMLStr = "";
            if ($alerts.length > 0) {
                for (var i = 0; i < $alerts.length; i++) {
                    var $severity = $alerts[i].severity;
                    var $createdOn = $alerts[i].CratedOn;
                    var alertClass = $severity == "Caution" ? "bg-yellow bg-yellow-ball" : "flash bg-red bg-red-ball";
                    $HTMLStr += '<div class="nn-alert">';
                    $HTMLStr += '    <div class="notify-buble-lg pull-left p-t-5 m-r-5 ' + alertClass + '"></div>';
                    $HTMLStr += '    <strong>' + $alerts[i].parameter + ' ' + $alerts[i].severity;
                    $HTMLStr += '        <span class="fs-12 pull-right">' + $alerts[i].device_name + '</span>';
                    $HTMLStr += '    </strong>';
                    $HTMLStr += '    <p>' + $alerts[i].description;
                    $HTMLStr += '    <span class="fs-12 pull-right">' + localDateFromate($createdOn, 1) + '</span></p>';
                    $HTMLStr += '</div>';
                }
                //console.log($alerts[0].TCount);
                if ($alerts[0].TCount > $alerts.length) {
                    $HTMLStr += '<div class="nn-alert text-center cursor more-alerts" onclick="return showMore(' + $alerts[$alerts.length - 1].ID + ',10);" style="min-height:34px !important;width:231px;">';
                    $HTMLStr += '    <strong>More Alerts</strong>';
                    $HTMLStr += '</div>';
                }
                $("#moreAlertContainer").html($HTMLStr);
                $("#modalMoreAlert").modal("show");
            }
        }
    }

    function showMore($ID, $showCount) {
        $(".more-alerts").remove();
        $("#moreAlertContainer").append('<div class="loading text-center"><div class="loading-bar"></div><div class="loading-bar"></div><div class="loading-bar"></div><div class="loading-bar"></div></div>');

        $.ajax({
            type: 'POST',
            url: 'ajax.php',
            data: {'c': 'Device', 'a': 'GetAlerts', 'ID': $ID, 'showCount': $showCount},
            dataType: 'json',
            success: function (res) {
                if (res.results != null && res.results != "") {
                    $alerts = res.results;
                    if ($alerts.length > 0) {
                        $("#moreAlertContainer .loading").remove();
                        for (var i = 0; i < $alerts.length; i++) {
                            var $severity = $alerts[i].severity;
                            var $createdOn = $alerts[i].CratedOn;
                            var alertClass = $severity == "Caution" ? "bg-yellow bg-yellow-ball" : "flash bg-red bg-red-ball";
                            $HTMLStr = '<div class="nn-alert">';
                            $HTMLStr += '    <div class="notify-buble-lg pull-left p-t-5 m-r-5 ' + alertClass + '"></div>';
                            $HTMLStr += '    <strong>' + $alerts[i].parameter + ' ' + $alerts[i].severity;
                            $HTMLStr += '        <span class="fs-12 pull-right">' + $alerts[i].device_name + '</span>';
                            $HTMLStr += '    </strong>';
                            $HTMLStr += '    <p>' + $alerts[i].description;
                            $HTMLStr += '    <span class="fs-12 pull-right">' + localDateFromate($createdOn, 1) + '</span></p>';
                            $HTMLStr += '</div>';
                            $("#moreAlertContainer").append($HTMLStr);
                        }
                        //console.log($alerts[0].TCount);
                        if ($alerts[0].TCount > $alerts.length && $alerts[$alerts.length - 1].ID != 1) {
                            $HTMLStr = '<div class="nn-alert text-center cursor more-alerts" onclick="return showMore(' + $alerts[$alerts.length - 1].ID + ',10);" style="min-height:34px !important;width:231px;">';
                            $HTMLStr += '    <strong>More Alerts</strong>';
                            $HTMLStr += '</div>';
                            $("#moreAlertContainer").append($HTMLStr);
                        }
                    }
                }
                else {
                    $("#moreAlertContainer .loading").remove();
                }
            },
            error: function (r) {
                console.log(r);
                $("#moreAlertContainer .loading").remove();
            }
        });
    }

</script>

