<?php
	
	// Include Helper Class
	set_include_path(get_include_path() . PATH_SEPARATOR . 'phpseclib');
	
	require_once('config.php');
	
	
	require_once("_classes/256.php");
	require_once('vendor/autoload.php');
	include('Net/SFTP.php');
	include('Crypt/RSA.php');

	use InboundProcess\InboundQueueClient;
	use Pheanstalk\Pheanstalk;
	use Pheanstalk\PheanstalkInterface;
	
	
	include_once("_classes/UserController.php");
	include_once("_classes/DashboardController.php");
	include_once("_classes/DeviceController.php");
	include_once("_classes/ChartController.php");
	include_once("_classes/LocationController.php");
	include_once("_classes/SMTPController.php");
	include_once("_classes/parameterController.php");
	include_once("_classes/LocationEditorController.php");
	include_once("_classes/LocationSettingsController.php");
	include_once("_classes/SettingsController.php");
	include_once("_classes/TerminalController.php");
	include_once("_classes/ClientController.php");
	include_once("_classes/CloudUserController.php");
	include_once("_classes/OutboundController.php");
	
	$controller = $_REQUEST["c"]."Controller"; // Controller
	$action = $_REQUEST["a"]; // Action
	//print_r($controller);die;
	$localActions = array('ClearAlert',
							'deleteAlertInfo',
							'treeViewChangeOrder',
							'addDeviceAlert',
							'updateAlert',
							'saveSmtpConfig',
							'SaveGaugeSettings',
							'saveTreeNodeName',
							'deleteTreeNode',
							'insertNodeToCluster',
							'removePolicy',
							'insertOrUpdatePolicy',
							'insertOrUpdateATPolicy',
							'removeATPolicy',
							'mergepolicy',
							'removeMergePolicy',
							'sentToNode',
							'updateSwitchSchedule',
							'saveSwitchName',
							'clearTag',
							'sendCluster',
							'sendHardwarePolicy',
							'setPower',
							'sendControl',
							'saveUploadImage',
							'saveMapNode',
							'exeCommands',
							'retoreToDefault',
							'getLocalIPAddr',
							'insertOrUpdateOverriderPolicy',
							'removeOverridePolicy');

	$controller = new $controller();
	
	
	if( !empty($action) ){
		if( in_array($action, $localActions ) )  {

			$timestamp = time();
			if($action == 'saveUploadImage' || $action == 'updateSwitchSchedule')
			{
				if($action == 'saveUploadImage')
				{
					//$newFilename = '1509431380_docmanager (1).png';
					$newFilename = $timestamp."_".$_REQUEST['imagename'];
					$sourceFilepath = ROOTPATH."/img/location/".$_REQUEST['imagename'];
					$targetFilePath = ROOTPATH."/img/location/".$newFilename;
					rename($sourceFilepath,$targetFilePath); // rename oldfile with new timestamp file name in cloud
					chmod($targetFilePath,0755);
					$_REQUEST['imagename'] = $newFilename;
					$cloudUrl = ROOTPATH."/img/location/";
					$sourcePath = $cloudUrl.$newFilename;
					$serverPath = SERVER_URL.SERVER_LOCATION."/".$newFilename;
				}
				else
				{
					$cloudUrl = ROOTPATH."files/";
					$sourcePath = $cloudUrl.$_REQUEST['fileName'];
					$serverPath = SERVER_URL.SERVER_IMPORT."/".$_REQUEST['fileName'];
				}


				$sftp = new Net_SFTP(SERVER_DOMAIN,SERVER_PORT);
				$key = new Crypt_RSA();
				$key->setPassword(PRIVATE_KEY);
				$key->loadKey(file_get_contents('/home/inspexto/pvt/private-key-root'));
				if (!$sftp->login(SERVER_USER, $key) && !$sftp->login(SERVER_USER, SERVER_PASSWORD)) {
					die(json_encode($result));
				}

				//var_dump($sftp);

				if($sftp->put($serverPath, $sourcePath, NET_SFTP_LOCAL_FILE)){
					//echo ">>>1";
				}
			}
			
			$_REQUEST['___ts'] = $timestamp;
			$_REQUEST['isLocal'] = 1;
			$_REQUEST['loggedUserID'] = $_SESSION['LOGGED_USER_ID'];
			$_REQUEST['loggedUserRoleID'] = $_SESSION['LOGGED_USER_ROLE_ID'];
			$_REQUEST['cloudSessionInfo'] = $_SESSION;

			
			$outboundController = new OutboundController();
			$obLastInsertedId = $outboundController->saveOutboundData($_REQUEST); 	
			
			if( $obLastInsertedId > 0 ){
				
				if( !isset($_REQUEST['status']) ) $_REQUEST['status'] = 1;
				if( !isset($_REQUEST['component']) ) $_REQUEST['component'] = 1;
				$_REQUEST['cloudSessionInfo'] = '';
					
					
					 if( $action == 'sentToNode' ) {
						$start_term = microtime(true);
						$limit_term = 60;  // Seconds
						$_REQUEST['status'] = 0;
						
						while(true) {
						  // Skip further 
						  $replyRecord = $outboundController->getLogReply($obLastInsertedId); 
						  
						  
						  if( sizeof($replyRecord) > 0 ) {
							  $terminalOut  = $replyRecord[0]['Data'];
							  if( !empty($terminalOut) ){
								$_REQUEST['result'] = json_decode($terminalOut); 
								$_REQUEST['status'] = 1;
							  }
							  else $_REQUEST['result'] = '';
							  
							  // exit loop 
							  break; 
						  }
							
						  // timeout 
						  if (microtime(true) - $start_term >= $limit_term) {
							$_REQUEST['result'] = "TO";
							$_REQUEST['status'] = 0;
							break;
						  }
						  
						}
						
					} // ENd Terminal 
					echo json_encode($_REQUEST);
					exit;
			}// End save
		}
		else $controller->$action($_REQUEST);
	}
	exit;
?>
