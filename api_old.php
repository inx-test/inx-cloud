<?php
	
	
	include_once("config.php");
    require_once '_classes/DBC.php';
	
	 $a = $_REQUEST["a"];
	  if( $a == "setIP" ) {
	  // Read the IP Address
	  $result = $_REQUEST["ip"];

	  file_put_contents("files/-raw-".time()."-ip.txt",$result);



  if( !empty($result) ){
  // Instead it will save to database
  

	  
	
	$db->autocommit(false);
	try {
		//$ = file_get_contents("./files/1486360482-ip.txt");
		if (!empty($result)) {
			$devID = 0;
			$deviceName = "";
			$macAdd = "";
			$assemblyNo = "";
			$serialNo = "";
			$modelNo = "";
			$systemSerialNo = "";
			$ipAddress = "";
			$existsDevice = array();
			
			$rsSplitSectionArr = explode("*****", $result);
			
			if (isset($rsSplitSectionArr[0])) {
				// print_r($rsSplitSectionArr[0]);
				$strRemoveSpChar = str_replace('!', '', $rsSplitSectionArr[0]);
				$rslineArr = explode("\n", $strRemoveSpChar);

				foreach ($rslineArr as $key => $value) {
					if (preg_match("#^hostname(.*)$#i", $value)) {
						$splitSpace = explode(" ", trim($value));
						if (sizeof($splitSpace) > 1)
							$deviceName = trim($splitSpace[1]);
					}
				}
				//echo $deviceName;
			}



			if (isset($rsSplitSectionArr[1])) {
				$strRemoveSpChar = str_replace('!', '', $rsSplitSectionArr[1]);
				$rslineArr = explode("\n", $strRemoveSpChar);
				foreach ($rslineArr as $key => $value) {
					if (preg_match("#^Base Ethernet MAC Address(.*)$#i", $value)) {
						$splitSpace = explode(" : ", trim($value));
						if (sizeof($splitSpace) > 1)
							$macAdd = trim($splitSpace[1]);
					}
					else if (preg_match("#^Motherboard Assembly Number(.*)$#i", $value)) {
						$splitSpace = explode(":", trim($value));
						if (sizeof($splitSpace) > 1)
							$assemblyNo = trim($splitSpace[1]);
					}
					else if (preg_match("#^Motherboard Serial Number(.*)$#i", $value)) {
						$splitSpace = explode(":", trim($value));
						if (sizeof($splitSpace) > 1)
							$serialNo = trim($splitSpace[1]);
					}
					else if (preg_match("#^Model Number(.*)$#i", $value)) {
						$splitSpace = explode(":", trim($value));
						if (sizeof($splitSpace) > 1)
							$modelNo = trim($splitSpace[1]);
					}
					else if (preg_match("#^System Serial Number(.*)$#i", $value)) {
						$splitSpace = explode(":", trim($value));
						if (sizeof($splitSpace) > 1)
							$systemSerialNo = trim($splitSpace[1]);
					}
				}
				 /*echo '<pre>';
				  echo $macAdd."\n";
				  echo $assemblyNo."\n";
				  echo $serialNo."\n";
				  echo $modelNo."\n";
				  echo $systemSerialNo."\n";
				  //print_r($rslineArr);
				  echo '</pre>'; */
			}

			if (isset($rsSplitSectionArr[2])) {
				$strRemoveSpChar = str_replace('!', '', $rsSplitSectionArr[2]);
				$rslineArr = explode("\n", $strRemoveSpChar);
				foreach ($rslineArr as $key => $value) {
					if (preg_match('/\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/', $value, $ipMatch)) $ipAddress = $ipMatch[0];
				}
			}
			
			/*echo '<pre>';
			echo $ipAddress."\n";
			echo '</pre>'; */
			if (($macAdd != "" && $macAdd != NULL) && ($deviceName != "" && $deviceName != NULL)) {
				
				$sql = "SELECT id,device_name,serial_no,ip_address,mac_address,assembly_no,model_no,serial_no_sys FROM pmi_device WHERE mac_address='$macAdd' AND serial_no_sys ='$systemSerialNo'";
				$existsDevice = $db->get_result($sql);
				//print_r($existsDevice);
				//echo '<<<<<>>>>>';
				if( sizeof($existsDevice) > 0 ){
					$devID = $existsDevice[0]["id"];
					$recArr = array();
					if(urlencode ($deviceName) != $existsDevice[0]["device_name"]) $recArr["device_name"] = urlencode ($deviceName);
					if($serialNo != $existsDevice[0]["serial_no"]) $recArr["serial_no"] = $serialNo;
					if($ipAddress != $existsDevice[0]["ip_address"]) $recArr["ip_address"] = $ipAddress;
					if($macAdd != $existsDevice[0]["mac_address"]) $recArr["mac_address"] = $macAdd;
					if($assemblyNo != $existsDevice[0]["assembly_no"]) $recArr["assembly_no"] = $assemblyNo;
					if($modelNo != $existsDevice[0]["model_no"]) $recArr["model_no"] = $modelNo;
					if($systemSerialNo != $existsDevice[0]["serial_no_sys"]) $recArr["serial_no_sys"] = $systemSerialNo;
					if(count($recArr) > 0) $devUpdateStat =  $db->update_query($recArr, "pmi_device", "id = $devID");
				}
				else{
					$recArr["device_name"] = urlencode($deviceName);
					$recArr["type"] = 1;
					$recArr["serial_no"] = $serialNo;
					$recArr["ip_address"] = $ipAddress;
					$recArr["mac_address"] = $macAdd;
					$recArr["assembly_no"] = $assemblyNo;
					$recArr["model_no"] = $modelNo;
					$recArr["serial_no_sys"] = $systemSerialNo;
					$insertStat = $db->insert_query($recArr, "pmi_device");
					if($insertStat) $devID = $db->get_insert_id ();
					//echo 'Inserted ID =='.$devID."\n";
				}
			}
			if (isset($rsSplitSectionArr[3])) {
				$rsLineArr = explode("\n", $rsSplitSectionArr[3]);
				$ciscotblArr = array();
				$logsql = "INSERT INTO pmi_device_log (DeviceID, Interface, Port, AdminState, OperState, AdminPolice, OperPolice, CutoffPower, OperPower, LogDate) VALUES ";
				$logfieldsql = "";
				$logDate = gmdate('Y-m-d H:i:s');
				for ($i = 0; $i < count($rsLineArr); $i++) {
					$tabSplit = preg_split('/\s+/', $rsLineArr[$i]);
					if ($i == 5) {
						$ciscotblArr["module"] = $tabSplit[0];
						$ciscotblArr["available_watts"] = $tabSplit[1];
						$ciscotblArr["used_watts"] = $tabSplit[2];
						$ciscotblArr["remaining_watts"] = $tabSplit[3];
						//var_dump($tabSplit);
					} else if ($i > 8) {
						if ($tabSplit[0] == "Totals:") {
							//print_r($tabSplit);
							$ciscotblArr["oper_power_total"] = $tabSplit[1];
						} else if (trim($tabSplit[0], " ") != "" && $tabSplit[0] != "---------") {
							$logfieldsql .= ($logfieldsql == "" ? "" : ", ");
							//DeviceID
							$logfieldsql .= "(" . $devID;
							//Interface                           
							$logfieldsql .= ",'" . $tabSplit[0] . "' ";
							//Port
							$interface = explode("/", $tabSplit[0]);
							$logfieldsql .= "," . $interface[2];
							//AdminState
							$logfieldsql .= ",'" . $tabSplit[1] . "'";
							//OperState
							if ($tabSplit[2] == "on")
								$logfieldsql .= ",1";
							else if ($tabSplit[2] == "off")
								$logfieldsql .= ",0";
							else
								$logfieldsql .= ",3";
							//AdminPolice
							$logfieldsql .= ",'" . $tabSplit[3] . "'";
							//OperPolice
							$logfieldsql .= ",'" . $tabSplit[4] . "'";
							//CutoffPower
							$logfieldsql .= "," . ($tabSplit[5] == "n/a" ? 0 : $tabSplit[5]);
							//OperPower
							$logfieldsql .= "," . ($tabSplit[6] == "n/a" ? 0 : $tabSplit[6]);
							//LogDate
							$logfieldsql .= ",'" . $logDate . "')";
						}
					}
				}
				$db->update_query($ciscotblArr, "pmi_device", array("id" => $devID));
				if ($logfieldsql != ""){
					$db->update_query(array("Status" => 0), "pmi_device_log", array("DeviceID" => $devID));
					$db->query($logsql . $logfieldsql);
				}
				//echo $logfieldsql;
				//echo "\n\nCommiting results\n";
			}
			//echo 'here';
			$db->commit();
			// Reply with Y
			echo "Y";
		}
	} 
	catch (Exception $ex) {
		echo "N";
		$db->rollback();
		
	}
	$db->close();
  }
  else {
  // Reply with Y
  echo "N";
  }
  } 
?>
