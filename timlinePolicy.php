<?php
    // Include Helper Class
    require_once('config.php');
    require_once('_classes/DBC.php');
    require_once('vendor/autoload.php');
    
    use UDPProcess\UDPQueueClient;

    $db = new \DBC();
    $sql = "select ID from pmi_policy";
    $policyArr = $db->get_result($sql);
    
    //print_r($policyArr);
    // Initialize and start the workers
    for ($i=0; $i< sizeof($policyArr); $i++) {
        $data = array(
            "policyID" => $policyArr[$i]["ID"]
        );
        echo "\n Queue Start ... \n";
        //print_r($data);
        $notQue = new UDPQueueClient(NOTIF_QUEUE_SERVER,SYSTEMLOG_QUEUE_NAME);
        $notQue->queueNotificationRequest(POLICY_TIMELINE_PROCESS, $data);
    }
    
?>

