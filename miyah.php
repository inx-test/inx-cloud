<?php //include_once("tpl/header.tpl.php");
include_once("tpl/dashboard-top.tpl.php"); 

 ?>
<!-- START PAGE CONTENT -->
<div class="content clearfix p-b-0">

	<?php include_once("tpl/leftTreeViewPanel.tpl.php"); ?>
	
    <div class="content-center ">
        <div class="panel panel-transparent">    
            <div class="panel-heading ">
                <div class="panel-title bold fs-16">
                    Send Commands to Cluster
                </div>
            </div>
            <div class="panel-body">
			    <ul>
                                        <!-- Drop down starts for device --->
					<li>
						<select  data-init-plugin="select2" id="selDeviceType" style="width: 100px;" onchange="$clientReport.__setDevice();">
							<option value="1">All</option>
							<!--option value="2">Fixture</option-->
							<option value="3">Cluster</option>
						</select>
					</li>
					<!-- Drop down ends --->
					
					<!-- Drop down starts for device --->
					<li>
						<select data-init-plugin="select2" id="selDev" style="width: 200px;"  disabled>
							<option value="-1">All selected</option>
						</select>
					</li>
					<!-- Drop down ends --->
					
					<!-- Drop down starts for chart parameters --->
					<li>
						<select  data-init-plugin="select2" id="selCMD" style="width: 300px;" onchange="$clientReport.intBarchart();">
						  <option value="" selected="">>>>Select a Command<<< </option>
						  <option value="<?php echo RESTORE_TO_DEFAULT;?>">Restore to Def Policy</option>
						  <option value="<?php echo SET_FIXTURE_TYPE;?>">Set Fix type </option>
						  <option value="<?php echo SET_DEF_DIM;?>">Set OC Settings </option>
						  <option value="<?php echo TEST_SENSOR;?>">Test Sensor Mode ON=1 , OFF= 2  </option>
						  <option value="<?php echo SET_SENSOR_TYPE;?>">Sensor Type 11=Mic 22=PIR  33=Both </option>
						  <option value="<?php echo CLEAR_TAG;?>">Clear Tag Back End  </option>
						  <option value="<?php echo SET_COLOR;?>">SET COLOR TEMP  </option>		
						  <option value="<?php echo AUTOTUNE_ENABLE;?>">SET autotune feature 100 to enable  </option>	
						</select>			
					</li>
                                        <!-- Drop down ends --->
                </ul>

				<tr>
				  <td colspan="2">
					<p>Enter First Parameter </p>
					<input type="text" id="param1" maxlenth="20"/>
				  </td>
				</tr>
				
				<tr>
				  <td colspan="2">
					<p>Enter Second Parameter </p>
					<input type="text" id="param2" maxlenth="20" value=""/>
				  </td>
				</tr>
				
				<tr>
				  <td colspan="2">
					<p>Enter Third Parameter </p>
					<input type="text" id="param3" maxlenth="20" value=""/>
				  </td>
				</tr>
			    <div class="row">
                    <div class="col-md-12 text-center">
                        <button class="btn btn-complete btn-cons bold btn-cons" onclick="return $clientReport.__send();">Send Command ==></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="hdnClusterID" name="hdnClusterID" value="">
 
</div>
<!-- END PAGE CONTENT -->
<?php include_once("tpl/footer.tpl.php"); ?>
<script src="js/jquery.path.js" type="text/javascript"></script>
<script type="text/javascript">
    $mergePolicyStatus =1;
    $(document).ready(function () {
        $ClusterTree.init(0);
       // $mergePolicy.getAll();
		//$clientReport.init();
        //$ClusterTree.getAllCluster("#selCluster")
    });
	var $clientReport = {
		__setDevice :function (){

			var selVal = $("#selDeviceType").select2("val");

			var $options = '';
			$('#selDev').html('');   
			$('#selDev').select2('data', null);
			if( selVal == 1 ){
				$('#selDev').append('<option value="-1">All selected</option>');
				$('#selDev').select2('val', "-1");
				$("#selDev").prop("disabled", true);
			}
			else{    
				$options ="";
				$('#selDev').select2('data', null);
				$("#selDev").prop("disabled", false);
				if( selVal == 2) $ClusterTree.getAllTagGroup("#selDev"); 
				else if( selVal == 3) $ClusterTree.getAllCluster("#selDev");      
				$('#selDev').select2('val', "1");
			}
			

		},
		/*__send : function(){	
			var $type = $("#selDeviceType").val();
			var $clusterID = $("#selDev").val();
			
			var $cmd =  $("#selCMD").val();
			var $param1 = $("#param1").val();
			var $param2 = $("#param1").val();
			var $param3 = $("#param1").val();
			
			try {
				
				// URL for the ajax call
				var $url = 'ajax.php';

				//JSON Data to server
				var $postdata = {
					'a': 'sendCluster',
					'c': 'LocationSettings',
					t:$type,
					cluster: $clusterID,
					p1: $param1,
					p2: $param2,
					p3: $param3,
					command:$cmd
					
				};
				
				//Success call back function for ajax
				var $successCB = function (data)
				{
					if (data.status == 1) {
						var $resultData = data.result;
						$createAlert({status: "success", title: "Successfully", text: res.msg});

						
					}
				};

				//Error call back function for ajax
				var $errorCB = function () {

				};

				//Complete callback function for ajax
				var $completeCB = function () {

					//$clientReport.intBarchart();
				};

				//ajax call
				asyncPostToServer($url, $postdata, $successCB, $errorCB, $completeCB, true);
			}

			//catch start
			catch (ex) {
				alert(ex.message);
			}
			
		}*/
		__send: function () {
			var $type = $("#selDeviceType").val();
			var $clusterID = $("#selDev").val();

			var $cmd =  $("#selCMD").val();
			var $param1 = $("#param1").val();
			var $param2 = $("#param2").val();
			var $param3 = $("#param3").val();
            if ($cmd != "") {
                var $data = {
					'a': 'sendCluster',
					'c': 'LocationSettings',
					t:$type,
					cluster: $clusterID,
					p1: $param1,
					p2: $param2,
					p3: $param3,
					command:$cmd
                };

                $.ajax({
                    type: 'POST',
                    url: 'ajax.php',
                    data: $data,
                    dataType: 'json',
                    beforeSend: function () {
                        __showLoadingAnimation();
                    },
                    success: function (res) {
                        var $status = res.status;
                        if ($status == 1) {

                            $createAlert({status: "success", title: "Successfully", text: " Sent Command to Cluster ! "});
							__hideLoadingAnimation();
                        }
                        else {
                            $createAlert({status: "fail", title: "Failed", text: " To Send Command to Cluster! "});
							__hideLoadingAnimation();
                        }
                    },
                    complete: function () {
                        __hideLoadingAnimation();
                    },
                    error: function (r) {
                        __hideLoadingAnimation();
                        $createAlert({status: "fail", title: "Failed", text: r});
                    }
                });
            }
            else {
                $createAlert({status: "info", title: "Warring", text: "Select command to send"});
            }

        }
	}
</script>