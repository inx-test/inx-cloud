<?php


if (class_exists("ZMQ") && defined("ZMQ::LIBZMQ_VER")) {
    echo ZMQ::LIBZMQ_VER, PHP_EOL;
}
die();
// Autoload
define('CISCO_SSH_LOGIN','root');
define('CISCO_SSH_PASSWORD','pmi2017');

include 'vendor/autoload.php';

/*$loader = new \Composer\Autoload\ClassLoader();
$loader->addPsr4('phpseclib\\', __DIR__ . '/path/to/phpseclib2.0');
$loader->register();
*/
error_reporting(E_ALL);
ini_set('display_errors', '1');

// Load namespace
use phpseclib\Net\SSH2;

// Cisco Parameters
$ciscoIP = "192.168.1.34";

// Init SSHa
$ssh = new SSH2($ciscoIP);

// Login to SSH
if (!$ssh->login( CISCO_SSH_LOGIN , CISCO_SSH_PASSWORD )) { // Username / Password
    exit('SSH Login Failed. Invalid Login or Password.');
}

// Enable Privilage Mode
$ciscoPassword = CISCO_SSH_PASSWORD;
$ssh->write("\n");
$ssh->write("en\n");
$ssh->read('Password:');
$ssh->write($ciscoPassword."\n");
$ssh->setTimeout(2);

// Read Hostname
$ssh->write("tclsh flash:/Log.tcl\n");
//$ssh->setTimeout(10);
echo "<pre>";
$buffer = "";
while(true){
    $rx = $ssh->read();
    echo ">>>> $rx";
    $pos = strpos($rx, "Totals:");
    echo  "$pos";
    if( $pos > 0   ){
        echo "###### $pos";
        break;
    }

    // Append
    $buffer .= $rx;
}
echo $buffer;
echo "</pre>";
?>
