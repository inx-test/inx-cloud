<?php
require_once 'config.php';
require_once 'vendor/autoload.php';
require_once 'phpMailer/PHPMailerAutoload.php';


// Set to unlimited
ini_set('memory_limit','-1');

// Select all cloud instances
// Check disk usage of instances
// Notify user if about to notifyable range.
// Purge old data

// Init DB & CommonFunction
$dbc = new DBC();
$common = new CommonFunction();

// Select all instances
$instances = $db->get_result_array("SELECT pmi_inx_device.*, pmi_inx_device_db.DeviceDBName FROM pmi_inx_device JOIN pmi_inx_device_db ON (pmi_inx_device.ID = pmi_inx_device_db.DeviceID) WHERE deleted = 0");
$instanceCount = sizeof($instances);

if( $instanceCount > 0 ){
    echo "Found $instanceCount instances \n";

    // Loop through instances & purge old data
    foreach( $instances as $index => $instance ){
        $instanceID = $instance['id'];
        $instanceDBName = trim($instance['DeviceDBName']);
        $durationToKeepData = trim($instance['DataDuration']); // In Days
        $diskUsageLimit = trim($instance['DataSpaceLimit']); // Disk Usage Notification Limit
        $lastNotified = trim($instance['LastNotified']); // Last Notified
        $notifyToUsers = trim($instance['ReportMailID']);

        if( !empty($instanceDBName) ){

            echo "Starting housekeeping for instance $instanceID : $instanceDBName \n";

            // Find all tables from this database
            $instanceTables = $db->get_result_array("SELECT table_name FROM INFORMATION_SCHEMA.tables WHERE table_schema = '$instanceDBName'");
            if( sizeof($instanceTables) >  0 ){

                // 1: Purge old records from tables
                if( $durationToKeepData > 0 ){
                  // Loop through tables
                  foreach( $instanceTables as $table ){
                      $tableName = trim($table['table_name']);
                      if( !empty($tableName) ){
                        // Purge
                        $qry = "DELETE FROM $instanceDBName.$tableName WHERE Synchronize < UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL $durationToKeepData DAY))";
                       //  echo $qry;
                        //die;
                      //  if( $db->query($qry) ) {
                              // Delete true
                      //  } // End if

                      } // End if

                  } // End foreach
                } // End if


                // 2: Find disk usage & notify user if needed.
                $queryFindUsage  = "SELECT (data_length+index_length)/power(1024,2) tablesize_mb FROM information_schema.tables WHERE table_schema='$instanceDBName'";
				//echo $queryFindUsage;die;
                $tableUsages = $db->get_result_array($queryFindUsage);
                if( sizeof($tableUsages) > 0  ){
                  // Loop
                  $totalUsage = 0;
                  foreach( $tableUsages as $usage ){
                      $size = $usage['tablesize_mb'];
                      $totalUsage += $size;
                  }
				  echo "iiii".$totalUsage."\n";
				 // print_r($instance);die;
				 // echo $totalUsage;die;
                  //$totalUsage  = 10000;
                  if( $totalUsage >=  $diskUsageLimit ){
                      // Check if notiged within in x days.
                      if( empty($lastNotified) ){
                          goto sendEmail;
                      }
                      else {
                        $date1 = $lastNotified;
                        $date2 = time();

                        $diff = abs(strtotime($date2) - strtotime($date1));

                        $years = floor($diff / (365*60*60*24));
                        $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
                        $lastModifiedDays = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

                        if( $lastModifiedDays > 3 ){
                          goto sendEmail;
                        }
                     }


                     // Email Notification
                     sendEmail:
                        //Sending email
                        $emailTemplate = file_get_contents('tpl/emailTemplate.html');
                        $emailTemplate = str_replace("[[EMAIL_TITLE]]",'Disk usage warning!',$emailTemplate);
                        $emailTemplate = str_replace("[[EMAIL_CONTENT]]","<p></p>This is just note to warn you that the disk usage (<b>$totalUsage MB</b>) of Instance: <b>$instanceDBName</b> is greater than usage limit of <b>$diskUsageLimit MB</b> specified. ",$emailTemplate);
                        //echo $messgHtml;
                        if( !empty($notifyToUsers) )
                        {
                          $notifyToUsersList = explode(',',$notifyToUsers); // Just in case user add multiple
                          if( !is_array($notifyToUsersList) ) $notifyToUsersList = array($notifyToUsersList);

                          $emailSent = sendEmail($notifyToUsersList,'Disk usage warning',$emailTemplate,'Disk usage warning!');
                        }
                  }
                  // Foreach
                }
            }
        }// End if
    }
    // End foreach

}
else {
    echo "Nothing to clean. No instances found. \n";
}




function sendEmail($toUsers,$subject, $messageHtml,$messageTxt)
{

  $mail = new PHPMailer;
  $mail->SMTPDebug = 1;	// Enable verbose debug output 

  $mail->isSMTP();                                      // Set mailer to use SMTP
  $mail->Host = CLOUD_MAIL_SERVER; // 'smtp1.example.com;smtp2.example.com';  // Specify main and backup SMTP servers
  $mail->SMTPAuth = true;                              // Enable SMTP authentication
  $mail->Username = CLOUD_MAIL_EMAIL;                 // SMTP username
  $mail->Password = CLOUD_MAIL_PASSWORD;                           // SMTP password
  $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
  $mail->Port = CLOUD_MAIL_PORT;                                    // TCP port to connect to

  $mail->setFrom(CLOUD_MAIL_FROM, 'PMI Cloud Support');

  foreach($toUsers as $toUser)
  {
    $mail->addAddress($toUser);     // Add a recipient
  }

  $mail->isHTML(true);    // Set email format to HTML

  $mail->Subject = $subject;
  $mail->Body    = $messageHtml;
  $mail->AltBody = $messageTxt;
 print_R($toUsers);
		die;
  if(!$mail->send()) {	 
    echo 'Message could not be sent.';
    echo 'Mailer Error: ' . $mail->ErrorInfo;
    return 0;
  } else {
	  
    echo 'Message has been sent';
  }
   
  return 1;
}

// End
echo "Done House Keeping. \n";
?>
