<?php
require_once 'vendor/autoload.php';
require_once 'config.php';
use Pheanstalk\Pheanstalk;


$queue = new Pheanstalk(NOTIF_QUEUE_SERVER);
$queue->watch(SYSTEMLOG_QUEUE_NAME);
while($job=$queue->reserve(0))
{
	echo "deleting job\n";
	$queue->delete($job);
}
?>
