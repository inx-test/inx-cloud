<?php
	$inputJSON = file_get_contents('php://input');
	if ( $inputJSON != "" )	{
		$INPUT = json_decode($inputJSON, TRUE); //convert JSON into array
	}
	else $INPUT = $_REQUEST;
	
	//file_put_contents("test.log",print_r($INPUT,true),FILE_APPEND);
	
	
	// Include Helper Class
	set_include_path(get_include_path() . PATH_SEPARATOR . 'phpseclib');
	
	require_once('config.php');
	
	//file_put_contents("test.log","CP2",FILE_APPEND);
	
	require_once("_classes/256.php");
	require_once('vendor/autoload.php');
	include('Net/SFTP.php');
	include('Crypt/RSA.php');
	
	//file_put_contents("test.log","CP3",FILE_APPEND);
	use InboundProcess\InboundQueueClient;
	use Pheanstalk\Pheanstalk;
	use Pheanstalk\PheanstalkInterface;
	
	include_once("_classes/SyncController.php");
	
	
	$controller = $INPUT["c"]."Controller"; // Controller
	$action = $INPUT["a"]; // Action
	
	//file_put_contents("test.log","$controller, $action",FILE_APPEND);
	
	$controller = new $controller();
	$controller->$action($INPUT);
	
	exit;
?>
