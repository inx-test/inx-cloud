<?php
//require_once('tpl/header.tpl.php');
include_once("tpl/dashboard-top.tpl.php");
?>

<div class="content clearfix p-b-0">
    <?php include_once("tpl/leftTreeViewPanel.tpl.php"); ?>

    <div class="content-left ">
        <div class="panel panel-transparent m-b-0">
            <div class="panel-heading ">
                <div class="panel-title full-width">
                    <h5 class="semi-bold bold m-l-25 m-b-0 m-t-0">
                        Location Editor <span id="spClusterName" class="fs-14"></span>
                    </h5>
                </div>
            </div>
            <div class="panel-body">
                <div class="panel m-b-0 panel-savings no-margin">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-9 p-r-0">
                                <div class="location-map" id="mapContainer">
                                    <div class="chart-nodata"><strong>Please select a cluster.</strong></div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="widget-11 panel-map-items panel no-border no-margin">
                                    <div class="panel-body padding-0 b-b b-t b-l b-r b-grey">
                                        <div class="widget-11-table auto-overflow">
                                            <table class="table table-condensed table-hover overflow" id="tableDeviceMap">
                                                <thead>
                                                    <tr>
                                                        <th width="50%" class="">
                                                            Tag name
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <?php if ($loggedUserRoleId == ADMIN_ROLE || $loggedUserRoleId == ASSISTANT_ROLE || $loggedUserRoleId == SUPER_ADMIN_ROLE) { ?>
                                <div class="col-sm-2 hide" id="divUploadBtn">
                                    <span class="btn btn-tag btn-complete btn-tag-rounded fileinput-button m-t-15">
                                        <span>Select map <i class="fa fa-upload"></i></span>
                                        <input id="fileupload" type="file" name="files[]">
                                    </span>
                                </div>
                            <?php } ?>
                            <div class="col-sm-3 m-t-5 upload-progress hide" id="uploadProgress">
                                <span id="fileName" class="font-montserrat">No file selected</span>
                                <div class="meter m-t-5">
                                    <span style="width:0%; height:15px"></span>
                                    <p class="p-t-20"></p>
                                </div>
                            </div>
                            <?php if ($loggedUserRoleId == ADMIN_ROLE || $loggedUserRoleId == ASSISTANT_ROLE || $loggedUserRoleId == SUPER_ADMIN_ROLE) { ?>
                                <div class="col-sm-3 pull-right m-t-15 hide" id="btnSaveCancel">
                                    <button type="button" class="btn btn-tag btn-tag-rounded btn-danger pull-right m-l-10" onclick="$locationEditor.cancelNodeChange();">Cancel</button>
                                    <button type="button" id="btnSaveDeviceNodes" class="btn btn-tag btn-success btn-tag-rounded pull-right" onclick="$locationEditor.saveDeviceNodes();">Save</button>
                                </div>
                            <?php } ?>
                        </div>
                    </div>

                </div>
                <div class="row m-t-10">
                    <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12" id="divExeButtonContainer">
                    </div>
                </div>


            </div>
        </div>

        <input type='hidden' id='hdnClusterID' name='hdnClusterID' value="">
        <input type='hidden' id='hdntagID' name='hdntagID' value="">
    </div>
</div>
<div class="clearfix"></div>

		<nav id="context-menu" class="context-menu">
		</nav>
<?php
require_once('tpl/footer.tpl.php')
?>
<script src="js/context-menu.js" type="text/javascript"></script>
<script type="text/javascript">

    var loggedUser = '<?php echo $loggedUserRoleId; ?>';
    $locationEditorStatus =1;

    $(document).ready(function () {
        //alert($(window).height());
        //$cmdExecute.getCommands();
        $ClusterTree.init(0);
			console.log($(window).height());
        if ($(window).height() > 735)
            $(".content").css("min-height", ($(window).height())-126);
        else
            $(".content").css("min-height", "665px");
        $draggable('.drag', '.handle');
        $locationEditor.fileUpload();
        //loadActiveDevicesandNodes();


    });

    var $locationEditor = {
        getActiveNode:function($clusterID){
            //console.log(111111);
            if($clusterID != null && $clusterID != 0){
                $("#hdnClusterID").val($clusterID);
                $("#tableDeviceMap tbody").html("");
                $.ajax({
                    type: 'POST',
                    url: 'ajax.php',
                    data: {'c': 'Location', 'a': 'fetchDeviceInfo',clusterID:$clusterID},
                    dataType: 'json',
                    success: function (response) {
                        var imagepath = "";
                        if (response.status == 1) {
                            if(!response.image){
                                 $("#mapContainer").html('<div class="chart-nodata"><strong>Please select a map.</strong></div>');
                            }
                            else{
                                imagepath = 'img/location/' + response.image;
                                $("#tableDeviceMap tbody").html("");
                                $("#mapContainer").html("");
                                $("#mapContainer").html('<img src="' + imagepath + '" id="locationMapImg" alt="" width="100%" height="100%">');
                            }
                            var checked = '';
                            var tableHTML = '';
                            var dragNodeHTML = '';
                            var deviceInfo = response.device;
                            var checkedDevices = new Array();
                            var deviceId = '';
                            var k = 0;
                            if(deviceInfo.length > 0){
                                $cmdExecute.getCommands($clusterID);
                                for (var i = 0; i < deviceInfo.length; i++)
                                {
                                    var enabled = deviceInfo[i]['enabled'];
                                    var tagID = deviceInfo[i]['TagID'];
                                    var name = deviceInfo[i]['Name'];
                                    var xpos = deviceInfo[i]['x_pos'];
                                    var ypos = deviceInfo[i]['y_pos'];
                                    var tagTypeID = deviceInfo[i]['TagTypeID'];
                                    var $nodeID = deviceInfo[i]['NodeID'];
                                    //alert(devicename);

                                    if (enabled == 1) {
                                        checked = 'checked';
                                    }
                                    else {
                                        checked = '';
                                    }
                                    tableHTML += '<tr class="cursor"><td class="text-left">';
                                    if (loggedUser == 1 || loggedUser == 2 || loggedUser == 4) {
                                        tableHTML += '<div class="checkbox check-primary m-t-5 m-b-0">';
                                        tableHTML += '<input name="tagCheckEnable" type="checkbox" ' + checked + ' value="' + tagID + '" id="checkbox_' + tagID + '" xPos="' + xpos + '" yPos="' + ypos + '" data-name="' + name + '" data-type="'+tagTypeID+'" onclick="$locationEditor.selDevice(this,' + tagID + ')">';
                                        tableHTML += '<label datatoggle="tooltip" for="checkbox_' + tagID + '" class="bold">' + name + '</label>';
                                        tableHTML += '</div>';
                                    }
                                    else {
                                        tableHTML += name;
                                    }

                                    /*if(tagTypeID == 1){
                                        tableHTML +="<div class='location-controller-div' data-toggle=\"tooltip\" data-placement=\"left\" data-original-title=\"Click here to open controls\"><a href='javascript:void(0)' onclick='$(\"#nodeCmdPopover_"+tagID+"\").toggleClass(\"closed\");' class='location-controller-a'><i class=\"fa fa-cog\" aria-hidden=\"true\"></i></a></div>";
                                        tableHTML += '<div class="row nod-cmd-slider closed" id="nodeCmdPopover_'+tagID+'">';
                                        tableHTML += '<div class="col-xl-12 col-sm-12 col-lg-12 col-md-12">';
                                        $.each( $__CMDButtonHtmlStr, function( key, value ) {
                                            tableHTML += '<div class="clearfix fullwidth"><button type="button" class="btn btn-default bold m-r-15 m-t-1 m-b-0 btn-block text-left node-cmd-btn" onclick="return $cmdExecute.exeCommand(this);" data-cmd="'+key+'" data-tagid="'+tagID+'" data-tagType="2">'+value+'</button></div>';
                                        });

                                        tableHTML += '</div></div>';
                                    }*/
                                    tableHTML += '</td></tr>';

                                    dragNodeHTML += $locationEditor.setDeviceLocationNode(enabled, tagID, name, xpos, ypos,tagTypeID,$nodeID);
                                }
                                $("#tableDeviceMap tbody").append(tableHTML);
                                $("#mapContainer").append(dragNodeHTML);
                                $draggable('.drag', '.handle');
                            }
                            $('input:checkbox[name=tagCheckEnable]:checked').each(function ()
                            {
                                deviceId = $(this).val();
                                checkedDevices[k] = deviceId;
                                k++;
                            });

                            if (checkedDevices.length == 0) {
                                $("#btnSaveCancel").addClass("hide");
                            }
                            else {
                                $("#btnSaveCancel").removeClass("hide");
                            }
                        }
                        else if (response.status == 0 ) {
                            if(!response.image){
                                $("#mapContainer").html('<div class="chart-nodata"><strong>Please select a map.</strong></div>');
                            }
                            else{
                                imagepath = 'img/location/' + response.image;
                                $("#mapContainer").html("");
                                $("#mapContainer").html('<img src="' + imagepath + '" id="locationMapImg" alt="" width="100%" height="100%">');
                            }
                            $("#divExeButtonContainer").html('<label class="text-center text-l-help m-t-0" style="font-size:18px !important">Can\'t trigger any actions, please select a cluster with active nodes.</label>');

                        }
                    },
                    error: function (r) {
                        console.log(r);
                    },
                    complete: function () {
                        $('[data-toggle="tooltip"]').tooltip({container: "body"});
                        $(".pin").on("click", function (e) {
                            var $className = e.target.className;
                            if ($className != "handle" && $className != "fa fa-arrows") {
                                popDeviceSel(e.target.outerHTML, true);
                            }
                        });

                        initContextMenu($clusterID);
                    }
                });
            }
            else{
                $("#hdnClusterID").val("");
            }
        },
        selDevice:function($obj, id){
            var deviceDragNode = '';
            var name = $("#checkbox_" + id).attr("data-name");
            var xpos = $("#checkbox_" + id).attr("xPos");
            var ypos = $("#checkbox_" + id).attr("yPos");
            var tagType = $("#checkbox_" + id).attr("data-type");
            var checkedDevices = new Array();
            var deviceId = '';
            var i = 0;

            if ($("#mapContainer #tagNode_" + id).is(":visible") == true || $("#mapContainer #tagNode_" + id).hasClass("hide") == true) {
                if ($obj.checked)
                    $("#tagNode_" + id).removeClass("hide");
                else
                    $("#tagNode_" + id).addClass("hide");
            }
            else {
                if ($obj.checked) {
                    deviceDragNode += $locationEditor.setDeviceLocationNode(1, id, name, xpos, ypos,tagType);
                    $("#mapContainer").append(deviceDragNode);
                }
            }
            $draggable('#tagNode_' + id, '.handle');
            $('input:checkbox[name=tagCheckEnable]:checked').each(function ()
            {
                deviceId = $(this).val();
                checkedDevices[i] = deviceId;
                i++;
            });

            if (checkedDevices.length == 0) {
                $("#btnSaveCancel").addClass("hide");
            }
            else {
                $("#btnSaveCancel").removeClass("hide");
            }
        },
        setDeviceLocationNode : function(enabled, id, name, xpos, ypos,tagType,$nodeID){
            var nodeHTML = '';
            var drag = '';
            var $icon = ""
            var $tagIconClass = "fa-lightbulb-o";
            if(tagType == <?php echo FIXTURE_TYPE_WALLSWITCH ?>) $tagIconClass = "fa-power-off";
            /*if(tagType == 1) $icon = "fa-lightbulb-o";
            else if(tagType == 2) $icon = "fa-power-off";*/


            xpos = (xpos == null || xpos == "undefined") ? 20 : xpos;
            ypos = (ypos == null || ypos == "undefined") ? 5 : ypos;
            if (enabled == 1) {
                if (xpos != "" && ypos != "") {
                    if (loggedUser == 1 || loggedUser == 2 || loggedUser == 4) {
                        drag = "drag";
                    }
                    nodeHTML += '<div class="pin bounce ' + drag + ' task" id="tagNode_' + id + '" style="position: absolute; left: ' + xpos + 'px; top: ' + ypos + 'px;" data-tagID="'+$nodeID+'">';
                    nodeHTML += '<div class="handle task__content" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Click To Drag Or Right Click: '+name+'" ><i class="fa '+$tagIconClass+'" aria-hidden="true"></i></div>';
                    nodeHTML += '<label>' + name + '</label>';
                    nodeHTML += '</div>';
                }
                else {
                    nodeHTML += '<div class="pin bounce  ' + drag + ' task" id="tagNode_' + id + '" style="position: absolute; left: 400px; top: 0px;" data-tagID="'+$nodeID+'">';
                    nodeHTML += '<div class="handle task__content" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Click To Drag Or Right Click:  '+name+'"><i class="fa '+$tagIconClass+'" aria-hidden="true"></i></div>';
                    nodeHTML += '<label>' + name + '</label>';
                    nodeHTML += '</div>';
                }
            }
            else {
                nodeHTML += '<div class="pin bounce  ' + drag + ' hide task" id="tagNode_' + id + '" style="position: absolute; left: ' + xpos + 'px; top: ' + ypos + 'px;" data-tagID="'+$nodeID+'">';
                nodeHTML += '<div class="handle task__content" data-toggle="tooltip" data-placement="top" title="Click To Drag Or Right Click: : '+name+'"><i class="fa '+$tagIconClass+'" aria-hidden="true"></i></div>';
                nodeHTML += '<label>' + name + '</label>';
                nodeHTML += '</div>';
            }
            $draggable('#tagNode_' + id,".handle");
            return nodeHTML;
        },
        fileUpload : function() {


                // Call the fileupload widget and set some parameters
                $('#fileupload').fileupload({
                    url: 'files.php',
                    dataType: 'json',
                    done: function (e, data) {
                        $.each(data.result.files, function (index, file) {
                            console.log(file);
                            var imageName = file.name;
                            //$("#uploadSuccess #fileName").html(file.name);
                            //$("#uploadFail #fileName").html(file.name);

                            if (file.error) {
                                //alert(file.error);
                                $("#uploadProgress").addClass("hide");
                                $createAlert({status: "fail", title: "Failed", text: "Failed to upload " + file.name});
                                //$("#uploadSuccess").addClass("hide");
                                //$("#uploadFail").removeClass("hide");
                                //$("#uploadError").html(file.error);

                                setTimeout(function () {
                                    $('#uploadFail').fadeOut(2000);
                                }, 6000);
                            }
                            else {
                                var $clusterID = $("#hdnClusterID").val();
                                if($clusterID == null || $clusterID == 0){
                                    $createAlert({status: "success", title: "Location map uploaded", text: "Please select a cluster."});
                                    return false;
                                }
                                    $.ajax({
                                        type: 'POST',
                                        url: 'ajax.php',
                                        data: {'c': 'Location', 'a': 'saveUploadImage', 'imagename': imageName,clusterId:$clusterID},
                                        dataType: 'json',
                                        success: function (response) {
                                            setTimeout(function () {
                                                $('#uploadSuccess').fadeOut(2000);
                                            }, 6000);
											console.log(response);
											//var retobj = response.component;
											var imagepath = 'img/location/' + imageName;
											//alert(imagepath);
											//console.log(imagepath);
											//$("#uploadFail").addClass("hide");
											//$("#uploadSuccess").removeClass("hide");
											if( $("#mapContainer").find(".chart-nodata").length > 0){
												$locationEditor.getActiveNode($clusterID);
											}
											else $("#locationMapImg").attr("src", imagepath);
											$createAlert({status: "success", title: "Location map queued", text: "Successfully request queued to upload location map."});

                                        },
                                        error: function (r) {
                                            console.log(r);
                                        }
                                    });

                                //}
                               // else  $createAlert({status: "success", title: "Location map uploaded", text: "Please select a cluster."});
                            }

                        });

                    },
                    progressall: function (e, data) {
                        // Update the progress bar while files are being uploaded
                        var progress = parseInt(data.loaded / data.total * 100, 10);
                        //alert(progress);
                        var width = $('.meter span').css('width');

                        if (width == 0) {
                            setTimeout(function () {
                                $("#uploadProgress").removeClass("hide");
                                $('.meter span').css('width', progress + '%');
                                $('.meter p').html(progress + '%');
                            }, 4000);
                        }
                        else if (width == 100) {
                            $("#uploadProgress").addClass("hide");
                            //$("#uploadSuccess").removeClass("hide");
                        }
                    }
                });

        },
        cancelNodeChange:function(){
            var $clusterID = $("#hdnClusterID").val();
            if($clusterID != null && $clusterID != 0)$locationEditor.getActiveNode($clusterID);
        },
        saveDeviceNodes:function(){
            var $nodes = new Array();
            var $clusterID = $("#hdnClusterID").val();

            if($clusterID != null && $clusterID != 0){
                var i = 0;

                $('input:checkbox[name=tagCheckEnable]:checked').each(function ()
                {
                    var $id = $(this).val();
                    //$nodes[i]["tagID"] = $id;

                    //Draggabilly Positions
                    var draggie = $("#tagNode_" + $id).data('draggabilly');
                    var $xPos = draggie.position.x;
                    var $yPos = draggie.position.y;

                    $nodes.push({"tagID":$id,"xPos":$xPos,"yPos":$yPos});
                    i++;
                });

                //alert(checkedDevices);


                $.ajax({
                    type: 'POST',
                    url: 'ajax.php',
                    data: {'c': 'Location', 'a': 'saveMapNode', nodes: $nodes, clusterID:$clusterID},
                    dataType: 'json',
                    beforeSend: function () {
                        __showLoadingAnimation();
                    },
                    success: function (response) {
                        if (response.component != '') {
                            $createAlert({status: "success", title: "Successfully Queued", text: response.message});
                        }
                        else if (response.status == 0) {
                            $createAlert({status: "fail", title: "Failed", text: response.message});
                        }
                        $locationEditor.getActiveNode($clusterID);
                    },
                    complete: function () {
                        __hideLoadingAnimation();
                    },
                    error: function (r) {
                        __hideLoadingAnimation();
                        console.log(r);
                    }
                });
            }
            else  $createAlert({status: "success", title: "Location map uploaded", text: "Please select a cluster."});
        }
    };
    $exeCmdIp = "192.168.1.11";
    $exePort = "10026";
    $__CMDButtonHtmlStr = [];
    var $cmdExecute = {
        getCommands : function($clusterID){
            $.ajax({
                type: 'POST',
                url: 'ajax.php',
                data: {'c': 'Location', 'a': 'getExeCommands'},
                dataType: 'json',
                beforeSend: function () {
                   // __showLoadingAnimation();
                },
                success: function (response) {
                    if (response.status == 1) {
                        console.log(response);
                        $__CMDButtonHtmlStr = response.reuslt;
                        //console.log($__CMDButtonHtmlStr);

                        var $htmlStr = "";
                        var $navStr = "<ul class=\"context-menu__items\">";
                        $htmlStr += '<div class="row p-l-15 p-r-10">';
                        $.each( response.reuslt, function( key, value ) {
                            if(key == <?php echo LIGHT_DIM ?>){

                                $htmlStr += '<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 col-xl-2 p-r-5 p-l-0">';
                                $htmlStr += '<div style="width:43%" class="pull-left cleafix">';
                                $htmlStr += '<input type="text" id="txtClusterDim" value="" placeholder="0-100%" class="form-control" onkeypress="return $cmdExecute.dimValidation(event);" onchange="return $cmdExecute.dimHandleChange(this);">';
                                $htmlStr += '</div>';
                                $htmlStr += '<div style="width:56%" class="pull-left">';
                                $htmlStr += '<button type="button" class="btn btn-block btn-default bold m-r-10" onclick="return $cmdExecute.exeCommand(this);" data-cmd="'+key+'" data-tagType="1">'+value+'</button>';
                                $htmlStr += '</div>';
                                $htmlStr += '</div>';

                                $navStr += "<li class=\"context-menu__item clearfix m-b-0\">";
                                $navStr += '<div style="width:50%" class="pull-left">';
                                $navStr += "<a href=\"javascript:void(0);\" class=\"context-menu__link\"  onclick=\"return $cmdExecute.exeCommand(this);\" data-cmd=\""+key+"\" data-tagType=\"2\">"+value+"</a>";
                                $navStr += '</div>';
                                $navStr += '<div style="width:50%" class="pull-left cleafix">';
                                $navStr += '<input type="text" id="txtTagDim" value="" placeholder="0-100%" class="form-control context-menu__link">';
                                $navStr += '</div>';
                                $navStr += "</li>";
                    }
                            else{
                                $htmlStr += '<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 col-xl-2 p-r-5 p-l-0">';
                                $htmlStr += '<button type="button" class="btn btn-block btn-default bold m-r-10" onclick="return $cmdExecute.exeCommand(this);" data-cmd="'+key+'" data-tagType="1">'+value+'</button>';
                                $htmlStr += '</div>';

                                $navStr += "<li class=\"context-menu__item m-b-0\">";
                                $navStr += " <a href=\"javascript:void(0);\" class=\"context-menu__link\"  onclick=\"return $cmdExecute.exeCommand(this);\" data-cmd=\""+key+"\" data-tagType=\"2\">"+value+"</a>";
                                $navStr += "</li>";
                            }

                        });

                        $htmlStr += '</div>';
                        $navStr += "</ul>";
                        if(typeof $clusterID != "undefined"){
                            $("#divExeButtonContainer").html($htmlStr);
                            $("#hdnClusterID").val($clusterID);
                        }
                        $("#context-menu").html($navStr);
                    }
                },
                complete: function () {
                    //__hideLoadingAnimation();
                },
                error: function (r) {
                    //__hideLoadingAnimation();
                    console.log(r);
                }
            });
        },
        openNodeController : function($tagID){
            $("#nodeCmdPopover_"+$tagID).removeClass("closed");
           if($("#nodeCmdPopover_"+$tagID).hasClass("closed")) $("#nodeCmdPopover_"+$tagID).removeClass("closed");
           else  $("#nodeCmdPopover_"+$tagID).addClass("closed");
        },
        exeCommand : function($obj){
            var $cmd = $($obj).attr('data-cmd');
            var $devID = 0;//$($obj).attr('data-tagid');
            var $tagType = $($obj).attr('data-tagType');
            var $rangeVal = -1;
            console.log(">>>>>> ::::: "+$tagType);
            if($tagType == 1) $devID = $("#hdnClusterID").val();
            else if($tagType == 2) $devID = $("#hdntagID").val();
            var $btnHtml = $($obj).html();
            if($devID == 0){
                $createAlert({status: "fail", title: "Failed", text: "Attempt failed, try again."});
                return false;
            }
            if($cmd == <?php echo LIGHT_DIM ?>){
                if($tagType == 1) $rangeVal = $("#txtClusterDim").val();
                if($tagType == 2) $rangeVal = $("#txtTagDim").val();
                if($rangeVal == -1 || $rangeVal == ""){
                    $createAlert({status: "fail", title: "Failed", text: "Please enter dim range."});
                    return false;
                }
            }
            $.ajax({
                type: 'POST',
                url: 'ajax.php',
                data: {'c': 'Location', 'a': 'exeCommands','cmd':$cmd,'devID':$devID,"type":$tagType,"rangeVal":$rangeVal},
                dataType: 'json',
                beforeSend: function () {
                    $($obj).html("Sending...");
                    $($obj).attr("disabled","disabled");
                },
                success: function (response) {
                    if (response.component != '') {
                       // var $tagStat = response.data;
                        var $overallStatus= 0;
						var $tagStat = response.component;
                        /*if($tagStat.length > 0){
                            for(var $i=0; $i < $tagStat.length; $i++){
                                var $status = $tagStat[$i]["Status"];
                                var $msg = $tagStat[$i]["MSG"];
                                if($status == 1){
                                    $createAlert({status: "success", title: "Successfully executed", text: $msg});
                                }
                                if($status == 2){
                                    $overallStatus++;
                                    $createAlert({status: "fail", title: "Failed to execute", text: $msg});
                                }

                            }
                        }*/
						if($tagStat != '')
						{
							$createAlert({status: "success", title: "Successfully Queued ", text: 'Successfully request queued to execute'});
						}
						else
						{
							$createAlert({status: "success", title: "Failed to Queue", text: 'Failed to  request queue to execute'});
						}
                        $($obj).removeAttr("disabled");
                        if($overallStatus == 0){
                            $($obj).html("Success");
                        setTimeout(function(){
                             $($obj).html($btnHtml);
                        },1740);
                    }
                        else $($obj).html($btnHtml);

                    }
                    else {
                        $($obj).removeAttr("disabled");
                        setTimeout(function(){
                             $($obj).html($btnHtml);
                        },1740);
                        $createAlert({status: "fail", title: "Failed", text: "Attempt failed, try again."});
                    }

                    $("#txtClusterDim").val("");
                    $("#txtTagDim").val("");
                },
                error: function (r) {
                    console.log(r);
                   $createAlert({status: "fail", title: "Failed", text: "Attempt failed, try again."});
                    $($obj).removeAttr("disabled");
                    setTimeout(function(){
                         $($obj).html($btnHtml);
                    },1740);

                    $("#txtClusterDim").val("");
                    $("#txtTagDim").val("");
                }
            });
        },
        dimValidation : function(e){
            //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                //display error message
                return false;
            }
        },
        dimHandleChange : function(input){
            if (input.value < 0) input.value = 0;
            if (input.value > 100) input.value = 100;
        }

    };
</script>
