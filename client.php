<?php
include "config.php";
//include_once("tpl/header.tpl.php");
include_once("tpl/dashboard-top.tpl.php"); 
?>
<div class="content" style=min-height: calc(100% - 200px);>
    <?php if ($loggedUserRoleId == ADMIN_ROLE || $loggedUserRoleId == SUPER_ADMIN_ROLE) { ?>
        <div class="r-button tooltips" data-placement="top" data-trigger="hover" data-toggle="tooltip" title="" data-original-title="Click here to add new client"> 
            <input type="checkbox" id="your-id" class="r-button-checkbox" onclick="return addNewClient(0);"> 
            <label class="r-button-trigger" for="your-id"> 
                <i class="fa fa-plus material-icons1 fa-lg"></i>
            </label> 
        </div>
    <?php } ?>
    <!-- START PANEL -->
    <div class="panel panel-transparent">
        <div class="panel-heading ">
            <div class="panel-title full-width">
                <h5 class="semi-bold bold m-l-25 m-b-0 m-t-0">
                    Manage clients 
                </h5>
            </div>
        </div>
        <div class="panel-body">
            <div id="portlet-advance" class="panel panel-default  panel-savings clientmanage">
                <div class="panel-body " id="clientManage" style="padding:15px 5px 5px 5px;">
                    <!--Client Space-->	
                </div>

                <!-- MODAL CONFIRMATION ALERT -->
                <div class="modal fade slide-up disable-scroll" id="modalCnfirmation" tabindex="-1" role="dialog" aria-hidden="false">
                    <div class="modal-dialog" style="width: 300px;">
                        <div class="modal-content-wrapper">
                            <div class="modal-content mdal-custom">
                                <div class="modal-body p-b-5">
                                    <p class="bold fs-20 p-t-20" id="cnCntent">
                                        Do you want to remove this Client?
                                    </p>
                                </div>
                                <div class="modal-footer text-right">
                                    <button type="button" class="btn btn-danger btn-sm pull-right" data-dismiss="modal">Cancel</button>
                                    <button type="button" class="btn btn-success btn-sm pull-right m-r-10" onclick="removeConfirm();">OK</button>
                                </div>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                </div>
                <!-- END CONFIRMATION ALERT -->

                <input type="hidden" id="delClientId" name="delClientId" value="" />
            </div>


            <!-- Add New Client Starts -->
            <div id="div-new-client-modal" class="hide">
                <div class="accordion" style="max-width:850px;">
                    <a class="active" href="#">
                        <i class="fa fa-client"></i>
                        <span id="spTitle">Enter client details</span>
                        <button type="button" class="close" onclick="closeAddClient();">&times;</button>
                    </a>
                    <div class="sub-nav active p-b-15" style="display: block;">
                        <div class="html about-me">
                            <form id="frm-new-client" name="formAddClient" id="formAddClient">
                                <div class="hide save-alert" id="alertBox">
                                    <p class="alert-msg"></p>
                                </div>
                                <div class="add-client-section">
                                    <div class="user-div">
                                        <div class="form-group form-group-default m-b-10" id="divFirstName">
                                            <label>First Name</label>
                                            <input class="form-control" type="text" value="" id="txt-client-firstname" name="firstname" onblur="return checkValidation('txt-client-firstname');" onkeyup="removeErrorAlert('divFirstName');" required>
                                            <span class="text-danger hide"></span>
                                        </div>
										 <div class="form-group form-group-default m-b-10" id="divLastName">
                                            <label>Last Name</label>
                                            <input class="form-control" type="text" value="" id="txt-client-lastname" name="lastname" onblur="return checkValidation('txt-client-lastname');" onkeyup="removeErrorAlert('divLastName');" required>
                                            <span class="text-danger hide"></span>
                                        </div>
                                        
                                                                               
                                    </div>
                                    <div class="role-div">
                                        <div class="form-group form-group-default m-b-10" id="divEmailID">
                                            <label>Email ID</label>
                                            <input class="form-control" type="email" value="" id="txt-client-email" name="email" onblur="return checkValidation('txt-client-email');" onkeyup="removeErrorAlert('divEmailID');" required>
                                            <span class="text-danger hide"></span>
                                        </div>
										 <div class="form-group form-group-default m-b-10 m-t-10" id="divContactNo">
                                            <label>Contact number</label>
                                            <input class="form-control" type="tel" value="" id="txt-client-contact" name="contact" onblur="return checkValidation('txt-client-contact');" onkeyup="removeErrorAlert('divContactNo');" required>
                                            <span class="text-danger hide"></span>
                                        </div>
                                    </div>
                                    <input type="hidden" id="hdnClientID" value="">
                                </div>
                                <div class="p-t-10">
                                    <button type="button" class="btn btn-danger pull-right" onclick="closeAddClient();">Close</button>
                                    <button onclick="SaveClient();" id="btnSaveClient" type="button" class="btn btn-success pull-right m-r-10">Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Add New Client Ends -->
        </div>
    </div>
    <!-- END PANEL -->
</div>
<!-- END PAGE CONTENT -->
<?php include_once("tpl/footer.tpl.php"); ?>

<!--script src="js/jquery.min.js" type="text/javascript"></script-->


<script type="text/javascript">
    var loggedClientId = '<?php echo $loggedInUserId; ?>';
    var loggedClientRole = '<?php echo $loggedUserRoleId; ?>';
    var allRolesId = new Array();
    var allRolesName = new Array();
    $(document).ready(function ()
    {
		//alert(loggedClientRole);
       // $(".content").css("min-height", ($(window).height()));
        loadClient();

        $('.form-control').keypress(function (e) {
            if (e.keyCode == 13) {
                $('#btnSaveClient').click();
                return false;
            }
        });
    });

    $('html').on('click', function (e) {
        if (typeof $(e.target).data('original-title') == 'undefined' &&
                !$(e.target).parents().is('.popover.in')) {
            $('.popover').popover('hide');
        }
    });

    function loadClient() {
        $.ajax({
            type: 'POST',
            url: 'ajax.php',
            data: {'c': 'Client', 'a': 'fetchAllClient'},
            dataType: 'json',
            success: function (response) {
                if (response.status == 1)
                {
                    var id = '';
                    var name = '';
                    var role = '';
                    var email = '';
                    var active = '';
                    var enableClass = '';
                    var enableTitle = '';
                    var imgPath = '';
                    var clientHTML = '';
                    var count = 1;
                    var clientArr = response.clientInfo;
                    var imagePathArr = response.imagePath;
                    var d = new Date();

                    $("#clientManage").html("");

                    for (var i = 0; i < clientArr.length; i++)
                    {
                        id = clientArr[i]['id'];
                        name = clientArr[i]['firstname'] + " " + clientArr[i]['lastname'];
						phone = clientArr[i]['phone']
                        email = clientArr[i]['email'];
                        imgPath = imagePathArr[i];
                       // role = clientRoleArr[i];

                        if (active == 1) {
                            enableClass = 'fa fa-check tooltips enable-user-class';
                            enableTitle = 'Disable Client';
                        }
                        else {
                            enableClass = 'fa fa-times tooltips disable-user-class';
                            enableTitle = 'Enable Client';
                        }

                        if (id != loggedClientId)
                        {
                            var mod = count % 3;
                            if (mod != 0) {
                                clientHTML += '<div class="col-sm-4 p-r-5 m-b-15" id="clientProfile_' + id + '">';
                            }
                            else {
                                clientHTML += '<div class="col-sm-4 m-b-15" id="clientProfile_' + id + '">';
                            }
                            clientHTML += '<div class="user-card padding-10"><input type="hidden" id="hdnCompanyClientID_4" value="">';
                           // clientHTML += '<div class="col-sm-2 no-padding"><img src="' + imgPath + '?' + d.getTime() + '" class="imgWrap thumbnail-wrapper d48 circular " alt="img"></div>';
                            clientHTML += '<div class="col-sm-10"><span>' + name + '<span class="p-l-5 bold"></span><br> <span class="fa fa-envelope p-r-5 "></span><small>' + email + '</small></span>';
                            if (loggedClientRole == 4) {
                               // clientHTML += '<span class="pull-right"><span class="accept cursor" style=""><a class="' + enableClass + '" data-placement="left" data-toggle="tooltip" title="' + enableTitle + '" id="acceptedInv_4" userstatus="' + active + '" onclick="return changeClientStatus(' + id + ');"></a></span>';
							    clientHTML += '<span class="pull-right"><span class="accept cursor" style=""></a></span>';
                                clientHTML += '<a class="fa fa-trash removeClient  p-r-15 text-danger curPoint cursor" data-placement="left" data-toggle="tooltip" title="Remove Client" dbid="0" id="projectClient_4" onclick="return removeClient(' + id + ');"></a>';
                                clientHTML += '<a class="fa fa-pencil cursor" data-placement="left" data-toggle="tooltip" title="Edit Client" id="rolePopover_' + id + '" onclick="showClientEdit(' + id + ');"></a>';
                                clientHTML += '</span>';
                            }
                            clientHTML += '</div><div class="col-sm-10"><span class="fa fa-phone p-r-15 "><span class="p-l-5 ">' + phone + '</span></span></div></div></div>';
                            count++;
                        }

                    }

                    $("#clientManage").append(clientHTML);
                    $('[data-toggle="tooltip"]').tooltip();
                }
                else if (response.status == 0) {
					$("#clientManage").append('<h3 style="text-align:center">No Client Users</h3>');
                }
            },
            error: function (r) {
                console.log(r);
            }
        });
    }

    function addNewClient()
    {
        var roleHTML = '';
        $(".form-group").removeClass("has-error");
        $(".form-group .text-danger").addClass("hide");
        $('#txt-client-firstname').val("");
        $('#txt-client-firstname').focus();
        $('#txt-client-lastname').val("");
        $('#txt-client-email').val("");
        $('#txt-client-contact').val("");
        $('.clientmanage').hide();
        $('#div-new-client-modal').removeClass("hide");
        $('#btnAddClient').hide();
        $("#alertBox").addClass("hide");
        $("#role-select").html("");
        $("#hdnClientID").val("");
        $("#spTitle").html("Enter client details");

        for (var i = 0; i < allRolesId.length; i++) {
            roleHTML += '<option value="' + allRolesId[i] + '">' + allRolesName[i] + '</option>';
        }
        $("#role-select").append(roleHTML);
    }

    function closeAddClient()
    {
        $('#txt-client-firstname').val("");
        $('#txt-client-lastname').val("");
        $('#txt-client-email').val("");
        $('#txt-client-contact').val("");
        $('#txt-client-clientname').val("");
        $('#txt-client-password').val("");
        $("#alertBox p").html("");
        $('.clientmanage').show();
        $('#div-new-client-modal').addClass("hide");
        $('#btnAddClient').show();
    }

    function SaveClient()
    {
		
        var firstname = $('#txt-client-firstname').val();
        var lastname = $('#txt-client-lastname').val();
        var email = $('#txt-client-email').val();
        var contactno = $('#txt-client-contact').val();
        var clientID = $("#hdnClientID").val();
        if (firstname == "")
        {
            $("#divFirstName").addClass("has-error");
            $("#divFirstName span").html("Please enter first name");
            $("#divFirstName span").removeClass("hide");
            $("#txt-client-firstname").focus();

            return false;
        }
        if (lastname == "")
        {
            $("#divLastName").addClass("has-error");
            $("#divLastName span").html("Please enter last name");
            $("#divLastName span").removeClass("hide");
            $("#txt-client-lastname").focus();

            return false;
        }
        if (email == "")
        {
            $("#divEmailID").addClass("has-error");
            $("#divEmailID span").html("Please enter email address");
            $("#divEmailID span").removeClass("hide");
            $("#txt-client-email").focus();
            return false;
        }
        if (!ValidateEmail(email))
        {
            $("#divEmailID").addClass("has-error");
            $("#divEmailID span").html("Invalid email address");
            $("#divEmailID span").removeClass("hide");
            $("#txt-client-email").focus();
            return false;
        }
        if (contactno == "") {
            $("#divContactNo").addClass("has-error");
            $("#divContactNo span").html("Please enter contact number");
            $("#divContactNo span").removeClass("hide");
            $("#txt-client-contact").focus();

            return false;
        }
        if (!isNumber(contactno)) {
            $("#divContactNo").addClass("has-error");
            $("#divContactNo span").html("Invalid contact number");
            $("#divContactNo span").removeClass("hide");
            $("#txt-client-contact").focus();
            return false;
        }

        
        var dataArr = {'c': 'Client',
            'a': 'saveClientInfo',
            'firstname': firstname,
            'lastname': lastname,
            'email': email,
            'contact': contactno };

        if (clientID != "") {
            //dataArr["a"] = 'updateClientInfo';
            dataArr["clientID"] = clientID;
        }

        $.ajax({
            type: 'POST',
            url: 'ajax.php',
            data: dataArr,
            dataType: 'json',
            beforeSend: function () {
                __showLoadingAnimation();
            },
            success: function (response) {
                if (response.status == 1) {
                    $createAlert({status: "success", title: "Success", text: response.message});
                    $('#div-new-client-modal').addClass("hide");
                    $('.clientmanage').show();
                    $('#btnAddClient').show();
                    loadClient();
                }
                else if (response.status == 0) {
                    $createAlert({status: "fail", title: "Failed", text: response.message});
                }
            },
            complete: function () {
                __hideLoadingAnimation();
            },
            error: function (r) {
                __hideLoadingAnimation();
                console.log(r);
            }
        })
    }

    function changeClientStatus(clientId)
    {
        var clientStatus = $('#clientProfile_' + clientId + ' #acceptedInv_4').attr("clientstatus");
        //alert("client: "+clientId+" status: "+clientStatus);
        // return false;

        $.ajax({
            type: 'POST',
            url: 'ajax.php',
            data: {'c': 'Client', 'a': 'updateClientStatus', 'clientstatus': clientStatus, 'clientid': clientId},
            dataType: 'json',
            beforeSend: function () {
                __showLoadingAnimation();
            },
            success: function (response) {
                if (response.status == 1) {
                    $createAlert({status: "success", title: "Success", text: response.message});
                    loadClient();
                }
                else if (response.status == 0) {
                    $createAlert({status: "fail", title: "Failed", text: response.message});
                }
            },
            complete: function () {
                __hideLoadingAnimation();
            },
            error: function (r) {
                __hideLoadingAnimation();
                console.log(r);
            }
        })
    }

    function removeClient(removeClientId)
    {
        $("#btnCnfmYes").addClass("btn-success");
        $("#cnCntent").html("Do you wish to remove this client?");
        $("#modalCnfirmation").modal("show");
        $("#delClientId").val(removeClientId);
    }

    function removeConfirm()
    {
        var removeId = $("#delClientId").val();
        $.ajax({
            type: 'POST',
            url: 'ajax.php',
            data: {'c': 'Client', 'a': 'removeClientInfo', 'clientid': removeId},
            dataType: 'json',
            beforeSend: function () {
                __showLoadingAnimation();
            },
            success: function (response) {
                if (response.status == 1) {
                    $createAlert({status: "success", title: "Success", text: response.message});
                    $("#modalCnfirmation").modal("hide");
                    $("#delClientId").val("");
                    loadClient();
                }
                else if (response.status == 0) {
                    $createAlert({status: "fail", title: "Failed", text: response.message});
                    $("#modalCnfirmation").modal("hide");
                    $("#delClientId").val("");
                }
            },
            complete: function () {
                __hideLoadingAnimation();
            },
            error: function (r) {
                __hideLoadingAnimation();
                console.log(r);
            }
        })
    }

    function showClientEdit(ClientId)
    {
        var clientRole = new Array();
        $.ajax({
            type: 'POST',
            url: 'ajax.php',
            data: {'c': 'Client', 'a': 'fetchClientData', 'clientid': ClientId},
            dataType: 'json',
            success: function (response) {
                if (response.status == 1) {
                   
                    addNewClient();
                    if (response.result.length > 0) {
                        $('#txt-client-firstname').val(response.result[0].firstname);
                        $('#txt-client-lastname').val(response.result[0].lastname);
                        $('#txt-client-email').val(response.result[0].email);
                        $('#txt-client-contact').val(response.result[0].phone);
                        $('#div-new-client-modal').removeClass("hide");
                        $('#btnAddClient').hide();
                        $("#alertBox").addClass("hide");
                        $("#hdnClientID").val(ClientId);
                        $("#spTitle").html("Edit client details");
                    }
                }
            },
            error: function (r) {
                console.log(r);
            }
        });
    }
    
    function removeErrorAlert(id) {
        $("#" + id).removeClass("has-error");
        $("#" + id + " span").html("");
        $("#" + id + " span").addClass("hide");
    }

    function checkValidation($eleID) {
        if ($eleID == "txt-client-firstname") {
            var $eleVal = $("#txt-client-firstname").val();
            if ($eleVal.trim() == "") {
                $("#divFirstName").addClass("has-error");
                $("#divFirstName span").html("Please enter first name");
                $("#divFirstName span").removeClass("hide");
                $("#txt-client-firstname").focus();
            }
        }
        else if ($eleID == "txt-client-lastname") {
            var $eleVal = $("#txt-client-lastname").val();
            if ($eleVal.trim() == "") {
                $("#divLastName").addClass("has-error");
                $("#divLastName span").html("Please enter last name");
                $("#divLastName span").removeClass("hide");
                $("#txt-client-lastname").focus();
            }
        }
        else if ($eleID == "txt-client-email") {
            var $eleVal = $("#txt-client-email").val();
            if ($eleVal.trim() == "") {
                $("#divEmailID").addClass("has-error");
                $("#divEmailID span").html("Please enter email address");
                $("#divEmailID span").removeClass("hide");
                $("#txt-client-email").focus();
            }
            else if (!ValidateEmail($eleVal)) {
                $("#divEmailID").addClass("has-error");
                $("#divEmailID span").html("Invalid email address");
                $("#divEmailID span").removeClass("hide");
                $("#txt-client-email").focus();
            }
        }
        else if ($eleID == "txt-client-contact") {
            var $eleVal = $("#txt-client-contact").val();
            if ($eleVal == "" || !isNumber($eleVal)) {
                $("#divContactNo").addClass("has-error");
                $("#divContactNo span").html("Please enter contact number");
                $("#divContactNo span").removeClass("hide");
                $("#txt-client-contact").focus();
            }
            else if (!isNumber($eleVal)) {
                $("#divContactNo").addClass("has-error");
                $("#divContactNo span").html("Invalid contact number");
                $("#divContactNo span").removeClass("hide");
                $("#txt-client-contact").focus();
            }
        }
        else if ($eleID == "txt-client-clientname") {
            var $eleVal = $("#txt-client-clientname").val();
            if ($eleVal == "") {
                $("#divClientName").addClass("has-error");
                $("#divClientName span").html("Please enter a valid clientname");
                $("#divClientName span").removeClass("hide");
                $("#txt-client-clientname").focus();
            }
        }
        else if ($eleID == "txt-client-password") {
            var $eleVal = $("#txt-client-password").val();
            if ($eleVal.trim() == "") {
                $("#divPassword").addClass("has-error");
                $("#divPassword span").html("Please enter a valid password");
                $("#divPassword span").removeClass("hide");
                $("#txt-client-password").focus();
            }
            else if ($eleVal.length <= 3) {
                $("#divPassword").addClass("has-error");
                $("#divPassword span").html("min 3 characters");
                $("#divPassword span").removeClass("hide");
                $("#txt-client-password").focus();
            }
        }
    }

   
</script>

