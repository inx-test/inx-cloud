<?php 
include "config.php";
//include_once("tpl/header.tpl.php");  
  include_once("tpl/dashboard-top.tpl.php");
?>
   <div class="content p-b-0">
      <!-- START PANEL -->
       <div class="panel panel-transparent">
         <div class="panel-body">
             <div class="col-xl-2 col-l-2 col-md-2 col-sm-12"></div>
             <div class="col-xl-8 col-l-8 col-md-8 col-sm-12">
                    <div class="row">
                        <div id="portlet-advance " class="panel panel-default  panel-savings">
                            <div class="panel-heading p-t-15">
                                <div class="col-md-12 bold m-b-15">
                                    <font size="5">Alerts</font> 
                                    <div class="pull-right">
                                        <button  type="button" class="btn btn-cons btn-danger btn-sm" onclick='return ClearAlert(1,2);'>Delete All </button>
                                    </div>
                                </div>									
                            </div>
                            <div class="panel-body" id="alertContainer">
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- END PANEL -->
    
<?php include_once("tpl/footer.tpl.php"); ?>

<script type="text/javascript">
     var loggedUserId = <?php echo $loggedInUserId; ?>;
     var loggedUserRole = <?php echo $loggedUserRoleId; ?>;
     
     $(document).ready(function(){
        $(".content").css("min-height",($(window).height()));
        showMore(0,20);
    });
    
    function showMore($ID,$showCount){
        if($(".more-alerts").length > 0) $(".more-alerts").remove();
        $("#alertContainer").append('<div class="loading text-center"><div class="loading-bar"></div><div class="loading-bar"></div><div class="loading-bar"></div><div class="loading-bar"></div></div>');
        
        $.ajax({
        type:'POST',
        url: 'ajax.php',
        data:{ 'c':'Device', 'a':'GetAlerts','ID':$ID,'showCount':$showCount},
        dataType: 'json',
        success: function(res) {
            if(res.results != null && res.results != ""){
                $alerts = res.results;
                if($alerts.length > 0){
                    $(".panel-heading").removeClass("hide");
                    if($("#alertContainer text-l-help").length > 0)$("#alertContainer").html("");
                    $("#alertContainer .loading").remove();
                    for (var i = 0; i < $alerts.length ; i++) {
                        $("#alertContainer").append(getAlertContent($alerts[i]));
                    }
                    //console.log($alerts[0].TCount);
                    if($alerts[0].TCount > $alerts.length && $alerts[$alerts.length - 1].ID != 1){
                        var $HTMLStr = '<div class="nn-alert text-center cursor more-alerts" onclick="return showMore('+ $alerts[$alerts.length - 1].ID +',20);" style="min-height:34px !important;width:231px;">';
                            $HTMLStr += '    <strong>More Alerts</strong>';
                            $HTMLStr += '</div>';
                        $("#alertContainer").append($HTMLStr);
                    }
                }
                else $("#alertContainer").html("<h5 class=\"text-center text-l-help\">No alerts found.</h5>");
            }
            else{
                $("#alertContainer .loading").remove();
                $(".panel-heading").addClass("hide");
                $("#alertContainer").html("<h5 class=\"text-center text-l-help m-t-15 m-b-0\">No alerts found.</h5>");
            }
        },
        error: function(r) {
            console.log(r);
            $("#alertContainer .loading").remove();
        }
    });
    }
    
    function getAlertContent($alerts){
        var $severity = $alerts.Severity;
        var $createdOn = $alerts.CratedOn;
        var $status = $alerts.Status;
        var $type = $alerts.AlertType;
        var alertClass = "";
		var $elm = 1;	
        console.log("$severity === "+$severity);
        if($severity == 1) alertClass = "bg-green bg-green-ball";
        else if($severity == 2) alertClass = "bg-blue bg-blue-ball";
        else if($severity == 3) alertClass = "bg-dark-blue bg-dark-blue-ball";
        else if($severity == 4) alertClass = "bg-yellow bg-yellow-ball";
        else if($severity == 5) alertClass = "bg-red bg-red-ball";
        
        var $HTMLStr = '<div class="nn-alert" id="alert_content_'+$alerts.ID+'">';
        $HTMLStr += '    <div class="notify-buble-lg pull-left p-t-5 m-r-5 '+alertClass+'"></div>';
        $HTMLStr += '    <strong>'+$alerts.Title;
        if($type == 2 ){
            $HTMLStr += '    <a href="javascript:void(0);" class="btn btn-xs btn-complete bold " style="padding:0px 5px 0px 5px;line-height: 15px;margin-top: -5px;" onclick="return retryPolicy('+$alerts.ID+');"> Retry'
            $HTMLStr += '    </a>'
        }
		//-------------AK tag 
        $HTMLStr += '    <a href="javascript:void(0);" class="btn btn-xs btn-complete bold " style="padding:0px 5px 0px 5px;line-height: 15px;margin-top: -5px;" onclick="return ClearAlert('+$alerts.ID + ', 1);"> Delete Alert '
        $HTMLStr += '    </a>'
        if($alerts.device_name != null && $alerts.device_name != ""){
        $HTMLStr += '        <span class="fs-12 pull-right">'+$alerts.device_name+'</span>';
        }
        $HTMLStr += '    </strong>';
        $HTMLStr += '    <p>'+$alerts.Description;
        $HTMLStr += '    <span class="fs-12 pull-right">'+localDateFromate($createdOn,1)+'</span></p>';
        $HTMLStr += '</div>';
        
        return $HTMLStr;
    }
	
    function ClearAlert($dbID,$elm){
    
        // URL for the ajax call
        var $url = 'ajax.php';

        //JSON Data to server
        var $postdata = {
            'c'           : 'Settings', 
            'a'          : 'ClearAlert',
            'dbID'        : $dbID,
			'elm'        : $elm
        };

        //Success call back function for ajax
        var $successCB = function (data)
        {
			var $component = data.component;
			//alert("yyy");
            if($component != '' && $elm == 1){
                $createAlert({status: "info", title: "Waiting", text: "Successfully Removed Alert Request Queued."});
                $("#alert_content_"+$dbID).remove();
            }
			else if($component != '' && $elm == 2){
				//location.reload();
                $createAlert({status: "info", title: "Waiting", text: "Successfully Removed ALL Alerts Request Queued."});
				$(".panel-heading").addClass("hide");
				$("#alertContainer").html("<h5 class=\"text-center text-l-help\">No alerts found.</h5>");

            }
			else $createAlert({status: "fail", title: "Failed", text: "Failed to Removed Alert."});
        };

        //Error call back function for ajax
        var $errorCB = function () {
            $createAlert({status: "fail", title: "Failed", text: "Failed Ajax Alert Alert(s)."});
        };

        //Complete callback function for ajax
        var $completeCB = function () {  

        };

        //ajax call
        asyncPostToServer($url, $postdata, $successCB, $errorCB, $completeCB, false);
    }
    function retryPolicy($dbID){
    
        // URL for the ajax call
        var $url = 'ajax.php';

        //JSON Data to server
        var $postdata = {
            'c'           : 'LocationSettings', 
            'a'          : 'retryPolicy',
            'dbID'        : $dbID
        };

        //Success call back function for ajax
        var $successCB = function (data)
        {
            if(data.status == 1){
                $createAlert({status: "info", title: "Waiting", text: "Successfully queued Policy retry."});
                $("#alert_content_"+$dbID).remove();
            }
            else $createAlert({status: "fail", title: "Failed", text: "Failed to policy retry."});

        };

        //Error call back function for ajax
        var $errorCB = function () {
            $createAlert({status: "fail", title: "Failed", text: "Failed to policy retry."});
        };

        //Complete callback function for ajax
        var $completeCB = function () {

        };

        //ajax call
        asyncPostToServer($url, $postdata, $successCB, $errorCB, $completeCB, false);
    }
    
    
</script>