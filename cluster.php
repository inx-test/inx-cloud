
<?php //include_once("tpl/header.tpl.php");
include_once("tpl/dashboard-top.tpl.php"); ?>
<!-- START PAGE CONTENT -->
<div class="content clearfix  p-b-0">

    <?php include_once("tpl/leftTreeViewPanel.tpl.php"); ?>

    <div class="content-center ">
        <div class="panel panel-transparent m-b-0">    
            <div class="panel-heading ">
                <div class="panel-title bold fs-16">
                    Cluster
                </div>
            </div>
            <div class="panel-body">
                <div class="row m-t-10">
                    <div class="col-xl-4 col-l-4 col-md-4 col-sm-12">
                        <button class="btn btn-complete btn-cons bold btn-block" onclick="return $addToCluster.save();">Add to cluster list</button>
                    </div>
                    
                    <div class="col-xl-8 col-l-8 col-md-8 col-sm-12 p-t-10">
                        <span class="bold fs-16">Cluster : </span>
                        <span class="bold fs-16" id="lblClusterName"><span class="semi-bold text-warning">Please select a cluster on cluster panel</span></span>
                    </div>
                </div>
                <div class="row m-t-10" id="clusterShelf">
                    
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="hdnClusterID" name="hdnClusterID" value="">
    <?php include_once("tpl/rightTreeViewPanel.tpl.php"); ?>

</div>
<!-- END PAGE CONTENT -->
<?php include_once("tpl/footer.tpl.php"); ?>
<script src="js/jquery.path.js" type="text/javascript"></script>
<script type="text/javascript">
    $nodeAddToStatus =1;
    $(document).ready(function () {
        $ClusterTree.init(0);
    });
</script>
