-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.40-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table inspexto_ems_cloud.pmi_client
CREATE TABLE IF NOT EXISTS `pmi_client` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(50) DEFAULT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `added_by` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  `deleted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `email` (`email`),
  KEY `deleted` (`deleted`),
  KEY `added_by` (`added_by`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Dumping data for table inspexto_ems_cloud.pmi_client: ~6 rows (approximately)
DELETE FROM `pmi_client`;
/*!40000 ALTER TABLE `pmi_client` DISABLE KEYS */;
INSERT INTO `pmi_client` (`id`, `firstname`, `lastname`, `email`, `phone`, `added_by`, `deleted`, `deleted_by`) VALUES
	(1, 'Jeff ', 'Moris ', 'akkhalis@gmail.com', '0471248798', 1001, 1, 1000),
	(2, 'Joe1', 'Robbin', 'ak@inspextor.com', '0471248798', 1001, 1, 1000),
	(3, 'SUpervisor', '14', 'ak@mhtlighting.com', '888989988', 1000, 0, NULL),
	(4, 'test', 'test2', 't@T.com', '888778877', 1000, 0, NULL),
	(7, 'A', 'P', 'a@y.com', '', 0, 0, NULL),
	(8, 'A1', 'P1', 'a1@y.com', '', 0, 0, NULL);
/*!40000 ALTER TABLE `pmi_client` ENABLE KEYS */;

-- Dumping structure for table inspexto_ems_cloud.pmi_inx_device
CREATE TABLE IF NOT EXISTS `pmi_inx_device` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `device_name` varchar(100) DEFAULT NULL,
  `inspextorID` varchar(30) DEFAULT NULL,
  `Password` varchar(30) DEFAULT NULL,
  `Status` tinyint(4) DEFAULT '0' COMMENT '0:Inactie,1:Active',
  `Claimed` enum('Y','N') DEFAULT 'N',
  `DataDuration` int(11) DEFAULT NULL,
  `DataSpaceLimit` int(11) DEFAULT NULL,
  `ReportMailID` varchar(250) DEFAULT NULL,
  `ClientID` int(11) DEFAULT '0',
  `LastNotified` timestamp NULL DEFAULT NULL,
  `CreatedBy` bigint(20) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0',
  `deleted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `deleted` (`deleted`),
  KEY `inspextorID` (`inspextorID`),
  KEY `Password` (`Password`),
  KEY `deleted_by` (`deleted_by`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Dumping data for table inspexto_ems_cloud.pmi_inx_device: ~5 rows (approximately)
DELETE FROM `pmi_inx_device`;
/*!40000 ALTER TABLE `pmi_inx_device` DISABLE KEYS */;
INSERT INTO `pmi_inx_device` (`id`, `device_name`, `inspextorID`, `Password`, `Status`, `Claimed`, `DataDuration`, `DataSpaceLimit`, `ReportMailID`, `ClientID`, `LastNotified`, `CreatedBy`, `CreatedOn`, `deleted`, `deleted_by`) VALUES
	(1, 'Workstation 500', 'STN-501', 'password', 1, 'N', 1, 1, 'a_param@yahoo.com', 0, '0000-00-00 00:00:00', 1000, '2018-06-18 00:00:00', 0, 1000),
	(5, 'PMI_SUP14', 'PMI_SUP14', '12345678', 1, 'N', 30, 100, 'ak@mhtlighting.com', 0, '0000-00-00 00:00:00', 1000, '2018-06-20 00:00:00', 0, 0),
	(6, 'inx-mht ', '12345', 'password', 1, 'N', 2, 200, 'ak@mhtlighting.com', 0, '0000-00-00 00:00:00', 1000, '2020-07-21 00:00:00', 0, 0),
	(7, 'ArTest1', 'Ar12345', 'password', 1, 'Y', 1, 10, 'a@y.com', 0, '0000-00-00 00:00:00', 1000, '2020-11-03 00:00:00', 0, 0),
	(8, 'TestInstance1', '3082349999982398ba1545cc6df778', '308238500001607469e7a440fe73c3', 0, 'Y', 1, 1, 'ak@y.com', 0, '0000-00-00 00:00:00', 1000, '2020-11-10 00:00:00', 0, 0);
/*!40000 ALTER TABLE `pmi_inx_device` ENABLE KEYS */;

-- Dumping structure for table inspexto_ems_cloud.pmi_inx_device_db
CREATE TABLE IF NOT EXISTS `pmi_inx_device_db` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `DeviceID` bigint(20) DEFAULT NULL,
  `DeviceDBName` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `DeviceID` (`DeviceID`),
  KEY `DeviceDBName` (`DeviceDBName`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Dumping data for table inspexto_ems_cloud.pmi_inx_device_db: ~4 rows (approximately)
DELETE FROM `pmi_inx_device_db`;
/*!40000 ALTER TABLE `pmi_inx_device_db` DISABLE KEYS */;
INSERT INTO `pmi_inx_device_db` (`ID`, `DeviceID`, `DeviceDBName`) VALUES
	(1, 1, 'poe_1'),
	(5, 5, 'poe_2'),
	(6, 6, 'poe_3'),
	(7, 7, 'inspextor_local_7'),
	(8, 8, 'inspextor_local_8');
/*!40000 ALTER TABLE `pmi_inx_device_db` ENABLE KEYS */;

-- Dumping structure for table inspexto_ems_cloud.pmi_role_master
CREATE TABLE IF NOT EXISTS `pmi_role_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(20) DEFAULT NULL,
  `dev_role` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table inspexto_ems_cloud.pmi_role_master: ~4 rows (approximately)
DELETE FROM `pmi_role_master`;
/*!40000 ALTER TABLE `pmi_role_master` DISABLE KEYS */;
INSERT INTO `pmi_role_master` (`id`, `role_name`, `dev_role`) VALUES
	(1, 'Administrator', 1),
	(2, 'Assistant', 0),
	(3, 'User', 2),
	(4, 'Super Administrator', 3);
/*!40000 ALTER TABLE `pmi_role_master` ENABLE KEYS */;

-- Dumping structure for table inspexto_ems_cloud.pmi_user
CREATE TABLE IF NOT EXISTS `pmi_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(50) DEFAULT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `active` int(11) DEFAULT '0',
  `clientId` int(11) DEFAULT '0',
  `added_by` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  `deleted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `email` (`email`),
  KEY `active` (`active`),
  KEY `deleted` (`deleted`),
  KEY `username` (`username`),
  KEY `password` (`password`),
  KEY `added_by` (`added_by`)
) ENGINE=InnoDB AUTO_INCREMENT=1021 DEFAULT CHARSET=latin1;

-- Dumping data for table inspexto_ems_cloud.pmi_user: ~7 rows (approximately)
DELETE FROM `pmi_user`;
/*!40000 ALTER TABLE `pmi_user` DISABLE KEYS */;
INSERT INTO `pmi_user` (`id`, `firstname`, `lastname`, `email`, `phone`, `username`, `password`, `active`, `clientId`, `added_by`, `deleted`, `deleted_by`) VALUES
	(1000, 'Super', 'Administrator', 'pmisupervisor1@gmail.com', '8888', 'sadmin', 'sadmin', 1, NULL, NULL, 0, NULL),
	(1001, 'Tino', 'pomolux', 'tino@pomolux.com', '8887876655', 'tpomolux', 'password', 1, 1, 1000, 1, 1001),
	(1002, 'don', 'cannella', 'dcannella@mhtlighting.com', '888989988', 'dcannella', 'password', 1, 3, 1000, 0, NULL),
	(1003, 'ak', 'khalis', 'ak@mhtlighting.com', '888989988', 'akramino', 'password', 1, 3, 1000, 0, NULL),
	(1004, 'Vince', 'Murphy', 'vincem@nameenergygroup.com', '888998899', 'vincem', 'password', 1, 3, 1000, 0, NULL),
	(1019, 'A', 'P', 'a@y.com', '', 'a@y.com', 'passme', 1, 7, 0, 0, NULL),
	(1020, 'A1', 'P1', 'a1@y.com', '', 'a1@y.com', 'pass', 1, 8, 0, 0, NULL);
/*!40000 ALTER TABLE `pmi_user` ENABLE KEYS */;

-- Dumping structure for table inspexto_ems_cloud.pmi_user_roles
CREATE TABLE IF NOT EXISTS `pmi_user_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `role_id` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- Dumping data for table inspexto_ems_cloud.pmi_user_roles: ~10 rows (approximately)
DELETE FROM `pmi_user_roles`;
/*!40000 ALTER TABLE `pmi_user_roles` DISABLE KEYS */;
INSERT INTO `pmi_user_roles` (`id`, `user_id`, `role_id`) VALUES
	(1, 1000, 4),
	(2, 1001, 4),
	(3, 1002, 1),
	(4, 1003, 1),
	(5, 1004, 1),
	(6, 1016, 1),
	(7, 1017, 1),
	(8, 1018, 1),
	(9, 1019, 1),
	(10, 1020, 1);
/*!40000 ALTER TABLE `pmi_user_roles` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
