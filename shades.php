<?php //include_once("tpl/header.tpl.php");
include_once("tpl/dashboard-top.tpl.php"); 

 ?>
 <style>
.slidecontainer {
    width: 100%;
}

.slider {
    -webkit-appearance: none;
    width: 100%;
    height: 15px;
    border-radius: 5px;
    background: #d3d3d3;
    outline: none;
    opacity: 0.7;
    -webkit-transition: .2s;
    transition: opacity .2s;
}

.slider:hover {
    opacity: 1;
}

.slider::-webkit-slider-thumb {
    -webkit-appearance: none;
    appearance: none;
    width: 25px;
    height: 25px;
    border-radius: 50%;
    background: #4CAF50;
    cursor: pointer;
}

.slider::-moz-range-thumb {
    width: 25px;
    height: 25px;
    border-radius: 50%;
    background: #4CAF50;
    cursor: pointer;
}
 </style>
<!-- START PAGE CONTENT -->
<div class="content clearfix p-b-0">
	

        <div class="panel panel-transparent">    
            <div class="panel-heading ">
                <div class="panel-title bold fs-16">
                    Shade control
                </div>
            </div>
            <div class="panel-body">
			<div class="col-md-12 text-center">
			    
                                        <!-- Drop down starts for device --->
					<li>
						<select  data-init-plugin="select2" id="selDeviceType" style="width: 100px;" onchange="$clientReport.__setDevice();">
							<option value="1">All</option>
							<!--option value="2">Fixture</option-->
							<option value="3">Cluster</option>
						</select>
					</li>
					<!-- Drop down ends --->
					
					<!-- Drop down starts for device --->
					<li>
						<select data-init-plugin="select2" id="selDev" style="width: auto;"  disabled onchange="$clientReport.__selectFix(this.value);">
							<option value="-1">All Clusters</option>
						</select>
					</li>
					
					<li>
						<select data-init-plugin="select2" id="selFix" style="width: auto;"  disabled >
							<option value="-1">All Shades</option>
						</select>
					</li>
					
					<br>
					<br>
					<!-- Drop down ends --->
					<input type="text" name="ipaddress" value="192.168.1.8" placeholder="IP Address">

               
				</div>
			    <!--div class="row">
                    <div class="col-md-12 text-center">
                        <button class="btn btn-complete btn-cons bold btn-cons" onclick="return $clientReport.__send(210);">Light ON </button>
                    </div>
                </div>
				<br>
			    <div class="row">
                    <div class="col-md-12 text-center">
                        <button class="btn btn-complete btn-cons bold btn-cons" onclick="return $clientReport.__send(211,0);">Light OFF</button>
                    </div>
                </div-->
				<br>
				<br>
				<br>
				<div class="row">
                    <div class="col-md-20 text-center">
                        <button class="btn btn-complete btn-cons bold btn-cons" onclick="return $clientReport.__send(1,0);"> UP</button>
                    </div>
                </div>
				<br>
	
				<div class="row">
                    <div class="col-md-20 text-center">
                        <button class="btn btn-complete btn-cons bold btn-cons" onclick="return $clientReport.__send(2,0);"> DOWN</button>
                    </div>
                </div>
					<br>
				<div class="row">
			
					Select Position
					
                   <div class="""slidecontainer">
					<input type="range" min="1" max="100" value="50" class="slider" id="myRange" onchange="$clientReport.__send(3,this.value)">
					<p> <span id="demo"></span></p>
					</div>
                    <!--button class="btn btn-complete btn-cons bold btn-cons" onclick="return $clientReport.__send(111);">Light Dim </button-->
                </div>
				<br>
				<div class="row">
                    <div class="col-md-12 text-center">
                        <button class="btn btn-complete btn-cons bold btn-cons" onclick="return $clientReport.__send(4,0);"> STOP</button>
                    </div>
                </div>
				<br>

                </div>
            </div>
        </div>
    <input type="hidden" id="hdnClusterID" name="hdnClusterID" value="">
 
</div>
<!-- END PAGE CONTENT -->
<?php include_once("tpl/footer.tpl.php"); ?>
<script src="js/jquery.path.js" type="text/javascript"></script>

<script type="text/javascript">
    $mergePolicyStatus =1;
    $(document).ready(function () {
        $ClusterTree.init(0);
    });
	

var slider = document.getElementById("myRange");
var output = document.getElementById("demo");
output.innerHTML = slider.value;

slider.oninput = function() {
  output.innerHTML = this.value;
}




//-------------
	var $clientReport = {
		__setDevice :function (){

			var selVal = $("#selDeviceType").select2("val");

			var $options = '';
			$('#selDev').html('');   
			$('#selDev').select2('data', null);
			if( selVal == 1 ){
				$('#selDev').append('<option value="-1">All Clusters</option>');
				$('#selDev').select2('val', "-1");
				$("#selDev").prop("disabled", true);
				$('#selFix').append('<option value="-1">All Fixtures</option>');
				$('#selFix').select2('val', "-1");
				$("#selFix").prop("disabled", true);
			}
			else{    
				$options ="";
				$('#selDev').select2('data', null);
				$("#selDev").prop("disabled", false);
				if( selVal == 2) $ClusterTree.getAllTagGroup("#selDev"); 
				else if( selVal == 3){
					$ClusterTree.getAllCluster("#selDev");  
				}					
				$('#selDev').select2('val', "1");
			}
			

		},
		__selectFix :function (){
			$options ="";
				$('#selFix').select2('data', null);
				$("#selFix").prop("disabled", false);
			$ClusterTree.getAllTagGroup("#selFix");		
			//$('#selFix').select2('val', "1");

		},
		__send: function ($control,$dim) {
			var $type = $("#selDeviceType").val();
			var $clusterID = $("#selDev").val();
			var $tagID = $("#selFix").select2("val");
			
			//var $dim = $("#dimV").val();

                var $data = {
					'a': 'getLocalIPAddr',
					'c': 'LocationSettings',
					t:$type,
					cluster: $clusterID,
					p1: $dim,
					tag: $tagID,
					command:$control
                };

                $.ajax({
                    type: 'POST',
                    url: 'ajax.php',
                    data: $data,
                    dataType: 'json',
                    beforeSend: function () {
                        __showLoadingAnimation();
                    },
                    success: function (res) {
                        var $status = res.status;
                        if ($status == 1) {

                            $createAlert({status: "success", title: "Successfully", text: "Send Command "});
							__hideLoadingAnimation();
                        }
                        else {
                            $createAlert({status: "fail", title: "Failed", text: " To Send Command"});
							__hideLoadingAnimation();
                        }
                    },
                    complete: function () {
                        __hideLoadingAnimation();
                    },
                    error: function (r) {
                        __hideLoadingAnimation();
                        $createAlert({status: "fail", title: "Failed", text: r});
                    }
                });

        }
	}
</script>