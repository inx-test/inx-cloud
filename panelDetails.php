<?php 
include_once("tpl/header.tpl.php");  
?>
<div class="content">
    <!-- START PANEL -->
    <div class="panel panel-transparent">
        <div class="panel-heading ">
            <div class="panel-title">Panel Details
            </div>
        </div>
        <div class="panel-body">
            <!--Graphs-->
            <div class="row p-t-10 m-b-10">
                <!--Capacitance Graph -->
                <div class="col-sm-6 p-r-5">
                    <div id="portlet-advance" class="panel panel-default m-b-0 panel-savings">
                        <div class="panel-heading clearfix p-b-0">
                            <div class="panel-title title-metal">
                                Capacitance
                                <span class="Sub_deviceName fs-12">  <?php
                                   if($activeDevice != 0) echo ' - Dev Unit '.$activeDevice;
                                ?></span>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                              <div class="col-sm-4 text-center b-r-1">
                                    <input type="text" id="__capPh1">
                                    <div class="row clearfix p-t-5">
                                    <span class="tem-text">Cap Ph 1</span>
                                    </div>
                                </div>
                                <div class="col-sm-4 text-center b-r-1">
                                    <input type="text" id="__capPh2">
                                    <div class="row clearfix p-t-5">
                                        <span class="tem-text">Cap Ph 2</span>
                                    </div>
                                </div>
                                <div class="col-sm-4 text-center ">
                                    <input type="text" id="__capPh3">
                                    <div class="row clearfix p-t-5">
                                        <span class="tem-text">Cap Ph 3</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Capacitance Graph Ends -->
                
                <!--Temperature Graph -->
                <div class="col-sm-6 p-r-15 p-l-5">
                    <div id="portlet-advance" class="panel panel-default m-b-0 panel-savings">
                        <div class="panel-heading clearfix p-b-0">
                            <div class="panel-title title-metal">
                                Temperature
                                <span class="Sub_deviceName fs-12">  <?php
                                   if($activeDevice != 0) echo ' - Dev Unit '.$activeDevice;
                                ?></span>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                              <!--div class="col-sm-4 text-center b-r-1">
                                    <input type="text" id="__temMax">
                                    <div class="row clearfix p-t-5">
                                    <span class="tem-text">Precision &#8457;</span>
                                    </div>
                                </div-->
                                <div class="col-sm-6 text-center b-r-1">
                                    <input type="text" id="__temint">
                                    <div class="row clearfix p-t-5">
                                        <span class="tem-text">Int &#8457;</span>
                                    </div>
                                </div>
                                <div class="col-sm-6 text-center ">
                                    <input type="text" id="__temAmb">
                                    <div class="row clearfix p-t-5">
                                        <span class="tem-text">Amb &#8457;</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Temperature Graph Ends -->
            </div>
            
             
            <div class="panel m-b-0 panel-savings no-margin">
                <div class="panel-body">
                    <div class="bg-white p-t-5 p-b-0 p-l-15 p-r-15" id="panel-details"> 
                        <!--row 1-->
                        <div class="row p-t-10 p-b-10">
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">Time:</label>
                                <label class="param-value bold pull-right" id="time"></label>
                            </div>
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">Date:</label>
                                <label class="param-value bold pull-right" id="date"></label>
                            </div>
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">Serial #:</label>
                                <label class="param-value bold pull-right" id="serial"></label>
                            </div>
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">Main Rev:</label>
                                <label class="param-value bold pull-right" id="mainRev"></label>
                            </div>
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">Inter Rev:</label>
                                <label class="param-value bold pull-right" id="interRev"></label>
                            </div>
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">Meter Rev:</label>
                                <label class="param-value bold pull-right" id="meterRev"></label>
                            </div>
                        </div>
                        <!--row 2-->
                        <div class="row p-t-15 p-b-10 bg-grey">
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">3 Ph Volt:</label>
                                <label class="param-value bold pull-right" id="3PhVolt"></label>
                            </div>
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">Volt Ph1:</label>
                                <label class="param-value bold pull-right" id="VoltPh1"></label>
                            </div>
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">Volt Ph2:</label>
                                <label class="param-value bold pull-right" id="VoltPh2"></label>
                            </div>
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">Volt Ph3:</label>
                                <label class="param-value bold pull-right" id="VoltPh3"></label>
                            </div>
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">Min24h 3PhV:</label>
                                <label class="param-value bold pull-right" id="Min3PhVolt"></label>
                            </div>
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">Max24h 3PhV:</label>
                                <label class="param-value bold pull-right" id="Max3PhVolt"></label>
                            </div>
                        </div>
                        <!--row 3-->
                        <div class="row p-t-15 p-b-10">
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">3 Ph Amps:</label>
                                <label class="param-value bold pull-right" id="3PhAmps"></label>
                            </div>
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">Amps Ph1:</label>
                                <label class="param-value bold pull-right" id="AmpsPh1"></label>
                            </div>
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">Amps Ph2:</label>
                                <label class="param-value bold pull-right" id="AmpsPh2"></label>
                            </div>
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">Amps Ph3:</label>
                                <label class="param-value bold pull-right" id="AmpsPh3"></label>
                            </div>
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">Min24h 3PhA:</label>
                                <label class="param-value bold pull-right" id="Min3PhAmps"></label>
                            </div>
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">Max24h 3PhA:</label>
                                <label class="param-value bold pull-right" id="Max3PhAmps"></label>
                            </div>
                        </div>
                        <!--row 4-->
                        <div class="row p-t-15 p-b-10 bg-grey">
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">Avr 3Ph PF:</label>
                                <label class="param-value bold pull-right" id="Avr3PhPF"></label>
                            </div>
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">Ph1 PF:</label>
                                <label class="param-value bold pull-right" id="AvrPh1PF"></label>
                            </div>
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">Ph2 PF:</label>
                                <label class="param-value bold pull-right" id="AvrPh2PF"></label>
                            </div>
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">Ph3 PF:</label>
                                <label class="param-value bold pull-right" id="AvrPh3PF"></label>
                            </div>
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">Min24h 3Ph PF:</label>
                                <label class="param-value bold pull-right" id="MinAvr3PhPF"></label>
                            </div>
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">Max24h 3Ph PF:</label>
                                <label class="param-value bold pull-right" id="MaxAvr3PhPF"></label>
                            </div>
                        </div>
                        <!--row 5-->
                        <div class="row p-t-15 p-b-10">
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">Appar 3Ph Pwr:</label>
                                <label class="param-value bold pull-right" id="Appar3PhPwr"></label>
                            </div>
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">Appar Ph1 Pwr:</label>
                                <label class="param-value bold pull-right" id="ApparPh1Pwr"></label>
                            </div>
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">Appar Ph2 Pwr:</label>
                                <label class="param-value bold pull-right" id="ApparPh2Pwr"></label>
                            </div>
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">Appar Ph3 Pwr:</label>
                                <label class="param-value bold pull-right" id="ApparPh3Pwr"></label>
                            </div>
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">System Status:</label>
                                <label class="param-value bold pull-right" id="sysStatus"></label>
                            </div>
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">Soft Lock:</label>
                                <label class="param-value bold pull-right" id="softLock"></label>
                            </div>
                        </div>
                        <!--row 6-->
                        <div class="row p-t-15 p-b-10 bg-grey">
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">Real 3Ph Pwr:</label>
                                <label class="param-value bold pull-right" id="Real3PhPwr"></label>
                            </div>
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">Real Ph1 Pwr:</label>
                                <label class="param-value bold pull-right" id="RealPh1Pwr"></label>
                            </div>
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">Real Ph2 Pwr:</label>
                                <label class="param-value bold pull-right" id="RealPh2Pwr"></label>
                            </div>
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">Real Ph3 Pwr:</label>
                                <label class="param-value bold pull-right" id="RealPh3Pwr"></label>
                            </div>
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">Multiplier:</label>
                                <label class="param-value bold pull-right" id="multiplier"></label>
                            </div>
                        </div>
                        <!--row 7-->
                        <div class="row p-t-15 p-b-10">
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">React 3Ph Pwr:</label>
                                <label class="param-value bold pull-right" id="React3PhPwr"></label>
                            </div>
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">React Ph1 Pwr:</label>
                                <label class="param-value bold pull-right" id="ReactPh1Pwr"></label>
                            </div>
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">React Ph2 Pwr:</label>
                                <label class="param-value bold pull-right" id="ReactPh2Pwr"></label>
                            </div>
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">React Ph3 Pwr:</label>
                                <label class="param-value bold pull-right" id="ReactPh3Pwr"></label>
                            </div>
                        </div>
                        <!--row 8-->
                        <div class="row p-t-15 p-b-10 bg-grey">
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">Voltage Imb:</label>
                                <label class="param-value bold pull-right" id="VoltageImb"></label>
                            </div>
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">Current Imb:</label>
                                <label class="param-value bold pull-right" id="CurrentImb"></label>
                            </div>
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">Line Frq:</label>
                                <label class="param-value bold pull-right" id="LineFrq"></label>
                            </div>
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">Max 24h Vimb:</label>
                                <label class="param-value bold pull-right" id="Max24hVimb"></label>
                            </div>
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">Max 24h Aimb:</label>
                                <label class="param-value bold pull-right" id="Max24hAimb"></label>
                            </div>
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">Neutral Amps:</label>
                                <label class="param-value bold pull-right" id="neutralAmps"></label>
                            </div>
                        </div>
                        <!--row 9-->
                        <div class="row p-t-15 p-b-10">
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">Volts THD:</label>
                                <label class="param-value bold pull-right" id="VoltsTHD"></label>
                            </div>
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">Amps THD:</label>
                                <label class="param-value bold pull-right" id="AmpsTHD"></label>
                            </div>
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">Peak V Harm:</label>
                                <label class="param-value bold pull-right" id="PeakVHarm"></label>
                            </div>
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">Peak VH Frq:</label>
                                <label class="param-value bold pull-right" id="PeakVHFrq"></label>
                            </div>
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">Peak A Harm:</label>
                                <label class="param-value bold pull-right" id="PeakAHarm"></label>
                            </div>
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">Peak AH Frq:</label>
                                <label class="param-value bold pull-right" id="PeakAHFrq"></label>
                            </div>
                        </div>
			
                         <!--row 10-->
                        <div class="row p-t-15 p-b-10">
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">1st Harmonics:</label>			
                                <label class="param-value bold pull-right" id="FirstHarm"></label>
                            </div>
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">3rd Harmonics:</label>
                                <label class="param-value bold pull-right" id="ThirdHarm"></label>
                            </div>
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">5th Harmonics:</label>
                                <label class="param-value bold pull-right" id="FifthHarm"></label>
                            </div>
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">7th Harmonics:</label>
                                <label class="param-value bold pull-right" id="SeventhHarm"></label>
                            </div>
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">9th Harmonics:</label>
                                <label class="param-value bold pull-right" id="NinthHarm"></label>
                            </div>
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">11th Harmonics:</label>
                                <label class="param-value bold pull-right" id="EleventhHarm"></label>
                            </div>
                        </div>
                        <!--row 11-->
                        <div class="row p-t-15 p-b-10 bg-grey">
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">V1 Cal. Slope:</label>
                                <label class="param-value bold pull-right" id="V1CalSlope"></label>
                            </div>
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">V2 Cal. Slope:</label>
                                <label class="param-value bold pull-right" id="V2CalSlope"></label>
                            </div>
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">V3 Cal. Slope:</label>
                                <label class="param-value bold pull-right" id="V3CalSlope"></label>
                            </div>
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">A1 Cal. Slope:</label>
                                <label class="param-value bold pull-right" id="A1CalSlope"></label>
                            </div>
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">A2 Cal. Slope:</label>
                                <label class="param-value bold pull-right" id="A2CalSlope"></label>
                            </div>
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">A3 Cal. Slope:</label>
                                <label class="param-value bold pull-right" id="A3CalSlope"></label>
                            </div>
                        </div>
                        <!--row 12-->
                        <div class="row p-t-15 p-b-10">
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">V1 Cal. Inter:</label>
                                <label class="param-value bold pull-right" id="V1CalInter"></label>
                            </div>
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">V2 Cal. Inter:</label>
                                <label class="param-value bold pull-right" id="V2CalInter"></label>
                            </div>
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">V3 Cal. Inter:</label>
                                <label class="param-value bold pull-right" id="V3CalInter"></label>
                            </div>
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">A1 Cal. Inter:</label>
                                <label class="param-value bold pull-right" id="A1CalInter"></label>
                            </div>
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">A2 Cal. Inter:</label>
                                <label class="param-value bold pull-right" id="A2CalInter"></label>
                            </div>
                            <div class="col-sm-2 p-l-5 p-r-5">
                                <label class="bold param-label">A3 Cal. Inter:</label>
                                <label class="param-value bold pull-right" id="A3CalInter"></label>
                            </div>
                        </div>
                        <!--row 13-->
                        <div class="row p-t-15 p-b-10 bg-grey">
                             <div class="col-sm-3 p-l-5 p-r-5">
                                <label class="bold param-label">Banks Status</label>
                                <div class="pull-right bold banks-status" id="bank0">Bank-0 N/A</div>
                            </div>
                            <div class="col-sm-3 p-l-5 p-r-5">
                                <div class="bold banks-status pull-left" id="bank1">Bank-1 N/A</div>
                                <div class="banks-status bold" id="bank2">Bank-2 N/A</div>
                            </div>
                            <div class="col-sm-3 p-l-5 p-r-5">
                                <div class="bold banks-status pull-left" id="bank3">Bank-3 N/A</div>
                                <div class="pull-right bold banks-status" id="bank4">Bank-4 N/A</div>
                            </div>
                            <div class="col-sm-3 p-l-5 p-r-5">
                                <div class="bold banks-status pull-left" id="bank5">Bank-5 N/A</div>
                                <div class="pull-right bold banks-status" id="bank6">Bank-6 N/A</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END PANEL -->
 
        
    
    
<?php include_once("tpl/footer.tpl.php"); ?>

<script type="text/javascript">
    $(document).ready(function(){
        
        //__intknobBar("#__temMax",0,"#99F969");
        __intknobBar("#__temint",0,"#99F969");
        __intknobBar("#__temAmb",0,"#99F969");
        
        __intknobBarCaps("#__capPh1",0,"#99F969");
        __intknobBarCaps("#__capPh2",0,"#99F969");
        __intknobBarCaps("#__capPh3",0,"#99F969");
        
        $(".content").css("min-height",($(window).height()));
        
        $(".param-value").html("Nil");
        setLogData();
    });
</script>
