<?php
include "config.php";
//include_once("tpl/header.tpl.php");
include_once("tpl/dashboard-top.tpl.php"); 
?>
<div class="content clearfix p-b-0">
    <?php include_once("tpl/leftTreeViewPanel.tpl.php"); ?>
    <!-- START PANEL -->
    <div class="content-center">
        <div class="panel panel-transparent">
            <div class="panel-heading ">
                <div class="panel-title"></div>
            </div>
            <div class="panel-body">
                <div class="col-sm-2"></div>
                <div class="col-sm-8">
                    
                    <div class="row">
                        <div id="portlet-advance " class="panel panel-default  panel-savings">
                            <div class="panel-heading p-t-15 bold fs-16 upper b-b b-grey">
                                Policy Definition
                            </div>
                            <div class="panel-body">
                                <!-- START POLICY CREATION CONTAINER -->
                                <?php include_once("tpl/policyContent.tpl.php"); ?>
                                <!-- END POLICY CREATION CONTAINER -->
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
            <div class="row hide" id="policyFilterContainer">
                <div class="col-md-12 text-center">
                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-8 m-t-10 filterTitle" >
                            <h5 class="bold"> Showing policy for cluster : <span id="lblCluster"></span></h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 m-t-5">
                            <a href="javascript:void(0);" onclick="return $Policy.getAll();" class="bold"> <i class="fa fa-arrow-left" aria-hidden="true"></i> Back to default</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include_once("tpl/rightTreeViewPanel.tpl.php"); ?>
</div>
<!-- END PANEL -->

<?php include_once("tpl/footer.tpl.php"); ?>

<script type="text/javascript">
    var loggedUserId = <?php echo $loggedInUserId; ?>;
    var loggedUserRole = <?php echo $loggedUserRoleId; ?>;
    $policyStatus = 1;

    $(document).ready(function () {
        $ClusterTree.init(0);
        $('[data-toggle="tooltip"]').tooltip();
        $Policy.int();
        $Policy.getAll();
    });

    
</script>




