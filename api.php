<?php

include_once("config.php");
require_once '_classes/DBC.php';

$a = $_REQUEST["a"];
if ($a == "setIP") {
    // Read the IP Address
    $result = $_REQUEST["ip"];
	//file_put_contents("files/-req-" . time() . "-ip.txt", $result);
    //$result = file_get_contents("./files/apiContent.txt");
    //die($result);
    if (!empty($result)) {
        // Instead it will save to database

        $db->autocommit(false);
        try {
            //$ = file_get_contents("./files/1486360482-ip.txt");
            if (!empty($result)) {
                $devID = 0;
                $macAdd = "";
                $assemblyNo = "";
                $serialNo = "";
                $modelNo = "";
                $systemSerialNo = "";
                $ipAddress = "";
                $existsDevice = array();

                $rsSplitSectionArr = explode("*****", $result);
                //file_put_contents("files/-api-data-" . time() . "-ip.txt", $result);
                //echo sizeof($rsSplitSectionArr);
				 if (isset($rsSplitSectionArr[0])) {
                    $strRemoveSpChar = str_replace('!', '', $rsSplitSectionArr[0]);
                    $rslineArr = explode("\n", $strRemoveSpChar);
                    foreach ($rslineArr as $key => $value) {
                        if (preg_match('/\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/', $value, $ipMatch))
                            $ipAddress = $ipMatch[0];
                    }
                }
				
				//file_put_contents("files/-api-data-IP" . time() . "-ip.txt", $ipAddress);
				
                if (isset($rsSplitSectionArr[1])) {
                    $strRemoveSpChar = str_replace('!', '', $rsSplitSectionArr[1]);
                    $rslineArr = explode("\n", $strRemoveSpChar);
                    foreach ($rslineArr as $key => $value) {
                        //echo $value." \n\n";
                        if (preg_match("#^Base Ethernet MAC Address(.*)$#i", $value)) {
                            $splitSpace = explode(" : ", trim($value));
                            if (sizeof($splitSpace) > 1)
                                $macAdd = trim($splitSpace[1]);
                            //echo 'Mac === '.$macAdd;
                        }
                        else if (preg_match("#^Motherboard Assembly Number(.*)$#i", $value)) {
                            $splitSpace = explode(":", trim($value));
                            if (sizeof($splitSpace) > 1)
                                $assemblyNo = trim($splitSpace[1]);
                            //echo 'assemblyNo === '.$assemblyNo;
                        }
                        else if (preg_match("#^Motherboard Serial Number(.*)$#i", $value)) {
                            $splitSpace = explode(":", trim($value));
                            if (sizeof($splitSpace) > 1)
                                $serialNo = trim($splitSpace[1]);
                            //echo 'serialNo === '.$serialNo;
                        }
                        else if (preg_match("#^Model Number(.*)$#i", $value)) {
                            $splitSpace = explode(":", trim($value));
                            if (sizeof($splitSpace) > 1)
                                $modelNo = trim($splitSpace[1]);
                            //echo 'modelNo === '.$modelNo;
                        }
                        else if (preg_match("#^System Serial Number(.*)$#i", $value)) {
                            $splitSpace = explode(":", trim($value));
                            if (sizeof($splitSpace) > 1)
                                $systemSerialNo = trim($splitSpace[1]);
                            //echo 'systemSerialNo === '.$systemSerialNo;
                        }
                    }
                     /*echo '<pre>';
                      echo $macAdd."\n";
                      echo $assemblyNo."\n";
                      echo $serialNo."\n";
                      echo $modelNo."\n";
                      echo $systemSerialNo."\n";
                      //print_r($rslineArr);
                      echo '</pre>'; */
                }

                 /*echo '<pre>';
                  echo $ipAddress."\n";
                  echo '</pre>'; */
                if (($macAdd != "" && $macAdd != NULL) && ($ipAddress != "" && $ipAddress != NULL)) {

                    $sql = "SELECT id,device_name,serial_no,ip_address,mac_address,assembly_no,model_no,serial_no_sys FROM pmi_device WHERE mac_address='$macAdd' AND serial_no_sys ='$systemSerialNo'";
                    $existsDevice = $db->get_result($sql);
                    //print_r($existsDevice);
					//file_put_contents("files/-api-data-IP" . time() . "-ip.txt", JSON.stringify( $existsDevice) );
                    //echo '<<<<<>>>>>';
                    if (sizeof($existsDevice) > 0) {
                        $devID = $existsDevice[0]["id"];
                        $recArr = array();
                        //if (urlencode($deviceName) != $existsDevice[0]["device_name"])
                            //$recArr["device_name"] = urlencode($deviceName);
                        if ($serialNo != $existsDevice[0]["serial_no"])
                            $recArr["serial_no"] = $serialNo;
                        if ($ipAddress != $existsDevice[0]["ip_address"])
                            $recArr["ip_address"] = $ipAddress;
                        if ($macAdd != $existsDevice[0]["mac_address"])
                            $recArr["mac_address"] = $macAdd;
                        if ($assemblyNo != $existsDevice[0]["assembly_no"])
                            $recArr["assembly_no"] = $assemblyNo;
                        if ($modelNo != $existsDevice[0]["model_no"])
                            $recArr["model_no"] = $modelNo;
                        if ($systemSerialNo != $existsDevice[0]["serial_no_sys"])
                            $recArr["serial_no_sys"] = $systemSerialNo;
                        if (count($recArr) > 0)
                            $devUpdateStat = $db->update_query($recArr, "pmi_device", "id = $devID");
                    }
                    else {
                        //$recArr["device_name"] = urlencode($deviceName);
                        $recArr["type"] = 1;
                        $recArr["serial_no"] = $serialNo;
                        $recArr["ip_address"] = $ipAddress;
                        $recArr["mac_address"] = $macAdd;
                        $recArr["assembly_no"] = $assemblyNo;
                        $recArr["model_no"] = $modelNo;
                        $recArr["serial_no_sys"] = $systemSerialNo;
                        //print_r($recArr);
                        $insertStat = $db->insert_query($recArr, "pmi_device");
                        if ($insertStat)
                            $devID = $db->get_insert_id();
                        //echo 'Inserted ID =='.$devID."\n";
                    }
                }
                //echo 'here';
                $db->commit();
                // Reply with Y
                echo "Y";
            }
        } catch (Exception $ex) {
            echo "N";
            $db->rollback();
        }
        $db->close();
    } else {
        // Reply with Y
        echo "N";
    }
}
?>
