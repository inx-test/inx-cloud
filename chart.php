
<?php
	require_once('tpl/header.tpl.php')
?>

   <div class="content clearfix">
       
    <?php include_once("tpl/leftTreeViewPanel.tpl.php"); ?>
       <div class="content-left">
   <button type="button" class="btn btn-snapshot btn-blue pull-right m-t-5 m-r-5" 
            data-toggle="tooltip" data-trigger="hover" data-placement="bottom" title="Snapshot" onclick="return Sceenshot();" >
            <i class="fa fa-camera fa-lg"></i>
        </button>
      <!-- START PANEL -->
       <div class="panel panel-transparent">    
         <!--div class="panel-heading ">
            <div class="panel-title full-width">
                <h5 class="semi-bold bold m-l-25 m-b-0 m-t-0 text-center">
                   Chart
                </h5>
            </div>
           <div class="panel-controls back-db" style="display:none;">
              <ul>
                <li><a data-toggle="tooltip" data-placement="left" title="Back to Dashboard" class="portlet-close" href="dashboard.php"><i class="fa fa-arrow-left" style="color:white;"></i></a>
                </li>
              </ul>
          </div>
         </div-->
         <div class="panel-body">
            <div id="portlet-chart" class="panel panel-default panel-savings">
            <div class="panel-body">
                <div class='chart-param col-sm-12 btn-indic'>
                        <div class='col-sm-6 padding-0'>
                            <div class="form-group form-group-default form-group-default-select2 m-b-0" id="divDevice" style="padding:2px;">
                                <label>Node</label>
                                <select class=" full-width" id="chartSelDevice" onchange="return selectValidation('chartSelDevice','device','divDevice');">
                                </select>
                                <span class="sp_error text-danger hide"></span>
                            </div>
                            <div class="form-group form-group-default form-group-default-select2 m-b-0" id="divParameter" onchange="return multiSelValidation('chartParamSelection','parameter','divParameter');">
                                <label>Chart Parameters</label>
                                <select class=" full-width" multiple id="chartParamSelection">
                                </select>
                                <div id="sel-param-error-box"></div>
                                <span class="sp_error text-danger hide"></span>
                            </div>
                        </div>
                        <div class='col-sm-3 padding-0'>
                            <div class="form-group form-group-default form-group-default-select2 m-b-0" style="padding:2px;">
                                <label>Chart Type</label>
                                <select class=" full-width" data-init-plugin="select2" id="selChartType" onchange="return changeChartType(this.value);">
                                  <option value="1" selected>Bar Chart</option>
                                  <option value="2" >Area Chart</option>
                                  <option value="3">Line Chart</option>
                                </select>
                            </div>
                            <!--div class="form-group">
                                <div class='input-group date' id="dateRange">
                                    <label>Date & Time</label>
                                    <input type='text' class="form-control" />
                                    <span class="input-group-addon" style="height: 56px;">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                </div>
                            </div-->
                            <div class="form-group form-group-default input-group priventClick" style="overflow:auto !important;">
                                    <label>Start date & Time</label>
                                    <input type="text" class="form-control" placeholder="Pick a date" id="dateRange">
                                    <span class="input-group-addon" style="height: 56px;">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                              </div>
                        </div>
                        <div class='col-sm-3 padding-0'>
                            <div class="clearfix" style="height: 60px;">
                                <div class="form-group form-group-default form-group-default-select2 m-b-0" style="padding:2px;">
                                    <label>Duration</label>
                                    <select class=" full-width" data-init-plugin="select2" id="selDuration">
                                      <optgroup label="Backward">
                                    		 <option value="1m" selected>15 min - 5 sec interval</option>
                                    	</optgroup>
                                    	<optgroup label="Forward">
		                                      <option value="5m">5 min - 5 sec interval</option>
		                                      <option value="30m" >30 min - 1 min interval</option>
		                                      <option value="1h">1 hr - 1 min interval</option>
		                                      <option value="12h">12 hrs - 5 min interval</option>
		                                      <option value="24h">24 hrs - 5 min interval</option>
		                                      <option value="7d">7 days - 1 hrs interval</option>
		                                      <option value="30d">30 days - 4 hrs interval</option>
		                                      <option value="1y">One year 1 day interval</option>
                                      	</optgroup>
                                    </select>
                                </div>
                            </div>
                            <div class="clearfix padding-0">
                                <button class="btn m-t-20 btn-complete pull-right btn-generateChart" onclick="__getChartJSON();">Generate Chart</button>
                                <div class="pull-right back-to-chart hide m-r-10">
                                    <button type="button" class="btn m-t-20 btn-tag-light p-l-10 p-r-10" data-toggle="tooltip" data-placement="left" title="One step back" onclick="return  generateChart(3);">
                                        <i class="fa fa-arrow-left"></i>
                                    </button>
                                </div>
                            </div>

                        </div>
                    <input type="hidden" id="hdnHrs" val="0"/>
                </div>
                <div class="clearfix"></div>
                <!--  data-x-grid="false" -- >
                <!-- BEGIN CHART -->
                <a onclick="GetPrevNextTimeChart(0);" class="group1-Prev"><i class="fa fa-chevron-circle-left fa-3x"></i></a>
		<a onclick="GetPrevNextTimeChart(1);" class="group1-Next"><i class="fa fa-chevron-circle-right fa-3x"></i></a>
                
                <div id="div-chart-wrapper" style="background-color:#fff; border-radius:6px;min-height:400px;" class="m-t-10">
                </div>
                <!--div id="nvd3-area" class="col-sm-12 padding-0 line-chart m-t-10 text-center">
                    <svg id="areaSvg"></svg>
                </div>
                <div id="nvd3-line" class="col-sm-12 padding-0 line-chart m-t-10 text-center hide">
                    <svg id="lineSvg"></svg>
                </div>
                <div id="nvd3-Bar" class="col-sm-12 padding-0 line-chart m-t-30 text-center hide">
                    <svg id="barSvg"></svg>
                </div-->
                <div class="clearfix"></div>
                <!-- END CHART -->
                </div>
            </div>
  	</div>
</div>
      <div id="exportChart" class="hide">
          <canvas style="width: 500px;width: 100%"></canvas>
          <div id="svgdataurl"></div>
      </div>
      
<!-- END PANEL -->
       </div>
   </div>
   <!-- END PAGE CONTENT -->
<?php
	require_once('tpl/footer.tpl.php')
?>

<style type="text/css">
	
	a.group1-Prev, a.group1-Next {
	    
	    display: block;
	    cursor: pointer;
	    position: absolute;
	    top: 50%;
	}
	
	a.group1-Prev {
	    left: -8px;
	}
	a.group1-Next {
	    right: -8px;
	}
	
</style>
   
<script type="text/javascript">
        
        $(document).ready(function (){
            $ClusterTree.init(0);
            $("#selChartType").select2({
                minimumResultsForSearch: -1
            });
             $('#dateRange').datetimepicker({
                //daysOfWeekDisabled: [0, 6],
                format: 'MM/DD/YYYY h:mm a', 
                maxDate : new Date(),
                date : new Date(),
                sideBySide: true
            });
            
            __intTimeknobBar("#timeRange",1,"#99F969");
            //$ClusterTree.getAllNode("#chartSelDevice",1);
            getAllDevice();
            setParameter();
        });
        
        function getAllDevice(){
            $.ajax ({
                type:'POST',
                url:'ajax.php',
                data: { 'c':'LocationSettings', 'a':'getAllNodes' },
                dataType: 'json',
                async:false,
                success: function(response){
                    var sel = $('#chartSelDevice');
                    if(response.status == 1){
                        var $node = response.data;
                        for(var i=0; i<$node.length; i++){
                            //alert(device[i]['id']);
                            var $name = decodeURIComponent(($node[i]['Name']).replace(/\+/g, " "));
                            var $id = $node[i]['ID'];
                             sel.append('<option value="'+$id+'">'+$name+'</option>');
                        }
                    }
                    $(sel).select2({});
                },
                error: function(r){
                    console.log(r);
                }
            });
        
        }
        
        
        function generateChart($rangeType){//$rangeType 1=Date 2=Time
            var $type = $("#selChartType").val();
            /*var startDate = new Date(new Date($('#dateRange').data('date')));
            var endDate = new Date(new Date($('#dateRange').data('date')));
            var timeRange = $("#selDuration").select2("val");
            var inct = 1;
            if(timeRange == "5m"){inct = 5;endDate.setMinutes(endDate.getMinutes() + 5);}
            else if(timeRange == "30m"){inct = 1;endDate.setMinutes(endDate.getMinutes() + 30);}
            else if(timeRange == "1h"){inct = 1;endDate.setHours(endDate.getHours() + 1);}
            else if(timeRange == "12h"){inct = 5;endDate.setHours(endDate.getHours() + 12);}
            else if(timeRange == "24h"){inct = 1;endDate.setHours(endDate.getHours() + 24);}
            else if(timeRange == "7d"){inct = 1;endDate.setDate(endDate.getDate() + 7);}
            else if(timeRange == "30d"){inct = 1;endDate.setDate(endDate.getDate() + 30);}
            else if(timeRange == "1y"){inct = 1;endDate.setDate(endDate.getDate() + 365);}*/
            //console.log("stratDate==="+new Date(startDate)+"    End==========="+new Date(endDate));
            //return false;
            if(!selectValidation('chartSelDevice','device','divDevice') || 
                    !multiSelValidation('chartParamSelection','parameter','divParameter')) return false;
            //console.log("start....");
            
            if($type == "1"){
               $(".line-chart").addClass("hide");
               $("#nvd3-area").removeClass("hide");
               __getChartJSON()
                //initAreaChart($rangeType);
            }
            else if($type == "2"){
               $(".line-chart").addClass("hide");
               $("#nvd3-line").removeClass("hide");
               initLineChart($rangeType);
            }
            else if($type == "3"){
               $(".line-chart").addClass("hide");
               $("#nvd3-line2").removeClass("hide");
                initLineAreaChart($rangeType);
            }
            else if($type == "4"){
               $(".line-chart").addClass("hide");
               $("#nvd3-Bar").removeClass("hide");
               initBarChart($rangeType);
            }
            else if($type == "5"){
               $(".line-chart").addClass("hide");
               $("#nvd3-Scope").removeClass("hide");
                //$("#chartParamSelection").select2("val",["Voltage","Amperage"]);
               initScopeChart($rangeType);
            }
        }
        
        function GetPrevNextTimeChart(typeForward){
        	
        	var currentSelectedDate = new Date($('#dateRange').data('date'));
        	var timeRange = $("#selDuration").select2("val");
        	var fb = 1;
        	
        	if(typeForward === 0) fb = -1;
        	
        	//console.log("Before:" + currentSelectedDate);
        	
        	if(timeRange == "1m") currentSelectedDate.setMinutes(currentSelectedDate.getMinutes() + (fb * 15));
                else if(timeRange == "5m") currentSelectedDate.setMinutes(currentSelectedDate.getMinutes() + (fb * 5));
                else if(timeRange == "30m") currentSelectedDate.setMinutes(currentSelectedDate.getMinutes() + (fb * 30));
                else if(timeRange == "1h") currentSelectedDate.setHours(currentSelectedDate.getHours() + (fb * 1));
                else if(timeRange == "12h") currentSelectedDate.setHours(currentSelectedDate.getHours() + (fb * 12));
                else if(timeRange == "24h") currentSelectedDate.setHours(currentSelectedDate.getHours() + (fb * 24));
                else if(timeRange == "7d") currentSelectedDate.setDate(currentSelectedDate.getDate() + (fb * 7));
                else if(timeRange == "30d") currentSelectedDate.setDate(currentSelectedDate.getDate() + (fb * 30));
                else if(timeRange == "1y") currentSelectedDate.setYear(currentSelectedDate.getFullYear() + (fb * 1));
        	
        	if(currentSelectedDate > new Date()) return false;
        	//console.log("After:" + currentSelectedDate);
        	$('#dateRange').data("DateTimePicker").date(currentSelectedDate)
        	
        	__getChartJSON();
        }
        
        function chanerage($type){
            if($type == 1){
                $(".date-hours-tab-b").removeClass("active");
                $(".date-hours-tab-t").addClass("active");
                $(".date-hours-tab-t").find(".fa-check").removeClass("hide");
                $(".date-hours-tab-b").find(".fa-check").addClass("hide");
                $("#div-timeRange").addClass("hide");
                $("#div-dateRange").removeClass("hide");
            }
            else{
                $(".date-hours-tab-t").removeClass("active");
                $(".date-hours-tab-b").addClass("active");
                $(".date-hours-tab-b").find(".fa-check").removeClass("hide");
                $(".date-hours-tab-t").find(".fa-check").addClass("hide");
                $("#div-dateRange").addClass("hide");
                $("#div-timeRange").removeClass("hide");
            }
        }
        
        function changeChartType($val){
            if($val == 5){
                $('#chartParamSelection').select2("disable");
                $("#daterangepicker").prop("disabled", true);
                $("#timeRange").prop("disabled", true);

            }
            else{
                $('#chartParamSelection').select2("enable");
                $("#daterangepicker").prop("disabled", false);
            }
        }
        
        function setParameter(){
            $.ajax({
                type:'POST',
                url: 'ajax.php',
                data:{ 'c':'Dashboard', 'a':'getAllParameter'},
                dataType: 'json',
                async:false,
                success: function(res) {
                        var $param = res.reuslt;
                        //console.log($param);
                        var sel = $('#chartParamSelection');
                        var count =0 ;
                        if($param.length > 0 ){
                            for (var i = 0; i < $param.length; i++) {
                                var $identity = ($param[i].label).replace(/ /g,'');
                                var $selected = $identity == "DriverAKW" ? "selected" : "";
                                sel.append('<option value="'+$identity+'" '+$selected+'>'+$param[i].label+'</option>');
                                formattedGuageArray[$identity] = $param[i];
                            }
                        }
                        $("#chartParamSelection").select2({
                            maximumSelectionSize: 5,
                            formatSelectionTooBig: function (limit) {
                                return 'Sorry, only '+limit+' limit parameters allowed';
                            }
                        });
                },
                complete:function(){
                    if(window.location.href.indexOf("parameter") > -1) {
                        var $parm = getUrlParameter("parameter");
                        var $parameter = $parm.split("_")[0];
                        $("#chartParamSelection").select2("val",$parameter);
                        $(".back-db").show();
                    }
                    __getChartJSON();
                },
                error: function(r) {
                    console.log(r);
                }
            });
        }
        
        function Sceenshot(){     
            $(".group1-Next").addClass("hide");  
            $(".btn-snapshot").addClass("hide");
            $(".group1-Next").addClass("hide");
            $(".group1-Prev").addClass("hide");
            $(".btn-generateChart").addClass("hide");
                html2canvas( $("#portlet-chart"), {
                  onrendered: function(canvas) {
                    var data = canvas.toDataURL('image/png');
                    //$("#btnSelguage").removeClass("hide");
                    $(".group1-Next").removeClass("hide");  
                    $(".btn-snapshot").removeClass("hide");
                    $(".group1-Next").removeClass("hide");
                    $(".group1-Prev").removeClass("hide");
                    $(".btn-generateChart").removeClass("hide");
                    var a = document.createElement("a");
                    a.download = "dashboard.png";
                    a.href = data;
                    a.click();
                    //window.open(data);
                  }
                });
           /* if(!$("#nvd3-area").hasClass("hide")) saveSvgAsPng(document.getElementById("areaSvg"), "Area Chart.png");
            else if(!$("#nvd3-line").hasClass("hide")) saveSvgAsPng(document.getElementById("lineSvg"), "Line Chart.png");
            else if(!$("#nvd3-Bar").hasClass("hide")) saveSvgAsPng(document.getElementById("barSvg"), "Bar Chart.png");*/
        }
    
    
</script>

       